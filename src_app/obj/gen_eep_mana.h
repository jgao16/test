#ifndef GEN_EEP_MANA_H__
#define GEN_EEP_MANA_H__
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

/*****this file is generate by perl**************************************************************************************** **/
#include"stdint.h"
// #include "dev_ipc.h"

#ifdef __cplusplus
extern "C" {
#endif

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
/*
	0x03									;			(imx->vip), read all eeprom command
 	0x83,xx,xx,xx,xx						;			(vip->imx), transfer all eeprom to imx
	
	
	(imx->vip), read eeprom by offset
	0x04,offset_h,offset_l, len_h, len_l,(offset_h,offset_l, len_h, len_l);	
	
	(vip->imx), transfer eeprom to imx by offset	
	0x84,offset_h,offset_l, len_h, len_l, xx xx xx xx,(offset_h,offset_l, len_h, len_l, xx xx xx xx);	

	
	(imx->vip), write eeprom by offset
	0x05,offset_h,offset_l, len_h, len_l, xx xx xx xx,(offset_h,offset_l, len_h, len_l, xx xx xx xx);
	
	(vip->imx), write eeprom by offset success 
	0x85,offset_h,offset_l, len_h, len_l, (offset_h,offset_l, len_h, len_l)			 ;	
	
	
	
	0x20,eep_list_id0_h eep_list_id0_l eep_list_id1_h eep_list_id1_l xx xx ;				(imx->vip), read eeprom by eeprom multi list id
	0xA0,eep_list_id0_h,eep_list_id0_l xx xx xx xx xx,eep_list_id1_h eep_list_id1_l xx xx ;	(vip->imx), transfer eeprom to imx by multi eeprom list id 
	
	
	0x21,eep_list_id0_h,eep_list_id0_l xx xx xx xx xx,eep_list_id1_h eep_list_id1_l xx xx;		(imx->vip), write eeprom by eeprom list id
	0xA1,eep_list_id0_h eep_list_id0_l eep_list_id1_h eep_list_id1_l xx xx ;					(vip->imx), write eeprom by list id success 
2.	
*/

//<<INCLUDE>>

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#define EEP_ITEM_TYPE_8			(0)
#define EEP_ITEM_TYPE_16		(1)
#define EEP_ITEM_TYPE_32		(2)

#define EEP_GEN_TIME	(1571293326)


#define EEP_NORMAL_ADDR	(0u)	/*lint -e534 -e835*/
#define EEP_BACKUK_ADDR	(1024u)
#define EEP_SECTOR_SIZE	(8u)
#define EEP_PROGRAM_SIZE	(4u)


#define DFLASH_START_ADDR	(0x10000000)
#define DFLASH_SECTOR_SIZE	(4096)
#define DFLASH_PROGRAM_SIZE	(8u)


#define EEP_BLOCK_manufacture_BLOCK_ID			(0)
#define	EEP_BLOCK_manufacture_LEN  (512u)
#define EEP_BLOCK_manufacture_IS_EEPROM (FALSE)
#define EEP_BLOCK_manufacture_DFLASH_START_ADDR	(0+DFLASH_START_ADDR)
#define EEP_BLOCK_manufacture_DFLASH_END_ADDR	(8191+DFLASH_START_ADDR)
#define EEP_BLOCK_manufacture_DFLASH_SECTOR_START (0)
#define EEP_BLOCK_manufacture_DFLASH_SECTOR_END	(1)
#define EEP_BLOCK_manufacture_DFLASH_SECTOR_NUM 	(2)
#define EEP_BLOCK_development_nvm_BLOCK_ID			(1)
#define	EEP_BLOCK_development_nvm_LEN  (1024u)
#define EEP_BLOCK_development_nvm_IS_EEPROM (FALSE)
#define EEP_BLOCK_development_nvm_DFLASH_START_ADDR	(8192+DFLASH_START_ADDR)
#define EEP_BLOCK_development_nvm_DFLASH_END_ADDR	(16383+DFLASH_START_ADDR)
#define EEP_BLOCK_development_nvm_DFLASH_SECTOR_START (2)
#define EEP_BLOCK_development_nvm_DFLASH_SECTOR_END	(3)
#define EEP_BLOCK_development_nvm_DFLASH_SECTOR_NUM 	(2)
#define EEP_BLOCK_OemSetting_BLOCK_ID			(2)
#define	EEP_BLOCK_OemSetting_LEN  (256u)
#define EEP_BLOCK_OemSetting_IS_EEPROM (FALSE)
#define EEP_BLOCK_OemSetting_DFLASH_START_ADDR	(16384+DFLASH_START_ADDR)
#define EEP_BLOCK_OemSetting_DFLASH_END_ADDR	(24575+DFLASH_START_ADDR)
#define EEP_BLOCK_OemSetting_DFLASH_SECTOR_START (4)
#define EEP_BLOCK_OemSetting_DFLASH_SECTOR_END	(5)
#define EEP_BLOCK_OemSetting_DFLASH_SECTOR_NUM 	(2)
#define EEP_BLOCK_user_setting_BLOCK_ID			(3)
#define	EEP_BLOCK_user_setting_LEN  (256u)
#define EEP_BLOCK_user_setting_IS_EEPROM (FALSE)
#define EEP_BLOCK_user_setting_DFLASH_START_ADDR	(24576+DFLASH_START_ADDR)
#define EEP_BLOCK_user_setting_DFLASH_END_ADDR	(32767+DFLASH_START_ADDR)
#define EEP_BLOCK_user_setting_DFLASH_SECTOR_START (6)
#define EEP_BLOCK_user_setting_DFLASH_SECTOR_END	(7)
#define EEP_BLOCK_user_setting_DFLASH_SECTOR_NUM 	(2)
#define EEP_BLOCK_DTC_INFO_BLOCK_ID			(4)
#define	EEP_BLOCK_DTC_INFO_LEN  (256u)
#define EEP_BLOCK_DTC_INFO_IS_EEPROM (FALSE)
#define EEP_BLOCK_DTC_INFO_DFLASH_START_ADDR	(32768+DFLASH_START_ADDR)
#define EEP_BLOCK_DTC_INFO_DFLASH_END_ADDR	(40959+DFLASH_START_ADDR)
#define EEP_BLOCK_DTC_INFO_DFLASH_SECTOR_START (8)
#define EEP_BLOCK_DTC_INFO_DFLASH_SECTOR_END	(9)
#define EEP_BLOCK_DTC_INFO_DFLASH_SECTOR_NUM 	(2)
#define EEP_BLOCK_OdoInfo_BLOCK_ID			(5)
#define	EEP_BLOCK_OdoInfo_LEN  (64u)
#define EEP_BLOCK_OdoInfo_IS_EEPROM (FALSE)
#define EEP_BLOCK_OdoInfo_DFLASH_START_ADDR	(40960+DFLASH_START_ADDR)
#define EEP_BLOCK_OdoInfo_DFLASH_END_ADDR	(73727+DFLASH_START_ADDR)
#define EEP_BLOCK_OdoInfo_DFLASH_SECTOR_START (10)
#define EEP_BLOCK_OdoInfo_DFLASH_SECTOR_END	(17)
#define EEP_BLOCK_OdoInfo_DFLASH_SECTOR_NUM 	(8)
#define EEP_BLOCK_share_eep_BLOCK_ID			(6)
#define	EEP_BLOCK_share_eep_LEN  (128u)
#define EEP_BLOCK_share_eep_IS_EEPROM (FALSE)
#define EEP_BLOCK_share_eep_DFLASH_START_ADDR	(114688+DFLASH_START_ADDR)
#define EEP_BLOCK_share_eep_DFLASH_END_ADDR	(122879+DFLASH_START_ADDR)
#define EEP_BLOCK_share_eep_DFLASH_SECTOR_START (28)
#define EEP_BLOCK_share_eep_DFLASH_SECTOR_END	(29)
#define EEP_BLOCK_share_eep_DFLASH_SECTOR_NUM 	(2)
#define EEP_BLOCK_ResetInfo_BLOCK_ID			(7)
#define	EEP_BLOCK_ResetInfo_LEN  (128u)
#define EEP_BLOCK_ResetInfo_IS_EEPROM (FALSE)
#define EEP_BLOCK_ResetInfo_DFLASH_START_ADDR	(122880+DFLASH_START_ADDR)
#define EEP_BLOCK_ResetInfo_DFLASH_END_ADDR	(131071+DFLASH_START_ADDR)
#define EEP_BLOCK_ResetInfo_DFLASH_SECTOR_START (30)
#define EEP_BLOCK_ResetInfo_DFLASH_SECTOR_END	(31)
#define EEP_BLOCK_ResetInfo_DFLASH_SECTOR_NUM 	(2)
#define	EEP_BLOCK_NUMBER_MAX	(8)


#define EEP_CONTENT_Manuf_Visteon_Part_Number_LIST_ID				(0)
#define EEP_CONTENT_Manuf_Visteon_Part_Number_ITEM_LEN		(17)
#define EEP_CONTENT_Manuf_Visteon_Part_Number_IN_BLOCK_NUM 	(0)
#define EEP_CONTENT_Manuf_Visteon_Part_Number_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_Manuf_Visteon_Part_Number_ITEM_BYTE_LEN		17
#define EEP_CONTENT_Manuf_Visteon_Part_Number_CONST_VARIBLE	(0u)
#define EEP_CONTENT_Manuf_EquippedPCB_VPN_Sub_LIST_ID				(1)
#define EEP_CONTENT_Manuf_EquippedPCB_VPN_Sub_ITEM_LEN		(17)
#define EEP_CONTENT_Manuf_EquippedPCB_VPN_Sub_IN_BLOCK_NUM 	(0)
#define EEP_CONTENT_Manuf_EquippedPCB_VPN_Sub_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_Manuf_EquippedPCB_VPN_Sub_ITEM_BYTE_LEN		17
#define EEP_CONTENT_Manuf_EquippedPCB_VPN_Sub_CONST_VARIBLE	(0u)
#define EEP_CONTENT_Manuf_EquippedPCB_VPN_Main_LIST_ID				(2)
#define EEP_CONTENT_Manuf_EquippedPCB_VPN_Main_ITEM_LEN		(17)
#define EEP_CONTENT_Manuf_EquippedPCB_VPN_Main_IN_BLOCK_NUM 	(0)
#define EEP_CONTENT_Manuf_EquippedPCB_VPN_Main_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_Manuf_EquippedPCB_VPN_Main_ITEM_BYTE_LEN		17
#define EEP_CONTENT_Manuf_EquippedPCB_VPN_Main_CONST_VARIBLE	(0u)
#define EEP_CONTENT_Manuf_Product_Serial_Number_LIST_ID				(3)
#define EEP_CONTENT_Manuf_Product_Serial_Number_ITEM_LEN		(5)
#define EEP_CONTENT_Manuf_Product_Serial_Number_IN_BLOCK_NUM 	(0)
#define EEP_CONTENT_Manuf_Product_Serial_Number_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_Manuf_Product_Serial_Number_ITEM_BYTE_LEN		5
#define EEP_CONTENT_Manuf_Product_Serial_Number_CONST_VARIBLE	(0u)
#define EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Sub_LIST_ID				(4)
#define EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Sub_ITEM_LEN		(5)
#define EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Sub_IN_BLOCK_NUM 	(0)
#define EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Sub_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Sub_ITEM_BYTE_LEN		5
#define EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Sub_CONST_VARIBLE	(0u)
#define EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Main_LIST_ID				(5)
#define EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Main_ITEM_LEN		(5)
#define EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Main_IN_BLOCK_NUM 	(0)
#define EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Main_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Main_ITEM_BYTE_LEN		5
#define EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Main_CONST_VARIBLE	(0u)
#define EEP_CONTENT_NVM_Revision_LIST_ID				(6)
#define EEP_CONTENT_NVM_Revision_ITEM_LEN		(1)
#define EEP_CONTENT_NVM_Revision_IN_BLOCK_NUM 	(0)
#define EEP_CONTENT_NVM_Revision_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_NVM_Revision_ITEM_BYTE_LEN		1
#define EEP_CONTENT_NVM_Revision_CONST_VARIBLE	(0u)
#define EEP_CONTENT_Manuf_SMD_Date1_LIST_ID				(7)
#define EEP_CONTENT_Manuf_SMD_Date1_ITEM_LEN		(3)
#define EEP_CONTENT_Manuf_SMD_Date1_IN_BLOCK_NUM 	(0)
#define EEP_CONTENT_Manuf_SMD_Date1_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_Manuf_SMD_Date1_ITEM_BYTE_LEN		3
#define EEP_CONTENT_Manuf_SMD_Date1_CONST_VARIBLE	(0u)
#define EEP_CONTENT_Manuf_SMD_Date2_LIST_ID				(8)
#define EEP_CONTENT_Manuf_SMD_Date2_ITEM_LEN		(3)
#define EEP_CONTENT_Manuf_SMD_Date2_IN_BLOCK_NUM 	(0)
#define EEP_CONTENT_Manuf_SMD_Date2_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_Manuf_SMD_Date2_ITEM_BYTE_LEN		3
#define EEP_CONTENT_Manuf_SMD_Date2_CONST_VARIBLE	(0u)
#define EEP_CONTENT_Manuf_Assembly_Date_LIST_ID				(9)
#define EEP_CONTENT_Manuf_Assembly_Date_ITEM_LEN		(3)
#define EEP_CONTENT_Manuf_Assembly_Date_IN_BLOCK_NUM 	(0)
#define EEP_CONTENT_Manuf_Assembly_Date_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_Manuf_Assembly_Date_ITEM_BYTE_LEN		3
#define EEP_CONTENT_Manuf_Assembly_Date_CONST_VARIBLE	(0u)
#define EEP_CONTENT_Traceability_Data_LIST_ID				(10)
#define EEP_CONTENT_Traceability_Data_ITEM_LEN		(9)
#define EEP_CONTENT_Traceability_Data_IN_BLOCK_NUM 	(0)
#define EEP_CONTENT_Traceability_Data_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_Traceability_Data_ITEM_BYTE_LEN		9
#define EEP_CONTENT_Traceability_Data_CONST_VARIBLE	(0u)
#define EEP_CONTENT_VehicleManufacturerSparePartNumber_LIST_ID				(11)
#define EEP_CONTENT_VehicleManufacturerSparePartNumber_ITEM_LEN		(10)
#define EEP_CONTENT_VehicleManufacturerSparePartNumber_IN_BLOCK_NUM 	(0)
#define EEP_CONTENT_VehicleManufacturerSparePartNumber_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_VehicleManufacturerSparePartNumber_ITEM_BYTE_LEN		10
#define EEP_CONTENT_VehicleManufacturerSparePartNumber_CONST_VARIBLE	(0u)
#define EEP_CONTENT_VehicleManufacturerSparePartNumber_Assembly_LIST_ID				(12)
#define EEP_CONTENT_VehicleManufacturerSparePartNumber_Assembly_ITEM_LEN		(10)
#define EEP_CONTENT_VehicleManufacturerSparePartNumber_Assembly_IN_BLOCK_NUM 	(0)
#define EEP_CONTENT_VehicleManufacturerSparePartNumber_Assembly_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_VehicleManufacturerSparePartNumber_Assembly_ITEM_BYTE_LEN		10
#define EEP_CONTENT_VehicleManufacturerSparePartNumber_Assembly_CONST_VARIBLE	(0u)
#define EEP_CONTENT_Operational_Reference_LIST_ID				(13)
#define EEP_CONTENT_Operational_Reference_ITEM_LEN		(10)
#define EEP_CONTENT_Operational_Reference_IN_BLOCK_NUM 	(0)
#define EEP_CONTENT_Operational_Reference_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_Operational_Reference_ITEM_BYTE_LEN		10
#define EEP_CONTENT_Operational_Reference_CONST_VARIBLE	(0u)
#define EEP_CONTENT_Supplier_Number_LIST_ID				(14)
#define EEP_CONTENT_Supplier_Number_ITEM_LEN		(4)
#define EEP_CONTENT_Supplier_Number_IN_BLOCK_NUM 	(0)
#define EEP_CONTENT_Supplier_Number_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_Supplier_Number_ITEM_BYTE_LEN		4
#define EEP_CONTENT_Supplier_Number_CONST_VARIBLE	(0u)
#define EEP_CONTENT_ECU_Serial_Number_LIST_ID				(15)
#define EEP_CONTENT_ECU_Serial_Number_ITEM_LEN		(20)
#define EEP_CONTENT_ECU_Serial_Number_IN_BLOCK_NUM 	(0)
#define EEP_CONTENT_ECU_Serial_Number_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_ECU_Serial_Number_ITEM_BYTE_LEN		20
#define EEP_CONTENT_ECU_Serial_Number_CONST_VARIBLE	(0u)
#define EEP_CONTENT_Manufacturing_Identification_Code_LIST_ID				(16)
#define EEP_CONTENT_Manufacturing_Identification_Code_ITEM_LEN		(1)
#define EEP_CONTENT_Manufacturing_Identification_Code_IN_BLOCK_NUM 	(0)
#define EEP_CONTENT_Manufacturing_Identification_Code_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_Manufacturing_Identification_Code_ITEM_BYTE_LEN		1
#define EEP_CONTENT_Manufacturing_Identification_Code_CONST_VARIBLE	(0u)
#define EEP_CONTENT_VDIAG_LIST_ID				(17)
#define EEP_CONTENT_VDIAG_ITEM_LEN		(1)
#define EEP_CONTENT_VDIAG_IN_BLOCK_NUM 	(0)
#define EEP_CONTENT_VDIAG_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_VDIAG_ITEM_BYTE_LEN		1
#define EEP_CONTENT_VDIAG_CONST_VARIBLE	(0u)
#define EEP_CONTENT_Config_EQ1_LIST_ID				(18)
#define EEP_CONTENT_Config_EQ1_ITEM_LEN		(1)
#define EEP_CONTENT_Config_EQ1_IN_BLOCK_NUM 	(0)
#define EEP_CONTENT_Config_EQ1_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_Config_EQ1_ITEM_BYTE_LEN		1
#define EEP_CONTENT_Config_EQ1_CONST_VARIBLE	(0u)
#define EEP_CONTENT_Vehicle_Type_LIST_ID				(19)
#define EEP_CONTENT_Vehicle_Type_ITEM_LEN		(1)
#define EEP_CONTENT_Vehicle_Type_IN_BLOCK_NUM 	(0)
#define EEP_CONTENT_Vehicle_Type_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_Vehicle_Type_ITEM_BYTE_LEN		1
#define EEP_CONTENT_Vehicle_Type_CONST_VARIBLE	(0u)
#define EEP_CONTENT_UUID_LIST_ID				(20)
#define EEP_CONTENT_UUID_ITEM_LEN		(16)
#define EEP_CONTENT_UUID_IN_BLOCK_NUM 	(0)
#define EEP_CONTENT_UUID_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_UUID_ITEM_BYTE_LEN		16
#define EEP_CONTENT_UUID_CONST_VARIBLE	(0u)
#define EEP_CONTENT_NAVI_ID_LIST_ID				(21)
#define EEP_CONTENT_NAVI_ID_ITEM_LEN		(16)
#define EEP_CONTENT_NAVI_ID_IN_BLOCK_NUM 	(0)
#define EEP_CONTENT_NAVI_ID_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_NAVI_ID_ITEM_BYTE_LEN		16
#define EEP_CONTENT_NAVI_ID_CONST_VARIBLE	(0u)
#define EEP_CONTENT_DA_ID_LIST_ID				(22)
#define EEP_CONTENT_DA_ID_ITEM_LEN		(16)
#define EEP_CONTENT_DA_ID_IN_BLOCK_NUM 	(0)
#define EEP_CONTENT_DA_ID_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_DA_ID_ITEM_BYTE_LEN		16
#define EEP_CONTENT_DA_ID_CONST_VARIBLE	(0u)
#define EEP_CONTENT_DAMainBoardHwVer_LIST_ID				(23)
#define EEP_CONTENT_DAMainBoardHwVer_ITEM_LEN		(16)
#define EEP_CONTENT_DAMainBoardHwVer_IN_BLOCK_NUM 	(0)
#define EEP_CONTENT_DAMainBoardHwVer_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_DAMainBoardHwVer_ITEM_BYTE_LEN		16
#define EEP_CONTENT_DAMainBoardHwVer_CONST_VARIBLE	(0u)
#define EEP_CONTENT_SW_Version_Date_Format_LIST_ID				(24)
#define EEP_CONTENT_SW_Version_Date_Format_ITEM_LEN		(18)
#define EEP_CONTENT_SW_Version_Date_Format_IN_BLOCK_NUM 	(0)
#define EEP_CONTENT_SW_Version_Date_Format_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_SW_Version_Date_Format_ITEM_BYTE_LEN		18
#define EEP_CONTENT_SW_Version_Date_Format_CONST_VARIBLE	(0u)
#define EEP_CONTENT_SW_Edition_Version_LIST_ID				(25)
#define EEP_CONTENT_SW_Edition_Version_ITEM_LEN		(18)
#define EEP_CONTENT_SW_Edition_Version_IN_BLOCK_NUM 	(0)
#define EEP_CONTENT_SW_Edition_Version_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_SW_Edition_Version_ITEM_BYTE_LEN		18
#define EEP_CONTENT_SW_Edition_Version_CONST_VARIBLE	(0u)
#define EEP_CONTENT_VehicleManufacturerSparePartNumber_Nissan_LIST_ID				(26)
#define EEP_CONTENT_VehicleManufacturerSparePartNumber_Nissan_ITEM_LEN		(10)
#define EEP_CONTENT_VehicleManufacturerSparePartNumber_Nissan_IN_BLOCK_NUM 	(0)
#define EEP_CONTENT_VehicleManufacturerSparePartNumber_Nissan_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_VehicleManufacturerSparePartNumber_Nissan_ITEM_BYTE_LEN		10
#define EEP_CONTENT_VehicleManufacturerSparePartNumber_Nissan_CONST_VARIBLE	(0u)
#define EEP_CONTENT_VisteonProductPartNumber_LIST_ID				(27)
#define EEP_CONTENT_VisteonProductPartNumber_ITEM_LEN		(17)
#define EEP_CONTENT_VisteonProductPartNumber_IN_BLOCK_NUM 	(0)
#define EEP_CONTENT_VisteonProductPartNumber_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_VisteonProductPartNumber_ITEM_BYTE_LEN		17
#define EEP_CONTENT_VisteonProductPartNumber_CONST_VARIBLE	(0u)
#define EEP_CONTENT_EquippedPCBVisteonPartNumMainBorad_LIST_ID				(28)
#define EEP_CONTENT_EquippedPCBVisteonPartNumMainBorad_ITEM_LEN		(17)
#define EEP_CONTENT_EquippedPCBVisteonPartNumMainBorad_IN_BLOCK_NUM 	(0)
#define EEP_CONTENT_EquippedPCBVisteonPartNumMainBorad_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_EquippedPCBVisteonPartNumMainBorad_ITEM_BYTE_LEN		17
#define EEP_CONTENT_EquippedPCBVisteonPartNumMainBorad_CONST_VARIBLE	(0u)
#define EEP_CONTENT_EquippedPCBVisteonPartNumSubBorad_LIST_ID				(29)
#define EEP_CONTENT_EquippedPCBVisteonPartNumSubBorad_ITEM_LEN		(17)
#define EEP_CONTENT_EquippedPCBVisteonPartNumSubBorad_IN_BLOCK_NUM 	(0)
#define EEP_CONTENT_EquippedPCBVisteonPartNumSubBorad_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_EquippedPCBVisteonPartNumSubBorad_ITEM_BYTE_LEN		17
#define EEP_CONTENT_EquippedPCBVisteonPartNumSubBorad_CONST_VARIBLE	(0u)
#define EEP_CONTENT_DAUniqueID_LIST_ID				(30)
#define EEP_CONTENT_DAUniqueID_ITEM_LEN		(16)
#define EEP_CONTENT_DAUniqueID_IN_BLOCK_NUM 	(0)
#define EEP_CONTENT_DAUniqueID_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_DAUniqueID_ITEM_BYTE_LEN		16
#define EEP_CONTENT_DAUniqueID_CONST_VARIBLE	(0u)
#define EEP_CONTENT_ProductSerialNum_LIST_ID				(31)
#define EEP_CONTENT_ProductSerialNum_ITEM_LEN		(5)
#define EEP_CONTENT_ProductSerialNum_IN_BLOCK_NUM 	(0)
#define EEP_CONTENT_ProductSerialNum_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_ProductSerialNum_ITEM_BYTE_LEN		5
#define EEP_CONTENT_ProductSerialNum_CONST_VARIBLE	(0u)
#define EEP_CONTENT_ProductSerialNumMainBoard_LIST_ID				(32)
#define EEP_CONTENT_ProductSerialNumMainBoard_ITEM_LEN		(5)
#define EEP_CONTENT_ProductSerialNumMainBoard_IN_BLOCK_NUM 	(0)
#define EEP_CONTENT_ProductSerialNumMainBoard_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_ProductSerialNumMainBoard_ITEM_BYTE_LEN		5
#define EEP_CONTENT_ProductSerialNumMainBoard_CONST_VARIBLE	(0u)
#define EEP_CONTENT_ProductSerialNumSubBoard_LIST_ID				(33)
#define EEP_CONTENT_ProductSerialNumSubBoard_ITEM_LEN		(5)
#define EEP_CONTENT_ProductSerialNumSubBoard_IN_BLOCK_NUM 	(0)
#define EEP_CONTENT_ProductSerialNumSubBoard_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_ProductSerialNumSubBoard_ITEM_BYTE_LEN		5
#define EEP_CONTENT_ProductSerialNumSubBoard_CONST_VARIBLE	(0u)
#define EEP_CONTENT_SMDManufacturingDate_LIST_ID				(34)
#define EEP_CONTENT_SMDManufacturingDate_ITEM_LEN		(3)
#define EEP_CONTENT_SMDManufacturingDate_IN_BLOCK_NUM 	(0)
#define EEP_CONTENT_SMDManufacturingDate_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_SMDManufacturingDate_ITEM_BYTE_LEN		3
#define EEP_CONTENT_SMDManufacturingDate_CONST_VARIBLE	(0u)
#define EEP_CONTENT_AssemblyManufacturingDate_LIST_ID				(35)
#define EEP_CONTENT_AssemblyManufacturingDate_ITEM_LEN		(3)
#define EEP_CONTENT_AssemblyManufacturingDate_IN_BLOCK_NUM 	(0)
#define EEP_CONTENT_AssemblyManufacturingDate_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_AssemblyManufacturingDate_ITEM_BYTE_LEN		3
#define EEP_CONTENT_AssemblyManufacturingDate_CONST_VARIBLE	(0u)
#define EEP_CONTENT_TraceabilityBytes_LIST_ID				(36)
#define EEP_CONTENT_TraceabilityBytes_ITEM_LEN		(3)
#define EEP_CONTENT_TraceabilityBytes_IN_BLOCK_NUM 	(0)
#define EEP_CONTENT_TraceabilityBytes_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_TraceabilityBytes_ITEM_BYTE_LEN		3
#define EEP_CONTENT_TraceabilityBytes_CONST_VARIBLE	(0u)
#define EEP_CONTENT_SMDPlantNum_LIST_ID				(37)
#define EEP_CONTENT_SMDPlantNum_ITEM_LEN		(3)
#define EEP_CONTENT_SMDPlantNum_IN_BLOCK_NUM 	(0)
#define EEP_CONTENT_SMDPlantNum_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_SMDPlantNum_ITEM_BYTE_LEN		3
#define EEP_CONTENT_SMDPlantNum_CONST_VARIBLE	(0u)
#define EEP_CONTENT_AssemblyPlantNum_LIST_ID				(38)
#define EEP_CONTENT_AssemblyPlantNum_ITEM_LEN		(3)
#define EEP_CONTENT_AssemblyPlantNum_IN_BLOCK_NUM 	(0)
#define EEP_CONTENT_AssemblyPlantNum_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_AssemblyPlantNum_ITEM_BYTE_LEN		3
#define EEP_CONTENT_AssemblyPlantNum_CONST_VARIBLE	(0u)
#define EEP_CONTENT_DEM_EVENT_CAN_COM_DATA_LIST_ID				(39)
#define EEP_CONTENT_DEM_EVENT_CAN_COM_DATA_ITEM_LEN		(10)
#define EEP_CONTENT_DEM_EVENT_CAN_COM_DATA_IN_BLOCK_NUM 	(0)
#define EEP_CONTENT_DEM_EVENT_CAN_COM_DATA_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_DEM_EVENT_CAN_COM_DATA_ITEM_BYTE_LEN		10
#define EEP_CONTENT_DEM_EVENT_CAN_COM_DATA_CONST_VARIBLE	(0u)
#define EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_LIST_ID				(40)
#define EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_LEN		(10)
#define EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_IN_BLOCK_NUM 	(0)
#define EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_BYTE_LEN		10
#define EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_CONST_VARIBLE	(0u)
#define EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_LIST_ID				(41)
#define EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_LEN		(10)
#define EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_IN_BLOCK_NUM 	(0)
#define EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_BYTE_LEN		10
#define EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_CONST_VARIBLE	(0u)
#define EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_LIST_ID				(42)
#define EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_LEN		(10)
#define EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_IN_BLOCK_NUM 	(0)
#define EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_BYTE_LEN		10
#define EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_CONST_VARIBLE	(0u)
#define EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_LIST_ID				(43)
#define EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_LEN		(10)
#define EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_IN_BLOCK_NUM 	(0)
#define EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_BYTE_LEN		10
#define EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_CONST_VARIBLE	(0u)
#define EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_LIST_ID				(44)
#define EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_LEN		(10)
#define EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_IN_BLOCK_NUM 	(0)
#define EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_BYTE_LEN		10
#define EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_CONST_VARIBLE	(0u)
#define EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_LIST_ID				(45)
#define EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_LEN		(10)
#define EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_IN_BLOCK_NUM 	(0)
#define EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_BYTE_LEN		10
#define EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_CONST_VARIBLE	(0u)
#define EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_LIST_ID				(46)
#define EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_LEN		(10)
#define EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_IN_BLOCK_NUM 	(0)
#define EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_BYTE_LEN		10
#define EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_CONST_VARIBLE	(0u)
#define EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_LIST_ID				(47)
#define EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_LEN		(10)
#define EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_IN_BLOCK_NUM 	(0)
#define EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_BYTE_LEN		10
#define EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_CONST_VARIBLE	(0u)
#define EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_LIST_ID				(48)
#define EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_ITEM_LEN		(10)
#define EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_IN_BLOCK_NUM 	(0)
#define EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_ITEM_BYTE_LEN		10
#define EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_CONST_VARIBLE	(0u)
#define EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_LIST_ID				(49)
#define EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_ITEM_LEN		(10)
#define EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_IN_BLOCK_NUM 	(0)
#define EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_ITEM_BYTE_LEN		10
#define EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_CONST_VARIBLE	(0u)
#define EEP_CONTENT_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_LIST_ID				(50)
#define EEP_CONTENT_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_ITEM_LEN		(10)
#define EEP_CONTENT_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_IN_BLOCK_NUM 	(0)
#define EEP_CONTENT_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_ITEM_BYTE_LEN		10
#define EEP_CONTENT_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_CONST_VARIBLE	(0u)
#define EEP_CONTENT_TachoUpValue_LIST_ID				(51)
#define EEP_CONTENT_TachoUpValue_ITEM_LEN		(2)
#define EEP_CONTENT_TachoUpValue_IN_BLOCK_NUM 	(1)
#define EEP_CONTENT_TachoUpValue_ITEM_TYPE		EEP_ITEM_TYPE_16
#define EEP_CONTENT_TachoUpValue_ITEM_BYTE_LEN		4
#define EEP_CONTENT_TachoUpValue_CONST_VARIBLE	(1u)
#define EEP_CONTENT_TachoLowValue_LIST_ID				(52)
#define EEP_CONTENT_TachoLowValue_ITEM_LEN		(2)
#define EEP_CONTENT_TachoLowValue_IN_BLOCK_NUM 	(1)
#define EEP_CONTENT_TachoLowValue_ITEM_TYPE		EEP_ITEM_TYPE_16
#define EEP_CONTENT_TachoLowValue_ITEM_BYTE_LEN		4
#define EEP_CONTENT_TachoLowValue_CONST_VARIBLE	(1u)
#define EEP_CONTENT_TachoLimitValue_LIST_ID				(53)
#define EEP_CONTENT_TachoLimitValue_ITEM_LEN		(2)
#define EEP_CONTENT_TachoLimitValue_IN_BLOCK_NUM 	(1)
#define EEP_CONTENT_TachoLimitValue_ITEM_TYPE		EEP_ITEM_TYPE_16
#define EEP_CONTENT_TachoLimitValue_ITEM_BYTE_LEN		4
#define EEP_CONTENT_TachoLimitValue_CONST_VARIBLE	(1u)
#define EEP_CONTENT_TachoUDeltaLimit_LIST_ID				(54)
#define EEP_CONTENT_TachoUDeltaLimit_ITEM_LEN		(2)
#define EEP_CONTENT_TachoUDeltaLimit_IN_BLOCK_NUM 	(1)
#define EEP_CONTENT_TachoUDeltaLimit_ITEM_TYPE		EEP_ITEM_TYPE_16
#define EEP_CONTENT_TachoUDeltaLimit_ITEM_BYTE_LEN		4
#define EEP_CONTENT_TachoUDeltaLimit_CONST_VARIBLE	(1u)
#define EEP_CONTENT_TachoLDeltaLimit_LIST_ID				(55)
#define EEP_CONTENT_TachoLDeltaLimit_ITEM_LEN		(2)
#define EEP_CONTENT_TachoLDeltaLimit_IN_BLOCK_NUM 	(1)
#define EEP_CONTENT_TachoLDeltaLimit_ITEM_TYPE		EEP_ITEM_TYPE_16
#define EEP_CONTENT_TachoLDeltaLimit_ITEM_BYTE_LEN		4
#define EEP_CONTENT_TachoLDeltaLimit_CONST_VARIBLE	(1u)
#define EEP_CONTENT_TachoUpChangeValue_LIST_ID				(56)
#define EEP_CONTENT_TachoUpChangeValue_ITEM_LEN		(2)
#define EEP_CONTENT_TachoUpChangeValue_IN_BLOCK_NUM 	(1)
#define EEP_CONTENT_TachoUpChangeValue_ITEM_TYPE		EEP_ITEM_TYPE_16
#define EEP_CONTENT_TachoUpChangeValue_ITEM_BYTE_LEN		4
#define EEP_CONTENT_TachoUpChangeValue_CONST_VARIBLE	(1u)
#define EEP_CONTENT_TachoLowChangeValue_LIST_ID				(57)
#define EEP_CONTENT_TachoLowChangeValue_ITEM_LEN		(2)
#define EEP_CONTENT_TachoLowChangeValue_IN_BLOCK_NUM 	(1)
#define EEP_CONTENT_TachoLowChangeValue_ITEM_TYPE		EEP_ITEM_TYPE_16
#define EEP_CONTENT_TachoLowChangeValue_ITEM_BYTE_LEN		4
#define EEP_CONTENT_TachoLowChangeValue_CONST_VARIBLE	(1u)
#define EEP_CONTENT_TachoRun_LIST_ID				(58)
#define EEP_CONTENT_TachoRun_ITEM_LEN		(2)
#define EEP_CONTENT_TachoRun_IN_BLOCK_NUM 	(1)
#define EEP_CONTENT_TachoRun_ITEM_TYPE		EEP_ITEM_TYPE_16
#define EEP_CONTENT_TachoRun_ITEM_BYTE_LEN		4
#define EEP_CONTENT_TachoRun_CONST_VARIBLE	(1u)
#define EEP_CONTENT_Speed_Rm_1_LIST_ID				(59)
#define EEP_CONTENT_Speed_Rm_1_ITEM_LEN		(1)
#define EEP_CONTENT_Speed_Rm_1_IN_BLOCK_NUM 	(1)
#define EEP_CONTENT_Speed_Rm_1_ITEM_TYPE		EEP_ITEM_TYPE_16
#define EEP_CONTENT_Speed_Rm_1_ITEM_BYTE_LEN		2
#define EEP_CONTENT_Speed_Rm_1_CONST_VARIBLE	(1u)
#define EEP_CONTENT_Speed_Rm_2_LIST_ID				(60)
#define EEP_CONTENT_Speed_Rm_2_ITEM_LEN		(1)
#define EEP_CONTENT_Speed_Rm_2_IN_BLOCK_NUM 	(1)
#define EEP_CONTENT_Speed_Rm_2_ITEM_TYPE		EEP_ITEM_TYPE_16
#define EEP_CONTENT_Speed_Rm_2_ITEM_BYTE_LEN		2
#define EEP_CONTENT_Speed_Rm_2_CONST_VARIBLE	(1u)
#define EEP_CONTENT_Speed_Rc_LIST_ID				(61)
#define EEP_CONTENT_Speed_Rc_ITEM_LEN		(1)
#define EEP_CONTENT_Speed_Rc_IN_BLOCK_NUM 	(1)
#define EEP_CONTENT_Speed_Rc_ITEM_TYPE		EEP_ITEM_TYPE_16
#define EEP_CONTENT_Speed_Rc_ITEM_BYTE_LEN		2
#define EEP_CONTENT_Speed_Rc_CONST_VARIBLE	(1u)
#define EEP_CONTENT_FuelTelltaleOn_R_LIST_ID				(62)
#define EEP_CONTENT_FuelTelltaleOn_R_ITEM_LEN		(1)
#define EEP_CONTENT_FuelTelltaleOn_R_IN_BLOCK_NUM 	(1)
#define EEP_CONTENT_FuelTelltaleOn_R_ITEM_TYPE		EEP_ITEM_TYPE_16
#define EEP_CONTENT_FuelTelltaleOn_R_ITEM_BYTE_LEN		2
#define EEP_CONTENT_FuelTelltaleOn_R_CONST_VARIBLE	(1u)
#define EEP_CONTENT_FuelTelltaleOff_R_LIST_ID				(63)
#define EEP_CONTENT_FuelTelltaleOff_R_ITEM_LEN		(1)
#define EEP_CONTENT_FuelTelltaleOff_R_IN_BLOCK_NUM 	(1)
#define EEP_CONTENT_FuelTelltaleOff_R_ITEM_TYPE		EEP_ITEM_TYPE_16
#define EEP_CONTENT_FuelTelltaleOff_R_ITEM_BYTE_LEN		2
#define EEP_CONTENT_FuelTelltaleOff_R_CONST_VARIBLE	(1u)
#define EEP_CONTENT_FuelWarningOnPoint_R_LIST_ID				(64)
#define EEP_CONTENT_FuelWarningOnPoint_R_ITEM_LEN		(1)
#define EEP_CONTENT_FuelWarningOnPoint_R_IN_BLOCK_NUM 	(1)
#define EEP_CONTENT_FuelWarningOnPoint_R_ITEM_TYPE		EEP_ITEM_TYPE_16
#define EEP_CONTENT_FuelWarningOnPoint_R_ITEM_BYTE_LEN		2
#define EEP_CONTENT_FuelWarningOnPoint_R_CONST_VARIBLE	(1u)
#define EEP_CONTENT_Fuel_R_Open_LIST_ID				(65)
#define EEP_CONTENT_Fuel_R_Open_ITEM_LEN		(1)
#define EEP_CONTENT_Fuel_R_Open_IN_BLOCK_NUM 	(1)
#define EEP_CONTENT_Fuel_R_Open_ITEM_TYPE		EEP_ITEM_TYPE_16
#define EEP_CONTENT_Fuel_R_Open_ITEM_BYTE_LEN		2
#define EEP_CONTENT_Fuel_R_Open_CONST_VARIBLE	(1u)
#define EEP_CONTENT_Fuel_R_Short_LIST_ID				(66)
#define EEP_CONTENT_Fuel_R_Short_ITEM_LEN		(1)
#define EEP_CONTENT_Fuel_R_Short_IN_BLOCK_NUM 	(1)
#define EEP_CONTENT_Fuel_R_Short_ITEM_TYPE		EEP_ITEM_TYPE_16
#define EEP_CONTENT_Fuel_R_Short_ITEM_BYTE_LEN		2
#define EEP_CONTENT_Fuel_R_Short_CONST_VARIBLE	(1u)
#define EEP_CONTENT_PkbWarningJudgeV1_LIST_ID				(67)
#define EEP_CONTENT_PkbWarningJudgeV1_ITEM_LEN		(1)
#define EEP_CONTENT_PkbWarningJudgeV1_IN_BLOCK_NUM 	(1)
#define EEP_CONTENT_PkbWarningJudgeV1_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_PkbWarningJudgeV1_ITEM_BYTE_LEN		1
#define EEP_CONTENT_PkbWarningJudgeV1_CONST_VARIBLE	(1u)
#define EEP_CONTENT_PkbWarningJudgeV2_LIST_ID				(68)
#define EEP_CONTENT_PkbWarningJudgeV2_ITEM_LEN		(1)
#define EEP_CONTENT_PkbWarningJudgeV2_IN_BLOCK_NUM 	(1)
#define EEP_CONTENT_PkbWarningJudgeV2_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_PkbWarningJudgeV2_ITEM_BYTE_LEN		1
#define EEP_CONTENT_PkbWarningJudgeV2_CONST_VARIBLE	(1u)
#define EEP_CONTENT_AudioEepTest_LIST_ID				(69)
#define EEP_CONTENT_AudioEepTest_ITEM_LEN		(1)
#define EEP_CONTENT_AudioEepTest_IN_BLOCK_NUM 	(1)
#define EEP_CONTENT_AudioEepTest_ITEM_TYPE		EEP_ITEM_TYPE_16
#define EEP_CONTENT_AudioEepTest_ITEM_BYTE_LEN		2
#define EEP_CONTENT_AudioEepTest_CONST_VARIBLE	(0u)
#define EEP_CONTENT_DspKeyCodeOnOff_LIST_ID				(70)
#define EEP_CONTENT_DspKeyCodeOnOff_ITEM_LEN		(1)
#define EEP_CONTENT_DspKeyCodeOnOff_IN_BLOCK_NUM 	(1)
#define EEP_CONTENT_DspKeyCodeOnOff_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_DspKeyCodeOnOff_ITEM_BYTE_LEN		1
#define EEP_CONTENT_DspKeyCodeOnOff_CONST_VARIBLE	(0u)
#define EEP_CONTENT_DspKeyCode_LIST_ID				(71)
#define EEP_CONTENT_DspKeyCode_ITEM_LEN		(1)
#define EEP_CONTENT_DspKeyCode_IN_BLOCK_NUM 	(1)
#define EEP_CONTENT_DspKeyCode_ITEM_TYPE		EEP_ITEM_TYPE_32
#define EEP_CONTENT_DspKeyCode_ITEM_BYTE_LEN		4
#define EEP_CONTENT_DspKeyCode_CONST_VARIBLE	(0u)
#define EEP_CONTENT_BatState_ChgDly_LIST_ID				(72)
#define EEP_CONTENT_BatState_ChgDly_ITEM_LEN		(1)
#define EEP_CONTENT_BatState_ChgDly_IN_BLOCK_NUM 	(1)
#define EEP_CONTENT_BatState_ChgDly_ITEM_TYPE		EEP_ITEM_TYPE_16
#define EEP_CONTENT_BatState_ChgDly_ITEM_BYTE_LEN		2
#define EEP_CONTENT_BatState_ChgDly_CONST_VARIBLE	(1u)
#define EEP_CONTENT_BatVolVeryHigh_Hysteresis_H_LIST_ID				(73)
#define EEP_CONTENT_BatVolVeryHigh_Hysteresis_H_ITEM_LEN		(1)
#define EEP_CONTENT_BatVolVeryHigh_Hysteresis_H_IN_BLOCK_NUM 	(1)
#define EEP_CONTENT_BatVolVeryHigh_Hysteresis_H_ITEM_TYPE		EEP_ITEM_TYPE_16
#define EEP_CONTENT_BatVolVeryHigh_Hysteresis_H_ITEM_BYTE_LEN		2
#define EEP_CONTENT_BatVolVeryHigh_Hysteresis_H_CONST_VARIBLE	(1u)
#define EEP_CONTENT_BatVolVeryHigh_Hysteresis_L_LIST_ID				(74)
#define EEP_CONTENT_BatVolVeryHigh_Hysteresis_L_ITEM_LEN		(1)
#define EEP_CONTENT_BatVolVeryHigh_Hysteresis_L_IN_BLOCK_NUM 	(1)
#define EEP_CONTENT_BatVolVeryHigh_Hysteresis_L_ITEM_TYPE		EEP_ITEM_TYPE_16
#define EEP_CONTENT_BatVolVeryHigh_Hysteresis_L_ITEM_BYTE_LEN		2
#define EEP_CONTENT_BatVolVeryHigh_Hysteresis_L_CONST_VARIBLE	(1u)
#define EEP_CONTENT_BatVolHigh_Hysteresis_H_LIST_ID				(75)
#define EEP_CONTENT_BatVolHigh_Hysteresis_H_ITEM_LEN		(1)
#define EEP_CONTENT_BatVolHigh_Hysteresis_H_IN_BLOCK_NUM 	(1)
#define EEP_CONTENT_BatVolHigh_Hysteresis_H_ITEM_TYPE		EEP_ITEM_TYPE_16
#define EEP_CONTENT_BatVolHigh_Hysteresis_H_ITEM_BYTE_LEN		2
#define EEP_CONTENT_BatVolHigh_Hysteresis_H_CONST_VARIBLE	(1u)
#define EEP_CONTENT_BatVolHigh_Hysteresis_L_LIST_ID				(76)
#define EEP_CONTENT_BatVolHigh_Hysteresis_L_ITEM_LEN		(1)
#define EEP_CONTENT_BatVolHigh_Hysteresis_L_IN_BLOCK_NUM 	(1)
#define EEP_CONTENT_BatVolHigh_Hysteresis_L_ITEM_TYPE		EEP_ITEM_TYPE_16
#define EEP_CONTENT_BatVolHigh_Hysteresis_L_ITEM_BYTE_LEN		2
#define EEP_CONTENT_BatVolHigh_Hysteresis_L_CONST_VARIBLE	(1u)
#define EEP_CONTENT_BatVolLow_Hysteresis_H_LIST_ID				(77)
#define EEP_CONTENT_BatVolLow_Hysteresis_H_ITEM_LEN		(1)
#define EEP_CONTENT_BatVolLow_Hysteresis_H_IN_BLOCK_NUM 	(1)
#define EEP_CONTENT_BatVolLow_Hysteresis_H_ITEM_TYPE		EEP_ITEM_TYPE_16
#define EEP_CONTENT_BatVolLow_Hysteresis_H_ITEM_BYTE_LEN		2
#define EEP_CONTENT_BatVolLow_Hysteresis_H_CONST_VARIBLE	(1u)
#define EEP_CONTENT_BatVolLow_Hysteresis_L_LIST_ID				(78)
#define EEP_CONTENT_BatVolLow_Hysteresis_L_ITEM_LEN		(1)
#define EEP_CONTENT_BatVolLow_Hysteresis_L_IN_BLOCK_NUM 	(1)
#define EEP_CONTENT_BatVolLow_Hysteresis_L_ITEM_TYPE		EEP_ITEM_TYPE_16
#define EEP_CONTENT_BatVolLow_Hysteresis_L_ITEM_BYTE_LEN		2
#define EEP_CONTENT_BatVolLow_Hysteresis_L_CONST_VARIBLE	(1u)
#define EEP_CONTENT_BatVolVeryLow_Hysteresis_H_LIST_ID				(79)
#define EEP_CONTENT_BatVolVeryLow_Hysteresis_H_ITEM_LEN		(1)
#define EEP_CONTENT_BatVolVeryLow_Hysteresis_H_IN_BLOCK_NUM 	(1)
#define EEP_CONTENT_BatVolVeryLow_Hysteresis_H_ITEM_TYPE		EEP_ITEM_TYPE_16
#define EEP_CONTENT_BatVolVeryLow_Hysteresis_H_ITEM_BYTE_LEN		2
#define EEP_CONTENT_BatVolVeryLow_Hysteresis_H_CONST_VARIBLE	(1u)
#define EEP_CONTENT_BatVolVeryLow_Hysteresis_L_LIST_ID				(80)
#define EEP_CONTENT_BatVolVeryLow_Hysteresis_L_ITEM_LEN		(1)
#define EEP_CONTENT_BatVolVeryLow_Hysteresis_L_IN_BLOCK_NUM 	(1)
#define EEP_CONTENT_BatVolVeryLow_Hysteresis_L_ITEM_TYPE		EEP_ITEM_TYPE_16
#define EEP_CONTENT_BatVolVeryLow_Hysteresis_L_ITEM_BYTE_LEN		2
#define EEP_CONTENT_BatVolVeryLow_Hysteresis_L_CONST_VARIBLE	(1u)
#define EEP_CONTENT_TempState_ChgDly_LIST_ID				(81)
#define EEP_CONTENT_TempState_ChgDly_ITEM_LEN		(1)
#define EEP_CONTENT_TempState_ChgDly_IN_BLOCK_NUM 	(1)
#define EEP_CONTENT_TempState_ChgDly_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_TempState_ChgDly_ITEM_BYTE_LEN		1
#define EEP_CONTENT_TempState_ChgDly_CONST_VARIBLE	(0u)
#define EEP_CONTENT_TempDegC_Low_Hysteresis_L_LIST_ID				(82)
#define EEP_CONTENT_TempDegC_Low_Hysteresis_L_ITEM_LEN		(1)
#define EEP_CONTENT_TempDegC_Low_Hysteresis_L_IN_BLOCK_NUM 	(1)
#define EEP_CONTENT_TempDegC_Low_Hysteresis_L_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_TempDegC_Low_Hysteresis_L_ITEM_BYTE_LEN		1
#define EEP_CONTENT_TempDegC_Low_Hysteresis_L_CONST_VARIBLE	(0u)
#define EEP_CONTENT_TempDegC_Low_Hysteresis_H_LIST_ID				(83)
#define EEP_CONTENT_TempDegC_Low_Hysteresis_H_ITEM_LEN		(1)
#define EEP_CONTENT_TempDegC_Low_Hysteresis_H_IN_BLOCK_NUM 	(1)
#define EEP_CONTENT_TempDegC_Low_Hysteresis_H_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_TempDegC_Low_Hysteresis_H_ITEM_BYTE_LEN		1
#define EEP_CONTENT_TempDegC_Low_Hysteresis_H_CONST_VARIBLE	(0u)
#define EEP_CONTENT_TempDegC_High_Hysteresis_L_LIST_ID				(84)
#define EEP_CONTENT_TempDegC_High_Hysteresis_L_ITEM_LEN		(1)
#define EEP_CONTENT_TempDegC_High_Hysteresis_L_IN_BLOCK_NUM 	(1)
#define EEP_CONTENT_TempDegC_High_Hysteresis_L_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_TempDegC_High_Hysteresis_L_ITEM_BYTE_LEN		1
#define EEP_CONTENT_TempDegC_High_Hysteresis_L_CONST_VARIBLE	(0u)
#define EEP_CONTENT_TempDegC_High_Hysteresis_H_LIST_ID				(85)
#define EEP_CONTENT_TempDegC_High_Hysteresis_H_ITEM_LEN		(1)
#define EEP_CONTENT_TempDegC_High_Hysteresis_H_IN_BLOCK_NUM 	(1)
#define EEP_CONTENT_TempDegC_High_Hysteresis_H_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_TempDegC_High_Hysteresis_H_ITEM_BYTE_LEN		1
#define EEP_CONTENT_TempDegC_High_Hysteresis_H_CONST_VARIBLE	(0u)
#define EEP_CONTENT_AmpHighTempProction_LIST_ID				(86)
#define EEP_CONTENT_AmpHighTempProction_ITEM_LEN		(5)
#define EEP_CONTENT_AmpHighTempProction_IN_BLOCK_NUM 	(1)
#define EEP_CONTENT_AmpHighTempProction_ITEM_TYPE		EEP_ITEM_TYPE_32
#define EEP_CONTENT_AmpHighTempProction_ITEM_BYTE_LEN		20
#define EEP_CONTENT_AmpHighTempProction_CONST_VARIBLE	(0u)
#define EEP_CONTENT_CanNm_DA_S1_Delay_ms_LIST_ID				(87)
#define EEP_CONTENT_CanNm_DA_S1_Delay_ms_ITEM_LEN		(1)
#define EEP_CONTENT_CanNm_DA_S1_Delay_ms_IN_BLOCK_NUM 	(1)
#define EEP_CONTENT_CanNm_DA_S1_Delay_ms_ITEM_TYPE		EEP_ITEM_TYPE_16
#define EEP_CONTENT_CanNm_DA_S1_Delay_ms_ITEM_BYTE_LEN		2
#define EEP_CONTENT_CanNm_DA_S1_Delay_ms_CONST_VARIBLE	(0u)
#define EEP_CONTENT_Vin_LIST_ID				(88)
#define EEP_CONTENT_Vin_ITEM_LEN		(17)
#define EEP_CONTENT_Vin_IN_BLOCK_NUM 	(2)
#define EEP_CONTENT_Vin_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_Vin_ITEM_BYTE_LEN		17
#define EEP_CONTENT_Vin_CONST_VARIBLE	(0u)
#define EEP_CONTENT_AutoSyncTimeWithGps_LIST_ID				(89)
#define EEP_CONTENT_AutoSyncTimeWithGps_ITEM_LEN		(1)
#define EEP_CONTENT_AutoSyncTimeWithGps_IN_BLOCK_NUM 	(3)
#define EEP_CONTENT_AutoSyncTimeWithGps_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_AutoSyncTimeWithGps_ITEM_BYTE_LEN		1
#define EEP_CONTENT_AutoSyncTimeWithGps_CONST_VARIBLE	(0u)
#define EEP_CONTENT_ScreenBackLightValOnDay_LIST_ID				(90)
#define EEP_CONTENT_ScreenBackLightValOnDay_ITEM_LEN		(1)
#define EEP_CONTENT_ScreenBackLightValOnDay_IN_BLOCK_NUM 	(3)
#define EEP_CONTENT_ScreenBackLightValOnDay_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_ScreenBackLightValOnDay_ITEM_BYTE_LEN		1
#define EEP_CONTENT_ScreenBackLightValOnDay_CONST_VARIBLE	(0u)
#define EEP_CONTENT_ScreenBackLightValOnNight_LIST_ID				(91)
#define EEP_CONTENT_ScreenBackLightValOnNight_ITEM_LEN		(1)
#define EEP_CONTENT_ScreenBackLightValOnNight_IN_BLOCK_NUM 	(3)
#define EEP_CONTENT_ScreenBackLightValOnNight_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_ScreenBackLightValOnNight_ITEM_BYTE_LEN		1
#define EEP_CONTENT_ScreenBackLightValOnNight_CONST_VARIBLE	(0u)
#define EEP_CONTENT_HistoryAverFuelCons_LIST_ID				(92)
#define EEP_CONTENT_HistoryAverFuelCons_ITEM_LEN		(6)
#define EEP_CONTENT_HistoryAverFuelCons_IN_BLOCK_NUM 	(3)
#define EEP_CONTENT_HistoryAverFuelCons_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_HistoryAverFuelCons_ITEM_BYTE_LEN		6
#define EEP_CONTENT_HistoryAverFuelCons_CONST_VARIBLE	(0u)
#define EEP_CONTENT_FuelResistance_LIST_ID				(93)
#define EEP_CONTENT_FuelResistance_ITEM_LEN		(1)
#define EEP_CONTENT_FuelResistance_IN_BLOCK_NUM 	(3)
#define EEP_CONTENT_FuelResistance_ITEM_TYPE		EEP_ITEM_TYPE_32
#define EEP_CONTENT_FuelResistance_ITEM_BYTE_LEN		4
#define EEP_CONTENT_FuelResistance_CONST_VARIBLE	(0u)
#define EEP_CONTENT_dtc_example_LIST_ID				(94)
#define EEP_CONTENT_dtc_example_ITEM_LEN		(10)
#define EEP_CONTENT_dtc_example_IN_BLOCK_NUM 	(4)
#define EEP_CONTENT_dtc_example_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_dtc_example_ITEM_BYTE_LEN		10
#define EEP_CONTENT_dtc_example_CONST_VARIBLE	(0u)
#define EEP_CONTENT_TotalOdo_LIST_ID				(95)
#define EEP_CONTENT_TotalOdo_ITEM_LEN		(1)
#define EEP_CONTENT_TotalOdo_IN_BLOCK_NUM 	(5)
#define EEP_CONTENT_TotalOdo_ITEM_TYPE		EEP_ITEM_TYPE_32
#define EEP_CONTENT_TotalOdo_ITEM_BYTE_LEN		4
#define EEP_CONTENT_TotalOdo_CONST_VARIBLE	(0u)
#define EEP_CONTENT_TotalTime_LIST_ID				(96)
#define EEP_CONTENT_TotalTime_ITEM_LEN		(1)
#define EEP_CONTENT_TotalTime_IN_BLOCK_NUM 	(5)
#define EEP_CONTENT_TotalTime_ITEM_TYPE		EEP_ITEM_TYPE_32
#define EEP_CONTENT_TotalTime_ITEM_BYTE_LEN		4
#define EEP_CONTENT_TotalTime_CONST_VARIBLE	(0u)
#define EEP_CONTENT_TravelTime_LIST_ID				(97)
#define EEP_CONTENT_TravelTime_ITEM_LEN		(1)
#define EEP_CONTENT_TravelTime_IN_BLOCK_NUM 	(5)
#define EEP_CONTENT_TravelTime_ITEM_TYPE		EEP_ITEM_TYPE_32
#define EEP_CONTENT_TravelTime_ITEM_BYTE_LEN		4
#define EEP_CONTENT_TravelTime_CONST_VARIBLE	(0u)
#define EEP_CONTENT_TravelOdo_LIST_ID				(98)
#define EEP_CONTENT_TravelOdo_ITEM_LEN		(1)
#define EEP_CONTENT_TravelOdo_IN_BLOCK_NUM 	(5)
#define EEP_CONTENT_TravelOdo_ITEM_TYPE		EEP_ITEM_TYPE_32
#define EEP_CONTENT_TravelOdo_ITEM_BYTE_LEN		4
#define EEP_CONTENT_TravelOdo_CONST_VARIBLE	(0u)
#define EEP_CONTENT_TripAMeter_LIST_ID				(99)
#define EEP_CONTENT_TripAMeter_ITEM_LEN		(1)
#define EEP_CONTENT_TripAMeter_IN_BLOCK_NUM 	(5)
#define EEP_CONTENT_TripAMeter_ITEM_TYPE		EEP_ITEM_TYPE_32
#define EEP_CONTENT_TripAMeter_ITEM_BYTE_LEN		4
#define EEP_CONTENT_TripAMeter_CONST_VARIBLE	(0u)
#define EEP_CONTENT_TripATime_LIST_ID				(100)
#define EEP_CONTENT_TripATime_ITEM_LEN		(1)
#define EEP_CONTENT_TripATime_IN_BLOCK_NUM 	(5)
#define EEP_CONTENT_TripATime_ITEM_TYPE		EEP_ITEM_TYPE_32
#define EEP_CONTENT_TripATime_ITEM_BYTE_LEN		4
#define EEP_CONTENT_TripATime_CONST_VARIBLE	(0u)
#define EEP_CONTENT_TripBMeter_LIST_ID				(101)
#define EEP_CONTENT_TripBMeter_ITEM_LEN		(1)
#define EEP_CONTENT_TripBMeter_IN_BLOCK_NUM 	(5)
#define EEP_CONTENT_TripBMeter_ITEM_TYPE		EEP_ITEM_TYPE_32
#define EEP_CONTENT_TripBMeter_ITEM_BYTE_LEN		4
#define EEP_CONTENT_TripBMeter_CONST_VARIBLE	(0u)
#define EEP_CONTENT_TripBTime_LIST_ID				(102)
#define EEP_CONTENT_TripBTime_ITEM_LEN		(1)
#define EEP_CONTENT_TripBTime_IN_BLOCK_NUM 	(5)
#define EEP_CONTENT_TripBTime_ITEM_TYPE		EEP_ITEM_TYPE_32
#define EEP_CONTENT_TripBTime_ITEM_BYTE_LEN		4
#define EEP_CONTENT_TripBTime_CONST_VARIBLE	(0u)
#define EEP_CONTENT_CruiseDistance_LIST_ID				(103)
#define EEP_CONTENT_CruiseDistance_ITEM_LEN		(1)
#define EEP_CONTENT_CruiseDistance_IN_BLOCK_NUM 	(5)
#define EEP_CONTENT_CruiseDistance_ITEM_TYPE		EEP_ITEM_TYPE_32
#define EEP_CONTENT_CruiseDistance_ITEM_BYTE_LEN		4
#define EEP_CONTENT_CruiseDistance_CONST_VARIBLE	(0u)
#define EEP_CONTENT_VipSwdlShareMem_LIST_ID				(104)
#define EEP_CONTENT_VipSwdlShareMem_ITEM_LEN		(36)
#define EEP_CONTENT_VipSwdlShareMem_IN_BLOCK_NUM 	(6)
#define EEP_CONTENT_VipSwdlShareMem_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_VipSwdlShareMem_ITEM_BYTE_LEN		36
#define EEP_CONTENT_VipSwdlShareMem_CONST_VARIBLE	(0u)
#define EEP_CONTENT_TotalImageSFailCnt_LIST_ID				(105)
#define EEP_CONTENT_TotalImageSFailCnt_ITEM_LEN		(1)
#define EEP_CONTENT_TotalImageSFailCnt_IN_BLOCK_NUM 	(6)
#define EEP_CONTENT_TotalImageSFailCnt_ITEM_TYPE		EEP_ITEM_TYPE_32
#define EEP_CONTENT_TotalImageSFailCnt_ITEM_BYTE_LEN		4
#define EEP_CONTENT_TotalImageSFailCnt_CONST_VARIBLE	(0u)
#define EEP_CONTENT_trace_phy_en_LIST_ID				(106)
#define EEP_CONTENT_trace_phy_en_ITEM_LEN		(1)
#define EEP_CONTENT_trace_phy_en_IN_BLOCK_NUM 	(6)
#define EEP_CONTENT_trace_phy_en_ITEM_TYPE		EEP_ITEM_TYPE_16
#define EEP_CONTENT_trace_phy_en_ITEM_BYTE_LEN		2
#define EEP_CONTENT_trace_phy_en_CONST_VARIBLE	(1u)
#define EEP_CONTENT_trace_phy_LIST_ID				(107)
#define EEP_CONTENT_trace_phy_ITEM_LEN		(1)
#define EEP_CONTENT_trace_phy_IN_BLOCK_NUM 	(6)
#define EEP_CONTENT_trace_phy_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_trace_phy_ITEM_BYTE_LEN		1
#define EEP_CONTENT_trace_phy_CONST_VARIBLE	(1u)
#define EEP_CONTENT_VipLogModLvl_LIST_ID				(108)
#define EEP_CONTENT_VipLogModLvl_ITEM_LEN		(32)
#define EEP_CONTENT_VipLogModLvl_IN_BLOCK_NUM 	(6)
#define EEP_CONTENT_VipLogModLvl_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_VipLogModLvl_ITEM_BYTE_LEN		32
#define EEP_CONTENT_VipLogModLvl_CONST_VARIBLE	(0u)
#define EEP_CONTENT_TraceModuleLvlCrc_LIST_ID				(109)
#define EEP_CONTENT_TraceModuleLvlCrc_ITEM_LEN		(1)
#define EEP_CONTENT_TraceModuleLvlCrc_IN_BLOCK_NUM 	(6)
#define EEP_CONTENT_TraceModuleLvlCrc_ITEM_TYPE		EEP_ITEM_TYPE_16
#define EEP_CONTENT_TraceModuleLvlCrc_ITEM_BYTE_LEN		2
#define EEP_CONTENT_TraceModuleLvlCrc_CONST_VARIBLE	(0u)
#define EEP_CONTENT_TotalReset_Cnt_LIST_ID				(110)
#define EEP_CONTENT_TotalReset_Cnt_ITEM_LEN		(1)
#define EEP_CONTENT_TotalReset_Cnt_IN_BLOCK_NUM 	(7)
#define EEP_CONTENT_TotalReset_Cnt_ITEM_TYPE		EEP_ITEM_TYPE_32
#define EEP_CONTENT_TotalReset_Cnt_ITEM_BYTE_LEN		4
#define EEP_CONTENT_TotalReset_Cnt_CONST_VARIBLE	(0u)
#define EEP_CONTENT_DspPOR_cnt_LIST_ID				(111)
#define EEP_CONTENT_DspPOR_cnt_ITEM_LEN		(1)
#define EEP_CONTENT_DspPOR_cnt_IN_BLOCK_NUM 	(7)
#define EEP_CONTENT_DspPOR_cnt_ITEM_TYPE		EEP_ITEM_TYPE_16
#define EEP_CONTENT_DspPOR_cnt_ITEM_BYTE_LEN		2
#define EEP_CONTENT_DspPOR_cnt_CONST_VARIBLE	(0u)
#define EEP_CONTENT_AudioCmdExecMaxTm_LIST_ID				(112)
#define EEP_CONTENT_AudioCmdExecMaxTm_ITEM_LEN		(1)
#define EEP_CONTENT_AudioCmdExecMaxTm_IN_BLOCK_NUM 	(7)
#define EEP_CONTENT_AudioCmdExecMaxTm_ITEM_TYPE		EEP_ITEM_TYPE_16
#define EEP_CONTENT_AudioCmdExecMaxTm_ITEM_BYTE_LEN		2
#define EEP_CONTENT_AudioCmdExecMaxTm_CONST_VARIBLE	(0u)
#define EEP_CONTENT_RadioCmdExecMaxTm_LIST_ID				(113)
#define EEP_CONTENT_RadioCmdExecMaxTm_ITEM_LEN		(1)
#define EEP_CONTENT_RadioCmdExecMaxTm_IN_BLOCK_NUM 	(7)
#define EEP_CONTENT_RadioCmdExecMaxTm_ITEM_TYPE		EEP_ITEM_TYPE_16
#define EEP_CONTENT_RadioCmdExecMaxTm_ITEM_BYTE_LEN		2
#define EEP_CONTENT_RadioCmdExecMaxTm_CONST_VARIBLE	(0u)
#define EEP_CONTENT_CpuDisIntMaxNest_LIST_ID				(114)
#define EEP_CONTENT_CpuDisIntMaxNest_ITEM_LEN		(1)
#define EEP_CONTENT_CpuDisIntMaxNest_IN_BLOCK_NUM 	(7)
#define EEP_CONTENT_CpuDisIntMaxNest_ITEM_TYPE		EEP_ITEM_TYPE_16
#define EEP_CONTENT_CpuDisIntMaxNest_ITEM_BYTE_LEN		2
#define EEP_CONTENT_CpuDisIntMaxNest_CONST_VARIBLE	(0u)
#define EEP_CONTENT_CpuDisIntMaxTm_LIST_ID				(115)
#define EEP_CONTENT_CpuDisIntMaxTm_ITEM_LEN		(1)
#define EEP_CONTENT_CpuDisIntMaxTm_IN_BLOCK_NUM 	(7)
#define EEP_CONTENT_CpuDisIntMaxTm_ITEM_TYPE		EEP_ITEM_TYPE_16
#define EEP_CONTENT_CpuDisIntMaxTm_ITEM_BYTE_LEN		2
#define EEP_CONTENT_CpuDisIntMaxTm_CONST_VARIBLE	(0u)
#define EEP_CONTENT_CpuDisIntTooLongCnt_LIST_ID				(116)
#define EEP_CONTENT_CpuDisIntTooLongCnt_ITEM_LEN		(1)
#define EEP_CONTENT_CpuDisIntTooLongCnt_IN_BLOCK_NUM 	(7)
#define EEP_CONTENT_CpuDisIntTooLongCnt_ITEM_TYPE		EEP_ITEM_TYPE_16
#define EEP_CONTENT_CpuDisIntTooLongCnt_ITEM_BYTE_LEN		2
#define EEP_CONTENT_CpuDisIntTooLongCnt_CONST_VARIBLE	(0u)
#define EEP_CONTENT_SocResetReason_LIST_ID				(117)
#define EEP_CONTENT_SocResetReason_ITEM_LEN		(32)
#define EEP_CONTENT_SocResetReason_IN_BLOCK_NUM 	(7)
#define EEP_CONTENT_SocResetReason_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_SocResetReason_ITEM_BYTE_LEN		32
#define EEP_CONTENT_SocResetReason_CONST_VARIBLE	(0u)
#define	EEP_CONTENT_LIST_NUMBER_MAX	(118)




#define EEP_CONT_LIST_FLAG_ARRAY_NUM ((EEP_CONTENT_LIST_NUMBER_MAX+31)/32)

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/



typedef struct
{
	uint16_t id;
	uint16_t len;
	uint8_t *eep;
}EEP_ITEM_LIST_INFO;

typedef struct
{
	/* >>>>>>>>>>>>>>>>>> manufacture start >>>>>>>>>>>>>>>>>>>>>>>>>>> */
	uint32_t manufacture_write_count;
	uint8_t   Manuf_Visteon_Part_Number[17];
	uint8_t   Manuf_EquippedPCB_VPN_Sub[17];
	uint8_t   Manuf_EquippedPCB_VPN_Main[17];
	uint8_t   Manuf_Product_Serial_Number[5];
	uint8_t   Manuf_EquippedPCB_Serial_Number_Sub[5];
	uint8_t   Manuf_EquippedPCB_Serial_Number_Main[5];
	uint8_t   NVM_Revision;
	uint8_t   Manuf_SMD_Date1[3];
	uint8_t   Manuf_SMD_Date2[3];
	uint8_t   Manuf_Assembly_Date[3];
	uint8_t   Traceability_Data[9];
	uint8_t   VehicleManufacturerSparePartNumber[10];
	uint8_t   VehicleManufacturerSparePartNumber_Assembly[10];
	uint8_t   Operational_Reference[10];
	uint8_t   Supplier_Number[4];
	uint8_t   ECU_Serial_Number[20];
	uint8_t   Manufacturing_Identification_Code;
	uint8_t   VDIAG;
	uint8_t   Config_EQ1;
	uint8_t   Vehicle_Type;
	uint8_t   UUID[16];
	uint8_t   NAVI_ID[16];
	uint8_t   DA_ID[16];
	uint8_t   DAMainBoardHwVer[16];
	uint8_t   SW_Version_Date_Format[18];
	uint8_t   SW_Edition_Version[18];
	uint8_t   VehicleManufacturerSparePartNumber_Nissan[10];
	uint8_t   VisteonProductPartNumber[17];
	uint8_t   EquippedPCBVisteonPartNumMainBorad[17];
	uint8_t   EquippedPCBVisteonPartNumSubBorad[17];
	uint8_t   DAUniqueID[16];
	uint8_t   ProductSerialNum[5];
	uint8_t   ProductSerialNumMainBoard[5];
	uint8_t   ProductSerialNumSubBoard[5];
	uint8_t   SMDManufacturingDate[3];
	uint8_t   AssemblyManufacturingDate[3];
	uint8_t   TraceabilityBytes[3];
	uint8_t   SMDPlantNum[3];
	uint8_t   AssemblyPlantNum[3];
	uint8_t   DEM_EVENT_CAN_COM_DATA[10];
	uint8_t   DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA[10];
	uint8_t   DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA[10];
	uint8_t   DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA[10];
	uint8_t   DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA[10];
	uint8_t   DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA[10];
	uint8_t   DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA[10];
	uint8_t   DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA[10];
	uint8_t   DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA[10];
	uint8_t   DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA[10];
	uint8_t   DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA[10];
	uint8_t   DEM_EVENT_HVAC_PANEL_CONNECTION_DATA[10];
	/* eeprom manufacture id size = 474 */ 
	uint8_t manufacture_reserved_474[34];
	uint32_t manufacture_crc;	/* manufacture crc32 checksum */
	/*	eeprom manufacture total size = 512 */ 
	/* >>>>>>>>>>>>>>>>>> manufacture end >>>>>>>>>>>>>>>>>>>>>>>>>>> */

	/* >>>>>>>>>>>>>>>>>> development_nvm start >>>>>>>>>>>>>>>>>>>>>>>>>>> */
	uint32_t development_nvm_write_count;
	uint16_t   TachoUpValue[2];
	uint16_t   TachoLowValue[2];
	uint16_t   TachoLimitValue[2];
	uint16_t   TachoUDeltaLimit[2];
	uint16_t   TachoLDeltaLimit[2];
	uint16_t   TachoUpChangeValue[2];
	uint16_t   TachoLowChangeValue[2];
	uint16_t   TachoRun[2];
	uint16_t   Speed_Rm_1;
	uint16_t   Speed_Rm_2;
	uint16_t   Speed_Rc;
	uint16_t   FuelTelltaleOn_R;
	uint16_t   FuelTelltaleOff_R;
	uint16_t   FuelWarningOnPoint_R;
	uint16_t   Fuel_R_Open;
	uint16_t   Fuel_R_Short;
	uint8_t   PkbWarningJudgeV1;
	uint8_t   PkbWarningJudgeV2;
	uint16_t   AudioEepTest;
	uint8_t   DspKeyCodeOnOff;
	uint8_t reserved_57_DspKeyCode[3];
	uint32_t   DspKeyCode;
	uint16_t   BatState_ChgDly;
	uint16_t   BatVolVeryHigh_Hysteresis_H;
	uint16_t   BatVolVeryHigh_Hysteresis_L;
	uint16_t   BatVolHigh_Hysteresis_H;
	uint16_t   BatVolHigh_Hysteresis_L;
	uint16_t   BatVolLow_Hysteresis_H;
	uint16_t   BatVolLow_Hysteresis_L;
	uint16_t   BatVolVeryLow_Hysteresis_H;
	uint16_t   BatVolVeryLow_Hysteresis_L;
	uint8_t   TempState_ChgDly;
	uint8_t   TempDegC_Low_Hysteresis_L;
	uint8_t   TempDegC_Low_Hysteresis_H;
	uint8_t   TempDegC_High_Hysteresis_L;
	uint8_t   TempDegC_High_Hysteresis_H;
	uint8_t reserved_87_AmpHighTempProction[1];
	uint32_t   AmpHighTempProction[5];
	uint16_t   CanNm_DA_S1_Delay_ms;
	/* eeprom development_nvm id size = 110 */ 
	uint8_t development_nvm_reserved_110[910];
	uint32_t development_nvm_crc;	/* development_nvm crc32 checksum */
	/*	eeprom development_nvm total size = 1024 */ 
	/* >>>>>>>>>>>>>>>>>> development_nvm end >>>>>>>>>>>>>>>>>>>>>>>>>>> */

	/* >>>>>>>>>>>>>>>>>> OemSetting start >>>>>>>>>>>>>>>>>>>>>>>>>>> */
	uint32_t OemSetting_write_count;
	uint8_t   Vin[17];
	/* eeprom OemSetting id size = 21 */ 
	uint8_t OemSetting_reserved_21[231];
	uint32_t OemSetting_crc;	/* OemSetting crc32 checksum */
	/*	eeprom OemSetting total size = 256 */ 
	/* >>>>>>>>>>>>>>>>>> OemSetting end >>>>>>>>>>>>>>>>>>>>>>>>>>> */

	/* >>>>>>>>>>>>>>>>>> user_setting start >>>>>>>>>>>>>>>>>>>>>>>>>>> */
	uint32_t user_setting_write_count;
	uint8_t   AutoSyncTimeWithGps;
	uint8_t   ScreenBackLightValOnDay;
	uint8_t   ScreenBackLightValOnNight;
	uint8_t   HistoryAverFuelCons[6];
	uint8_t reserved_13_FuelResistance[3];
	uint32_t   FuelResistance;
	/* eeprom user_setting id size = 20 */ 
	uint8_t user_setting_reserved_20[232];
	uint32_t user_setting_crc;	/* user_setting crc32 checksum */
	/*	eeprom user_setting total size = 256 */ 
	/* >>>>>>>>>>>>>>>>>> user_setting end >>>>>>>>>>>>>>>>>>>>>>>>>>> */

	/* >>>>>>>>>>>>>>>>>> DTC_INFO start >>>>>>>>>>>>>>>>>>>>>>>>>>> */
	uint32_t DTC_INFO_write_count;
	uint8_t   dtc_example[10];
	/* eeprom DTC_INFO id size = 14 */ 
	uint8_t DTC_INFO_reserved_14[238];
	uint32_t DTC_INFO_crc;	/* DTC_INFO crc32 checksum */
	/*	eeprom DTC_INFO total size = 256 */ 
	/* >>>>>>>>>>>>>>>>>> DTC_INFO end >>>>>>>>>>>>>>>>>>>>>>>>>>> */

	/* >>>>>>>>>>>>>>>>>> OdoInfo start >>>>>>>>>>>>>>>>>>>>>>>>>>> */
	uint32_t OdoInfo_write_count;
	uint32_t   TotalOdo;
	uint32_t   TotalTime;
	uint32_t   TravelTime;
	uint32_t   TravelOdo;
	uint32_t   TripAMeter;
	uint32_t   TripATime;
	uint32_t   TripBMeter;
	uint32_t   TripBTime;
	uint32_t   CruiseDistance;
	/* eeprom OdoInfo id size = 40 */ 
	uint8_t OdoInfo_reserved_40[20];
	uint32_t OdoInfo_crc;	/* OdoInfo crc32 checksum */
	/*	eeprom OdoInfo total size = 64 */ 
	/* >>>>>>>>>>>>>>>>>> OdoInfo end >>>>>>>>>>>>>>>>>>>>>>>>>>> */

	/* >>>>>>>>>>>>>>>>>> share_eep start >>>>>>>>>>>>>>>>>>>>>>>>>>> */
	uint32_t share_eep_write_count;
	uint8_t   VipSwdlShareMem[36];
	uint32_t   TotalImageSFailCnt;
	uint16_t   trace_phy_en;
	uint8_t   trace_phy;
	uint8_t   VipLogModLvl[32];
	uint8_t reserved_79_TraceModuleLvlCrc;
	uint16_t   TraceModuleLvlCrc;
	/* eeprom share_eep id size = 82 */ 
	uint8_t share_eep_reserved_82[42];
	uint32_t share_eep_crc;	/* share_eep crc32 checksum */
	/*	eeprom share_eep total size = 128 */ 
	/* >>>>>>>>>>>>>>>>>> share_eep end >>>>>>>>>>>>>>>>>>>>>>>>>>> */

	/* >>>>>>>>>>>>>>>>>> ResetInfo start >>>>>>>>>>>>>>>>>>>>>>>>>>> */
	uint32_t ResetInfo_write_count;
	uint32_t   TotalReset_Cnt;
	uint16_t   DspPOR_cnt;
	uint16_t   AudioCmdExecMaxTm;
	uint16_t   RadioCmdExecMaxTm;
	uint16_t   CpuDisIntMaxNest;
	uint16_t   CpuDisIntMaxTm;
	uint16_t   CpuDisIntTooLongCnt;
	uint8_t   SocResetReason[32];
	/* eeprom ResetInfo id size = 52 */ 
	uint8_t ResetInfo_reserved_52[72];
	uint32_t ResetInfo_crc;	/* ResetInfo crc32 checksum */
	/*	eeprom ResetInfo total size = 128 */ 
	/* >>>>>>>>>>>>>>>>>> ResetInfo end >>>>>>>>>>>>>>>>>>>>>>>>>>> */

}EEPROM_ST;

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
//extern uint16_t eep_write_block_req;
//extern uint32_t wr_eep_by_list_req[EEP_CONT_LIST_FLAG_ARRAY_NUM];
//extern uint32_t tx_eep_by_list_to_imx_req[EEP_CONT_LIST_FLAG_ARRAY_NUM];
//extern uint32_t tx_eep_by_list_to_imx_cur[EEP_CONT_LIST_FLAG_ARRAY_NUM];
// eeprom ram image 
//extern EEPROM_ST eep_ram_image;
// write eep_ram_image->eep_hw_image, than eep_hw_image->real eeprom 
//extern EEPROM_ST eep_hw_image; 

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
uint32_t GenEep_Get_manufacture_write_count(void);
#if EEP_CONTENT_Manuf_Visteon_Part_Number_ITEM_LEN > 1
void GenEep_Get_Manuf_Visteon_Part_Number(uint8_t * const Manuf_Visteon_Part_Number );
void GenEep_Get_Manuf_Visteon_Part_Number_Ex(uint8_t * const Manuf_Visteon_Part_Number,uint16_t const offset,uint16_t const len );
void GenEep_Set_Manuf_Visteon_Part_Number(uint8_t const* Manuf_Visteon_Part_Number );
void GenEep_Set_Manuf_Visteon_Part_Number_Ex(uint8_t const* Manuf_Visteon_Part_Number,uint16_t const offset,uint16_t const len );
#else 
uint8_t GenEep_Get_Manuf_Visteon_Part_Number(void);
void GenEep_Set_Manuf_Visteon_Part_Number(uint8_t const Manuf_Visteon_Part_Number);
#endif
#if EEP_CONTENT_Manuf_EquippedPCB_VPN_Sub_ITEM_LEN > 1
void GenEep_Get_Manuf_EquippedPCB_VPN_Sub(uint8_t * const Manuf_EquippedPCB_VPN_Sub );
void GenEep_Get_Manuf_EquippedPCB_VPN_Sub_Ex(uint8_t * const Manuf_EquippedPCB_VPN_Sub,uint16_t const offset,uint16_t const len );
void GenEep_Set_Manuf_EquippedPCB_VPN_Sub(uint8_t const* Manuf_EquippedPCB_VPN_Sub );
void GenEep_Set_Manuf_EquippedPCB_VPN_Sub_Ex(uint8_t const* Manuf_EquippedPCB_VPN_Sub,uint16_t const offset,uint16_t const len );
#else 
uint8_t GenEep_Get_Manuf_EquippedPCB_VPN_Sub(void);
void GenEep_Set_Manuf_EquippedPCB_VPN_Sub(uint8_t const Manuf_EquippedPCB_VPN_Sub);
#endif
#if EEP_CONTENT_Manuf_EquippedPCB_VPN_Main_ITEM_LEN > 1
void GenEep_Get_Manuf_EquippedPCB_VPN_Main(uint8_t * const Manuf_EquippedPCB_VPN_Main );
void GenEep_Get_Manuf_EquippedPCB_VPN_Main_Ex(uint8_t * const Manuf_EquippedPCB_VPN_Main,uint16_t const offset,uint16_t const len );
void GenEep_Set_Manuf_EquippedPCB_VPN_Main(uint8_t const* Manuf_EquippedPCB_VPN_Main );
void GenEep_Set_Manuf_EquippedPCB_VPN_Main_Ex(uint8_t const* Manuf_EquippedPCB_VPN_Main,uint16_t const offset,uint16_t const len );
#else 
uint8_t GenEep_Get_Manuf_EquippedPCB_VPN_Main(void);
void GenEep_Set_Manuf_EquippedPCB_VPN_Main(uint8_t const Manuf_EquippedPCB_VPN_Main);
#endif
#if EEP_CONTENT_Manuf_Product_Serial_Number_ITEM_LEN > 1
void GenEep_Get_Manuf_Product_Serial_Number(uint8_t * const Manuf_Product_Serial_Number );
void GenEep_Get_Manuf_Product_Serial_Number_Ex(uint8_t * const Manuf_Product_Serial_Number,uint16_t const offset,uint16_t const len );
void GenEep_Set_Manuf_Product_Serial_Number(uint8_t const* Manuf_Product_Serial_Number );
void GenEep_Set_Manuf_Product_Serial_Number_Ex(uint8_t const* Manuf_Product_Serial_Number,uint16_t const offset,uint16_t const len );
#else 
uint8_t GenEep_Get_Manuf_Product_Serial_Number(void);
void GenEep_Set_Manuf_Product_Serial_Number(uint8_t const Manuf_Product_Serial_Number);
#endif
#if EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Sub_ITEM_LEN > 1
void GenEep_Get_Manuf_EquippedPCB_Serial_Number_Sub(uint8_t * const Manuf_EquippedPCB_Serial_Number_Sub );
void GenEep_Get_Manuf_EquippedPCB_Serial_Number_Sub_Ex(uint8_t * const Manuf_EquippedPCB_Serial_Number_Sub,uint16_t const offset,uint16_t const len );
void GenEep_Set_Manuf_EquippedPCB_Serial_Number_Sub(uint8_t const* Manuf_EquippedPCB_Serial_Number_Sub );
void GenEep_Set_Manuf_EquippedPCB_Serial_Number_Sub_Ex(uint8_t const* Manuf_EquippedPCB_Serial_Number_Sub,uint16_t const offset,uint16_t const len );
#else 
uint8_t GenEep_Get_Manuf_EquippedPCB_Serial_Number_Sub(void);
void GenEep_Set_Manuf_EquippedPCB_Serial_Number_Sub(uint8_t const Manuf_EquippedPCB_Serial_Number_Sub);
#endif
#if EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Main_ITEM_LEN > 1
void GenEep_Get_Manuf_EquippedPCB_Serial_Number_Main(uint8_t * const Manuf_EquippedPCB_Serial_Number_Main );
void GenEep_Get_Manuf_EquippedPCB_Serial_Number_Main_Ex(uint8_t * const Manuf_EquippedPCB_Serial_Number_Main,uint16_t const offset,uint16_t const len );
void GenEep_Set_Manuf_EquippedPCB_Serial_Number_Main(uint8_t const* Manuf_EquippedPCB_Serial_Number_Main );
void GenEep_Set_Manuf_EquippedPCB_Serial_Number_Main_Ex(uint8_t const* Manuf_EquippedPCB_Serial_Number_Main,uint16_t const offset,uint16_t const len );
#else 
uint8_t GenEep_Get_Manuf_EquippedPCB_Serial_Number_Main(void);
void GenEep_Set_Manuf_EquippedPCB_Serial_Number_Main(uint8_t const Manuf_EquippedPCB_Serial_Number_Main);
#endif
#if EEP_CONTENT_NVM_Revision_ITEM_LEN > 1
void GenEep_Get_NVM_Revision(uint8_t * const NVM_Revision );
void GenEep_Get_NVM_Revision_Ex(uint8_t * const NVM_Revision,uint16_t const offset,uint16_t const len );
void GenEep_Set_NVM_Revision(uint8_t const* NVM_Revision );
void GenEep_Set_NVM_Revision_Ex(uint8_t const* NVM_Revision,uint16_t const offset,uint16_t const len );
#else 
uint8_t GenEep_Get_NVM_Revision(void);
void GenEep_Set_NVM_Revision(uint8_t const NVM_Revision);
#endif
#if EEP_CONTENT_Manuf_SMD_Date1_ITEM_LEN > 1
void GenEep_Get_Manuf_SMD_Date1(uint8_t * const Manuf_SMD_Date1 );
void GenEep_Get_Manuf_SMD_Date1_Ex(uint8_t * const Manuf_SMD_Date1,uint16_t const offset,uint16_t const len );
void GenEep_Set_Manuf_SMD_Date1(uint8_t const* Manuf_SMD_Date1 );
void GenEep_Set_Manuf_SMD_Date1_Ex(uint8_t const* Manuf_SMD_Date1,uint16_t const offset,uint16_t const len );
#else 
uint8_t GenEep_Get_Manuf_SMD_Date1(void);
void GenEep_Set_Manuf_SMD_Date1(uint8_t const Manuf_SMD_Date1);
#endif
#if EEP_CONTENT_Manuf_SMD_Date2_ITEM_LEN > 1
void GenEep_Get_Manuf_SMD_Date2(uint8_t * const Manuf_SMD_Date2 );
void GenEep_Get_Manuf_SMD_Date2_Ex(uint8_t * const Manuf_SMD_Date2,uint16_t const offset,uint16_t const len );
void GenEep_Set_Manuf_SMD_Date2(uint8_t const* Manuf_SMD_Date2 );
void GenEep_Set_Manuf_SMD_Date2_Ex(uint8_t const* Manuf_SMD_Date2,uint16_t const offset,uint16_t const len );
#else 
uint8_t GenEep_Get_Manuf_SMD_Date2(void);
void GenEep_Set_Manuf_SMD_Date2(uint8_t const Manuf_SMD_Date2);
#endif
#if EEP_CONTENT_Manuf_Assembly_Date_ITEM_LEN > 1
void GenEep_Get_Manuf_Assembly_Date(uint8_t * const Manuf_Assembly_Date );
void GenEep_Get_Manuf_Assembly_Date_Ex(uint8_t * const Manuf_Assembly_Date,uint16_t const offset,uint16_t const len );
void GenEep_Set_Manuf_Assembly_Date(uint8_t const* Manuf_Assembly_Date );
void GenEep_Set_Manuf_Assembly_Date_Ex(uint8_t const* Manuf_Assembly_Date,uint16_t const offset,uint16_t const len );
#else 
uint8_t GenEep_Get_Manuf_Assembly_Date(void);
void GenEep_Set_Manuf_Assembly_Date(uint8_t const Manuf_Assembly_Date);
#endif
#if EEP_CONTENT_Traceability_Data_ITEM_LEN > 1
void GenEep_Get_Traceability_Data(uint8_t * const Traceability_Data );
void GenEep_Get_Traceability_Data_Ex(uint8_t * const Traceability_Data,uint16_t const offset,uint16_t const len );
void GenEep_Set_Traceability_Data(uint8_t const* Traceability_Data );
void GenEep_Set_Traceability_Data_Ex(uint8_t const* Traceability_Data,uint16_t const offset,uint16_t const len );
#else 
uint8_t GenEep_Get_Traceability_Data(void);
void GenEep_Set_Traceability_Data(uint8_t const Traceability_Data);
#endif
#if EEP_CONTENT_VehicleManufacturerSparePartNumber_ITEM_LEN > 1
void GenEep_Get_VehicleManufacturerSparePartNumber(uint8_t * const VehicleManufacturerSparePartNumber );
void GenEep_Get_VehicleManufacturerSparePartNumber_Ex(uint8_t * const VehicleManufacturerSparePartNumber,uint16_t const offset,uint16_t const len );
void GenEep_Set_VehicleManufacturerSparePartNumber(uint8_t const* VehicleManufacturerSparePartNumber );
void GenEep_Set_VehicleManufacturerSparePartNumber_Ex(uint8_t const* VehicleManufacturerSparePartNumber,uint16_t const offset,uint16_t const len );
#else 
uint8_t GenEep_Get_VehicleManufacturerSparePartNumber(void);
void GenEep_Set_VehicleManufacturerSparePartNumber(uint8_t const VehicleManufacturerSparePartNumber);
#endif
#if EEP_CONTENT_VehicleManufacturerSparePartNumber_Assembly_ITEM_LEN > 1
void GenEep_Get_VehicleManufacturerSparePartNumber_Assembly(uint8_t * const VehicleManufacturerSparePartNumber_Assembly );
void GenEep_Get_VehicleManufacturerSparePartNumber_Assembly_Ex(uint8_t * const VehicleManufacturerSparePartNumber_Assembly,uint16_t const offset,uint16_t const len );
void GenEep_Set_VehicleManufacturerSparePartNumber_Assembly(uint8_t const* VehicleManufacturerSparePartNumber_Assembly );
void GenEep_Set_VehicleManufacturerSparePartNumber_Assembly_Ex(uint8_t const* VehicleManufacturerSparePartNumber_Assembly,uint16_t const offset,uint16_t const len );
#else 
uint8_t GenEep_Get_VehicleManufacturerSparePartNumber_Assembly(void);
void GenEep_Set_VehicleManufacturerSparePartNumber_Assembly(uint8_t const VehicleManufacturerSparePartNumber_Assembly);
#endif
#if EEP_CONTENT_Operational_Reference_ITEM_LEN > 1
void GenEep_Get_Operational_Reference(uint8_t * const Operational_Reference );
void GenEep_Get_Operational_Reference_Ex(uint8_t * const Operational_Reference,uint16_t const offset,uint16_t const len );
void GenEep_Set_Operational_Reference(uint8_t const* Operational_Reference );
void GenEep_Set_Operational_Reference_Ex(uint8_t const* Operational_Reference,uint16_t const offset,uint16_t const len );
#else 
uint8_t GenEep_Get_Operational_Reference(void);
void GenEep_Set_Operational_Reference(uint8_t const Operational_Reference);
#endif
#if EEP_CONTENT_Supplier_Number_ITEM_LEN > 1
void GenEep_Get_Supplier_Number(uint8_t * const Supplier_Number );
void GenEep_Get_Supplier_Number_Ex(uint8_t * const Supplier_Number,uint16_t const offset,uint16_t const len );
void GenEep_Set_Supplier_Number(uint8_t const* Supplier_Number );
void GenEep_Set_Supplier_Number_Ex(uint8_t const* Supplier_Number,uint16_t const offset,uint16_t const len );
#else 
uint8_t GenEep_Get_Supplier_Number(void);
void GenEep_Set_Supplier_Number(uint8_t const Supplier_Number);
#endif
#if EEP_CONTENT_ECU_Serial_Number_ITEM_LEN > 1
void GenEep_Get_ECU_Serial_Number(uint8_t * const ECU_Serial_Number );
void GenEep_Get_ECU_Serial_Number_Ex(uint8_t * const ECU_Serial_Number,uint16_t const offset,uint16_t const len );
void GenEep_Set_ECU_Serial_Number(uint8_t const* ECU_Serial_Number );
void GenEep_Set_ECU_Serial_Number_Ex(uint8_t const* ECU_Serial_Number,uint16_t const offset,uint16_t const len );
#else 
uint8_t GenEep_Get_ECU_Serial_Number(void);
void GenEep_Set_ECU_Serial_Number(uint8_t const ECU_Serial_Number);
#endif
#if EEP_CONTENT_Manufacturing_Identification_Code_ITEM_LEN > 1
void GenEep_Get_Manufacturing_Identification_Code(uint8_t * const Manufacturing_Identification_Code );
void GenEep_Get_Manufacturing_Identification_Code_Ex(uint8_t * const Manufacturing_Identification_Code,uint16_t const offset,uint16_t const len );
void GenEep_Set_Manufacturing_Identification_Code(uint8_t const* Manufacturing_Identification_Code );
void GenEep_Set_Manufacturing_Identification_Code_Ex(uint8_t const* Manufacturing_Identification_Code,uint16_t const offset,uint16_t const len );
#else 
uint8_t GenEep_Get_Manufacturing_Identification_Code(void);
void GenEep_Set_Manufacturing_Identification_Code(uint8_t const Manufacturing_Identification_Code);
#endif
#if EEP_CONTENT_VDIAG_ITEM_LEN > 1
void GenEep_Get_VDIAG(uint8_t * const VDIAG );
void GenEep_Get_VDIAG_Ex(uint8_t * const VDIAG,uint16_t const offset,uint16_t const len );
void GenEep_Set_VDIAG(uint8_t const* VDIAG );
void GenEep_Set_VDIAG_Ex(uint8_t const* VDIAG,uint16_t const offset,uint16_t const len );
#else 
uint8_t GenEep_Get_VDIAG(void);
void GenEep_Set_VDIAG(uint8_t const VDIAG);
#endif
#if EEP_CONTENT_Config_EQ1_ITEM_LEN > 1
void GenEep_Get_Config_EQ1(uint8_t * const Config_EQ1 );
void GenEep_Get_Config_EQ1_Ex(uint8_t * const Config_EQ1,uint16_t const offset,uint16_t const len );
void GenEep_Set_Config_EQ1(uint8_t const* Config_EQ1 );
void GenEep_Set_Config_EQ1_Ex(uint8_t const* Config_EQ1,uint16_t const offset,uint16_t const len );
#else 
uint8_t GenEep_Get_Config_EQ1(void);
void GenEep_Set_Config_EQ1(uint8_t const Config_EQ1);
#endif
#if EEP_CONTENT_Vehicle_Type_ITEM_LEN > 1
void GenEep_Get_Vehicle_Type(uint8_t * const Vehicle_Type );
void GenEep_Get_Vehicle_Type_Ex(uint8_t * const Vehicle_Type,uint16_t const offset,uint16_t const len );
void GenEep_Set_Vehicle_Type(uint8_t const* Vehicle_Type );
void GenEep_Set_Vehicle_Type_Ex(uint8_t const* Vehicle_Type,uint16_t const offset,uint16_t const len );
#else 
uint8_t GenEep_Get_Vehicle_Type(void);
void GenEep_Set_Vehicle_Type(uint8_t const Vehicle_Type);
#endif
#if EEP_CONTENT_UUID_ITEM_LEN > 1
void GenEep_Get_UUID(uint8_t * const UUID );
void GenEep_Get_UUID_Ex(uint8_t * const UUID,uint16_t const offset,uint16_t const len );
void GenEep_Set_UUID(uint8_t const* UUID );
void GenEep_Set_UUID_Ex(uint8_t const* UUID,uint16_t const offset,uint16_t const len );
#else 
uint8_t GenEep_Get_UUID(void);
void GenEep_Set_UUID(uint8_t const UUID);
#endif
#if EEP_CONTENT_NAVI_ID_ITEM_LEN > 1
void GenEep_Get_NAVI_ID(uint8_t * const NAVI_ID );
void GenEep_Get_NAVI_ID_Ex(uint8_t * const NAVI_ID,uint16_t const offset,uint16_t const len );
void GenEep_Set_NAVI_ID(uint8_t const* NAVI_ID );
void GenEep_Set_NAVI_ID_Ex(uint8_t const* NAVI_ID,uint16_t const offset,uint16_t const len );
#else 
uint8_t GenEep_Get_NAVI_ID(void);
void GenEep_Set_NAVI_ID(uint8_t const NAVI_ID);
#endif
#if EEP_CONTENT_DA_ID_ITEM_LEN > 1
void GenEep_Get_DA_ID(uint8_t * const DA_ID );
void GenEep_Get_DA_ID_Ex(uint8_t * const DA_ID,uint16_t const offset,uint16_t const len );
void GenEep_Set_DA_ID(uint8_t const* DA_ID );
void GenEep_Set_DA_ID_Ex(uint8_t const* DA_ID,uint16_t const offset,uint16_t const len );
#else 
uint8_t GenEep_Get_DA_ID(void);
void GenEep_Set_DA_ID(uint8_t const DA_ID);
#endif
#if EEP_CONTENT_DAMainBoardHwVer_ITEM_LEN > 1
void GenEep_Get_DAMainBoardHwVer(uint8_t * const DAMainBoardHwVer );
void GenEep_Get_DAMainBoardHwVer_Ex(uint8_t * const DAMainBoardHwVer,uint16_t const offset,uint16_t const len );
void GenEep_Set_DAMainBoardHwVer(uint8_t const* DAMainBoardHwVer );
void GenEep_Set_DAMainBoardHwVer_Ex(uint8_t const* DAMainBoardHwVer,uint16_t const offset,uint16_t const len );
#else 
uint8_t GenEep_Get_DAMainBoardHwVer(void);
void GenEep_Set_DAMainBoardHwVer(uint8_t const DAMainBoardHwVer);
#endif
#if EEP_CONTENT_SW_Version_Date_Format_ITEM_LEN > 1
void GenEep_Get_SW_Version_Date_Format(uint8_t * const SW_Version_Date_Format );
void GenEep_Get_SW_Version_Date_Format_Ex(uint8_t * const SW_Version_Date_Format,uint16_t const offset,uint16_t const len );
void GenEep_Set_SW_Version_Date_Format(uint8_t const* SW_Version_Date_Format );
void GenEep_Set_SW_Version_Date_Format_Ex(uint8_t const* SW_Version_Date_Format,uint16_t const offset,uint16_t const len );
#else 
uint8_t GenEep_Get_SW_Version_Date_Format(void);
void GenEep_Set_SW_Version_Date_Format(uint8_t const SW_Version_Date_Format);
#endif
#if EEP_CONTENT_SW_Edition_Version_ITEM_LEN > 1
void GenEep_Get_SW_Edition_Version(uint8_t * const SW_Edition_Version );
void GenEep_Get_SW_Edition_Version_Ex(uint8_t * const SW_Edition_Version,uint16_t const offset,uint16_t const len );
void GenEep_Set_SW_Edition_Version(uint8_t const* SW_Edition_Version );
void GenEep_Set_SW_Edition_Version_Ex(uint8_t const* SW_Edition_Version,uint16_t const offset,uint16_t const len );
#else 
uint8_t GenEep_Get_SW_Edition_Version(void);
void GenEep_Set_SW_Edition_Version(uint8_t const SW_Edition_Version);
#endif
#if EEP_CONTENT_VehicleManufacturerSparePartNumber_Nissan_ITEM_LEN > 1
void GenEep_Get_VehicleManufacturerSparePartNumber_Nissan(uint8_t * const VehicleManufacturerSparePartNumber_Nissan );
void GenEep_Get_VehicleManufacturerSparePartNumber_Nissan_Ex(uint8_t * const VehicleManufacturerSparePartNumber_Nissan,uint16_t const offset,uint16_t const len );
void GenEep_Set_VehicleManufacturerSparePartNumber_Nissan(uint8_t const* VehicleManufacturerSparePartNumber_Nissan );
void GenEep_Set_VehicleManufacturerSparePartNumber_Nissan_Ex(uint8_t const* VehicleManufacturerSparePartNumber_Nissan,uint16_t const offset,uint16_t const len );
#else 
uint8_t GenEep_Get_VehicleManufacturerSparePartNumber_Nissan(void);
void GenEep_Set_VehicleManufacturerSparePartNumber_Nissan(uint8_t const VehicleManufacturerSparePartNumber_Nissan);
#endif
#if EEP_CONTENT_VisteonProductPartNumber_ITEM_LEN > 1
void GenEep_Get_VisteonProductPartNumber(uint8_t * const VisteonProductPartNumber );
void GenEep_Get_VisteonProductPartNumber_Ex(uint8_t * const VisteonProductPartNumber,uint16_t const offset,uint16_t const len );
void GenEep_Set_VisteonProductPartNumber(uint8_t const* VisteonProductPartNumber );
void GenEep_Set_VisteonProductPartNumber_Ex(uint8_t const* VisteonProductPartNumber,uint16_t const offset,uint16_t const len );
#else 
uint8_t GenEep_Get_VisteonProductPartNumber(void);
void GenEep_Set_VisteonProductPartNumber(uint8_t const VisteonProductPartNumber);
#endif
#if EEP_CONTENT_EquippedPCBVisteonPartNumMainBorad_ITEM_LEN > 1
void GenEep_Get_EquippedPCBVisteonPartNumMainBorad(uint8_t * const EquippedPCBVisteonPartNumMainBorad );
void GenEep_Get_EquippedPCBVisteonPartNumMainBorad_Ex(uint8_t * const EquippedPCBVisteonPartNumMainBorad,uint16_t const offset,uint16_t const len );
void GenEep_Set_EquippedPCBVisteonPartNumMainBorad(uint8_t const* EquippedPCBVisteonPartNumMainBorad );
void GenEep_Set_EquippedPCBVisteonPartNumMainBorad_Ex(uint8_t const* EquippedPCBVisteonPartNumMainBorad,uint16_t const offset,uint16_t const len );
#else 
uint8_t GenEep_Get_EquippedPCBVisteonPartNumMainBorad(void);
void GenEep_Set_EquippedPCBVisteonPartNumMainBorad(uint8_t const EquippedPCBVisteonPartNumMainBorad);
#endif
#if EEP_CONTENT_EquippedPCBVisteonPartNumSubBorad_ITEM_LEN > 1
void GenEep_Get_EquippedPCBVisteonPartNumSubBorad(uint8_t * const EquippedPCBVisteonPartNumSubBorad );
void GenEep_Get_EquippedPCBVisteonPartNumSubBorad_Ex(uint8_t * const EquippedPCBVisteonPartNumSubBorad,uint16_t const offset,uint16_t const len );
void GenEep_Set_EquippedPCBVisteonPartNumSubBorad(uint8_t const* EquippedPCBVisteonPartNumSubBorad );
void GenEep_Set_EquippedPCBVisteonPartNumSubBorad_Ex(uint8_t const* EquippedPCBVisteonPartNumSubBorad,uint16_t const offset,uint16_t const len );
#else 
uint8_t GenEep_Get_EquippedPCBVisteonPartNumSubBorad(void);
void GenEep_Set_EquippedPCBVisteonPartNumSubBorad(uint8_t const EquippedPCBVisteonPartNumSubBorad);
#endif
#if EEP_CONTENT_DAUniqueID_ITEM_LEN > 1
void GenEep_Get_DAUniqueID(uint8_t * const DAUniqueID );
void GenEep_Get_DAUniqueID_Ex(uint8_t * const DAUniqueID,uint16_t const offset,uint16_t const len );
void GenEep_Set_DAUniqueID(uint8_t const* DAUniqueID );
void GenEep_Set_DAUniqueID_Ex(uint8_t const* DAUniqueID,uint16_t const offset,uint16_t const len );
#else 
uint8_t GenEep_Get_DAUniqueID(void);
void GenEep_Set_DAUniqueID(uint8_t const DAUniqueID);
#endif
#if EEP_CONTENT_ProductSerialNum_ITEM_LEN > 1
void GenEep_Get_ProductSerialNum(uint8_t * const ProductSerialNum );
void GenEep_Get_ProductSerialNum_Ex(uint8_t * const ProductSerialNum,uint16_t const offset,uint16_t const len );
void GenEep_Set_ProductSerialNum(uint8_t const* ProductSerialNum );
void GenEep_Set_ProductSerialNum_Ex(uint8_t const* ProductSerialNum,uint16_t const offset,uint16_t const len );
#else 
uint8_t GenEep_Get_ProductSerialNum(void);
void GenEep_Set_ProductSerialNum(uint8_t const ProductSerialNum);
#endif
#if EEP_CONTENT_ProductSerialNumMainBoard_ITEM_LEN > 1
void GenEep_Get_ProductSerialNumMainBoard(uint8_t * const ProductSerialNumMainBoard );
void GenEep_Get_ProductSerialNumMainBoard_Ex(uint8_t * const ProductSerialNumMainBoard,uint16_t const offset,uint16_t const len );
void GenEep_Set_ProductSerialNumMainBoard(uint8_t const* ProductSerialNumMainBoard );
void GenEep_Set_ProductSerialNumMainBoard_Ex(uint8_t const* ProductSerialNumMainBoard,uint16_t const offset,uint16_t const len );
#else 
uint8_t GenEep_Get_ProductSerialNumMainBoard(void);
void GenEep_Set_ProductSerialNumMainBoard(uint8_t const ProductSerialNumMainBoard);
#endif
#if EEP_CONTENT_ProductSerialNumSubBoard_ITEM_LEN > 1
void GenEep_Get_ProductSerialNumSubBoard(uint8_t * const ProductSerialNumSubBoard );
void GenEep_Get_ProductSerialNumSubBoard_Ex(uint8_t * const ProductSerialNumSubBoard,uint16_t const offset,uint16_t const len );
void GenEep_Set_ProductSerialNumSubBoard(uint8_t const* ProductSerialNumSubBoard );
void GenEep_Set_ProductSerialNumSubBoard_Ex(uint8_t const* ProductSerialNumSubBoard,uint16_t const offset,uint16_t const len );
#else 
uint8_t GenEep_Get_ProductSerialNumSubBoard(void);
void GenEep_Set_ProductSerialNumSubBoard(uint8_t const ProductSerialNumSubBoard);
#endif
#if EEP_CONTENT_SMDManufacturingDate_ITEM_LEN > 1
void GenEep_Get_SMDManufacturingDate(uint8_t * const SMDManufacturingDate );
void GenEep_Get_SMDManufacturingDate_Ex(uint8_t * const SMDManufacturingDate,uint16_t const offset,uint16_t const len );
void GenEep_Set_SMDManufacturingDate(uint8_t const* SMDManufacturingDate );
void GenEep_Set_SMDManufacturingDate_Ex(uint8_t const* SMDManufacturingDate,uint16_t const offset,uint16_t const len );
#else 
uint8_t GenEep_Get_SMDManufacturingDate(void);
void GenEep_Set_SMDManufacturingDate(uint8_t const SMDManufacturingDate);
#endif
#if EEP_CONTENT_AssemblyManufacturingDate_ITEM_LEN > 1
void GenEep_Get_AssemblyManufacturingDate(uint8_t * const AssemblyManufacturingDate );
void GenEep_Get_AssemblyManufacturingDate_Ex(uint8_t * const AssemblyManufacturingDate,uint16_t const offset,uint16_t const len );
void GenEep_Set_AssemblyManufacturingDate(uint8_t const* AssemblyManufacturingDate );
void GenEep_Set_AssemblyManufacturingDate_Ex(uint8_t const* AssemblyManufacturingDate,uint16_t const offset,uint16_t const len );
#else 
uint8_t GenEep_Get_AssemblyManufacturingDate(void);
void GenEep_Set_AssemblyManufacturingDate(uint8_t const AssemblyManufacturingDate);
#endif
#if EEP_CONTENT_TraceabilityBytes_ITEM_LEN > 1
void GenEep_Get_TraceabilityBytes(uint8_t * const TraceabilityBytes );
void GenEep_Get_TraceabilityBytes_Ex(uint8_t * const TraceabilityBytes,uint16_t const offset,uint16_t const len );
void GenEep_Set_TraceabilityBytes(uint8_t const* TraceabilityBytes );
void GenEep_Set_TraceabilityBytes_Ex(uint8_t const* TraceabilityBytes,uint16_t const offset,uint16_t const len );
#else 
uint8_t GenEep_Get_TraceabilityBytes(void);
void GenEep_Set_TraceabilityBytes(uint8_t const TraceabilityBytes);
#endif
#if EEP_CONTENT_SMDPlantNum_ITEM_LEN > 1
void GenEep_Get_SMDPlantNum(uint8_t * const SMDPlantNum );
void GenEep_Get_SMDPlantNum_Ex(uint8_t * const SMDPlantNum,uint16_t const offset,uint16_t const len );
void GenEep_Set_SMDPlantNum(uint8_t const* SMDPlantNum );
void GenEep_Set_SMDPlantNum_Ex(uint8_t const* SMDPlantNum,uint16_t const offset,uint16_t const len );
#else 
uint8_t GenEep_Get_SMDPlantNum(void);
void GenEep_Set_SMDPlantNum(uint8_t const SMDPlantNum);
#endif
#if EEP_CONTENT_AssemblyPlantNum_ITEM_LEN > 1
void GenEep_Get_AssemblyPlantNum(uint8_t * const AssemblyPlantNum );
void GenEep_Get_AssemblyPlantNum_Ex(uint8_t * const AssemblyPlantNum,uint16_t const offset,uint16_t const len );
void GenEep_Set_AssemblyPlantNum(uint8_t const* AssemblyPlantNum );
void GenEep_Set_AssemblyPlantNum_Ex(uint8_t const* AssemblyPlantNum,uint16_t const offset,uint16_t const len );
#else 
uint8_t GenEep_Get_AssemblyPlantNum(void);
void GenEep_Set_AssemblyPlantNum(uint8_t const AssemblyPlantNum);
#endif
#if EEP_CONTENT_DEM_EVENT_CAN_COM_DATA_ITEM_LEN > 1
void GenEep_Get_DEM_EVENT_CAN_COM_DATA(uint8_t * const DEM_EVENT_CAN_COM_DATA );
void GenEep_Get_DEM_EVENT_CAN_COM_DATA_Ex(uint8_t * const DEM_EVENT_CAN_COM_DATA,uint16_t const offset,uint16_t const len );
void GenEep_Set_DEM_EVENT_CAN_COM_DATA(uint8_t const* DEM_EVENT_CAN_COM_DATA );
void GenEep_Set_DEM_EVENT_CAN_COM_DATA_Ex(uint8_t const* DEM_EVENT_CAN_COM_DATA,uint16_t const offset,uint16_t const len );
#else 
uint8_t GenEep_Get_DEM_EVENT_CAN_COM_DATA(void);
void GenEep_Set_DEM_EVENT_CAN_COM_DATA(uint8_t const DEM_EVENT_CAN_COM_DATA);
#endif
#if EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_LEN > 1
void GenEep_Get_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA(uint8_t * const DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA );
void GenEep_Get_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_Ex(uint8_t * const DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA,uint16_t const offset,uint16_t const len );
void GenEep_Set_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA(uint8_t const* DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA );
void GenEep_Set_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_Ex(uint8_t const* DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA,uint16_t const offset,uint16_t const len );
#else 
uint8_t GenEep_Get_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA(void);
void GenEep_Set_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA(uint8_t const DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA);
#endif
#if EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_LEN > 1
void GenEep_Get_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA(uint8_t * const DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA );
void GenEep_Get_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_Ex(uint8_t * const DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA,uint16_t const offset,uint16_t const len );
void GenEep_Set_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA(uint8_t const* DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA );
void GenEep_Set_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_Ex(uint8_t const* DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA,uint16_t const offset,uint16_t const len );
#else 
uint8_t GenEep_Get_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA(void);
void GenEep_Set_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA(uint8_t const DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA);
#endif
#if EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_LEN > 1
void GenEep_Get_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA(uint8_t * const DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA );
void GenEep_Get_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_Ex(uint8_t * const DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA,uint16_t const offset,uint16_t const len );
void GenEep_Set_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA(uint8_t const* DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA );
void GenEep_Set_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_Ex(uint8_t const* DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA,uint16_t const offset,uint16_t const len );
#else 
uint8_t GenEep_Get_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA(void);
void GenEep_Set_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA(uint8_t const DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA);
#endif
#if EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_LEN > 1
void GenEep_Get_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA(uint8_t * const DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA );
void GenEep_Get_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_Ex(uint8_t * const DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA,uint16_t const offset,uint16_t const len );
void GenEep_Set_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA(uint8_t const* DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA );
void GenEep_Set_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_Ex(uint8_t const* DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA,uint16_t const offset,uint16_t const len );
#else 
uint8_t GenEep_Get_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA(void);
void GenEep_Set_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA(uint8_t const DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA);
#endif
#if EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_LEN > 1
void GenEep_Get_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA(uint8_t * const DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA );
void GenEep_Get_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_Ex(uint8_t * const DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA,uint16_t const offset,uint16_t const len );
void GenEep_Set_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA(uint8_t const* DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA );
void GenEep_Set_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_Ex(uint8_t const* DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA,uint16_t const offset,uint16_t const len );
#else 
uint8_t GenEep_Get_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA(void);
void GenEep_Set_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA(uint8_t const DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA);
#endif
#if EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_LEN > 1
void GenEep_Get_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA(uint8_t * const DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA );
void GenEep_Get_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_Ex(uint8_t * const DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA,uint16_t const offset,uint16_t const len );
void GenEep_Set_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA(uint8_t const* DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA );
void GenEep_Set_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_Ex(uint8_t const* DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA,uint16_t const offset,uint16_t const len );
#else 
uint8_t GenEep_Get_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA(void);
void GenEep_Set_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA(uint8_t const DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA);
#endif
#if EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_LEN > 1
void GenEep_Get_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA(uint8_t * const DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA );
void GenEep_Get_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_Ex(uint8_t * const DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA,uint16_t const offset,uint16_t const len );
void GenEep_Set_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA(uint8_t const* DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA );
void GenEep_Set_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_Ex(uint8_t const* DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA,uint16_t const offset,uint16_t const len );
#else 
uint8_t GenEep_Get_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA(void);
void GenEep_Set_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA(uint8_t const DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA);
#endif
#if EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_LEN > 1
void GenEep_Get_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA(uint8_t * const DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA );
void GenEep_Get_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_Ex(uint8_t * const DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA,uint16_t const offset,uint16_t const len );
void GenEep_Set_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA(uint8_t const* DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA );
void GenEep_Set_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_Ex(uint8_t const* DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA,uint16_t const offset,uint16_t const len );
#else 
uint8_t GenEep_Get_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA(void);
void GenEep_Set_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA(uint8_t const DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA);
#endif
#if EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_ITEM_LEN > 1
void GenEep_Get_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA(uint8_t * const DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA );
void GenEep_Get_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_Ex(uint8_t * const DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA,uint16_t const offset,uint16_t const len );
void GenEep_Set_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA(uint8_t const* DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA );
void GenEep_Set_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_Ex(uint8_t const* DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA,uint16_t const offset,uint16_t const len );
#else 
uint8_t GenEep_Get_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA(void);
void GenEep_Set_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA(uint8_t const DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA);
#endif
#if EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_ITEM_LEN > 1
void GenEep_Get_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA(uint8_t * const DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA );
void GenEep_Get_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_Ex(uint8_t * const DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA,uint16_t const offset,uint16_t const len );
void GenEep_Set_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA(uint8_t const* DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA );
void GenEep_Set_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_Ex(uint8_t const* DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA,uint16_t const offset,uint16_t const len );
#else 
uint8_t GenEep_Get_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA(void);
void GenEep_Set_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA(uint8_t const DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA);
#endif
#if EEP_CONTENT_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_ITEM_LEN > 1
void GenEep_Get_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA(uint8_t * const DEM_EVENT_HVAC_PANEL_CONNECTION_DATA );
void GenEep_Get_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_Ex(uint8_t * const DEM_EVENT_HVAC_PANEL_CONNECTION_DATA,uint16_t const offset,uint16_t const len );
void GenEep_Set_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA(uint8_t const* DEM_EVENT_HVAC_PANEL_CONNECTION_DATA );
void GenEep_Set_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_Ex(uint8_t const* DEM_EVENT_HVAC_PANEL_CONNECTION_DATA,uint16_t const offset,uint16_t const len );
#else 
uint8_t GenEep_Get_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA(void);
void GenEep_Set_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA(uint8_t const DEM_EVENT_HVAC_PANEL_CONNECTION_DATA);
#endif
uint32_t GenEep_Get_development_nvm_write_count(void);
#if EEP_CONTENT_TachoUpValue_ITEM_LEN > 1
void GenEep_Get_TachoUpValue(uint16_t * const TachoUpValue );
void GenEep_Get_TachoUpValue_Ex(uint8_t * const TachoUpValue,uint16_t const offset,uint16_t const len );
void GenEep_Set_TachoUpValue(uint16_t const* TachoUpValue );
void GenEep_Set_TachoUpValue_Ex(uint8_t const* TachoUpValue,uint16_t const offset,uint16_t const len );
#else 
uint16_t GenEep_Get_TachoUpValue(void);
void GenEep_Set_TachoUpValue(uint16_t const TachoUpValue);
#endif
#if EEP_CONTENT_TachoLowValue_ITEM_LEN > 1
void GenEep_Get_TachoLowValue(uint16_t * const TachoLowValue );
void GenEep_Get_TachoLowValue_Ex(uint8_t * const TachoLowValue,uint16_t const offset,uint16_t const len );
void GenEep_Set_TachoLowValue(uint16_t const* TachoLowValue );
void GenEep_Set_TachoLowValue_Ex(uint8_t const* TachoLowValue,uint16_t const offset,uint16_t const len );
#else 
uint16_t GenEep_Get_TachoLowValue(void);
void GenEep_Set_TachoLowValue(uint16_t const TachoLowValue);
#endif
#if EEP_CONTENT_TachoLimitValue_ITEM_LEN > 1
void GenEep_Get_TachoLimitValue(uint16_t * const TachoLimitValue );
void GenEep_Get_TachoLimitValue_Ex(uint8_t * const TachoLimitValue,uint16_t const offset,uint16_t const len );
void GenEep_Set_TachoLimitValue(uint16_t const* TachoLimitValue );
void GenEep_Set_TachoLimitValue_Ex(uint8_t const* TachoLimitValue,uint16_t const offset,uint16_t const len );
#else 
uint16_t GenEep_Get_TachoLimitValue(void);
void GenEep_Set_TachoLimitValue(uint16_t const TachoLimitValue);
#endif
#if EEP_CONTENT_TachoUDeltaLimit_ITEM_LEN > 1
void GenEep_Get_TachoUDeltaLimit(uint16_t * const TachoUDeltaLimit );
void GenEep_Get_TachoUDeltaLimit_Ex(uint8_t * const TachoUDeltaLimit,uint16_t const offset,uint16_t const len );
void GenEep_Set_TachoUDeltaLimit(uint16_t const* TachoUDeltaLimit );
void GenEep_Set_TachoUDeltaLimit_Ex(uint8_t const* TachoUDeltaLimit,uint16_t const offset,uint16_t const len );
#else 
uint16_t GenEep_Get_TachoUDeltaLimit(void);
void GenEep_Set_TachoUDeltaLimit(uint16_t const TachoUDeltaLimit);
#endif
#if EEP_CONTENT_TachoLDeltaLimit_ITEM_LEN > 1
void GenEep_Get_TachoLDeltaLimit(uint16_t * const TachoLDeltaLimit );
void GenEep_Get_TachoLDeltaLimit_Ex(uint8_t * const TachoLDeltaLimit,uint16_t const offset,uint16_t const len );
void GenEep_Set_TachoLDeltaLimit(uint16_t const* TachoLDeltaLimit );
void GenEep_Set_TachoLDeltaLimit_Ex(uint8_t const* TachoLDeltaLimit,uint16_t const offset,uint16_t const len );
#else 
uint16_t GenEep_Get_TachoLDeltaLimit(void);
void GenEep_Set_TachoLDeltaLimit(uint16_t const TachoLDeltaLimit);
#endif
#if EEP_CONTENT_TachoUpChangeValue_ITEM_LEN > 1
void GenEep_Get_TachoUpChangeValue(uint16_t * const TachoUpChangeValue );
void GenEep_Get_TachoUpChangeValue_Ex(uint8_t * const TachoUpChangeValue,uint16_t const offset,uint16_t const len );
void GenEep_Set_TachoUpChangeValue(uint16_t const* TachoUpChangeValue );
void GenEep_Set_TachoUpChangeValue_Ex(uint8_t const* TachoUpChangeValue,uint16_t const offset,uint16_t const len );
#else 
uint16_t GenEep_Get_TachoUpChangeValue(void);
void GenEep_Set_TachoUpChangeValue(uint16_t const TachoUpChangeValue);
#endif
#if EEP_CONTENT_TachoLowChangeValue_ITEM_LEN > 1
void GenEep_Get_TachoLowChangeValue(uint16_t * const TachoLowChangeValue );
void GenEep_Get_TachoLowChangeValue_Ex(uint8_t * const TachoLowChangeValue,uint16_t const offset,uint16_t const len );
void GenEep_Set_TachoLowChangeValue(uint16_t const* TachoLowChangeValue );
void GenEep_Set_TachoLowChangeValue_Ex(uint8_t const* TachoLowChangeValue,uint16_t const offset,uint16_t const len );
#else 
uint16_t GenEep_Get_TachoLowChangeValue(void);
void GenEep_Set_TachoLowChangeValue(uint16_t const TachoLowChangeValue);
#endif
#if EEP_CONTENT_TachoRun_ITEM_LEN > 1
void GenEep_Get_TachoRun(uint16_t * const TachoRun );
void GenEep_Get_TachoRun_Ex(uint8_t * const TachoRun,uint16_t const offset,uint16_t const len );
void GenEep_Set_TachoRun(uint16_t const* TachoRun );
void GenEep_Set_TachoRun_Ex(uint8_t const* TachoRun,uint16_t const offset,uint16_t const len );
#else 
uint16_t GenEep_Get_TachoRun(void);
void GenEep_Set_TachoRun(uint16_t const TachoRun);
#endif
#if EEP_CONTENT_Speed_Rm_1_ITEM_LEN > 1
void GenEep_Get_Speed_Rm_1(uint16_t * const Speed_Rm_1 );
void GenEep_Get_Speed_Rm_1_Ex(uint8_t * const Speed_Rm_1,uint16_t const offset,uint16_t const len );
void GenEep_Set_Speed_Rm_1(uint16_t const* Speed_Rm_1 );
void GenEep_Set_Speed_Rm_1_Ex(uint8_t const* Speed_Rm_1,uint16_t const offset,uint16_t const len );
#else 
uint16_t GenEep_Get_Speed_Rm_1(void);
void GenEep_Set_Speed_Rm_1(uint16_t const Speed_Rm_1);
#endif
#if EEP_CONTENT_Speed_Rm_2_ITEM_LEN > 1
void GenEep_Get_Speed_Rm_2(uint16_t * const Speed_Rm_2 );
void GenEep_Get_Speed_Rm_2_Ex(uint8_t * const Speed_Rm_2,uint16_t const offset,uint16_t const len );
void GenEep_Set_Speed_Rm_2(uint16_t const* Speed_Rm_2 );
void GenEep_Set_Speed_Rm_2_Ex(uint8_t const* Speed_Rm_2,uint16_t const offset,uint16_t const len );
#else 
uint16_t GenEep_Get_Speed_Rm_2(void);
void GenEep_Set_Speed_Rm_2(uint16_t const Speed_Rm_2);
#endif
#if EEP_CONTENT_Speed_Rc_ITEM_LEN > 1
void GenEep_Get_Speed_Rc(uint16_t * const Speed_Rc );
void GenEep_Get_Speed_Rc_Ex(uint8_t * const Speed_Rc,uint16_t const offset,uint16_t const len );
void GenEep_Set_Speed_Rc(uint16_t const* Speed_Rc );
void GenEep_Set_Speed_Rc_Ex(uint8_t const* Speed_Rc,uint16_t const offset,uint16_t const len );
#else 
uint16_t GenEep_Get_Speed_Rc(void);
void GenEep_Set_Speed_Rc(uint16_t const Speed_Rc);
#endif
#if EEP_CONTENT_FuelTelltaleOn_R_ITEM_LEN > 1
void GenEep_Get_FuelTelltaleOn_R(uint16_t * const FuelTelltaleOn_R );
void GenEep_Get_FuelTelltaleOn_R_Ex(uint8_t * const FuelTelltaleOn_R,uint16_t const offset,uint16_t const len );
void GenEep_Set_FuelTelltaleOn_R(uint16_t const* FuelTelltaleOn_R );
void GenEep_Set_FuelTelltaleOn_R_Ex(uint8_t const* FuelTelltaleOn_R,uint16_t const offset,uint16_t const len );
#else 
uint16_t GenEep_Get_FuelTelltaleOn_R(void);
void GenEep_Set_FuelTelltaleOn_R(uint16_t const FuelTelltaleOn_R);
#endif
#if EEP_CONTENT_FuelTelltaleOff_R_ITEM_LEN > 1
void GenEep_Get_FuelTelltaleOff_R(uint16_t * const FuelTelltaleOff_R );
void GenEep_Get_FuelTelltaleOff_R_Ex(uint8_t * const FuelTelltaleOff_R,uint16_t const offset,uint16_t const len );
void GenEep_Set_FuelTelltaleOff_R(uint16_t const* FuelTelltaleOff_R );
void GenEep_Set_FuelTelltaleOff_R_Ex(uint8_t const* FuelTelltaleOff_R,uint16_t const offset,uint16_t const len );
#else 
uint16_t GenEep_Get_FuelTelltaleOff_R(void);
void GenEep_Set_FuelTelltaleOff_R(uint16_t const FuelTelltaleOff_R);
#endif
#if EEP_CONTENT_FuelWarningOnPoint_R_ITEM_LEN > 1
void GenEep_Get_FuelWarningOnPoint_R(uint16_t * const FuelWarningOnPoint_R );
void GenEep_Get_FuelWarningOnPoint_R_Ex(uint8_t * const FuelWarningOnPoint_R,uint16_t const offset,uint16_t const len );
void GenEep_Set_FuelWarningOnPoint_R(uint16_t const* FuelWarningOnPoint_R );
void GenEep_Set_FuelWarningOnPoint_R_Ex(uint8_t const* FuelWarningOnPoint_R,uint16_t const offset,uint16_t const len );
#else 
uint16_t GenEep_Get_FuelWarningOnPoint_R(void);
void GenEep_Set_FuelWarningOnPoint_R(uint16_t const FuelWarningOnPoint_R);
#endif
#if EEP_CONTENT_Fuel_R_Open_ITEM_LEN > 1
void GenEep_Get_Fuel_R_Open(uint16_t * const Fuel_R_Open );
void GenEep_Get_Fuel_R_Open_Ex(uint8_t * const Fuel_R_Open,uint16_t const offset,uint16_t const len );
void GenEep_Set_Fuel_R_Open(uint16_t const* Fuel_R_Open );
void GenEep_Set_Fuel_R_Open_Ex(uint8_t const* Fuel_R_Open,uint16_t const offset,uint16_t const len );
#else 
uint16_t GenEep_Get_Fuel_R_Open(void);
void GenEep_Set_Fuel_R_Open(uint16_t const Fuel_R_Open);
#endif
#if EEP_CONTENT_Fuel_R_Short_ITEM_LEN > 1
void GenEep_Get_Fuel_R_Short(uint16_t * const Fuel_R_Short );
void GenEep_Get_Fuel_R_Short_Ex(uint8_t * const Fuel_R_Short,uint16_t const offset,uint16_t const len );
void GenEep_Set_Fuel_R_Short(uint16_t const* Fuel_R_Short );
void GenEep_Set_Fuel_R_Short_Ex(uint8_t const* Fuel_R_Short,uint16_t const offset,uint16_t const len );
#else 
uint16_t GenEep_Get_Fuel_R_Short(void);
void GenEep_Set_Fuel_R_Short(uint16_t const Fuel_R_Short);
#endif
#if EEP_CONTENT_PkbWarningJudgeV1_ITEM_LEN > 1
void GenEep_Get_PkbWarningJudgeV1(uint8_t * const PkbWarningJudgeV1 );
void GenEep_Get_PkbWarningJudgeV1_Ex(uint8_t * const PkbWarningJudgeV1,uint16_t const offset,uint16_t const len );
void GenEep_Set_PkbWarningJudgeV1(uint8_t const* PkbWarningJudgeV1 );
void GenEep_Set_PkbWarningJudgeV1_Ex(uint8_t const* PkbWarningJudgeV1,uint16_t const offset,uint16_t const len );
#else 
uint8_t GenEep_Get_PkbWarningJudgeV1(void);
void GenEep_Set_PkbWarningJudgeV1(uint8_t const PkbWarningJudgeV1);
#endif
#if EEP_CONTENT_PkbWarningJudgeV2_ITEM_LEN > 1
void GenEep_Get_PkbWarningJudgeV2(uint8_t * const PkbWarningJudgeV2 );
void GenEep_Get_PkbWarningJudgeV2_Ex(uint8_t * const PkbWarningJudgeV2,uint16_t const offset,uint16_t const len );
void GenEep_Set_PkbWarningJudgeV2(uint8_t const* PkbWarningJudgeV2 );
void GenEep_Set_PkbWarningJudgeV2_Ex(uint8_t const* PkbWarningJudgeV2,uint16_t const offset,uint16_t const len );
#else 
uint8_t GenEep_Get_PkbWarningJudgeV2(void);
void GenEep_Set_PkbWarningJudgeV2(uint8_t const PkbWarningJudgeV2);
#endif
#if EEP_CONTENT_AudioEepTest_ITEM_LEN > 1
void GenEep_Get_AudioEepTest(uint16_t * const AudioEepTest );
void GenEep_Get_AudioEepTest_Ex(uint8_t * const AudioEepTest,uint16_t const offset,uint16_t const len );
void GenEep_Set_AudioEepTest(uint16_t const* AudioEepTest );
void GenEep_Set_AudioEepTest_Ex(uint8_t const* AudioEepTest,uint16_t const offset,uint16_t const len );
#else 
uint16_t GenEep_Get_AudioEepTest(void);
void GenEep_Set_AudioEepTest(uint16_t const AudioEepTest);
#endif
#if EEP_CONTENT_DspKeyCodeOnOff_ITEM_LEN > 1
void GenEep_Get_DspKeyCodeOnOff(uint8_t * const DspKeyCodeOnOff );
void GenEep_Get_DspKeyCodeOnOff_Ex(uint8_t * const DspKeyCodeOnOff,uint16_t const offset,uint16_t const len );
void GenEep_Set_DspKeyCodeOnOff(uint8_t const* DspKeyCodeOnOff );
void GenEep_Set_DspKeyCodeOnOff_Ex(uint8_t const* DspKeyCodeOnOff,uint16_t const offset,uint16_t const len );
#else 
uint8_t GenEep_Get_DspKeyCodeOnOff(void);
void GenEep_Set_DspKeyCodeOnOff(uint8_t const DspKeyCodeOnOff);
#endif
#if EEP_CONTENT_DspKeyCode_ITEM_LEN > 1
void GenEep_Get_DspKeyCode(uint32_t * const DspKeyCode );
void GenEep_Get_DspKeyCode_Ex(uint8_t * const DspKeyCode,uint16_t const offset,uint16_t const len );
void GenEep_Set_DspKeyCode(uint32_t const* DspKeyCode );
void GenEep_Set_DspKeyCode_Ex(uint8_t const* DspKeyCode,uint16_t const offset,uint16_t const len );
#else 
uint32_t GenEep_Get_DspKeyCode(void);
void GenEep_Set_DspKeyCode(uint32_t const DspKeyCode);
#endif
#if EEP_CONTENT_BatState_ChgDly_ITEM_LEN > 1
void GenEep_Get_BatState_ChgDly(uint16_t * const BatState_ChgDly );
void GenEep_Get_BatState_ChgDly_Ex(uint8_t * const BatState_ChgDly,uint16_t const offset,uint16_t const len );
void GenEep_Set_BatState_ChgDly(uint16_t const* BatState_ChgDly );
void GenEep_Set_BatState_ChgDly_Ex(uint8_t const* BatState_ChgDly,uint16_t const offset,uint16_t const len );
#else 
uint16_t GenEep_Get_BatState_ChgDly(void);
void GenEep_Set_BatState_ChgDly(uint16_t const BatState_ChgDly);
#endif
#if EEP_CONTENT_BatVolVeryHigh_Hysteresis_H_ITEM_LEN > 1
void GenEep_Get_BatVolVeryHigh_Hysteresis_H(uint16_t * const BatVolVeryHigh_Hysteresis_H );
void GenEep_Get_BatVolVeryHigh_Hysteresis_H_Ex(uint8_t * const BatVolVeryHigh_Hysteresis_H,uint16_t const offset,uint16_t const len );
void GenEep_Set_BatVolVeryHigh_Hysteresis_H(uint16_t const* BatVolVeryHigh_Hysteresis_H );
void GenEep_Set_BatVolVeryHigh_Hysteresis_H_Ex(uint8_t const* BatVolVeryHigh_Hysteresis_H,uint16_t const offset,uint16_t const len );
#else 
uint16_t GenEep_Get_BatVolVeryHigh_Hysteresis_H(void);
void GenEep_Set_BatVolVeryHigh_Hysteresis_H(uint16_t const BatVolVeryHigh_Hysteresis_H);
#endif
#if EEP_CONTENT_BatVolVeryHigh_Hysteresis_L_ITEM_LEN > 1
void GenEep_Get_BatVolVeryHigh_Hysteresis_L(uint16_t * const BatVolVeryHigh_Hysteresis_L );
void GenEep_Get_BatVolVeryHigh_Hysteresis_L_Ex(uint8_t * const BatVolVeryHigh_Hysteresis_L,uint16_t const offset,uint16_t const len );
void GenEep_Set_BatVolVeryHigh_Hysteresis_L(uint16_t const* BatVolVeryHigh_Hysteresis_L );
void GenEep_Set_BatVolVeryHigh_Hysteresis_L_Ex(uint8_t const* BatVolVeryHigh_Hysteresis_L,uint16_t const offset,uint16_t const len );
#else 
uint16_t GenEep_Get_BatVolVeryHigh_Hysteresis_L(void);
void GenEep_Set_BatVolVeryHigh_Hysteresis_L(uint16_t const BatVolVeryHigh_Hysteresis_L);
#endif
#if EEP_CONTENT_BatVolHigh_Hysteresis_H_ITEM_LEN > 1
void GenEep_Get_BatVolHigh_Hysteresis_H(uint16_t * const BatVolHigh_Hysteresis_H );
void GenEep_Get_BatVolHigh_Hysteresis_H_Ex(uint8_t * const BatVolHigh_Hysteresis_H,uint16_t const offset,uint16_t const len );
void GenEep_Set_BatVolHigh_Hysteresis_H(uint16_t const* BatVolHigh_Hysteresis_H );
void GenEep_Set_BatVolHigh_Hysteresis_H_Ex(uint8_t const* BatVolHigh_Hysteresis_H,uint16_t const offset,uint16_t const len );
#else 
uint16_t GenEep_Get_BatVolHigh_Hysteresis_H(void);
void GenEep_Set_BatVolHigh_Hysteresis_H(uint16_t const BatVolHigh_Hysteresis_H);
#endif
#if EEP_CONTENT_BatVolHigh_Hysteresis_L_ITEM_LEN > 1
void GenEep_Get_BatVolHigh_Hysteresis_L(uint16_t * const BatVolHigh_Hysteresis_L );
void GenEep_Get_BatVolHigh_Hysteresis_L_Ex(uint8_t * const BatVolHigh_Hysteresis_L,uint16_t const offset,uint16_t const len );
void GenEep_Set_BatVolHigh_Hysteresis_L(uint16_t const* BatVolHigh_Hysteresis_L );
void GenEep_Set_BatVolHigh_Hysteresis_L_Ex(uint8_t const* BatVolHigh_Hysteresis_L,uint16_t const offset,uint16_t const len );
#else 
uint16_t GenEep_Get_BatVolHigh_Hysteresis_L(void);
void GenEep_Set_BatVolHigh_Hysteresis_L(uint16_t const BatVolHigh_Hysteresis_L);
#endif
#if EEP_CONTENT_BatVolLow_Hysteresis_H_ITEM_LEN > 1
void GenEep_Get_BatVolLow_Hysteresis_H(uint16_t * const BatVolLow_Hysteresis_H );
void GenEep_Get_BatVolLow_Hysteresis_H_Ex(uint8_t * const BatVolLow_Hysteresis_H,uint16_t const offset,uint16_t const len );
void GenEep_Set_BatVolLow_Hysteresis_H(uint16_t const* BatVolLow_Hysteresis_H );
void GenEep_Set_BatVolLow_Hysteresis_H_Ex(uint8_t const* BatVolLow_Hysteresis_H,uint16_t const offset,uint16_t const len );
#else 
uint16_t GenEep_Get_BatVolLow_Hysteresis_H(void);
void GenEep_Set_BatVolLow_Hysteresis_H(uint16_t const BatVolLow_Hysteresis_H);
#endif
#if EEP_CONTENT_BatVolLow_Hysteresis_L_ITEM_LEN > 1
void GenEep_Get_BatVolLow_Hysteresis_L(uint16_t * const BatVolLow_Hysteresis_L );
void GenEep_Get_BatVolLow_Hysteresis_L_Ex(uint8_t * const BatVolLow_Hysteresis_L,uint16_t const offset,uint16_t const len );
void GenEep_Set_BatVolLow_Hysteresis_L(uint16_t const* BatVolLow_Hysteresis_L );
void GenEep_Set_BatVolLow_Hysteresis_L_Ex(uint8_t const* BatVolLow_Hysteresis_L,uint16_t const offset,uint16_t const len );
#else 
uint16_t GenEep_Get_BatVolLow_Hysteresis_L(void);
void GenEep_Set_BatVolLow_Hysteresis_L(uint16_t const BatVolLow_Hysteresis_L);
#endif
#if EEP_CONTENT_BatVolVeryLow_Hysteresis_H_ITEM_LEN > 1
void GenEep_Get_BatVolVeryLow_Hysteresis_H(uint16_t * const BatVolVeryLow_Hysteresis_H );
void GenEep_Get_BatVolVeryLow_Hysteresis_H_Ex(uint8_t * const BatVolVeryLow_Hysteresis_H,uint16_t const offset,uint16_t const len );
void GenEep_Set_BatVolVeryLow_Hysteresis_H(uint16_t const* BatVolVeryLow_Hysteresis_H );
void GenEep_Set_BatVolVeryLow_Hysteresis_H_Ex(uint8_t const* BatVolVeryLow_Hysteresis_H,uint16_t const offset,uint16_t const len );
#else 
uint16_t GenEep_Get_BatVolVeryLow_Hysteresis_H(void);
void GenEep_Set_BatVolVeryLow_Hysteresis_H(uint16_t const BatVolVeryLow_Hysteresis_H);
#endif
#if EEP_CONTENT_BatVolVeryLow_Hysteresis_L_ITEM_LEN > 1
void GenEep_Get_BatVolVeryLow_Hysteresis_L(uint16_t * const BatVolVeryLow_Hysteresis_L );
void GenEep_Get_BatVolVeryLow_Hysteresis_L_Ex(uint8_t * const BatVolVeryLow_Hysteresis_L,uint16_t const offset,uint16_t const len );
void GenEep_Set_BatVolVeryLow_Hysteresis_L(uint16_t const* BatVolVeryLow_Hysteresis_L );
void GenEep_Set_BatVolVeryLow_Hysteresis_L_Ex(uint8_t const* BatVolVeryLow_Hysteresis_L,uint16_t const offset,uint16_t const len );
#else 
uint16_t GenEep_Get_BatVolVeryLow_Hysteresis_L(void);
void GenEep_Set_BatVolVeryLow_Hysteresis_L(uint16_t const BatVolVeryLow_Hysteresis_L);
#endif
#if EEP_CONTENT_TempState_ChgDly_ITEM_LEN > 1
void GenEep_Get_TempState_ChgDly(uint8_t * const TempState_ChgDly );
void GenEep_Get_TempState_ChgDly_Ex(uint8_t * const TempState_ChgDly,uint16_t const offset,uint16_t const len );
void GenEep_Set_TempState_ChgDly(uint8_t const* TempState_ChgDly );
void GenEep_Set_TempState_ChgDly_Ex(uint8_t const* TempState_ChgDly,uint16_t const offset,uint16_t const len );
#else 
uint8_t GenEep_Get_TempState_ChgDly(void);
void GenEep_Set_TempState_ChgDly(uint8_t const TempState_ChgDly);
#endif
#if EEP_CONTENT_TempDegC_Low_Hysteresis_L_ITEM_LEN > 1
void GenEep_Get_TempDegC_Low_Hysteresis_L(uint8_t * const TempDegC_Low_Hysteresis_L );
void GenEep_Get_TempDegC_Low_Hysteresis_L_Ex(uint8_t * const TempDegC_Low_Hysteresis_L,uint16_t const offset,uint16_t const len );
void GenEep_Set_TempDegC_Low_Hysteresis_L(uint8_t const* TempDegC_Low_Hysteresis_L );
void GenEep_Set_TempDegC_Low_Hysteresis_L_Ex(uint8_t const* TempDegC_Low_Hysteresis_L,uint16_t const offset,uint16_t const len );
#else 
uint8_t GenEep_Get_TempDegC_Low_Hysteresis_L(void);
void GenEep_Set_TempDegC_Low_Hysteresis_L(uint8_t const TempDegC_Low_Hysteresis_L);
#endif
#if EEP_CONTENT_TempDegC_Low_Hysteresis_H_ITEM_LEN > 1
void GenEep_Get_TempDegC_Low_Hysteresis_H(uint8_t * const TempDegC_Low_Hysteresis_H );
void GenEep_Get_TempDegC_Low_Hysteresis_H_Ex(uint8_t * const TempDegC_Low_Hysteresis_H,uint16_t const offset,uint16_t const len );
void GenEep_Set_TempDegC_Low_Hysteresis_H(uint8_t const* TempDegC_Low_Hysteresis_H );
void GenEep_Set_TempDegC_Low_Hysteresis_H_Ex(uint8_t const* TempDegC_Low_Hysteresis_H,uint16_t const offset,uint16_t const len );
#else 
uint8_t GenEep_Get_TempDegC_Low_Hysteresis_H(void);
void GenEep_Set_TempDegC_Low_Hysteresis_H(uint8_t const TempDegC_Low_Hysteresis_H);
#endif
#if EEP_CONTENT_TempDegC_High_Hysteresis_L_ITEM_LEN > 1
void GenEep_Get_TempDegC_High_Hysteresis_L(uint8_t * const TempDegC_High_Hysteresis_L );
void GenEep_Get_TempDegC_High_Hysteresis_L_Ex(uint8_t * const TempDegC_High_Hysteresis_L,uint16_t const offset,uint16_t const len );
void GenEep_Set_TempDegC_High_Hysteresis_L(uint8_t const* TempDegC_High_Hysteresis_L );
void GenEep_Set_TempDegC_High_Hysteresis_L_Ex(uint8_t const* TempDegC_High_Hysteresis_L,uint16_t const offset,uint16_t const len );
#else 
uint8_t GenEep_Get_TempDegC_High_Hysteresis_L(void);
void GenEep_Set_TempDegC_High_Hysteresis_L(uint8_t const TempDegC_High_Hysteresis_L);
#endif
#if EEP_CONTENT_TempDegC_High_Hysteresis_H_ITEM_LEN > 1
void GenEep_Get_TempDegC_High_Hysteresis_H(uint8_t * const TempDegC_High_Hysteresis_H );
void GenEep_Get_TempDegC_High_Hysteresis_H_Ex(uint8_t * const TempDegC_High_Hysteresis_H,uint16_t const offset,uint16_t const len );
void GenEep_Set_TempDegC_High_Hysteresis_H(uint8_t const* TempDegC_High_Hysteresis_H );
void GenEep_Set_TempDegC_High_Hysteresis_H_Ex(uint8_t const* TempDegC_High_Hysteresis_H,uint16_t const offset,uint16_t const len );
#else 
uint8_t GenEep_Get_TempDegC_High_Hysteresis_H(void);
void GenEep_Set_TempDegC_High_Hysteresis_H(uint8_t const TempDegC_High_Hysteresis_H);
#endif
#if EEP_CONTENT_AmpHighTempProction_ITEM_LEN > 1
void GenEep_Get_AmpHighTempProction(uint32_t * const AmpHighTempProction );
void GenEep_Get_AmpHighTempProction_Ex(uint8_t * const AmpHighTempProction,uint16_t const offset,uint16_t const len );
void GenEep_Set_AmpHighTempProction(uint32_t const* AmpHighTempProction );
void GenEep_Set_AmpHighTempProction_Ex(uint8_t const* AmpHighTempProction,uint16_t const offset,uint16_t const len );
#else 
uint32_t GenEep_Get_AmpHighTempProction(void);
void GenEep_Set_AmpHighTempProction(uint32_t const AmpHighTempProction);
#endif
#if EEP_CONTENT_CanNm_DA_S1_Delay_ms_ITEM_LEN > 1
void GenEep_Get_CanNm_DA_S1_Delay_ms(uint16_t * const CanNm_DA_S1_Delay_ms );
void GenEep_Get_CanNm_DA_S1_Delay_ms_Ex(uint8_t * const CanNm_DA_S1_Delay_ms,uint16_t const offset,uint16_t const len );
void GenEep_Set_CanNm_DA_S1_Delay_ms(uint16_t const* CanNm_DA_S1_Delay_ms );
void GenEep_Set_CanNm_DA_S1_Delay_ms_Ex(uint8_t const* CanNm_DA_S1_Delay_ms,uint16_t const offset,uint16_t const len );
#else 
uint16_t GenEep_Get_CanNm_DA_S1_Delay_ms(void);
void GenEep_Set_CanNm_DA_S1_Delay_ms(uint16_t const CanNm_DA_S1_Delay_ms);
#endif
uint32_t GenEep_Get_OemSetting_write_count(void);
#if EEP_CONTENT_Vin_ITEM_LEN > 1
void GenEep_Get_Vin(uint8_t * const Vin );
void GenEep_Get_Vin_Ex(uint8_t * const Vin,uint16_t const offset,uint16_t const len );
void GenEep_Set_Vin(uint8_t const* Vin );
void GenEep_Set_Vin_Ex(uint8_t const* Vin,uint16_t const offset,uint16_t const len );
#else 
uint8_t GenEep_Get_Vin(void);
void GenEep_Set_Vin(uint8_t const Vin);
#endif
uint32_t GenEep_Get_user_setting_write_count(void);
#if EEP_CONTENT_AutoSyncTimeWithGps_ITEM_LEN > 1
void GenEep_Get_AutoSyncTimeWithGps(uint8_t * const AutoSyncTimeWithGps );
void GenEep_Get_AutoSyncTimeWithGps_Ex(uint8_t * const AutoSyncTimeWithGps,uint16_t const offset,uint16_t const len );
void GenEep_Set_AutoSyncTimeWithGps(uint8_t const* AutoSyncTimeWithGps );
void GenEep_Set_AutoSyncTimeWithGps_Ex(uint8_t const* AutoSyncTimeWithGps,uint16_t const offset,uint16_t const len );
#else 
uint8_t GenEep_Get_AutoSyncTimeWithGps(void);
void GenEep_Set_AutoSyncTimeWithGps(uint8_t const AutoSyncTimeWithGps);
#endif
#if EEP_CONTENT_ScreenBackLightValOnDay_ITEM_LEN > 1
void GenEep_Get_ScreenBackLightValOnDay(uint8_t * const ScreenBackLightValOnDay );
void GenEep_Get_ScreenBackLightValOnDay_Ex(uint8_t * const ScreenBackLightValOnDay,uint16_t const offset,uint16_t const len );
void GenEep_Set_ScreenBackLightValOnDay(uint8_t const* ScreenBackLightValOnDay );
void GenEep_Set_ScreenBackLightValOnDay_Ex(uint8_t const* ScreenBackLightValOnDay,uint16_t const offset,uint16_t const len );
#else 
uint8_t GenEep_Get_ScreenBackLightValOnDay(void);
void GenEep_Set_ScreenBackLightValOnDay(uint8_t const ScreenBackLightValOnDay);
#endif
#if EEP_CONTENT_ScreenBackLightValOnNight_ITEM_LEN > 1
void GenEep_Get_ScreenBackLightValOnNight(uint8_t * const ScreenBackLightValOnNight );
void GenEep_Get_ScreenBackLightValOnNight_Ex(uint8_t * const ScreenBackLightValOnNight,uint16_t const offset,uint16_t const len );
void GenEep_Set_ScreenBackLightValOnNight(uint8_t const* ScreenBackLightValOnNight );
void GenEep_Set_ScreenBackLightValOnNight_Ex(uint8_t const* ScreenBackLightValOnNight,uint16_t const offset,uint16_t const len );
#else 
uint8_t GenEep_Get_ScreenBackLightValOnNight(void);
void GenEep_Set_ScreenBackLightValOnNight(uint8_t const ScreenBackLightValOnNight);
#endif
#if EEP_CONTENT_HistoryAverFuelCons_ITEM_LEN > 1
void GenEep_Get_HistoryAverFuelCons(uint8_t * const HistoryAverFuelCons );
void GenEep_Get_HistoryAverFuelCons_Ex(uint8_t * const HistoryAverFuelCons,uint16_t const offset,uint16_t const len );
void GenEep_Set_HistoryAverFuelCons(uint8_t const* HistoryAverFuelCons );
void GenEep_Set_HistoryAverFuelCons_Ex(uint8_t const* HistoryAverFuelCons,uint16_t const offset,uint16_t const len );
#else 
uint8_t GenEep_Get_HistoryAverFuelCons(void);
void GenEep_Set_HistoryAverFuelCons(uint8_t const HistoryAverFuelCons);
#endif
#if EEP_CONTENT_FuelResistance_ITEM_LEN > 1
void GenEep_Get_FuelResistance(uint32_t * const FuelResistance );
void GenEep_Get_FuelResistance_Ex(uint8_t * const FuelResistance,uint16_t const offset,uint16_t const len );
void GenEep_Set_FuelResistance(uint32_t const* FuelResistance );
void GenEep_Set_FuelResistance_Ex(uint8_t const* FuelResistance,uint16_t const offset,uint16_t const len );
#else 
uint32_t GenEep_Get_FuelResistance(void);
void GenEep_Set_FuelResistance(uint32_t const FuelResistance);
#endif
uint32_t GenEep_Get_DTC_INFO_write_count(void);
#if EEP_CONTENT_dtc_example_ITEM_LEN > 1
void GenEep_Get_dtc_example(uint8_t * const dtc_example );
void GenEep_Get_dtc_example_Ex(uint8_t * const dtc_example,uint16_t const offset,uint16_t const len );
void GenEep_Set_dtc_example(uint8_t const* dtc_example );
void GenEep_Set_dtc_example_Ex(uint8_t const* dtc_example,uint16_t const offset,uint16_t const len );
#else 
uint8_t GenEep_Get_dtc_example(void);
void GenEep_Set_dtc_example(uint8_t const dtc_example);
#endif
uint32_t GenEep_Get_OdoInfo_write_count(void);
#if EEP_CONTENT_TotalOdo_ITEM_LEN > 1
void GenEep_Get_TotalOdo(uint32_t * const TotalOdo );
void GenEep_Get_TotalOdo_Ex(uint8_t * const TotalOdo,uint16_t const offset,uint16_t const len );
void GenEep_Set_TotalOdo(uint32_t const* TotalOdo );
void GenEep_Set_TotalOdo_Ex(uint8_t const* TotalOdo,uint16_t const offset,uint16_t const len );
#else 
uint32_t GenEep_Get_TotalOdo(void);
void GenEep_Set_TotalOdo(uint32_t const TotalOdo);
#endif
#if EEP_CONTENT_TotalTime_ITEM_LEN > 1
void GenEep_Get_TotalTime(uint32_t * const TotalTime );
void GenEep_Get_TotalTime_Ex(uint8_t * const TotalTime,uint16_t const offset,uint16_t const len );
void GenEep_Set_TotalTime(uint32_t const* TotalTime );
void GenEep_Set_TotalTime_Ex(uint8_t const* TotalTime,uint16_t const offset,uint16_t const len );
#else 
uint32_t GenEep_Get_TotalTime(void);
void GenEep_Set_TotalTime(uint32_t const TotalTime);
#endif
#if EEP_CONTENT_TravelTime_ITEM_LEN > 1
void GenEep_Get_TravelTime(uint32_t * const TravelTime );
void GenEep_Get_TravelTime_Ex(uint8_t * const TravelTime,uint16_t const offset,uint16_t const len );
void GenEep_Set_TravelTime(uint32_t const* TravelTime );
void GenEep_Set_TravelTime_Ex(uint8_t const* TravelTime,uint16_t const offset,uint16_t const len );
#else 
uint32_t GenEep_Get_TravelTime(void);
void GenEep_Set_TravelTime(uint32_t const TravelTime);
#endif
#if EEP_CONTENT_TravelOdo_ITEM_LEN > 1
void GenEep_Get_TravelOdo(uint32_t * const TravelOdo );
void GenEep_Get_TravelOdo_Ex(uint8_t * const TravelOdo,uint16_t const offset,uint16_t const len );
void GenEep_Set_TravelOdo(uint32_t const* TravelOdo );
void GenEep_Set_TravelOdo_Ex(uint8_t const* TravelOdo,uint16_t const offset,uint16_t const len );
#else 
uint32_t GenEep_Get_TravelOdo(void);
void GenEep_Set_TravelOdo(uint32_t const TravelOdo);
#endif
#if EEP_CONTENT_TripAMeter_ITEM_LEN > 1
void GenEep_Get_TripAMeter(uint32_t * const TripAMeter );
void GenEep_Get_TripAMeter_Ex(uint8_t * const TripAMeter,uint16_t const offset,uint16_t const len );
void GenEep_Set_TripAMeter(uint32_t const* TripAMeter );
void GenEep_Set_TripAMeter_Ex(uint8_t const* TripAMeter,uint16_t const offset,uint16_t const len );
#else 
uint32_t GenEep_Get_TripAMeter(void);
void GenEep_Set_TripAMeter(uint32_t const TripAMeter);
#endif
#if EEP_CONTENT_TripATime_ITEM_LEN > 1
void GenEep_Get_TripATime(uint32_t * const TripATime );
void GenEep_Get_TripATime_Ex(uint8_t * const TripATime,uint16_t const offset,uint16_t const len );
void GenEep_Set_TripATime(uint32_t const* TripATime );
void GenEep_Set_TripATime_Ex(uint8_t const* TripATime,uint16_t const offset,uint16_t const len );
#else 
uint32_t GenEep_Get_TripATime(void);
void GenEep_Set_TripATime(uint32_t const TripATime);
#endif
#if EEP_CONTENT_TripBMeter_ITEM_LEN > 1
void GenEep_Get_TripBMeter(uint32_t * const TripBMeter );
void GenEep_Get_TripBMeter_Ex(uint8_t * const TripBMeter,uint16_t const offset,uint16_t const len );
void GenEep_Set_TripBMeter(uint32_t const* TripBMeter );
void GenEep_Set_TripBMeter_Ex(uint8_t const* TripBMeter,uint16_t const offset,uint16_t const len );
#else 
uint32_t GenEep_Get_TripBMeter(void);
void GenEep_Set_TripBMeter(uint32_t const TripBMeter);
#endif
#if EEP_CONTENT_TripBTime_ITEM_LEN > 1
void GenEep_Get_TripBTime(uint32_t * const TripBTime );
void GenEep_Get_TripBTime_Ex(uint8_t * const TripBTime,uint16_t const offset,uint16_t const len );
void GenEep_Set_TripBTime(uint32_t const* TripBTime );
void GenEep_Set_TripBTime_Ex(uint8_t const* TripBTime,uint16_t const offset,uint16_t const len );
#else 
uint32_t GenEep_Get_TripBTime(void);
void GenEep_Set_TripBTime(uint32_t const TripBTime);
#endif
#if EEP_CONTENT_CruiseDistance_ITEM_LEN > 1
void GenEep_Get_CruiseDistance(uint32_t * const CruiseDistance );
void GenEep_Get_CruiseDistance_Ex(uint8_t * const CruiseDistance,uint16_t const offset,uint16_t const len );
void GenEep_Set_CruiseDistance(uint32_t const* CruiseDistance );
void GenEep_Set_CruiseDistance_Ex(uint8_t const* CruiseDistance,uint16_t const offset,uint16_t const len );
#else 
uint32_t GenEep_Get_CruiseDistance(void);
void GenEep_Set_CruiseDistance(uint32_t const CruiseDistance);
#endif
uint32_t GenEep_Get_share_eep_write_count(void);
#if EEP_CONTENT_VipSwdlShareMem_ITEM_LEN > 1
void GenEep_Get_VipSwdlShareMem(uint8_t * const VipSwdlShareMem );
void GenEep_Get_VipSwdlShareMem_Ex(uint8_t * const VipSwdlShareMem,uint16_t const offset,uint16_t const len );
void GenEep_Set_VipSwdlShareMem(uint8_t const* VipSwdlShareMem );
void GenEep_Set_VipSwdlShareMem_Ex(uint8_t const* VipSwdlShareMem,uint16_t const offset,uint16_t const len );
#else 
uint8_t GenEep_Get_VipSwdlShareMem(void);
void GenEep_Set_VipSwdlShareMem(uint8_t const VipSwdlShareMem);
#endif
#if EEP_CONTENT_TotalImageSFailCnt_ITEM_LEN > 1
void GenEep_Get_TotalImageSFailCnt(uint32_t * const TotalImageSFailCnt );
void GenEep_Get_TotalImageSFailCnt_Ex(uint8_t * const TotalImageSFailCnt,uint16_t const offset,uint16_t const len );
void GenEep_Set_TotalImageSFailCnt(uint32_t const* TotalImageSFailCnt );
void GenEep_Set_TotalImageSFailCnt_Ex(uint8_t const* TotalImageSFailCnt,uint16_t const offset,uint16_t const len );
#else 
uint32_t GenEep_Get_TotalImageSFailCnt(void);
void GenEep_Set_TotalImageSFailCnt(uint32_t const TotalImageSFailCnt);
#endif
#if EEP_CONTENT_trace_phy_en_ITEM_LEN > 1
void GenEep_Get_trace_phy_en(uint16_t * const trace_phy_en );
void GenEep_Get_trace_phy_en_Ex(uint8_t * const trace_phy_en,uint16_t const offset,uint16_t const len );
void GenEep_Set_trace_phy_en(uint16_t const* trace_phy_en );
void GenEep_Set_trace_phy_en_Ex(uint8_t const* trace_phy_en,uint16_t const offset,uint16_t const len );
#else 
uint16_t GenEep_Get_trace_phy_en(void);
void GenEep_Set_trace_phy_en(uint16_t const trace_phy_en);
#endif
#if EEP_CONTENT_trace_phy_ITEM_LEN > 1
void GenEep_Get_trace_phy(uint8_t * const trace_phy );
void GenEep_Get_trace_phy_Ex(uint8_t * const trace_phy,uint16_t const offset,uint16_t const len );
void GenEep_Set_trace_phy(uint8_t const* trace_phy );
void GenEep_Set_trace_phy_Ex(uint8_t const* trace_phy,uint16_t const offset,uint16_t const len );
#else 
uint8_t GenEep_Get_trace_phy(void);
void GenEep_Set_trace_phy(uint8_t const trace_phy);
#endif
#if EEP_CONTENT_VipLogModLvl_ITEM_LEN > 1
void GenEep_Get_VipLogModLvl(uint8_t * const VipLogModLvl );
void GenEep_Get_VipLogModLvl_Ex(uint8_t * const VipLogModLvl,uint16_t const offset,uint16_t const len );
void GenEep_Set_VipLogModLvl(uint8_t const* VipLogModLvl );
void GenEep_Set_VipLogModLvl_Ex(uint8_t const* VipLogModLvl,uint16_t const offset,uint16_t const len );
#else 
uint8_t GenEep_Get_VipLogModLvl(void);
void GenEep_Set_VipLogModLvl(uint8_t const VipLogModLvl);
#endif
#if EEP_CONTENT_TraceModuleLvlCrc_ITEM_LEN > 1
void GenEep_Get_TraceModuleLvlCrc(uint16_t * const TraceModuleLvlCrc );
void GenEep_Get_TraceModuleLvlCrc_Ex(uint8_t * const TraceModuleLvlCrc,uint16_t const offset,uint16_t const len );
void GenEep_Set_TraceModuleLvlCrc(uint16_t const* TraceModuleLvlCrc );
void GenEep_Set_TraceModuleLvlCrc_Ex(uint8_t const* TraceModuleLvlCrc,uint16_t const offset,uint16_t const len );
#else 
uint16_t GenEep_Get_TraceModuleLvlCrc(void);
void GenEep_Set_TraceModuleLvlCrc(uint16_t const TraceModuleLvlCrc);
#endif
uint32_t GenEep_Get_ResetInfo_write_count(void);
#if EEP_CONTENT_TotalReset_Cnt_ITEM_LEN > 1
void GenEep_Get_TotalReset_Cnt(uint32_t * const TotalReset_Cnt );
void GenEep_Get_TotalReset_Cnt_Ex(uint8_t * const TotalReset_Cnt,uint16_t const offset,uint16_t const len );
void GenEep_Set_TotalReset_Cnt(uint32_t const* TotalReset_Cnt );
void GenEep_Set_TotalReset_Cnt_Ex(uint8_t const* TotalReset_Cnt,uint16_t const offset,uint16_t const len );
#else 
uint32_t GenEep_Get_TotalReset_Cnt(void);
void GenEep_Set_TotalReset_Cnt(uint32_t const TotalReset_Cnt);
#endif
#if EEP_CONTENT_DspPOR_cnt_ITEM_LEN > 1
void GenEep_Get_DspPOR_cnt(uint16_t * const DspPOR_cnt );
void GenEep_Get_DspPOR_cnt_Ex(uint8_t * const DspPOR_cnt,uint16_t const offset,uint16_t const len );
void GenEep_Set_DspPOR_cnt(uint16_t const* DspPOR_cnt );
void GenEep_Set_DspPOR_cnt_Ex(uint8_t const* DspPOR_cnt,uint16_t const offset,uint16_t const len );
#else 
uint16_t GenEep_Get_DspPOR_cnt(void);
void GenEep_Set_DspPOR_cnt(uint16_t const DspPOR_cnt);
#endif
#if EEP_CONTENT_AudioCmdExecMaxTm_ITEM_LEN > 1
void GenEep_Get_AudioCmdExecMaxTm(uint16_t * const AudioCmdExecMaxTm );
void GenEep_Get_AudioCmdExecMaxTm_Ex(uint8_t * const AudioCmdExecMaxTm,uint16_t const offset,uint16_t const len );
void GenEep_Set_AudioCmdExecMaxTm(uint16_t const* AudioCmdExecMaxTm );
void GenEep_Set_AudioCmdExecMaxTm_Ex(uint8_t const* AudioCmdExecMaxTm,uint16_t const offset,uint16_t const len );
#else 
uint16_t GenEep_Get_AudioCmdExecMaxTm(void);
void GenEep_Set_AudioCmdExecMaxTm(uint16_t const AudioCmdExecMaxTm);
#endif
#if EEP_CONTENT_RadioCmdExecMaxTm_ITEM_LEN > 1
void GenEep_Get_RadioCmdExecMaxTm(uint16_t * const RadioCmdExecMaxTm );
void GenEep_Get_RadioCmdExecMaxTm_Ex(uint8_t * const RadioCmdExecMaxTm,uint16_t const offset,uint16_t const len );
void GenEep_Set_RadioCmdExecMaxTm(uint16_t const* RadioCmdExecMaxTm );
void GenEep_Set_RadioCmdExecMaxTm_Ex(uint8_t const* RadioCmdExecMaxTm,uint16_t const offset,uint16_t const len );
#else 
uint16_t GenEep_Get_RadioCmdExecMaxTm(void);
void GenEep_Set_RadioCmdExecMaxTm(uint16_t const RadioCmdExecMaxTm);
#endif
#if EEP_CONTENT_CpuDisIntMaxNest_ITEM_LEN > 1
void GenEep_Get_CpuDisIntMaxNest(uint16_t * const CpuDisIntMaxNest );
void GenEep_Get_CpuDisIntMaxNest_Ex(uint8_t * const CpuDisIntMaxNest,uint16_t const offset,uint16_t const len );
void GenEep_Set_CpuDisIntMaxNest(uint16_t const* CpuDisIntMaxNest );
void GenEep_Set_CpuDisIntMaxNest_Ex(uint8_t const* CpuDisIntMaxNest,uint16_t const offset,uint16_t const len );
#else 
uint16_t GenEep_Get_CpuDisIntMaxNest(void);
void GenEep_Set_CpuDisIntMaxNest(uint16_t const CpuDisIntMaxNest);
#endif
#if EEP_CONTENT_CpuDisIntMaxTm_ITEM_LEN > 1
void GenEep_Get_CpuDisIntMaxTm(uint16_t * const CpuDisIntMaxTm );
void GenEep_Get_CpuDisIntMaxTm_Ex(uint8_t * const CpuDisIntMaxTm,uint16_t const offset,uint16_t const len );
void GenEep_Set_CpuDisIntMaxTm(uint16_t const* CpuDisIntMaxTm );
void GenEep_Set_CpuDisIntMaxTm_Ex(uint8_t const* CpuDisIntMaxTm,uint16_t const offset,uint16_t const len );
#else 
uint16_t GenEep_Get_CpuDisIntMaxTm(void);
void GenEep_Set_CpuDisIntMaxTm(uint16_t const CpuDisIntMaxTm);
#endif
#if EEP_CONTENT_CpuDisIntTooLongCnt_ITEM_LEN > 1
void GenEep_Get_CpuDisIntTooLongCnt(uint16_t * const CpuDisIntTooLongCnt );
void GenEep_Get_CpuDisIntTooLongCnt_Ex(uint8_t * const CpuDisIntTooLongCnt,uint16_t const offset,uint16_t const len );
void GenEep_Set_CpuDisIntTooLongCnt(uint16_t const* CpuDisIntTooLongCnt );
void GenEep_Set_CpuDisIntTooLongCnt_Ex(uint8_t const* CpuDisIntTooLongCnt,uint16_t const offset,uint16_t const len );
#else 
uint16_t GenEep_Get_CpuDisIntTooLongCnt(void);
void GenEep_Set_CpuDisIntTooLongCnt(uint16_t const CpuDisIntTooLongCnt);
#endif
#if EEP_CONTENT_SocResetReason_ITEM_LEN > 1
void GenEep_Get_SocResetReason(uint8_t * const SocResetReason );
void GenEep_Get_SocResetReason_Ex(uint8_t * const SocResetReason,uint16_t const offset,uint16_t const len );
void GenEep_Set_SocResetReason(uint8_t const* SocResetReason );
void GenEep_Set_SocResetReason_Ex(uint8_t const* SocResetReason,uint16_t const offset,uint16_t const len );
#else 
uint8_t GenEep_Get_SocResetReason(void);
void GenEep_Set_SocResetReason(uint8_t const SocResetReason);
#endif


void GenEep_Get_ByOffset(uint16_t const offset, uint16_t const len,uint8_t *const data);
void GenEep_Set_ByOffset(uint16_t const offset, uint16_t const len,uint8_t const* data);


void EepMana_Init(void);
boolean EepMana_InitedComp(void);
void EepMana_Sync(void);
void EepMana_IdleTask(void);
// void EepMana_RxMsg(uint16_t const len,  uint8_t const*dat);
// void EepMana_OnOff(boolean const xon_off, uint8_t const new_client,uint8_t const old_client);
// uint16_t EepMana_SegTxStart(void);
// uint16_t EepMana_SegTxGetBuf(uint8_t *const buf, uint16_t const len);
// void EepMana_SegTxResult(TX_RESULE const result);

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#ifdef __cplusplus
}
#endif

#endif
/** >>>>>>>> end of file  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/




