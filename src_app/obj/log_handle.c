
#include "Trace_cfg.h"
#include "Trace_api.h"

//#include <stdio.h>           /* For sprintf() */
//#include <string.h>          /* for memset() */
#include "Trace_Common.h"
#include "Trace_Buffer.h"  /*For buffer management routines. */
#include "Trace_Server.h"
#include "Trace_Common.h"

#include "log_cfg.h"
#include "crc16.h"
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
//define CHECKSUM_INIT_VALUE (0xAA55)
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
/** current module-mask storing the state of all 16 modules */
//NO_INIT_DATA static uint32_t	Trace_ModuleMask_u32;
__no_init uint8_t MOD_ALWAYS_Lvl;
__no_init uint8_t MOD_OS_Lvl;
__no_init uint8_t MOD_HW_Lvl;
__no_init uint8_t MOD_CAN_Lvl;
__no_init uint8_t MOD_DIAG_Lvl;
__no_init uint8_t MOD_EEPROM_Lvl;
__no_init uint8_t MOD_DEV_IPC_SPI_0_Lvl;
__no_init uint8_t MOD_DEV_IPC_IIC_IC_Lvl;
__no_init uint8_t MOD_GPS_Lvl;
__no_init uint8_t MOD_AUDIO_Lvl;
__no_init uint8_t MOD_CF_KERNEL_Lvl;
__no_init uint8_t MOD_CF_STUB_Lvl;
__no_init uint8_t MOD_CF_PROXY_Lvl;
__no_init uint8_t MOD_SWDL_Lvl;
__no_init uint8_t MOD_METER_GAUGES_Lvl;
__no_init uint8_t MOD_METER_OBC_Lvl;
__no_init uint8_t MOD_METER_WARNING_Lvl;
__no_init uint8_t MOD_METER_ILL_Lvl;
__no_init uint8_t MOD_METER_TELLTALE_Lvl;
__no_init uint8_t MOD_METER_BUZZER_Lvl;
__no_init uint8_t MOD_METER_OUTPUT_Lvl;
__no_init uint8_t MOD_IPC_DEV_SIG_Lvl;
__no_init uint8_t MOD_IC_DEV_SIG_Lvl;
__no_init uint8_t MOD_METER_SWDL_Lvl;
__no_init uint8_t MOD_HOST_I2C_Lvl;
__no_init uint8_t MOD_TFT_ADC_Lvl;
__no_init uint8_t MOD_TOUCH_IIC_Lvl;

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void LogHandle_Init(void)
{
	MOD_ALWAYS_Lvl=MOD_ALWAYS_DefaultLevel;
	MOD_OS_Lvl=MOD_OS_DefaultLevel;
	MOD_HW_Lvl=MOD_HW_DefaultLevel;
	MOD_CAN_Lvl=MOD_CAN_DefaultLevel;
	MOD_DIAG_Lvl=MOD_DIAG_DefaultLevel;
	MOD_EEPROM_Lvl=MOD_EEPROM_DefaultLevel;
	MOD_DEV_IPC_SPI_0_Lvl=MOD_DEV_IPC_SPI_0_DefaultLevel;
	MOD_DEV_IPC_IIC_IC_Lvl=MOD_DEV_IPC_IIC_IC_DefaultLevel;
	MOD_GPS_Lvl=MOD_GPS_DefaultLevel;
	MOD_AUDIO_Lvl=MOD_AUDIO_DefaultLevel;
	MOD_CF_KERNEL_Lvl=MOD_CF_KERNEL_DefaultLevel;
	MOD_CF_STUB_Lvl=MOD_CF_STUB_DefaultLevel;
	MOD_CF_PROXY_Lvl=MOD_CF_PROXY_DefaultLevel;
	MOD_SWDL_Lvl=MOD_SWDL_DefaultLevel;
	MOD_METER_GAUGES_Lvl=MOD_METER_GAUGES_DefaultLevel;
	MOD_METER_OBC_Lvl=MOD_METER_OBC_DefaultLevel;
	MOD_METER_WARNING_Lvl=MOD_METER_WARNING_DefaultLevel;
	MOD_METER_ILL_Lvl=MOD_METER_ILL_DefaultLevel;
	MOD_METER_TELLTALE_Lvl=MOD_METER_TELLTALE_DefaultLevel;
	MOD_METER_BUZZER_Lvl=MOD_METER_BUZZER_DefaultLevel;
	MOD_METER_OUTPUT_Lvl=MOD_METER_OUTPUT_DefaultLevel;
	MOD_IPC_DEV_SIG_Lvl=MOD_IPC_DEV_SIG_DefaultLevel;
	MOD_IC_DEV_SIG_Lvl=MOD_IC_DEV_SIG_DefaultLevel;
	MOD_METER_SWDL_Lvl=MOD_METER_SWDL_DefaultLevel;
	MOD_HOST_I2C_Lvl=MOD_HOST_I2C_DefaultLevel;
	MOD_TFT_ADC_Lvl=MOD_TFT_ADC_DefaultLevel;
	MOD_TOUCH_IIC_Lvl=MOD_TOUCH_IIC_DefaultLevel;
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void LogHandle_SetModuleLogLvl(uint8_t module_id, uint8_t log_lvl)
{
	// GET EEPROM
	uint8_t mod_lvl[32];
	LogHandle_GetEepModLvl(mod_lvl);
	
	if(module_id==MOD_ALWAYS/*&&MOD_ALWAYS_Lvl!=log_lvl*/){
		MOD_ALWAYS_Lvl=log_lvl; mod_lvl[MOD_ALWAYS]=log_lvl;
		TRACE_VALUE2(TRACE_SWITCH_ALWAYS, MOD_ALWAYS, " ----- Log-Info:SetModuleLogLvl(MOD_ALWAYS) module_id=%d,log_level=%d ...", module_id,log_lvl);
	}

	if(module_id==MOD_OS/*&&MOD_OS_Lvl!=log_lvl*/){
		MOD_OS_Lvl=log_lvl; mod_lvl[MOD_OS]=log_lvl;
		TRACE_VALUE2(TRACE_SWITCH_ALWAYS, MOD_ALWAYS, " ----- Log-Info:SetModuleLogLvl(MOD_OS) module_id=%d,log_level=%d ...", module_id,log_lvl);
	}

	if(module_id==MOD_HW/*&&MOD_HW_Lvl!=log_lvl*/){
		MOD_HW_Lvl=log_lvl; mod_lvl[MOD_HW]=log_lvl;
		TRACE_VALUE2(TRACE_SWITCH_ALWAYS, MOD_ALWAYS, " ----- Log-Info:SetModuleLogLvl(MOD_HW) module_id=%d,log_level=%d ...", module_id,log_lvl);
	}

	if(module_id==MOD_CAN/*&&MOD_CAN_Lvl!=log_lvl*/){
		MOD_CAN_Lvl=log_lvl; mod_lvl[MOD_CAN]=log_lvl;
		TRACE_VALUE2(TRACE_SWITCH_ALWAYS, MOD_ALWAYS, " ----- Log-Info:SetModuleLogLvl(MOD_CAN) module_id=%d,log_level=%d ...", module_id,log_lvl);
	}

	if(module_id==MOD_DIAG/*&&MOD_DIAG_Lvl!=log_lvl*/){
		MOD_DIAG_Lvl=log_lvl; mod_lvl[MOD_DIAG]=log_lvl;
		TRACE_VALUE2(TRACE_SWITCH_ALWAYS, MOD_ALWAYS, " ----- Log-Info:SetModuleLogLvl(MOD_DIAG) module_id=%d,log_level=%d ...", module_id,log_lvl);
	}

	if(module_id==MOD_EEPROM/*&&MOD_EEPROM_Lvl!=log_lvl*/){
		MOD_EEPROM_Lvl=log_lvl; mod_lvl[MOD_EEPROM]=log_lvl;
		TRACE_VALUE2(TRACE_SWITCH_ALWAYS, MOD_ALWAYS, " ----- Log-Info:SetModuleLogLvl(MOD_EEPROM) module_id=%d,log_level=%d ...", module_id,log_lvl);
	}

	if(module_id==MOD_DEV_IPC_SPI_0/*&&MOD_DEV_IPC_SPI_0_Lvl!=log_lvl*/){
		MOD_DEV_IPC_SPI_0_Lvl=log_lvl; mod_lvl[MOD_DEV_IPC_SPI_0]=log_lvl;
		TRACE_VALUE2(TRACE_SWITCH_ALWAYS, MOD_ALWAYS, " ----- Log-Info:SetModuleLogLvl(MOD_DEV_IPC_SPI_0) module_id=%d,log_level=%d ...", module_id,log_lvl);
	}

	if(module_id==MOD_DEV_IPC_IIC_IC/*&&MOD_DEV_IPC_IIC_IC_Lvl!=log_lvl*/){
		MOD_DEV_IPC_IIC_IC_Lvl=log_lvl; mod_lvl[MOD_DEV_IPC_IIC_IC]=log_lvl;
		TRACE_VALUE2(TRACE_SWITCH_ALWAYS, MOD_ALWAYS, " ----- Log-Info:SetModuleLogLvl(MOD_DEV_IPC_IIC_IC) module_id=%d,log_level=%d ...", module_id,log_lvl);
	}

	if(module_id==MOD_GPS/*&&MOD_GPS_Lvl!=log_lvl*/){
		MOD_GPS_Lvl=log_lvl; mod_lvl[MOD_GPS]=log_lvl;
		TRACE_VALUE2(TRACE_SWITCH_ALWAYS, MOD_ALWAYS, " ----- Log-Info:SetModuleLogLvl(MOD_GPS) module_id=%d,log_level=%d ...", module_id,log_lvl);
	}

	if(module_id==MOD_AUDIO/*&&MOD_AUDIO_Lvl!=log_lvl*/){
		MOD_AUDIO_Lvl=log_lvl; mod_lvl[MOD_AUDIO]=log_lvl;
		TRACE_VALUE2(TRACE_SWITCH_ALWAYS, MOD_ALWAYS, " ----- Log-Info:SetModuleLogLvl(MOD_AUDIO) module_id=%d,log_level=%d ...", module_id,log_lvl);
	}

	if(module_id==MOD_CF_KERNEL/*&&MOD_CF_KERNEL_Lvl!=log_lvl*/){
		MOD_CF_KERNEL_Lvl=log_lvl; mod_lvl[MOD_CF_KERNEL]=log_lvl;
		TRACE_VALUE2(TRACE_SWITCH_ALWAYS, MOD_ALWAYS, " ----- Log-Info:SetModuleLogLvl(MOD_CF_KERNEL) module_id=%d,log_level=%d ...", module_id,log_lvl);
	}

	if(module_id==MOD_CF_STUB/*&&MOD_CF_STUB_Lvl!=log_lvl*/){
		MOD_CF_STUB_Lvl=log_lvl; mod_lvl[MOD_CF_STUB]=log_lvl;
		TRACE_VALUE2(TRACE_SWITCH_ALWAYS, MOD_ALWAYS, " ----- Log-Info:SetModuleLogLvl(MOD_CF_STUB) module_id=%d,log_level=%d ...", module_id,log_lvl);
	}

	if(module_id==MOD_CF_PROXY/*&&MOD_CF_PROXY_Lvl!=log_lvl*/){
		MOD_CF_PROXY_Lvl=log_lvl; mod_lvl[MOD_CF_PROXY]=log_lvl;
		TRACE_VALUE2(TRACE_SWITCH_ALWAYS, MOD_ALWAYS, " ----- Log-Info:SetModuleLogLvl(MOD_CF_PROXY) module_id=%d,log_level=%d ...", module_id,log_lvl);
	}

	if(module_id==MOD_SWDL/*&&MOD_SWDL_Lvl!=log_lvl*/){
		MOD_SWDL_Lvl=log_lvl; mod_lvl[MOD_SWDL]=log_lvl;
		TRACE_VALUE2(TRACE_SWITCH_ALWAYS, MOD_ALWAYS, " ----- Log-Info:SetModuleLogLvl(MOD_SWDL) module_id=%d,log_level=%d ...", module_id,log_lvl);
	}

	if(module_id==MOD_METER_GAUGES/*&&MOD_METER_GAUGES_Lvl!=log_lvl*/){
		MOD_METER_GAUGES_Lvl=log_lvl; mod_lvl[MOD_METER_GAUGES]=log_lvl;
		TRACE_VALUE2(TRACE_SWITCH_ALWAYS, MOD_ALWAYS, " ----- Log-Info:SetModuleLogLvl(MOD_METER_GAUGES) module_id=%d,log_level=%d ...", module_id,log_lvl);
	}

	if(module_id==MOD_METER_OBC/*&&MOD_METER_OBC_Lvl!=log_lvl*/){
		MOD_METER_OBC_Lvl=log_lvl; mod_lvl[MOD_METER_OBC]=log_lvl;
		TRACE_VALUE2(TRACE_SWITCH_ALWAYS, MOD_ALWAYS, " ----- Log-Info:SetModuleLogLvl(MOD_METER_OBC) module_id=%d,log_level=%d ...", module_id,log_lvl);
	}

	if(module_id==MOD_METER_WARNING/*&&MOD_METER_WARNING_Lvl!=log_lvl*/){
		MOD_METER_WARNING_Lvl=log_lvl; mod_lvl[MOD_METER_WARNING]=log_lvl;
		TRACE_VALUE2(TRACE_SWITCH_ALWAYS, MOD_ALWAYS, " ----- Log-Info:SetModuleLogLvl(MOD_METER_WARNING) module_id=%d,log_level=%d ...", module_id,log_lvl);
	}

	if(module_id==MOD_METER_ILL/*&&MOD_METER_ILL_Lvl!=log_lvl*/){
		MOD_METER_ILL_Lvl=log_lvl; mod_lvl[MOD_METER_ILL]=log_lvl;
		TRACE_VALUE2(TRACE_SWITCH_ALWAYS, MOD_ALWAYS, " ----- Log-Info:SetModuleLogLvl(MOD_METER_ILL) module_id=%d,log_level=%d ...", module_id,log_lvl);
	}

	if(module_id==MOD_METER_TELLTALE/*&&MOD_METER_TELLTALE_Lvl!=log_lvl*/){
		MOD_METER_TELLTALE_Lvl=log_lvl; mod_lvl[MOD_METER_TELLTALE]=log_lvl;
		TRACE_VALUE2(TRACE_SWITCH_ALWAYS, MOD_ALWAYS, " ----- Log-Info:SetModuleLogLvl(MOD_METER_TELLTALE) module_id=%d,log_level=%d ...", module_id,log_lvl);
	}

	if(module_id==MOD_METER_BUZZER/*&&MOD_METER_BUZZER_Lvl!=log_lvl*/){
		MOD_METER_BUZZER_Lvl=log_lvl; mod_lvl[MOD_METER_BUZZER]=log_lvl;
		TRACE_VALUE2(TRACE_SWITCH_ALWAYS, MOD_ALWAYS, " ----- Log-Info:SetModuleLogLvl(MOD_METER_BUZZER) module_id=%d,log_level=%d ...", module_id,log_lvl);
	}

	if(module_id==MOD_METER_OUTPUT/*&&MOD_METER_OUTPUT_Lvl!=log_lvl*/){
		MOD_METER_OUTPUT_Lvl=log_lvl; mod_lvl[MOD_METER_OUTPUT]=log_lvl;
		TRACE_VALUE2(TRACE_SWITCH_ALWAYS, MOD_ALWAYS, " ----- Log-Info:SetModuleLogLvl(MOD_METER_OUTPUT) module_id=%d,log_level=%d ...", module_id,log_lvl);
	}

	if(module_id==MOD_IPC_DEV_SIG/*&&MOD_IPC_DEV_SIG_Lvl!=log_lvl*/){
		MOD_IPC_DEV_SIG_Lvl=log_lvl; mod_lvl[MOD_IPC_DEV_SIG]=log_lvl;
		TRACE_VALUE2(TRACE_SWITCH_ALWAYS, MOD_ALWAYS, " ----- Log-Info:SetModuleLogLvl(MOD_IPC_DEV_SIG) module_id=%d,log_level=%d ...", module_id,log_lvl);
	}

	if(module_id==MOD_IC_DEV_SIG/*&&MOD_IC_DEV_SIG_Lvl!=log_lvl*/){
		MOD_IC_DEV_SIG_Lvl=log_lvl; mod_lvl[MOD_IC_DEV_SIG]=log_lvl;
		TRACE_VALUE2(TRACE_SWITCH_ALWAYS, MOD_ALWAYS, " ----- Log-Info:SetModuleLogLvl(MOD_IC_DEV_SIG) module_id=%d,log_level=%d ...", module_id,log_lvl);
	}

	if(module_id==MOD_METER_SWDL/*&&MOD_METER_SWDL_Lvl!=log_lvl*/){
		MOD_METER_SWDL_Lvl=log_lvl; mod_lvl[MOD_METER_SWDL]=log_lvl;
		TRACE_VALUE2(TRACE_SWITCH_ALWAYS, MOD_ALWAYS, " ----- Log-Info:SetModuleLogLvl(MOD_METER_SWDL) module_id=%d,log_level=%d ...", module_id,log_lvl);
	}

	if(module_id==MOD_HOST_I2C/*&&MOD_HOST_I2C_Lvl!=log_lvl*/){
		MOD_HOST_I2C_Lvl=log_lvl; mod_lvl[MOD_HOST_I2C]=log_lvl;
		TRACE_VALUE2(TRACE_SWITCH_ALWAYS, MOD_ALWAYS, " ----- Log-Info:SetModuleLogLvl(MOD_HOST_I2C) module_id=%d,log_level=%d ...", module_id,log_lvl);
	}

	if(module_id==MOD_TFT_ADC/*&&MOD_TFT_ADC_Lvl!=log_lvl*/){
		MOD_TFT_ADC_Lvl=log_lvl; mod_lvl[MOD_TFT_ADC]=log_lvl;
		TRACE_VALUE2(TRACE_SWITCH_ALWAYS, MOD_ALWAYS, " ----- Log-Info:SetModuleLogLvl(MOD_TFT_ADC) module_id=%d,log_level=%d ...", module_id,log_lvl);
	}

	if(module_id==MOD_TOUCH_IIC/*&&MOD_TOUCH_IIC_Lvl!=log_lvl*/){
		MOD_TOUCH_IIC_Lvl=log_lvl; mod_lvl[MOD_TOUCH_IIC]=log_lvl;
		TRACE_VALUE2(TRACE_SWITCH_ALWAYS, MOD_ALWAYS, " ----- Log-Info:SetModuleLogLvl(MOD_TOUCH_IIC) module_id=%d,log_level=%d ...", module_id,log_lvl);
	}

	
	// SET EEPROM
	LogHandle_SetEepModLvl(mod_lvl);
	//uint16_t crc = CHECKSUM_INIT_VALUE;
	//for ( uint8_t i = 0; i < 32; i ++ )
	//{
	//	crc += mod_lvl[i];
	//}
	uint16_t crc = Crc16_Calc(mod_lvl,32);
	LogHandle_SetEepModLvlCrc(crc);
}


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void LogHandle_OutputAllModLvl(void)
{
	TRACE_VALUE2(LOG_SHOUT, MOD_ALWAYS, " ----- Log-Info:OutputModuleLogLvl(MOD_ALWAYS) module_id=%d,log_level=%d ...", MOD_ALWAYS,MOD_ALWAYS_Lvl);
	TRACE_VALUE2(LOG_SHOUT, MOD_ALWAYS, " ----- Log-Info:OutputModuleLogLvl(MOD_OS) module_id=%d,log_level=%d ...", MOD_OS,MOD_OS_Lvl);
	TRACE_VALUE2(LOG_SHOUT, MOD_ALWAYS, " ----- Log-Info:OutputModuleLogLvl(MOD_HW) module_id=%d,log_level=%d ...", MOD_HW,MOD_HW_Lvl);
	TRACE_VALUE2(LOG_SHOUT, MOD_ALWAYS, " ----- Log-Info:OutputModuleLogLvl(MOD_CAN) module_id=%d,log_level=%d ...", MOD_CAN,MOD_CAN_Lvl);
	TRACE_VALUE2(LOG_SHOUT, MOD_ALWAYS, " ----- Log-Info:OutputModuleLogLvl(MOD_DIAG) module_id=%d,log_level=%d ...", MOD_DIAG,MOD_DIAG_Lvl);
	TRACE_VALUE2(LOG_SHOUT, MOD_ALWAYS, " ----- Log-Info:OutputModuleLogLvl(MOD_EEPROM) module_id=%d,log_level=%d ...", MOD_EEPROM,MOD_EEPROM_Lvl);
	TRACE_VALUE2(LOG_SHOUT, MOD_ALWAYS, " ----- Log-Info:OutputModuleLogLvl(MOD_DEV_IPC_SPI_0) module_id=%d,log_level=%d ...", MOD_DEV_IPC_SPI_0,MOD_DEV_IPC_SPI_0_Lvl);
	TRACE_VALUE2(LOG_SHOUT, MOD_ALWAYS, " ----- Log-Info:OutputModuleLogLvl(MOD_DEV_IPC_IIC_IC) module_id=%d,log_level=%d ...", MOD_DEV_IPC_IIC_IC,MOD_DEV_IPC_IIC_IC_Lvl);
	TRACE_VALUE2(LOG_SHOUT, MOD_ALWAYS, " ----- Log-Info:OutputModuleLogLvl(MOD_GPS) module_id=%d,log_level=%d ...", MOD_GPS,MOD_GPS_Lvl);
	TRACE_VALUE2(LOG_SHOUT, MOD_ALWAYS, " ----- Log-Info:OutputModuleLogLvl(MOD_AUDIO) module_id=%d,log_level=%d ...", MOD_AUDIO,MOD_AUDIO_Lvl);
	TRACE_VALUE2(LOG_SHOUT, MOD_ALWAYS, " ----- Log-Info:OutputModuleLogLvl(MOD_CF_KERNEL) module_id=%d,log_level=%d ...", MOD_CF_KERNEL,MOD_CF_KERNEL_Lvl);
	TRACE_VALUE2(LOG_SHOUT, MOD_ALWAYS, " ----- Log-Info:OutputModuleLogLvl(MOD_CF_STUB) module_id=%d,log_level=%d ...", MOD_CF_STUB,MOD_CF_STUB_Lvl);
	TRACE_VALUE2(LOG_SHOUT, MOD_ALWAYS, " ----- Log-Info:OutputModuleLogLvl(MOD_CF_PROXY) module_id=%d,log_level=%d ...", MOD_CF_PROXY,MOD_CF_PROXY_Lvl);
	TRACE_VALUE2(LOG_SHOUT, MOD_ALWAYS, " ----- Log-Info:OutputModuleLogLvl(MOD_SWDL) module_id=%d,log_level=%d ...", MOD_SWDL,MOD_SWDL_Lvl);
	TRACE_VALUE2(LOG_SHOUT, MOD_ALWAYS, " ----- Log-Info:OutputModuleLogLvl(MOD_METER_GAUGES) module_id=%d,log_level=%d ...", MOD_METER_GAUGES,MOD_METER_GAUGES_Lvl);
	TRACE_VALUE2(LOG_SHOUT, MOD_ALWAYS, " ----- Log-Info:OutputModuleLogLvl(MOD_METER_OBC) module_id=%d,log_level=%d ...", MOD_METER_OBC,MOD_METER_OBC_Lvl);
	TRACE_VALUE2(LOG_SHOUT, MOD_ALWAYS, " ----- Log-Info:OutputModuleLogLvl(MOD_METER_WARNING) module_id=%d,log_level=%d ...", MOD_METER_WARNING,MOD_METER_WARNING_Lvl);
	TRACE_VALUE2(LOG_SHOUT, MOD_ALWAYS, " ----- Log-Info:OutputModuleLogLvl(MOD_METER_ILL) module_id=%d,log_level=%d ...", MOD_METER_ILL,MOD_METER_ILL_Lvl);
	TRACE_VALUE2(LOG_SHOUT, MOD_ALWAYS, " ----- Log-Info:OutputModuleLogLvl(MOD_METER_TELLTALE) module_id=%d,log_level=%d ...", MOD_METER_TELLTALE,MOD_METER_TELLTALE_Lvl);
	TRACE_VALUE2(LOG_SHOUT, MOD_ALWAYS, " ----- Log-Info:OutputModuleLogLvl(MOD_METER_BUZZER) module_id=%d,log_level=%d ...", MOD_METER_BUZZER,MOD_METER_BUZZER_Lvl);
	TRACE_VALUE2(LOG_SHOUT, MOD_ALWAYS, " ----- Log-Info:OutputModuleLogLvl(MOD_METER_OUTPUT) module_id=%d,log_level=%d ...", MOD_METER_OUTPUT,MOD_METER_OUTPUT_Lvl);
	TRACE_VALUE2(LOG_SHOUT, MOD_ALWAYS, " ----- Log-Info:OutputModuleLogLvl(MOD_IPC_DEV_SIG) module_id=%d,log_level=%d ...", MOD_IPC_DEV_SIG,MOD_IPC_DEV_SIG_Lvl);
	TRACE_VALUE2(LOG_SHOUT, MOD_ALWAYS, " ----- Log-Info:OutputModuleLogLvl(MOD_IC_DEV_SIG) module_id=%d,log_level=%d ...", MOD_IC_DEV_SIG,MOD_IC_DEV_SIG_Lvl);
	TRACE_VALUE2(LOG_SHOUT, MOD_ALWAYS, " ----- Log-Info:OutputModuleLogLvl(MOD_METER_SWDL) module_id=%d,log_level=%d ...", MOD_METER_SWDL,MOD_METER_SWDL_Lvl);
	TRACE_VALUE2(LOG_SHOUT, MOD_ALWAYS, " ----- Log-Info:OutputModuleLogLvl(MOD_HOST_I2C) module_id=%d,log_level=%d ...", MOD_HOST_I2C,MOD_HOST_I2C_Lvl);
	TRACE_VALUE2(LOG_SHOUT, MOD_ALWAYS, " ----- Log-Info:OutputModuleLogLvl(MOD_TFT_ADC) module_id=%d,log_level=%d ...", MOD_TFT_ADC,MOD_TFT_ADC_Lvl);
	TRACE_VALUE2(LOG_SHOUT, MOD_ALWAYS, " ----- Log-Info:OutputModuleLogLvl(MOD_TOUCH_IIC) module_id=%d,log_level=%d ...", MOD_TOUCH_IIC,MOD_TOUCH_IIC_Lvl);
	
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if LOG_EEP_MOD_LVL_AVBL
void LogHandle_RestoreModuleLogLvlFromEep(void)
{
	uint8_t mod_lvl[32];
	
	LogHandle_GetEepModLvl(mod_lvl);
	
	//uint16_t crc = CHECKSUM_INIT_VALUE;//GenEep_Get_TraceModuleLvlCrc();	
	//for ( uint8_t i = 0; i < 32; i ++ )
	//{
	//	crc += mod_lvl[i];
	//}
	uint16_t crc = Crc16_Calc(mod_lvl,32);
	if ( crc ==  LogHandle_GetEepModLvlCrc() )
	{
		for ( uint8_t i = 0; i < LOG_MODULE_NAME_MAX; i ++ )
		{
			LogHandle_SetModuleLogLvl(i,mod_lvl[i]);
		}		
	}
	else
	{	// calc crc for module log level
		MOD_ALWAYS_Lvl=MOD_ALWAYS_DefaultLevel;
		mod_lvl[MOD_ALWAYS]=MOD_ALWAYS_Lvl;
		MOD_OS_Lvl=MOD_OS_DefaultLevel;
		mod_lvl[MOD_OS]=MOD_OS_Lvl;
		MOD_HW_Lvl=MOD_HW_DefaultLevel;
		mod_lvl[MOD_HW]=MOD_HW_Lvl;
		MOD_CAN_Lvl=MOD_CAN_DefaultLevel;
		mod_lvl[MOD_CAN]=MOD_CAN_Lvl;
		MOD_DIAG_Lvl=MOD_DIAG_DefaultLevel;
		mod_lvl[MOD_DIAG]=MOD_DIAG_Lvl;
		MOD_EEPROM_Lvl=MOD_EEPROM_DefaultLevel;
		mod_lvl[MOD_EEPROM]=MOD_EEPROM_Lvl;
		MOD_DEV_IPC_SPI_0_Lvl=MOD_DEV_IPC_SPI_0_DefaultLevel;
		mod_lvl[MOD_DEV_IPC_SPI_0]=MOD_DEV_IPC_SPI_0_Lvl;
		MOD_DEV_IPC_IIC_IC_Lvl=MOD_DEV_IPC_IIC_IC_DefaultLevel;
		mod_lvl[MOD_DEV_IPC_IIC_IC]=MOD_DEV_IPC_IIC_IC_Lvl;
		MOD_GPS_Lvl=MOD_GPS_DefaultLevel;
		mod_lvl[MOD_GPS]=MOD_GPS_Lvl;
		MOD_AUDIO_Lvl=MOD_AUDIO_DefaultLevel;
		mod_lvl[MOD_AUDIO]=MOD_AUDIO_Lvl;
		MOD_CF_KERNEL_Lvl=MOD_CF_KERNEL_DefaultLevel;
		mod_lvl[MOD_CF_KERNEL]=MOD_CF_KERNEL_Lvl;
		MOD_CF_STUB_Lvl=MOD_CF_STUB_DefaultLevel;
		mod_lvl[MOD_CF_STUB]=MOD_CF_STUB_Lvl;
		MOD_CF_PROXY_Lvl=MOD_CF_PROXY_DefaultLevel;
		mod_lvl[MOD_CF_PROXY]=MOD_CF_PROXY_Lvl;
		MOD_SWDL_Lvl=MOD_SWDL_DefaultLevel;
		mod_lvl[MOD_SWDL]=MOD_SWDL_Lvl;
		MOD_METER_GAUGES_Lvl=MOD_METER_GAUGES_DefaultLevel;
		mod_lvl[MOD_METER_GAUGES]=MOD_METER_GAUGES_Lvl;
		MOD_METER_OBC_Lvl=MOD_METER_OBC_DefaultLevel;
		mod_lvl[MOD_METER_OBC]=MOD_METER_OBC_Lvl;
		MOD_METER_WARNING_Lvl=MOD_METER_WARNING_DefaultLevel;
		mod_lvl[MOD_METER_WARNING]=MOD_METER_WARNING_Lvl;
		MOD_METER_ILL_Lvl=MOD_METER_ILL_DefaultLevel;
		mod_lvl[MOD_METER_ILL]=MOD_METER_ILL_Lvl;
		MOD_METER_TELLTALE_Lvl=MOD_METER_TELLTALE_DefaultLevel;
		mod_lvl[MOD_METER_TELLTALE]=MOD_METER_TELLTALE_Lvl;
		MOD_METER_BUZZER_Lvl=MOD_METER_BUZZER_DefaultLevel;
		mod_lvl[MOD_METER_BUZZER]=MOD_METER_BUZZER_Lvl;
		MOD_METER_OUTPUT_Lvl=MOD_METER_OUTPUT_DefaultLevel;
		mod_lvl[MOD_METER_OUTPUT]=MOD_METER_OUTPUT_Lvl;
		MOD_IPC_DEV_SIG_Lvl=MOD_IPC_DEV_SIG_DefaultLevel;
		mod_lvl[MOD_IPC_DEV_SIG]=MOD_IPC_DEV_SIG_Lvl;
		MOD_IC_DEV_SIG_Lvl=MOD_IC_DEV_SIG_DefaultLevel;
		mod_lvl[MOD_IC_DEV_SIG]=MOD_IC_DEV_SIG_Lvl;
		MOD_METER_SWDL_Lvl=MOD_METER_SWDL_DefaultLevel;
		mod_lvl[MOD_METER_SWDL]=MOD_METER_SWDL_Lvl;
		MOD_HOST_I2C_Lvl=MOD_HOST_I2C_DefaultLevel;
		mod_lvl[MOD_HOST_I2C]=MOD_HOST_I2C_Lvl;
		MOD_TFT_ADC_Lvl=MOD_TFT_ADC_DefaultLevel;
		mod_lvl[MOD_TFT_ADC]=MOD_TFT_ADC_Lvl;
		MOD_TOUCH_IIC_Lvl=MOD_TOUCH_IIC_DefaultLevel;
		mod_lvl[MOD_TOUCH_IIC]=MOD_TOUCH_IIC_Lvl;

		//crc = CHECKSUM_INIT_VALUE;
		//for ( uint8_t i = 0; i < 32; i ++ )
		//{
		//	crc += mod_lvl[i];
		//}
		crc = Crc16_Calc(mod_lvl,32);
		/* write modlule log to eeprom */
		LogHandle_SetEepModLvl(mod_lvl);
		LogHandle_SetEepModLvlCrc(crc);
	}
}
#endif

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
/***********************************************************************************************/
/* Function Name   :    SetModuleMask(eTR_MODULE_SET_TYPE setType, uint32_t ModuleMask)          */
/* Description     :    This function sets the Module-mask.                                    */
/***********************************************************************************************/
/**  @brief  This function sets the Module-mask.
 *   @param  setType The type of module-mask set-operation
 *   @param  ModuleMask The module-mask to set
 */
void LogHandle_SetModuleMask(eTR_MODULE_SET_TYPE setType, uint32_t ModuleMask)
{
    // check, what type of set should be performed...
    switch(setType)
    {
    case NORMAL_SET:	// regular module mask set.
        /* Ensure that MOD_ALWAYS is always enabled.*/
        ModuleMask         |= (uint32_t)0x00000001;
		for ( uint8_t i = 0; i < LOG_MODULE_NAME_MAX; i ++ )
		{
			if ( ModuleMask & (1u<<i) ){
				LogHandle_SetModuleLogLvl(i,1);
			}else{
				LogHandle_SetModuleLogLvl(i,0);
			}
		}
		break;
   
    case DISABLE_ALL: // all modules should be disabled.
		for ( uint8_t i = 0; i < LOG_MODULE_NAME_MAX; i ++ )
		{
			LogHandle_SetModuleLogLvl(i,0);
		}		
		break;
		
    default:
		break;
    }
}

#if 0
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void 	SetModuleInitialMask(void)
{
	SetModuleMask(NORMAL_SET, Trace_ModuleMask_u32);
}

/***********************************************************************************************/
/* Function Name   :    Trace_GetConfiguredModuleMask(void)                                    */
/* Description     :    This function returns the default Module-mask as configured in         */
/*                      Tr_Modules.cfg.                                                        */
/***********************************************************************************************/
/**  @brief  This function returns the default Module-mask as configured in Tr_Modules.cfg.
 */
uint32_t Trace_GetConfiguredModuleMask(void)
{
    uint32_t returnValue = 0;
    uint32_t bitPos = 0x01;

#define TRACE_MODULE(ModuleNumber, DefaultValue)         \
      if( DefaultValue )     {returnValue |= bitPos;}       \
      TRACE_MODULE_##ModuleNumber##_bit = DefaultValue;     \
      bitPos = bitPos << 1;
#include <Trace_Defines.h>
#undef  TRACE_MODULE

    return(returnValue);
}

#endif

static uint8 ProcGetModuleLvl(uint8 module_id)
{
	uint8 log_lvl = 0x00;
	
	if(module_id==MOD_ALWAYS){
		log_lvl=MOD_ALWAYS_Lvl;
	}

	if(module_id==MOD_OS){
		log_lvl=MOD_OS_Lvl;
	}

	if(module_id==MOD_HW){
		log_lvl=MOD_HW_Lvl;
	}

	if(module_id==MOD_CAN){
		log_lvl=MOD_CAN_Lvl;
	}

	if(module_id==MOD_DIAG){
		log_lvl=MOD_DIAG_Lvl;
	}

	if(module_id==MOD_EEPROM){
		log_lvl=MOD_EEPROM_Lvl;
	}

	if(module_id==MOD_DEV_IPC_SPI_0){
		log_lvl=MOD_DEV_IPC_SPI_0_Lvl;
	}

	if(module_id==MOD_DEV_IPC_IIC_IC){
		log_lvl=MOD_DEV_IPC_IIC_IC_Lvl;
	}

	if(module_id==MOD_GPS){
		log_lvl=MOD_GPS_Lvl;
	}

	if(module_id==MOD_AUDIO){
		log_lvl=MOD_AUDIO_Lvl;
	}

	if(module_id==MOD_CF_KERNEL){
		log_lvl=MOD_CF_KERNEL_Lvl;
	}

	if(module_id==MOD_CF_STUB){
		log_lvl=MOD_CF_STUB_Lvl;
	}

	if(module_id==MOD_CF_PROXY){
		log_lvl=MOD_CF_PROXY_Lvl;
	}

	if(module_id==MOD_SWDL){
		log_lvl=MOD_SWDL_Lvl;
	}

	if(module_id==MOD_METER_GAUGES){
		log_lvl=MOD_METER_GAUGES_Lvl;
	}

	if(module_id==MOD_METER_OBC){
		log_lvl=MOD_METER_OBC_Lvl;
	}

	if(module_id==MOD_METER_WARNING){
		log_lvl=MOD_METER_WARNING_Lvl;
	}

	if(module_id==MOD_METER_ILL){
		log_lvl=MOD_METER_ILL_Lvl;
	}

	if(module_id==MOD_METER_TELLTALE){
		log_lvl=MOD_METER_TELLTALE_Lvl;
	}

	if(module_id==MOD_METER_BUZZER){
		log_lvl=MOD_METER_BUZZER_Lvl;
	}

	if(module_id==MOD_METER_OUTPUT){
		log_lvl=MOD_METER_OUTPUT_Lvl;
	}

	if(module_id==MOD_IPC_DEV_SIG){
		log_lvl=MOD_IPC_DEV_SIG_Lvl;
	}

	if(module_id==MOD_IC_DEV_SIG){
		log_lvl=MOD_IC_DEV_SIG_Lvl;
	}

	if(module_id==MOD_METER_SWDL){
		log_lvl=MOD_METER_SWDL_Lvl;
	}

	if(module_id==MOD_HOST_I2C){
		log_lvl=MOD_HOST_I2C_Lvl;
	}

	if(module_id==MOD_TFT_ADC){
		log_lvl=MOD_TFT_ADC_Lvl;
	}

	if(module_id==MOD_TOUCH_IIC){
		log_lvl=MOD_TOUCH_IIC_Lvl;
	}

	
	
	return log_lvl;
}

void Trace_GetModuleLvl(uint8 channel,uint8 module_id)
{
	TRACE_FRAME tr_frame;

    tr_frame.trRawData[0] = TRACE_MODULE_MASK_HEADER;
	tr_frame.trRawData[1] = 0x00;
	tr_frame.trRawData[2] = channel;
	tr_frame.trRawData[3] = 0x01;
	tr_frame.trRawData[4] = 0x02;
	tr_frame.trRawData[5] = 0x00;
	tr_frame.trRawData[6] = module_id;
	tr_frame.trRawData[7] = ProcGetModuleLvl(module_id);
	
    TraceBuffer_AddFrame(&tr_frame);	
}

/***********************************************************************************************/
/* Function Name   :    Trace_Text(uint16_t TraceID_u16)                                         */
/* Description     :    Call this function to log a simple text message.                       */
/***********************************************************************************************/
/**  @brief  Call this function to log a simple text message.
 *   @param  TraceID_u16 The Trace-ID for the text to trace. This ID is generated automatocally with the compilation process.
 */
void Trace_Text(uint16_t TraceID_u16)
{
	Trace_TraceMacroCalledCounter++;
	
    //TRACE_FRAME trFrame;
	TRACE_FRAME tr_frame;

    tr_frame.trMSG.Trace_Header_u8		= TRACE_MESSAGE_HEADER;
    tr_frame.trMSG.TraceID_u16			= TraceID_u16;
	*((uint32_t*)(tr_frame.trMSG.Val))	= 0x00;
    TraceBuffer_AddFrame(&tr_frame);
}



/***********************************************************************************************/
/* Function Name   :  Trace_Value(uint16_t TraceID_u16, uint32_t Val_u32)                          */
/* Description     :  This function may be called when a text message ALONG WITH A 32 bit-VALUE*/
/*                    needs to be logged.                                                      */
/***********************************************************************************************/
/**  @brief  This function may be called when a text message ALONG WITH A 32 bit-VALUE needs to be logged.
 *   @param  TraceID_u16 The Trace-ID for the text to trace. This ID is generated automatocally with the compilation process.
 *   @param  Val_u32 The 32-bit value to trace.
 */
void Trace_Value(uint16_t TraceID_u16,
                 uint32_t Val_u32)
{
    //TRACE_FRAME trFrame;
    Trace_TraceMacroCalledCounter++;

    /* Check, if we have to wait until a client is connected */
    TRACE_FRAME tr_frame;

    tr_frame.trMSG.Trace_Header_u8       = TRACE_MESSAGE_HEADER;
    tr_frame.trMSG.TraceID_u16            = TraceID_u16;
    *((uint32_t*)(tr_frame.trMSG.Val))    = Val_u32;

    TraceBuffer_AddFrame(&tr_frame);
}

/************************************************************************************************/
/* Function Name    :  Trace_Value2(uint16_t TraceID_u16,   uint16_t Val1_u16,   uint16_t Val2_u16)   */
/* Description      :  Call this function to log a text message ALONG WITH 2 16 bit-VALUES      */
/************************************************************************************************/
/**  @brief  Call this function to log a text message ALONG WITH 2 16 bit-VALUES
 *   @param  TraceID_u16 The Trace-ID for the text to trace. This ID is generated automatocally with the compilation process.
 *   @param  Val1_u16 The first 16-bit value to trace.
 *   @param  Val2_u16 The second 16-bit value to trace.
 */
void Trace_Value2(uint16_t TraceID_u16,
                  uint16_t Val1_u16,
                  uint16_t Val2_u16)
{
    Trace_TraceMacroCalledCounter++;

    TRACE_FRAME tr_frame;
    tr_frame.trMSG.Trace_Header_u8       	= TRACE_MESSAGE_HEADER;
    tr_frame.trMSG.TraceID_u16            	= TraceID_u16;
    *((uint16_t*)(tr_frame.trMSG.Val)  )  	= Val1_u16;
    *((uint16_t*)((tr_frame.trMSG.Val)+2))	= Val2_u16;

    TraceBuffer_AddFrame(&tr_frame);
}




/***********************************************************************************************/
/* Function Name     :  Trace_Value4( uint16_t TraceID_u16, uint8_t Val1_u8,                       */
/*                                   uint8_t Val2_u8,       uint8_t Val3_u8,      uint8_t Val4_u8)   */
/* Description       :  Call this function to log a text message ALONG WITH 4 8 bit-VALUES     */
/***********************************************************************************************/
/**  @brief  Call this function to log a text message ALONG WITH 4 8 bit-VALUES
 *   @param  TraceID_u16 The Trace-ID for the text to trace. This ID is generated automatocally with the compilation process.
 *   @param  Val1_u8 The first 8-bit value to trace.
 *   @param  Val2_u8 The second 8-bit value to trace.
 *   @param  Val3_u8 The third 8-bit value to trace.
 *   @param  Val4_u8 The fourth 8-bit value to trace.
 */
void Trace_Value4(uint16_t TraceID_u16,
                  uint8_t  Val1_u8,
                  uint8_t  Val2_u8,
                  uint8_t  Val3_u8,
                  uint8_t  Val4_u8)
{
    //TRACE_FRAME trFrame;
    Trace_TraceMacroCalledCounter++;

    TRACE_FRAME tr_frame;
    tr_frame.trMSG.Trace_Header_u8	= TRACE_MESSAGE_HEADER;
    tr_frame.trMSG.TraceID_u16		= TraceID_u16;
    tr_frame.trMSG.Val[0]			= Val1_u8;
    tr_frame.trMSG.Val[1] 			= Val2_u8;
    tr_frame.trMSG.Val[2]			= Val3_u8;
    tr_frame.trMSG.Val[3]			= Val4_u8;

    TraceBuffer_AddFrame(&tr_frame);

}



/************************************************************************************************/
/* Function Name     :  Trace_Mem( uint16_t TraceID_u16,   uint8_t Length,    char * dataPointer     )      */
/* Description       :  Call this function to log complete block of memory                      */
/************************************************************************************************/
/**  @brief   Call this function to log complete block of memory
 *   @param  TraceID_u16 The Trace-ID for the text to trace. This ID is generated automatocally with the compilation process.
 *   @param  Length Number bytes to log
 *   @param  dataPointer Pointer to the start-address of of the memory-block to trace.
 */
void Trace_Mem(uint16_t  TraceID_u16,
               uint8_t   Length,
               const uint8_t *  dataPointer)
{
    uint8_t  noOfFrames;// = 0;
    uint8_t  DataID;
    
    Trace_TraceMacroCalledCounter++;
	
	if ( dataPointer != NULL && Length > 0 )
	{
		/* If more than 96 bytes should be traced with this TRACE_MEM call transfer only 96 */
		if ( Length > MAX_BYTES_PER_TRACE_MEM )             /* totally 16 * 6 = 96 byte can be transferred in one TRACE_MEM call*/
		{
			/* 16 Data-Frames may be transmitted in one TRACE_MEM call */
			TRACE_VALUE(TRACE_SWITCH_ALWAYS, MOD_ALWAYS, "################# TRACE_MEM or TRACE_STRING called with %d bytes - maximum 96 Bytes !! ", Length);
		}
		else
		{	
			
			/* Calculate how many Data-Frames have to be used for this TRACE_MEM call and how many bytes the last frame will contain */
			noOfFrames = (Length+BYTES_PER_DATA_FRAME-1) / BYTES_PER_DATA_FRAME ;       /* Calculate number of Trace-Data frames - One Data frame contains 6 bytes */

			TRACE_FRAME tr_frame;
			
			CPU_SR_ALLOC();		
			CPU_CRITICAL_ENTER();		// disable interrupt 
			
			if ( (noOfFrames + 1) <= TraceBuffer_GetFifoAvblNum() )
			{	// there is enouth buffer on trace buffer 
				/* Get the DataID, to identify the current data-transfer */
		        DataID = Trace_DataID_u8;
		        /* Increase the Data-ID, so that the next data-transfer used a new ID */
		        Trace_IncreaseDataIDBy1();

		        /* Write the content of the Server-Data-Frame which is transmitted first */
		        tr_frame.trServerData.Trace_Header_u8	= TRACE_SERVER_DATA_HEADER;
		        tr_frame.trServerData.TraceID_u16		= TraceID_u16;
		        tr_frame.trServerData.DataLength_u8		= Length;
		        tr_frame.trServerData.Trace_DataID_u8	= DataID;
		       	tr_frame.trRawData[6]         			= TRACE_CLEAR_LETTER;
		        tr_frame.trRawData[7]         			= TRACE_CLEAR_LETTER;

		        TraceBuffer_AddFrame(&tr_frame);

		        
				/* In this loop, one Data-Frame after another is filled with content */
				for ( uint8_t frameCount = 0 ; frameCount < noOfFrames ; frameCount++ )
				{
					tr_frame.trData.Trace_Header_u8  = (  TRACE_DATA_HEADER   | (frameCount & SEQUENCE_NUMBER_BIT_MASK ) );
					tr_frame.trData.Trace_DataID_u8 = DataID;
					
					tr_frame.trData.Val[0] = *dataPointer++;	// 6 byte per frame
					tr_frame.trData.Val[1] = *dataPointer++;
					tr_frame.trData.Val[2] = *dataPointer++;
					tr_frame.trData.Val[3] = *dataPointer++;
					tr_frame.trData.Val[4] = *dataPointer++;
					tr_frame.trData.Val[5] = *dataPointer++;

					TraceBuffer_AddFrame(&tr_frame);
				}
			
			}

			CPU_CRITICAL_EXIT();
		}
	}
}




/************************************************************************************************/
/* Function Name   :  Trace_String(uint16_t  TraceID_u16, const char* str)                        */
/* Description     :  Call this function to log information that consists of a format string    */
/*                      and a null terminated string of "printable" characters.                 */
/************************************************************************************************/
/**  @brief  Call this function to log information that consists of a format string and a null terminated string of "printable" characters.
 *   @param  TraceID_u16 The Trace-ID for the text to trace. This ID is generated automatocally with the compilation process.
 *   @param  str Pointer to the string to trace.
 */
void Trace_String(uint16_t  TraceID_u16,
                  const char* str)
{
    /*lint -save -e64*/ /* lint has a problem with signed/unsigned pointers*/
    Trace_Mem(TraceID_u16, strlen(str), (uint8_t*)str);	//lint !e734
    /*lint -restore */
    return;
}



/***********************************************************************************************/
/* Function Name   :  printf_(const char* str)                                                 */
/* Description     :  Transmit a string of characters.                                         */
/***********************************************************************************************/
/**  @brief  Transmit a string of characters.
 *   @param  str Pointer to the string to trace.
 */
void printf_(const char* str)
{
    /*lint -save -e64*/ /* lint has a problem with signed/unsigned pointers*/
    Trace_Mem(TRACE_ID_PRINTF, strlen((char*)str),(const uint8_t*) str);	//lint !e734
    /*lint -restore */
    return;
}




/***********************************************************************************************/
/* Function Name   :  printf_1(const char* str, unsigned long p1)                              */
/* Description     :  Transmit a formatted string with a value.                                */
/***********************************************************************************************/
/**  @brief  Transmit a formatted string with a value.
 *   @param  str Pointer to the string to trace.
 *   @param  p1 Pointer to the value to trace.
 */
void printf_1(const char* str, unsigned long p1)
{
    char  text[MAX_BYTES_PER_TRACE_MEM];

    sprintf(text, str, p1);
    Trace_Mem(TRACE_ID_PRINTF, strlen((char*)text),(const uint8_t*) text); //lint !e734
    /*lint -restore */
    return;
}




/**
 * @}
 */

