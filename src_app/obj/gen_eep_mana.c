

/*****this file is generate by perl********************************************/
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#include "std_type.h"
#include "gen_eep_mana.h"
#include "cpu.h"
#include "hal_eep.h"
#include "hal_dflash.h"
#include "crc32.h"
#include "trace_api.h"

#include "bat_mana.h"
#include "hal_os.h"

/*lint -e835 -e845*/
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#define TX_EEP_OFFSET_SIZE	64ul
#define OFFSET_FIELD_NUM	((sizeof(EEPROM_ST)+TX_EEP_OFFSET_SIZE-1)/TX_EEP_OFFSET_SIZE)
#define EEP_SIZE			(sizeof(EEPROM_ST))

#define OFFSET_FIELD_TBL_NUM		((OFFSET_FIELD_NUM+31)/32)

#define TRANS_WHOLE_EEP_CMD		(0x03)
#define TRANS_EEP_OFFSET_CMD	(0x04)
#define	WRITE_EEP_OFFSET_CMD	(0x05)
#define	TRANS_EEP_LIST_CMD		(0x20)
#define	WRITE_EEP_LIST_CMD		(0x21)
#define EEP_IPC_CMD_RESP		(0x80)

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
// typedef enum
// {
// 	EEP_IPC_IDLE,

// 	EEP_IPC_TX_WHOLE,
// 	EEP_IPC_TX_WHOLE_REQ,
	
// 	EEP_IPC_TX_BY_OFFSET,
// 	EEP_IPC_TX_BY_OFFSET_REQ,

// 	EEP_IPC_TX_EEP_BY_LIST,
// 	EEP_IPC_TX_EEP_BY_LIST_REQ
	
// }EEP_IPC_STATE;

typedef void (*EepCopyUserImageToHwImageFunc)(void);

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

//static uint8 eep_sector[EEP_SECTOR_SIZE];
static boolean			eep_init_comp;
// static uint8_t			eep_last_client;
static uint8_t 			process_block;				// if process block = 0xFF, eeprom is idle 
static uint8_t			wr_hw_try_cnt;
static uint16_t			wr_eep_back;
static uint16_t			wr_eep_index;
// static EEP_IPC_STATE	eep_ipc_state;	

static boolean			eep_write_enable;
// static boolean			eep_transfer_whole;
// static boolean			ipc_tx_cmd_header;
// static boolean			ipc_tx_sub_item_header;
// static uint16_t			ipc_tx_idx;
// static uint16_t  		ipc_tx_list;	// for list and offset


// static uint32_t			trans_eep_offset_req[OFFSET_FIELD_TBL_NUM];
// static uint32_t			trans_eep_offset_cur[OFFSET_FIELD_TBL_NUM];

// static uint32_t			write_eep_offset_req[OFFSET_FIELD_TBL_NUM];	// write to real eeprom by imx command


static uint16_t eep_write_block_req;
static uint32_t wr_eep_by_list_req[EEP_CONT_LIST_FLAG_ARRAY_NUM];
// static uint32_t tx_eep_by_list_to_imx_req[EEP_CONT_LIST_FLAG_ARRAY_NUM];
// static uint32_t tx_eep_by_list_to_imx_cur[EEP_CONT_LIST_FLAG_ARRAY_NUM];
static EEPROM_ST eep_hw_image; // write eep_ram_image->eep_hw_image, than eep_hw_image->real eeprom 
// eeprom ram image 
static EEPROM_ST eep_ram_image;

static uint32_t dflash_manufacture_current_write_addr;
static uint32_t dflash_development_nvm_current_write_addr;
static uint32_t dflash_OemSetting_current_write_addr;
static uint32_t dflash_user_setting_current_write_addr;
static uint32_t dflash_DTC_INFO_current_write_addr;
static uint32_t dflash_OdoInfo_current_write_addr;
static uint32_t dflash_share_eep_current_write_addr;
static uint32_t dflash_ResetInfo_current_write_addr;


static EEPROM_ST const dflt_eep_val=
{	/*lint -save -e651*/
	.Manuf_Visteon_Part_Number = { 0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF },
	.Manuf_EquippedPCB_VPN_Sub = { 0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF },
	.Manuf_EquippedPCB_VPN_Main = { 0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF },
	.Manuf_Product_Serial_Number = { 0xFF,0xFF,0xFF,0xFF,0xFF },
	.Manuf_EquippedPCB_Serial_Number_Sub = { 0xFF,0xFF,0xFF,0xFF,0xFF },
	.Manuf_EquippedPCB_Serial_Number_Main = { 0xFF,0xFF,0xFF,0xFF,0xFF },
	.NVM_Revision = 0xFF,
	.Manuf_SMD_Date1 = { 0xFF,0xFF,0xFF },
	.Manuf_SMD_Date2 = { 0xFF,0xFF,0xFF },
	.Manuf_Assembly_Date = { 0xFF,0xFF,0xFF },
	.Traceability_Data = { 0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00 },
	.VehicleManufacturerSparePartNumber = { 0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20 },
	.VehicleManufacturerSparePartNumber_Assembly = { 0x32,0x38,0x30,0x39,0x30,0x32,0x47,0x4C,0x30,0x41 },
	.Operational_Reference = { 0x32,0x38,0x30,0x39,0x30,0x32,0x47,0x4C,0x30,0x41 },
	.Supplier_Number = { 0x35,0x41,0x4D,0x30 },
	.ECU_Serial_Number = { 0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20 },
	.Manufacturing_Identification_Code = 0x80,
	.VDIAG = 0x50,
	.Config_EQ1 = 0x01,
	.Vehicle_Type = 0x01,
	.NAVI_ID = { 0x53,0x56,0x41,0x45,0x4E,0x41,0x56,0x49,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00 },
	.DA_ID = { 0x56,0x56,0x54,0x36,0x30,0x20,0x30,0x30,0x30,0x31,0x30,0x31,0x30,0x30,0x30,0x30 },
	.SW_Version_Date_Format = { 0x4E,0x33,0x33,0x31,0x2D,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20 },
	.SW_Edition_Version = { 0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20 },
	.VehicleManufacturerSparePartNumber_Nissan = { 0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20 },
	.VisteonProductPartNumber = { 0x20,0x20,0x20,0x20,0x20,0x20,0x2D,0x20,0x20,0x20,0x20,0x20,0x20,0x2D,0x20,0x20,0x20 },
	.EquippedPCBVisteonPartNumMainBorad = { 0x20,0x20,0x20,0x20,0x20,0x20,0x2D,0x20,0x20,0x20,0x20,0x20,0x20,0x2D,0x20,0x20,0x20 },
	.EquippedPCBVisteonPartNumSubBorad = { 0x20,0x20,0x20,0x20,0x20,0x20,0x2D,0x20,0x20,0x20,0x20,0x20,0x20,0x2D,0x20,0x20,0x20 },
	.DAUniqueID = { 0x56,0x56,0x54,0x36,0x30,0x20,0x30,0x30,0x30,0x31,0x30,0x31,0x30,0x30,0x30,0x30 },
	.ProductSerialNum = { 0xFF,0xFF,0xFF,0xFF,0xFF },
	.ProductSerialNumMainBoard = { 0xFF,0xFF,0xFF,0xFF,0xFF },
	.ProductSerialNumSubBoard = { 0xFF,0xFF,0xFF,0xFF,0xFF },
	.SMDManufacturingDate = { 0xFF,0xFF,0xFF },
	.AssemblyManufacturingDate = { 0xFF,0xFF,0xFF },
	.TraceabilityBytes = { 0xFF,0xFF,0xFF },
	.SMDPlantNum = { 0xFF,0xFF,0xFF },
	.AssemblyPlantNum = { 0xFF,0xFF,0xFF },
	.DEM_EVENT_CAN_COM_DATA = { 0xFF,0xFF,0xFF,0xFF,0,0,0xFF,0,0,0 },
	.DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA = { 0xFF,0xFF,0xFF,0xFF,0,0,0xFF,0,0,0 },
	.DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA = { 0xFF,0xFF,0xFF,0xFF,0,0,0xFF,0,0,0 },
	.DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA = { 0xFF,0xFF,0xFF,0xFF,0,0,0xFF,0,0,0 },
	.DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA = { 0xFF,0xFF,0xFF,0xFF,0,0,0xFF,0,0,0 },
	.DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA = { 0xFF,0xFF,0xFF,0xFF,0,0,0xFF,0,0,0 },
	.DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA = { 0xFF,0xFF,0xFF,0xFF,0,0,0xFF,0,0,0 },
	.DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA = { 0xFF,0xFF,0xFF,0xFF,0,0,0xFF,0,0,0 },
	.DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA = { 0xFF,0xFF,0xFF,0xFF,0,0,0xFF,0,0,0 },
	.DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA = { 0xFF,0xFF,0xFF,0xFF,0,0,0xFF,0,0,0 },
	.DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA = { 0xFF,0xFF,0xFF,0xFF,0,0,0xFF,0,0,0 },
	.DEM_EVENT_HVAC_PANEL_CONNECTION_DATA = { 0xFF,0xFF,0xFF,0xFF,0,0,0xFF,0,0,0 },
	.manufacture_reserved_474[0] = 0x00,
	.manufacture_reserved_474[1] = 0x00,
	.manufacture_reserved_474[2] = 0x00,
	.manufacture_reserved_474[3] = 0x00,
	.manufacture_reserved_474[4] = 0x00,
	.manufacture_reserved_474[5] = 0x00,
	.manufacture_reserved_474[6] = 0x00,
	.manufacture_reserved_474[7] = 0x00,
	.manufacture_reserved_474[8] = 0x00,
	.manufacture_reserved_474[9] = 0x00,
	.manufacture_reserved_474[10] = 0x00,
	.manufacture_reserved_474[11] = 0x00,
	.manufacture_reserved_474[12] = 0x00,
	.manufacture_reserved_474[13] = 0x00,
	.manufacture_reserved_474[14] = 0x00,
	.manufacture_reserved_474[15] = 0x00,
	.manufacture_reserved_474[16] = 0x00,
	.manufacture_reserved_474[17] = 0x00,
	.manufacture_reserved_474[18] = 0x00,
	.manufacture_reserved_474[19] = 0x00,
	.manufacture_reserved_474[20] = 0x00,
	.manufacture_reserved_474[21] = 0x00,
	.manufacture_reserved_474[22] = 0x00,
	.manufacture_reserved_474[23] = 0x00,
	.manufacture_reserved_474[24] = 0x00,
	.manufacture_reserved_474[25] = 0x00,
	.manufacture_reserved_474[26] = 0x00,
	.manufacture_reserved_474[27] = 0x00,
	.manufacture_reserved_474[28] = 0x00,
	.manufacture_reserved_474[29] = 0x00,
	.manufacture_reserved_474[30] = 0x00,
	.manufacture_reserved_474[31] = 0x00,
	.manufacture_reserved_474[32] = 0x00,
	.manufacture_reserved_474[33] = 0x00,
	.TachoUpValue = { 0x0320 },
	.TachoLowValue = { 0x0118 },
	.TachoLimitValue = { 0x0190 },
	.TachoUDeltaLimit = { 0x0028 },
	.TachoLDeltaLimit = { 0x0028 },
	.TachoUpChangeValue = { 0x000A },
	.TachoLowChangeValue = { 0x000A },
	.TachoRun = { 0xAA },
	.Speed_Rm_1 = 3344,
	.Speed_Rm_2 = 3355,
	.Speed_Rc = 3310,
	.FuelTelltaleOn_R = 2550,
	.FuelTelltaleOff_R = 2435,
	.FuelWarningOnPoint_R = 2550,
	.Fuel_R_Open = 3500,
	.Fuel_R_Short = 50,
	.PkbWarningJudgeV1 = 70,
	.PkbWarningJudgeV2 = 30,
	.AudioEepTest = 0xa5a5,
	.DspKeyCodeOnOff = 0,
	.reserved_57_DspKeyCode[0] = 0x00,
	.reserved_57_DspKeyCode[1] = 0x00,
	.reserved_57_DspKeyCode[2] = 0x00,
	.DspKeyCode = 0x800001,
	.BatState_ChgDly = 12,
	.BatVolVeryHigh_Hysteresis_H = 18500,
	.BatVolVeryHigh_Hysteresis_L = 17500,
	.BatVolHigh_Hysteresis_H = 17000,
	.BatVolHigh_Hysteresis_L = 16000,
	.BatVolLow_Hysteresis_H = 9000,
	.BatVolLow_Hysteresis_L = 8000,
	.BatVolVeryLow_Hysteresis_H = 7500,
	.BatVolVeryLow_Hysteresis_L = 6500,
	.TempState_ChgDly = 2,
	.TempDegC_Low_Hysteresis_L = 5,
	.TempDegC_Low_Hysteresis_H = 15,
	.TempDegC_High_Hysteresis_L = 125,
	.TempDegC_High_Hysteresis_H = 135,
	.reserved_87_AmpHighTempProction[0] = 0x00,
	.AmpHighTempProction = { 0 },
	.CanNm_DA_S1_Delay_ms = 200,
	.development_nvm_reserved_110[0] = 0x00,
	.development_nvm_reserved_110[1] = 0x00,
	.development_nvm_reserved_110[2] = 0x00,
	.development_nvm_reserved_110[3] = 0x00,
	.development_nvm_reserved_110[4] = 0x00,
	.development_nvm_reserved_110[5] = 0x00,
	.development_nvm_reserved_110[6] = 0x00,
	.development_nvm_reserved_110[7] = 0x00,
	.development_nvm_reserved_110[8] = 0x00,
	.development_nvm_reserved_110[9] = 0x00,
	.development_nvm_reserved_110[10] = 0x00,
	.development_nvm_reserved_110[11] = 0x00,
	.development_nvm_reserved_110[12] = 0x00,
	.development_nvm_reserved_110[13] = 0x00,
	.development_nvm_reserved_110[14] = 0x00,
	.development_nvm_reserved_110[15] = 0x00,
	.development_nvm_reserved_110[16] = 0x00,
	.development_nvm_reserved_110[17] = 0x00,
	.development_nvm_reserved_110[18] = 0x00,
	.development_nvm_reserved_110[19] = 0x00,
	.development_nvm_reserved_110[20] = 0x00,
	.development_nvm_reserved_110[21] = 0x00,
	.development_nvm_reserved_110[22] = 0x00,
	.development_nvm_reserved_110[23] = 0x00,
	.development_nvm_reserved_110[24] = 0x00,
	.development_nvm_reserved_110[25] = 0x00,
	.development_nvm_reserved_110[26] = 0x00,
	.development_nvm_reserved_110[27] = 0x00,
	.development_nvm_reserved_110[28] = 0x00,
	.development_nvm_reserved_110[29] = 0x00,
	.development_nvm_reserved_110[30] = 0x00,
	.development_nvm_reserved_110[31] = 0x00,
	.development_nvm_reserved_110[32] = 0x00,
	.development_nvm_reserved_110[33] = 0x00,
	.development_nvm_reserved_110[34] = 0x00,
	.development_nvm_reserved_110[35] = 0x00,
	.development_nvm_reserved_110[36] = 0x00,
	.development_nvm_reserved_110[37] = 0x00,
	.development_nvm_reserved_110[38] = 0x00,
	.development_nvm_reserved_110[39] = 0x00,
	.development_nvm_reserved_110[40] = 0x00,
	.development_nvm_reserved_110[41] = 0x00,
	.development_nvm_reserved_110[42] = 0x00,
	.development_nvm_reserved_110[43] = 0x00,
	.development_nvm_reserved_110[44] = 0x00,
	.development_nvm_reserved_110[45] = 0x00,
	.development_nvm_reserved_110[46] = 0x00,
	.development_nvm_reserved_110[47] = 0x00,
	.development_nvm_reserved_110[48] = 0x00,
	.development_nvm_reserved_110[49] = 0x00,
	.development_nvm_reserved_110[50] = 0x00,
	.development_nvm_reserved_110[51] = 0x00,
	.development_nvm_reserved_110[52] = 0x00,
	.development_nvm_reserved_110[53] = 0x00,
	.development_nvm_reserved_110[54] = 0x00,
	.development_nvm_reserved_110[55] = 0x00,
	.development_nvm_reserved_110[56] = 0x00,
	.development_nvm_reserved_110[57] = 0x00,
	.development_nvm_reserved_110[58] = 0x00,
	.development_nvm_reserved_110[59] = 0x00,
	.development_nvm_reserved_110[60] = 0x00,
	.development_nvm_reserved_110[61] = 0x00,
	.development_nvm_reserved_110[62] = 0x00,
	.development_nvm_reserved_110[63] = 0x00,
	.development_nvm_reserved_110[64] = 0x00,
	.development_nvm_reserved_110[65] = 0x00,
	.development_nvm_reserved_110[66] = 0x00,
	.development_nvm_reserved_110[67] = 0x00,
	.development_nvm_reserved_110[68] = 0x00,
	.development_nvm_reserved_110[69] = 0x00,
	.development_nvm_reserved_110[70] = 0x00,
	.development_nvm_reserved_110[71] = 0x00,
	.development_nvm_reserved_110[72] = 0x00,
	.development_nvm_reserved_110[73] = 0x00,
	.development_nvm_reserved_110[74] = 0x00,
	.development_nvm_reserved_110[75] = 0x00,
	.development_nvm_reserved_110[76] = 0x00,
	.development_nvm_reserved_110[77] = 0x00,
	.development_nvm_reserved_110[78] = 0x00,
	.development_nvm_reserved_110[79] = 0x00,
	.development_nvm_reserved_110[80] = 0x00,
	.development_nvm_reserved_110[81] = 0x00,
	.development_nvm_reserved_110[82] = 0x00,
	.development_nvm_reserved_110[83] = 0x00,
	.development_nvm_reserved_110[84] = 0x00,
	.development_nvm_reserved_110[85] = 0x00,
	.development_nvm_reserved_110[86] = 0x00,
	.development_nvm_reserved_110[87] = 0x00,
	.development_nvm_reserved_110[88] = 0x00,
	.development_nvm_reserved_110[89] = 0x00,
	.development_nvm_reserved_110[90] = 0x00,
	.development_nvm_reserved_110[91] = 0x00,
	.development_nvm_reserved_110[92] = 0x00,
	.development_nvm_reserved_110[93] = 0x00,
	.development_nvm_reserved_110[94] = 0x00,
	.development_nvm_reserved_110[95] = 0x00,
	.development_nvm_reserved_110[96] = 0x00,
	.development_nvm_reserved_110[97] = 0x00,
	.development_nvm_reserved_110[98] = 0x00,
	.development_nvm_reserved_110[99] = 0x00,
	.development_nvm_reserved_110[100] = 0x00,
	.development_nvm_reserved_110[101] = 0x00,
	.development_nvm_reserved_110[102] = 0x00,
	.development_nvm_reserved_110[103] = 0x00,
	.development_nvm_reserved_110[104] = 0x00,
	.development_nvm_reserved_110[105] = 0x00,
	.development_nvm_reserved_110[106] = 0x00,
	.development_nvm_reserved_110[107] = 0x00,
	.development_nvm_reserved_110[108] = 0x00,
	.development_nvm_reserved_110[109] = 0x00,
	.development_nvm_reserved_110[110] = 0x00,
	.development_nvm_reserved_110[111] = 0x00,
	.development_nvm_reserved_110[112] = 0x00,
	.development_nvm_reserved_110[113] = 0x00,
	.development_nvm_reserved_110[114] = 0x00,
	.development_nvm_reserved_110[115] = 0x00,
	.development_nvm_reserved_110[116] = 0x00,
	.development_nvm_reserved_110[117] = 0x00,
	.development_nvm_reserved_110[118] = 0x00,
	.development_nvm_reserved_110[119] = 0x00,
	.development_nvm_reserved_110[120] = 0x00,
	.development_nvm_reserved_110[121] = 0x00,
	.development_nvm_reserved_110[122] = 0x00,
	.development_nvm_reserved_110[123] = 0x00,
	.development_nvm_reserved_110[124] = 0x00,
	.development_nvm_reserved_110[125] = 0x00,
	.development_nvm_reserved_110[126] = 0x00,
	.development_nvm_reserved_110[127] = 0x00,
	.development_nvm_reserved_110[128] = 0x00,
	.development_nvm_reserved_110[129] = 0x00,
	.development_nvm_reserved_110[130] = 0x00,
	.development_nvm_reserved_110[131] = 0x00,
	.development_nvm_reserved_110[132] = 0x00,
	.development_nvm_reserved_110[133] = 0x00,
	.development_nvm_reserved_110[134] = 0x00,
	.development_nvm_reserved_110[135] = 0x00,
	.development_nvm_reserved_110[136] = 0x00,
	.development_nvm_reserved_110[137] = 0x00,
	.development_nvm_reserved_110[138] = 0x00,
	.development_nvm_reserved_110[139] = 0x00,
	.development_nvm_reserved_110[140] = 0x00,
	.development_nvm_reserved_110[141] = 0x00,
	.development_nvm_reserved_110[142] = 0x00,
	.development_nvm_reserved_110[143] = 0x00,
	.development_nvm_reserved_110[144] = 0x00,
	.development_nvm_reserved_110[145] = 0x00,
	.development_nvm_reserved_110[146] = 0x00,
	.development_nvm_reserved_110[147] = 0x00,
	.development_nvm_reserved_110[148] = 0x00,
	.development_nvm_reserved_110[149] = 0x00,
	.development_nvm_reserved_110[150] = 0x00,
	.development_nvm_reserved_110[151] = 0x00,
	.development_nvm_reserved_110[152] = 0x00,
	.development_nvm_reserved_110[153] = 0x00,
	.development_nvm_reserved_110[154] = 0x00,
	.development_nvm_reserved_110[155] = 0x00,
	.development_nvm_reserved_110[156] = 0x00,
	.development_nvm_reserved_110[157] = 0x00,
	.development_nvm_reserved_110[158] = 0x00,
	.development_nvm_reserved_110[159] = 0x00,
	.development_nvm_reserved_110[160] = 0x00,
	.development_nvm_reserved_110[161] = 0x00,
	.development_nvm_reserved_110[162] = 0x00,
	.development_nvm_reserved_110[163] = 0x00,
	.development_nvm_reserved_110[164] = 0x00,
	.development_nvm_reserved_110[165] = 0x00,
	.development_nvm_reserved_110[166] = 0x00,
	.development_nvm_reserved_110[167] = 0x00,
	.development_nvm_reserved_110[168] = 0x00,
	.development_nvm_reserved_110[169] = 0x00,
	.development_nvm_reserved_110[170] = 0x00,
	.development_nvm_reserved_110[171] = 0x00,
	.development_nvm_reserved_110[172] = 0x00,
	.development_nvm_reserved_110[173] = 0x00,
	.development_nvm_reserved_110[174] = 0x00,
	.development_nvm_reserved_110[175] = 0x00,
	.development_nvm_reserved_110[176] = 0x00,
	.development_nvm_reserved_110[177] = 0x00,
	.development_nvm_reserved_110[178] = 0x00,
	.development_nvm_reserved_110[179] = 0x00,
	.development_nvm_reserved_110[180] = 0x00,
	.development_nvm_reserved_110[181] = 0x00,
	.development_nvm_reserved_110[182] = 0x00,
	.development_nvm_reserved_110[183] = 0x00,
	.development_nvm_reserved_110[184] = 0x00,
	.development_nvm_reserved_110[185] = 0x00,
	.development_nvm_reserved_110[186] = 0x00,
	.development_nvm_reserved_110[187] = 0x00,
	.development_nvm_reserved_110[188] = 0x00,
	.development_nvm_reserved_110[189] = 0x00,
	.development_nvm_reserved_110[190] = 0x00,
	.development_nvm_reserved_110[191] = 0x00,
	.development_nvm_reserved_110[192] = 0x00,
	.development_nvm_reserved_110[193] = 0x00,
	.development_nvm_reserved_110[194] = 0x00,
	.development_nvm_reserved_110[195] = 0x00,
	.development_nvm_reserved_110[196] = 0x00,
	.development_nvm_reserved_110[197] = 0x00,
	.development_nvm_reserved_110[198] = 0x00,
	.development_nvm_reserved_110[199] = 0x00,
	.development_nvm_reserved_110[200] = 0x00,
	.development_nvm_reserved_110[201] = 0x00,
	.development_nvm_reserved_110[202] = 0x00,
	.development_nvm_reserved_110[203] = 0x00,
	.development_nvm_reserved_110[204] = 0x00,
	.development_nvm_reserved_110[205] = 0x00,
	.development_nvm_reserved_110[206] = 0x00,
	.development_nvm_reserved_110[207] = 0x00,
	.development_nvm_reserved_110[208] = 0x00,
	.development_nvm_reserved_110[209] = 0x00,
	.development_nvm_reserved_110[210] = 0x00,
	.development_nvm_reserved_110[211] = 0x00,
	.development_nvm_reserved_110[212] = 0x00,
	.development_nvm_reserved_110[213] = 0x00,
	.development_nvm_reserved_110[214] = 0x00,
	.development_nvm_reserved_110[215] = 0x00,
	.development_nvm_reserved_110[216] = 0x00,
	.development_nvm_reserved_110[217] = 0x00,
	.development_nvm_reserved_110[218] = 0x00,
	.development_nvm_reserved_110[219] = 0x00,
	.development_nvm_reserved_110[220] = 0x00,
	.development_nvm_reserved_110[221] = 0x00,
	.development_nvm_reserved_110[222] = 0x00,
	.development_nvm_reserved_110[223] = 0x00,
	.development_nvm_reserved_110[224] = 0x00,
	.development_nvm_reserved_110[225] = 0x00,
	.development_nvm_reserved_110[226] = 0x00,
	.development_nvm_reserved_110[227] = 0x00,
	.development_nvm_reserved_110[228] = 0x00,
	.development_nvm_reserved_110[229] = 0x00,
	.development_nvm_reserved_110[230] = 0x00,
	.development_nvm_reserved_110[231] = 0x00,
	.development_nvm_reserved_110[232] = 0x00,
	.development_nvm_reserved_110[233] = 0x00,
	.development_nvm_reserved_110[234] = 0x00,
	.development_nvm_reserved_110[235] = 0x00,
	.development_nvm_reserved_110[236] = 0x00,
	.development_nvm_reserved_110[237] = 0x00,
	.development_nvm_reserved_110[238] = 0x00,
	.development_nvm_reserved_110[239] = 0x00,
	.development_nvm_reserved_110[240] = 0x00,
	.development_nvm_reserved_110[241] = 0x00,
	.development_nvm_reserved_110[242] = 0x00,
	.development_nvm_reserved_110[243] = 0x00,
	.development_nvm_reserved_110[244] = 0x00,
	.development_nvm_reserved_110[245] = 0x00,
	.development_nvm_reserved_110[246] = 0x00,
	.development_nvm_reserved_110[247] = 0x00,
	.development_nvm_reserved_110[248] = 0x00,
	.development_nvm_reserved_110[249] = 0x00,
	.development_nvm_reserved_110[250] = 0x00,
	.development_nvm_reserved_110[251] = 0x00,
	.development_nvm_reserved_110[252] = 0x00,
	.development_nvm_reserved_110[253] = 0x00,
	.development_nvm_reserved_110[254] = 0x00,
	.development_nvm_reserved_110[255] = 0x00,
	.development_nvm_reserved_110[256] = 0x00,
	.development_nvm_reserved_110[257] = 0x00,
	.development_nvm_reserved_110[258] = 0x00,
	.development_nvm_reserved_110[259] = 0x00,
	.development_nvm_reserved_110[260] = 0x00,
	.development_nvm_reserved_110[261] = 0x00,
	.development_nvm_reserved_110[262] = 0x00,
	.development_nvm_reserved_110[263] = 0x00,
	.development_nvm_reserved_110[264] = 0x00,
	.development_nvm_reserved_110[265] = 0x00,
	.development_nvm_reserved_110[266] = 0x00,
	.development_nvm_reserved_110[267] = 0x00,
	.development_nvm_reserved_110[268] = 0x00,
	.development_nvm_reserved_110[269] = 0x00,
	.development_nvm_reserved_110[270] = 0x00,
	.development_nvm_reserved_110[271] = 0x00,
	.development_nvm_reserved_110[272] = 0x00,
	.development_nvm_reserved_110[273] = 0x00,
	.development_nvm_reserved_110[274] = 0x00,
	.development_nvm_reserved_110[275] = 0x00,
	.development_nvm_reserved_110[276] = 0x00,
	.development_nvm_reserved_110[277] = 0x00,
	.development_nvm_reserved_110[278] = 0x00,
	.development_nvm_reserved_110[279] = 0x00,
	.development_nvm_reserved_110[280] = 0x00,
	.development_nvm_reserved_110[281] = 0x00,
	.development_nvm_reserved_110[282] = 0x00,
	.development_nvm_reserved_110[283] = 0x00,
	.development_nvm_reserved_110[284] = 0x00,
	.development_nvm_reserved_110[285] = 0x00,
	.development_nvm_reserved_110[286] = 0x00,
	.development_nvm_reserved_110[287] = 0x00,
	.development_nvm_reserved_110[288] = 0x00,
	.development_nvm_reserved_110[289] = 0x00,
	.development_nvm_reserved_110[290] = 0x00,
	.development_nvm_reserved_110[291] = 0x00,
	.development_nvm_reserved_110[292] = 0x00,
	.development_nvm_reserved_110[293] = 0x00,
	.development_nvm_reserved_110[294] = 0x00,
	.development_nvm_reserved_110[295] = 0x00,
	.development_nvm_reserved_110[296] = 0x00,
	.development_nvm_reserved_110[297] = 0x00,
	.development_nvm_reserved_110[298] = 0x00,
	.development_nvm_reserved_110[299] = 0x00,
	.development_nvm_reserved_110[300] = 0x00,
	.development_nvm_reserved_110[301] = 0x00,
	.development_nvm_reserved_110[302] = 0x00,
	.development_nvm_reserved_110[303] = 0x00,
	.development_nvm_reserved_110[304] = 0x00,
	.development_nvm_reserved_110[305] = 0x00,
	.development_nvm_reserved_110[306] = 0x00,
	.development_nvm_reserved_110[307] = 0x00,
	.development_nvm_reserved_110[308] = 0x00,
	.development_nvm_reserved_110[309] = 0x00,
	.development_nvm_reserved_110[310] = 0x00,
	.development_nvm_reserved_110[311] = 0x00,
	.development_nvm_reserved_110[312] = 0x00,
	.development_nvm_reserved_110[313] = 0x00,
	.development_nvm_reserved_110[314] = 0x00,
	.development_nvm_reserved_110[315] = 0x00,
	.development_nvm_reserved_110[316] = 0x00,
	.development_nvm_reserved_110[317] = 0x00,
	.development_nvm_reserved_110[318] = 0x00,
	.development_nvm_reserved_110[319] = 0x00,
	.development_nvm_reserved_110[320] = 0x00,
	.development_nvm_reserved_110[321] = 0x00,
	.development_nvm_reserved_110[322] = 0x00,
	.development_nvm_reserved_110[323] = 0x00,
	.development_nvm_reserved_110[324] = 0x00,
	.development_nvm_reserved_110[325] = 0x00,
	.development_nvm_reserved_110[326] = 0x00,
	.development_nvm_reserved_110[327] = 0x00,
	.development_nvm_reserved_110[328] = 0x00,
	.development_nvm_reserved_110[329] = 0x00,
	.development_nvm_reserved_110[330] = 0x00,
	.development_nvm_reserved_110[331] = 0x00,
	.development_nvm_reserved_110[332] = 0x00,
	.development_nvm_reserved_110[333] = 0x00,
	.development_nvm_reserved_110[334] = 0x00,
	.development_nvm_reserved_110[335] = 0x00,
	.development_nvm_reserved_110[336] = 0x00,
	.development_nvm_reserved_110[337] = 0x00,
	.development_nvm_reserved_110[338] = 0x00,
	.development_nvm_reserved_110[339] = 0x00,
	.development_nvm_reserved_110[340] = 0x00,
	.development_nvm_reserved_110[341] = 0x00,
	.development_nvm_reserved_110[342] = 0x00,
	.development_nvm_reserved_110[343] = 0x00,
	.development_nvm_reserved_110[344] = 0x00,
	.development_nvm_reserved_110[345] = 0x00,
	.development_nvm_reserved_110[346] = 0x00,
	.development_nvm_reserved_110[347] = 0x00,
	.development_nvm_reserved_110[348] = 0x00,
	.development_nvm_reserved_110[349] = 0x00,
	.development_nvm_reserved_110[350] = 0x00,
	.development_nvm_reserved_110[351] = 0x00,
	.development_nvm_reserved_110[352] = 0x00,
	.development_nvm_reserved_110[353] = 0x00,
	.development_nvm_reserved_110[354] = 0x00,
	.development_nvm_reserved_110[355] = 0x00,
	.development_nvm_reserved_110[356] = 0x00,
	.development_nvm_reserved_110[357] = 0x00,
	.development_nvm_reserved_110[358] = 0x00,
	.development_nvm_reserved_110[359] = 0x00,
	.development_nvm_reserved_110[360] = 0x00,
	.development_nvm_reserved_110[361] = 0x00,
	.development_nvm_reserved_110[362] = 0x00,
	.development_nvm_reserved_110[363] = 0x00,
	.development_nvm_reserved_110[364] = 0x00,
	.development_nvm_reserved_110[365] = 0x00,
	.development_nvm_reserved_110[366] = 0x00,
	.development_nvm_reserved_110[367] = 0x00,
	.development_nvm_reserved_110[368] = 0x00,
	.development_nvm_reserved_110[369] = 0x00,
	.development_nvm_reserved_110[370] = 0x00,
	.development_nvm_reserved_110[371] = 0x00,
	.development_nvm_reserved_110[372] = 0x00,
	.development_nvm_reserved_110[373] = 0x00,
	.development_nvm_reserved_110[374] = 0x00,
	.development_nvm_reserved_110[375] = 0x00,
	.development_nvm_reserved_110[376] = 0x00,
	.development_nvm_reserved_110[377] = 0x00,
	.development_nvm_reserved_110[378] = 0x00,
	.development_nvm_reserved_110[379] = 0x00,
	.development_nvm_reserved_110[380] = 0x00,
	.development_nvm_reserved_110[381] = 0x00,
	.development_nvm_reserved_110[382] = 0x00,
	.development_nvm_reserved_110[383] = 0x00,
	.development_nvm_reserved_110[384] = 0x00,
	.development_nvm_reserved_110[385] = 0x00,
	.development_nvm_reserved_110[386] = 0x00,
	.development_nvm_reserved_110[387] = 0x00,
	.development_nvm_reserved_110[388] = 0x00,
	.development_nvm_reserved_110[389] = 0x00,
	.development_nvm_reserved_110[390] = 0x00,
	.development_nvm_reserved_110[391] = 0x00,
	.development_nvm_reserved_110[392] = 0x00,
	.development_nvm_reserved_110[393] = 0x00,
	.development_nvm_reserved_110[394] = 0x00,
	.development_nvm_reserved_110[395] = 0x00,
	.development_nvm_reserved_110[396] = 0x00,
	.development_nvm_reserved_110[397] = 0x00,
	.development_nvm_reserved_110[398] = 0x00,
	.development_nvm_reserved_110[399] = 0x00,
	.development_nvm_reserved_110[400] = 0x00,
	.development_nvm_reserved_110[401] = 0x00,
	.development_nvm_reserved_110[402] = 0x00,
	.development_nvm_reserved_110[403] = 0x00,
	.development_nvm_reserved_110[404] = 0x00,
	.development_nvm_reserved_110[405] = 0x00,
	.development_nvm_reserved_110[406] = 0x00,
	.development_nvm_reserved_110[407] = 0x00,
	.development_nvm_reserved_110[408] = 0x00,
	.development_nvm_reserved_110[409] = 0x00,
	.development_nvm_reserved_110[410] = 0x00,
	.development_nvm_reserved_110[411] = 0x00,
	.development_nvm_reserved_110[412] = 0x00,
	.development_nvm_reserved_110[413] = 0x00,
	.development_nvm_reserved_110[414] = 0x00,
	.development_nvm_reserved_110[415] = 0x00,
	.development_nvm_reserved_110[416] = 0x00,
	.development_nvm_reserved_110[417] = 0x00,
	.development_nvm_reserved_110[418] = 0x00,
	.development_nvm_reserved_110[419] = 0x00,
	.development_nvm_reserved_110[420] = 0x00,
	.development_nvm_reserved_110[421] = 0x00,
	.development_nvm_reserved_110[422] = 0x00,
	.development_nvm_reserved_110[423] = 0x00,
	.development_nvm_reserved_110[424] = 0x00,
	.development_nvm_reserved_110[425] = 0x00,
	.development_nvm_reserved_110[426] = 0x00,
	.development_nvm_reserved_110[427] = 0x00,
	.development_nvm_reserved_110[428] = 0x00,
	.development_nvm_reserved_110[429] = 0x00,
	.development_nvm_reserved_110[430] = 0x00,
	.development_nvm_reserved_110[431] = 0x00,
	.development_nvm_reserved_110[432] = 0x00,
	.development_nvm_reserved_110[433] = 0x00,
	.development_nvm_reserved_110[434] = 0x00,
	.development_nvm_reserved_110[435] = 0x00,
	.development_nvm_reserved_110[436] = 0x00,
	.development_nvm_reserved_110[437] = 0x00,
	.development_nvm_reserved_110[438] = 0x00,
	.development_nvm_reserved_110[439] = 0x00,
	.development_nvm_reserved_110[440] = 0x00,
	.development_nvm_reserved_110[441] = 0x00,
	.development_nvm_reserved_110[442] = 0x00,
	.development_nvm_reserved_110[443] = 0x00,
	.development_nvm_reserved_110[444] = 0x00,
	.development_nvm_reserved_110[445] = 0x00,
	.development_nvm_reserved_110[446] = 0x00,
	.development_nvm_reserved_110[447] = 0x00,
	.development_nvm_reserved_110[448] = 0x00,
	.development_nvm_reserved_110[449] = 0x00,
	.development_nvm_reserved_110[450] = 0x00,
	.development_nvm_reserved_110[451] = 0x00,
	.development_nvm_reserved_110[452] = 0x00,
	.development_nvm_reserved_110[453] = 0x00,
	.development_nvm_reserved_110[454] = 0x00,
	.development_nvm_reserved_110[455] = 0x00,
	.development_nvm_reserved_110[456] = 0x00,
	.development_nvm_reserved_110[457] = 0x00,
	.development_nvm_reserved_110[458] = 0x00,
	.development_nvm_reserved_110[459] = 0x00,
	.development_nvm_reserved_110[460] = 0x00,
	.development_nvm_reserved_110[461] = 0x00,
	.development_nvm_reserved_110[462] = 0x00,
	.development_nvm_reserved_110[463] = 0x00,
	.development_nvm_reserved_110[464] = 0x00,
	.development_nvm_reserved_110[465] = 0x00,
	.development_nvm_reserved_110[466] = 0x00,
	.development_nvm_reserved_110[467] = 0x00,
	.development_nvm_reserved_110[468] = 0x00,
	.development_nvm_reserved_110[469] = 0x00,
	.development_nvm_reserved_110[470] = 0x00,
	.development_nvm_reserved_110[471] = 0x00,
	.development_nvm_reserved_110[472] = 0x00,
	.development_nvm_reserved_110[473] = 0x00,
	.development_nvm_reserved_110[474] = 0x00,
	.development_nvm_reserved_110[475] = 0x00,
	.development_nvm_reserved_110[476] = 0x00,
	.development_nvm_reserved_110[477] = 0x00,
	.development_nvm_reserved_110[478] = 0x00,
	.development_nvm_reserved_110[479] = 0x00,
	.development_nvm_reserved_110[480] = 0x00,
	.development_nvm_reserved_110[481] = 0x00,
	.development_nvm_reserved_110[482] = 0x00,
	.development_nvm_reserved_110[483] = 0x00,
	.development_nvm_reserved_110[484] = 0x00,
	.development_nvm_reserved_110[485] = 0x00,
	.development_nvm_reserved_110[486] = 0x00,
	.development_nvm_reserved_110[487] = 0x00,
	.development_nvm_reserved_110[488] = 0x00,
	.development_nvm_reserved_110[489] = 0x00,
	.development_nvm_reserved_110[490] = 0x00,
	.development_nvm_reserved_110[491] = 0x00,
	.development_nvm_reserved_110[492] = 0x00,
	.development_nvm_reserved_110[493] = 0x00,
	.development_nvm_reserved_110[494] = 0x00,
	.development_nvm_reserved_110[495] = 0x00,
	.development_nvm_reserved_110[496] = 0x00,
	.development_nvm_reserved_110[497] = 0x00,
	.development_nvm_reserved_110[498] = 0x00,
	.development_nvm_reserved_110[499] = 0x00,
	.development_nvm_reserved_110[500] = 0x00,
	.development_nvm_reserved_110[501] = 0x00,
	.development_nvm_reserved_110[502] = 0x00,
	.development_nvm_reserved_110[503] = 0x00,
	.development_nvm_reserved_110[504] = 0x00,
	.development_nvm_reserved_110[505] = 0x00,
	.development_nvm_reserved_110[506] = 0x00,
	.development_nvm_reserved_110[507] = 0x00,
	.development_nvm_reserved_110[508] = 0x00,
	.development_nvm_reserved_110[509] = 0x00,
	.development_nvm_reserved_110[510] = 0x00,
	.development_nvm_reserved_110[511] = 0x00,
	.development_nvm_reserved_110[512] = 0x00,
	.development_nvm_reserved_110[513] = 0x00,
	.development_nvm_reserved_110[514] = 0x00,
	.development_nvm_reserved_110[515] = 0x00,
	.development_nvm_reserved_110[516] = 0x00,
	.development_nvm_reserved_110[517] = 0x00,
	.development_nvm_reserved_110[518] = 0x00,
	.development_nvm_reserved_110[519] = 0x00,
	.development_nvm_reserved_110[520] = 0x00,
	.development_nvm_reserved_110[521] = 0x00,
	.development_nvm_reserved_110[522] = 0x00,
	.development_nvm_reserved_110[523] = 0x00,
	.development_nvm_reserved_110[524] = 0x00,
	.development_nvm_reserved_110[525] = 0x00,
	.development_nvm_reserved_110[526] = 0x00,
	.development_nvm_reserved_110[527] = 0x00,
	.development_nvm_reserved_110[528] = 0x00,
	.development_nvm_reserved_110[529] = 0x00,
	.development_nvm_reserved_110[530] = 0x00,
	.development_nvm_reserved_110[531] = 0x00,
	.development_nvm_reserved_110[532] = 0x00,
	.development_nvm_reserved_110[533] = 0x00,
	.development_nvm_reserved_110[534] = 0x00,
	.development_nvm_reserved_110[535] = 0x00,
	.development_nvm_reserved_110[536] = 0x00,
	.development_nvm_reserved_110[537] = 0x00,
	.development_nvm_reserved_110[538] = 0x00,
	.development_nvm_reserved_110[539] = 0x00,
	.development_nvm_reserved_110[540] = 0x00,
	.development_nvm_reserved_110[541] = 0x00,
	.development_nvm_reserved_110[542] = 0x00,
	.development_nvm_reserved_110[543] = 0x00,
	.development_nvm_reserved_110[544] = 0x00,
	.development_nvm_reserved_110[545] = 0x00,
	.development_nvm_reserved_110[546] = 0x00,
	.development_nvm_reserved_110[547] = 0x00,
	.development_nvm_reserved_110[548] = 0x00,
	.development_nvm_reserved_110[549] = 0x00,
	.development_nvm_reserved_110[550] = 0x00,
	.development_nvm_reserved_110[551] = 0x00,
	.development_nvm_reserved_110[552] = 0x00,
	.development_nvm_reserved_110[553] = 0x00,
	.development_nvm_reserved_110[554] = 0x00,
	.development_nvm_reserved_110[555] = 0x00,
	.development_nvm_reserved_110[556] = 0x00,
	.development_nvm_reserved_110[557] = 0x00,
	.development_nvm_reserved_110[558] = 0x00,
	.development_nvm_reserved_110[559] = 0x00,
	.development_nvm_reserved_110[560] = 0x00,
	.development_nvm_reserved_110[561] = 0x00,
	.development_nvm_reserved_110[562] = 0x00,
	.development_nvm_reserved_110[563] = 0x00,
	.development_nvm_reserved_110[564] = 0x00,
	.development_nvm_reserved_110[565] = 0x00,
	.development_nvm_reserved_110[566] = 0x00,
	.development_nvm_reserved_110[567] = 0x00,
	.development_nvm_reserved_110[568] = 0x00,
	.development_nvm_reserved_110[569] = 0x00,
	.development_nvm_reserved_110[570] = 0x00,
	.development_nvm_reserved_110[571] = 0x00,
	.development_nvm_reserved_110[572] = 0x00,
	.development_nvm_reserved_110[573] = 0x00,
	.development_nvm_reserved_110[574] = 0x00,
	.development_nvm_reserved_110[575] = 0x00,
	.development_nvm_reserved_110[576] = 0x00,
	.development_nvm_reserved_110[577] = 0x00,
	.development_nvm_reserved_110[578] = 0x00,
	.development_nvm_reserved_110[579] = 0x00,
	.development_nvm_reserved_110[580] = 0x00,
	.development_nvm_reserved_110[581] = 0x00,
	.development_nvm_reserved_110[582] = 0x00,
	.development_nvm_reserved_110[583] = 0x00,
	.development_nvm_reserved_110[584] = 0x00,
	.development_nvm_reserved_110[585] = 0x00,
	.development_nvm_reserved_110[586] = 0x00,
	.development_nvm_reserved_110[587] = 0x00,
	.development_nvm_reserved_110[588] = 0x00,
	.development_nvm_reserved_110[589] = 0x00,
	.development_nvm_reserved_110[590] = 0x00,
	.development_nvm_reserved_110[591] = 0x00,
	.development_nvm_reserved_110[592] = 0x00,
	.development_nvm_reserved_110[593] = 0x00,
	.development_nvm_reserved_110[594] = 0x00,
	.development_nvm_reserved_110[595] = 0x00,
	.development_nvm_reserved_110[596] = 0x00,
	.development_nvm_reserved_110[597] = 0x00,
	.development_nvm_reserved_110[598] = 0x00,
	.development_nvm_reserved_110[599] = 0x00,
	.development_nvm_reserved_110[600] = 0x00,
	.development_nvm_reserved_110[601] = 0x00,
	.development_nvm_reserved_110[602] = 0x00,
	.development_nvm_reserved_110[603] = 0x00,
	.development_nvm_reserved_110[604] = 0x00,
	.development_nvm_reserved_110[605] = 0x00,
	.development_nvm_reserved_110[606] = 0x00,
	.development_nvm_reserved_110[607] = 0x00,
	.development_nvm_reserved_110[608] = 0x00,
	.development_nvm_reserved_110[609] = 0x00,
	.development_nvm_reserved_110[610] = 0x00,
	.development_nvm_reserved_110[611] = 0x00,
	.development_nvm_reserved_110[612] = 0x00,
	.development_nvm_reserved_110[613] = 0x00,
	.development_nvm_reserved_110[614] = 0x00,
	.development_nvm_reserved_110[615] = 0x00,
	.development_nvm_reserved_110[616] = 0x00,
	.development_nvm_reserved_110[617] = 0x00,
	.development_nvm_reserved_110[618] = 0x00,
	.development_nvm_reserved_110[619] = 0x00,
	.development_nvm_reserved_110[620] = 0x00,
	.development_nvm_reserved_110[621] = 0x00,
	.development_nvm_reserved_110[622] = 0x00,
	.development_nvm_reserved_110[623] = 0x00,
	.development_nvm_reserved_110[624] = 0x00,
	.development_nvm_reserved_110[625] = 0x00,
	.development_nvm_reserved_110[626] = 0x00,
	.development_nvm_reserved_110[627] = 0x00,
	.development_nvm_reserved_110[628] = 0x00,
	.development_nvm_reserved_110[629] = 0x00,
	.development_nvm_reserved_110[630] = 0x00,
	.development_nvm_reserved_110[631] = 0x00,
	.development_nvm_reserved_110[632] = 0x00,
	.development_nvm_reserved_110[633] = 0x00,
	.development_nvm_reserved_110[634] = 0x00,
	.development_nvm_reserved_110[635] = 0x00,
	.development_nvm_reserved_110[636] = 0x00,
	.development_nvm_reserved_110[637] = 0x00,
	.development_nvm_reserved_110[638] = 0x00,
	.development_nvm_reserved_110[639] = 0x00,
	.development_nvm_reserved_110[640] = 0x00,
	.development_nvm_reserved_110[641] = 0x00,
	.development_nvm_reserved_110[642] = 0x00,
	.development_nvm_reserved_110[643] = 0x00,
	.development_nvm_reserved_110[644] = 0x00,
	.development_nvm_reserved_110[645] = 0x00,
	.development_nvm_reserved_110[646] = 0x00,
	.development_nvm_reserved_110[647] = 0x00,
	.development_nvm_reserved_110[648] = 0x00,
	.development_nvm_reserved_110[649] = 0x00,
	.development_nvm_reserved_110[650] = 0x00,
	.development_nvm_reserved_110[651] = 0x00,
	.development_nvm_reserved_110[652] = 0x00,
	.development_nvm_reserved_110[653] = 0x00,
	.development_nvm_reserved_110[654] = 0x00,
	.development_nvm_reserved_110[655] = 0x00,
	.development_nvm_reserved_110[656] = 0x00,
	.development_nvm_reserved_110[657] = 0x00,
	.development_nvm_reserved_110[658] = 0x00,
	.development_nvm_reserved_110[659] = 0x00,
	.development_nvm_reserved_110[660] = 0x00,
	.development_nvm_reserved_110[661] = 0x00,
	.development_nvm_reserved_110[662] = 0x00,
	.development_nvm_reserved_110[663] = 0x00,
	.development_nvm_reserved_110[664] = 0x00,
	.development_nvm_reserved_110[665] = 0x00,
	.development_nvm_reserved_110[666] = 0x00,
	.development_nvm_reserved_110[667] = 0x00,
	.development_nvm_reserved_110[668] = 0x00,
	.development_nvm_reserved_110[669] = 0x00,
	.development_nvm_reserved_110[670] = 0x00,
	.development_nvm_reserved_110[671] = 0x00,
	.development_nvm_reserved_110[672] = 0x00,
	.development_nvm_reserved_110[673] = 0x00,
	.development_nvm_reserved_110[674] = 0x00,
	.development_nvm_reserved_110[675] = 0x00,
	.development_nvm_reserved_110[676] = 0x00,
	.development_nvm_reserved_110[677] = 0x00,
	.development_nvm_reserved_110[678] = 0x00,
	.development_nvm_reserved_110[679] = 0x00,
	.development_nvm_reserved_110[680] = 0x00,
	.development_nvm_reserved_110[681] = 0x00,
	.development_nvm_reserved_110[682] = 0x00,
	.development_nvm_reserved_110[683] = 0x00,
	.development_nvm_reserved_110[684] = 0x00,
	.development_nvm_reserved_110[685] = 0x00,
	.development_nvm_reserved_110[686] = 0x00,
	.development_nvm_reserved_110[687] = 0x00,
	.development_nvm_reserved_110[688] = 0x00,
	.development_nvm_reserved_110[689] = 0x00,
	.development_nvm_reserved_110[690] = 0x00,
	.development_nvm_reserved_110[691] = 0x00,
	.development_nvm_reserved_110[692] = 0x00,
	.development_nvm_reserved_110[693] = 0x00,
	.development_nvm_reserved_110[694] = 0x00,
	.development_nvm_reserved_110[695] = 0x00,
	.development_nvm_reserved_110[696] = 0x00,
	.development_nvm_reserved_110[697] = 0x00,
	.development_nvm_reserved_110[698] = 0x00,
	.development_nvm_reserved_110[699] = 0x00,
	.development_nvm_reserved_110[700] = 0x00,
	.development_nvm_reserved_110[701] = 0x00,
	.development_nvm_reserved_110[702] = 0x00,
	.development_nvm_reserved_110[703] = 0x00,
	.development_nvm_reserved_110[704] = 0x00,
	.development_nvm_reserved_110[705] = 0x00,
	.development_nvm_reserved_110[706] = 0x00,
	.development_nvm_reserved_110[707] = 0x00,
	.development_nvm_reserved_110[708] = 0x00,
	.development_nvm_reserved_110[709] = 0x00,
	.development_nvm_reserved_110[710] = 0x00,
	.development_nvm_reserved_110[711] = 0x00,
	.development_nvm_reserved_110[712] = 0x00,
	.development_nvm_reserved_110[713] = 0x00,
	.development_nvm_reserved_110[714] = 0x00,
	.development_nvm_reserved_110[715] = 0x00,
	.development_nvm_reserved_110[716] = 0x00,
	.development_nvm_reserved_110[717] = 0x00,
	.development_nvm_reserved_110[718] = 0x00,
	.development_nvm_reserved_110[719] = 0x00,
	.development_nvm_reserved_110[720] = 0x00,
	.development_nvm_reserved_110[721] = 0x00,
	.development_nvm_reserved_110[722] = 0x00,
	.development_nvm_reserved_110[723] = 0x00,
	.development_nvm_reserved_110[724] = 0x00,
	.development_nvm_reserved_110[725] = 0x00,
	.development_nvm_reserved_110[726] = 0x00,
	.development_nvm_reserved_110[727] = 0x00,
	.development_nvm_reserved_110[728] = 0x00,
	.development_nvm_reserved_110[729] = 0x00,
	.development_nvm_reserved_110[730] = 0x00,
	.development_nvm_reserved_110[731] = 0x00,
	.development_nvm_reserved_110[732] = 0x00,
	.development_nvm_reserved_110[733] = 0x00,
	.development_nvm_reserved_110[734] = 0x00,
	.development_nvm_reserved_110[735] = 0x00,
	.development_nvm_reserved_110[736] = 0x00,
	.development_nvm_reserved_110[737] = 0x00,
	.development_nvm_reserved_110[738] = 0x00,
	.development_nvm_reserved_110[739] = 0x00,
	.development_nvm_reserved_110[740] = 0x00,
	.development_nvm_reserved_110[741] = 0x00,
	.development_nvm_reserved_110[742] = 0x00,
	.development_nvm_reserved_110[743] = 0x00,
	.development_nvm_reserved_110[744] = 0x00,
	.development_nvm_reserved_110[745] = 0x00,
	.development_nvm_reserved_110[746] = 0x00,
	.development_nvm_reserved_110[747] = 0x00,
	.development_nvm_reserved_110[748] = 0x00,
	.development_nvm_reserved_110[749] = 0x00,
	.development_nvm_reserved_110[750] = 0x00,
	.development_nvm_reserved_110[751] = 0x00,
	.development_nvm_reserved_110[752] = 0x00,
	.development_nvm_reserved_110[753] = 0x00,
	.development_nvm_reserved_110[754] = 0x00,
	.development_nvm_reserved_110[755] = 0x00,
	.development_nvm_reserved_110[756] = 0x00,
	.development_nvm_reserved_110[757] = 0x00,
	.development_nvm_reserved_110[758] = 0x00,
	.development_nvm_reserved_110[759] = 0x00,
	.development_nvm_reserved_110[760] = 0x00,
	.development_nvm_reserved_110[761] = 0x00,
	.development_nvm_reserved_110[762] = 0x00,
	.development_nvm_reserved_110[763] = 0x00,
	.development_nvm_reserved_110[764] = 0x00,
	.development_nvm_reserved_110[765] = 0x00,
	.development_nvm_reserved_110[766] = 0x00,
	.development_nvm_reserved_110[767] = 0x00,
	.development_nvm_reserved_110[768] = 0x00,
	.development_nvm_reserved_110[769] = 0x00,
	.development_nvm_reserved_110[770] = 0x00,
	.development_nvm_reserved_110[771] = 0x00,
	.development_nvm_reserved_110[772] = 0x00,
	.development_nvm_reserved_110[773] = 0x00,
	.development_nvm_reserved_110[774] = 0x00,
	.development_nvm_reserved_110[775] = 0x00,
	.development_nvm_reserved_110[776] = 0x00,
	.development_nvm_reserved_110[777] = 0x00,
	.development_nvm_reserved_110[778] = 0x00,
	.development_nvm_reserved_110[779] = 0x00,
	.development_nvm_reserved_110[780] = 0x00,
	.development_nvm_reserved_110[781] = 0x00,
	.development_nvm_reserved_110[782] = 0x00,
	.development_nvm_reserved_110[783] = 0x00,
	.development_nvm_reserved_110[784] = 0x00,
	.development_nvm_reserved_110[785] = 0x00,
	.development_nvm_reserved_110[786] = 0x00,
	.development_nvm_reserved_110[787] = 0x00,
	.development_nvm_reserved_110[788] = 0x00,
	.development_nvm_reserved_110[789] = 0x00,
	.development_nvm_reserved_110[790] = 0x00,
	.development_nvm_reserved_110[791] = 0x00,
	.development_nvm_reserved_110[792] = 0x00,
	.development_nvm_reserved_110[793] = 0x00,
	.development_nvm_reserved_110[794] = 0x00,
	.development_nvm_reserved_110[795] = 0x00,
	.development_nvm_reserved_110[796] = 0x00,
	.development_nvm_reserved_110[797] = 0x00,
	.development_nvm_reserved_110[798] = 0x00,
	.development_nvm_reserved_110[799] = 0x00,
	.development_nvm_reserved_110[800] = 0x00,
	.development_nvm_reserved_110[801] = 0x00,
	.development_nvm_reserved_110[802] = 0x00,
	.development_nvm_reserved_110[803] = 0x00,
	.development_nvm_reserved_110[804] = 0x00,
	.development_nvm_reserved_110[805] = 0x00,
	.development_nvm_reserved_110[806] = 0x00,
	.development_nvm_reserved_110[807] = 0x00,
	.development_nvm_reserved_110[808] = 0x00,
	.development_nvm_reserved_110[809] = 0x00,
	.development_nvm_reserved_110[810] = 0x00,
	.development_nvm_reserved_110[811] = 0x00,
	.development_nvm_reserved_110[812] = 0x00,
	.development_nvm_reserved_110[813] = 0x00,
	.development_nvm_reserved_110[814] = 0x00,
	.development_nvm_reserved_110[815] = 0x00,
	.development_nvm_reserved_110[816] = 0x00,
	.development_nvm_reserved_110[817] = 0x00,
	.development_nvm_reserved_110[818] = 0x00,
	.development_nvm_reserved_110[819] = 0x00,
	.development_nvm_reserved_110[820] = 0x00,
	.development_nvm_reserved_110[821] = 0x00,
	.development_nvm_reserved_110[822] = 0x00,
	.development_nvm_reserved_110[823] = 0x00,
	.development_nvm_reserved_110[824] = 0x00,
	.development_nvm_reserved_110[825] = 0x00,
	.development_nvm_reserved_110[826] = 0x00,
	.development_nvm_reserved_110[827] = 0x00,
	.development_nvm_reserved_110[828] = 0x00,
	.development_nvm_reserved_110[829] = 0x00,
	.development_nvm_reserved_110[830] = 0x00,
	.development_nvm_reserved_110[831] = 0x00,
	.development_nvm_reserved_110[832] = 0x00,
	.development_nvm_reserved_110[833] = 0x00,
	.development_nvm_reserved_110[834] = 0x00,
	.development_nvm_reserved_110[835] = 0x00,
	.development_nvm_reserved_110[836] = 0x00,
	.development_nvm_reserved_110[837] = 0x00,
	.development_nvm_reserved_110[838] = 0x00,
	.development_nvm_reserved_110[839] = 0x00,
	.development_nvm_reserved_110[840] = 0x00,
	.development_nvm_reserved_110[841] = 0x00,
	.development_nvm_reserved_110[842] = 0x00,
	.development_nvm_reserved_110[843] = 0x00,
	.development_nvm_reserved_110[844] = 0x00,
	.development_nvm_reserved_110[845] = 0x00,
	.development_nvm_reserved_110[846] = 0x00,
	.development_nvm_reserved_110[847] = 0x00,
	.development_nvm_reserved_110[848] = 0x00,
	.development_nvm_reserved_110[849] = 0x00,
	.development_nvm_reserved_110[850] = 0x00,
	.development_nvm_reserved_110[851] = 0x00,
	.development_nvm_reserved_110[852] = 0x00,
	.development_nvm_reserved_110[853] = 0x00,
	.development_nvm_reserved_110[854] = 0x00,
	.development_nvm_reserved_110[855] = 0x00,
	.development_nvm_reserved_110[856] = 0x00,
	.development_nvm_reserved_110[857] = 0x00,
	.development_nvm_reserved_110[858] = 0x00,
	.development_nvm_reserved_110[859] = 0x00,
	.development_nvm_reserved_110[860] = 0x00,
	.development_nvm_reserved_110[861] = 0x00,
	.development_nvm_reserved_110[862] = 0x00,
	.development_nvm_reserved_110[863] = 0x00,
	.development_nvm_reserved_110[864] = 0x00,
	.development_nvm_reserved_110[865] = 0x00,
	.development_nvm_reserved_110[866] = 0x00,
	.development_nvm_reserved_110[867] = 0x00,
	.development_nvm_reserved_110[868] = 0x00,
	.development_nvm_reserved_110[869] = 0x00,
	.development_nvm_reserved_110[870] = 0x00,
	.development_nvm_reserved_110[871] = 0x00,
	.development_nvm_reserved_110[872] = 0x00,
	.development_nvm_reserved_110[873] = 0x00,
	.development_nvm_reserved_110[874] = 0x00,
	.development_nvm_reserved_110[875] = 0x00,
	.development_nvm_reserved_110[876] = 0x00,
	.development_nvm_reserved_110[877] = 0x00,
	.development_nvm_reserved_110[878] = 0x00,
	.development_nvm_reserved_110[879] = 0x00,
	.development_nvm_reserved_110[880] = 0x00,
	.development_nvm_reserved_110[881] = 0x00,
	.development_nvm_reserved_110[882] = 0x00,
	.development_nvm_reserved_110[883] = 0x00,
	.development_nvm_reserved_110[884] = 0x00,
	.development_nvm_reserved_110[885] = 0x00,
	.development_nvm_reserved_110[886] = 0x00,
	.development_nvm_reserved_110[887] = 0x00,
	.development_nvm_reserved_110[888] = 0x00,
	.development_nvm_reserved_110[889] = 0x00,
	.development_nvm_reserved_110[890] = 0x00,
	.development_nvm_reserved_110[891] = 0x00,
	.development_nvm_reserved_110[892] = 0x00,
	.development_nvm_reserved_110[893] = 0x00,
	.development_nvm_reserved_110[894] = 0x00,
	.development_nvm_reserved_110[895] = 0x00,
	.development_nvm_reserved_110[896] = 0x00,
	.development_nvm_reserved_110[897] = 0x00,
	.development_nvm_reserved_110[898] = 0x00,
	.development_nvm_reserved_110[899] = 0x00,
	.development_nvm_reserved_110[900] = 0x00,
	.development_nvm_reserved_110[901] = 0x00,
	.development_nvm_reserved_110[902] = 0x00,
	.development_nvm_reserved_110[903] = 0x00,
	.development_nvm_reserved_110[904] = 0x00,
	.development_nvm_reserved_110[905] = 0x00,
	.development_nvm_reserved_110[906] = 0x00,
	.development_nvm_reserved_110[907] = 0x00,
	.development_nvm_reserved_110[908] = 0x00,
	.development_nvm_reserved_110[909] = 0x00,
	.Vin = { 0x00,0x01,0x02 },
	.OemSetting_reserved_21[0] = 0x00,
	.OemSetting_reserved_21[1] = 0x00,
	.OemSetting_reserved_21[2] = 0x00,
	.OemSetting_reserved_21[3] = 0x00,
	.OemSetting_reserved_21[4] = 0x00,
	.OemSetting_reserved_21[5] = 0x00,
	.OemSetting_reserved_21[6] = 0x00,
	.OemSetting_reserved_21[7] = 0x00,
	.OemSetting_reserved_21[8] = 0x00,
	.OemSetting_reserved_21[9] = 0x00,
	.OemSetting_reserved_21[10] = 0x00,
	.OemSetting_reserved_21[11] = 0x00,
	.OemSetting_reserved_21[12] = 0x00,
	.OemSetting_reserved_21[13] = 0x00,
	.OemSetting_reserved_21[14] = 0x00,
	.OemSetting_reserved_21[15] = 0x00,
	.OemSetting_reserved_21[16] = 0x00,
	.OemSetting_reserved_21[17] = 0x00,
	.OemSetting_reserved_21[18] = 0x00,
	.OemSetting_reserved_21[19] = 0x00,
	.OemSetting_reserved_21[20] = 0x00,
	.OemSetting_reserved_21[21] = 0x00,
	.OemSetting_reserved_21[22] = 0x00,
	.OemSetting_reserved_21[23] = 0x00,
	.OemSetting_reserved_21[24] = 0x00,
	.OemSetting_reserved_21[25] = 0x00,
	.OemSetting_reserved_21[26] = 0x00,
	.OemSetting_reserved_21[27] = 0x00,
	.OemSetting_reserved_21[28] = 0x00,
	.OemSetting_reserved_21[29] = 0x00,
	.OemSetting_reserved_21[30] = 0x00,
	.OemSetting_reserved_21[31] = 0x00,
	.OemSetting_reserved_21[32] = 0x00,
	.OemSetting_reserved_21[33] = 0x00,
	.OemSetting_reserved_21[34] = 0x00,
	.OemSetting_reserved_21[35] = 0x00,
	.OemSetting_reserved_21[36] = 0x00,
	.OemSetting_reserved_21[37] = 0x00,
	.OemSetting_reserved_21[38] = 0x00,
	.OemSetting_reserved_21[39] = 0x00,
	.OemSetting_reserved_21[40] = 0x00,
	.OemSetting_reserved_21[41] = 0x00,
	.OemSetting_reserved_21[42] = 0x00,
	.OemSetting_reserved_21[43] = 0x00,
	.OemSetting_reserved_21[44] = 0x00,
	.OemSetting_reserved_21[45] = 0x00,
	.OemSetting_reserved_21[46] = 0x00,
	.OemSetting_reserved_21[47] = 0x00,
	.OemSetting_reserved_21[48] = 0x00,
	.OemSetting_reserved_21[49] = 0x00,
	.OemSetting_reserved_21[50] = 0x00,
	.OemSetting_reserved_21[51] = 0x00,
	.OemSetting_reserved_21[52] = 0x00,
	.OemSetting_reserved_21[53] = 0x00,
	.OemSetting_reserved_21[54] = 0x00,
	.OemSetting_reserved_21[55] = 0x00,
	.OemSetting_reserved_21[56] = 0x00,
	.OemSetting_reserved_21[57] = 0x00,
	.OemSetting_reserved_21[58] = 0x00,
	.OemSetting_reserved_21[59] = 0x00,
	.OemSetting_reserved_21[60] = 0x00,
	.OemSetting_reserved_21[61] = 0x00,
	.OemSetting_reserved_21[62] = 0x00,
	.OemSetting_reserved_21[63] = 0x00,
	.OemSetting_reserved_21[64] = 0x00,
	.OemSetting_reserved_21[65] = 0x00,
	.OemSetting_reserved_21[66] = 0x00,
	.OemSetting_reserved_21[67] = 0x00,
	.OemSetting_reserved_21[68] = 0x00,
	.OemSetting_reserved_21[69] = 0x00,
	.OemSetting_reserved_21[70] = 0x00,
	.OemSetting_reserved_21[71] = 0x00,
	.OemSetting_reserved_21[72] = 0x00,
	.OemSetting_reserved_21[73] = 0x00,
	.OemSetting_reserved_21[74] = 0x00,
	.OemSetting_reserved_21[75] = 0x00,
	.OemSetting_reserved_21[76] = 0x00,
	.OemSetting_reserved_21[77] = 0x00,
	.OemSetting_reserved_21[78] = 0x00,
	.OemSetting_reserved_21[79] = 0x00,
	.OemSetting_reserved_21[80] = 0x00,
	.OemSetting_reserved_21[81] = 0x00,
	.OemSetting_reserved_21[82] = 0x00,
	.OemSetting_reserved_21[83] = 0x00,
	.OemSetting_reserved_21[84] = 0x00,
	.OemSetting_reserved_21[85] = 0x00,
	.OemSetting_reserved_21[86] = 0x00,
	.OemSetting_reserved_21[87] = 0x00,
	.OemSetting_reserved_21[88] = 0x00,
	.OemSetting_reserved_21[89] = 0x00,
	.OemSetting_reserved_21[90] = 0x00,
	.OemSetting_reserved_21[91] = 0x00,
	.OemSetting_reserved_21[92] = 0x00,
	.OemSetting_reserved_21[93] = 0x00,
	.OemSetting_reserved_21[94] = 0x00,
	.OemSetting_reserved_21[95] = 0x00,
	.OemSetting_reserved_21[96] = 0x00,
	.OemSetting_reserved_21[97] = 0x00,
	.OemSetting_reserved_21[98] = 0x00,
	.OemSetting_reserved_21[99] = 0x00,
	.OemSetting_reserved_21[100] = 0x00,
	.OemSetting_reserved_21[101] = 0x00,
	.OemSetting_reserved_21[102] = 0x00,
	.OemSetting_reserved_21[103] = 0x00,
	.OemSetting_reserved_21[104] = 0x00,
	.OemSetting_reserved_21[105] = 0x00,
	.OemSetting_reserved_21[106] = 0x00,
	.OemSetting_reserved_21[107] = 0x00,
	.OemSetting_reserved_21[108] = 0x00,
	.OemSetting_reserved_21[109] = 0x00,
	.OemSetting_reserved_21[110] = 0x00,
	.OemSetting_reserved_21[111] = 0x00,
	.OemSetting_reserved_21[112] = 0x00,
	.OemSetting_reserved_21[113] = 0x00,
	.OemSetting_reserved_21[114] = 0x00,
	.OemSetting_reserved_21[115] = 0x00,
	.OemSetting_reserved_21[116] = 0x00,
	.OemSetting_reserved_21[117] = 0x00,
	.OemSetting_reserved_21[118] = 0x00,
	.OemSetting_reserved_21[119] = 0x00,
	.OemSetting_reserved_21[120] = 0x00,
	.OemSetting_reserved_21[121] = 0x00,
	.OemSetting_reserved_21[122] = 0x00,
	.OemSetting_reserved_21[123] = 0x00,
	.OemSetting_reserved_21[124] = 0x00,
	.OemSetting_reserved_21[125] = 0x00,
	.OemSetting_reserved_21[126] = 0x00,
	.OemSetting_reserved_21[127] = 0x00,
	.OemSetting_reserved_21[128] = 0x00,
	.OemSetting_reserved_21[129] = 0x00,
	.OemSetting_reserved_21[130] = 0x00,
	.OemSetting_reserved_21[131] = 0x00,
	.OemSetting_reserved_21[132] = 0x00,
	.OemSetting_reserved_21[133] = 0x00,
	.OemSetting_reserved_21[134] = 0x00,
	.OemSetting_reserved_21[135] = 0x00,
	.OemSetting_reserved_21[136] = 0x00,
	.OemSetting_reserved_21[137] = 0x00,
	.OemSetting_reserved_21[138] = 0x00,
	.OemSetting_reserved_21[139] = 0x00,
	.OemSetting_reserved_21[140] = 0x00,
	.OemSetting_reserved_21[141] = 0x00,
	.OemSetting_reserved_21[142] = 0x00,
	.OemSetting_reserved_21[143] = 0x00,
	.OemSetting_reserved_21[144] = 0x00,
	.OemSetting_reserved_21[145] = 0x00,
	.OemSetting_reserved_21[146] = 0x00,
	.OemSetting_reserved_21[147] = 0x00,
	.OemSetting_reserved_21[148] = 0x00,
	.OemSetting_reserved_21[149] = 0x00,
	.OemSetting_reserved_21[150] = 0x00,
	.OemSetting_reserved_21[151] = 0x00,
	.OemSetting_reserved_21[152] = 0x00,
	.OemSetting_reserved_21[153] = 0x00,
	.OemSetting_reserved_21[154] = 0x00,
	.OemSetting_reserved_21[155] = 0x00,
	.OemSetting_reserved_21[156] = 0x00,
	.OemSetting_reserved_21[157] = 0x00,
	.OemSetting_reserved_21[158] = 0x00,
	.OemSetting_reserved_21[159] = 0x00,
	.OemSetting_reserved_21[160] = 0x00,
	.OemSetting_reserved_21[161] = 0x00,
	.OemSetting_reserved_21[162] = 0x00,
	.OemSetting_reserved_21[163] = 0x00,
	.OemSetting_reserved_21[164] = 0x00,
	.OemSetting_reserved_21[165] = 0x00,
	.OemSetting_reserved_21[166] = 0x00,
	.OemSetting_reserved_21[167] = 0x00,
	.OemSetting_reserved_21[168] = 0x00,
	.OemSetting_reserved_21[169] = 0x00,
	.OemSetting_reserved_21[170] = 0x00,
	.OemSetting_reserved_21[171] = 0x00,
	.OemSetting_reserved_21[172] = 0x00,
	.OemSetting_reserved_21[173] = 0x00,
	.OemSetting_reserved_21[174] = 0x00,
	.OemSetting_reserved_21[175] = 0x00,
	.OemSetting_reserved_21[176] = 0x00,
	.OemSetting_reserved_21[177] = 0x00,
	.OemSetting_reserved_21[178] = 0x00,
	.OemSetting_reserved_21[179] = 0x00,
	.OemSetting_reserved_21[180] = 0x00,
	.OemSetting_reserved_21[181] = 0x00,
	.OemSetting_reserved_21[182] = 0x00,
	.OemSetting_reserved_21[183] = 0x00,
	.OemSetting_reserved_21[184] = 0x00,
	.OemSetting_reserved_21[185] = 0x00,
	.OemSetting_reserved_21[186] = 0x00,
	.OemSetting_reserved_21[187] = 0x00,
	.OemSetting_reserved_21[188] = 0x00,
	.OemSetting_reserved_21[189] = 0x00,
	.OemSetting_reserved_21[190] = 0x00,
	.OemSetting_reserved_21[191] = 0x00,
	.OemSetting_reserved_21[192] = 0x00,
	.OemSetting_reserved_21[193] = 0x00,
	.OemSetting_reserved_21[194] = 0x00,
	.OemSetting_reserved_21[195] = 0x00,
	.OemSetting_reserved_21[196] = 0x00,
	.OemSetting_reserved_21[197] = 0x00,
	.OemSetting_reserved_21[198] = 0x00,
	.OemSetting_reserved_21[199] = 0x00,
	.OemSetting_reserved_21[200] = 0x00,
	.OemSetting_reserved_21[201] = 0x00,
	.OemSetting_reserved_21[202] = 0x00,
	.OemSetting_reserved_21[203] = 0x00,
	.OemSetting_reserved_21[204] = 0x00,
	.OemSetting_reserved_21[205] = 0x00,
	.OemSetting_reserved_21[206] = 0x00,
	.OemSetting_reserved_21[207] = 0x00,
	.OemSetting_reserved_21[208] = 0x00,
	.OemSetting_reserved_21[209] = 0x00,
	.OemSetting_reserved_21[210] = 0x00,
	.OemSetting_reserved_21[211] = 0x00,
	.OemSetting_reserved_21[212] = 0x00,
	.OemSetting_reserved_21[213] = 0x00,
	.OemSetting_reserved_21[214] = 0x00,
	.OemSetting_reserved_21[215] = 0x00,
	.OemSetting_reserved_21[216] = 0x00,
	.OemSetting_reserved_21[217] = 0x00,
	.OemSetting_reserved_21[218] = 0x00,
	.OemSetting_reserved_21[219] = 0x00,
	.OemSetting_reserved_21[220] = 0x00,
	.OemSetting_reserved_21[221] = 0x00,
	.OemSetting_reserved_21[222] = 0x00,
	.OemSetting_reserved_21[223] = 0x00,
	.OemSetting_reserved_21[224] = 0x00,
	.OemSetting_reserved_21[225] = 0x00,
	.OemSetting_reserved_21[226] = 0x00,
	.OemSetting_reserved_21[227] = 0x00,
	.OemSetting_reserved_21[228] = 0x00,
	.OemSetting_reserved_21[229] = 0x00,
	.OemSetting_reserved_21[230] = 0x00,
	.AutoSyncTimeWithGps = 1,
	.ScreenBackLightValOnDay = 10,
	.ScreenBackLightValOnNight = 10,
	.HistoryAverFuelCons = { 10 },
	.reserved_13_FuelResistance[0] = 0x00,
	.reserved_13_FuelResistance[1] = 0x00,
	.reserved_13_FuelResistance[2] = 0x00,
	.user_setting_reserved_20[0] = 0x00,
	.user_setting_reserved_20[1] = 0x00,
	.user_setting_reserved_20[2] = 0x00,
	.user_setting_reserved_20[3] = 0x00,
	.user_setting_reserved_20[4] = 0x00,
	.user_setting_reserved_20[5] = 0x00,
	.user_setting_reserved_20[6] = 0x00,
	.user_setting_reserved_20[7] = 0x00,
	.user_setting_reserved_20[8] = 0x00,
	.user_setting_reserved_20[9] = 0x00,
	.user_setting_reserved_20[10] = 0x00,
	.user_setting_reserved_20[11] = 0x00,
	.user_setting_reserved_20[12] = 0x00,
	.user_setting_reserved_20[13] = 0x00,
	.user_setting_reserved_20[14] = 0x00,
	.user_setting_reserved_20[15] = 0x00,
	.user_setting_reserved_20[16] = 0x00,
	.user_setting_reserved_20[17] = 0x00,
	.user_setting_reserved_20[18] = 0x00,
	.user_setting_reserved_20[19] = 0x00,
	.user_setting_reserved_20[20] = 0x00,
	.user_setting_reserved_20[21] = 0x00,
	.user_setting_reserved_20[22] = 0x00,
	.user_setting_reserved_20[23] = 0x00,
	.user_setting_reserved_20[24] = 0x00,
	.user_setting_reserved_20[25] = 0x00,
	.user_setting_reserved_20[26] = 0x00,
	.user_setting_reserved_20[27] = 0x00,
	.user_setting_reserved_20[28] = 0x00,
	.user_setting_reserved_20[29] = 0x00,
	.user_setting_reserved_20[30] = 0x00,
	.user_setting_reserved_20[31] = 0x00,
	.user_setting_reserved_20[32] = 0x00,
	.user_setting_reserved_20[33] = 0x00,
	.user_setting_reserved_20[34] = 0x00,
	.user_setting_reserved_20[35] = 0x00,
	.user_setting_reserved_20[36] = 0x00,
	.user_setting_reserved_20[37] = 0x00,
	.user_setting_reserved_20[38] = 0x00,
	.user_setting_reserved_20[39] = 0x00,
	.user_setting_reserved_20[40] = 0x00,
	.user_setting_reserved_20[41] = 0x00,
	.user_setting_reserved_20[42] = 0x00,
	.user_setting_reserved_20[43] = 0x00,
	.user_setting_reserved_20[44] = 0x00,
	.user_setting_reserved_20[45] = 0x00,
	.user_setting_reserved_20[46] = 0x00,
	.user_setting_reserved_20[47] = 0x00,
	.user_setting_reserved_20[48] = 0x00,
	.user_setting_reserved_20[49] = 0x00,
	.user_setting_reserved_20[50] = 0x00,
	.user_setting_reserved_20[51] = 0x00,
	.user_setting_reserved_20[52] = 0x00,
	.user_setting_reserved_20[53] = 0x00,
	.user_setting_reserved_20[54] = 0x00,
	.user_setting_reserved_20[55] = 0x00,
	.user_setting_reserved_20[56] = 0x00,
	.user_setting_reserved_20[57] = 0x00,
	.user_setting_reserved_20[58] = 0x00,
	.user_setting_reserved_20[59] = 0x00,
	.user_setting_reserved_20[60] = 0x00,
	.user_setting_reserved_20[61] = 0x00,
	.user_setting_reserved_20[62] = 0x00,
	.user_setting_reserved_20[63] = 0x00,
	.user_setting_reserved_20[64] = 0x00,
	.user_setting_reserved_20[65] = 0x00,
	.user_setting_reserved_20[66] = 0x00,
	.user_setting_reserved_20[67] = 0x00,
	.user_setting_reserved_20[68] = 0x00,
	.user_setting_reserved_20[69] = 0x00,
	.user_setting_reserved_20[70] = 0x00,
	.user_setting_reserved_20[71] = 0x00,
	.user_setting_reserved_20[72] = 0x00,
	.user_setting_reserved_20[73] = 0x00,
	.user_setting_reserved_20[74] = 0x00,
	.user_setting_reserved_20[75] = 0x00,
	.user_setting_reserved_20[76] = 0x00,
	.user_setting_reserved_20[77] = 0x00,
	.user_setting_reserved_20[78] = 0x00,
	.user_setting_reserved_20[79] = 0x00,
	.user_setting_reserved_20[80] = 0x00,
	.user_setting_reserved_20[81] = 0x00,
	.user_setting_reserved_20[82] = 0x00,
	.user_setting_reserved_20[83] = 0x00,
	.user_setting_reserved_20[84] = 0x00,
	.user_setting_reserved_20[85] = 0x00,
	.user_setting_reserved_20[86] = 0x00,
	.user_setting_reserved_20[87] = 0x00,
	.user_setting_reserved_20[88] = 0x00,
	.user_setting_reserved_20[89] = 0x00,
	.user_setting_reserved_20[90] = 0x00,
	.user_setting_reserved_20[91] = 0x00,
	.user_setting_reserved_20[92] = 0x00,
	.user_setting_reserved_20[93] = 0x00,
	.user_setting_reserved_20[94] = 0x00,
	.user_setting_reserved_20[95] = 0x00,
	.user_setting_reserved_20[96] = 0x00,
	.user_setting_reserved_20[97] = 0x00,
	.user_setting_reserved_20[98] = 0x00,
	.user_setting_reserved_20[99] = 0x00,
	.user_setting_reserved_20[100] = 0x00,
	.user_setting_reserved_20[101] = 0x00,
	.user_setting_reserved_20[102] = 0x00,
	.user_setting_reserved_20[103] = 0x00,
	.user_setting_reserved_20[104] = 0x00,
	.user_setting_reserved_20[105] = 0x00,
	.user_setting_reserved_20[106] = 0x00,
	.user_setting_reserved_20[107] = 0x00,
	.user_setting_reserved_20[108] = 0x00,
	.user_setting_reserved_20[109] = 0x00,
	.user_setting_reserved_20[110] = 0x00,
	.user_setting_reserved_20[111] = 0x00,
	.user_setting_reserved_20[112] = 0x00,
	.user_setting_reserved_20[113] = 0x00,
	.user_setting_reserved_20[114] = 0x00,
	.user_setting_reserved_20[115] = 0x00,
	.user_setting_reserved_20[116] = 0x00,
	.user_setting_reserved_20[117] = 0x00,
	.user_setting_reserved_20[118] = 0x00,
	.user_setting_reserved_20[119] = 0x00,
	.user_setting_reserved_20[120] = 0x00,
	.user_setting_reserved_20[121] = 0x00,
	.user_setting_reserved_20[122] = 0x00,
	.user_setting_reserved_20[123] = 0x00,
	.user_setting_reserved_20[124] = 0x00,
	.user_setting_reserved_20[125] = 0x00,
	.user_setting_reserved_20[126] = 0x00,
	.user_setting_reserved_20[127] = 0x00,
	.user_setting_reserved_20[128] = 0x00,
	.user_setting_reserved_20[129] = 0x00,
	.user_setting_reserved_20[130] = 0x00,
	.user_setting_reserved_20[131] = 0x00,
	.user_setting_reserved_20[132] = 0x00,
	.user_setting_reserved_20[133] = 0x00,
	.user_setting_reserved_20[134] = 0x00,
	.user_setting_reserved_20[135] = 0x00,
	.user_setting_reserved_20[136] = 0x00,
	.user_setting_reserved_20[137] = 0x00,
	.user_setting_reserved_20[138] = 0x00,
	.user_setting_reserved_20[139] = 0x00,
	.user_setting_reserved_20[140] = 0x00,
	.user_setting_reserved_20[141] = 0x00,
	.user_setting_reserved_20[142] = 0x00,
	.user_setting_reserved_20[143] = 0x00,
	.user_setting_reserved_20[144] = 0x00,
	.user_setting_reserved_20[145] = 0x00,
	.user_setting_reserved_20[146] = 0x00,
	.user_setting_reserved_20[147] = 0x00,
	.user_setting_reserved_20[148] = 0x00,
	.user_setting_reserved_20[149] = 0x00,
	.user_setting_reserved_20[150] = 0x00,
	.user_setting_reserved_20[151] = 0x00,
	.user_setting_reserved_20[152] = 0x00,
	.user_setting_reserved_20[153] = 0x00,
	.user_setting_reserved_20[154] = 0x00,
	.user_setting_reserved_20[155] = 0x00,
	.user_setting_reserved_20[156] = 0x00,
	.user_setting_reserved_20[157] = 0x00,
	.user_setting_reserved_20[158] = 0x00,
	.user_setting_reserved_20[159] = 0x00,
	.user_setting_reserved_20[160] = 0x00,
	.user_setting_reserved_20[161] = 0x00,
	.user_setting_reserved_20[162] = 0x00,
	.user_setting_reserved_20[163] = 0x00,
	.user_setting_reserved_20[164] = 0x00,
	.user_setting_reserved_20[165] = 0x00,
	.user_setting_reserved_20[166] = 0x00,
	.user_setting_reserved_20[167] = 0x00,
	.user_setting_reserved_20[168] = 0x00,
	.user_setting_reserved_20[169] = 0x00,
	.user_setting_reserved_20[170] = 0x00,
	.user_setting_reserved_20[171] = 0x00,
	.user_setting_reserved_20[172] = 0x00,
	.user_setting_reserved_20[173] = 0x00,
	.user_setting_reserved_20[174] = 0x00,
	.user_setting_reserved_20[175] = 0x00,
	.user_setting_reserved_20[176] = 0x00,
	.user_setting_reserved_20[177] = 0x00,
	.user_setting_reserved_20[178] = 0x00,
	.user_setting_reserved_20[179] = 0x00,
	.user_setting_reserved_20[180] = 0x00,
	.user_setting_reserved_20[181] = 0x00,
	.user_setting_reserved_20[182] = 0x00,
	.user_setting_reserved_20[183] = 0x00,
	.user_setting_reserved_20[184] = 0x00,
	.user_setting_reserved_20[185] = 0x00,
	.user_setting_reserved_20[186] = 0x00,
	.user_setting_reserved_20[187] = 0x00,
	.user_setting_reserved_20[188] = 0x00,
	.user_setting_reserved_20[189] = 0x00,
	.user_setting_reserved_20[190] = 0x00,
	.user_setting_reserved_20[191] = 0x00,
	.user_setting_reserved_20[192] = 0x00,
	.user_setting_reserved_20[193] = 0x00,
	.user_setting_reserved_20[194] = 0x00,
	.user_setting_reserved_20[195] = 0x00,
	.user_setting_reserved_20[196] = 0x00,
	.user_setting_reserved_20[197] = 0x00,
	.user_setting_reserved_20[198] = 0x00,
	.user_setting_reserved_20[199] = 0x00,
	.user_setting_reserved_20[200] = 0x00,
	.user_setting_reserved_20[201] = 0x00,
	.user_setting_reserved_20[202] = 0x00,
	.user_setting_reserved_20[203] = 0x00,
	.user_setting_reserved_20[204] = 0x00,
	.user_setting_reserved_20[205] = 0x00,
	.user_setting_reserved_20[206] = 0x00,
	.user_setting_reserved_20[207] = 0x00,
	.user_setting_reserved_20[208] = 0x00,
	.user_setting_reserved_20[209] = 0x00,
	.user_setting_reserved_20[210] = 0x00,
	.user_setting_reserved_20[211] = 0x00,
	.user_setting_reserved_20[212] = 0x00,
	.user_setting_reserved_20[213] = 0x00,
	.user_setting_reserved_20[214] = 0x00,
	.user_setting_reserved_20[215] = 0x00,
	.user_setting_reserved_20[216] = 0x00,
	.user_setting_reserved_20[217] = 0x00,
	.user_setting_reserved_20[218] = 0x00,
	.user_setting_reserved_20[219] = 0x00,
	.user_setting_reserved_20[220] = 0x00,
	.user_setting_reserved_20[221] = 0x00,
	.user_setting_reserved_20[222] = 0x00,
	.user_setting_reserved_20[223] = 0x00,
	.user_setting_reserved_20[224] = 0x00,
	.user_setting_reserved_20[225] = 0x00,
	.user_setting_reserved_20[226] = 0x00,
	.user_setting_reserved_20[227] = 0x00,
	.user_setting_reserved_20[228] = 0x00,
	.user_setting_reserved_20[229] = 0x00,
	.user_setting_reserved_20[230] = 0x00,
	.user_setting_reserved_20[231] = 0x00,
	.DTC_INFO_reserved_14[0] = 0x00,
	.DTC_INFO_reserved_14[1] = 0x00,
	.DTC_INFO_reserved_14[2] = 0x00,
	.DTC_INFO_reserved_14[3] = 0x00,
	.DTC_INFO_reserved_14[4] = 0x00,
	.DTC_INFO_reserved_14[5] = 0x00,
	.DTC_INFO_reserved_14[6] = 0x00,
	.DTC_INFO_reserved_14[7] = 0x00,
	.DTC_INFO_reserved_14[8] = 0x00,
	.DTC_INFO_reserved_14[9] = 0x00,
	.DTC_INFO_reserved_14[10] = 0x00,
	.DTC_INFO_reserved_14[11] = 0x00,
	.DTC_INFO_reserved_14[12] = 0x00,
	.DTC_INFO_reserved_14[13] = 0x00,
	.DTC_INFO_reserved_14[14] = 0x00,
	.DTC_INFO_reserved_14[15] = 0x00,
	.DTC_INFO_reserved_14[16] = 0x00,
	.DTC_INFO_reserved_14[17] = 0x00,
	.DTC_INFO_reserved_14[18] = 0x00,
	.DTC_INFO_reserved_14[19] = 0x00,
	.DTC_INFO_reserved_14[20] = 0x00,
	.DTC_INFO_reserved_14[21] = 0x00,
	.DTC_INFO_reserved_14[22] = 0x00,
	.DTC_INFO_reserved_14[23] = 0x00,
	.DTC_INFO_reserved_14[24] = 0x00,
	.DTC_INFO_reserved_14[25] = 0x00,
	.DTC_INFO_reserved_14[26] = 0x00,
	.DTC_INFO_reserved_14[27] = 0x00,
	.DTC_INFO_reserved_14[28] = 0x00,
	.DTC_INFO_reserved_14[29] = 0x00,
	.DTC_INFO_reserved_14[30] = 0x00,
	.DTC_INFO_reserved_14[31] = 0x00,
	.DTC_INFO_reserved_14[32] = 0x00,
	.DTC_INFO_reserved_14[33] = 0x00,
	.DTC_INFO_reserved_14[34] = 0x00,
	.DTC_INFO_reserved_14[35] = 0x00,
	.DTC_INFO_reserved_14[36] = 0x00,
	.DTC_INFO_reserved_14[37] = 0x00,
	.DTC_INFO_reserved_14[38] = 0x00,
	.DTC_INFO_reserved_14[39] = 0x00,
	.DTC_INFO_reserved_14[40] = 0x00,
	.DTC_INFO_reserved_14[41] = 0x00,
	.DTC_INFO_reserved_14[42] = 0x00,
	.DTC_INFO_reserved_14[43] = 0x00,
	.DTC_INFO_reserved_14[44] = 0x00,
	.DTC_INFO_reserved_14[45] = 0x00,
	.DTC_INFO_reserved_14[46] = 0x00,
	.DTC_INFO_reserved_14[47] = 0x00,
	.DTC_INFO_reserved_14[48] = 0x00,
	.DTC_INFO_reserved_14[49] = 0x00,
	.DTC_INFO_reserved_14[50] = 0x00,
	.DTC_INFO_reserved_14[51] = 0x00,
	.DTC_INFO_reserved_14[52] = 0x00,
	.DTC_INFO_reserved_14[53] = 0x00,
	.DTC_INFO_reserved_14[54] = 0x00,
	.DTC_INFO_reserved_14[55] = 0x00,
	.DTC_INFO_reserved_14[56] = 0x00,
	.DTC_INFO_reserved_14[57] = 0x00,
	.DTC_INFO_reserved_14[58] = 0x00,
	.DTC_INFO_reserved_14[59] = 0x00,
	.DTC_INFO_reserved_14[60] = 0x00,
	.DTC_INFO_reserved_14[61] = 0x00,
	.DTC_INFO_reserved_14[62] = 0x00,
	.DTC_INFO_reserved_14[63] = 0x00,
	.DTC_INFO_reserved_14[64] = 0x00,
	.DTC_INFO_reserved_14[65] = 0x00,
	.DTC_INFO_reserved_14[66] = 0x00,
	.DTC_INFO_reserved_14[67] = 0x00,
	.DTC_INFO_reserved_14[68] = 0x00,
	.DTC_INFO_reserved_14[69] = 0x00,
	.DTC_INFO_reserved_14[70] = 0x00,
	.DTC_INFO_reserved_14[71] = 0x00,
	.DTC_INFO_reserved_14[72] = 0x00,
	.DTC_INFO_reserved_14[73] = 0x00,
	.DTC_INFO_reserved_14[74] = 0x00,
	.DTC_INFO_reserved_14[75] = 0x00,
	.DTC_INFO_reserved_14[76] = 0x00,
	.DTC_INFO_reserved_14[77] = 0x00,
	.DTC_INFO_reserved_14[78] = 0x00,
	.DTC_INFO_reserved_14[79] = 0x00,
	.DTC_INFO_reserved_14[80] = 0x00,
	.DTC_INFO_reserved_14[81] = 0x00,
	.DTC_INFO_reserved_14[82] = 0x00,
	.DTC_INFO_reserved_14[83] = 0x00,
	.DTC_INFO_reserved_14[84] = 0x00,
	.DTC_INFO_reserved_14[85] = 0x00,
	.DTC_INFO_reserved_14[86] = 0x00,
	.DTC_INFO_reserved_14[87] = 0x00,
	.DTC_INFO_reserved_14[88] = 0x00,
	.DTC_INFO_reserved_14[89] = 0x00,
	.DTC_INFO_reserved_14[90] = 0x00,
	.DTC_INFO_reserved_14[91] = 0x00,
	.DTC_INFO_reserved_14[92] = 0x00,
	.DTC_INFO_reserved_14[93] = 0x00,
	.DTC_INFO_reserved_14[94] = 0x00,
	.DTC_INFO_reserved_14[95] = 0x00,
	.DTC_INFO_reserved_14[96] = 0x00,
	.DTC_INFO_reserved_14[97] = 0x00,
	.DTC_INFO_reserved_14[98] = 0x00,
	.DTC_INFO_reserved_14[99] = 0x00,
	.DTC_INFO_reserved_14[100] = 0x00,
	.DTC_INFO_reserved_14[101] = 0x00,
	.DTC_INFO_reserved_14[102] = 0x00,
	.DTC_INFO_reserved_14[103] = 0x00,
	.DTC_INFO_reserved_14[104] = 0x00,
	.DTC_INFO_reserved_14[105] = 0x00,
	.DTC_INFO_reserved_14[106] = 0x00,
	.DTC_INFO_reserved_14[107] = 0x00,
	.DTC_INFO_reserved_14[108] = 0x00,
	.DTC_INFO_reserved_14[109] = 0x00,
	.DTC_INFO_reserved_14[110] = 0x00,
	.DTC_INFO_reserved_14[111] = 0x00,
	.DTC_INFO_reserved_14[112] = 0x00,
	.DTC_INFO_reserved_14[113] = 0x00,
	.DTC_INFO_reserved_14[114] = 0x00,
	.DTC_INFO_reserved_14[115] = 0x00,
	.DTC_INFO_reserved_14[116] = 0x00,
	.DTC_INFO_reserved_14[117] = 0x00,
	.DTC_INFO_reserved_14[118] = 0x00,
	.DTC_INFO_reserved_14[119] = 0x00,
	.DTC_INFO_reserved_14[120] = 0x00,
	.DTC_INFO_reserved_14[121] = 0x00,
	.DTC_INFO_reserved_14[122] = 0x00,
	.DTC_INFO_reserved_14[123] = 0x00,
	.DTC_INFO_reserved_14[124] = 0x00,
	.DTC_INFO_reserved_14[125] = 0x00,
	.DTC_INFO_reserved_14[126] = 0x00,
	.DTC_INFO_reserved_14[127] = 0x00,
	.DTC_INFO_reserved_14[128] = 0x00,
	.DTC_INFO_reserved_14[129] = 0x00,
	.DTC_INFO_reserved_14[130] = 0x00,
	.DTC_INFO_reserved_14[131] = 0x00,
	.DTC_INFO_reserved_14[132] = 0x00,
	.DTC_INFO_reserved_14[133] = 0x00,
	.DTC_INFO_reserved_14[134] = 0x00,
	.DTC_INFO_reserved_14[135] = 0x00,
	.DTC_INFO_reserved_14[136] = 0x00,
	.DTC_INFO_reserved_14[137] = 0x00,
	.DTC_INFO_reserved_14[138] = 0x00,
	.DTC_INFO_reserved_14[139] = 0x00,
	.DTC_INFO_reserved_14[140] = 0x00,
	.DTC_INFO_reserved_14[141] = 0x00,
	.DTC_INFO_reserved_14[142] = 0x00,
	.DTC_INFO_reserved_14[143] = 0x00,
	.DTC_INFO_reserved_14[144] = 0x00,
	.DTC_INFO_reserved_14[145] = 0x00,
	.DTC_INFO_reserved_14[146] = 0x00,
	.DTC_INFO_reserved_14[147] = 0x00,
	.DTC_INFO_reserved_14[148] = 0x00,
	.DTC_INFO_reserved_14[149] = 0x00,
	.DTC_INFO_reserved_14[150] = 0x00,
	.DTC_INFO_reserved_14[151] = 0x00,
	.DTC_INFO_reserved_14[152] = 0x00,
	.DTC_INFO_reserved_14[153] = 0x00,
	.DTC_INFO_reserved_14[154] = 0x00,
	.DTC_INFO_reserved_14[155] = 0x00,
	.DTC_INFO_reserved_14[156] = 0x00,
	.DTC_INFO_reserved_14[157] = 0x00,
	.DTC_INFO_reserved_14[158] = 0x00,
	.DTC_INFO_reserved_14[159] = 0x00,
	.DTC_INFO_reserved_14[160] = 0x00,
	.DTC_INFO_reserved_14[161] = 0x00,
	.DTC_INFO_reserved_14[162] = 0x00,
	.DTC_INFO_reserved_14[163] = 0x00,
	.DTC_INFO_reserved_14[164] = 0x00,
	.DTC_INFO_reserved_14[165] = 0x00,
	.DTC_INFO_reserved_14[166] = 0x00,
	.DTC_INFO_reserved_14[167] = 0x00,
	.DTC_INFO_reserved_14[168] = 0x00,
	.DTC_INFO_reserved_14[169] = 0x00,
	.DTC_INFO_reserved_14[170] = 0x00,
	.DTC_INFO_reserved_14[171] = 0x00,
	.DTC_INFO_reserved_14[172] = 0x00,
	.DTC_INFO_reserved_14[173] = 0x00,
	.DTC_INFO_reserved_14[174] = 0x00,
	.DTC_INFO_reserved_14[175] = 0x00,
	.DTC_INFO_reserved_14[176] = 0x00,
	.DTC_INFO_reserved_14[177] = 0x00,
	.DTC_INFO_reserved_14[178] = 0x00,
	.DTC_INFO_reserved_14[179] = 0x00,
	.DTC_INFO_reserved_14[180] = 0x00,
	.DTC_INFO_reserved_14[181] = 0x00,
	.DTC_INFO_reserved_14[182] = 0x00,
	.DTC_INFO_reserved_14[183] = 0x00,
	.DTC_INFO_reserved_14[184] = 0x00,
	.DTC_INFO_reserved_14[185] = 0x00,
	.DTC_INFO_reserved_14[186] = 0x00,
	.DTC_INFO_reserved_14[187] = 0x00,
	.DTC_INFO_reserved_14[188] = 0x00,
	.DTC_INFO_reserved_14[189] = 0x00,
	.DTC_INFO_reserved_14[190] = 0x00,
	.DTC_INFO_reserved_14[191] = 0x00,
	.DTC_INFO_reserved_14[192] = 0x00,
	.DTC_INFO_reserved_14[193] = 0x00,
	.DTC_INFO_reserved_14[194] = 0x00,
	.DTC_INFO_reserved_14[195] = 0x00,
	.DTC_INFO_reserved_14[196] = 0x00,
	.DTC_INFO_reserved_14[197] = 0x00,
	.DTC_INFO_reserved_14[198] = 0x00,
	.DTC_INFO_reserved_14[199] = 0x00,
	.DTC_INFO_reserved_14[200] = 0x00,
	.DTC_INFO_reserved_14[201] = 0x00,
	.DTC_INFO_reserved_14[202] = 0x00,
	.DTC_INFO_reserved_14[203] = 0x00,
	.DTC_INFO_reserved_14[204] = 0x00,
	.DTC_INFO_reserved_14[205] = 0x00,
	.DTC_INFO_reserved_14[206] = 0x00,
	.DTC_INFO_reserved_14[207] = 0x00,
	.DTC_INFO_reserved_14[208] = 0x00,
	.DTC_INFO_reserved_14[209] = 0x00,
	.DTC_INFO_reserved_14[210] = 0x00,
	.DTC_INFO_reserved_14[211] = 0x00,
	.DTC_INFO_reserved_14[212] = 0x00,
	.DTC_INFO_reserved_14[213] = 0x00,
	.DTC_INFO_reserved_14[214] = 0x00,
	.DTC_INFO_reserved_14[215] = 0x00,
	.DTC_INFO_reserved_14[216] = 0x00,
	.DTC_INFO_reserved_14[217] = 0x00,
	.DTC_INFO_reserved_14[218] = 0x00,
	.DTC_INFO_reserved_14[219] = 0x00,
	.DTC_INFO_reserved_14[220] = 0x00,
	.DTC_INFO_reserved_14[221] = 0x00,
	.DTC_INFO_reserved_14[222] = 0x00,
	.DTC_INFO_reserved_14[223] = 0x00,
	.DTC_INFO_reserved_14[224] = 0x00,
	.DTC_INFO_reserved_14[225] = 0x00,
	.DTC_INFO_reserved_14[226] = 0x00,
	.DTC_INFO_reserved_14[227] = 0x00,
	.DTC_INFO_reserved_14[228] = 0x00,
	.DTC_INFO_reserved_14[229] = 0x00,
	.DTC_INFO_reserved_14[230] = 0x00,
	.DTC_INFO_reserved_14[231] = 0x00,
	.DTC_INFO_reserved_14[232] = 0x00,
	.DTC_INFO_reserved_14[233] = 0x00,
	.DTC_INFO_reserved_14[234] = 0x00,
	.DTC_INFO_reserved_14[235] = 0x00,
	.DTC_INFO_reserved_14[236] = 0x00,
	.DTC_INFO_reserved_14[237] = 0x00,
	.OdoInfo_reserved_40[0] = 0x00,
	.OdoInfo_reserved_40[1] = 0x00,
	.OdoInfo_reserved_40[2] = 0x00,
	.OdoInfo_reserved_40[3] = 0x00,
	.OdoInfo_reserved_40[4] = 0x00,
	.OdoInfo_reserved_40[5] = 0x00,
	.OdoInfo_reserved_40[6] = 0x00,
	.OdoInfo_reserved_40[7] = 0x00,
	.OdoInfo_reserved_40[8] = 0x00,
	.OdoInfo_reserved_40[9] = 0x00,
	.OdoInfo_reserved_40[10] = 0x00,
	.OdoInfo_reserved_40[11] = 0x00,
	.OdoInfo_reserved_40[12] = 0x00,
	.OdoInfo_reserved_40[13] = 0x00,
	.OdoInfo_reserved_40[14] = 0x00,
	.OdoInfo_reserved_40[15] = 0x00,
	.OdoInfo_reserved_40[16] = 0x00,
	.OdoInfo_reserved_40[17] = 0x00,
	.OdoInfo_reserved_40[18] = 0x00,
	.OdoInfo_reserved_40[19] = 0x00,
	.trace_phy_en = 0x0000,
	.trace_phy = 4,
	.reserved_79_TraceModuleLvlCrc = 0x00,
	.TraceModuleLvlCrc = 0xA5A5,
	.share_eep_reserved_82[0] = 0x00,
	.share_eep_reserved_82[1] = 0x00,
	.share_eep_reserved_82[2] = 0x00,
	.share_eep_reserved_82[3] = 0x00,
	.share_eep_reserved_82[4] = 0x00,
	.share_eep_reserved_82[5] = 0x00,
	.share_eep_reserved_82[6] = 0x00,
	.share_eep_reserved_82[7] = 0x00,
	.share_eep_reserved_82[8] = 0x00,
	.share_eep_reserved_82[9] = 0x00,
	.share_eep_reserved_82[10] = 0x00,
	.share_eep_reserved_82[11] = 0x00,
	.share_eep_reserved_82[12] = 0x00,
	.share_eep_reserved_82[13] = 0x00,
	.share_eep_reserved_82[14] = 0x00,
	.share_eep_reserved_82[15] = 0x00,
	.share_eep_reserved_82[16] = 0x00,
	.share_eep_reserved_82[17] = 0x00,
	.share_eep_reserved_82[18] = 0x00,
	.share_eep_reserved_82[19] = 0x00,
	.share_eep_reserved_82[20] = 0x00,
	.share_eep_reserved_82[21] = 0x00,
	.share_eep_reserved_82[22] = 0x00,
	.share_eep_reserved_82[23] = 0x00,
	.share_eep_reserved_82[24] = 0x00,
	.share_eep_reserved_82[25] = 0x00,
	.share_eep_reserved_82[26] = 0x00,
	.share_eep_reserved_82[27] = 0x00,
	.share_eep_reserved_82[28] = 0x00,
	.share_eep_reserved_82[29] = 0x00,
	.share_eep_reserved_82[30] = 0x00,
	.share_eep_reserved_82[31] = 0x00,
	.share_eep_reserved_82[32] = 0x00,
	.share_eep_reserved_82[33] = 0x00,
	.share_eep_reserved_82[34] = 0x00,
	.share_eep_reserved_82[35] = 0x00,
	.share_eep_reserved_82[36] = 0x00,
	.share_eep_reserved_82[37] = 0x00,
	.share_eep_reserved_82[38] = 0x00,
	.share_eep_reserved_82[39] = 0x00,
	.share_eep_reserved_82[40] = 0x00,
	.share_eep_reserved_82[41] = 0x00,
	.ResetInfo_reserved_52[0] = 0x00,
	.ResetInfo_reserved_52[1] = 0x00,
	.ResetInfo_reserved_52[2] = 0x00,
	.ResetInfo_reserved_52[3] = 0x00,
	.ResetInfo_reserved_52[4] = 0x00,
	.ResetInfo_reserved_52[5] = 0x00,
	.ResetInfo_reserved_52[6] = 0x00,
	.ResetInfo_reserved_52[7] = 0x00,
	.ResetInfo_reserved_52[8] = 0x00,
	.ResetInfo_reserved_52[9] = 0x00,
	.ResetInfo_reserved_52[10] = 0x00,
	.ResetInfo_reserved_52[11] = 0x00,
	.ResetInfo_reserved_52[12] = 0x00,
	.ResetInfo_reserved_52[13] = 0x00,
	.ResetInfo_reserved_52[14] = 0x00,
	.ResetInfo_reserved_52[15] = 0x00,
	.ResetInfo_reserved_52[16] = 0x00,
	.ResetInfo_reserved_52[17] = 0x00,
	.ResetInfo_reserved_52[18] = 0x00,
	.ResetInfo_reserved_52[19] = 0x00,
	.ResetInfo_reserved_52[20] = 0x00,
	.ResetInfo_reserved_52[21] = 0x00,
	.ResetInfo_reserved_52[22] = 0x00,
	.ResetInfo_reserved_52[23] = 0x00,
	.ResetInfo_reserved_52[24] = 0x00,
	.ResetInfo_reserved_52[25] = 0x00,
	.ResetInfo_reserved_52[26] = 0x00,
	.ResetInfo_reserved_52[27] = 0x00,
	.ResetInfo_reserved_52[28] = 0x00,
	.ResetInfo_reserved_52[29] = 0x00,
	.ResetInfo_reserved_52[30] = 0x00,
	.ResetInfo_reserved_52[31] = 0x00,
	.ResetInfo_reserved_52[32] = 0x00,
	.ResetInfo_reserved_52[33] = 0x00,
	.ResetInfo_reserved_52[34] = 0x00,
	.ResetInfo_reserved_52[35] = 0x00,
	.ResetInfo_reserved_52[36] = 0x00,
	.ResetInfo_reserved_52[37] = 0x00,
	.ResetInfo_reserved_52[38] = 0x00,
	.ResetInfo_reserved_52[39] = 0x00,
	.ResetInfo_reserved_52[40] = 0x00,
	.ResetInfo_reserved_52[41] = 0x00,
	.ResetInfo_reserved_52[42] = 0x00,
	.ResetInfo_reserved_52[43] = 0x00,
	.ResetInfo_reserved_52[44] = 0x00,
	.ResetInfo_reserved_52[45] = 0x00,
	.ResetInfo_reserved_52[46] = 0x00,
	.ResetInfo_reserved_52[47] = 0x00,
	.ResetInfo_reserved_52[48] = 0x00,
	.ResetInfo_reserved_52[49] = 0x00,
	.ResetInfo_reserved_52[50] = 0x00,
	.ResetInfo_reserved_52[51] = 0x00,
	.ResetInfo_reserved_52[52] = 0x00,
	.ResetInfo_reserved_52[53] = 0x00,
	.ResetInfo_reserved_52[54] = 0x00,
	.ResetInfo_reserved_52[55] = 0x00,
	.ResetInfo_reserved_52[56] = 0x00,
	.ResetInfo_reserved_52[57] = 0x00,
	.ResetInfo_reserved_52[58] = 0x00,
	.ResetInfo_reserved_52[59] = 0x00,
	.ResetInfo_reserved_52[60] = 0x00,
	.ResetInfo_reserved_52[61] = 0x00,
	.ResetInfo_reserved_52[62] = 0x00,
	.ResetInfo_reserved_52[63] = 0x00,
	.ResetInfo_reserved_52[64] = 0x00,
	.ResetInfo_reserved_52[65] = 0x00,
	.ResetInfo_reserved_52[66] = 0x00,
	.ResetInfo_reserved_52[67] = 0x00,
	.ResetInfo_reserved_52[68] = 0x00,
	.ResetInfo_reserved_52[69] = 0x00,
	.ResetInfo_reserved_52[70] = 0x00,
	.ResetInfo_reserved_52[71] = 0x00,
	/*lint -restore */
};

#if 0
static const EEP_ITEM_LIST_INFO eep_list_info[EEP_CONTENT_LIST_NUMBER_MAX]=
{
	{EEP_CONTENT_Manuf_Visteon_Part_Number_LIST_ID, EEP_CONTENT_Manuf_Visteon_Part_Number_ITEM_BYTE_LEN, (uint8_t*)(eep_ram_image.Manuf_Visteon_Part_Number)},
	{EEP_CONTENT_Manuf_EquippedPCB_VPN_Sub_LIST_ID, EEP_CONTENT_Manuf_EquippedPCB_VPN_Sub_ITEM_BYTE_LEN, (uint8_t*)(eep_ram_image.Manuf_EquippedPCB_VPN_Sub)},
	{EEP_CONTENT_Manuf_EquippedPCB_VPN_Main_LIST_ID, EEP_CONTENT_Manuf_EquippedPCB_VPN_Main_ITEM_BYTE_LEN, (uint8_t*)(eep_ram_image.Manuf_EquippedPCB_VPN_Main)},
	{EEP_CONTENT_Manuf_Product_Serial_Number_LIST_ID, EEP_CONTENT_Manuf_Product_Serial_Number_ITEM_BYTE_LEN, (uint8_t*)(eep_ram_image.Manuf_Product_Serial_Number)},
	{EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Sub_LIST_ID, EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Sub_ITEM_BYTE_LEN, (uint8_t*)(eep_ram_image.Manuf_EquippedPCB_Serial_Number_Sub)},
	{EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Main_LIST_ID, EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Main_ITEM_BYTE_LEN, (uint8_t*)(eep_ram_image.Manuf_EquippedPCB_Serial_Number_Main)},
	{EEP_CONTENT_NVM_Revision_LIST_ID, EEP_CONTENT_NVM_Revision_ITEM_BYTE_LEN, (uint8_t*)(&eep_ram_image.NVM_Revision)},
	{EEP_CONTENT_Manuf_SMD_Date1_LIST_ID, EEP_CONTENT_Manuf_SMD_Date1_ITEM_BYTE_LEN, (uint8_t*)(eep_ram_image.Manuf_SMD_Date1)},
	{EEP_CONTENT_Manuf_SMD_Date2_LIST_ID, EEP_CONTENT_Manuf_SMD_Date2_ITEM_BYTE_LEN, (uint8_t*)(eep_ram_image.Manuf_SMD_Date2)},
	{EEP_CONTENT_Manuf_Assembly_Date_LIST_ID, EEP_CONTENT_Manuf_Assembly_Date_ITEM_BYTE_LEN, (uint8_t*)(eep_ram_image.Manuf_Assembly_Date)},
	{EEP_CONTENT_Traceability_Data_LIST_ID, EEP_CONTENT_Traceability_Data_ITEM_BYTE_LEN, (uint8_t*)(eep_ram_image.Traceability_Data)},
	{EEP_CONTENT_VehicleManufacturerSparePartNumber_LIST_ID, EEP_CONTENT_VehicleManufacturerSparePartNumber_ITEM_BYTE_LEN, (uint8_t*)(eep_ram_image.VehicleManufacturerSparePartNumber)},
	{EEP_CONTENT_VehicleManufacturerSparePartNumber_Assembly_LIST_ID, EEP_CONTENT_VehicleManufacturerSparePartNumber_Assembly_ITEM_BYTE_LEN, (uint8_t*)(eep_ram_image.VehicleManufacturerSparePartNumber_Assembly)},
	{EEP_CONTENT_Operational_Reference_LIST_ID, EEP_CONTENT_Operational_Reference_ITEM_BYTE_LEN, (uint8_t*)(eep_ram_image.Operational_Reference)},
	{EEP_CONTENT_Supplier_Number_LIST_ID, EEP_CONTENT_Supplier_Number_ITEM_BYTE_LEN, (uint8_t*)(eep_ram_image.Supplier_Number)},
	{EEP_CONTENT_ECU_Serial_Number_LIST_ID, EEP_CONTENT_ECU_Serial_Number_ITEM_BYTE_LEN, (uint8_t*)(eep_ram_image.ECU_Serial_Number)},
	{EEP_CONTENT_Manufacturing_Identification_Code_LIST_ID, EEP_CONTENT_Manufacturing_Identification_Code_ITEM_BYTE_LEN, (uint8_t*)(&eep_ram_image.Manufacturing_Identification_Code)},
	{EEP_CONTENT_VDIAG_LIST_ID, EEP_CONTENT_VDIAG_ITEM_BYTE_LEN, (uint8_t*)(&eep_ram_image.VDIAG)},
	{EEP_CONTENT_Config_EQ1_LIST_ID, EEP_CONTENT_Config_EQ1_ITEM_BYTE_LEN, (uint8_t*)(&eep_ram_image.Config_EQ1)},
	{EEP_CONTENT_Vehicle_Type_LIST_ID, EEP_CONTENT_Vehicle_Type_ITEM_BYTE_LEN, (uint8_t*)(&eep_ram_image.Vehicle_Type)},
	{EEP_CONTENT_UUID_LIST_ID, EEP_CONTENT_UUID_ITEM_BYTE_LEN, (uint8_t*)(eep_ram_image.UUID)},
	{EEP_CONTENT_NAVI_ID_LIST_ID, EEP_CONTENT_NAVI_ID_ITEM_BYTE_LEN, (uint8_t*)(eep_ram_image.NAVI_ID)},
	{EEP_CONTENT_DA_ID_LIST_ID, EEP_CONTENT_DA_ID_ITEM_BYTE_LEN, (uint8_t*)(eep_ram_image.DA_ID)},
	{EEP_CONTENT_DAMainBoardHwVer_LIST_ID, EEP_CONTENT_DAMainBoardHwVer_ITEM_BYTE_LEN, (uint8_t*)(eep_ram_image.DAMainBoardHwVer)},
	{EEP_CONTENT_SW_Version_Date_Format_LIST_ID, EEP_CONTENT_SW_Version_Date_Format_ITEM_BYTE_LEN, (uint8_t*)(eep_ram_image.SW_Version_Date_Format)},
	{EEP_CONTENT_SW_Edition_Version_LIST_ID, EEP_CONTENT_SW_Edition_Version_ITEM_BYTE_LEN, (uint8_t*)(eep_ram_image.SW_Edition_Version)},
	{EEP_CONTENT_VehicleManufacturerSparePartNumber_Nissan_LIST_ID, EEP_CONTENT_VehicleManufacturerSparePartNumber_Nissan_ITEM_BYTE_LEN, (uint8_t*)(eep_ram_image.VehicleManufacturerSparePartNumber_Nissan)},
	{EEP_CONTENT_VisteonProductPartNumber_LIST_ID, EEP_CONTENT_VisteonProductPartNumber_ITEM_BYTE_LEN, (uint8_t*)(eep_ram_image.VisteonProductPartNumber)},
	{EEP_CONTENT_EquippedPCBVisteonPartNumMainBorad_LIST_ID, EEP_CONTENT_EquippedPCBVisteonPartNumMainBorad_ITEM_BYTE_LEN, (uint8_t*)(eep_ram_image.EquippedPCBVisteonPartNumMainBorad)},
	{EEP_CONTENT_EquippedPCBVisteonPartNumSubBorad_LIST_ID, EEP_CONTENT_EquippedPCBVisteonPartNumSubBorad_ITEM_BYTE_LEN, (uint8_t*)(eep_ram_image.EquippedPCBVisteonPartNumSubBorad)},
	{EEP_CONTENT_DAUniqueID_LIST_ID, EEP_CONTENT_DAUniqueID_ITEM_BYTE_LEN, (uint8_t*)(eep_ram_image.DAUniqueID)},
	{EEP_CONTENT_ProductSerialNum_LIST_ID, EEP_CONTENT_ProductSerialNum_ITEM_BYTE_LEN, (uint8_t*)(eep_ram_image.ProductSerialNum)},
	{EEP_CONTENT_ProductSerialNumMainBoard_LIST_ID, EEP_CONTENT_ProductSerialNumMainBoard_ITEM_BYTE_LEN, (uint8_t*)(eep_ram_image.ProductSerialNumMainBoard)},
	{EEP_CONTENT_ProductSerialNumSubBoard_LIST_ID, EEP_CONTENT_ProductSerialNumSubBoard_ITEM_BYTE_LEN, (uint8_t*)(eep_ram_image.ProductSerialNumSubBoard)},
	{EEP_CONTENT_SMDManufacturingDate_LIST_ID, EEP_CONTENT_SMDManufacturingDate_ITEM_BYTE_LEN, (uint8_t*)(eep_ram_image.SMDManufacturingDate)},
	{EEP_CONTENT_AssemblyManufacturingDate_LIST_ID, EEP_CONTENT_AssemblyManufacturingDate_ITEM_BYTE_LEN, (uint8_t*)(eep_ram_image.AssemblyManufacturingDate)},
	{EEP_CONTENT_TraceabilityBytes_LIST_ID, EEP_CONTENT_TraceabilityBytes_ITEM_BYTE_LEN, (uint8_t*)(eep_ram_image.TraceabilityBytes)},
	{EEP_CONTENT_SMDPlantNum_LIST_ID, EEP_CONTENT_SMDPlantNum_ITEM_BYTE_LEN, (uint8_t*)(eep_ram_image.SMDPlantNum)},
	{EEP_CONTENT_AssemblyPlantNum_LIST_ID, EEP_CONTENT_AssemblyPlantNum_ITEM_BYTE_LEN, (uint8_t*)(eep_ram_image.AssemblyPlantNum)},
	{EEP_CONTENT_DEM_EVENT_CAN_COM_DATA_LIST_ID, EEP_CONTENT_DEM_EVENT_CAN_COM_DATA_ITEM_BYTE_LEN, (uint8_t*)(eep_ram_image.DEM_EVENT_CAN_COM_DATA)},
	{EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_LIST_ID, EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_BYTE_LEN, (uint8_t*)(eep_ram_image.DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA)},
	{EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_LIST_ID, EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_BYTE_LEN, (uint8_t*)(eep_ram_image.DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA)},
	{EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_LIST_ID, EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_BYTE_LEN, (uint8_t*)(eep_ram_image.DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA)},
	{EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_LIST_ID, EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_BYTE_LEN, (uint8_t*)(eep_ram_image.DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA)},
	{EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_LIST_ID, EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_BYTE_LEN, (uint8_t*)(eep_ram_image.DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA)},
	{EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_LIST_ID, EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_BYTE_LEN, (uint8_t*)(eep_ram_image.DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA)},
	{EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_LIST_ID, EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_BYTE_LEN, (uint8_t*)(eep_ram_image.DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA)},
	{EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_LIST_ID, EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_BYTE_LEN, (uint8_t*)(eep_ram_image.DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA)},
	{EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_LIST_ID, EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_ITEM_BYTE_LEN, (uint8_t*)(eep_ram_image.DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA)},
	{EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_LIST_ID, EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_ITEM_BYTE_LEN, (uint8_t*)(eep_ram_image.DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA)},
	{EEP_CONTENT_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_LIST_ID, EEP_CONTENT_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_ITEM_BYTE_LEN, (uint8_t*)(eep_ram_image.DEM_EVENT_HVAC_PANEL_CONNECTION_DATA)},
	{EEP_CONTENT_TachoUpValue_LIST_ID, EEP_CONTENT_TachoUpValue_ITEM_BYTE_LEN, (uint8_t*)(eep_ram_image.TachoUpValue)},
	{EEP_CONTENT_TachoLowValue_LIST_ID, EEP_CONTENT_TachoLowValue_ITEM_BYTE_LEN, (uint8_t*)(eep_ram_image.TachoLowValue)},
	{EEP_CONTENT_TachoLimitValue_LIST_ID, EEP_CONTENT_TachoLimitValue_ITEM_BYTE_LEN, (uint8_t*)(eep_ram_image.TachoLimitValue)},
	{EEP_CONTENT_TachoUDeltaLimit_LIST_ID, EEP_CONTENT_TachoUDeltaLimit_ITEM_BYTE_LEN, (uint8_t*)(eep_ram_image.TachoUDeltaLimit)},
	{EEP_CONTENT_TachoLDeltaLimit_LIST_ID, EEP_CONTENT_TachoLDeltaLimit_ITEM_BYTE_LEN, (uint8_t*)(eep_ram_image.TachoLDeltaLimit)},
	{EEP_CONTENT_TachoUpChangeValue_LIST_ID, EEP_CONTENT_TachoUpChangeValue_ITEM_BYTE_LEN, (uint8_t*)(eep_ram_image.TachoUpChangeValue)},
	{EEP_CONTENT_TachoLowChangeValue_LIST_ID, EEP_CONTENT_TachoLowChangeValue_ITEM_BYTE_LEN, (uint8_t*)(eep_ram_image.TachoLowChangeValue)},
	{EEP_CONTENT_TachoRun_LIST_ID, EEP_CONTENT_TachoRun_ITEM_BYTE_LEN, (uint8_t*)(eep_ram_image.TachoRun)},
	{EEP_CONTENT_Speed_Rm_1_LIST_ID, EEP_CONTENT_Speed_Rm_1_ITEM_BYTE_LEN, (uint8_t*)(&eep_ram_image.Speed_Rm_1)},
	{EEP_CONTENT_Speed_Rm_2_LIST_ID, EEP_CONTENT_Speed_Rm_2_ITEM_BYTE_LEN, (uint8_t*)(&eep_ram_image.Speed_Rm_2)},
	{EEP_CONTENT_Speed_Rc_LIST_ID, EEP_CONTENT_Speed_Rc_ITEM_BYTE_LEN, (uint8_t*)(&eep_ram_image.Speed_Rc)},
	{EEP_CONTENT_FuelTelltaleOn_R_LIST_ID, EEP_CONTENT_FuelTelltaleOn_R_ITEM_BYTE_LEN, (uint8_t*)(&eep_ram_image.FuelTelltaleOn_R)},
	{EEP_CONTENT_FuelTelltaleOff_R_LIST_ID, EEP_CONTENT_FuelTelltaleOff_R_ITEM_BYTE_LEN, (uint8_t*)(&eep_ram_image.FuelTelltaleOff_R)},
	{EEP_CONTENT_FuelWarningOnPoint_R_LIST_ID, EEP_CONTENT_FuelWarningOnPoint_R_ITEM_BYTE_LEN, (uint8_t*)(&eep_ram_image.FuelWarningOnPoint_R)},
	{EEP_CONTENT_Fuel_R_Open_LIST_ID, EEP_CONTENT_Fuel_R_Open_ITEM_BYTE_LEN, (uint8_t*)(&eep_ram_image.Fuel_R_Open)},
	{EEP_CONTENT_Fuel_R_Short_LIST_ID, EEP_CONTENT_Fuel_R_Short_ITEM_BYTE_LEN, (uint8_t*)(&eep_ram_image.Fuel_R_Short)},
	{EEP_CONTENT_PkbWarningJudgeV1_LIST_ID, EEP_CONTENT_PkbWarningJudgeV1_ITEM_BYTE_LEN, (uint8_t*)(&eep_ram_image.PkbWarningJudgeV1)},
	{EEP_CONTENT_PkbWarningJudgeV2_LIST_ID, EEP_CONTENT_PkbWarningJudgeV2_ITEM_BYTE_LEN, (uint8_t*)(&eep_ram_image.PkbWarningJudgeV2)},
	{EEP_CONTENT_AudioEepTest_LIST_ID, EEP_CONTENT_AudioEepTest_ITEM_BYTE_LEN, (uint8_t*)(&eep_ram_image.AudioEepTest)},
	{EEP_CONTENT_DspKeyCodeOnOff_LIST_ID, EEP_CONTENT_DspKeyCodeOnOff_ITEM_BYTE_LEN, (uint8_t*)(&eep_ram_image.DspKeyCodeOnOff)},
	{EEP_CONTENT_DspKeyCode_LIST_ID, EEP_CONTENT_DspKeyCode_ITEM_BYTE_LEN, (uint8_t*)(&eep_ram_image.DspKeyCode)},
	{EEP_CONTENT_BatState_ChgDly_LIST_ID, EEP_CONTENT_BatState_ChgDly_ITEM_BYTE_LEN, (uint8_t*)(&eep_ram_image.BatState_ChgDly)},
	{EEP_CONTENT_BatVolVeryHigh_Hysteresis_H_LIST_ID, EEP_CONTENT_BatVolVeryHigh_Hysteresis_H_ITEM_BYTE_LEN, (uint8_t*)(&eep_ram_image.BatVolVeryHigh_Hysteresis_H)},
	{EEP_CONTENT_BatVolVeryHigh_Hysteresis_L_LIST_ID, EEP_CONTENT_BatVolVeryHigh_Hysteresis_L_ITEM_BYTE_LEN, (uint8_t*)(&eep_ram_image.BatVolVeryHigh_Hysteresis_L)},
	{EEP_CONTENT_BatVolHigh_Hysteresis_H_LIST_ID, EEP_CONTENT_BatVolHigh_Hysteresis_H_ITEM_BYTE_LEN, (uint8_t*)(&eep_ram_image.BatVolHigh_Hysteresis_H)},
	{EEP_CONTENT_BatVolHigh_Hysteresis_L_LIST_ID, EEP_CONTENT_BatVolHigh_Hysteresis_L_ITEM_BYTE_LEN, (uint8_t*)(&eep_ram_image.BatVolHigh_Hysteresis_L)},
	{EEP_CONTENT_BatVolLow_Hysteresis_H_LIST_ID, EEP_CONTENT_BatVolLow_Hysteresis_H_ITEM_BYTE_LEN, (uint8_t*)(&eep_ram_image.BatVolLow_Hysteresis_H)},
	{EEP_CONTENT_BatVolLow_Hysteresis_L_LIST_ID, EEP_CONTENT_BatVolLow_Hysteresis_L_ITEM_BYTE_LEN, (uint8_t*)(&eep_ram_image.BatVolLow_Hysteresis_L)},
	{EEP_CONTENT_BatVolVeryLow_Hysteresis_H_LIST_ID, EEP_CONTENT_BatVolVeryLow_Hysteresis_H_ITEM_BYTE_LEN, (uint8_t*)(&eep_ram_image.BatVolVeryLow_Hysteresis_H)},
	{EEP_CONTENT_BatVolVeryLow_Hysteresis_L_LIST_ID, EEP_CONTENT_BatVolVeryLow_Hysteresis_L_ITEM_BYTE_LEN, (uint8_t*)(&eep_ram_image.BatVolVeryLow_Hysteresis_L)},
	{EEP_CONTENT_TempState_ChgDly_LIST_ID, EEP_CONTENT_TempState_ChgDly_ITEM_BYTE_LEN, (uint8_t*)(&eep_ram_image.TempState_ChgDly)},
	{EEP_CONTENT_TempDegC_Low_Hysteresis_L_LIST_ID, EEP_CONTENT_TempDegC_Low_Hysteresis_L_ITEM_BYTE_LEN, (uint8_t*)(&eep_ram_image.TempDegC_Low_Hysteresis_L)},
	{EEP_CONTENT_TempDegC_Low_Hysteresis_H_LIST_ID, EEP_CONTENT_TempDegC_Low_Hysteresis_H_ITEM_BYTE_LEN, (uint8_t*)(&eep_ram_image.TempDegC_Low_Hysteresis_H)},
	{EEP_CONTENT_TempDegC_High_Hysteresis_L_LIST_ID, EEP_CONTENT_TempDegC_High_Hysteresis_L_ITEM_BYTE_LEN, (uint8_t*)(&eep_ram_image.TempDegC_High_Hysteresis_L)},
	{EEP_CONTENT_TempDegC_High_Hysteresis_H_LIST_ID, EEP_CONTENT_TempDegC_High_Hysteresis_H_ITEM_BYTE_LEN, (uint8_t*)(&eep_ram_image.TempDegC_High_Hysteresis_H)},
	{EEP_CONTENT_AmpHighTempProction_LIST_ID, EEP_CONTENT_AmpHighTempProction_ITEM_BYTE_LEN, (uint8_t*)(eep_ram_image.AmpHighTempProction)},
	{EEP_CONTENT_CanNm_DA_S1_Delay_ms_LIST_ID, EEP_CONTENT_CanNm_DA_S1_Delay_ms_ITEM_BYTE_LEN, (uint8_t*)(&eep_ram_image.CanNm_DA_S1_Delay_ms)},
	{EEP_CONTENT_Vin_LIST_ID, EEP_CONTENT_Vin_ITEM_BYTE_LEN, (uint8_t*)(eep_ram_image.Vin)},
	{EEP_CONTENT_AutoSyncTimeWithGps_LIST_ID, EEP_CONTENT_AutoSyncTimeWithGps_ITEM_BYTE_LEN, (uint8_t*)(&eep_ram_image.AutoSyncTimeWithGps)},
	{EEP_CONTENT_ScreenBackLightValOnDay_LIST_ID, EEP_CONTENT_ScreenBackLightValOnDay_ITEM_BYTE_LEN, (uint8_t*)(&eep_ram_image.ScreenBackLightValOnDay)},
	{EEP_CONTENT_ScreenBackLightValOnNight_LIST_ID, EEP_CONTENT_ScreenBackLightValOnNight_ITEM_BYTE_LEN, (uint8_t*)(&eep_ram_image.ScreenBackLightValOnNight)},
	{EEP_CONTENT_HistoryAverFuelCons_LIST_ID, EEP_CONTENT_HistoryAverFuelCons_ITEM_BYTE_LEN, (uint8_t*)(eep_ram_image.HistoryAverFuelCons)},
	{EEP_CONTENT_FuelResistance_LIST_ID, EEP_CONTENT_FuelResistance_ITEM_BYTE_LEN, (uint8_t*)(&eep_ram_image.FuelResistance)},
	{EEP_CONTENT_dtc_example_LIST_ID, EEP_CONTENT_dtc_example_ITEM_BYTE_LEN, (uint8_t*)(eep_ram_image.dtc_example)},
	{EEP_CONTENT_TotalOdo_LIST_ID, EEP_CONTENT_TotalOdo_ITEM_BYTE_LEN, (uint8_t*)(&eep_ram_image.TotalOdo)},
	{EEP_CONTENT_TotalTime_LIST_ID, EEP_CONTENT_TotalTime_ITEM_BYTE_LEN, (uint8_t*)(&eep_ram_image.TotalTime)},
	{EEP_CONTENT_TravelTime_LIST_ID, EEP_CONTENT_TravelTime_ITEM_BYTE_LEN, (uint8_t*)(&eep_ram_image.TravelTime)},
	{EEP_CONTENT_TravelOdo_LIST_ID, EEP_CONTENT_TravelOdo_ITEM_BYTE_LEN, (uint8_t*)(&eep_ram_image.TravelOdo)},
	{EEP_CONTENT_TripAMeter_LIST_ID, EEP_CONTENT_TripAMeter_ITEM_BYTE_LEN, (uint8_t*)(&eep_ram_image.TripAMeter)},
	{EEP_CONTENT_TripATime_LIST_ID, EEP_CONTENT_TripATime_ITEM_BYTE_LEN, (uint8_t*)(&eep_ram_image.TripATime)},
	{EEP_CONTENT_TripBMeter_LIST_ID, EEP_CONTENT_TripBMeter_ITEM_BYTE_LEN, (uint8_t*)(&eep_ram_image.TripBMeter)},
	{EEP_CONTENT_TripBTime_LIST_ID, EEP_CONTENT_TripBTime_ITEM_BYTE_LEN, (uint8_t*)(&eep_ram_image.TripBTime)},
	{EEP_CONTENT_CruiseDistance_LIST_ID, EEP_CONTENT_CruiseDistance_ITEM_BYTE_LEN, (uint8_t*)(&eep_ram_image.CruiseDistance)},
	{EEP_CONTENT_VipSwdlShareMem_LIST_ID, EEP_CONTENT_VipSwdlShareMem_ITEM_BYTE_LEN, (uint8_t*)(eep_ram_image.VipSwdlShareMem)},
	{EEP_CONTENT_TotalImageSFailCnt_LIST_ID, EEP_CONTENT_TotalImageSFailCnt_ITEM_BYTE_LEN, (uint8_t*)(&eep_ram_image.TotalImageSFailCnt)},
	{EEP_CONTENT_trace_phy_en_LIST_ID, EEP_CONTENT_trace_phy_en_ITEM_BYTE_LEN, (uint8_t*)(&eep_ram_image.trace_phy_en)},
	{EEP_CONTENT_trace_phy_LIST_ID, EEP_CONTENT_trace_phy_ITEM_BYTE_LEN, (uint8_t*)(&eep_ram_image.trace_phy)},
	{EEP_CONTENT_VipLogModLvl_LIST_ID, EEP_CONTENT_VipLogModLvl_ITEM_BYTE_LEN, (uint8_t*)(eep_ram_image.VipLogModLvl)},
	{EEP_CONTENT_TraceModuleLvlCrc_LIST_ID, EEP_CONTENT_TraceModuleLvlCrc_ITEM_BYTE_LEN, (uint8_t*)(&eep_ram_image.TraceModuleLvlCrc)},
	{EEP_CONTENT_TotalReset_Cnt_LIST_ID, EEP_CONTENT_TotalReset_Cnt_ITEM_BYTE_LEN, (uint8_t*)(&eep_ram_image.TotalReset_Cnt)},
	{EEP_CONTENT_DspPOR_cnt_LIST_ID, EEP_CONTENT_DspPOR_cnt_ITEM_BYTE_LEN, (uint8_t*)(&eep_ram_image.DspPOR_cnt)},
	{EEP_CONTENT_AudioCmdExecMaxTm_LIST_ID, EEP_CONTENT_AudioCmdExecMaxTm_ITEM_BYTE_LEN, (uint8_t*)(&eep_ram_image.AudioCmdExecMaxTm)},
	{EEP_CONTENT_RadioCmdExecMaxTm_LIST_ID, EEP_CONTENT_RadioCmdExecMaxTm_ITEM_BYTE_LEN, (uint8_t*)(&eep_ram_image.RadioCmdExecMaxTm)},
	{EEP_CONTENT_CpuDisIntMaxNest_LIST_ID, EEP_CONTENT_CpuDisIntMaxNest_ITEM_BYTE_LEN, (uint8_t*)(&eep_ram_image.CpuDisIntMaxNest)},
	{EEP_CONTENT_CpuDisIntMaxTm_LIST_ID, EEP_CONTENT_CpuDisIntMaxTm_ITEM_BYTE_LEN, (uint8_t*)(&eep_ram_image.CpuDisIntMaxTm)},
	{EEP_CONTENT_CpuDisIntTooLongCnt_LIST_ID, EEP_CONTENT_CpuDisIntTooLongCnt_ITEM_BYTE_LEN, (uint8_t*)(&eep_ram_image.CpuDisIntTooLongCnt)},
	{EEP_CONTENT_SocResetReason_LIST_ID, EEP_CONTENT_SocResetReason_ITEM_BYTE_LEN, (uint8_t*)(eep_ram_image.SocResetReason)},
};
#endif

#if 0
static uint16_t const eep_block_offset_info[EEP_BLOCK_NUMBER_MAX+1] = 
{
	0,
	512,
	1536,
	1792,
	2048,
	2304,
	2368,
	2496,
	0xFFFF
};
#endif

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void EepWrProcess(void);
// static void EepIpcTxProcess(void);

// static void ProcessImxReadEepByOffset( uint16 len, uint8 const* cmd );
// static void ProcessImxWriteEepByOffset( uint16 len, uint8 const* cmd );
// static void ProcessImxReadEepByList( uint16 len, uint8 const* cmd );
// static void ProcessImxWriteEepByList( uint16 len, uint8 const* cmd );
static void EepCopyRamImageToEepImage(void);
static void EepCopyEepImageToHw(void);

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
uint32_t GenEep_Get_manufacture_write_count(void)
{
	return eep_ram_image.manufacture_write_count;
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> Manuf_Visteon_Part_Number function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_Manuf_Visteon_Part_Number_ITEM_LEN > 1
void GenEep_Get_Manuf_Visteon_Part_Number(uint8_t * const Manuf_Visteon_Part_Number )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_Manuf_Visteon_Part_Number_ITEM_LEN; i ++)
    {
        Manuf_Visteon_Part_Number[i] = eep_ram_image.Manuf_Visteon_Part_Number[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_Manuf_Visteon_Part_Number_Ex(uint8_t * const Manuf_Visteon_Part_Number,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.Manuf_Visteon_Part_Number;	//EEP_CONTENT_Manuf_Visteon_Part_Number_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_Manuf_Visteon_Part_Number_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_Manuf_Visteon_Part_Number_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        Manuf_Visteon_Part_Number[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_Manuf_Visteon_Part_Number(uint8_t const* Manuf_Visteon_Part_Number )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_Manuf_Visteon_Part_Number_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.Manuf_Visteon_Part_Number[i] != Manuf_Visteon_Part_Number[i] )
        {
            eep_ram_image.Manuf_Visteon_Part_Number[i] = Manuf_Visteon_Part_Number[i];
            wr_eep_by_list_req[EEP_CONTENT_Manuf_Visteon_Part_Number_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_Manuf_Visteon_Part_Number_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_Manuf_Visteon_Part_Number_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_Manuf_Visteon_Part_Number_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:Manuf_Visteon_Part_Number firt item[0] = %d",Manuf_Visteon_Part_Number[0]);
}

void GenEep_Set_Manuf_Visteon_Part_Number_Ex(uint8_t const* Manuf_Visteon_Part_Number,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_Manuf_Visteon_Part_Number_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_Manuf_Visteon_Part_Number_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.Manuf_Visteon_Part_Number;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != Manuf_Visteon_Part_Number[i] )
        {
            dest[offset+i] = Manuf_Visteon_Part_Number[i];
            wr_eep_by_list_req[EEP_CONTENT_Manuf_Visteon_Part_Number_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_Manuf_Visteon_Part_Number_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_Manuf_Visteon_Part_Number_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_Manuf_Visteon_Part_Number_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:Manuf_Visteon_Part_Number by Extend Api");
}

static void EepCopy_Manuf_Visteon_Part_Number_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_Manuf_Visteon_Part_Number_ITEM_LEN; i ++)
    {
        eep_hw_image.Manuf_Visteon_Part_Number[i] = eep_ram_image.Manuf_Visteon_Part_Number[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_Manuf_Visteon_Part_Number_IN_BLOCK_NUM);
}

#else 
uint8_t GenEep_Get_Manuf_Visteon_Part_Number(void)
{
    uint8_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.Manuf_Visteon_Part_Number;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_Manuf_Visteon_Part_Number(uint8_t const Manuf_Visteon_Part_Number)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.Manuf_Visteon_Part_Number != Manuf_Visteon_Part_Number )
    {
        eep_ram_image.Manuf_Visteon_Part_Number = Manuf_Visteon_Part_Number;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_Manuf_Visteon_Part_Number_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_Manuf_Visteon_Part_Number_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_Manuf_Visteon_Part_Number_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_Manuf_Visteon_Part_Number_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:Manuf_Visteon_Part_Number=%d",Manuf_Visteon_Part_Number);
}

static void EepCopy_Manuf_Visteon_Part_Number_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.Manuf_Visteon_Part_Number = eep_ram_image.Manuf_Visteon_Part_Number;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_Manuf_Visteon_Part_Number_IN_BLOCK_NUM);
}
#endif
//static void Proc_Manuf_Visteon_Part_Number_RestoreToDefault(void)
//{
//#if EEP_CONTENT_Manuf_Visteon_Part_Number_CONST_VARIBLE
//	GenEep_Set_Manuf_Visteon_Part_Number(dflt_eep_val.Manuf_Visteon_Part_Number);
//#endif	
//}
#if EEP_CONTENT_Manuf_Visteon_Part_Number_CONST_VARIBLE
#define Proc_Manuf_Visteon_Part_Number_RestoreToDefault()	do{GenEep_Set_Manuf_Visteon_Part_Number(dflt_eep_val.Manuf_Visteon_Part_Number);}while(0)
#else
#define Proc_Manuf_Visteon_Part_Number_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> Manuf_Visteon_Part_Number function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> Manuf_EquippedPCB_VPN_Sub function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_Manuf_EquippedPCB_VPN_Sub_ITEM_LEN > 1
void GenEep_Get_Manuf_EquippedPCB_VPN_Sub(uint8_t * const Manuf_EquippedPCB_VPN_Sub )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_Manuf_EquippedPCB_VPN_Sub_ITEM_LEN; i ++)
    {
        Manuf_EquippedPCB_VPN_Sub[i] = eep_ram_image.Manuf_EquippedPCB_VPN_Sub[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_Manuf_EquippedPCB_VPN_Sub_Ex(uint8_t * const Manuf_EquippedPCB_VPN_Sub,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.Manuf_EquippedPCB_VPN_Sub;	//EEP_CONTENT_Manuf_EquippedPCB_VPN_Sub_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_Manuf_EquippedPCB_VPN_Sub_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_Manuf_EquippedPCB_VPN_Sub_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        Manuf_EquippedPCB_VPN_Sub[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_Manuf_EquippedPCB_VPN_Sub(uint8_t const* Manuf_EquippedPCB_VPN_Sub )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_Manuf_EquippedPCB_VPN_Sub_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.Manuf_EquippedPCB_VPN_Sub[i] != Manuf_EquippedPCB_VPN_Sub[i] )
        {
            eep_ram_image.Manuf_EquippedPCB_VPN_Sub[i] = Manuf_EquippedPCB_VPN_Sub[i];
            wr_eep_by_list_req[EEP_CONTENT_Manuf_EquippedPCB_VPN_Sub_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_Manuf_EquippedPCB_VPN_Sub_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_Manuf_EquippedPCB_VPN_Sub_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_Manuf_EquippedPCB_VPN_Sub_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:Manuf_EquippedPCB_VPN_Sub firt item[0] = %d",Manuf_EquippedPCB_VPN_Sub[0]);
}

void GenEep_Set_Manuf_EquippedPCB_VPN_Sub_Ex(uint8_t const* Manuf_EquippedPCB_VPN_Sub,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_Manuf_EquippedPCB_VPN_Sub_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_Manuf_EquippedPCB_VPN_Sub_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.Manuf_EquippedPCB_VPN_Sub;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != Manuf_EquippedPCB_VPN_Sub[i] )
        {
            dest[offset+i] = Manuf_EquippedPCB_VPN_Sub[i];
            wr_eep_by_list_req[EEP_CONTENT_Manuf_EquippedPCB_VPN_Sub_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_Manuf_EquippedPCB_VPN_Sub_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_Manuf_EquippedPCB_VPN_Sub_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_Manuf_EquippedPCB_VPN_Sub_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:Manuf_EquippedPCB_VPN_Sub by Extend Api");
}

static void EepCopy_Manuf_EquippedPCB_VPN_Sub_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_Manuf_EquippedPCB_VPN_Sub_ITEM_LEN; i ++)
    {
        eep_hw_image.Manuf_EquippedPCB_VPN_Sub[i] = eep_ram_image.Manuf_EquippedPCB_VPN_Sub[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_Manuf_EquippedPCB_VPN_Sub_IN_BLOCK_NUM);
}

#else 
uint8_t GenEep_Get_Manuf_EquippedPCB_VPN_Sub(void)
{
    uint8_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.Manuf_EquippedPCB_VPN_Sub;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_Manuf_EquippedPCB_VPN_Sub(uint8_t const Manuf_EquippedPCB_VPN_Sub)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.Manuf_EquippedPCB_VPN_Sub != Manuf_EquippedPCB_VPN_Sub )
    {
        eep_ram_image.Manuf_EquippedPCB_VPN_Sub = Manuf_EquippedPCB_VPN_Sub;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_Manuf_EquippedPCB_VPN_Sub_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_Manuf_EquippedPCB_VPN_Sub_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_Manuf_EquippedPCB_VPN_Sub_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_Manuf_EquippedPCB_VPN_Sub_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:Manuf_EquippedPCB_VPN_Sub=%d",Manuf_EquippedPCB_VPN_Sub);
}

static void EepCopy_Manuf_EquippedPCB_VPN_Sub_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.Manuf_EquippedPCB_VPN_Sub = eep_ram_image.Manuf_EquippedPCB_VPN_Sub;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_Manuf_EquippedPCB_VPN_Sub_IN_BLOCK_NUM);
}
#endif
//static void Proc_Manuf_EquippedPCB_VPN_Sub_RestoreToDefault(void)
//{
//#if EEP_CONTENT_Manuf_EquippedPCB_VPN_Sub_CONST_VARIBLE
//	GenEep_Set_Manuf_EquippedPCB_VPN_Sub(dflt_eep_val.Manuf_EquippedPCB_VPN_Sub);
//#endif	
//}
#if EEP_CONTENT_Manuf_EquippedPCB_VPN_Sub_CONST_VARIBLE
#define Proc_Manuf_EquippedPCB_VPN_Sub_RestoreToDefault()	do{GenEep_Set_Manuf_EquippedPCB_VPN_Sub(dflt_eep_val.Manuf_EquippedPCB_VPN_Sub);}while(0)
#else
#define Proc_Manuf_EquippedPCB_VPN_Sub_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> Manuf_EquippedPCB_VPN_Sub function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> Manuf_EquippedPCB_VPN_Main function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_Manuf_EquippedPCB_VPN_Main_ITEM_LEN > 1
void GenEep_Get_Manuf_EquippedPCB_VPN_Main(uint8_t * const Manuf_EquippedPCB_VPN_Main )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_Manuf_EquippedPCB_VPN_Main_ITEM_LEN; i ++)
    {
        Manuf_EquippedPCB_VPN_Main[i] = eep_ram_image.Manuf_EquippedPCB_VPN_Main[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_Manuf_EquippedPCB_VPN_Main_Ex(uint8_t * const Manuf_EquippedPCB_VPN_Main,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.Manuf_EquippedPCB_VPN_Main;	//EEP_CONTENT_Manuf_EquippedPCB_VPN_Main_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_Manuf_EquippedPCB_VPN_Main_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_Manuf_EquippedPCB_VPN_Main_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        Manuf_EquippedPCB_VPN_Main[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_Manuf_EquippedPCB_VPN_Main(uint8_t const* Manuf_EquippedPCB_VPN_Main )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_Manuf_EquippedPCB_VPN_Main_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.Manuf_EquippedPCB_VPN_Main[i] != Manuf_EquippedPCB_VPN_Main[i] )
        {
            eep_ram_image.Manuf_EquippedPCB_VPN_Main[i] = Manuf_EquippedPCB_VPN_Main[i];
            wr_eep_by_list_req[EEP_CONTENT_Manuf_EquippedPCB_VPN_Main_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_Manuf_EquippedPCB_VPN_Main_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_Manuf_EquippedPCB_VPN_Main_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_Manuf_EquippedPCB_VPN_Main_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:Manuf_EquippedPCB_VPN_Main firt item[0] = %d",Manuf_EquippedPCB_VPN_Main[0]);
}

void GenEep_Set_Manuf_EquippedPCB_VPN_Main_Ex(uint8_t const* Manuf_EquippedPCB_VPN_Main,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_Manuf_EquippedPCB_VPN_Main_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_Manuf_EquippedPCB_VPN_Main_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.Manuf_EquippedPCB_VPN_Main;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != Manuf_EquippedPCB_VPN_Main[i] )
        {
            dest[offset+i] = Manuf_EquippedPCB_VPN_Main[i];
            wr_eep_by_list_req[EEP_CONTENT_Manuf_EquippedPCB_VPN_Main_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_Manuf_EquippedPCB_VPN_Main_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_Manuf_EquippedPCB_VPN_Main_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_Manuf_EquippedPCB_VPN_Main_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:Manuf_EquippedPCB_VPN_Main by Extend Api");
}

static void EepCopy_Manuf_EquippedPCB_VPN_Main_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_Manuf_EquippedPCB_VPN_Main_ITEM_LEN; i ++)
    {
        eep_hw_image.Manuf_EquippedPCB_VPN_Main[i] = eep_ram_image.Manuf_EquippedPCB_VPN_Main[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_Manuf_EquippedPCB_VPN_Main_IN_BLOCK_NUM);
}

#else 
uint8_t GenEep_Get_Manuf_EquippedPCB_VPN_Main(void)
{
    uint8_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.Manuf_EquippedPCB_VPN_Main;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_Manuf_EquippedPCB_VPN_Main(uint8_t const Manuf_EquippedPCB_VPN_Main)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.Manuf_EquippedPCB_VPN_Main != Manuf_EquippedPCB_VPN_Main )
    {
        eep_ram_image.Manuf_EquippedPCB_VPN_Main = Manuf_EquippedPCB_VPN_Main;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_Manuf_EquippedPCB_VPN_Main_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_Manuf_EquippedPCB_VPN_Main_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_Manuf_EquippedPCB_VPN_Main_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_Manuf_EquippedPCB_VPN_Main_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:Manuf_EquippedPCB_VPN_Main=%d",Manuf_EquippedPCB_VPN_Main);
}

static void EepCopy_Manuf_EquippedPCB_VPN_Main_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.Manuf_EquippedPCB_VPN_Main = eep_ram_image.Manuf_EquippedPCB_VPN_Main;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_Manuf_EquippedPCB_VPN_Main_IN_BLOCK_NUM);
}
#endif
//static void Proc_Manuf_EquippedPCB_VPN_Main_RestoreToDefault(void)
//{
//#if EEP_CONTENT_Manuf_EquippedPCB_VPN_Main_CONST_VARIBLE
//	GenEep_Set_Manuf_EquippedPCB_VPN_Main(dflt_eep_val.Manuf_EquippedPCB_VPN_Main);
//#endif	
//}
#if EEP_CONTENT_Manuf_EquippedPCB_VPN_Main_CONST_VARIBLE
#define Proc_Manuf_EquippedPCB_VPN_Main_RestoreToDefault()	do{GenEep_Set_Manuf_EquippedPCB_VPN_Main(dflt_eep_val.Manuf_EquippedPCB_VPN_Main);}while(0)
#else
#define Proc_Manuf_EquippedPCB_VPN_Main_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> Manuf_EquippedPCB_VPN_Main function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> Manuf_Product_Serial_Number function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_Manuf_Product_Serial_Number_ITEM_LEN > 1
void GenEep_Get_Manuf_Product_Serial_Number(uint8_t * const Manuf_Product_Serial_Number )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_Manuf_Product_Serial_Number_ITEM_LEN; i ++)
    {
        Manuf_Product_Serial_Number[i] = eep_ram_image.Manuf_Product_Serial_Number[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_Manuf_Product_Serial_Number_Ex(uint8_t * const Manuf_Product_Serial_Number,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.Manuf_Product_Serial_Number;	//EEP_CONTENT_Manuf_Product_Serial_Number_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_Manuf_Product_Serial_Number_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_Manuf_Product_Serial_Number_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        Manuf_Product_Serial_Number[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_Manuf_Product_Serial_Number(uint8_t const* Manuf_Product_Serial_Number )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_Manuf_Product_Serial_Number_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.Manuf_Product_Serial_Number[i] != Manuf_Product_Serial_Number[i] )
        {
            eep_ram_image.Manuf_Product_Serial_Number[i] = Manuf_Product_Serial_Number[i];
            wr_eep_by_list_req[EEP_CONTENT_Manuf_Product_Serial_Number_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_Manuf_Product_Serial_Number_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_Manuf_Product_Serial_Number_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_Manuf_Product_Serial_Number_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:Manuf_Product_Serial_Number firt item[0] = %d",Manuf_Product_Serial_Number[0]);
}

void GenEep_Set_Manuf_Product_Serial_Number_Ex(uint8_t const* Manuf_Product_Serial_Number,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_Manuf_Product_Serial_Number_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_Manuf_Product_Serial_Number_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.Manuf_Product_Serial_Number;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != Manuf_Product_Serial_Number[i] )
        {
            dest[offset+i] = Manuf_Product_Serial_Number[i];
            wr_eep_by_list_req[EEP_CONTENT_Manuf_Product_Serial_Number_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_Manuf_Product_Serial_Number_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_Manuf_Product_Serial_Number_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_Manuf_Product_Serial_Number_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:Manuf_Product_Serial_Number by Extend Api");
}

static void EepCopy_Manuf_Product_Serial_Number_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_Manuf_Product_Serial_Number_ITEM_LEN; i ++)
    {
        eep_hw_image.Manuf_Product_Serial_Number[i] = eep_ram_image.Manuf_Product_Serial_Number[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_Manuf_Product_Serial_Number_IN_BLOCK_NUM);
}

#else 
uint8_t GenEep_Get_Manuf_Product_Serial_Number(void)
{
    uint8_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.Manuf_Product_Serial_Number;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_Manuf_Product_Serial_Number(uint8_t const Manuf_Product_Serial_Number)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.Manuf_Product_Serial_Number != Manuf_Product_Serial_Number )
    {
        eep_ram_image.Manuf_Product_Serial_Number = Manuf_Product_Serial_Number;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_Manuf_Product_Serial_Number_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_Manuf_Product_Serial_Number_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_Manuf_Product_Serial_Number_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_Manuf_Product_Serial_Number_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:Manuf_Product_Serial_Number=%d",Manuf_Product_Serial_Number);
}

static void EepCopy_Manuf_Product_Serial_Number_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.Manuf_Product_Serial_Number = eep_ram_image.Manuf_Product_Serial_Number;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_Manuf_Product_Serial_Number_IN_BLOCK_NUM);
}
#endif
//static void Proc_Manuf_Product_Serial_Number_RestoreToDefault(void)
//{
//#if EEP_CONTENT_Manuf_Product_Serial_Number_CONST_VARIBLE
//	GenEep_Set_Manuf_Product_Serial_Number(dflt_eep_val.Manuf_Product_Serial_Number);
//#endif	
//}
#if EEP_CONTENT_Manuf_Product_Serial_Number_CONST_VARIBLE
#define Proc_Manuf_Product_Serial_Number_RestoreToDefault()	do{GenEep_Set_Manuf_Product_Serial_Number(dflt_eep_val.Manuf_Product_Serial_Number);}while(0)
#else
#define Proc_Manuf_Product_Serial_Number_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> Manuf_Product_Serial_Number function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> Manuf_EquippedPCB_Serial_Number_Sub function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Sub_ITEM_LEN > 1
void GenEep_Get_Manuf_EquippedPCB_Serial_Number_Sub(uint8_t * const Manuf_EquippedPCB_Serial_Number_Sub )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Sub_ITEM_LEN; i ++)
    {
        Manuf_EquippedPCB_Serial_Number_Sub[i] = eep_ram_image.Manuf_EquippedPCB_Serial_Number_Sub[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_Manuf_EquippedPCB_Serial_Number_Sub_Ex(uint8_t * const Manuf_EquippedPCB_Serial_Number_Sub,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.Manuf_EquippedPCB_Serial_Number_Sub;	//EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Sub_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Sub_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Sub_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        Manuf_EquippedPCB_Serial_Number_Sub[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_Manuf_EquippedPCB_Serial_Number_Sub(uint8_t const* Manuf_EquippedPCB_Serial_Number_Sub )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Sub_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.Manuf_EquippedPCB_Serial_Number_Sub[i] != Manuf_EquippedPCB_Serial_Number_Sub[i] )
        {
            eep_ram_image.Manuf_EquippedPCB_Serial_Number_Sub[i] = Manuf_EquippedPCB_Serial_Number_Sub[i];
            wr_eep_by_list_req[EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Sub_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Sub_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Sub_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Sub_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:Manuf_EquippedPCB_Serial_Number_Sub firt item[0] = %d",Manuf_EquippedPCB_Serial_Number_Sub[0]);
}

void GenEep_Set_Manuf_EquippedPCB_Serial_Number_Sub_Ex(uint8_t const* Manuf_EquippedPCB_Serial_Number_Sub,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Sub_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Sub_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.Manuf_EquippedPCB_Serial_Number_Sub;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != Manuf_EquippedPCB_Serial_Number_Sub[i] )
        {
            dest[offset+i] = Manuf_EquippedPCB_Serial_Number_Sub[i];
            wr_eep_by_list_req[EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Sub_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Sub_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Sub_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Sub_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:Manuf_EquippedPCB_Serial_Number_Sub by Extend Api");
}

static void EepCopy_Manuf_EquippedPCB_Serial_Number_Sub_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Sub_ITEM_LEN; i ++)
    {
        eep_hw_image.Manuf_EquippedPCB_Serial_Number_Sub[i] = eep_ram_image.Manuf_EquippedPCB_Serial_Number_Sub[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Sub_IN_BLOCK_NUM);
}

#else 
uint8_t GenEep_Get_Manuf_EquippedPCB_Serial_Number_Sub(void)
{
    uint8_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.Manuf_EquippedPCB_Serial_Number_Sub;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_Manuf_EquippedPCB_Serial_Number_Sub(uint8_t const Manuf_EquippedPCB_Serial_Number_Sub)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.Manuf_EquippedPCB_Serial_Number_Sub != Manuf_EquippedPCB_Serial_Number_Sub )
    {
        eep_ram_image.Manuf_EquippedPCB_Serial_Number_Sub = Manuf_EquippedPCB_Serial_Number_Sub;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Sub_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Sub_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Sub_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Sub_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:Manuf_EquippedPCB_Serial_Number_Sub=%d",Manuf_EquippedPCB_Serial_Number_Sub);
}

static void EepCopy_Manuf_EquippedPCB_Serial_Number_Sub_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.Manuf_EquippedPCB_Serial_Number_Sub = eep_ram_image.Manuf_EquippedPCB_Serial_Number_Sub;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Sub_IN_BLOCK_NUM);
}
#endif
//static void Proc_Manuf_EquippedPCB_Serial_Number_Sub_RestoreToDefault(void)
//{
//#if EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Sub_CONST_VARIBLE
//	GenEep_Set_Manuf_EquippedPCB_Serial_Number_Sub(dflt_eep_val.Manuf_EquippedPCB_Serial_Number_Sub);
//#endif	
//}
#if EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Sub_CONST_VARIBLE
#define Proc_Manuf_EquippedPCB_Serial_Number_Sub_RestoreToDefault()	do{GenEep_Set_Manuf_EquippedPCB_Serial_Number_Sub(dflt_eep_val.Manuf_EquippedPCB_Serial_Number_Sub);}while(0)
#else
#define Proc_Manuf_EquippedPCB_Serial_Number_Sub_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> Manuf_EquippedPCB_Serial_Number_Sub function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> Manuf_EquippedPCB_Serial_Number_Main function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Main_ITEM_LEN > 1
void GenEep_Get_Manuf_EquippedPCB_Serial_Number_Main(uint8_t * const Manuf_EquippedPCB_Serial_Number_Main )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Main_ITEM_LEN; i ++)
    {
        Manuf_EquippedPCB_Serial_Number_Main[i] = eep_ram_image.Manuf_EquippedPCB_Serial_Number_Main[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_Manuf_EquippedPCB_Serial_Number_Main_Ex(uint8_t * const Manuf_EquippedPCB_Serial_Number_Main,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.Manuf_EquippedPCB_Serial_Number_Main;	//EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Main_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Main_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Main_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        Manuf_EquippedPCB_Serial_Number_Main[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_Manuf_EquippedPCB_Serial_Number_Main(uint8_t const* Manuf_EquippedPCB_Serial_Number_Main )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Main_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.Manuf_EquippedPCB_Serial_Number_Main[i] != Manuf_EquippedPCB_Serial_Number_Main[i] )
        {
            eep_ram_image.Manuf_EquippedPCB_Serial_Number_Main[i] = Manuf_EquippedPCB_Serial_Number_Main[i];
            wr_eep_by_list_req[EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Main_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Main_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Main_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Main_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:Manuf_EquippedPCB_Serial_Number_Main firt item[0] = %d",Manuf_EquippedPCB_Serial_Number_Main[0]);
}

void GenEep_Set_Manuf_EquippedPCB_Serial_Number_Main_Ex(uint8_t const* Manuf_EquippedPCB_Serial_Number_Main,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Main_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Main_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.Manuf_EquippedPCB_Serial_Number_Main;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != Manuf_EquippedPCB_Serial_Number_Main[i] )
        {
            dest[offset+i] = Manuf_EquippedPCB_Serial_Number_Main[i];
            wr_eep_by_list_req[EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Main_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Main_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Main_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Main_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:Manuf_EquippedPCB_Serial_Number_Main by Extend Api");
}

static void EepCopy_Manuf_EquippedPCB_Serial_Number_Main_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Main_ITEM_LEN; i ++)
    {
        eep_hw_image.Manuf_EquippedPCB_Serial_Number_Main[i] = eep_ram_image.Manuf_EquippedPCB_Serial_Number_Main[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Main_IN_BLOCK_NUM);
}

#else 
uint8_t GenEep_Get_Manuf_EquippedPCB_Serial_Number_Main(void)
{
    uint8_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.Manuf_EquippedPCB_Serial_Number_Main;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_Manuf_EquippedPCB_Serial_Number_Main(uint8_t const Manuf_EquippedPCB_Serial_Number_Main)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.Manuf_EquippedPCB_Serial_Number_Main != Manuf_EquippedPCB_Serial_Number_Main )
    {
        eep_ram_image.Manuf_EquippedPCB_Serial_Number_Main = Manuf_EquippedPCB_Serial_Number_Main;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Main_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Main_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Main_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Main_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:Manuf_EquippedPCB_Serial_Number_Main=%d",Manuf_EquippedPCB_Serial_Number_Main);
}

static void EepCopy_Manuf_EquippedPCB_Serial_Number_Main_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.Manuf_EquippedPCB_Serial_Number_Main = eep_ram_image.Manuf_EquippedPCB_Serial_Number_Main;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Main_IN_BLOCK_NUM);
}
#endif
//static void Proc_Manuf_EquippedPCB_Serial_Number_Main_RestoreToDefault(void)
//{
//#if EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Main_CONST_VARIBLE
//	GenEep_Set_Manuf_EquippedPCB_Serial_Number_Main(dflt_eep_val.Manuf_EquippedPCB_Serial_Number_Main);
//#endif	
//}
#if EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Main_CONST_VARIBLE
#define Proc_Manuf_EquippedPCB_Serial_Number_Main_RestoreToDefault()	do{GenEep_Set_Manuf_EquippedPCB_Serial_Number_Main(dflt_eep_val.Manuf_EquippedPCB_Serial_Number_Main);}while(0)
#else
#define Proc_Manuf_EquippedPCB_Serial_Number_Main_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> Manuf_EquippedPCB_Serial_Number_Main function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> NVM_Revision function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_NVM_Revision_ITEM_LEN > 1
void GenEep_Get_NVM_Revision(uint8_t * const NVM_Revision )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_NVM_Revision_ITEM_LEN; i ++)
    {
        NVM_Revision[i] = eep_ram_image.NVM_Revision[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_NVM_Revision_Ex(uint8_t * const NVM_Revision,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.NVM_Revision;	//EEP_CONTENT_NVM_Revision_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_NVM_Revision_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_NVM_Revision_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        NVM_Revision[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_NVM_Revision(uint8_t const* NVM_Revision )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_NVM_Revision_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.NVM_Revision[i] != NVM_Revision[i] )
        {
            eep_ram_image.NVM_Revision[i] = NVM_Revision[i];
            wr_eep_by_list_req[EEP_CONTENT_NVM_Revision_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_NVM_Revision_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_NVM_Revision_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_NVM_Revision_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:NVM_Revision firt item[0] = %d",NVM_Revision[0]);
}

void GenEep_Set_NVM_Revision_Ex(uint8_t const* NVM_Revision,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_NVM_Revision_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_NVM_Revision_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.NVM_Revision;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != NVM_Revision[i] )
        {
            dest[offset+i] = NVM_Revision[i];
            wr_eep_by_list_req[EEP_CONTENT_NVM_Revision_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_NVM_Revision_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_NVM_Revision_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_NVM_Revision_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:NVM_Revision by Extend Api");
}

static void EepCopy_NVM_Revision_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_NVM_Revision_ITEM_LEN; i ++)
    {
        eep_hw_image.NVM_Revision[i] = eep_ram_image.NVM_Revision[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_NVM_Revision_IN_BLOCK_NUM);
}

#else 
uint8_t GenEep_Get_NVM_Revision(void)
{
    uint8_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.NVM_Revision;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_NVM_Revision(uint8_t const NVM_Revision)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.NVM_Revision != NVM_Revision )
    {
        eep_ram_image.NVM_Revision = NVM_Revision;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_NVM_Revision_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_NVM_Revision_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_NVM_Revision_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_NVM_Revision_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:NVM_Revision=%d",NVM_Revision);
}

static void EepCopy_NVM_Revision_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.NVM_Revision = eep_ram_image.NVM_Revision;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_NVM_Revision_IN_BLOCK_NUM);
}
#endif
//static void Proc_NVM_Revision_RestoreToDefault(void)
//{
//#if EEP_CONTENT_NVM_Revision_CONST_VARIBLE
//	GenEep_Set_NVM_Revision(dflt_eep_val.NVM_Revision);
//#endif	
//}
#if EEP_CONTENT_NVM_Revision_CONST_VARIBLE
#define Proc_NVM_Revision_RestoreToDefault()	do{GenEep_Set_NVM_Revision(dflt_eep_val.NVM_Revision);}while(0)
#else
#define Proc_NVM_Revision_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> NVM_Revision function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> Manuf_SMD_Date1 function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_Manuf_SMD_Date1_ITEM_LEN > 1
void GenEep_Get_Manuf_SMD_Date1(uint8_t * const Manuf_SMD_Date1 )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_Manuf_SMD_Date1_ITEM_LEN; i ++)
    {
        Manuf_SMD_Date1[i] = eep_ram_image.Manuf_SMD_Date1[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_Manuf_SMD_Date1_Ex(uint8_t * const Manuf_SMD_Date1,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.Manuf_SMD_Date1;	//EEP_CONTENT_Manuf_SMD_Date1_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_Manuf_SMD_Date1_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_Manuf_SMD_Date1_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        Manuf_SMD_Date1[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_Manuf_SMD_Date1(uint8_t const* Manuf_SMD_Date1 )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_Manuf_SMD_Date1_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.Manuf_SMD_Date1[i] != Manuf_SMD_Date1[i] )
        {
            eep_ram_image.Manuf_SMD_Date1[i] = Manuf_SMD_Date1[i];
            wr_eep_by_list_req[EEP_CONTENT_Manuf_SMD_Date1_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_Manuf_SMD_Date1_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_Manuf_SMD_Date1_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_Manuf_SMD_Date1_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:Manuf_SMD_Date1 firt item[0] = %d",Manuf_SMD_Date1[0]);
}

void GenEep_Set_Manuf_SMD_Date1_Ex(uint8_t const* Manuf_SMD_Date1,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_Manuf_SMD_Date1_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_Manuf_SMD_Date1_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.Manuf_SMD_Date1;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != Manuf_SMD_Date1[i] )
        {
            dest[offset+i] = Manuf_SMD_Date1[i];
            wr_eep_by_list_req[EEP_CONTENT_Manuf_SMD_Date1_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_Manuf_SMD_Date1_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_Manuf_SMD_Date1_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_Manuf_SMD_Date1_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:Manuf_SMD_Date1 by Extend Api");
}

static void EepCopy_Manuf_SMD_Date1_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_Manuf_SMD_Date1_ITEM_LEN; i ++)
    {
        eep_hw_image.Manuf_SMD_Date1[i] = eep_ram_image.Manuf_SMD_Date1[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_Manuf_SMD_Date1_IN_BLOCK_NUM);
}

#else 
uint8_t GenEep_Get_Manuf_SMD_Date1(void)
{
    uint8_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.Manuf_SMD_Date1;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_Manuf_SMD_Date1(uint8_t const Manuf_SMD_Date1)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.Manuf_SMD_Date1 != Manuf_SMD_Date1 )
    {
        eep_ram_image.Manuf_SMD_Date1 = Manuf_SMD_Date1;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_Manuf_SMD_Date1_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_Manuf_SMD_Date1_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_Manuf_SMD_Date1_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_Manuf_SMD_Date1_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:Manuf_SMD_Date1=%d",Manuf_SMD_Date1);
}

static void EepCopy_Manuf_SMD_Date1_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.Manuf_SMD_Date1 = eep_ram_image.Manuf_SMD_Date1;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_Manuf_SMD_Date1_IN_BLOCK_NUM);
}
#endif
//static void Proc_Manuf_SMD_Date1_RestoreToDefault(void)
//{
//#if EEP_CONTENT_Manuf_SMD_Date1_CONST_VARIBLE
//	GenEep_Set_Manuf_SMD_Date1(dflt_eep_val.Manuf_SMD_Date1);
//#endif	
//}
#if EEP_CONTENT_Manuf_SMD_Date1_CONST_VARIBLE
#define Proc_Manuf_SMD_Date1_RestoreToDefault()	do{GenEep_Set_Manuf_SMD_Date1(dflt_eep_val.Manuf_SMD_Date1);}while(0)
#else
#define Proc_Manuf_SMD_Date1_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> Manuf_SMD_Date1 function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> Manuf_SMD_Date2 function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_Manuf_SMD_Date2_ITEM_LEN > 1
void GenEep_Get_Manuf_SMD_Date2(uint8_t * const Manuf_SMD_Date2 )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_Manuf_SMD_Date2_ITEM_LEN; i ++)
    {
        Manuf_SMD_Date2[i] = eep_ram_image.Manuf_SMD_Date2[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_Manuf_SMD_Date2_Ex(uint8_t * const Manuf_SMD_Date2,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.Manuf_SMD_Date2;	//EEP_CONTENT_Manuf_SMD_Date2_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_Manuf_SMD_Date2_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_Manuf_SMD_Date2_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        Manuf_SMD_Date2[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_Manuf_SMD_Date2(uint8_t const* Manuf_SMD_Date2 )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_Manuf_SMD_Date2_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.Manuf_SMD_Date2[i] != Manuf_SMD_Date2[i] )
        {
            eep_ram_image.Manuf_SMD_Date2[i] = Manuf_SMD_Date2[i];
            wr_eep_by_list_req[EEP_CONTENT_Manuf_SMD_Date2_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_Manuf_SMD_Date2_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_Manuf_SMD_Date2_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_Manuf_SMD_Date2_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:Manuf_SMD_Date2 firt item[0] = %d",Manuf_SMD_Date2[0]);
}

void GenEep_Set_Manuf_SMD_Date2_Ex(uint8_t const* Manuf_SMD_Date2,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_Manuf_SMD_Date2_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_Manuf_SMD_Date2_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.Manuf_SMD_Date2;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != Manuf_SMD_Date2[i] )
        {
            dest[offset+i] = Manuf_SMD_Date2[i];
            wr_eep_by_list_req[EEP_CONTENT_Manuf_SMD_Date2_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_Manuf_SMD_Date2_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_Manuf_SMD_Date2_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_Manuf_SMD_Date2_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:Manuf_SMD_Date2 by Extend Api");
}

static void EepCopy_Manuf_SMD_Date2_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_Manuf_SMD_Date2_ITEM_LEN; i ++)
    {
        eep_hw_image.Manuf_SMD_Date2[i] = eep_ram_image.Manuf_SMD_Date2[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_Manuf_SMD_Date2_IN_BLOCK_NUM);
}

#else 
uint8_t GenEep_Get_Manuf_SMD_Date2(void)
{
    uint8_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.Manuf_SMD_Date2;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_Manuf_SMD_Date2(uint8_t const Manuf_SMD_Date2)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.Manuf_SMD_Date2 != Manuf_SMD_Date2 )
    {
        eep_ram_image.Manuf_SMD_Date2 = Manuf_SMD_Date2;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_Manuf_SMD_Date2_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_Manuf_SMD_Date2_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_Manuf_SMD_Date2_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_Manuf_SMD_Date2_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:Manuf_SMD_Date2=%d",Manuf_SMD_Date2);
}

static void EepCopy_Manuf_SMD_Date2_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.Manuf_SMD_Date2 = eep_ram_image.Manuf_SMD_Date2;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_Manuf_SMD_Date2_IN_BLOCK_NUM);
}
#endif
//static void Proc_Manuf_SMD_Date2_RestoreToDefault(void)
//{
//#if EEP_CONTENT_Manuf_SMD_Date2_CONST_VARIBLE
//	GenEep_Set_Manuf_SMD_Date2(dflt_eep_val.Manuf_SMD_Date2);
//#endif	
//}
#if EEP_CONTENT_Manuf_SMD_Date2_CONST_VARIBLE
#define Proc_Manuf_SMD_Date2_RestoreToDefault()	do{GenEep_Set_Manuf_SMD_Date2(dflt_eep_val.Manuf_SMD_Date2);}while(0)
#else
#define Proc_Manuf_SMD_Date2_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> Manuf_SMD_Date2 function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> Manuf_Assembly_Date function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_Manuf_Assembly_Date_ITEM_LEN > 1
void GenEep_Get_Manuf_Assembly_Date(uint8_t * const Manuf_Assembly_Date )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_Manuf_Assembly_Date_ITEM_LEN; i ++)
    {
        Manuf_Assembly_Date[i] = eep_ram_image.Manuf_Assembly_Date[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_Manuf_Assembly_Date_Ex(uint8_t * const Manuf_Assembly_Date,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.Manuf_Assembly_Date;	//EEP_CONTENT_Manuf_Assembly_Date_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_Manuf_Assembly_Date_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_Manuf_Assembly_Date_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        Manuf_Assembly_Date[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_Manuf_Assembly_Date(uint8_t const* Manuf_Assembly_Date )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_Manuf_Assembly_Date_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.Manuf_Assembly_Date[i] != Manuf_Assembly_Date[i] )
        {
            eep_ram_image.Manuf_Assembly_Date[i] = Manuf_Assembly_Date[i];
            wr_eep_by_list_req[EEP_CONTENT_Manuf_Assembly_Date_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_Manuf_Assembly_Date_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_Manuf_Assembly_Date_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_Manuf_Assembly_Date_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:Manuf_Assembly_Date firt item[0] = %d",Manuf_Assembly_Date[0]);
}

void GenEep_Set_Manuf_Assembly_Date_Ex(uint8_t const* Manuf_Assembly_Date,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_Manuf_Assembly_Date_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_Manuf_Assembly_Date_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.Manuf_Assembly_Date;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != Manuf_Assembly_Date[i] )
        {
            dest[offset+i] = Manuf_Assembly_Date[i];
            wr_eep_by_list_req[EEP_CONTENT_Manuf_Assembly_Date_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_Manuf_Assembly_Date_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_Manuf_Assembly_Date_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_Manuf_Assembly_Date_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:Manuf_Assembly_Date by Extend Api");
}

static void EepCopy_Manuf_Assembly_Date_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_Manuf_Assembly_Date_ITEM_LEN; i ++)
    {
        eep_hw_image.Manuf_Assembly_Date[i] = eep_ram_image.Manuf_Assembly_Date[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_Manuf_Assembly_Date_IN_BLOCK_NUM);
}

#else 
uint8_t GenEep_Get_Manuf_Assembly_Date(void)
{
    uint8_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.Manuf_Assembly_Date;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_Manuf_Assembly_Date(uint8_t const Manuf_Assembly_Date)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.Manuf_Assembly_Date != Manuf_Assembly_Date )
    {
        eep_ram_image.Manuf_Assembly_Date = Manuf_Assembly_Date;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_Manuf_Assembly_Date_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_Manuf_Assembly_Date_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_Manuf_Assembly_Date_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_Manuf_Assembly_Date_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:Manuf_Assembly_Date=%d",Manuf_Assembly_Date);
}

static void EepCopy_Manuf_Assembly_Date_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.Manuf_Assembly_Date = eep_ram_image.Manuf_Assembly_Date;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_Manuf_Assembly_Date_IN_BLOCK_NUM);
}
#endif
//static void Proc_Manuf_Assembly_Date_RestoreToDefault(void)
//{
//#if EEP_CONTENT_Manuf_Assembly_Date_CONST_VARIBLE
//	GenEep_Set_Manuf_Assembly_Date(dflt_eep_val.Manuf_Assembly_Date);
//#endif	
//}
#if EEP_CONTENT_Manuf_Assembly_Date_CONST_VARIBLE
#define Proc_Manuf_Assembly_Date_RestoreToDefault()	do{GenEep_Set_Manuf_Assembly_Date(dflt_eep_val.Manuf_Assembly_Date);}while(0)
#else
#define Proc_Manuf_Assembly_Date_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> Manuf_Assembly_Date function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> Traceability_Data function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_Traceability_Data_ITEM_LEN > 1
void GenEep_Get_Traceability_Data(uint8_t * const Traceability_Data )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_Traceability_Data_ITEM_LEN; i ++)
    {
        Traceability_Data[i] = eep_ram_image.Traceability_Data[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_Traceability_Data_Ex(uint8_t * const Traceability_Data,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.Traceability_Data;	//EEP_CONTENT_Traceability_Data_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_Traceability_Data_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_Traceability_Data_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        Traceability_Data[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_Traceability_Data(uint8_t const* Traceability_Data )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_Traceability_Data_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.Traceability_Data[i] != Traceability_Data[i] )
        {
            eep_ram_image.Traceability_Data[i] = Traceability_Data[i];
            wr_eep_by_list_req[EEP_CONTENT_Traceability_Data_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_Traceability_Data_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_Traceability_Data_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_Traceability_Data_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:Traceability_Data firt item[0] = %d",Traceability_Data[0]);
}

void GenEep_Set_Traceability_Data_Ex(uint8_t const* Traceability_Data,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_Traceability_Data_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_Traceability_Data_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.Traceability_Data;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != Traceability_Data[i] )
        {
            dest[offset+i] = Traceability_Data[i];
            wr_eep_by_list_req[EEP_CONTENT_Traceability_Data_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_Traceability_Data_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_Traceability_Data_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_Traceability_Data_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:Traceability_Data by Extend Api");
}

static void EepCopy_Traceability_Data_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_Traceability_Data_ITEM_LEN; i ++)
    {
        eep_hw_image.Traceability_Data[i] = eep_ram_image.Traceability_Data[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_Traceability_Data_IN_BLOCK_NUM);
}

#else 
uint8_t GenEep_Get_Traceability_Data(void)
{
    uint8_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.Traceability_Data;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_Traceability_Data(uint8_t const Traceability_Data)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.Traceability_Data != Traceability_Data )
    {
        eep_ram_image.Traceability_Data = Traceability_Data;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_Traceability_Data_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_Traceability_Data_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_Traceability_Data_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_Traceability_Data_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:Traceability_Data=%d",Traceability_Data);
}

static void EepCopy_Traceability_Data_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.Traceability_Data = eep_ram_image.Traceability_Data;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_Traceability_Data_IN_BLOCK_NUM);
}
#endif
//static void Proc_Traceability_Data_RestoreToDefault(void)
//{
//#if EEP_CONTENT_Traceability_Data_CONST_VARIBLE
//	GenEep_Set_Traceability_Data(dflt_eep_val.Traceability_Data);
//#endif	
//}
#if EEP_CONTENT_Traceability_Data_CONST_VARIBLE
#define Proc_Traceability_Data_RestoreToDefault()	do{GenEep_Set_Traceability_Data(dflt_eep_val.Traceability_Data);}while(0)
#else
#define Proc_Traceability_Data_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> Traceability_Data function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> VehicleManufacturerSparePartNumber function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_VehicleManufacturerSparePartNumber_ITEM_LEN > 1
void GenEep_Get_VehicleManufacturerSparePartNumber(uint8_t * const VehicleManufacturerSparePartNumber )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_VehicleManufacturerSparePartNumber_ITEM_LEN; i ++)
    {
        VehicleManufacturerSparePartNumber[i] = eep_ram_image.VehicleManufacturerSparePartNumber[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_VehicleManufacturerSparePartNumber_Ex(uint8_t * const VehicleManufacturerSparePartNumber,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.VehicleManufacturerSparePartNumber;	//EEP_CONTENT_VehicleManufacturerSparePartNumber_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_VehicleManufacturerSparePartNumber_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_VehicleManufacturerSparePartNumber_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        VehicleManufacturerSparePartNumber[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_VehicleManufacturerSparePartNumber(uint8_t const* VehicleManufacturerSparePartNumber )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_VehicleManufacturerSparePartNumber_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.VehicleManufacturerSparePartNumber[i] != VehicleManufacturerSparePartNumber[i] )
        {
            eep_ram_image.VehicleManufacturerSparePartNumber[i] = VehicleManufacturerSparePartNumber[i];
            wr_eep_by_list_req[EEP_CONTENT_VehicleManufacturerSparePartNumber_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_VehicleManufacturerSparePartNumber_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_VehicleManufacturerSparePartNumber_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_VehicleManufacturerSparePartNumber_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:VehicleManufacturerSparePartNumber firt item[0] = %d",VehicleManufacturerSparePartNumber[0]);
}

void GenEep_Set_VehicleManufacturerSparePartNumber_Ex(uint8_t const* VehicleManufacturerSparePartNumber,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_VehicleManufacturerSparePartNumber_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_VehicleManufacturerSparePartNumber_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.VehicleManufacturerSparePartNumber;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != VehicleManufacturerSparePartNumber[i] )
        {
            dest[offset+i] = VehicleManufacturerSparePartNumber[i];
            wr_eep_by_list_req[EEP_CONTENT_VehicleManufacturerSparePartNumber_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_VehicleManufacturerSparePartNumber_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_VehicleManufacturerSparePartNumber_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_VehicleManufacturerSparePartNumber_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:VehicleManufacturerSparePartNumber by Extend Api");
}

static void EepCopy_VehicleManufacturerSparePartNumber_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_VehicleManufacturerSparePartNumber_ITEM_LEN; i ++)
    {
        eep_hw_image.VehicleManufacturerSparePartNumber[i] = eep_ram_image.VehicleManufacturerSparePartNumber[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_VehicleManufacturerSparePartNumber_IN_BLOCK_NUM);
}

#else 
uint8_t GenEep_Get_VehicleManufacturerSparePartNumber(void)
{
    uint8_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.VehicleManufacturerSparePartNumber;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_VehicleManufacturerSparePartNumber(uint8_t const VehicleManufacturerSparePartNumber)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.VehicleManufacturerSparePartNumber != VehicleManufacturerSparePartNumber )
    {
        eep_ram_image.VehicleManufacturerSparePartNumber = VehicleManufacturerSparePartNumber;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_VehicleManufacturerSparePartNumber_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_VehicleManufacturerSparePartNumber_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_VehicleManufacturerSparePartNumber_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_VehicleManufacturerSparePartNumber_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:VehicleManufacturerSparePartNumber=%d",VehicleManufacturerSparePartNumber);
}

static void EepCopy_VehicleManufacturerSparePartNumber_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.VehicleManufacturerSparePartNumber = eep_ram_image.VehicleManufacturerSparePartNumber;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_VehicleManufacturerSparePartNumber_IN_BLOCK_NUM);
}
#endif
//static void Proc_VehicleManufacturerSparePartNumber_RestoreToDefault(void)
//{
//#if EEP_CONTENT_VehicleManufacturerSparePartNumber_CONST_VARIBLE
//	GenEep_Set_VehicleManufacturerSparePartNumber(dflt_eep_val.VehicleManufacturerSparePartNumber);
//#endif	
//}
#if EEP_CONTENT_VehicleManufacturerSparePartNumber_CONST_VARIBLE
#define Proc_VehicleManufacturerSparePartNumber_RestoreToDefault()	do{GenEep_Set_VehicleManufacturerSparePartNumber(dflt_eep_val.VehicleManufacturerSparePartNumber);}while(0)
#else
#define Proc_VehicleManufacturerSparePartNumber_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> VehicleManufacturerSparePartNumber function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> VehicleManufacturerSparePartNumber_Assembly function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_VehicleManufacturerSparePartNumber_Assembly_ITEM_LEN > 1
void GenEep_Get_VehicleManufacturerSparePartNumber_Assembly(uint8_t * const VehicleManufacturerSparePartNumber_Assembly )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_VehicleManufacturerSparePartNumber_Assembly_ITEM_LEN; i ++)
    {
        VehicleManufacturerSparePartNumber_Assembly[i] = eep_ram_image.VehicleManufacturerSparePartNumber_Assembly[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_VehicleManufacturerSparePartNumber_Assembly_Ex(uint8_t * const VehicleManufacturerSparePartNumber_Assembly,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.VehicleManufacturerSparePartNumber_Assembly;	//EEP_CONTENT_VehicleManufacturerSparePartNumber_Assembly_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_VehicleManufacturerSparePartNumber_Assembly_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_VehicleManufacturerSparePartNumber_Assembly_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        VehicleManufacturerSparePartNumber_Assembly[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_VehicleManufacturerSparePartNumber_Assembly(uint8_t const* VehicleManufacturerSparePartNumber_Assembly )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_VehicleManufacturerSparePartNumber_Assembly_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.VehicleManufacturerSparePartNumber_Assembly[i] != VehicleManufacturerSparePartNumber_Assembly[i] )
        {
            eep_ram_image.VehicleManufacturerSparePartNumber_Assembly[i] = VehicleManufacturerSparePartNumber_Assembly[i];
            wr_eep_by_list_req[EEP_CONTENT_VehicleManufacturerSparePartNumber_Assembly_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_VehicleManufacturerSparePartNumber_Assembly_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_VehicleManufacturerSparePartNumber_Assembly_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_VehicleManufacturerSparePartNumber_Assembly_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:VehicleManufacturerSparePartNumber_Assembly firt item[0] = %d",VehicleManufacturerSparePartNumber_Assembly[0]);
}

void GenEep_Set_VehicleManufacturerSparePartNumber_Assembly_Ex(uint8_t const* VehicleManufacturerSparePartNumber_Assembly,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_VehicleManufacturerSparePartNumber_Assembly_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_VehicleManufacturerSparePartNumber_Assembly_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.VehicleManufacturerSparePartNumber_Assembly;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != VehicleManufacturerSparePartNumber_Assembly[i] )
        {
            dest[offset+i] = VehicleManufacturerSparePartNumber_Assembly[i];
            wr_eep_by_list_req[EEP_CONTENT_VehicleManufacturerSparePartNumber_Assembly_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_VehicleManufacturerSparePartNumber_Assembly_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_VehicleManufacturerSparePartNumber_Assembly_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_VehicleManufacturerSparePartNumber_Assembly_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:VehicleManufacturerSparePartNumber_Assembly by Extend Api");
}

static void EepCopy_VehicleManufacturerSparePartNumber_Assembly_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_VehicleManufacturerSparePartNumber_Assembly_ITEM_LEN; i ++)
    {
        eep_hw_image.VehicleManufacturerSparePartNumber_Assembly[i] = eep_ram_image.VehicleManufacturerSparePartNumber_Assembly[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_VehicleManufacturerSparePartNumber_Assembly_IN_BLOCK_NUM);
}

#else 
uint8_t GenEep_Get_VehicleManufacturerSparePartNumber_Assembly(void)
{
    uint8_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.VehicleManufacturerSparePartNumber_Assembly;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_VehicleManufacturerSparePartNumber_Assembly(uint8_t const VehicleManufacturerSparePartNumber_Assembly)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.VehicleManufacturerSparePartNumber_Assembly != VehicleManufacturerSparePartNumber_Assembly )
    {
        eep_ram_image.VehicleManufacturerSparePartNumber_Assembly = VehicleManufacturerSparePartNumber_Assembly;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_VehicleManufacturerSparePartNumber_Assembly_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_VehicleManufacturerSparePartNumber_Assembly_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_VehicleManufacturerSparePartNumber_Assembly_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_VehicleManufacturerSparePartNumber_Assembly_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:VehicleManufacturerSparePartNumber_Assembly=%d",VehicleManufacturerSparePartNumber_Assembly);
}

static void EepCopy_VehicleManufacturerSparePartNumber_Assembly_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.VehicleManufacturerSparePartNumber_Assembly = eep_ram_image.VehicleManufacturerSparePartNumber_Assembly;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_VehicleManufacturerSparePartNumber_Assembly_IN_BLOCK_NUM);
}
#endif
//static void Proc_VehicleManufacturerSparePartNumber_Assembly_RestoreToDefault(void)
//{
//#if EEP_CONTENT_VehicleManufacturerSparePartNumber_Assembly_CONST_VARIBLE
//	GenEep_Set_VehicleManufacturerSparePartNumber_Assembly(dflt_eep_val.VehicleManufacturerSparePartNumber_Assembly);
//#endif	
//}
#if EEP_CONTENT_VehicleManufacturerSparePartNumber_Assembly_CONST_VARIBLE
#define Proc_VehicleManufacturerSparePartNumber_Assembly_RestoreToDefault()	do{GenEep_Set_VehicleManufacturerSparePartNumber_Assembly(dflt_eep_val.VehicleManufacturerSparePartNumber_Assembly);}while(0)
#else
#define Proc_VehicleManufacturerSparePartNumber_Assembly_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> VehicleManufacturerSparePartNumber_Assembly function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> Operational_Reference function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_Operational_Reference_ITEM_LEN > 1
void GenEep_Get_Operational_Reference(uint8_t * const Operational_Reference )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_Operational_Reference_ITEM_LEN; i ++)
    {
        Operational_Reference[i] = eep_ram_image.Operational_Reference[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_Operational_Reference_Ex(uint8_t * const Operational_Reference,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.Operational_Reference;	//EEP_CONTENT_Operational_Reference_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_Operational_Reference_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_Operational_Reference_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        Operational_Reference[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_Operational_Reference(uint8_t const* Operational_Reference )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_Operational_Reference_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.Operational_Reference[i] != Operational_Reference[i] )
        {
            eep_ram_image.Operational_Reference[i] = Operational_Reference[i];
            wr_eep_by_list_req[EEP_CONTENT_Operational_Reference_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_Operational_Reference_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_Operational_Reference_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_Operational_Reference_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:Operational_Reference firt item[0] = %d",Operational_Reference[0]);
}

void GenEep_Set_Operational_Reference_Ex(uint8_t const* Operational_Reference,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_Operational_Reference_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_Operational_Reference_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.Operational_Reference;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != Operational_Reference[i] )
        {
            dest[offset+i] = Operational_Reference[i];
            wr_eep_by_list_req[EEP_CONTENT_Operational_Reference_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_Operational_Reference_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_Operational_Reference_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_Operational_Reference_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:Operational_Reference by Extend Api");
}

static void EepCopy_Operational_Reference_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_Operational_Reference_ITEM_LEN; i ++)
    {
        eep_hw_image.Operational_Reference[i] = eep_ram_image.Operational_Reference[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_Operational_Reference_IN_BLOCK_NUM);
}

#else 
uint8_t GenEep_Get_Operational_Reference(void)
{
    uint8_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.Operational_Reference;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_Operational_Reference(uint8_t const Operational_Reference)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.Operational_Reference != Operational_Reference )
    {
        eep_ram_image.Operational_Reference = Operational_Reference;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_Operational_Reference_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_Operational_Reference_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_Operational_Reference_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_Operational_Reference_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:Operational_Reference=%d",Operational_Reference);
}

static void EepCopy_Operational_Reference_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.Operational_Reference = eep_ram_image.Operational_Reference;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_Operational_Reference_IN_BLOCK_NUM);
}
#endif
//static void Proc_Operational_Reference_RestoreToDefault(void)
//{
//#if EEP_CONTENT_Operational_Reference_CONST_VARIBLE
//	GenEep_Set_Operational_Reference(dflt_eep_val.Operational_Reference);
//#endif	
//}
#if EEP_CONTENT_Operational_Reference_CONST_VARIBLE
#define Proc_Operational_Reference_RestoreToDefault()	do{GenEep_Set_Operational_Reference(dflt_eep_val.Operational_Reference);}while(0)
#else
#define Proc_Operational_Reference_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> Operational_Reference function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> Supplier_Number function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_Supplier_Number_ITEM_LEN > 1
void GenEep_Get_Supplier_Number(uint8_t * const Supplier_Number )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_Supplier_Number_ITEM_LEN; i ++)
    {
        Supplier_Number[i] = eep_ram_image.Supplier_Number[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_Supplier_Number_Ex(uint8_t * const Supplier_Number,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.Supplier_Number;	//EEP_CONTENT_Supplier_Number_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_Supplier_Number_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_Supplier_Number_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        Supplier_Number[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_Supplier_Number(uint8_t const* Supplier_Number )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_Supplier_Number_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.Supplier_Number[i] != Supplier_Number[i] )
        {
            eep_ram_image.Supplier_Number[i] = Supplier_Number[i];
            wr_eep_by_list_req[EEP_CONTENT_Supplier_Number_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_Supplier_Number_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_Supplier_Number_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_Supplier_Number_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:Supplier_Number firt item[0] = %d",Supplier_Number[0]);
}

void GenEep_Set_Supplier_Number_Ex(uint8_t const* Supplier_Number,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_Supplier_Number_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_Supplier_Number_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.Supplier_Number;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != Supplier_Number[i] )
        {
            dest[offset+i] = Supplier_Number[i];
            wr_eep_by_list_req[EEP_CONTENT_Supplier_Number_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_Supplier_Number_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_Supplier_Number_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_Supplier_Number_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:Supplier_Number by Extend Api");
}

static void EepCopy_Supplier_Number_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_Supplier_Number_ITEM_LEN; i ++)
    {
        eep_hw_image.Supplier_Number[i] = eep_ram_image.Supplier_Number[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_Supplier_Number_IN_BLOCK_NUM);
}

#else 
uint8_t GenEep_Get_Supplier_Number(void)
{
    uint8_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.Supplier_Number;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_Supplier_Number(uint8_t const Supplier_Number)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.Supplier_Number != Supplier_Number )
    {
        eep_ram_image.Supplier_Number = Supplier_Number;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_Supplier_Number_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_Supplier_Number_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_Supplier_Number_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_Supplier_Number_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:Supplier_Number=%d",Supplier_Number);
}

static void EepCopy_Supplier_Number_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.Supplier_Number = eep_ram_image.Supplier_Number;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_Supplier_Number_IN_BLOCK_NUM);
}
#endif
//static void Proc_Supplier_Number_RestoreToDefault(void)
//{
//#if EEP_CONTENT_Supplier_Number_CONST_VARIBLE
//	GenEep_Set_Supplier_Number(dflt_eep_val.Supplier_Number);
//#endif	
//}
#if EEP_CONTENT_Supplier_Number_CONST_VARIBLE
#define Proc_Supplier_Number_RestoreToDefault()	do{GenEep_Set_Supplier_Number(dflt_eep_val.Supplier_Number);}while(0)
#else
#define Proc_Supplier_Number_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> Supplier_Number function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> ECU_Serial_Number function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_ECU_Serial_Number_ITEM_LEN > 1
void GenEep_Get_ECU_Serial_Number(uint8_t * const ECU_Serial_Number )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_ECU_Serial_Number_ITEM_LEN; i ++)
    {
        ECU_Serial_Number[i] = eep_ram_image.ECU_Serial_Number[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_ECU_Serial_Number_Ex(uint8_t * const ECU_Serial_Number,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.ECU_Serial_Number;	//EEP_CONTENT_ECU_Serial_Number_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_ECU_Serial_Number_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_ECU_Serial_Number_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        ECU_Serial_Number[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_ECU_Serial_Number(uint8_t const* ECU_Serial_Number )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_ECU_Serial_Number_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.ECU_Serial_Number[i] != ECU_Serial_Number[i] )
        {
            eep_ram_image.ECU_Serial_Number[i] = ECU_Serial_Number[i];
            wr_eep_by_list_req[EEP_CONTENT_ECU_Serial_Number_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_ECU_Serial_Number_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_ECU_Serial_Number_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_ECU_Serial_Number_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:ECU_Serial_Number firt item[0] = %d",ECU_Serial_Number[0]);
}

void GenEep_Set_ECU_Serial_Number_Ex(uint8_t const* ECU_Serial_Number,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_ECU_Serial_Number_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_ECU_Serial_Number_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.ECU_Serial_Number;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != ECU_Serial_Number[i] )
        {
            dest[offset+i] = ECU_Serial_Number[i];
            wr_eep_by_list_req[EEP_CONTENT_ECU_Serial_Number_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_ECU_Serial_Number_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_ECU_Serial_Number_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_ECU_Serial_Number_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:ECU_Serial_Number by Extend Api");
}

static void EepCopy_ECU_Serial_Number_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_ECU_Serial_Number_ITEM_LEN; i ++)
    {
        eep_hw_image.ECU_Serial_Number[i] = eep_ram_image.ECU_Serial_Number[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_ECU_Serial_Number_IN_BLOCK_NUM);
}

#else 
uint8_t GenEep_Get_ECU_Serial_Number(void)
{
    uint8_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.ECU_Serial_Number;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_ECU_Serial_Number(uint8_t const ECU_Serial_Number)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.ECU_Serial_Number != ECU_Serial_Number )
    {
        eep_ram_image.ECU_Serial_Number = ECU_Serial_Number;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_ECU_Serial_Number_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_ECU_Serial_Number_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_ECU_Serial_Number_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_ECU_Serial_Number_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:ECU_Serial_Number=%d",ECU_Serial_Number);
}

static void EepCopy_ECU_Serial_Number_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.ECU_Serial_Number = eep_ram_image.ECU_Serial_Number;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_ECU_Serial_Number_IN_BLOCK_NUM);
}
#endif
//static void Proc_ECU_Serial_Number_RestoreToDefault(void)
//{
//#if EEP_CONTENT_ECU_Serial_Number_CONST_VARIBLE
//	GenEep_Set_ECU_Serial_Number(dflt_eep_val.ECU_Serial_Number);
//#endif	
//}
#if EEP_CONTENT_ECU_Serial_Number_CONST_VARIBLE
#define Proc_ECU_Serial_Number_RestoreToDefault()	do{GenEep_Set_ECU_Serial_Number(dflt_eep_val.ECU_Serial_Number);}while(0)
#else
#define Proc_ECU_Serial_Number_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> ECU_Serial_Number function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> Manufacturing_Identification_Code function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_Manufacturing_Identification_Code_ITEM_LEN > 1
void GenEep_Get_Manufacturing_Identification_Code(uint8_t * const Manufacturing_Identification_Code )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_Manufacturing_Identification_Code_ITEM_LEN; i ++)
    {
        Manufacturing_Identification_Code[i] = eep_ram_image.Manufacturing_Identification_Code[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_Manufacturing_Identification_Code_Ex(uint8_t * const Manufacturing_Identification_Code,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.Manufacturing_Identification_Code;	//EEP_CONTENT_Manufacturing_Identification_Code_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_Manufacturing_Identification_Code_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_Manufacturing_Identification_Code_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        Manufacturing_Identification_Code[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_Manufacturing_Identification_Code(uint8_t const* Manufacturing_Identification_Code )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_Manufacturing_Identification_Code_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.Manufacturing_Identification_Code[i] != Manufacturing_Identification_Code[i] )
        {
            eep_ram_image.Manufacturing_Identification_Code[i] = Manufacturing_Identification_Code[i];
            wr_eep_by_list_req[EEP_CONTENT_Manufacturing_Identification_Code_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_Manufacturing_Identification_Code_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_Manufacturing_Identification_Code_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_Manufacturing_Identification_Code_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:Manufacturing_Identification_Code firt item[0] = %d",Manufacturing_Identification_Code[0]);
}

void GenEep_Set_Manufacturing_Identification_Code_Ex(uint8_t const* Manufacturing_Identification_Code,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_Manufacturing_Identification_Code_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_Manufacturing_Identification_Code_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.Manufacturing_Identification_Code;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != Manufacturing_Identification_Code[i] )
        {
            dest[offset+i] = Manufacturing_Identification_Code[i];
            wr_eep_by_list_req[EEP_CONTENT_Manufacturing_Identification_Code_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_Manufacturing_Identification_Code_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_Manufacturing_Identification_Code_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_Manufacturing_Identification_Code_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:Manufacturing_Identification_Code by Extend Api");
}

static void EepCopy_Manufacturing_Identification_Code_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_Manufacturing_Identification_Code_ITEM_LEN; i ++)
    {
        eep_hw_image.Manufacturing_Identification_Code[i] = eep_ram_image.Manufacturing_Identification_Code[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_Manufacturing_Identification_Code_IN_BLOCK_NUM);
}

#else 
uint8_t GenEep_Get_Manufacturing_Identification_Code(void)
{
    uint8_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.Manufacturing_Identification_Code;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_Manufacturing_Identification_Code(uint8_t const Manufacturing_Identification_Code)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.Manufacturing_Identification_Code != Manufacturing_Identification_Code )
    {
        eep_ram_image.Manufacturing_Identification_Code = Manufacturing_Identification_Code;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_Manufacturing_Identification_Code_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_Manufacturing_Identification_Code_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_Manufacturing_Identification_Code_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_Manufacturing_Identification_Code_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:Manufacturing_Identification_Code=%d",Manufacturing_Identification_Code);
}

static void EepCopy_Manufacturing_Identification_Code_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.Manufacturing_Identification_Code = eep_ram_image.Manufacturing_Identification_Code;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_Manufacturing_Identification_Code_IN_BLOCK_NUM);
}
#endif
//static void Proc_Manufacturing_Identification_Code_RestoreToDefault(void)
//{
//#if EEP_CONTENT_Manufacturing_Identification_Code_CONST_VARIBLE
//	GenEep_Set_Manufacturing_Identification_Code(dflt_eep_val.Manufacturing_Identification_Code);
//#endif	
//}
#if EEP_CONTENT_Manufacturing_Identification_Code_CONST_VARIBLE
#define Proc_Manufacturing_Identification_Code_RestoreToDefault()	do{GenEep_Set_Manufacturing_Identification_Code(dflt_eep_val.Manufacturing_Identification_Code);}while(0)
#else
#define Proc_Manufacturing_Identification_Code_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> Manufacturing_Identification_Code function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> VDIAG function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_VDIAG_ITEM_LEN > 1
void GenEep_Get_VDIAG(uint8_t * const VDIAG )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_VDIAG_ITEM_LEN; i ++)
    {
        VDIAG[i] = eep_ram_image.VDIAG[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_VDIAG_Ex(uint8_t * const VDIAG,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.VDIAG;	//EEP_CONTENT_VDIAG_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_VDIAG_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_VDIAG_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        VDIAG[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_VDIAG(uint8_t const* VDIAG )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_VDIAG_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.VDIAG[i] != VDIAG[i] )
        {
            eep_ram_image.VDIAG[i] = VDIAG[i];
            wr_eep_by_list_req[EEP_CONTENT_VDIAG_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_VDIAG_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_VDIAG_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_VDIAG_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:VDIAG firt item[0] = %d",VDIAG[0]);
}

void GenEep_Set_VDIAG_Ex(uint8_t const* VDIAG,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_VDIAG_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_VDIAG_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.VDIAG;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != VDIAG[i] )
        {
            dest[offset+i] = VDIAG[i];
            wr_eep_by_list_req[EEP_CONTENT_VDIAG_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_VDIAG_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_VDIAG_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_VDIAG_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:VDIAG by Extend Api");
}

static void EepCopy_VDIAG_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_VDIAG_ITEM_LEN; i ++)
    {
        eep_hw_image.VDIAG[i] = eep_ram_image.VDIAG[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_VDIAG_IN_BLOCK_NUM);
}

#else 
uint8_t GenEep_Get_VDIAG(void)
{
    uint8_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.VDIAG;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_VDIAG(uint8_t const VDIAG)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.VDIAG != VDIAG )
    {
        eep_ram_image.VDIAG = VDIAG;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_VDIAG_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_VDIAG_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_VDIAG_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_VDIAG_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:VDIAG=%d",VDIAG);
}

static void EepCopy_VDIAG_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.VDIAG = eep_ram_image.VDIAG;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_VDIAG_IN_BLOCK_NUM);
}
#endif
//static void Proc_VDIAG_RestoreToDefault(void)
//{
//#if EEP_CONTENT_VDIAG_CONST_VARIBLE
//	GenEep_Set_VDIAG(dflt_eep_val.VDIAG);
//#endif	
//}
#if EEP_CONTENT_VDIAG_CONST_VARIBLE
#define Proc_VDIAG_RestoreToDefault()	do{GenEep_Set_VDIAG(dflt_eep_val.VDIAG);}while(0)
#else
#define Proc_VDIAG_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> VDIAG function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> Config_EQ1 function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_Config_EQ1_ITEM_LEN > 1
void GenEep_Get_Config_EQ1(uint8_t * const Config_EQ1 )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_Config_EQ1_ITEM_LEN; i ++)
    {
        Config_EQ1[i] = eep_ram_image.Config_EQ1[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_Config_EQ1_Ex(uint8_t * const Config_EQ1,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.Config_EQ1;	//EEP_CONTENT_Config_EQ1_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_Config_EQ1_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_Config_EQ1_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        Config_EQ1[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_Config_EQ1(uint8_t const* Config_EQ1 )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_Config_EQ1_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.Config_EQ1[i] != Config_EQ1[i] )
        {
            eep_ram_image.Config_EQ1[i] = Config_EQ1[i];
            wr_eep_by_list_req[EEP_CONTENT_Config_EQ1_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_Config_EQ1_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_Config_EQ1_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_Config_EQ1_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:Config_EQ1 firt item[0] = %d",Config_EQ1[0]);
}

void GenEep_Set_Config_EQ1_Ex(uint8_t const* Config_EQ1,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_Config_EQ1_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_Config_EQ1_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.Config_EQ1;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != Config_EQ1[i] )
        {
            dest[offset+i] = Config_EQ1[i];
            wr_eep_by_list_req[EEP_CONTENT_Config_EQ1_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_Config_EQ1_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_Config_EQ1_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_Config_EQ1_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:Config_EQ1 by Extend Api");
}

static void EepCopy_Config_EQ1_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_Config_EQ1_ITEM_LEN; i ++)
    {
        eep_hw_image.Config_EQ1[i] = eep_ram_image.Config_EQ1[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_Config_EQ1_IN_BLOCK_NUM);
}

#else 
uint8_t GenEep_Get_Config_EQ1(void)
{
    uint8_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.Config_EQ1;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_Config_EQ1(uint8_t const Config_EQ1)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.Config_EQ1 != Config_EQ1 )
    {
        eep_ram_image.Config_EQ1 = Config_EQ1;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_Config_EQ1_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_Config_EQ1_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_Config_EQ1_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_Config_EQ1_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:Config_EQ1=%d",Config_EQ1);
}

static void EepCopy_Config_EQ1_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.Config_EQ1 = eep_ram_image.Config_EQ1;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_Config_EQ1_IN_BLOCK_NUM);
}
#endif
//static void Proc_Config_EQ1_RestoreToDefault(void)
//{
//#if EEP_CONTENT_Config_EQ1_CONST_VARIBLE
//	GenEep_Set_Config_EQ1(dflt_eep_val.Config_EQ1);
//#endif	
//}
#if EEP_CONTENT_Config_EQ1_CONST_VARIBLE
#define Proc_Config_EQ1_RestoreToDefault()	do{GenEep_Set_Config_EQ1(dflt_eep_val.Config_EQ1);}while(0)
#else
#define Proc_Config_EQ1_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> Config_EQ1 function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> Vehicle_Type function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_Vehicle_Type_ITEM_LEN > 1
void GenEep_Get_Vehicle_Type(uint8_t * const Vehicle_Type )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_Vehicle_Type_ITEM_LEN; i ++)
    {
        Vehicle_Type[i] = eep_ram_image.Vehicle_Type[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_Vehicle_Type_Ex(uint8_t * const Vehicle_Type,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.Vehicle_Type;	//EEP_CONTENT_Vehicle_Type_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_Vehicle_Type_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_Vehicle_Type_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        Vehicle_Type[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_Vehicle_Type(uint8_t const* Vehicle_Type )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_Vehicle_Type_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.Vehicle_Type[i] != Vehicle_Type[i] )
        {
            eep_ram_image.Vehicle_Type[i] = Vehicle_Type[i];
            wr_eep_by_list_req[EEP_CONTENT_Vehicle_Type_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_Vehicle_Type_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_Vehicle_Type_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_Vehicle_Type_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:Vehicle_Type firt item[0] = %d",Vehicle_Type[0]);
}

void GenEep_Set_Vehicle_Type_Ex(uint8_t const* Vehicle_Type,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_Vehicle_Type_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_Vehicle_Type_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.Vehicle_Type;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != Vehicle_Type[i] )
        {
            dest[offset+i] = Vehicle_Type[i];
            wr_eep_by_list_req[EEP_CONTENT_Vehicle_Type_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_Vehicle_Type_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_Vehicle_Type_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_Vehicle_Type_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:Vehicle_Type by Extend Api");
}

static void EepCopy_Vehicle_Type_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_Vehicle_Type_ITEM_LEN; i ++)
    {
        eep_hw_image.Vehicle_Type[i] = eep_ram_image.Vehicle_Type[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_Vehicle_Type_IN_BLOCK_NUM);
}

#else 
uint8_t GenEep_Get_Vehicle_Type(void)
{
    uint8_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.Vehicle_Type;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_Vehicle_Type(uint8_t const Vehicle_Type)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.Vehicle_Type != Vehicle_Type )
    {
        eep_ram_image.Vehicle_Type = Vehicle_Type;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_Vehicle_Type_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_Vehicle_Type_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_Vehicle_Type_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_Vehicle_Type_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:Vehicle_Type=%d",Vehicle_Type);
}

static void EepCopy_Vehicle_Type_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.Vehicle_Type = eep_ram_image.Vehicle_Type;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_Vehicle_Type_IN_BLOCK_NUM);
}
#endif
//static void Proc_Vehicle_Type_RestoreToDefault(void)
//{
//#if EEP_CONTENT_Vehicle_Type_CONST_VARIBLE
//	GenEep_Set_Vehicle_Type(dflt_eep_val.Vehicle_Type);
//#endif	
//}
#if EEP_CONTENT_Vehicle_Type_CONST_VARIBLE
#define Proc_Vehicle_Type_RestoreToDefault()	do{GenEep_Set_Vehicle_Type(dflt_eep_val.Vehicle_Type);}while(0)
#else
#define Proc_Vehicle_Type_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> Vehicle_Type function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> UUID function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_UUID_ITEM_LEN > 1
void GenEep_Get_UUID(uint8_t * const UUID )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_UUID_ITEM_LEN; i ++)
    {
        UUID[i] = eep_ram_image.UUID[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_UUID_Ex(uint8_t * const UUID,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.UUID;	//EEP_CONTENT_UUID_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_UUID_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_UUID_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        UUID[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_UUID(uint8_t const* UUID )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_UUID_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.UUID[i] != UUID[i] )
        {
            eep_ram_image.UUID[i] = UUID[i];
            wr_eep_by_list_req[EEP_CONTENT_UUID_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_UUID_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_UUID_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_UUID_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:UUID firt item[0] = %d",UUID[0]);
}

void GenEep_Set_UUID_Ex(uint8_t const* UUID,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_UUID_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_UUID_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.UUID;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != UUID[i] )
        {
            dest[offset+i] = UUID[i];
            wr_eep_by_list_req[EEP_CONTENT_UUID_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_UUID_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_UUID_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_UUID_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:UUID by Extend Api");
}

static void EepCopy_UUID_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_UUID_ITEM_LEN; i ++)
    {
        eep_hw_image.UUID[i] = eep_ram_image.UUID[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_UUID_IN_BLOCK_NUM);
}

#else 
uint8_t GenEep_Get_UUID(void)
{
    uint8_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.UUID;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_UUID(uint8_t const UUID)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.UUID != UUID )
    {
        eep_ram_image.UUID = UUID;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_UUID_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_UUID_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_UUID_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_UUID_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:UUID=%d",UUID);
}

static void EepCopy_UUID_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.UUID = eep_ram_image.UUID;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_UUID_IN_BLOCK_NUM);
}
#endif
//static void Proc_UUID_RestoreToDefault(void)
//{
//#if EEP_CONTENT_UUID_CONST_VARIBLE
//	GenEep_Set_UUID(dflt_eep_val.UUID);
//#endif	
//}
#if EEP_CONTENT_UUID_CONST_VARIBLE
#define Proc_UUID_RestoreToDefault()	do{GenEep_Set_UUID(dflt_eep_val.UUID);}while(0)
#else
#define Proc_UUID_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> UUID function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> NAVI_ID function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_NAVI_ID_ITEM_LEN > 1
void GenEep_Get_NAVI_ID(uint8_t * const NAVI_ID )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_NAVI_ID_ITEM_LEN; i ++)
    {
        NAVI_ID[i] = eep_ram_image.NAVI_ID[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_NAVI_ID_Ex(uint8_t * const NAVI_ID,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.NAVI_ID;	//EEP_CONTENT_NAVI_ID_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_NAVI_ID_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_NAVI_ID_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        NAVI_ID[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_NAVI_ID(uint8_t const* NAVI_ID )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_NAVI_ID_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.NAVI_ID[i] != NAVI_ID[i] )
        {
            eep_ram_image.NAVI_ID[i] = NAVI_ID[i];
            wr_eep_by_list_req[EEP_CONTENT_NAVI_ID_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_NAVI_ID_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_NAVI_ID_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_NAVI_ID_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:NAVI_ID firt item[0] = %d",NAVI_ID[0]);
}

void GenEep_Set_NAVI_ID_Ex(uint8_t const* NAVI_ID,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_NAVI_ID_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_NAVI_ID_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.NAVI_ID;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != NAVI_ID[i] )
        {
            dest[offset+i] = NAVI_ID[i];
            wr_eep_by_list_req[EEP_CONTENT_NAVI_ID_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_NAVI_ID_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_NAVI_ID_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_NAVI_ID_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:NAVI_ID by Extend Api");
}

static void EepCopy_NAVI_ID_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_NAVI_ID_ITEM_LEN; i ++)
    {
        eep_hw_image.NAVI_ID[i] = eep_ram_image.NAVI_ID[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_NAVI_ID_IN_BLOCK_NUM);
}

#else 
uint8_t GenEep_Get_NAVI_ID(void)
{
    uint8_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.NAVI_ID;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_NAVI_ID(uint8_t const NAVI_ID)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.NAVI_ID != NAVI_ID )
    {
        eep_ram_image.NAVI_ID = NAVI_ID;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_NAVI_ID_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_NAVI_ID_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_NAVI_ID_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_NAVI_ID_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:NAVI_ID=%d",NAVI_ID);
}

static void EepCopy_NAVI_ID_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.NAVI_ID = eep_ram_image.NAVI_ID;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_NAVI_ID_IN_BLOCK_NUM);
}
#endif
//static void Proc_NAVI_ID_RestoreToDefault(void)
//{
//#if EEP_CONTENT_NAVI_ID_CONST_VARIBLE
//	GenEep_Set_NAVI_ID(dflt_eep_val.NAVI_ID);
//#endif	
//}
#if EEP_CONTENT_NAVI_ID_CONST_VARIBLE
#define Proc_NAVI_ID_RestoreToDefault()	do{GenEep_Set_NAVI_ID(dflt_eep_val.NAVI_ID);}while(0)
#else
#define Proc_NAVI_ID_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> NAVI_ID function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> DA_ID function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_DA_ID_ITEM_LEN > 1
void GenEep_Get_DA_ID(uint8_t * const DA_ID )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_DA_ID_ITEM_LEN; i ++)
    {
        DA_ID[i] = eep_ram_image.DA_ID[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_DA_ID_Ex(uint8_t * const DA_ID,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.DA_ID;	//EEP_CONTENT_DA_ID_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_DA_ID_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_DA_ID_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        DA_ID[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_DA_ID(uint8_t const* DA_ID )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_DA_ID_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.DA_ID[i] != DA_ID[i] )
        {
            eep_ram_image.DA_ID[i] = DA_ID[i];
            wr_eep_by_list_req[EEP_CONTENT_DA_ID_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_DA_ID_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_DA_ID_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_DA_ID_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:DA_ID firt item[0] = %d",DA_ID[0]);
}

void GenEep_Set_DA_ID_Ex(uint8_t const* DA_ID,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_DA_ID_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_DA_ID_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.DA_ID;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != DA_ID[i] )
        {
            dest[offset+i] = DA_ID[i];
            wr_eep_by_list_req[EEP_CONTENT_DA_ID_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_DA_ID_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_DA_ID_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_DA_ID_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:DA_ID by Extend Api");
}

static void EepCopy_DA_ID_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_DA_ID_ITEM_LEN; i ++)
    {
        eep_hw_image.DA_ID[i] = eep_ram_image.DA_ID[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_DA_ID_IN_BLOCK_NUM);
}

#else 
uint8_t GenEep_Get_DA_ID(void)
{
    uint8_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.DA_ID;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_DA_ID(uint8_t const DA_ID)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.DA_ID != DA_ID )
    {
        eep_ram_image.DA_ID = DA_ID;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_DA_ID_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_DA_ID_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_DA_ID_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_DA_ID_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:DA_ID=%d",DA_ID);
}

static void EepCopy_DA_ID_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.DA_ID = eep_ram_image.DA_ID;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_DA_ID_IN_BLOCK_NUM);
}
#endif
//static void Proc_DA_ID_RestoreToDefault(void)
//{
//#if EEP_CONTENT_DA_ID_CONST_VARIBLE
//	GenEep_Set_DA_ID(dflt_eep_val.DA_ID);
//#endif	
//}
#if EEP_CONTENT_DA_ID_CONST_VARIBLE
#define Proc_DA_ID_RestoreToDefault()	do{GenEep_Set_DA_ID(dflt_eep_val.DA_ID);}while(0)
#else
#define Proc_DA_ID_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> DA_ID function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> DAMainBoardHwVer function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_DAMainBoardHwVer_ITEM_LEN > 1
void GenEep_Get_DAMainBoardHwVer(uint8_t * const DAMainBoardHwVer )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_DAMainBoardHwVer_ITEM_LEN; i ++)
    {
        DAMainBoardHwVer[i] = eep_ram_image.DAMainBoardHwVer[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_DAMainBoardHwVer_Ex(uint8_t * const DAMainBoardHwVer,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.DAMainBoardHwVer;	//EEP_CONTENT_DAMainBoardHwVer_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_DAMainBoardHwVer_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_DAMainBoardHwVer_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        DAMainBoardHwVer[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_DAMainBoardHwVer(uint8_t const* DAMainBoardHwVer )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_DAMainBoardHwVer_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.DAMainBoardHwVer[i] != DAMainBoardHwVer[i] )
        {
            eep_ram_image.DAMainBoardHwVer[i] = DAMainBoardHwVer[i];
            wr_eep_by_list_req[EEP_CONTENT_DAMainBoardHwVer_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_DAMainBoardHwVer_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_DAMainBoardHwVer_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_DAMainBoardHwVer_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:DAMainBoardHwVer firt item[0] = %d",DAMainBoardHwVer[0]);
}

void GenEep_Set_DAMainBoardHwVer_Ex(uint8_t const* DAMainBoardHwVer,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_DAMainBoardHwVer_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_DAMainBoardHwVer_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.DAMainBoardHwVer;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != DAMainBoardHwVer[i] )
        {
            dest[offset+i] = DAMainBoardHwVer[i];
            wr_eep_by_list_req[EEP_CONTENT_DAMainBoardHwVer_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_DAMainBoardHwVer_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_DAMainBoardHwVer_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_DAMainBoardHwVer_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:DAMainBoardHwVer by Extend Api");
}

static void EepCopy_DAMainBoardHwVer_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_DAMainBoardHwVer_ITEM_LEN; i ++)
    {
        eep_hw_image.DAMainBoardHwVer[i] = eep_ram_image.DAMainBoardHwVer[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_DAMainBoardHwVer_IN_BLOCK_NUM);
}

#else 
uint8_t GenEep_Get_DAMainBoardHwVer(void)
{
    uint8_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.DAMainBoardHwVer;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_DAMainBoardHwVer(uint8_t const DAMainBoardHwVer)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.DAMainBoardHwVer != DAMainBoardHwVer )
    {
        eep_ram_image.DAMainBoardHwVer = DAMainBoardHwVer;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_DAMainBoardHwVer_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_DAMainBoardHwVer_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_DAMainBoardHwVer_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_DAMainBoardHwVer_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:DAMainBoardHwVer=%d",DAMainBoardHwVer);
}

static void EepCopy_DAMainBoardHwVer_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.DAMainBoardHwVer = eep_ram_image.DAMainBoardHwVer;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_DAMainBoardHwVer_IN_BLOCK_NUM);
}
#endif
//static void Proc_DAMainBoardHwVer_RestoreToDefault(void)
//{
//#if EEP_CONTENT_DAMainBoardHwVer_CONST_VARIBLE
//	GenEep_Set_DAMainBoardHwVer(dflt_eep_val.DAMainBoardHwVer);
//#endif	
//}
#if EEP_CONTENT_DAMainBoardHwVer_CONST_VARIBLE
#define Proc_DAMainBoardHwVer_RestoreToDefault()	do{GenEep_Set_DAMainBoardHwVer(dflt_eep_val.DAMainBoardHwVer);}while(0)
#else
#define Proc_DAMainBoardHwVer_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> DAMainBoardHwVer function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> SW_Version_Date_Format function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_SW_Version_Date_Format_ITEM_LEN > 1
void GenEep_Get_SW_Version_Date_Format(uint8_t * const SW_Version_Date_Format )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_SW_Version_Date_Format_ITEM_LEN; i ++)
    {
        SW_Version_Date_Format[i] = eep_ram_image.SW_Version_Date_Format[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_SW_Version_Date_Format_Ex(uint8_t * const SW_Version_Date_Format,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.SW_Version_Date_Format;	//EEP_CONTENT_SW_Version_Date_Format_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_SW_Version_Date_Format_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_SW_Version_Date_Format_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        SW_Version_Date_Format[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_SW_Version_Date_Format(uint8_t const* SW_Version_Date_Format )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_SW_Version_Date_Format_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.SW_Version_Date_Format[i] != SW_Version_Date_Format[i] )
        {
            eep_ram_image.SW_Version_Date_Format[i] = SW_Version_Date_Format[i];
            wr_eep_by_list_req[EEP_CONTENT_SW_Version_Date_Format_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_SW_Version_Date_Format_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_SW_Version_Date_Format_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_SW_Version_Date_Format_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:SW_Version_Date_Format firt item[0] = %d",SW_Version_Date_Format[0]);
}

void GenEep_Set_SW_Version_Date_Format_Ex(uint8_t const* SW_Version_Date_Format,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_SW_Version_Date_Format_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_SW_Version_Date_Format_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.SW_Version_Date_Format;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != SW_Version_Date_Format[i] )
        {
            dest[offset+i] = SW_Version_Date_Format[i];
            wr_eep_by_list_req[EEP_CONTENT_SW_Version_Date_Format_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_SW_Version_Date_Format_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_SW_Version_Date_Format_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_SW_Version_Date_Format_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:SW_Version_Date_Format by Extend Api");
}

static void EepCopy_SW_Version_Date_Format_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_SW_Version_Date_Format_ITEM_LEN; i ++)
    {
        eep_hw_image.SW_Version_Date_Format[i] = eep_ram_image.SW_Version_Date_Format[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_SW_Version_Date_Format_IN_BLOCK_NUM);
}

#else 
uint8_t GenEep_Get_SW_Version_Date_Format(void)
{
    uint8_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.SW_Version_Date_Format;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_SW_Version_Date_Format(uint8_t const SW_Version_Date_Format)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.SW_Version_Date_Format != SW_Version_Date_Format )
    {
        eep_ram_image.SW_Version_Date_Format = SW_Version_Date_Format;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_SW_Version_Date_Format_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_SW_Version_Date_Format_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_SW_Version_Date_Format_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_SW_Version_Date_Format_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:SW_Version_Date_Format=%d",SW_Version_Date_Format);
}

static void EepCopy_SW_Version_Date_Format_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.SW_Version_Date_Format = eep_ram_image.SW_Version_Date_Format;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_SW_Version_Date_Format_IN_BLOCK_NUM);
}
#endif
//static void Proc_SW_Version_Date_Format_RestoreToDefault(void)
//{
//#if EEP_CONTENT_SW_Version_Date_Format_CONST_VARIBLE
//	GenEep_Set_SW_Version_Date_Format(dflt_eep_val.SW_Version_Date_Format);
//#endif	
//}
#if EEP_CONTENT_SW_Version_Date_Format_CONST_VARIBLE
#define Proc_SW_Version_Date_Format_RestoreToDefault()	do{GenEep_Set_SW_Version_Date_Format(dflt_eep_val.SW_Version_Date_Format);}while(0)
#else
#define Proc_SW_Version_Date_Format_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> SW_Version_Date_Format function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> SW_Edition_Version function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_SW_Edition_Version_ITEM_LEN > 1
void GenEep_Get_SW_Edition_Version(uint8_t * const SW_Edition_Version )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_SW_Edition_Version_ITEM_LEN; i ++)
    {
        SW_Edition_Version[i] = eep_ram_image.SW_Edition_Version[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_SW_Edition_Version_Ex(uint8_t * const SW_Edition_Version,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.SW_Edition_Version;	//EEP_CONTENT_SW_Edition_Version_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_SW_Edition_Version_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_SW_Edition_Version_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        SW_Edition_Version[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_SW_Edition_Version(uint8_t const* SW_Edition_Version )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_SW_Edition_Version_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.SW_Edition_Version[i] != SW_Edition_Version[i] )
        {
            eep_ram_image.SW_Edition_Version[i] = SW_Edition_Version[i];
            wr_eep_by_list_req[EEP_CONTENT_SW_Edition_Version_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_SW_Edition_Version_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_SW_Edition_Version_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_SW_Edition_Version_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:SW_Edition_Version firt item[0] = %d",SW_Edition_Version[0]);
}

void GenEep_Set_SW_Edition_Version_Ex(uint8_t const* SW_Edition_Version,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_SW_Edition_Version_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_SW_Edition_Version_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.SW_Edition_Version;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != SW_Edition_Version[i] )
        {
            dest[offset+i] = SW_Edition_Version[i];
            wr_eep_by_list_req[EEP_CONTENT_SW_Edition_Version_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_SW_Edition_Version_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_SW_Edition_Version_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_SW_Edition_Version_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:SW_Edition_Version by Extend Api");
}

static void EepCopy_SW_Edition_Version_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_SW_Edition_Version_ITEM_LEN; i ++)
    {
        eep_hw_image.SW_Edition_Version[i] = eep_ram_image.SW_Edition_Version[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_SW_Edition_Version_IN_BLOCK_NUM);
}

#else 
uint8_t GenEep_Get_SW_Edition_Version(void)
{
    uint8_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.SW_Edition_Version;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_SW_Edition_Version(uint8_t const SW_Edition_Version)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.SW_Edition_Version != SW_Edition_Version )
    {
        eep_ram_image.SW_Edition_Version = SW_Edition_Version;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_SW_Edition_Version_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_SW_Edition_Version_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_SW_Edition_Version_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_SW_Edition_Version_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:SW_Edition_Version=%d",SW_Edition_Version);
}

static void EepCopy_SW_Edition_Version_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.SW_Edition_Version = eep_ram_image.SW_Edition_Version;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_SW_Edition_Version_IN_BLOCK_NUM);
}
#endif
//static void Proc_SW_Edition_Version_RestoreToDefault(void)
//{
//#if EEP_CONTENT_SW_Edition_Version_CONST_VARIBLE
//	GenEep_Set_SW_Edition_Version(dflt_eep_val.SW_Edition_Version);
//#endif	
//}
#if EEP_CONTENT_SW_Edition_Version_CONST_VARIBLE
#define Proc_SW_Edition_Version_RestoreToDefault()	do{GenEep_Set_SW_Edition_Version(dflt_eep_val.SW_Edition_Version);}while(0)
#else
#define Proc_SW_Edition_Version_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> SW_Edition_Version function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> VehicleManufacturerSparePartNumber_Nissan function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_VehicleManufacturerSparePartNumber_Nissan_ITEM_LEN > 1
void GenEep_Get_VehicleManufacturerSparePartNumber_Nissan(uint8_t * const VehicleManufacturerSparePartNumber_Nissan )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_VehicleManufacturerSparePartNumber_Nissan_ITEM_LEN; i ++)
    {
        VehicleManufacturerSparePartNumber_Nissan[i] = eep_ram_image.VehicleManufacturerSparePartNumber_Nissan[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_VehicleManufacturerSparePartNumber_Nissan_Ex(uint8_t * const VehicleManufacturerSparePartNumber_Nissan,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.VehicleManufacturerSparePartNumber_Nissan;	//EEP_CONTENT_VehicleManufacturerSparePartNumber_Nissan_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_VehicleManufacturerSparePartNumber_Nissan_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_VehicleManufacturerSparePartNumber_Nissan_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        VehicleManufacturerSparePartNumber_Nissan[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_VehicleManufacturerSparePartNumber_Nissan(uint8_t const* VehicleManufacturerSparePartNumber_Nissan )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_VehicleManufacturerSparePartNumber_Nissan_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.VehicleManufacturerSparePartNumber_Nissan[i] != VehicleManufacturerSparePartNumber_Nissan[i] )
        {
            eep_ram_image.VehicleManufacturerSparePartNumber_Nissan[i] = VehicleManufacturerSparePartNumber_Nissan[i];
            wr_eep_by_list_req[EEP_CONTENT_VehicleManufacturerSparePartNumber_Nissan_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_VehicleManufacturerSparePartNumber_Nissan_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_VehicleManufacturerSparePartNumber_Nissan_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_VehicleManufacturerSparePartNumber_Nissan_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:VehicleManufacturerSparePartNumber_Nissan firt item[0] = %d",VehicleManufacturerSparePartNumber_Nissan[0]);
}

void GenEep_Set_VehicleManufacturerSparePartNumber_Nissan_Ex(uint8_t const* VehicleManufacturerSparePartNumber_Nissan,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_VehicleManufacturerSparePartNumber_Nissan_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_VehicleManufacturerSparePartNumber_Nissan_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.VehicleManufacturerSparePartNumber_Nissan;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != VehicleManufacturerSparePartNumber_Nissan[i] )
        {
            dest[offset+i] = VehicleManufacturerSparePartNumber_Nissan[i];
            wr_eep_by_list_req[EEP_CONTENT_VehicleManufacturerSparePartNumber_Nissan_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_VehicleManufacturerSparePartNumber_Nissan_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_VehicleManufacturerSparePartNumber_Nissan_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_VehicleManufacturerSparePartNumber_Nissan_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:VehicleManufacturerSparePartNumber_Nissan by Extend Api");
}

static void EepCopy_VehicleManufacturerSparePartNumber_Nissan_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_VehicleManufacturerSparePartNumber_Nissan_ITEM_LEN; i ++)
    {
        eep_hw_image.VehicleManufacturerSparePartNumber_Nissan[i] = eep_ram_image.VehicleManufacturerSparePartNumber_Nissan[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_VehicleManufacturerSparePartNumber_Nissan_IN_BLOCK_NUM);
}

#else 
uint8_t GenEep_Get_VehicleManufacturerSparePartNumber_Nissan(void)
{
    uint8_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.VehicleManufacturerSparePartNumber_Nissan;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_VehicleManufacturerSparePartNumber_Nissan(uint8_t const VehicleManufacturerSparePartNumber_Nissan)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.VehicleManufacturerSparePartNumber_Nissan != VehicleManufacturerSparePartNumber_Nissan )
    {
        eep_ram_image.VehicleManufacturerSparePartNumber_Nissan = VehicleManufacturerSparePartNumber_Nissan;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_VehicleManufacturerSparePartNumber_Nissan_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_VehicleManufacturerSparePartNumber_Nissan_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_VehicleManufacturerSparePartNumber_Nissan_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_VehicleManufacturerSparePartNumber_Nissan_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:VehicleManufacturerSparePartNumber_Nissan=%d",VehicleManufacturerSparePartNumber_Nissan);
}

static void EepCopy_VehicleManufacturerSparePartNumber_Nissan_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.VehicleManufacturerSparePartNumber_Nissan = eep_ram_image.VehicleManufacturerSparePartNumber_Nissan;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_VehicleManufacturerSparePartNumber_Nissan_IN_BLOCK_NUM);
}
#endif
//static void Proc_VehicleManufacturerSparePartNumber_Nissan_RestoreToDefault(void)
//{
//#if EEP_CONTENT_VehicleManufacturerSparePartNumber_Nissan_CONST_VARIBLE
//	GenEep_Set_VehicleManufacturerSparePartNumber_Nissan(dflt_eep_val.VehicleManufacturerSparePartNumber_Nissan);
//#endif	
//}
#if EEP_CONTENT_VehicleManufacturerSparePartNumber_Nissan_CONST_VARIBLE
#define Proc_VehicleManufacturerSparePartNumber_Nissan_RestoreToDefault()	do{GenEep_Set_VehicleManufacturerSparePartNumber_Nissan(dflt_eep_val.VehicleManufacturerSparePartNumber_Nissan);}while(0)
#else
#define Proc_VehicleManufacturerSparePartNumber_Nissan_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> VehicleManufacturerSparePartNumber_Nissan function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> VisteonProductPartNumber function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_VisteonProductPartNumber_ITEM_LEN > 1
void GenEep_Get_VisteonProductPartNumber(uint8_t * const VisteonProductPartNumber )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_VisteonProductPartNumber_ITEM_LEN; i ++)
    {
        VisteonProductPartNumber[i] = eep_ram_image.VisteonProductPartNumber[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_VisteonProductPartNumber_Ex(uint8_t * const VisteonProductPartNumber,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.VisteonProductPartNumber;	//EEP_CONTENT_VisteonProductPartNumber_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_VisteonProductPartNumber_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_VisteonProductPartNumber_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        VisteonProductPartNumber[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_VisteonProductPartNumber(uint8_t const* VisteonProductPartNumber )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_VisteonProductPartNumber_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.VisteonProductPartNumber[i] != VisteonProductPartNumber[i] )
        {
            eep_ram_image.VisteonProductPartNumber[i] = VisteonProductPartNumber[i];
            wr_eep_by_list_req[EEP_CONTENT_VisteonProductPartNumber_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_VisteonProductPartNumber_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_VisteonProductPartNumber_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_VisteonProductPartNumber_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:VisteonProductPartNumber firt item[0] = %d",VisteonProductPartNumber[0]);
}

void GenEep_Set_VisteonProductPartNumber_Ex(uint8_t const* VisteonProductPartNumber,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_VisteonProductPartNumber_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_VisteonProductPartNumber_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.VisteonProductPartNumber;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != VisteonProductPartNumber[i] )
        {
            dest[offset+i] = VisteonProductPartNumber[i];
            wr_eep_by_list_req[EEP_CONTENT_VisteonProductPartNumber_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_VisteonProductPartNumber_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_VisteonProductPartNumber_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_VisteonProductPartNumber_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:VisteonProductPartNumber by Extend Api");
}

static void EepCopy_VisteonProductPartNumber_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_VisteonProductPartNumber_ITEM_LEN; i ++)
    {
        eep_hw_image.VisteonProductPartNumber[i] = eep_ram_image.VisteonProductPartNumber[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_VisteonProductPartNumber_IN_BLOCK_NUM);
}

#else 
uint8_t GenEep_Get_VisteonProductPartNumber(void)
{
    uint8_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.VisteonProductPartNumber;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_VisteonProductPartNumber(uint8_t const VisteonProductPartNumber)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.VisteonProductPartNumber != VisteonProductPartNumber )
    {
        eep_ram_image.VisteonProductPartNumber = VisteonProductPartNumber;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_VisteonProductPartNumber_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_VisteonProductPartNumber_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_VisteonProductPartNumber_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_VisteonProductPartNumber_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:VisteonProductPartNumber=%d",VisteonProductPartNumber);
}

static void EepCopy_VisteonProductPartNumber_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.VisteonProductPartNumber = eep_ram_image.VisteonProductPartNumber;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_VisteonProductPartNumber_IN_BLOCK_NUM);
}
#endif
//static void Proc_VisteonProductPartNumber_RestoreToDefault(void)
//{
//#if EEP_CONTENT_VisteonProductPartNumber_CONST_VARIBLE
//	GenEep_Set_VisteonProductPartNumber(dflt_eep_val.VisteonProductPartNumber);
//#endif	
//}
#if EEP_CONTENT_VisteonProductPartNumber_CONST_VARIBLE
#define Proc_VisteonProductPartNumber_RestoreToDefault()	do{GenEep_Set_VisteonProductPartNumber(dflt_eep_val.VisteonProductPartNumber);}while(0)
#else
#define Proc_VisteonProductPartNumber_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> VisteonProductPartNumber function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> EquippedPCBVisteonPartNumMainBorad function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_EquippedPCBVisteonPartNumMainBorad_ITEM_LEN > 1
void GenEep_Get_EquippedPCBVisteonPartNumMainBorad(uint8_t * const EquippedPCBVisteonPartNumMainBorad )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_EquippedPCBVisteonPartNumMainBorad_ITEM_LEN; i ++)
    {
        EquippedPCBVisteonPartNumMainBorad[i] = eep_ram_image.EquippedPCBVisteonPartNumMainBorad[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_EquippedPCBVisteonPartNumMainBorad_Ex(uint8_t * const EquippedPCBVisteonPartNumMainBorad,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.EquippedPCBVisteonPartNumMainBorad;	//EEP_CONTENT_EquippedPCBVisteonPartNumMainBorad_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_EquippedPCBVisteonPartNumMainBorad_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_EquippedPCBVisteonPartNumMainBorad_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        EquippedPCBVisteonPartNumMainBorad[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_EquippedPCBVisteonPartNumMainBorad(uint8_t const* EquippedPCBVisteonPartNumMainBorad )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_EquippedPCBVisteonPartNumMainBorad_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.EquippedPCBVisteonPartNumMainBorad[i] != EquippedPCBVisteonPartNumMainBorad[i] )
        {
            eep_ram_image.EquippedPCBVisteonPartNumMainBorad[i] = EquippedPCBVisteonPartNumMainBorad[i];
            wr_eep_by_list_req[EEP_CONTENT_EquippedPCBVisteonPartNumMainBorad_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_EquippedPCBVisteonPartNumMainBorad_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_EquippedPCBVisteonPartNumMainBorad_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_EquippedPCBVisteonPartNumMainBorad_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:EquippedPCBVisteonPartNumMainBorad firt item[0] = %d",EquippedPCBVisteonPartNumMainBorad[0]);
}

void GenEep_Set_EquippedPCBVisteonPartNumMainBorad_Ex(uint8_t const* EquippedPCBVisteonPartNumMainBorad,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_EquippedPCBVisteonPartNumMainBorad_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_EquippedPCBVisteonPartNumMainBorad_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.EquippedPCBVisteonPartNumMainBorad;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != EquippedPCBVisteonPartNumMainBorad[i] )
        {
            dest[offset+i] = EquippedPCBVisteonPartNumMainBorad[i];
            wr_eep_by_list_req[EEP_CONTENT_EquippedPCBVisteonPartNumMainBorad_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_EquippedPCBVisteonPartNumMainBorad_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_EquippedPCBVisteonPartNumMainBorad_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_EquippedPCBVisteonPartNumMainBorad_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:EquippedPCBVisteonPartNumMainBorad by Extend Api");
}

static void EepCopy_EquippedPCBVisteonPartNumMainBorad_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_EquippedPCBVisteonPartNumMainBorad_ITEM_LEN; i ++)
    {
        eep_hw_image.EquippedPCBVisteonPartNumMainBorad[i] = eep_ram_image.EquippedPCBVisteonPartNumMainBorad[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_EquippedPCBVisteonPartNumMainBorad_IN_BLOCK_NUM);
}

#else 
uint8_t GenEep_Get_EquippedPCBVisteonPartNumMainBorad(void)
{
    uint8_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.EquippedPCBVisteonPartNumMainBorad;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_EquippedPCBVisteonPartNumMainBorad(uint8_t const EquippedPCBVisteonPartNumMainBorad)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.EquippedPCBVisteonPartNumMainBorad != EquippedPCBVisteonPartNumMainBorad )
    {
        eep_ram_image.EquippedPCBVisteonPartNumMainBorad = EquippedPCBVisteonPartNumMainBorad;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_EquippedPCBVisteonPartNumMainBorad_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_EquippedPCBVisteonPartNumMainBorad_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_EquippedPCBVisteonPartNumMainBorad_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_EquippedPCBVisteonPartNumMainBorad_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:EquippedPCBVisteonPartNumMainBorad=%d",EquippedPCBVisteonPartNumMainBorad);
}

static void EepCopy_EquippedPCBVisteonPartNumMainBorad_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.EquippedPCBVisteonPartNumMainBorad = eep_ram_image.EquippedPCBVisteonPartNumMainBorad;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_EquippedPCBVisteonPartNumMainBorad_IN_BLOCK_NUM);
}
#endif
//static void Proc_EquippedPCBVisteonPartNumMainBorad_RestoreToDefault(void)
//{
//#if EEP_CONTENT_EquippedPCBVisteonPartNumMainBorad_CONST_VARIBLE
//	GenEep_Set_EquippedPCBVisteonPartNumMainBorad(dflt_eep_val.EquippedPCBVisteonPartNumMainBorad);
//#endif	
//}
#if EEP_CONTENT_EquippedPCBVisteonPartNumMainBorad_CONST_VARIBLE
#define Proc_EquippedPCBVisteonPartNumMainBorad_RestoreToDefault()	do{GenEep_Set_EquippedPCBVisteonPartNumMainBorad(dflt_eep_val.EquippedPCBVisteonPartNumMainBorad);}while(0)
#else
#define Proc_EquippedPCBVisteonPartNumMainBorad_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> EquippedPCBVisteonPartNumMainBorad function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> EquippedPCBVisteonPartNumSubBorad function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_EquippedPCBVisteonPartNumSubBorad_ITEM_LEN > 1
void GenEep_Get_EquippedPCBVisteonPartNumSubBorad(uint8_t * const EquippedPCBVisteonPartNumSubBorad )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_EquippedPCBVisteonPartNumSubBorad_ITEM_LEN; i ++)
    {
        EquippedPCBVisteonPartNumSubBorad[i] = eep_ram_image.EquippedPCBVisteonPartNumSubBorad[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_EquippedPCBVisteonPartNumSubBorad_Ex(uint8_t * const EquippedPCBVisteonPartNumSubBorad,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.EquippedPCBVisteonPartNumSubBorad;	//EEP_CONTENT_EquippedPCBVisteonPartNumSubBorad_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_EquippedPCBVisteonPartNumSubBorad_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_EquippedPCBVisteonPartNumSubBorad_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        EquippedPCBVisteonPartNumSubBorad[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_EquippedPCBVisteonPartNumSubBorad(uint8_t const* EquippedPCBVisteonPartNumSubBorad )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_EquippedPCBVisteonPartNumSubBorad_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.EquippedPCBVisteonPartNumSubBorad[i] != EquippedPCBVisteonPartNumSubBorad[i] )
        {
            eep_ram_image.EquippedPCBVisteonPartNumSubBorad[i] = EquippedPCBVisteonPartNumSubBorad[i];
            wr_eep_by_list_req[EEP_CONTENT_EquippedPCBVisteonPartNumSubBorad_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_EquippedPCBVisteonPartNumSubBorad_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_EquippedPCBVisteonPartNumSubBorad_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_EquippedPCBVisteonPartNumSubBorad_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:EquippedPCBVisteonPartNumSubBorad firt item[0] = %d",EquippedPCBVisteonPartNumSubBorad[0]);
}

void GenEep_Set_EquippedPCBVisteonPartNumSubBorad_Ex(uint8_t const* EquippedPCBVisteonPartNumSubBorad,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_EquippedPCBVisteonPartNumSubBorad_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_EquippedPCBVisteonPartNumSubBorad_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.EquippedPCBVisteonPartNumSubBorad;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != EquippedPCBVisteonPartNumSubBorad[i] )
        {
            dest[offset+i] = EquippedPCBVisteonPartNumSubBorad[i];
            wr_eep_by_list_req[EEP_CONTENT_EquippedPCBVisteonPartNumSubBorad_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_EquippedPCBVisteonPartNumSubBorad_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_EquippedPCBVisteonPartNumSubBorad_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_EquippedPCBVisteonPartNumSubBorad_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:EquippedPCBVisteonPartNumSubBorad by Extend Api");
}

static void EepCopy_EquippedPCBVisteonPartNumSubBorad_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_EquippedPCBVisteonPartNumSubBorad_ITEM_LEN; i ++)
    {
        eep_hw_image.EquippedPCBVisteonPartNumSubBorad[i] = eep_ram_image.EquippedPCBVisteonPartNumSubBorad[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_EquippedPCBVisteonPartNumSubBorad_IN_BLOCK_NUM);
}

#else 
uint8_t GenEep_Get_EquippedPCBVisteonPartNumSubBorad(void)
{
    uint8_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.EquippedPCBVisteonPartNumSubBorad;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_EquippedPCBVisteonPartNumSubBorad(uint8_t const EquippedPCBVisteonPartNumSubBorad)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.EquippedPCBVisteonPartNumSubBorad != EquippedPCBVisteonPartNumSubBorad )
    {
        eep_ram_image.EquippedPCBVisteonPartNumSubBorad = EquippedPCBVisteonPartNumSubBorad;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_EquippedPCBVisteonPartNumSubBorad_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_EquippedPCBVisteonPartNumSubBorad_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_EquippedPCBVisteonPartNumSubBorad_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_EquippedPCBVisteonPartNumSubBorad_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:EquippedPCBVisteonPartNumSubBorad=%d",EquippedPCBVisteonPartNumSubBorad);
}

static void EepCopy_EquippedPCBVisteonPartNumSubBorad_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.EquippedPCBVisteonPartNumSubBorad = eep_ram_image.EquippedPCBVisteonPartNumSubBorad;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_EquippedPCBVisteonPartNumSubBorad_IN_BLOCK_NUM);
}
#endif
//static void Proc_EquippedPCBVisteonPartNumSubBorad_RestoreToDefault(void)
//{
//#if EEP_CONTENT_EquippedPCBVisteonPartNumSubBorad_CONST_VARIBLE
//	GenEep_Set_EquippedPCBVisteonPartNumSubBorad(dflt_eep_val.EquippedPCBVisteonPartNumSubBorad);
//#endif	
//}
#if EEP_CONTENT_EquippedPCBVisteonPartNumSubBorad_CONST_VARIBLE
#define Proc_EquippedPCBVisteonPartNumSubBorad_RestoreToDefault()	do{GenEep_Set_EquippedPCBVisteonPartNumSubBorad(dflt_eep_val.EquippedPCBVisteonPartNumSubBorad);}while(0)
#else
#define Proc_EquippedPCBVisteonPartNumSubBorad_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> EquippedPCBVisteonPartNumSubBorad function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> DAUniqueID function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_DAUniqueID_ITEM_LEN > 1
void GenEep_Get_DAUniqueID(uint8_t * const DAUniqueID )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_DAUniqueID_ITEM_LEN; i ++)
    {
        DAUniqueID[i] = eep_ram_image.DAUniqueID[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_DAUniqueID_Ex(uint8_t * const DAUniqueID,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.DAUniqueID;	//EEP_CONTENT_DAUniqueID_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_DAUniqueID_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_DAUniqueID_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        DAUniqueID[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_DAUniqueID(uint8_t const* DAUniqueID )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_DAUniqueID_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.DAUniqueID[i] != DAUniqueID[i] )
        {
            eep_ram_image.DAUniqueID[i] = DAUniqueID[i];
            wr_eep_by_list_req[EEP_CONTENT_DAUniqueID_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_DAUniqueID_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_DAUniqueID_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_DAUniqueID_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:DAUniqueID firt item[0] = %d",DAUniqueID[0]);
}

void GenEep_Set_DAUniqueID_Ex(uint8_t const* DAUniqueID,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_DAUniqueID_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_DAUniqueID_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.DAUniqueID;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != DAUniqueID[i] )
        {
            dest[offset+i] = DAUniqueID[i];
            wr_eep_by_list_req[EEP_CONTENT_DAUniqueID_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_DAUniqueID_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_DAUniqueID_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_DAUniqueID_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:DAUniqueID by Extend Api");
}

static void EepCopy_DAUniqueID_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_DAUniqueID_ITEM_LEN; i ++)
    {
        eep_hw_image.DAUniqueID[i] = eep_ram_image.DAUniqueID[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_DAUniqueID_IN_BLOCK_NUM);
}

#else 
uint8_t GenEep_Get_DAUniqueID(void)
{
    uint8_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.DAUniqueID;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_DAUniqueID(uint8_t const DAUniqueID)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.DAUniqueID != DAUniqueID )
    {
        eep_ram_image.DAUniqueID = DAUniqueID;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_DAUniqueID_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_DAUniqueID_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_DAUniqueID_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_DAUniqueID_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:DAUniqueID=%d",DAUniqueID);
}

static void EepCopy_DAUniqueID_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.DAUniqueID = eep_ram_image.DAUniqueID;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_DAUniqueID_IN_BLOCK_NUM);
}
#endif
//static void Proc_DAUniqueID_RestoreToDefault(void)
//{
//#if EEP_CONTENT_DAUniqueID_CONST_VARIBLE
//	GenEep_Set_DAUniqueID(dflt_eep_val.DAUniqueID);
//#endif	
//}
#if EEP_CONTENT_DAUniqueID_CONST_VARIBLE
#define Proc_DAUniqueID_RestoreToDefault()	do{GenEep_Set_DAUniqueID(dflt_eep_val.DAUniqueID);}while(0)
#else
#define Proc_DAUniqueID_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> DAUniqueID function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> ProductSerialNum function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_ProductSerialNum_ITEM_LEN > 1
void GenEep_Get_ProductSerialNum(uint8_t * const ProductSerialNum )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_ProductSerialNum_ITEM_LEN; i ++)
    {
        ProductSerialNum[i] = eep_ram_image.ProductSerialNum[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_ProductSerialNum_Ex(uint8_t * const ProductSerialNum,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.ProductSerialNum;	//EEP_CONTENT_ProductSerialNum_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_ProductSerialNum_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_ProductSerialNum_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        ProductSerialNum[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_ProductSerialNum(uint8_t const* ProductSerialNum )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_ProductSerialNum_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.ProductSerialNum[i] != ProductSerialNum[i] )
        {
            eep_ram_image.ProductSerialNum[i] = ProductSerialNum[i];
            wr_eep_by_list_req[EEP_CONTENT_ProductSerialNum_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_ProductSerialNum_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_ProductSerialNum_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_ProductSerialNum_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:ProductSerialNum firt item[0] = %d",ProductSerialNum[0]);
}

void GenEep_Set_ProductSerialNum_Ex(uint8_t const* ProductSerialNum,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_ProductSerialNum_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_ProductSerialNum_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.ProductSerialNum;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != ProductSerialNum[i] )
        {
            dest[offset+i] = ProductSerialNum[i];
            wr_eep_by_list_req[EEP_CONTENT_ProductSerialNum_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_ProductSerialNum_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_ProductSerialNum_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_ProductSerialNum_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:ProductSerialNum by Extend Api");
}

static void EepCopy_ProductSerialNum_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_ProductSerialNum_ITEM_LEN; i ++)
    {
        eep_hw_image.ProductSerialNum[i] = eep_ram_image.ProductSerialNum[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_ProductSerialNum_IN_BLOCK_NUM);
}

#else 
uint8_t GenEep_Get_ProductSerialNum(void)
{
    uint8_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.ProductSerialNum;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_ProductSerialNum(uint8_t const ProductSerialNum)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.ProductSerialNum != ProductSerialNum )
    {
        eep_ram_image.ProductSerialNum = ProductSerialNum;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_ProductSerialNum_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_ProductSerialNum_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_ProductSerialNum_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_ProductSerialNum_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:ProductSerialNum=%d",ProductSerialNum);
}

static void EepCopy_ProductSerialNum_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.ProductSerialNum = eep_ram_image.ProductSerialNum;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_ProductSerialNum_IN_BLOCK_NUM);
}
#endif
//static void Proc_ProductSerialNum_RestoreToDefault(void)
//{
//#if EEP_CONTENT_ProductSerialNum_CONST_VARIBLE
//	GenEep_Set_ProductSerialNum(dflt_eep_val.ProductSerialNum);
//#endif	
//}
#if EEP_CONTENT_ProductSerialNum_CONST_VARIBLE
#define Proc_ProductSerialNum_RestoreToDefault()	do{GenEep_Set_ProductSerialNum(dflt_eep_val.ProductSerialNum);}while(0)
#else
#define Proc_ProductSerialNum_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> ProductSerialNum function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> ProductSerialNumMainBoard function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_ProductSerialNumMainBoard_ITEM_LEN > 1
void GenEep_Get_ProductSerialNumMainBoard(uint8_t * const ProductSerialNumMainBoard )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_ProductSerialNumMainBoard_ITEM_LEN; i ++)
    {
        ProductSerialNumMainBoard[i] = eep_ram_image.ProductSerialNumMainBoard[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_ProductSerialNumMainBoard_Ex(uint8_t * const ProductSerialNumMainBoard,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.ProductSerialNumMainBoard;	//EEP_CONTENT_ProductSerialNumMainBoard_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_ProductSerialNumMainBoard_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_ProductSerialNumMainBoard_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        ProductSerialNumMainBoard[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_ProductSerialNumMainBoard(uint8_t const* ProductSerialNumMainBoard )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_ProductSerialNumMainBoard_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.ProductSerialNumMainBoard[i] != ProductSerialNumMainBoard[i] )
        {
            eep_ram_image.ProductSerialNumMainBoard[i] = ProductSerialNumMainBoard[i];
            wr_eep_by_list_req[EEP_CONTENT_ProductSerialNumMainBoard_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_ProductSerialNumMainBoard_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_ProductSerialNumMainBoard_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_ProductSerialNumMainBoard_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:ProductSerialNumMainBoard firt item[0] = %d",ProductSerialNumMainBoard[0]);
}

void GenEep_Set_ProductSerialNumMainBoard_Ex(uint8_t const* ProductSerialNumMainBoard,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_ProductSerialNumMainBoard_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_ProductSerialNumMainBoard_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.ProductSerialNumMainBoard;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != ProductSerialNumMainBoard[i] )
        {
            dest[offset+i] = ProductSerialNumMainBoard[i];
            wr_eep_by_list_req[EEP_CONTENT_ProductSerialNumMainBoard_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_ProductSerialNumMainBoard_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_ProductSerialNumMainBoard_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_ProductSerialNumMainBoard_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:ProductSerialNumMainBoard by Extend Api");
}

static void EepCopy_ProductSerialNumMainBoard_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_ProductSerialNumMainBoard_ITEM_LEN; i ++)
    {
        eep_hw_image.ProductSerialNumMainBoard[i] = eep_ram_image.ProductSerialNumMainBoard[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_ProductSerialNumMainBoard_IN_BLOCK_NUM);
}

#else 
uint8_t GenEep_Get_ProductSerialNumMainBoard(void)
{
    uint8_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.ProductSerialNumMainBoard;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_ProductSerialNumMainBoard(uint8_t const ProductSerialNumMainBoard)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.ProductSerialNumMainBoard != ProductSerialNumMainBoard )
    {
        eep_ram_image.ProductSerialNumMainBoard = ProductSerialNumMainBoard;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_ProductSerialNumMainBoard_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_ProductSerialNumMainBoard_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_ProductSerialNumMainBoard_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_ProductSerialNumMainBoard_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:ProductSerialNumMainBoard=%d",ProductSerialNumMainBoard);
}

static void EepCopy_ProductSerialNumMainBoard_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.ProductSerialNumMainBoard = eep_ram_image.ProductSerialNumMainBoard;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_ProductSerialNumMainBoard_IN_BLOCK_NUM);
}
#endif
//static void Proc_ProductSerialNumMainBoard_RestoreToDefault(void)
//{
//#if EEP_CONTENT_ProductSerialNumMainBoard_CONST_VARIBLE
//	GenEep_Set_ProductSerialNumMainBoard(dflt_eep_val.ProductSerialNumMainBoard);
//#endif	
//}
#if EEP_CONTENT_ProductSerialNumMainBoard_CONST_VARIBLE
#define Proc_ProductSerialNumMainBoard_RestoreToDefault()	do{GenEep_Set_ProductSerialNumMainBoard(dflt_eep_val.ProductSerialNumMainBoard);}while(0)
#else
#define Proc_ProductSerialNumMainBoard_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> ProductSerialNumMainBoard function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> ProductSerialNumSubBoard function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_ProductSerialNumSubBoard_ITEM_LEN > 1
void GenEep_Get_ProductSerialNumSubBoard(uint8_t * const ProductSerialNumSubBoard )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_ProductSerialNumSubBoard_ITEM_LEN; i ++)
    {
        ProductSerialNumSubBoard[i] = eep_ram_image.ProductSerialNumSubBoard[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_ProductSerialNumSubBoard_Ex(uint8_t * const ProductSerialNumSubBoard,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.ProductSerialNumSubBoard;	//EEP_CONTENT_ProductSerialNumSubBoard_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_ProductSerialNumSubBoard_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_ProductSerialNumSubBoard_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        ProductSerialNumSubBoard[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_ProductSerialNumSubBoard(uint8_t const* ProductSerialNumSubBoard )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_ProductSerialNumSubBoard_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.ProductSerialNumSubBoard[i] != ProductSerialNumSubBoard[i] )
        {
            eep_ram_image.ProductSerialNumSubBoard[i] = ProductSerialNumSubBoard[i];
            wr_eep_by_list_req[EEP_CONTENT_ProductSerialNumSubBoard_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_ProductSerialNumSubBoard_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_ProductSerialNumSubBoard_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_ProductSerialNumSubBoard_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:ProductSerialNumSubBoard firt item[0] = %d",ProductSerialNumSubBoard[0]);
}

void GenEep_Set_ProductSerialNumSubBoard_Ex(uint8_t const* ProductSerialNumSubBoard,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_ProductSerialNumSubBoard_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_ProductSerialNumSubBoard_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.ProductSerialNumSubBoard;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != ProductSerialNumSubBoard[i] )
        {
            dest[offset+i] = ProductSerialNumSubBoard[i];
            wr_eep_by_list_req[EEP_CONTENT_ProductSerialNumSubBoard_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_ProductSerialNumSubBoard_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_ProductSerialNumSubBoard_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_ProductSerialNumSubBoard_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:ProductSerialNumSubBoard by Extend Api");
}

static void EepCopy_ProductSerialNumSubBoard_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_ProductSerialNumSubBoard_ITEM_LEN; i ++)
    {
        eep_hw_image.ProductSerialNumSubBoard[i] = eep_ram_image.ProductSerialNumSubBoard[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_ProductSerialNumSubBoard_IN_BLOCK_NUM);
}

#else 
uint8_t GenEep_Get_ProductSerialNumSubBoard(void)
{
    uint8_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.ProductSerialNumSubBoard;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_ProductSerialNumSubBoard(uint8_t const ProductSerialNumSubBoard)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.ProductSerialNumSubBoard != ProductSerialNumSubBoard )
    {
        eep_ram_image.ProductSerialNumSubBoard = ProductSerialNumSubBoard;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_ProductSerialNumSubBoard_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_ProductSerialNumSubBoard_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_ProductSerialNumSubBoard_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_ProductSerialNumSubBoard_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:ProductSerialNumSubBoard=%d",ProductSerialNumSubBoard);
}

static void EepCopy_ProductSerialNumSubBoard_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.ProductSerialNumSubBoard = eep_ram_image.ProductSerialNumSubBoard;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_ProductSerialNumSubBoard_IN_BLOCK_NUM);
}
#endif
//static void Proc_ProductSerialNumSubBoard_RestoreToDefault(void)
//{
//#if EEP_CONTENT_ProductSerialNumSubBoard_CONST_VARIBLE
//	GenEep_Set_ProductSerialNumSubBoard(dflt_eep_val.ProductSerialNumSubBoard);
//#endif	
//}
#if EEP_CONTENT_ProductSerialNumSubBoard_CONST_VARIBLE
#define Proc_ProductSerialNumSubBoard_RestoreToDefault()	do{GenEep_Set_ProductSerialNumSubBoard(dflt_eep_val.ProductSerialNumSubBoard);}while(0)
#else
#define Proc_ProductSerialNumSubBoard_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> ProductSerialNumSubBoard function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> SMDManufacturingDate function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_SMDManufacturingDate_ITEM_LEN > 1
void GenEep_Get_SMDManufacturingDate(uint8_t * const SMDManufacturingDate )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_SMDManufacturingDate_ITEM_LEN; i ++)
    {
        SMDManufacturingDate[i] = eep_ram_image.SMDManufacturingDate[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_SMDManufacturingDate_Ex(uint8_t * const SMDManufacturingDate,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.SMDManufacturingDate;	//EEP_CONTENT_SMDManufacturingDate_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_SMDManufacturingDate_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_SMDManufacturingDate_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        SMDManufacturingDate[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_SMDManufacturingDate(uint8_t const* SMDManufacturingDate )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_SMDManufacturingDate_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.SMDManufacturingDate[i] != SMDManufacturingDate[i] )
        {
            eep_ram_image.SMDManufacturingDate[i] = SMDManufacturingDate[i];
            wr_eep_by_list_req[EEP_CONTENT_SMDManufacturingDate_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_SMDManufacturingDate_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_SMDManufacturingDate_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_SMDManufacturingDate_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:SMDManufacturingDate firt item[0] = %d",SMDManufacturingDate[0]);
}

void GenEep_Set_SMDManufacturingDate_Ex(uint8_t const* SMDManufacturingDate,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_SMDManufacturingDate_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_SMDManufacturingDate_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.SMDManufacturingDate;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != SMDManufacturingDate[i] )
        {
            dest[offset+i] = SMDManufacturingDate[i];
            wr_eep_by_list_req[EEP_CONTENT_SMDManufacturingDate_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_SMDManufacturingDate_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_SMDManufacturingDate_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_SMDManufacturingDate_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:SMDManufacturingDate by Extend Api");
}

static void EepCopy_SMDManufacturingDate_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_SMDManufacturingDate_ITEM_LEN; i ++)
    {
        eep_hw_image.SMDManufacturingDate[i] = eep_ram_image.SMDManufacturingDate[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_SMDManufacturingDate_IN_BLOCK_NUM);
}

#else 
uint8_t GenEep_Get_SMDManufacturingDate(void)
{
    uint8_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.SMDManufacturingDate;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_SMDManufacturingDate(uint8_t const SMDManufacturingDate)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.SMDManufacturingDate != SMDManufacturingDate )
    {
        eep_ram_image.SMDManufacturingDate = SMDManufacturingDate;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_SMDManufacturingDate_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_SMDManufacturingDate_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_SMDManufacturingDate_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_SMDManufacturingDate_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:SMDManufacturingDate=%d",SMDManufacturingDate);
}

static void EepCopy_SMDManufacturingDate_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.SMDManufacturingDate = eep_ram_image.SMDManufacturingDate;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_SMDManufacturingDate_IN_BLOCK_NUM);
}
#endif
//static void Proc_SMDManufacturingDate_RestoreToDefault(void)
//{
//#if EEP_CONTENT_SMDManufacturingDate_CONST_VARIBLE
//	GenEep_Set_SMDManufacturingDate(dflt_eep_val.SMDManufacturingDate);
//#endif	
//}
#if EEP_CONTENT_SMDManufacturingDate_CONST_VARIBLE
#define Proc_SMDManufacturingDate_RestoreToDefault()	do{GenEep_Set_SMDManufacturingDate(dflt_eep_val.SMDManufacturingDate);}while(0)
#else
#define Proc_SMDManufacturingDate_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> SMDManufacturingDate function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> AssemblyManufacturingDate function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_AssemblyManufacturingDate_ITEM_LEN > 1
void GenEep_Get_AssemblyManufacturingDate(uint8_t * const AssemblyManufacturingDate )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_AssemblyManufacturingDate_ITEM_LEN; i ++)
    {
        AssemblyManufacturingDate[i] = eep_ram_image.AssemblyManufacturingDate[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_AssemblyManufacturingDate_Ex(uint8_t * const AssemblyManufacturingDate,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.AssemblyManufacturingDate;	//EEP_CONTENT_AssemblyManufacturingDate_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_AssemblyManufacturingDate_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_AssemblyManufacturingDate_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        AssemblyManufacturingDate[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_AssemblyManufacturingDate(uint8_t const* AssemblyManufacturingDate )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_AssemblyManufacturingDate_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.AssemblyManufacturingDate[i] != AssemblyManufacturingDate[i] )
        {
            eep_ram_image.AssemblyManufacturingDate[i] = AssemblyManufacturingDate[i];
            wr_eep_by_list_req[EEP_CONTENT_AssemblyManufacturingDate_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_AssemblyManufacturingDate_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_AssemblyManufacturingDate_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_AssemblyManufacturingDate_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:AssemblyManufacturingDate firt item[0] = %d",AssemblyManufacturingDate[0]);
}

void GenEep_Set_AssemblyManufacturingDate_Ex(uint8_t const* AssemblyManufacturingDate,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_AssemblyManufacturingDate_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_AssemblyManufacturingDate_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.AssemblyManufacturingDate;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != AssemblyManufacturingDate[i] )
        {
            dest[offset+i] = AssemblyManufacturingDate[i];
            wr_eep_by_list_req[EEP_CONTENT_AssemblyManufacturingDate_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_AssemblyManufacturingDate_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_AssemblyManufacturingDate_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_AssemblyManufacturingDate_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:AssemblyManufacturingDate by Extend Api");
}

static void EepCopy_AssemblyManufacturingDate_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_AssemblyManufacturingDate_ITEM_LEN; i ++)
    {
        eep_hw_image.AssemblyManufacturingDate[i] = eep_ram_image.AssemblyManufacturingDate[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_AssemblyManufacturingDate_IN_BLOCK_NUM);
}

#else 
uint8_t GenEep_Get_AssemblyManufacturingDate(void)
{
    uint8_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.AssemblyManufacturingDate;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_AssemblyManufacturingDate(uint8_t const AssemblyManufacturingDate)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.AssemblyManufacturingDate != AssemblyManufacturingDate )
    {
        eep_ram_image.AssemblyManufacturingDate = AssemblyManufacturingDate;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_AssemblyManufacturingDate_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_AssemblyManufacturingDate_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_AssemblyManufacturingDate_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_AssemblyManufacturingDate_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:AssemblyManufacturingDate=%d",AssemblyManufacturingDate);
}

static void EepCopy_AssemblyManufacturingDate_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.AssemblyManufacturingDate = eep_ram_image.AssemblyManufacturingDate;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_AssemblyManufacturingDate_IN_BLOCK_NUM);
}
#endif
//static void Proc_AssemblyManufacturingDate_RestoreToDefault(void)
//{
//#if EEP_CONTENT_AssemblyManufacturingDate_CONST_VARIBLE
//	GenEep_Set_AssemblyManufacturingDate(dflt_eep_val.AssemblyManufacturingDate);
//#endif	
//}
#if EEP_CONTENT_AssemblyManufacturingDate_CONST_VARIBLE
#define Proc_AssemblyManufacturingDate_RestoreToDefault()	do{GenEep_Set_AssemblyManufacturingDate(dflt_eep_val.AssemblyManufacturingDate);}while(0)
#else
#define Proc_AssemblyManufacturingDate_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> AssemblyManufacturingDate function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> TraceabilityBytes function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_TraceabilityBytes_ITEM_LEN > 1
void GenEep_Get_TraceabilityBytes(uint8_t * const TraceabilityBytes )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_TraceabilityBytes_ITEM_LEN; i ++)
    {
        TraceabilityBytes[i] = eep_ram_image.TraceabilityBytes[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_TraceabilityBytes_Ex(uint8_t * const TraceabilityBytes,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.TraceabilityBytes;	//EEP_CONTENT_TraceabilityBytes_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_TraceabilityBytes_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_TraceabilityBytes_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        TraceabilityBytes[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_TraceabilityBytes(uint8_t const* TraceabilityBytes )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_TraceabilityBytes_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.TraceabilityBytes[i] != TraceabilityBytes[i] )
        {
            eep_ram_image.TraceabilityBytes[i] = TraceabilityBytes[i];
            wr_eep_by_list_req[EEP_CONTENT_TraceabilityBytes_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_TraceabilityBytes_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_TraceabilityBytes_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_TraceabilityBytes_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:TraceabilityBytes firt item[0] = %d",TraceabilityBytes[0]);
}

void GenEep_Set_TraceabilityBytes_Ex(uint8_t const* TraceabilityBytes,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_TraceabilityBytes_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_TraceabilityBytes_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.TraceabilityBytes;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != TraceabilityBytes[i] )
        {
            dest[offset+i] = TraceabilityBytes[i];
            wr_eep_by_list_req[EEP_CONTENT_TraceabilityBytes_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_TraceabilityBytes_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_TraceabilityBytes_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_TraceabilityBytes_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:TraceabilityBytes by Extend Api");
}

static void EepCopy_TraceabilityBytes_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_TraceabilityBytes_ITEM_LEN; i ++)
    {
        eep_hw_image.TraceabilityBytes[i] = eep_ram_image.TraceabilityBytes[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_TraceabilityBytes_IN_BLOCK_NUM);
}

#else 
uint8_t GenEep_Get_TraceabilityBytes(void)
{
    uint8_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.TraceabilityBytes;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_TraceabilityBytes(uint8_t const TraceabilityBytes)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.TraceabilityBytes != TraceabilityBytes )
    {
        eep_ram_image.TraceabilityBytes = TraceabilityBytes;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_TraceabilityBytes_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_TraceabilityBytes_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_TraceabilityBytes_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_TraceabilityBytes_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:TraceabilityBytes=%d",TraceabilityBytes);
}

static void EepCopy_TraceabilityBytes_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.TraceabilityBytes = eep_ram_image.TraceabilityBytes;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_TraceabilityBytes_IN_BLOCK_NUM);
}
#endif
//static void Proc_TraceabilityBytes_RestoreToDefault(void)
//{
//#if EEP_CONTENT_TraceabilityBytes_CONST_VARIBLE
//	GenEep_Set_TraceabilityBytes(dflt_eep_val.TraceabilityBytes);
//#endif	
//}
#if EEP_CONTENT_TraceabilityBytes_CONST_VARIBLE
#define Proc_TraceabilityBytes_RestoreToDefault()	do{GenEep_Set_TraceabilityBytes(dflt_eep_val.TraceabilityBytes);}while(0)
#else
#define Proc_TraceabilityBytes_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> TraceabilityBytes function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> SMDPlantNum function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_SMDPlantNum_ITEM_LEN > 1
void GenEep_Get_SMDPlantNum(uint8_t * const SMDPlantNum )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_SMDPlantNum_ITEM_LEN; i ++)
    {
        SMDPlantNum[i] = eep_ram_image.SMDPlantNum[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_SMDPlantNum_Ex(uint8_t * const SMDPlantNum,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.SMDPlantNum;	//EEP_CONTENT_SMDPlantNum_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_SMDPlantNum_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_SMDPlantNum_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        SMDPlantNum[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_SMDPlantNum(uint8_t const* SMDPlantNum )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_SMDPlantNum_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.SMDPlantNum[i] != SMDPlantNum[i] )
        {
            eep_ram_image.SMDPlantNum[i] = SMDPlantNum[i];
            wr_eep_by_list_req[EEP_CONTENT_SMDPlantNum_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_SMDPlantNum_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_SMDPlantNum_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_SMDPlantNum_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:SMDPlantNum firt item[0] = %d",SMDPlantNum[0]);
}

void GenEep_Set_SMDPlantNum_Ex(uint8_t const* SMDPlantNum,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_SMDPlantNum_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_SMDPlantNum_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.SMDPlantNum;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != SMDPlantNum[i] )
        {
            dest[offset+i] = SMDPlantNum[i];
            wr_eep_by_list_req[EEP_CONTENT_SMDPlantNum_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_SMDPlantNum_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_SMDPlantNum_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_SMDPlantNum_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:SMDPlantNum by Extend Api");
}

static void EepCopy_SMDPlantNum_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_SMDPlantNum_ITEM_LEN; i ++)
    {
        eep_hw_image.SMDPlantNum[i] = eep_ram_image.SMDPlantNum[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_SMDPlantNum_IN_BLOCK_NUM);
}

#else 
uint8_t GenEep_Get_SMDPlantNum(void)
{
    uint8_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.SMDPlantNum;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_SMDPlantNum(uint8_t const SMDPlantNum)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.SMDPlantNum != SMDPlantNum )
    {
        eep_ram_image.SMDPlantNum = SMDPlantNum;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_SMDPlantNum_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_SMDPlantNum_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_SMDPlantNum_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_SMDPlantNum_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:SMDPlantNum=%d",SMDPlantNum);
}

static void EepCopy_SMDPlantNum_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.SMDPlantNum = eep_ram_image.SMDPlantNum;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_SMDPlantNum_IN_BLOCK_NUM);
}
#endif
//static void Proc_SMDPlantNum_RestoreToDefault(void)
//{
//#if EEP_CONTENT_SMDPlantNum_CONST_VARIBLE
//	GenEep_Set_SMDPlantNum(dflt_eep_val.SMDPlantNum);
//#endif	
//}
#if EEP_CONTENT_SMDPlantNum_CONST_VARIBLE
#define Proc_SMDPlantNum_RestoreToDefault()	do{GenEep_Set_SMDPlantNum(dflt_eep_val.SMDPlantNum);}while(0)
#else
#define Proc_SMDPlantNum_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> SMDPlantNum function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> AssemblyPlantNum function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_AssemblyPlantNum_ITEM_LEN > 1
void GenEep_Get_AssemblyPlantNum(uint8_t * const AssemblyPlantNum )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_AssemblyPlantNum_ITEM_LEN; i ++)
    {
        AssemblyPlantNum[i] = eep_ram_image.AssemblyPlantNum[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_AssemblyPlantNum_Ex(uint8_t * const AssemblyPlantNum,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.AssemblyPlantNum;	//EEP_CONTENT_AssemblyPlantNum_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_AssemblyPlantNum_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_AssemblyPlantNum_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        AssemblyPlantNum[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_AssemblyPlantNum(uint8_t const* AssemblyPlantNum )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_AssemblyPlantNum_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.AssemblyPlantNum[i] != AssemblyPlantNum[i] )
        {
            eep_ram_image.AssemblyPlantNum[i] = AssemblyPlantNum[i];
            wr_eep_by_list_req[EEP_CONTENT_AssemblyPlantNum_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_AssemblyPlantNum_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_AssemblyPlantNum_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_AssemblyPlantNum_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:AssemblyPlantNum firt item[0] = %d",AssemblyPlantNum[0]);
}

void GenEep_Set_AssemblyPlantNum_Ex(uint8_t const* AssemblyPlantNum,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_AssemblyPlantNum_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_AssemblyPlantNum_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.AssemblyPlantNum;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != AssemblyPlantNum[i] )
        {
            dest[offset+i] = AssemblyPlantNum[i];
            wr_eep_by_list_req[EEP_CONTENT_AssemblyPlantNum_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_AssemblyPlantNum_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_AssemblyPlantNum_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_AssemblyPlantNum_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:AssemblyPlantNum by Extend Api");
}

static void EepCopy_AssemblyPlantNum_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_AssemblyPlantNum_ITEM_LEN; i ++)
    {
        eep_hw_image.AssemblyPlantNum[i] = eep_ram_image.AssemblyPlantNum[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_AssemblyPlantNum_IN_BLOCK_NUM);
}

#else 
uint8_t GenEep_Get_AssemblyPlantNum(void)
{
    uint8_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.AssemblyPlantNum;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_AssemblyPlantNum(uint8_t const AssemblyPlantNum)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.AssemblyPlantNum != AssemblyPlantNum )
    {
        eep_ram_image.AssemblyPlantNum = AssemblyPlantNum;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_AssemblyPlantNum_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_AssemblyPlantNum_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_AssemblyPlantNum_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_AssemblyPlantNum_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:AssemblyPlantNum=%d",AssemblyPlantNum);
}

static void EepCopy_AssemblyPlantNum_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.AssemblyPlantNum = eep_ram_image.AssemblyPlantNum;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_AssemblyPlantNum_IN_BLOCK_NUM);
}
#endif
//static void Proc_AssemblyPlantNum_RestoreToDefault(void)
//{
//#if EEP_CONTENT_AssemblyPlantNum_CONST_VARIBLE
//	GenEep_Set_AssemblyPlantNum(dflt_eep_val.AssemblyPlantNum);
//#endif	
//}
#if EEP_CONTENT_AssemblyPlantNum_CONST_VARIBLE
#define Proc_AssemblyPlantNum_RestoreToDefault()	do{GenEep_Set_AssemblyPlantNum(dflt_eep_val.AssemblyPlantNum);}while(0)
#else
#define Proc_AssemblyPlantNum_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> AssemblyPlantNum function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> DEM_EVENT_CAN_COM_DATA function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_DEM_EVENT_CAN_COM_DATA_ITEM_LEN > 1
void GenEep_Get_DEM_EVENT_CAN_COM_DATA(uint8_t * const DEM_EVENT_CAN_COM_DATA )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_DEM_EVENT_CAN_COM_DATA_ITEM_LEN; i ++)
    {
        DEM_EVENT_CAN_COM_DATA[i] = eep_ram_image.DEM_EVENT_CAN_COM_DATA[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_DEM_EVENT_CAN_COM_DATA_Ex(uint8_t * const DEM_EVENT_CAN_COM_DATA,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.DEM_EVENT_CAN_COM_DATA;	//EEP_CONTENT_DEM_EVENT_CAN_COM_DATA_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_DEM_EVENT_CAN_COM_DATA_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_DEM_EVENT_CAN_COM_DATA_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        DEM_EVENT_CAN_COM_DATA[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_DEM_EVENT_CAN_COM_DATA(uint8_t const* DEM_EVENT_CAN_COM_DATA )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_DEM_EVENT_CAN_COM_DATA_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.DEM_EVENT_CAN_COM_DATA[i] != DEM_EVENT_CAN_COM_DATA[i] )
        {
            eep_ram_image.DEM_EVENT_CAN_COM_DATA[i] = DEM_EVENT_CAN_COM_DATA[i];
            wr_eep_by_list_req[EEP_CONTENT_DEM_EVENT_CAN_COM_DATA_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_DEM_EVENT_CAN_COM_DATA_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_DEM_EVENT_CAN_COM_DATA_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_DEM_EVENT_CAN_COM_DATA_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:DEM_EVENT_CAN_COM_DATA firt item[0] = %d",DEM_EVENT_CAN_COM_DATA[0]);
}

void GenEep_Set_DEM_EVENT_CAN_COM_DATA_Ex(uint8_t const* DEM_EVENT_CAN_COM_DATA,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_DEM_EVENT_CAN_COM_DATA_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_DEM_EVENT_CAN_COM_DATA_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.DEM_EVENT_CAN_COM_DATA;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != DEM_EVENT_CAN_COM_DATA[i] )
        {
            dest[offset+i] = DEM_EVENT_CAN_COM_DATA[i];
            wr_eep_by_list_req[EEP_CONTENT_DEM_EVENT_CAN_COM_DATA_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_DEM_EVENT_CAN_COM_DATA_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_DEM_EVENT_CAN_COM_DATA_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_DEM_EVENT_CAN_COM_DATA_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:DEM_EVENT_CAN_COM_DATA by Extend Api");
}

static void EepCopy_DEM_EVENT_CAN_COM_DATA_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_DEM_EVENT_CAN_COM_DATA_ITEM_LEN; i ++)
    {
        eep_hw_image.DEM_EVENT_CAN_COM_DATA[i] = eep_ram_image.DEM_EVENT_CAN_COM_DATA[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_DEM_EVENT_CAN_COM_DATA_IN_BLOCK_NUM);
}

#else 
uint8_t GenEep_Get_DEM_EVENT_CAN_COM_DATA(void)
{
    uint8_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.DEM_EVENT_CAN_COM_DATA;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_DEM_EVENT_CAN_COM_DATA(uint8_t const DEM_EVENT_CAN_COM_DATA)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.DEM_EVENT_CAN_COM_DATA != DEM_EVENT_CAN_COM_DATA )
    {
        eep_ram_image.DEM_EVENT_CAN_COM_DATA = DEM_EVENT_CAN_COM_DATA;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_DEM_EVENT_CAN_COM_DATA_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_DEM_EVENT_CAN_COM_DATA_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_DEM_EVENT_CAN_COM_DATA_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_DEM_EVENT_CAN_COM_DATA_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:DEM_EVENT_CAN_COM_DATA=%d",DEM_EVENT_CAN_COM_DATA);
}

static void EepCopy_DEM_EVENT_CAN_COM_DATA_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.DEM_EVENT_CAN_COM_DATA = eep_ram_image.DEM_EVENT_CAN_COM_DATA;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_DEM_EVENT_CAN_COM_DATA_IN_BLOCK_NUM);
}
#endif
//static void Proc_DEM_EVENT_CAN_COM_DATA_RestoreToDefault(void)
//{
//#if EEP_CONTENT_DEM_EVENT_CAN_COM_DATA_CONST_VARIBLE
//	GenEep_Set_DEM_EVENT_CAN_COM_DATA(dflt_eep_val.DEM_EVENT_CAN_COM_DATA);
//#endif	
//}
#if EEP_CONTENT_DEM_EVENT_CAN_COM_DATA_CONST_VARIBLE
#define Proc_DEM_EVENT_CAN_COM_DATA_RestoreToDefault()	do{GenEep_Set_DEM_EVENT_CAN_COM_DATA(dflt_eep_val.DEM_EVENT_CAN_COM_DATA);}while(0)
#else
#define Proc_DEM_EVENT_CAN_COM_DATA_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> DEM_EVENT_CAN_COM_DATA function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_LEN > 1
void GenEep_Get_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA(uint8_t * const DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_LEN; i ++)
    {
        DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA[i] = eep_ram_image.DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_Ex(uint8_t * const DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA;	//EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA(uint8_t const* DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA[i] != DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA[i] )
        {
            eep_ram_image.DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA[i] = DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA[i];
            wr_eep_by_list_req[EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA firt item[0] = %d",DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA[0]);
}

void GenEep_Set_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_Ex(uint8_t const* DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA[i] )
        {
            dest[offset+i] = DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA[i];
            wr_eep_by_list_req[EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA by Extend Api");
}

static void EepCopy_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_LEN; i ++)
    {
        eep_hw_image.DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA[i] = eep_ram_image.DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_IN_BLOCK_NUM);
}

#else 
uint8_t GenEep_Get_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA(void)
{
    uint8_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA(uint8_t const DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA != DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA )
    {
        eep_ram_image.DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA = DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA=%d",DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA);
}

static void EepCopy_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA = eep_ram_image.DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_IN_BLOCK_NUM);
}
#endif
//static void Proc_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_RestoreToDefault(void)
//{
//#if EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_CONST_VARIBLE
//	GenEep_Set_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA(dflt_eep_val.DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA);
//#endif	
//}
#if EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_CONST_VARIBLE
#define Proc_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_RestoreToDefault()	do{GenEep_Set_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA(dflt_eep_val.DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA);}while(0)
#else
#define Proc_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_LEN > 1
void GenEep_Get_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA(uint8_t * const DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_LEN; i ++)
    {
        DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA[i] = eep_ram_image.DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_Ex(uint8_t * const DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA;	//EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA(uint8_t const* DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA[i] != DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA[i] )
        {
            eep_ram_image.DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA[i] = DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA[i];
            wr_eep_by_list_req[EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA firt item[0] = %d",DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA[0]);
}

void GenEep_Set_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_Ex(uint8_t const* DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA[i] )
        {
            dest[offset+i] = DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA[i];
            wr_eep_by_list_req[EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA by Extend Api");
}

static void EepCopy_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_LEN; i ++)
    {
        eep_hw_image.DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA[i] = eep_ram_image.DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_IN_BLOCK_NUM);
}

#else 
uint8_t GenEep_Get_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA(void)
{
    uint8_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA(uint8_t const DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA != DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA )
    {
        eep_ram_image.DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA = DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA=%d",DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA);
}

static void EepCopy_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA = eep_ram_image.DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_IN_BLOCK_NUM);
}
#endif
//static void Proc_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_RestoreToDefault(void)
//{
//#if EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_CONST_VARIBLE
//	GenEep_Set_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA(dflt_eep_val.DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA);
//#endif	
//}
#if EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_CONST_VARIBLE
#define Proc_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_RestoreToDefault()	do{GenEep_Set_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA(dflt_eep_val.DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA);}while(0)
#else
#define Proc_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_LEN > 1
void GenEep_Get_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA(uint8_t * const DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_LEN; i ++)
    {
        DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA[i] = eep_ram_image.DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_Ex(uint8_t * const DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA;	//EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA(uint8_t const* DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA[i] != DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA[i] )
        {
            eep_ram_image.DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA[i] = DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA[i];
            wr_eep_by_list_req[EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA firt item[0] = %d",DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA[0]);
}

void GenEep_Set_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_Ex(uint8_t const* DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA[i] )
        {
            dest[offset+i] = DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA[i];
            wr_eep_by_list_req[EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA by Extend Api");
}

static void EepCopy_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_LEN; i ++)
    {
        eep_hw_image.DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA[i] = eep_ram_image.DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_IN_BLOCK_NUM);
}

#else 
uint8_t GenEep_Get_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA(void)
{
    uint8_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA(uint8_t const DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA != DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA )
    {
        eep_ram_image.DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA = DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA=%d",DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA);
}

static void EepCopy_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA = eep_ram_image.DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_IN_BLOCK_NUM);
}
#endif
//static void Proc_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_RestoreToDefault(void)
//{
//#if EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_CONST_VARIBLE
//	GenEep_Set_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA(dflt_eep_val.DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA);
//#endif	
//}
#if EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_CONST_VARIBLE
#define Proc_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_RestoreToDefault()	do{GenEep_Set_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA(dflt_eep_val.DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA);}while(0)
#else
#define Proc_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_LEN > 1
void GenEep_Get_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA(uint8_t * const DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_LEN; i ++)
    {
        DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA[i] = eep_ram_image.DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_Ex(uint8_t * const DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA;	//EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA(uint8_t const* DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA[i] != DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA[i] )
        {
            eep_ram_image.DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA[i] = DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA[i];
            wr_eep_by_list_req[EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA firt item[0] = %d",DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA[0]);
}

void GenEep_Set_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_Ex(uint8_t const* DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA[i] )
        {
            dest[offset+i] = DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA[i];
            wr_eep_by_list_req[EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA by Extend Api");
}

static void EepCopy_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_LEN; i ++)
    {
        eep_hw_image.DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA[i] = eep_ram_image.DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_IN_BLOCK_NUM);
}

#else 
uint8_t GenEep_Get_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA(void)
{
    uint8_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA(uint8_t const DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA != DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA )
    {
        eep_ram_image.DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA = DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA=%d",DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA);
}

static void EepCopy_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA = eep_ram_image.DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_IN_BLOCK_NUM);
}
#endif
//static void Proc_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_RestoreToDefault(void)
//{
//#if EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_CONST_VARIBLE
//	GenEep_Set_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA(dflt_eep_val.DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA);
//#endif	
//}
#if EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_CONST_VARIBLE
#define Proc_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_RestoreToDefault()	do{GenEep_Set_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA(dflt_eep_val.DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA);}while(0)
#else
#define Proc_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_LEN > 1
void GenEep_Get_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA(uint8_t * const DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_LEN; i ++)
    {
        DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA[i] = eep_ram_image.DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_Ex(uint8_t * const DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA;	//EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA(uint8_t const* DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA[i] != DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA[i] )
        {
            eep_ram_image.DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA[i] = DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA[i];
            wr_eep_by_list_req[EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA firt item[0] = %d",DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA[0]);
}

void GenEep_Set_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_Ex(uint8_t const* DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA[i] )
        {
            dest[offset+i] = DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA[i];
            wr_eep_by_list_req[EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA by Extend Api");
}

static void EepCopy_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_LEN; i ++)
    {
        eep_hw_image.DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA[i] = eep_ram_image.DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_IN_BLOCK_NUM);
}

#else 
uint8_t GenEep_Get_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA(void)
{
    uint8_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA(uint8_t const DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA != DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA )
    {
        eep_ram_image.DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA = DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA=%d",DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA);
}

static void EepCopy_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA = eep_ram_image.DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_IN_BLOCK_NUM);
}
#endif
//static void Proc_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_RestoreToDefault(void)
//{
//#if EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_CONST_VARIBLE
//	GenEep_Set_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA(dflt_eep_val.DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA);
//#endif	
//}
#if EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_CONST_VARIBLE
#define Proc_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_RestoreToDefault()	do{GenEep_Set_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA(dflt_eep_val.DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA);}while(0)
#else
#define Proc_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_LEN > 1
void GenEep_Get_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA(uint8_t * const DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_LEN; i ++)
    {
        DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA[i] = eep_ram_image.DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_Ex(uint8_t * const DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA;	//EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA(uint8_t const* DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA[i] != DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA[i] )
        {
            eep_ram_image.DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA[i] = DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA[i];
            wr_eep_by_list_req[EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA firt item[0] = %d",DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA[0]);
}

void GenEep_Set_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_Ex(uint8_t const* DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA[i] )
        {
            dest[offset+i] = DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA[i];
            wr_eep_by_list_req[EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA by Extend Api");
}

static void EepCopy_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_LEN; i ++)
    {
        eep_hw_image.DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA[i] = eep_ram_image.DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_IN_BLOCK_NUM);
}

#else 
uint8_t GenEep_Get_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA(void)
{
    uint8_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA(uint8_t const DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA != DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA )
    {
        eep_ram_image.DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA = DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA=%d",DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA);
}

static void EepCopy_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA = eep_ram_image.DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_IN_BLOCK_NUM);
}
#endif
//static void Proc_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_RestoreToDefault(void)
//{
//#if EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_CONST_VARIBLE
//	GenEep_Set_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA(dflt_eep_val.DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA);
//#endif	
//}
#if EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_CONST_VARIBLE
#define Proc_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_RestoreToDefault()	do{GenEep_Set_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA(dflt_eep_val.DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA);}while(0)
#else
#define Proc_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_LEN > 1
void GenEep_Get_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA(uint8_t * const DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_LEN; i ++)
    {
        DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA[i] = eep_ram_image.DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_Ex(uint8_t * const DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA;	//EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA(uint8_t const* DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA[i] != DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA[i] )
        {
            eep_ram_image.DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA[i] = DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA[i];
            wr_eep_by_list_req[EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA firt item[0] = %d",DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA[0]);
}

void GenEep_Set_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_Ex(uint8_t const* DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA[i] )
        {
            dest[offset+i] = DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA[i];
            wr_eep_by_list_req[EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA by Extend Api");
}

static void EepCopy_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_LEN; i ++)
    {
        eep_hw_image.DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA[i] = eep_ram_image.DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_IN_BLOCK_NUM);
}

#else 
uint8_t GenEep_Get_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA(void)
{
    uint8_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA(uint8_t const DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA != DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA )
    {
        eep_ram_image.DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA = DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA=%d",DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA);
}

static void EepCopy_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA = eep_ram_image.DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_IN_BLOCK_NUM);
}
#endif
//static void Proc_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_RestoreToDefault(void)
//{
//#if EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_CONST_VARIBLE
//	GenEep_Set_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA(dflt_eep_val.DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA);
//#endif	
//}
#if EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_CONST_VARIBLE
#define Proc_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_RestoreToDefault()	do{GenEep_Set_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA(dflt_eep_val.DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA);}while(0)
#else
#define Proc_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_LEN > 1
void GenEep_Get_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA(uint8_t * const DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_LEN; i ++)
    {
        DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA[i] = eep_ram_image.DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_Ex(uint8_t * const DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA;	//EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA(uint8_t const* DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA[i] != DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA[i] )
        {
            eep_ram_image.DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA[i] = DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA[i];
            wr_eep_by_list_req[EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA firt item[0] = %d",DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA[0]);
}

void GenEep_Set_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_Ex(uint8_t const* DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA[i] )
        {
            dest[offset+i] = DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA[i];
            wr_eep_by_list_req[EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA by Extend Api");
}

static void EepCopy_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_LEN; i ++)
    {
        eep_hw_image.DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA[i] = eep_ram_image.DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_IN_BLOCK_NUM);
}

#else 
uint8_t GenEep_Get_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA(void)
{
    uint8_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA(uint8_t const DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA != DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA )
    {
        eep_ram_image.DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA = DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA=%d",DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA);
}

static void EepCopy_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA = eep_ram_image.DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_IN_BLOCK_NUM);
}
#endif
//static void Proc_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_RestoreToDefault(void)
//{
//#if EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_CONST_VARIBLE
//	GenEep_Set_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA(dflt_eep_val.DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA);
//#endif	
//}
#if EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_CONST_VARIBLE
#define Proc_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_RestoreToDefault()	do{GenEep_Set_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA(dflt_eep_val.DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA);}while(0)
#else
#define Proc_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_ITEM_LEN > 1
void GenEep_Get_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA(uint8_t * const DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_ITEM_LEN; i ++)
    {
        DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA[i] = eep_ram_image.DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_Ex(uint8_t * const DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA;	//EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA(uint8_t const* DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA[i] != DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA[i] )
        {
            eep_ram_image.DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA[i] = DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA[i];
            wr_eep_by_list_req[EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA firt item[0] = %d",DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA[0]);
}

void GenEep_Set_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_Ex(uint8_t const* DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA[i] )
        {
            dest[offset+i] = DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA[i];
            wr_eep_by_list_req[EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA by Extend Api");
}

static void EepCopy_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_ITEM_LEN; i ++)
    {
        eep_hw_image.DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA[i] = eep_ram_image.DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_IN_BLOCK_NUM);
}

#else 
uint8_t GenEep_Get_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA(void)
{
    uint8_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA(uint8_t const DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA != DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA )
    {
        eep_ram_image.DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA = DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA=%d",DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA);
}

static void EepCopy_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA = eep_ram_image.DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_IN_BLOCK_NUM);
}
#endif
//static void Proc_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_RestoreToDefault(void)
//{
//#if EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_CONST_VARIBLE
//	GenEep_Set_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA(dflt_eep_val.DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA);
//#endif	
//}
#if EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_CONST_VARIBLE
#define Proc_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_RestoreToDefault()	do{GenEep_Set_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA(dflt_eep_val.DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA);}while(0)
#else
#define Proc_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_ITEM_LEN > 1
void GenEep_Get_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA(uint8_t * const DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_ITEM_LEN; i ++)
    {
        DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA[i] = eep_ram_image.DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_Ex(uint8_t * const DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA;	//EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA(uint8_t const* DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA[i] != DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA[i] )
        {
            eep_ram_image.DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA[i] = DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA[i];
            wr_eep_by_list_req[EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA firt item[0] = %d",DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA[0]);
}

void GenEep_Set_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_Ex(uint8_t const* DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA[i] )
        {
            dest[offset+i] = DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA[i];
            wr_eep_by_list_req[EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA by Extend Api");
}

static void EepCopy_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_ITEM_LEN; i ++)
    {
        eep_hw_image.DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA[i] = eep_ram_image.DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_IN_BLOCK_NUM);
}

#else 
uint8_t GenEep_Get_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA(void)
{
    uint8_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA(uint8_t const DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA != DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA )
    {
        eep_ram_image.DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA = DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA=%d",DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA);
}

static void EepCopy_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA = eep_ram_image.DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_IN_BLOCK_NUM);
}
#endif
//static void Proc_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_RestoreToDefault(void)
//{
//#if EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_CONST_VARIBLE
//	GenEep_Set_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA(dflt_eep_val.DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA);
//#endif	
//}
#if EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_CONST_VARIBLE
#define Proc_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_RestoreToDefault()	do{GenEep_Set_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA(dflt_eep_val.DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA);}while(0)
#else
#define Proc_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> DEM_EVENT_HVAC_PANEL_CONNECTION_DATA function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_ITEM_LEN > 1
void GenEep_Get_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA(uint8_t * const DEM_EVENT_HVAC_PANEL_CONNECTION_DATA )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_ITEM_LEN; i ++)
    {
        DEM_EVENT_HVAC_PANEL_CONNECTION_DATA[i] = eep_ram_image.DEM_EVENT_HVAC_PANEL_CONNECTION_DATA[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_Ex(uint8_t * const DEM_EVENT_HVAC_PANEL_CONNECTION_DATA,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.DEM_EVENT_HVAC_PANEL_CONNECTION_DATA;	//EEP_CONTENT_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        DEM_EVENT_HVAC_PANEL_CONNECTION_DATA[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA(uint8_t const* DEM_EVENT_HVAC_PANEL_CONNECTION_DATA )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.DEM_EVENT_HVAC_PANEL_CONNECTION_DATA[i] != DEM_EVENT_HVAC_PANEL_CONNECTION_DATA[i] )
        {
            eep_ram_image.DEM_EVENT_HVAC_PANEL_CONNECTION_DATA[i] = DEM_EVENT_HVAC_PANEL_CONNECTION_DATA[i];
            wr_eep_by_list_req[EEP_CONTENT_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:DEM_EVENT_HVAC_PANEL_CONNECTION_DATA firt item[0] = %d",DEM_EVENT_HVAC_PANEL_CONNECTION_DATA[0]);
}

void GenEep_Set_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_Ex(uint8_t const* DEM_EVENT_HVAC_PANEL_CONNECTION_DATA,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.DEM_EVENT_HVAC_PANEL_CONNECTION_DATA;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != DEM_EVENT_HVAC_PANEL_CONNECTION_DATA[i] )
        {
            dest[offset+i] = DEM_EVENT_HVAC_PANEL_CONNECTION_DATA[i];
            wr_eep_by_list_req[EEP_CONTENT_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:DEM_EVENT_HVAC_PANEL_CONNECTION_DATA by Extend Api");
}

static void EepCopy_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_ITEM_LEN; i ++)
    {
        eep_hw_image.DEM_EVENT_HVAC_PANEL_CONNECTION_DATA[i] = eep_ram_image.DEM_EVENT_HVAC_PANEL_CONNECTION_DATA[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_IN_BLOCK_NUM);
}

#else 
uint8_t GenEep_Get_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA(void)
{
    uint8_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.DEM_EVENT_HVAC_PANEL_CONNECTION_DATA;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA(uint8_t const DEM_EVENT_HVAC_PANEL_CONNECTION_DATA)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.DEM_EVENT_HVAC_PANEL_CONNECTION_DATA != DEM_EVENT_HVAC_PANEL_CONNECTION_DATA )
    {
        eep_ram_image.DEM_EVENT_HVAC_PANEL_CONNECTION_DATA = DEM_EVENT_HVAC_PANEL_CONNECTION_DATA;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:DEM_EVENT_HVAC_PANEL_CONNECTION_DATA=%d",DEM_EVENT_HVAC_PANEL_CONNECTION_DATA);
}

static void EepCopy_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.DEM_EVENT_HVAC_PANEL_CONNECTION_DATA = eep_ram_image.DEM_EVENT_HVAC_PANEL_CONNECTION_DATA;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_IN_BLOCK_NUM);
}
#endif
//static void Proc_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_RestoreToDefault(void)
//{
//#if EEP_CONTENT_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_CONST_VARIBLE
//	GenEep_Set_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA(dflt_eep_val.DEM_EVENT_HVAC_PANEL_CONNECTION_DATA);
//#endif	
//}
#if EEP_CONTENT_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_CONST_VARIBLE
#define Proc_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_RestoreToDefault()	do{GenEep_Set_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA(dflt_eep_val.DEM_EVENT_HVAC_PANEL_CONNECTION_DATA);}while(0)
#else
#define Proc_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> DEM_EVENT_HVAC_PANEL_CONNECTION_DATA function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

uint32_t GenEep_Get_development_nvm_write_count(void)
{
	return eep_ram_image.development_nvm_write_count;
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> TachoUpValue function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_TachoUpValue_ITEM_LEN > 1
void GenEep_Get_TachoUpValue(uint16_t * const TachoUpValue )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_TachoUpValue_ITEM_LEN; i ++)
    {
        TachoUpValue[i] = eep_ram_image.TachoUpValue[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_TachoUpValue_Ex(uint8_t * const TachoUpValue,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.TachoUpValue;	//EEP_CONTENT_TachoUpValue_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_TachoUpValue_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_TachoUpValue_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        TachoUpValue[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_TachoUpValue(uint16_t const* TachoUpValue )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_TachoUpValue_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.TachoUpValue[i] != TachoUpValue[i] )
        {
            eep_ram_image.TachoUpValue[i] = TachoUpValue[i];
            wr_eep_by_list_req[EEP_CONTENT_TachoUpValue_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_TachoUpValue_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_TachoUpValue_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_TachoUpValue_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:TachoUpValue firt item[0] = %d",TachoUpValue[0]);
}

void GenEep_Set_TachoUpValue_Ex(uint8_t const* TachoUpValue,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_TachoUpValue_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_TachoUpValue_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.TachoUpValue;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != TachoUpValue[i] )
        {
            dest[offset+i] = TachoUpValue[i];
            wr_eep_by_list_req[EEP_CONTENT_TachoUpValue_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_TachoUpValue_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_TachoUpValue_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_TachoUpValue_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:TachoUpValue by Extend Api");
}

static void EepCopy_TachoUpValue_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_TachoUpValue_ITEM_LEN; i ++)
    {
        eep_hw_image.TachoUpValue[i] = eep_ram_image.TachoUpValue[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_TachoUpValue_IN_BLOCK_NUM);
}

#else 
uint16_t GenEep_Get_TachoUpValue(void)
{
    uint16_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.TachoUpValue;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_TachoUpValue(uint16_t const TachoUpValue)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.TachoUpValue != TachoUpValue )
    {
        eep_ram_image.TachoUpValue = TachoUpValue;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_TachoUpValue_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_TachoUpValue_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_TachoUpValue_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_TachoUpValue_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:TachoUpValue=%d",TachoUpValue);
}

static void EepCopy_TachoUpValue_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.TachoUpValue = eep_ram_image.TachoUpValue;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_TachoUpValue_IN_BLOCK_NUM);
}
#endif
//static void Proc_TachoUpValue_RestoreToDefault(void)
//{
//#if EEP_CONTENT_TachoUpValue_CONST_VARIBLE
//	GenEep_Set_TachoUpValue(dflt_eep_val.TachoUpValue);
//#endif	
//}
#if EEP_CONTENT_TachoUpValue_CONST_VARIBLE
#define Proc_TachoUpValue_RestoreToDefault()	do{GenEep_Set_TachoUpValue(dflt_eep_val.TachoUpValue);}while(0)
#else
#define Proc_TachoUpValue_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> TachoUpValue function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> TachoLowValue function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_TachoLowValue_ITEM_LEN > 1
void GenEep_Get_TachoLowValue(uint16_t * const TachoLowValue )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_TachoLowValue_ITEM_LEN; i ++)
    {
        TachoLowValue[i] = eep_ram_image.TachoLowValue[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_TachoLowValue_Ex(uint8_t * const TachoLowValue,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.TachoLowValue;	//EEP_CONTENT_TachoLowValue_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_TachoLowValue_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_TachoLowValue_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        TachoLowValue[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_TachoLowValue(uint16_t const* TachoLowValue )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_TachoLowValue_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.TachoLowValue[i] != TachoLowValue[i] )
        {
            eep_ram_image.TachoLowValue[i] = TachoLowValue[i];
            wr_eep_by_list_req[EEP_CONTENT_TachoLowValue_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_TachoLowValue_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_TachoLowValue_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_TachoLowValue_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:TachoLowValue firt item[0] = %d",TachoLowValue[0]);
}

void GenEep_Set_TachoLowValue_Ex(uint8_t const* TachoLowValue,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_TachoLowValue_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_TachoLowValue_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.TachoLowValue;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != TachoLowValue[i] )
        {
            dest[offset+i] = TachoLowValue[i];
            wr_eep_by_list_req[EEP_CONTENT_TachoLowValue_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_TachoLowValue_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_TachoLowValue_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_TachoLowValue_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:TachoLowValue by Extend Api");
}

static void EepCopy_TachoLowValue_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_TachoLowValue_ITEM_LEN; i ++)
    {
        eep_hw_image.TachoLowValue[i] = eep_ram_image.TachoLowValue[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_TachoLowValue_IN_BLOCK_NUM);
}

#else 
uint16_t GenEep_Get_TachoLowValue(void)
{
    uint16_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.TachoLowValue;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_TachoLowValue(uint16_t const TachoLowValue)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.TachoLowValue != TachoLowValue )
    {
        eep_ram_image.TachoLowValue = TachoLowValue;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_TachoLowValue_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_TachoLowValue_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_TachoLowValue_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_TachoLowValue_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:TachoLowValue=%d",TachoLowValue);
}

static void EepCopy_TachoLowValue_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.TachoLowValue = eep_ram_image.TachoLowValue;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_TachoLowValue_IN_BLOCK_NUM);
}
#endif
//static void Proc_TachoLowValue_RestoreToDefault(void)
//{
//#if EEP_CONTENT_TachoLowValue_CONST_VARIBLE
//	GenEep_Set_TachoLowValue(dflt_eep_val.TachoLowValue);
//#endif	
//}
#if EEP_CONTENT_TachoLowValue_CONST_VARIBLE
#define Proc_TachoLowValue_RestoreToDefault()	do{GenEep_Set_TachoLowValue(dflt_eep_val.TachoLowValue);}while(0)
#else
#define Proc_TachoLowValue_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> TachoLowValue function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> TachoLimitValue function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_TachoLimitValue_ITEM_LEN > 1
void GenEep_Get_TachoLimitValue(uint16_t * const TachoLimitValue )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_TachoLimitValue_ITEM_LEN; i ++)
    {
        TachoLimitValue[i] = eep_ram_image.TachoLimitValue[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_TachoLimitValue_Ex(uint8_t * const TachoLimitValue,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.TachoLimitValue;	//EEP_CONTENT_TachoLimitValue_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_TachoLimitValue_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_TachoLimitValue_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        TachoLimitValue[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_TachoLimitValue(uint16_t const* TachoLimitValue )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_TachoLimitValue_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.TachoLimitValue[i] != TachoLimitValue[i] )
        {
            eep_ram_image.TachoLimitValue[i] = TachoLimitValue[i];
            wr_eep_by_list_req[EEP_CONTENT_TachoLimitValue_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_TachoLimitValue_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_TachoLimitValue_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_TachoLimitValue_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:TachoLimitValue firt item[0] = %d",TachoLimitValue[0]);
}

void GenEep_Set_TachoLimitValue_Ex(uint8_t const* TachoLimitValue,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_TachoLimitValue_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_TachoLimitValue_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.TachoLimitValue;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != TachoLimitValue[i] )
        {
            dest[offset+i] = TachoLimitValue[i];
            wr_eep_by_list_req[EEP_CONTENT_TachoLimitValue_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_TachoLimitValue_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_TachoLimitValue_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_TachoLimitValue_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:TachoLimitValue by Extend Api");
}

static void EepCopy_TachoLimitValue_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_TachoLimitValue_ITEM_LEN; i ++)
    {
        eep_hw_image.TachoLimitValue[i] = eep_ram_image.TachoLimitValue[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_TachoLimitValue_IN_BLOCK_NUM);
}

#else 
uint16_t GenEep_Get_TachoLimitValue(void)
{
    uint16_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.TachoLimitValue;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_TachoLimitValue(uint16_t const TachoLimitValue)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.TachoLimitValue != TachoLimitValue )
    {
        eep_ram_image.TachoLimitValue = TachoLimitValue;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_TachoLimitValue_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_TachoLimitValue_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_TachoLimitValue_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_TachoLimitValue_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:TachoLimitValue=%d",TachoLimitValue);
}

static void EepCopy_TachoLimitValue_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.TachoLimitValue = eep_ram_image.TachoLimitValue;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_TachoLimitValue_IN_BLOCK_NUM);
}
#endif
//static void Proc_TachoLimitValue_RestoreToDefault(void)
//{
//#if EEP_CONTENT_TachoLimitValue_CONST_VARIBLE
//	GenEep_Set_TachoLimitValue(dflt_eep_val.TachoLimitValue);
//#endif	
//}
#if EEP_CONTENT_TachoLimitValue_CONST_VARIBLE
#define Proc_TachoLimitValue_RestoreToDefault()	do{GenEep_Set_TachoLimitValue(dflt_eep_val.TachoLimitValue);}while(0)
#else
#define Proc_TachoLimitValue_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> TachoLimitValue function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> TachoUDeltaLimit function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_TachoUDeltaLimit_ITEM_LEN > 1
void GenEep_Get_TachoUDeltaLimit(uint16_t * const TachoUDeltaLimit )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_TachoUDeltaLimit_ITEM_LEN; i ++)
    {
        TachoUDeltaLimit[i] = eep_ram_image.TachoUDeltaLimit[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_TachoUDeltaLimit_Ex(uint8_t * const TachoUDeltaLimit,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.TachoUDeltaLimit;	//EEP_CONTENT_TachoUDeltaLimit_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_TachoUDeltaLimit_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_TachoUDeltaLimit_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        TachoUDeltaLimit[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_TachoUDeltaLimit(uint16_t const* TachoUDeltaLimit )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_TachoUDeltaLimit_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.TachoUDeltaLimit[i] != TachoUDeltaLimit[i] )
        {
            eep_ram_image.TachoUDeltaLimit[i] = TachoUDeltaLimit[i];
            wr_eep_by_list_req[EEP_CONTENT_TachoUDeltaLimit_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_TachoUDeltaLimit_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_TachoUDeltaLimit_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_TachoUDeltaLimit_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:TachoUDeltaLimit firt item[0] = %d",TachoUDeltaLimit[0]);
}

void GenEep_Set_TachoUDeltaLimit_Ex(uint8_t const* TachoUDeltaLimit,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_TachoUDeltaLimit_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_TachoUDeltaLimit_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.TachoUDeltaLimit;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != TachoUDeltaLimit[i] )
        {
            dest[offset+i] = TachoUDeltaLimit[i];
            wr_eep_by_list_req[EEP_CONTENT_TachoUDeltaLimit_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_TachoUDeltaLimit_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_TachoUDeltaLimit_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_TachoUDeltaLimit_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:TachoUDeltaLimit by Extend Api");
}

static void EepCopy_TachoUDeltaLimit_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_TachoUDeltaLimit_ITEM_LEN; i ++)
    {
        eep_hw_image.TachoUDeltaLimit[i] = eep_ram_image.TachoUDeltaLimit[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_TachoUDeltaLimit_IN_BLOCK_NUM);
}

#else 
uint16_t GenEep_Get_TachoUDeltaLimit(void)
{
    uint16_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.TachoUDeltaLimit;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_TachoUDeltaLimit(uint16_t const TachoUDeltaLimit)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.TachoUDeltaLimit != TachoUDeltaLimit )
    {
        eep_ram_image.TachoUDeltaLimit = TachoUDeltaLimit;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_TachoUDeltaLimit_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_TachoUDeltaLimit_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_TachoUDeltaLimit_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_TachoUDeltaLimit_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:TachoUDeltaLimit=%d",TachoUDeltaLimit);
}

static void EepCopy_TachoUDeltaLimit_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.TachoUDeltaLimit = eep_ram_image.TachoUDeltaLimit;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_TachoUDeltaLimit_IN_BLOCK_NUM);
}
#endif
//static void Proc_TachoUDeltaLimit_RestoreToDefault(void)
//{
//#if EEP_CONTENT_TachoUDeltaLimit_CONST_VARIBLE
//	GenEep_Set_TachoUDeltaLimit(dflt_eep_val.TachoUDeltaLimit);
//#endif	
//}
#if EEP_CONTENT_TachoUDeltaLimit_CONST_VARIBLE
#define Proc_TachoUDeltaLimit_RestoreToDefault()	do{GenEep_Set_TachoUDeltaLimit(dflt_eep_val.TachoUDeltaLimit);}while(0)
#else
#define Proc_TachoUDeltaLimit_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> TachoUDeltaLimit function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> TachoLDeltaLimit function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_TachoLDeltaLimit_ITEM_LEN > 1
void GenEep_Get_TachoLDeltaLimit(uint16_t * const TachoLDeltaLimit )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_TachoLDeltaLimit_ITEM_LEN; i ++)
    {
        TachoLDeltaLimit[i] = eep_ram_image.TachoLDeltaLimit[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_TachoLDeltaLimit_Ex(uint8_t * const TachoLDeltaLimit,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.TachoLDeltaLimit;	//EEP_CONTENT_TachoLDeltaLimit_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_TachoLDeltaLimit_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_TachoLDeltaLimit_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        TachoLDeltaLimit[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_TachoLDeltaLimit(uint16_t const* TachoLDeltaLimit )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_TachoLDeltaLimit_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.TachoLDeltaLimit[i] != TachoLDeltaLimit[i] )
        {
            eep_ram_image.TachoLDeltaLimit[i] = TachoLDeltaLimit[i];
            wr_eep_by_list_req[EEP_CONTENT_TachoLDeltaLimit_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_TachoLDeltaLimit_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_TachoLDeltaLimit_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_TachoLDeltaLimit_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:TachoLDeltaLimit firt item[0] = %d",TachoLDeltaLimit[0]);
}

void GenEep_Set_TachoLDeltaLimit_Ex(uint8_t const* TachoLDeltaLimit,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_TachoLDeltaLimit_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_TachoLDeltaLimit_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.TachoLDeltaLimit;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != TachoLDeltaLimit[i] )
        {
            dest[offset+i] = TachoLDeltaLimit[i];
            wr_eep_by_list_req[EEP_CONTENT_TachoLDeltaLimit_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_TachoLDeltaLimit_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_TachoLDeltaLimit_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_TachoLDeltaLimit_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:TachoLDeltaLimit by Extend Api");
}

static void EepCopy_TachoLDeltaLimit_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_TachoLDeltaLimit_ITEM_LEN; i ++)
    {
        eep_hw_image.TachoLDeltaLimit[i] = eep_ram_image.TachoLDeltaLimit[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_TachoLDeltaLimit_IN_BLOCK_NUM);
}

#else 
uint16_t GenEep_Get_TachoLDeltaLimit(void)
{
    uint16_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.TachoLDeltaLimit;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_TachoLDeltaLimit(uint16_t const TachoLDeltaLimit)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.TachoLDeltaLimit != TachoLDeltaLimit )
    {
        eep_ram_image.TachoLDeltaLimit = TachoLDeltaLimit;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_TachoLDeltaLimit_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_TachoLDeltaLimit_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_TachoLDeltaLimit_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_TachoLDeltaLimit_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:TachoLDeltaLimit=%d",TachoLDeltaLimit);
}

static void EepCopy_TachoLDeltaLimit_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.TachoLDeltaLimit = eep_ram_image.TachoLDeltaLimit;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_TachoLDeltaLimit_IN_BLOCK_NUM);
}
#endif
//static void Proc_TachoLDeltaLimit_RestoreToDefault(void)
//{
//#if EEP_CONTENT_TachoLDeltaLimit_CONST_VARIBLE
//	GenEep_Set_TachoLDeltaLimit(dflt_eep_val.TachoLDeltaLimit);
//#endif	
//}
#if EEP_CONTENT_TachoLDeltaLimit_CONST_VARIBLE
#define Proc_TachoLDeltaLimit_RestoreToDefault()	do{GenEep_Set_TachoLDeltaLimit(dflt_eep_val.TachoLDeltaLimit);}while(0)
#else
#define Proc_TachoLDeltaLimit_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> TachoLDeltaLimit function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> TachoUpChangeValue function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_TachoUpChangeValue_ITEM_LEN > 1
void GenEep_Get_TachoUpChangeValue(uint16_t * const TachoUpChangeValue )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_TachoUpChangeValue_ITEM_LEN; i ++)
    {
        TachoUpChangeValue[i] = eep_ram_image.TachoUpChangeValue[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_TachoUpChangeValue_Ex(uint8_t * const TachoUpChangeValue,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.TachoUpChangeValue;	//EEP_CONTENT_TachoUpChangeValue_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_TachoUpChangeValue_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_TachoUpChangeValue_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        TachoUpChangeValue[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_TachoUpChangeValue(uint16_t const* TachoUpChangeValue )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_TachoUpChangeValue_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.TachoUpChangeValue[i] != TachoUpChangeValue[i] )
        {
            eep_ram_image.TachoUpChangeValue[i] = TachoUpChangeValue[i];
            wr_eep_by_list_req[EEP_CONTENT_TachoUpChangeValue_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_TachoUpChangeValue_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_TachoUpChangeValue_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_TachoUpChangeValue_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:TachoUpChangeValue firt item[0] = %d",TachoUpChangeValue[0]);
}

void GenEep_Set_TachoUpChangeValue_Ex(uint8_t const* TachoUpChangeValue,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_TachoUpChangeValue_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_TachoUpChangeValue_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.TachoUpChangeValue;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != TachoUpChangeValue[i] )
        {
            dest[offset+i] = TachoUpChangeValue[i];
            wr_eep_by_list_req[EEP_CONTENT_TachoUpChangeValue_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_TachoUpChangeValue_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_TachoUpChangeValue_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_TachoUpChangeValue_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:TachoUpChangeValue by Extend Api");
}

static void EepCopy_TachoUpChangeValue_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_TachoUpChangeValue_ITEM_LEN; i ++)
    {
        eep_hw_image.TachoUpChangeValue[i] = eep_ram_image.TachoUpChangeValue[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_TachoUpChangeValue_IN_BLOCK_NUM);
}

#else 
uint16_t GenEep_Get_TachoUpChangeValue(void)
{
    uint16_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.TachoUpChangeValue;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_TachoUpChangeValue(uint16_t const TachoUpChangeValue)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.TachoUpChangeValue != TachoUpChangeValue )
    {
        eep_ram_image.TachoUpChangeValue = TachoUpChangeValue;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_TachoUpChangeValue_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_TachoUpChangeValue_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_TachoUpChangeValue_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_TachoUpChangeValue_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:TachoUpChangeValue=%d",TachoUpChangeValue);
}

static void EepCopy_TachoUpChangeValue_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.TachoUpChangeValue = eep_ram_image.TachoUpChangeValue;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_TachoUpChangeValue_IN_BLOCK_NUM);
}
#endif
//static void Proc_TachoUpChangeValue_RestoreToDefault(void)
//{
//#if EEP_CONTENT_TachoUpChangeValue_CONST_VARIBLE
//	GenEep_Set_TachoUpChangeValue(dflt_eep_val.TachoUpChangeValue);
//#endif	
//}
#if EEP_CONTENT_TachoUpChangeValue_CONST_VARIBLE
#define Proc_TachoUpChangeValue_RestoreToDefault()	do{GenEep_Set_TachoUpChangeValue(dflt_eep_val.TachoUpChangeValue);}while(0)
#else
#define Proc_TachoUpChangeValue_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> TachoUpChangeValue function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> TachoLowChangeValue function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_TachoLowChangeValue_ITEM_LEN > 1
void GenEep_Get_TachoLowChangeValue(uint16_t * const TachoLowChangeValue )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_TachoLowChangeValue_ITEM_LEN; i ++)
    {
        TachoLowChangeValue[i] = eep_ram_image.TachoLowChangeValue[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_TachoLowChangeValue_Ex(uint8_t * const TachoLowChangeValue,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.TachoLowChangeValue;	//EEP_CONTENT_TachoLowChangeValue_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_TachoLowChangeValue_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_TachoLowChangeValue_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        TachoLowChangeValue[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_TachoLowChangeValue(uint16_t const* TachoLowChangeValue )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_TachoLowChangeValue_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.TachoLowChangeValue[i] != TachoLowChangeValue[i] )
        {
            eep_ram_image.TachoLowChangeValue[i] = TachoLowChangeValue[i];
            wr_eep_by_list_req[EEP_CONTENT_TachoLowChangeValue_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_TachoLowChangeValue_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_TachoLowChangeValue_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_TachoLowChangeValue_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:TachoLowChangeValue firt item[0] = %d",TachoLowChangeValue[0]);
}

void GenEep_Set_TachoLowChangeValue_Ex(uint8_t const* TachoLowChangeValue,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_TachoLowChangeValue_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_TachoLowChangeValue_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.TachoLowChangeValue;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != TachoLowChangeValue[i] )
        {
            dest[offset+i] = TachoLowChangeValue[i];
            wr_eep_by_list_req[EEP_CONTENT_TachoLowChangeValue_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_TachoLowChangeValue_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_TachoLowChangeValue_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_TachoLowChangeValue_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:TachoLowChangeValue by Extend Api");
}

static void EepCopy_TachoLowChangeValue_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_TachoLowChangeValue_ITEM_LEN; i ++)
    {
        eep_hw_image.TachoLowChangeValue[i] = eep_ram_image.TachoLowChangeValue[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_TachoLowChangeValue_IN_BLOCK_NUM);
}

#else 
uint16_t GenEep_Get_TachoLowChangeValue(void)
{
    uint16_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.TachoLowChangeValue;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_TachoLowChangeValue(uint16_t const TachoLowChangeValue)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.TachoLowChangeValue != TachoLowChangeValue )
    {
        eep_ram_image.TachoLowChangeValue = TachoLowChangeValue;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_TachoLowChangeValue_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_TachoLowChangeValue_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_TachoLowChangeValue_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_TachoLowChangeValue_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:TachoLowChangeValue=%d",TachoLowChangeValue);
}

static void EepCopy_TachoLowChangeValue_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.TachoLowChangeValue = eep_ram_image.TachoLowChangeValue;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_TachoLowChangeValue_IN_BLOCK_NUM);
}
#endif
//static void Proc_TachoLowChangeValue_RestoreToDefault(void)
//{
//#if EEP_CONTENT_TachoLowChangeValue_CONST_VARIBLE
//	GenEep_Set_TachoLowChangeValue(dflt_eep_val.TachoLowChangeValue);
//#endif	
//}
#if EEP_CONTENT_TachoLowChangeValue_CONST_VARIBLE
#define Proc_TachoLowChangeValue_RestoreToDefault()	do{GenEep_Set_TachoLowChangeValue(dflt_eep_val.TachoLowChangeValue);}while(0)
#else
#define Proc_TachoLowChangeValue_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> TachoLowChangeValue function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> TachoRun function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_TachoRun_ITEM_LEN > 1
void GenEep_Get_TachoRun(uint16_t * const TachoRun )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_TachoRun_ITEM_LEN; i ++)
    {
        TachoRun[i] = eep_ram_image.TachoRun[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_TachoRun_Ex(uint8_t * const TachoRun,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.TachoRun;	//EEP_CONTENT_TachoRun_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_TachoRun_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_TachoRun_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        TachoRun[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_TachoRun(uint16_t const* TachoRun )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_TachoRun_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.TachoRun[i] != TachoRun[i] )
        {
            eep_ram_image.TachoRun[i] = TachoRun[i];
            wr_eep_by_list_req[EEP_CONTENT_TachoRun_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_TachoRun_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_TachoRun_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_TachoRun_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:TachoRun firt item[0] = %d",TachoRun[0]);
}

void GenEep_Set_TachoRun_Ex(uint8_t const* TachoRun,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_TachoRun_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_TachoRun_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.TachoRun;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != TachoRun[i] )
        {
            dest[offset+i] = TachoRun[i];
            wr_eep_by_list_req[EEP_CONTENT_TachoRun_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_TachoRun_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_TachoRun_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_TachoRun_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:TachoRun by Extend Api");
}

static void EepCopy_TachoRun_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_TachoRun_ITEM_LEN; i ++)
    {
        eep_hw_image.TachoRun[i] = eep_ram_image.TachoRun[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_TachoRun_IN_BLOCK_NUM);
}

#else 
uint16_t GenEep_Get_TachoRun(void)
{
    uint16_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.TachoRun;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_TachoRun(uint16_t const TachoRun)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.TachoRun != TachoRun )
    {
        eep_ram_image.TachoRun = TachoRun;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_TachoRun_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_TachoRun_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_TachoRun_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_TachoRun_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:TachoRun=%d",TachoRun);
}

static void EepCopy_TachoRun_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.TachoRun = eep_ram_image.TachoRun;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_TachoRun_IN_BLOCK_NUM);
}
#endif
//static void Proc_TachoRun_RestoreToDefault(void)
//{
//#if EEP_CONTENT_TachoRun_CONST_VARIBLE
//	GenEep_Set_TachoRun(dflt_eep_val.TachoRun);
//#endif	
//}
#if EEP_CONTENT_TachoRun_CONST_VARIBLE
#define Proc_TachoRun_RestoreToDefault()	do{GenEep_Set_TachoRun(dflt_eep_val.TachoRun);}while(0)
#else
#define Proc_TachoRun_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> TachoRun function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> Speed_Rm_1 function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_Speed_Rm_1_ITEM_LEN > 1
void GenEep_Get_Speed_Rm_1(uint16_t * const Speed_Rm_1 )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_Speed_Rm_1_ITEM_LEN; i ++)
    {
        Speed_Rm_1[i] = eep_ram_image.Speed_Rm_1[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_Speed_Rm_1_Ex(uint8_t * const Speed_Rm_1,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.Speed_Rm_1;	//EEP_CONTENT_Speed_Rm_1_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_Speed_Rm_1_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_Speed_Rm_1_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        Speed_Rm_1[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_Speed_Rm_1(uint16_t const* Speed_Rm_1 )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_Speed_Rm_1_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.Speed_Rm_1[i] != Speed_Rm_1[i] )
        {
            eep_ram_image.Speed_Rm_1[i] = Speed_Rm_1[i];
            wr_eep_by_list_req[EEP_CONTENT_Speed_Rm_1_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_Speed_Rm_1_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_Speed_Rm_1_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_Speed_Rm_1_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:Speed_Rm_1 firt item[0] = %d",Speed_Rm_1[0]);
}

void GenEep_Set_Speed_Rm_1_Ex(uint8_t const* Speed_Rm_1,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_Speed_Rm_1_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_Speed_Rm_1_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.Speed_Rm_1;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != Speed_Rm_1[i] )
        {
            dest[offset+i] = Speed_Rm_1[i];
            wr_eep_by_list_req[EEP_CONTENT_Speed_Rm_1_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_Speed_Rm_1_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_Speed_Rm_1_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_Speed_Rm_1_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:Speed_Rm_1 by Extend Api");
}

static void EepCopy_Speed_Rm_1_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_Speed_Rm_1_ITEM_LEN; i ++)
    {
        eep_hw_image.Speed_Rm_1[i] = eep_ram_image.Speed_Rm_1[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_Speed_Rm_1_IN_BLOCK_NUM);
}

#else 
uint16_t GenEep_Get_Speed_Rm_1(void)
{
    uint16_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.Speed_Rm_1;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_Speed_Rm_1(uint16_t const Speed_Rm_1)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.Speed_Rm_1 != Speed_Rm_1 )
    {
        eep_ram_image.Speed_Rm_1 = Speed_Rm_1;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_Speed_Rm_1_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_Speed_Rm_1_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_Speed_Rm_1_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_Speed_Rm_1_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:Speed_Rm_1=%d",Speed_Rm_1);
}

static void EepCopy_Speed_Rm_1_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.Speed_Rm_1 = eep_ram_image.Speed_Rm_1;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_Speed_Rm_1_IN_BLOCK_NUM);
}
#endif
//static void Proc_Speed_Rm_1_RestoreToDefault(void)
//{
//#if EEP_CONTENT_Speed_Rm_1_CONST_VARIBLE
//	GenEep_Set_Speed_Rm_1(dflt_eep_val.Speed_Rm_1);
//#endif	
//}
#if EEP_CONTENT_Speed_Rm_1_CONST_VARIBLE
#define Proc_Speed_Rm_1_RestoreToDefault()	do{GenEep_Set_Speed_Rm_1(dflt_eep_val.Speed_Rm_1);}while(0)
#else
#define Proc_Speed_Rm_1_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> Speed_Rm_1 function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> Speed_Rm_2 function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_Speed_Rm_2_ITEM_LEN > 1
void GenEep_Get_Speed_Rm_2(uint16_t * const Speed_Rm_2 )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_Speed_Rm_2_ITEM_LEN; i ++)
    {
        Speed_Rm_2[i] = eep_ram_image.Speed_Rm_2[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_Speed_Rm_2_Ex(uint8_t * const Speed_Rm_2,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.Speed_Rm_2;	//EEP_CONTENT_Speed_Rm_2_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_Speed_Rm_2_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_Speed_Rm_2_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        Speed_Rm_2[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_Speed_Rm_2(uint16_t const* Speed_Rm_2 )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_Speed_Rm_2_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.Speed_Rm_2[i] != Speed_Rm_2[i] )
        {
            eep_ram_image.Speed_Rm_2[i] = Speed_Rm_2[i];
            wr_eep_by_list_req[EEP_CONTENT_Speed_Rm_2_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_Speed_Rm_2_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_Speed_Rm_2_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_Speed_Rm_2_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:Speed_Rm_2 firt item[0] = %d",Speed_Rm_2[0]);
}

void GenEep_Set_Speed_Rm_2_Ex(uint8_t const* Speed_Rm_2,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_Speed_Rm_2_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_Speed_Rm_2_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.Speed_Rm_2;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != Speed_Rm_2[i] )
        {
            dest[offset+i] = Speed_Rm_2[i];
            wr_eep_by_list_req[EEP_CONTENT_Speed_Rm_2_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_Speed_Rm_2_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_Speed_Rm_2_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_Speed_Rm_2_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:Speed_Rm_2 by Extend Api");
}

static void EepCopy_Speed_Rm_2_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_Speed_Rm_2_ITEM_LEN; i ++)
    {
        eep_hw_image.Speed_Rm_2[i] = eep_ram_image.Speed_Rm_2[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_Speed_Rm_2_IN_BLOCK_NUM);
}

#else 
uint16_t GenEep_Get_Speed_Rm_2(void)
{
    uint16_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.Speed_Rm_2;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_Speed_Rm_2(uint16_t const Speed_Rm_2)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.Speed_Rm_2 != Speed_Rm_2 )
    {
        eep_ram_image.Speed_Rm_2 = Speed_Rm_2;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_Speed_Rm_2_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_Speed_Rm_2_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_Speed_Rm_2_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_Speed_Rm_2_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:Speed_Rm_2=%d",Speed_Rm_2);
}

static void EepCopy_Speed_Rm_2_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.Speed_Rm_2 = eep_ram_image.Speed_Rm_2;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_Speed_Rm_2_IN_BLOCK_NUM);
}
#endif
//static void Proc_Speed_Rm_2_RestoreToDefault(void)
//{
//#if EEP_CONTENT_Speed_Rm_2_CONST_VARIBLE
//	GenEep_Set_Speed_Rm_2(dflt_eep_val.Speed_Rm_2);
//#endif	
//}
#if EEP_CONTENT_Speed_Rm_2_CONST_VARIBLE
#define Proc_Speed_Rm_2_RestoreToDefault()	do{GenEep_Set_Speed_Rm_2(dflt_eep_val.Speed_Rm_2);}while(0)
#else
#define Proc_Speed_Rm_2_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> Speed_Rm_2 function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> Speed_Rc function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_Speed_Rc_ITEM_LEN > 1
void GenEep_Get_Speed_Rc(uint16_t * const Speed_Rc )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_Speed_Rc_ITEM_LEN; i ++)
    {
        Speed_Rc[i] = eep_ram_image.Speed_Rc[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_Speed_Rc_Ex(uint8_t * const Speed_Rc,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.Speed_Rc;	//EEP_CONTENT_Speed_Rc_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_Speed_Rc_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_Speed_Rc_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        Speed_Rc[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_Speed_Rc(uint16_t const* Speed_Rc )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_Speed_Rc_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.Speed_Rc[i] != Speed_Rc[i] )
        {
            eep_ram_image.Speed_Rc[i] = Speed_Rc[i];
            wr_eep_by_list_req[EEP_CONTENT_Speed_Rc_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_Speed_Rc_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_Speed_Rc_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_Speed_Rc_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:Speed_Rc firt item[0] = %d",Speed_Rc[0]);
}

void GenEep_Set_Speed_Rc_Ex(uint8_t const* Speed_Rc,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_Speed_Rc_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_Speed_Rc_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.Speed_Rc;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != Speed_Rc[i] )
        {
            dest[offset+i] = Speed_Rc[i];
            wr_eep_by_list_req[EEP_CONTENT_Speed_Rc_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_Speed_Rc_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_Speed_Rc_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_Speed_Rc_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:Speed_Rc by Extend Api");
}

static void EepCopy_Speed_Rc_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_Speed_Rc_ITEM_LEN; i ++)
    {
        eep_hw_image.Speed_Rc[i] = eep_ram_image.Speed_Rc[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_Speed_Rc_IN_BLOCK_NUM);
}

#else 
uint16_t GenEep_Get_Speed_Rc(void)
{
    uint16_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.Speed_Rc;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_Speed_Rc(uint16_t const Speed_Rc)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.Speed_Rc != Speed_Rc )
    {
        eep_ram_image.Speed_Rc = Speed_Rc;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_Speed_Rc_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_Speed_Rc_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_Speed_Rc_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_Speed_Rc_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:Speed_Rc=%d",Speed_Rc);
}

static void EepCopy_Speed_Rc_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.Speed_Rc = eep_ram_image.Speed_Rc;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_Speed_Rc_IN_BLOCK_NUM);
}
#endif
//static void Proc_Speed_Rc_RestoreToDefault(void)
//{
//#if EEP_CONTENT_Speed_Rc_CONST_VARIBLE
//	GenEep_Set_Speed_Rc(dflt_eep_val.Speed_Rc);
//#endif	
//}
#if EEP_CONTENT_Speed_Rc_CONST_VARIBLE
#define Proc_Speed_Rc_RestoreToDefault()	do{GenEep_Set_Speed_Rc(dflt_eep_val.Speed_Rc);}while(0)
#else
#define Proc_Speed_Rc_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> Speed_Rc function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> FuelTelltaleOn_R function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_FuelTelltaleOn_R_ITEM_LEN > 1
void GenEep_Get_FuelTelltaleOn_R(uint16_t * const FuelTelltaleOn_R )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_FuelTelltaleOn_R_ITEM_LEN; i ++)
    {
        FuelTelltaleOn_R[i] = eep_ram_image.FuelTelltaleOn_R[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_FuelTelltaleOn_R_Ex(uint8_t * const FuelTelltaleOn_R,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.FuelTelltaleOn_R;	//EEP_CONTENT_FuelTelltaleOn_R_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_FuelTelltaleOn_R_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_FuelTelltaleOn_R_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        FuelTelltaleOn_R[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_FuelTelltaleOn_R(uint16_t const* FuelTelltaleOn_R )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_FuelTelltaleOn_R_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.FuelTelltaleOn_R[i] != FuelTelltaleOn_R[i] )
        {
            eep_ram_image.FuelTelltaleOn_R[i] = FuelTelltaleOn_R[i];
            wr_eep_by_list_req[EEP_CONTENT_FuelTelltaleOn_R_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_FuelTelltaleOn_R_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_FuelTelltaleOn_R_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_FuelTelltaleOn_R_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:FuelTelltaleOn_R firt item[0] = %d",FuelTelltaleOn_R[0]);
}

void GenEep_Set_FuelTelltaleOn_R_Ex(uint8_t const* FuelTelltaleOn_R,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_FuelTelltaleOn_R_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_FuelTelltaleOn_R_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.FuelTelltaleOn_R;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != FuelTelltaleOn_R[i] )
        {
            dest[offset+i] = FuelTelltaleOn_R[i];
            wr_eep_by_list_req[EEP_CONTENT_FuelTelltaleOn_R_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_FuelTelltaleOn_R_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_FuelTelltaleOn_R_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_FuelTelltaleOn_R_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:FuelTelltaleOn_R by Extend Api");
}

static void EepCopy_FuelTelltaleOn_R_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_FuelTelltaleOn_R_ITEM_LEN; i ++)
    {
        eep_hw_image.FuelTelltaleOn_R[i] = eep_ram_image.FuelTelltaleOn_R[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_FuelTelltaleOn_R_IN_BLOCK_NUM);
}

#else 
uint16_t GenEep_Get_FuelTelltaleOn_R(void)
{
    uint16_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.FuelTelltaleOn_R;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_FuelTelltaleOn_R(uint16_t const FuelTelltaleOn_R)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.FuelTelltaleOn_R != FuelTelltaleOn_R )
    {
        eep_ram_image.FuelTelltaleOn_R = FuelTelltaleOn_R;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_FuelTelltaleOn_R_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_FuelTelltaleOn_R_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_FuelTelltaleOn_R_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_FuelTelltaleOn_R_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:FuelTelltaleOn_R=%d",FuelTelltaleOn_R);
}

static void EepCopy_FuelTelltaleOn_R_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.FuelTelltaleOn_R = eep_ram_image.FuelTelltaleOn_R;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_FuelTelltaleOn_R_IN_BLOCK_NUM);
}
#endif
//static void Proc_FuelTelltaleOn_R_RestoreToDefault(void)
//{
//#if EEP_CONTENT_FuelTelltaleOn_R_CONST_VARIBLE
//	GenEep_Set_FuelTelltaleOn_R(dflt_eep_val.FuelTelltaleOn_R);
//#endif	
//}
#if EEP_CONTENT_FuelTelltaleOn_R_CONST_VARIBLE
#define Proc_FuelTelltaleOn_R_RestoreToDefault()	do{GenEep_Set_FuelTelltaleOn_R(dflt_eep_val.FuelTelltaleOn_R);}while(0)
#else
#define Proc_FuelTelltaleOn_R_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> FuelTelltaleOn_R function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> FuelTelltaleOff_R function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_FuelTelltaleOff_R_ITEM_LEN > 1
void GenEep_Get_FuelTelltaleOff_R(uint16_t * const FuelTelltaleOff_R )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_FuelTelltaleOff_R_ITEM_LEN; i ++)
    {
        FuelTelltaleOff_R[i] = eep_ram_image.FuelTelltaleOff_R[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_FuelTelltaleOff_R_Ex(uint8_t * const FuelTelltaleOff_R,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.FuelTelltaleOff_R;	//EEP_CONTENT_FuelTelltaleOff_R_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_FuelTelltaleOff_R_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_FuelTelltaleOff_R_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        FuelTelltaleOff_R[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_FuelTelltaleOff_R(uint16_t const* FuelTelltaleOff_R )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_FuelTelltaleOff_R_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.FuelTelltaleOff_R[i] != FuelTelltaleOff_R[i] )
        {
            eep_ram_image.FuelTelltaleOff_R[i] = FuelTelltaleOff_R[i];
            wr_eep_by_list_req[EEP_CONTENT_FuelTelltaleOff_R_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_FuelTelltaleOff_R_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_FuelTelltaleOff_R_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_FuelTelltaleOff_R_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:FuelTelltaleOff_R firt item[0] = %d",FuelTelltaleOff_R[0]);
}

void GenEep_Set_FuelTelltaleOff_R_Ex(uint8_t const* FuelTelltaleOff_R,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_FuelTelltaleOff_R_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_FuelTelltaleOff_R_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.FuelTelltaleOff_R;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != FuelTelltaleOff_R[i] )
        {
            dest[offset+i] = FuelTelltaleOff_R[i];
            wr_eep_by_list_req[EEP_CONTENT_FuelTelltaleOff_R_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_FuelTelltaleOff_R_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_FuelTelltaleOff_R_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_FuelTelltaleOff_R_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:FuelTelltaleOff_R by Extend Api");
}

static void EepCopy_FuelTelltaleOff_R_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_FuelTelltaleOff_R_ITEM_LEN; i ++)
    {
        eep_hw_image.FuelTelltaleOff_R[i] = eep_ram_image.FuelTelltaleOff_R[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_FuelTelltaleOff_R_IN_BLOCK_NUM);
}

#else 
uint16_t GenEep_Get_FuelTelltaleOff_R(void)
{
    uint16_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.FuelTelltaleOff_R;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_FuelTelltaleOff_R(uint16_t const FuelTelltaleOff_R)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.FuelTelltaleOff_R != FuelTelltaleOff_R )
    {
        eep_ram_image.FuelTelltaleOff_R = FuelTelltaleOff_R;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_FuelTelltaleOff_R_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_FuelTelltaleOff_R_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_FuelTelltaleOff_R_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_FuelTelltaleOff_R_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:FuelTelltaleOff_R=%d",FuelTelltaleOff_R);
}

static void EepCopy_FuelTelltaleOff_R_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.FuelTelltaleOff_R = eep_ram_image.FuelTelltaleOff_R;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_FuelTelltaleOff_R_IN_BLOCK_NUM);
}
#endif
//static void Proc_FuelTelltaleOff_R_RestoreToDefault(void)
//{
//#if EEP_CONTENT_FuelTelltaleOff_R_CONST_VARIBLE
//	GenEep_Set_FuelTelltaleOff_R(dflt_eep_val.FuelTelltaleOff_R);
//#endif	
//}
#if EEP_CONTENT_FuelTelltaleOff_R_CONST_VARIBLE
#define Proc_FuelTelltaleOff_R_RestoreToDefault()	do{GenEep_Set_FuelTelltaleOff_R(dflt_eep_val.FuelTelltaleOff_R);}while(0)
#else
#define Proc_FuelTelltaleOff_R_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> FuelTelltaleOff_R function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> FuelWarningOnPoint_R function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_FuelWarningOnPoint_R_ITEM_LEN > 1
void GenEep_Get_FuelWarningOnPoint_R(uint16_t * const FuelWarningOnPoint_R )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_FuelWarningOnPoint_R_ITEM_LEN; i ++)
    {
        FuelWarningOnPoint_R[i] = eep_ram_image.FuelWarningOnPoint_R[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_FuelWarningOnPoint_R_Ex(uint8_t * const FuelWarningOnPoint_R,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.FuelWarningOnPoint_R;	//EEP_CONTENT_FuelWarningOnPoint_R_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_FuelWarningOnPoint_R_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_FuelWarningOnPoint_R_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        FuelWarningOnPoint_R[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_FuelWarningOnPoint_R(uint16_t const* FuelWarningOnPoint_R )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_FuelWarningOnPoint_R_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.FuelWarningOnPoint_R[i] != FuelWarningOnPoint_R[i] )
        {
            eep_ram_image.FuelWarningOnPoint_R[i] = FuelWarningOnPoint_R[i];
            wr_eep_by_list_req[EEP_CONTENT_FuelWarningOnPoint_R_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_FuelWarningOnPoint_R_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_FuelWarningOnPoint_R_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_FuelWarningOnPoint_R_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:FuelWarningOnPoint_R firt item[0] = %d",FuelWarningOnPoint_R[0]);
}

void GenEep_Set_FuelWarningOnPoint_R_Ex(uint8_t const* FuelWarningOnPoint_R,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_FuelWarningOnPoint_R_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_FuelWarningOnPoint_R_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.FuelWarningOnPoint_R;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != FuelWarningOnPoint_R[i] )
        {
            dest[offset+i] = FuelWarningOnPoint_R[i];
            wr_eep_by_list_req[EEP_CONTENT_FuelWarningOnPoint_R_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_FuelWarningOnPoint_R_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_FuelWarningOnPoint_R_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_FuelWarningOnPoint_R_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:FuelWarningOnPoint_R by Extend Api");
}

static void EepCopy_FuelWarningOnPoint_R_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_FuelWarningOnPoint_R_ITEM_LEN; i ++)
    {
        eep_hw_image.FuelWarningOnPoint_R[i] = eep_ram_image.FuelWarningOnPoint_R[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_FuelWarningOnPoint_R_IN_BLOCK_NUM);
}

#else 
uint16_t GenEep_Get_FuelWarningOnPoint_R(void)
{
    uint16_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.FuelWarningOnPoint_R;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_FuelWarningOnPoint_R(uint16_t const FuelWarningOnPoint_R)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.FuelWarningOnPoint_R != FuelWarningOnPoint_R )
    {
        eep_ram_image.FuelWarningOnPoint_R = FuelWarningOnPoint_R;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_FuelWarningOnPoint_R_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_FuelWarningOnPoint_R_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_FuelWarningOnPoint_R_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_FuelWarningOnPoint_R_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:FuelWarningOnPoint_R=%d",FuelWarningOnPoint_R);
}

static void EepCopy_FuelWarningOnPoint_R_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.FuelWarningOnPoint_R = eep_ram_image.FuelWarningOnPoint_R;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_FuelWarningOnPoint_R_IN_BLOCK_NUM);
}
#endif
//static void Proc_FuelWarningOnPoint_R_RestoreToDefault(void)
//{
//#if EEP_CONTENT_FuelWarningOnPoint_R_CONST_VARIBLE
//	GenEep_Set_FuelWarningOnPoint_R(dflt_eep_val.FuelWarningOnPoint_R);
//#endif	
//}
#if EEP_CONTENT_FuelWarningOnPoint_R_CONST_VARIBLE
#define Proc_FuelWarningOnPoint_R_RestoreToDefault()	do{GenEep_Set_FuelWarningOnPoint_R(dflt_eep_val.FuelWarningOnPoint_R);}while(0)
#else
#define Proc_FuelWarningOnPoint_R_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> FuelWarningOnPoint_R function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> Fuel_R_Open function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_Fuel_R_Open_ITEM_LEN > 1
void GenEep_Get_Fuel_R_Open(uint16_t * const Fuel_R_Open )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_Fuel_R_Open_ITEM_LEN; i ++)
    {
        Fuel_R_Open[i] = eep_ram_image.Fuel_R_Open[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_Fuel_R_Open_Ex(uint8_t * const Fuel_R_Open,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.Fuel_R_Open;	//EEP_CONTENT_Fuel_R_Open_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_Fuel_R_Open_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_Fuel_R_Open_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        Fuel_R_Open[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_Fuel_R_Open(uint16_t const* Fuel_R_Open )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_Fuel_R_Open_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.Fuel_R_Open[i] != Fuel_R_Open[i] )
        {
            eep_ram_image.Fuel_R_Open[i] = Fuel_R_Open[i];
            wr_eep_by_list_req[EEP_CONTENT_Fuel_R_Open_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_Fuel_R_Open_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_Fuel_R_Open_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_Fuel_R_Open_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:Fuel_R_Open firt item[0] = %d",Fuel_R_Open[0]);
}

void GenEep_Set_Fuel_R_Open_Ex(uint8_t const* Fuel_R_Open,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_Fuel_R_Open_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_Fuel_R_Open_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.Fuel_R_Open;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != Fuel_R_Open[i] )
        {
            dest[offset+i] = Fuel_R_Open[i];
            wr_eep_by_list_req[EEP_CONTENT_Fuel_R_Open_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_Fuel_R_Open_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_Fuel_R_Open_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_Fuel_R_Open_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:Fuel_R_Open by Extend Api");
}

static void EepCopy_Fuel_R_Open_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_Fuel_R_Open_ITEM_LEN; i ++)
    {
        eep_hw_image.Fuel_R_Open[i] = eep_ram_image.Fuel_R_Open[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_Fuel_R_Open_IN_BLOCK_NUM);
}

#else 
uint16_t GenEep_Get_Fuel_R_Open(void)
{
    uint16_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.Fuel_R_Open;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_Fuel_R_Open(uint16_t const Fuel_R_Open)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.Fuel_R_Open != Fuel_R_Open )
    {
        eep_ram_image.Fuel_R_Open = Fuel_R_Open;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_Fuel_R_Open_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_Fuel_R_Open_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_Fuel_R_Open_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_Fuel_R_Open_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:Fuel_R_Open=%d",Fuel_R_Open);
}

static void EepCopy_Fuel_R_Open_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.Fuel_R_Open = eep_ram_image.Fuel_R_Open;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_Fuel_R_Open_IN_BLOCK_NUM);
}
#endif
//static void Proc_Fuel_R_Open_RestoreToDefault(void)
//{
//#if EEP_CONTENT_Fuel_R_Open_CONST_VARIBLE
//	GenEep_Set_Fuel_R_Open(dflt_eep_val.Fuel_R_Open);
//#endif	
//}
#if EEP_CONTENT_Fuel_R_Open_CONST_VARIBLE
#define Proc_Fuel_R_Open_RestoreToDefault()	do{GenEep_Set_Fuel_R_Open(dflt_eep_val.Fuel_R_Open);}while(0)
#else
#define Proc_Fuel_R_Open_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> Fuel_R_Open function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> Fuel_R_Short function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_Fuel_R_Short_ITEM_LEN > 1
void GenEep_Get_Fuel_R_Short(uint16_t * const Fuel_R_Short )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_Fuel_R_Short_ITEM_LEN; i ++)
    {
        Fuel_R_Short[i] = eep_ram_image.Fuel_R_Short[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_Fuel_R_Short_Ex(uint8_t * const Fuel_R_Short,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.Fuel_R_Short;	//EEP_CONTENT_Fuel_R_Short_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_Fuel_R_Short_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_Fuel_R_Short_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        Fuel_R_Short[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_Fuel_R_Short(uint16_t const* Fuel_R_Short )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_Fuel_R_Short_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.Fuel_R_Short[i] != Fuel_R_Short[i] )
        {
            eep_ram_image.Fuel_R_Short[i] = Fuel_R_Short[i];
            wr_eep_by_list_req[EEP_CONTENT_Fuel_R_Short_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_Fuel_R_Short_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_Fuel_R_Short_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_Fuel_R_Short_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:Fuel_R_Short firt item[0] = %d",Fuel_R_Short[0]);
}

void GenEep_Set_Fuel_R_Short_Ex(uint8_t const* Fuel_R_Short,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_Fuel_R_Short_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_Fuel_R_Short_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.Fuel_R_Short;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != Fuel_R_Short[i] )
        {
            dest[offset+i] = Fuel_R_Short[i];
            wr_eep_by_list_req[EEP_CONTENT_Fuel_R_Short_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_Fuel_R_Short_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_Fuel_R_Short_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_Fuel_R_Short_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:Fuel_R_Short by Extend Api");
}

static void EepCopy_Fuel_R_Short_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_Fuel_R_Short_ITEM_LEN; i ++)
    {
        eep_hw_image.Fuel_R_Short[i] = eep_ram_image.Fuel_R_Short[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_Fuel_R_Short_IN_BLOCK_NUM);
}

#else 
uint16_t GenEep_Get_Fuel_R_Short(void)
{
    uint16_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.Fuel_R_Short;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_Fuel_R_Short(uint16_t const Fuel_R_Short)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.Fuel_R_Short != Fuel_R_Short )
    {
        eep_ram_image.Fuel_R_Short = Fuel_R_Short;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_Fuel_R_Short_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_Fuel_R_Short_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_Fuel_R_Short_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_Fuel_R_Short_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:Fuel_R_Short=%d",Fuel_R_Short);
}

static void EepCopy_Fuel_R_Short_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.Fuel_R_Short = eep_ram_image.Fuel_R_Short;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_Fuel_R_Short_IN_BLOCK_NUM);
}
#endif
//static void Proc_Fuel_R_Short_RestoreToDefault(void)
//{
//#if EEP_CONTENT_Fuel_R_Short_CONST_VARIBLE
//	GenEep_Set_Fuel_R_Short(dflt_eep_val.Fuel_R_Short);
//#endif	
//}
#if EEP_CONTENT_Fuel_R_Short_CONST_VARIBLE
#define Proc_Fuel_R_Short_RestoreToDefault()	do{GenEep_Set_Fuel_R_Short(dflt_eep_val.Fuel_R_Short);}while(0)
#else
#define Proc_Fuel_R_Short_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> Fuel_R_Short function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> PkbWarningJudgeV1 function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_PkbWarningJudgeV1_ITEM_LEN > 1
void GenEep_Get_PkbWarningJudgeV1(uint8_t * const PkbWarningJudgeV1 )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_PkbWarningJudgeV1_ITEM_LEN; i ++)
    {
        PkbWarningJudgeV1[i] = eep_ram_image.PkbWarningJudgeV1[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_PkbWarningJudgeV1_Ex(uint8_t * const PkbWarningJudgeV1,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.PkbWarningJudgeV1;	//EEP_CONTENT_PkbWarningJudgeV1_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_PkbWarningJudgeV1_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_PkbWarningJudgeV1_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        PkbWarningJudgeV1[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_PkbWarningJudgeV1(uint8_t const* PkbWarningJudgeV1 )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_PkbWarningJudgeV1_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.PkbWarningJudgeV1[i] != PkbWarningJudgeV1[i] )
        {
            eep_ram_image.PkbWarningJudgeV1[i] = PkbWarningJudgeV1[i];
            wr_eep_by_list_req[EEP_CONTENT_PkbWarningJudgeV1_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_PkbWarningJudgeV1_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_PkbWarningJudgeV1_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_PkbWarningJudgeV1_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:PkbWarningJudgeV1 firt item[0] = %d",PkbWarningJudgeV1[0]);
}

void GenEep_Set_PkbWarningJudgeV1_Ex(uint8_t const* PkbWarningJudgeV1,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_PkbWarningJudgeV1_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_PkbWarningJudgeV1_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.PkbWarningJudgeV1;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != PkbWarningJudgeV1[i] )
        {
            dest[offset+i] = PkbWarningJudgeV1[i];
            wr_eep_by_list_req[EEP_CONTENT_PkbWarningJudgeV1_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_PkbWarningJudgeV1_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_PkbWarningJudgeV1_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_PkbWarningJudgeV1_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:PkbWarningJudgeV1 by Extend Api");
}

static void EepCopy_PkbWarningJudgeV1_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_PkbWarningJudgeV1_ITEM_LEN; i ++)
    {
        eep_hw_image.PkbWarningJudgeV1[i] = eep_ram_image.PkbWarningJudgeV1[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_PkbWarningJudgeV1_IN_BLOCK_NUM);
}

#else 
uint8_t GenEep_Get_PkbWarningJudgeV1(void)
{
    uint8_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.PkbWarningJudgeV1;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_PkbWarningJudgeV1(uint8_t const PkbWarningJudgeV1)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.PkbWarningJudgeV1 != PkbWarningJudgeV1 )
    {
        eep_ram_image.PkbWarningJudgeV1 = PkbWarningJudgeV1;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_PkbWarningJudgeV1_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_PkbWarningJudgeV1_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_PkbWarningJudgeV1_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_PkbWarningJudgeV1_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:PkbWarningJudgeV1=%d",PkbWarningJudgeV1);
}

static void EepCopy_PkbWarningJudgeV1_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.PkbWarningJudgeV1 = eep_ram_image.PkbWarningJudgeV1;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_PkbWarningJudgeV1_IN_BLOCK_NUM);
}
#endif
//static void Proc_PkbWarningJudgeV1_RestoreToDefault(void)
//{
//#if EEP_CONTENT_PkbWarningJudgeV1_CONST_VARIBLE
//	GenEep_Set_PkbWarningJudgeV1(dflt_eep_val.PkbWarningJudgeV1);
//#endif	
//}
#if EEP_CONTENT_PkbWarningJudgeV1_CONST_VARIBLE
#define Proc_PkbWarningJudgeV1_RestoreToDefault()	do{GenEep_Set_PkbWarningJudgeV1(dflt_eep_val.PkbWarningJudgeV1);}while(0)
#else
#define Proc_PkbWarningJudgeV1_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> PkbWarningJudgeV1 function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> PkbWarningJudgeV2 function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_PkbWarningJudgeV2_ITEM_LEN > 1
void GenEep_Get_PkbWarningJudgeV2(uint8_t * const PkbWarningJudgeV2 )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_PkbWarningJudgeV2_ITEM_LEN; i ++)
    {
        PkbWarningJudgeV2[i] = eep_ram_image.PkbWarningJudgeV2[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_PkbWarningJudgeV2_Ex(uint8_t * const PkbWarningJudgeV2,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.PkbWarningJudgeV2;	//EEP_CONTENT_PkbWarningJudgeV2_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_PkbWarningJudgeV2_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_PkbWarningJudgeV2_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        PkbWarningJudgeV2[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_PkbWarningJudgeV2(uint8_t const* PkbWarningJudgeV2 )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_PkbWarningJudgeV2_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.PkbWarningJudgeV2[i] != PkbWarningJudgeV2[i] )
        {
            eep_ram_image.PkbWarningJudgeV2[i] = PkbWarningJudgeV2[i];
            wr_eep_by_list_req[EEP_CONTENT_PkbWarningJudgeV2_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_PkbWarningJudgeV2_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_PkbWarningJudgeV2_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_PkbWarningJudgeV2_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:PkbWarningJudgeV2 firt item[0] = %d",PkbWarningJudgeV2[0]);
}

void GenEep_Set_PkbWarningJudgeV2_Ex(uint8_t const* PkbWarningJudgeV2,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_PkbWarningJudgeV2_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_PkbWarningJudgeV2_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.PkbWarningJudgeV2;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != PkbWarningJudgeV2[i] )
        {
            dest[offset+i] = PkbWarningJudgeV2[i];
            wr_eep_by_list_req[EEP_CONTENT_PkbWarningJudgeV2_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_PkbWarningJudgeV2_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_PkbWarningJudgeV2_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_PkbWarningJudgeV2_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:PkbWarningJudgeV2 by Extend Api");
}

static void EepCopy_PkbWarningJudgeV2_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_PkbWarningJudgeV2_ITEM_LEN; i ++)
    {
        eep_hw_image.PkbWarningJudgeV2[i] = eep_ram_image.PkbWarningJudgeV2[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_PkbWarningJudgeV2_IN_BLOCK_NUM);
}

#else 
uint8_t GenEep_Get_PkbWarningJudgeV2(void)
{
    uint8_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.PkbWarningJudgeV2;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_PkbWarningJudgeV2(uint8_t const PkbWarningJudgeV2)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.PkbWarningJudgeV2 != PkbWarningJudgeV2 )
    {
        eep_ram_image.PkbWarningJudgeV2 = PkbWarningJudgeV2;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_PkbWarningJudgeV2_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_PkbWarningJudgeV2_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_PkbWarningJudgeV2_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_PkbWarningJudgeV2_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:PkbWarningJudgeV2=%d",PkbWarningJudgeV2);
}

static void EepCopy_PkbWarningJudgeV2_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.PkbWarningJudgeV2 = eep_ram_image.PkbWarningJudgeV2;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_PkbWarningJudgeV2_IN_BLOCK_NUM);
}
#endif
//static void Proc_PkbWarningJudgeV2_RestoreToDefault(void)
//{
//#if EEP_CONTENT_PkbWarningJudgeV2_CONST_VARIBLE
//	GenEep_Set_PkbWarningJudgeV2(dflt_eep_val.PkbWarningJudgeV2);
//#endif	
//}
#if EEP_CONTENT_PkbWarningJudgeV2_CONST_VARIBLE
#define Proc_PkbWarningJudgeV2_RestoreToDefault()	do{GenEep_Set_PkbWarningJudgeV2(dflt_eep_val.PkbWarningJudgeV2);}while(0)
#else
#define Proc_PkbWarningJudgeV2_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> PkbWarningJudgeV2 function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> AudioEepTest function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_AudioEepTest_ITEM_LEN > 1
void GenEep_Get_AudioEepTest(uint16_t * const AudioEepTest )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_AudioEepTest_ITEM_LEN; i ++)
    {
        AudioEepTest[i] = eep_ram_image.AudioEepTest[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_AudioEepTest_Ex(uint8_t * const AudioEepTest,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.AudioEepTest;	//EEP_CONTENT_AudioEepTest_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_AudioEepTest_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_AudioEepTest_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        AudioEepTest[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_AudioEepTest(uint16_t const* AudioEepTest )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_AudioEepTest_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.AudioEepTest[i] != AudioEepTest[i] )
        {
            eep_ram_image.AudioEepTest[i] = AudioEepTest[i];
            wr_eep_by_list_req[EEP_CONTENT_AudioEepTest_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_AudioEepTest_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_AudioEepTest_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_AudioEepTest_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:AudioEepTest firt item[0] = %d",AudioEepTest[0]);
}

void GenEep_Set_AudioEepTest_Ex(uint8_t const* AudioEepTest,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_AudioEepTest_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_AudioEepTest_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.AudioEepTest;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != AudioEepTest[i] )
        {
            dest[offset+i] = AudioEepTest[i];
            wr_eep_by_list_req[EEP_CONTENT_AudioEepTest_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_AudioEepTest_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_AudioEepTest_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_AudioEepTest_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:AudioEepTest by Extend Api");
}

static void EepCopy_AudioEepTest_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_AudioEepTest_ITEM_LEN; i ++)
    {
        eep_hw_image.AudioEepTest[i] = eep_ram_image.AudioEepTest[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_AudioEepTest_IN_BLOCK_NUM);
}

#else 
uint16_t GenEep_Get_AudioEepTest(void)
{
    uint16_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.AudioEepTest;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_AudioEepTest(uint16_t const AudioEepTest)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.AudioEepTest != AudioEepTest )
    {
        eep_ram_image.AudioEepTest = AudioEepTest;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_AudioEepTest_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_AudioEepTest_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_AudioEepTest_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_AudioEepTest_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:AudioEepTest=%d",AudioEepTest);
}

static void EepCopy_AudioEepTest_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.AudioEepTest = eep_ram_image.AudioEepTest;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_AudioEepTest_IN_BLOCK_NUM);
}
#endif
//static void Proc_AudioEepTest_RestoreToDefault(void)
//{
//#if EEP_CONTENT_AudioEepTest_CONST_VARIBLE
//	GenEep_Set_AudioEepTest(dflt_eep_val.AudioEepTest);
//#endif	
//}
#if EEP_CONTENT_AudioEepTest_CONST_VARIBLE
#define Proc_AudioEepTest_RestoreToDefault()	do{GenEep_Set_AudioEepTest(dflt_eep_val.AudioEepTest);}while(0)
#else
#define Proc_AudioEepTest_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> AudioEepTest function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> DspKeyCodeOnOff function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_DspKeyCodeOnOff_ITEM_LEN > 1
void GenEep_Get_DspKeyCodeOnOff(uint8_t * const DspKeyCodeOnOff )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_DspKeyCodeOnOff_ITEM_LEN; i ++)
    {
        DspKeyCodeOnOff[i] = eep_ram_image.DspKeyCodeOnOff[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_DspKeyCodeOnOff_Ex(uint8_t * const DspKeyCodeOnOff,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.DspKeyCodeOnOff;	//EEP_CONTENT_DspKeyCodeOnOff_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_DspKeyCodeOnOff_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_DspKeyCodeOnOff_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        DspKeyCodeOnOff[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_DspKeyCodeOnOff(uint8_t const* DspKeyCodeOnOff )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_DspKeyCodeOnOff_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.DspKeyCodeOnOff[i] != DspKeyCodeOnOff[i] )
        {
            eep_ram_image.DspKeyCodeOnOff[i] = DspKeyCodeOnOff[i];
            wr_eep_by_list_req[EEP_CONTENT_DspKeyCodeOnOff_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_DspKeyCodeOnOff_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_DspKeyCodeOnOff_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_DspKeyCodeOnOff_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:DspKeyCodeOnOff firt item[0] = %d",DspKeyCodeOnOff[0]);
}

void GenEep_Set_DspKeyCodeOnOff_Ex(uint8_t const* DspKeyCodeOnOff,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_DspKeyCodeOnOff_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_DspKeyCodeOnOff_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.DspKeyCodeOnOff;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != DspKeyCodeOnOff[i] )
        {
            dest[offset+i] = DspKeyCodeOnOff[i];
            wr_eep_by_list_req[EEP_CONTENT_DspKeyCodeOnOff_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_DspKeyCodeOnOff_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_DspKeyCodeOnOff_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_DspKeyCodeOnOff_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:DspKeyCodeOnOff by Extend Api");
}

static void EepCopy_DspKeyCodeOnOff_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_DspKeyCodeOnOff_ITEM_LEN; i ++)
    {
        eep_hw_image.DspKeyCodeOnOff[i] = eep_ram_image.DspKeyCodeOnOff[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_DspKeyCodeOnOff_IN_BLOCK_NUM);
}

#else 
uint8_t GenEep_Get_DspKeyCodeOnOff(void)
{
    uint8_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.DspKeyCodeOnOff;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_DspKeyCodeOnOff(uint8_t const DspKeyCodeOnOff)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.DspKeyCodeOnOff != DspKeyCodeOnOff )
    {
        eep_ram_image.DspKeyCodeOnOff = DspKeyCodeOnOff;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_DspKeyCodeOnOff_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_DspKeyCodeOnOff_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_DspKeyCodeOnOff_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_DspKeyCodeOnOff_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:DspKeyCodeOnOff=%d",DspKeyCodeOnOff);
}

static void EepCopy_DspKeyCodeOnOff_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.DspKeyCodeOnOff = eep_ram_image.DspKeyCodeOnOff;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_DspKeyCodeOnOff_IN_BLOCK_NUM);
}
#endif
//static void Proc_DspKeyCodeOnOff_RestoreToDefault(void)
//{
//#if EEP_CONTENT_DspKeyCodeOnOff_CONST_VARIBLE
//	GenEep_Set_DspKeyCodeOnOff(dflt_eep_val.DspKeyCodeOnOff);
//#endif	
//}
#if EEP_CONTENT_DspKeyCodeOnOff_CONST_VARIBLE
#define Proc_DspKeyCodeOnOff_RestoreToDefault()	do{GenEep_Set_DspKeyCodeOnOff(dflt_eep_val.DspKeyCodeOnOff);}while(0)
#else
#define Proc_DspKeyCodeOnOff_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> DspKeyCodeOnOff function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> DspKeyCode function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_DspKeyCode_ITEM_LEN > 1
void GenEep_Get_DspKeyCode(uint32_t * const DspKeyCode )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_DspKeyCode_ITEM_LEN; i ++)
    {
        DspKeyCode[i] = eep_ram_image.DspKeyCode[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_DspKeyCode_Ex(uint8_t * const DspKeyCode,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.DspKeyCode;	//EEP_CONTENT_DspKeyCode_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_DspKeyCode_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_DspKeyCode_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        DspKeyCode[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_DspKeyCode(uint32_t const* DspKeyCode )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_DspKeyCode_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.DspKeyCode[i] != DspKeyCode[i] )
        {
            eep_ram_image.DspKeyCode[i] = DspKeyCode[i];
            wr_eep_by_list_req[EEP_CONTENT_DspKeyCode_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_DspKeyCode_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_DspKeyCode_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_DspKeyCode_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:DspKeyCode firt item[0] = %d",DspKeyCode[0]);
}

void GenEep_Set_DspKeyCode_Ex(uint8_t const* DspKeyCode,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_DspKeyCode_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_DspKeyCode_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.DspKeyCode;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != DspKeyCode[i] )
        {
            dest[offset+i] = DspKeyCode[i];
            wr_eep_by_list_req[EEP_CONTENT_DspKeyCode_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_DspKeyCode_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_DspKeyCode_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_DspKeyCode_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:DspKeyCode by Extend Api");
}

static void EepCopy_DspKeyCode_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_DspKeyCode_ITEM_LEN; i ++)
    {
        eep_hw_image.DspKeyCode[i] = eep_ram_image.DspKeyCode[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_DspKeyCode_IN_BLOCK_NUM);
}

#else 
uint32_t GenEep_Get_DspKeyCode(void)
{
    uint32_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.DspKeyCode;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_DspKeyCode(uint32_t const DspKeyCode)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.DspKeyCode != DspKeyCode )
    {
        eep_ram_image.DspKeyCode = DspKeyCode;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_DspKeyCode_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_DspKeyCode_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_DspKeyCode_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_DspKeyCode_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:DspKeyCode=%d",DspKeyCode);
}

static void EepCopy_DspKeyCode_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.DspKeyCode = eep_ram_image.DspKeyCode;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_DspKeyCode_IN_BLOCK_NUM);
}
#endif
//static void Proc_DspKeyCode_RestoreToDefault(void)
//{
//#if EEP_CONTENT_DspKeyCode_CONST_VARIBLE
//	GenEep_Set_DspKeyCode(dflt_eep_val.DspKeyCode);
//#endif	
//}
#if EEP_CONTENT_DspKeyCode_CONST_VARIBLE
#define Proc_DspKeyCode_RestoreToDefault()	do{GenEep_Set_DspKeyCode(dflt_eep_val.DspKeyCode);}while(0)
#else
#define Proc_DspKeyCode_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> DspKeyCode function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> BatState_ChgDly function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_BatState_ChgDly_ITEM_LEN > 1
void GenEep_Get_BatState_ChgDly(uint16_t * const BatState_ChgDly )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_BatState_ChgDly_ITEM_LEN; i ++)
    {
        BatState_ChgDly[i] = eep_ram_image.BatState_ChgDly[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_BatState_ChgDly_Ex(uint8_t * const BatState_ChgDly,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.BatState_ChgDly;	//EEP_CONTENT_BatState_ChgDly_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_BatState_ChgDly_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_BatState_ChgDly_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        BatState_ChgDly[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_BatState_ChgDly(uint16_t const* BatState_ChgDly )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_BatState_ChgDly_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.BatState_ChgDly[i] != BatState_ChgDly[i] )
        {
            eep_ram_image.BatState_ChgDly[i] = BatState_ChgDly[i];
            wr_eep_by_list_req[EEP_CONTENT_BatState_ChgDly_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_BatState_ChgDly_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_BatState_ChgDly_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_BatState_ChgDly_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:BatState_ChgDly firt item[0] = %d",BatState_ChgDly[0]);
}

void GenEep_Set_BatState_ChgDly_Ex(uint8_t const* BatState_ChgDly,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_BatState_ChgDly_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_BatState_ChgDly_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.BatState_ChgDly;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != BatState_ChgDly[i] )
        {
            dest[offset+i] = BatState_ChgDly[i];
            wr_eep_by_list_req[EEP_CONTENT_BatState_ChgDly_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_BatState_ChgDly_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_BatState_ChgDly_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_BatState_ChgDly_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:BatState_ChgDly by Extend Api");
}

static void EepCopy_BatState_ChgDly_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_BatState_ChgDly_ITEM_LEN; i ++)
    {
        eep_hw_image.BatState_ChgDly[i] = eep_ram_image.BatState_ChgDly[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_BatState_ChgDly_IN_BLOCK_NUM);
}

#else 
uint16_t GenEep_Get_BatState_ChgDly(void)
{
    uint16_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.BatState_ChgDly;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_BatState_ChgDly(uint16_t const BatState_ChgDly)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.BatState_ChgDly != BatState_ChgDly )
    {
        eep_ram_image.BatState_ChgDly = BatState_ChgDly;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_BatState_ChgDly_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_BatState_ChgDly_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_BatState_ChgDly_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_BatState_ChgDly_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:BatState_ChgDly=%d",BatState_ChgDly);
}

static void EepCopy_BatState_ChgDly_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.BatState_ChgDly = eep_ram_image.BatState_ChgDly;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_BatState_ChgDly_IN_BLOCK_NUM);
}
#endif
//static void Proc_BatState_ChgDly_RestoreToDefault(void)
//{
//#if EEP_CONTENT_BatState_ChgDly_CONST_VARIBLE
//	GenEep_Set_BatState_ChgDly(dflt_eep_val.BatState_ChgDly);
//#endif	
//}
#if EEP_CONTENT_BatState_ChgDly_CONST_VARIBLE
#define Proc_BatState_ChgDly_RestoreToDefault()	do{GenEep_Set_BatState_ChgDly(dflt_eep_val.BatState_ChgDly);}while(0)
#else
#define Proc_BatState_ChgDly_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> BatState_ChgDly function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> BatVolVeryHigh_Hysteresis_H function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_BatVolVeryHigh_Hysteresis_H_ITEM_LEN > 1
void GenEep_Get_BatVolVeryHigh_Hysteresis_H(uint16_t * const BatVolVeryHigh_Hysteresis_H )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_BatVolVeryHigh_Hysteresis_H_ITEM_LEN; i ++)
    {
        BatVolVeryHigh_Hysteresis_H[i] = eep_ram_image.BatVolVeryHigh_Hysteresis_H[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_BatVolVeryHigh_Hysteresis_H_Ex(uint8_t * const BatVolVeryHigh_Hysteresis_H,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.BatVolVeryHigh_Hysteresis_H;	//EEP_CONTENT_BatVolVeryHigh_Hysteresis_H_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_BatVolVeryHigh_Hysteresis_H_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_BatVolVeryHigh_Hysteresis_H_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        BatVolVeryHigh_Hysteresis_H[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_BatVolVeryHigh_Hysteresis_H(uint16_t const* BatVolVeryHigh_Hysteresis_H )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_BatVolVeryHigh_Hysteresis_H_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.BatVolVeryHigh_Hysteresis_H[i] != BatVolVeryHigh_Hysteresis_H[i] )
        {
            eep_ram_image.BatVolVeryHigh_Hysteresis_H[i] = BatVolVeryHigh_Hysteresis_H[i];
            wr_eep_by_list_req[EEP_CONTENT_BatVolVeryHigh_Hysteresis_H_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_BatVolVeryHigh_Hysteresis_H_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_BatVolVeryHigh_Hysteresis_H_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_BatVolVeryHigh_Hysteresis_H_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:BatVolVeryHigh_Hysteresis_H firt item[0] = %d",BatVolVeryHigh_Hysteresis_H[0]);
}

void GenEep_Set_BatVolVeryHigh_Hysteresis_H_Ex(uint8_t const* BatVolVeryHigh_Hysteresis_H,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_BatVolVeryHigh_Hysteresis_H_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_BatVolVeryHigh_Hysteresis_H_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.BatVolVeryHigh_Hysteresis_H;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != BatVolVeryHigh_Hysteresis_H[i] )
        {
            dest[offset+i] = BatVolVeryHigh_Hysteresis_H[i];
            wr_eep_by_list_req[EEP_CONTENT_BatVolVeryHigh_Hysteresis_H_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_BatVolVeryHigh_Hysteresis_H_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_BatVolVeryHigh_Hysteresis_H_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_BatVolVeryHigh_Hysteresis_H_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:BatVolVeryHigh_Hysteresis_H by Extend Api");
}

static void EepCopy_BatVolVeryHigh_Hysteresis_H_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_BatVolVeryHigh_Hysteresis_H_ITEM_LEN; i ++)
    {
        eep_hw_image.BatVolVeryHigh_Hysteresis_H[i] = eep_ram_image.BatVolVeryHigh_Hysteresis_H[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_BatVolVeryHigh_Hysteresis_H_IN_BLOCK_NUM);
}

#else 
uint16_t GenEep_Get_BatVolVeryHigh_Hysteresis_H(void)
{
    uint16_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.BatVolVeryHigh_Hysteresis_H;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_BatVolVeryHigh_Hysteresis_H(uint16_t const BatVolVeryHigh_Hysteresis_H)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.BatVolVeryHigh_Hysteresis_H != BatVolVeryHigh_Hysteresis_H )
    {
        eep_ram_image.BatVolVeryHigh_Hysteresis_H = BatVolVeryHigh_Hysteresis_H;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_BatVolVeryHigh_Hysteresis_H_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_BatVolVeryHigh_Hysteresis_H_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_BatVolVeryHigh_Hysteresis_H_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_BatVolVeryHigh_Hysteresis_H_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:BatVolVeryHigh_Hysteresis_H=%d",BatVolVeryHigh_Hysteresis_H);
}

static void EepCopy_BatVolVeryHigh_Hysteresis_H_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.BatVolVeryHigh_Hysteresis_H = eep_ram_image.BatVolVeryHigh_Hysteresis_H;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_BatVolVeryHigh_Hysteresis_H_IN_BLOCK_NUM);
}
#endif
//static void Proc_BatVolVeryHigh_Hysteresis_H_RestoreToDefault(void)
//{
//#if EEP_CONTENT_BatVolVeryHigh_Hysteresis_H_CONST_VARIBLE
//	GenEep_Set_BatVolVeryHigh_Hysteresis_H(dflt_eep_val.BatVolVeryHigh_Hysteresis_H);
//#endif	
//}
#if EEP_CONTENT_BatVolVeryHigh_Hysteresis_H_CONST_VARIBLE
#define Proc_BatVolVeryHigh_Hysteresis_H_RestoreToDefault()	do{GenEep_Set_BatVolVeryHigh_Hysteresis_H(dflt_eep_val.BatVolVeryHigh_Hysteresis_H);}while(0)
#else
#define Proc_BatVolVeryHigh_Hysteresis_H_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> BatVolVeryHigh_Hysteresis_H function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> BatVolVeryHigh_Hysteresis_L function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_BatVolVeryHigh_Hysteresis_L_ITEM_LEN > 1
void GenEep_Get_BatVolVeryHigh_Hysteresis_L(uint16_t * const BatVolVeryHigh_Hysteresis_L )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_BatVolVeryHigh_Hysteresis_L_ITEM_LEN; i ++)
    {
        BatVolVeryHigh_Hysteresis_L[i] = eep_ram_image.BatVolVeryHigh_Hysteresis_L[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_BatVolVeryHigh_Hysteresis_L_Ex(uint8_t * const BatVolVeryHigh_Hysteresis_L,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.BatVolVeryHigh_Hysteresis_L;	//EEP_CONTENT_BatVolVeryHigh_Hysteresis_L_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_BatVolVeryHigh_Hysteresis_L_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_BatVolVeryHigh_Hysteresis_L_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        BatVolVeryHigh_Hysteresis_L[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_BatVolVeryHigh_Hysteresis_L(uint16_t const* BatVolVeryHigh_Hysteresis_L )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_BatVolVeryHigh_Hysteresis_L_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.BatVolVeryHigh_Hysteresis_L[i] != BatVolVeryHigh_Hysteresis_L[i] )
        {
            eep_ram_image.BatVolVeryHigh_Hysteresis_L[i] = BatVolVeryHigh_Hysteresis_L[i];
            wr_eep_by_list_req[EEP_CONTENT_BatVolVeryHigh_Hysteresis_L_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_BatVolVeryHigh_Hysteresis_L_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_BatVolVeryHigh_Hysteresis_L_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_BatVolVeryHigh_Hysteresis_L_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:BatVolVeryHigh_Hysteresis_L firt item[0] = %d",BatVolVeryHigh_Hysteresis_L[0]);
}

void GenEep_Set_BatVolVeryHigh_Hysteresis_L_Ex(uint8_t const* BatVolVeryHigh_Hysteresis_L,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_BatVolVeryHigh_Hysteresis_L_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_BatVolVeryHigh_Hysteresis_L_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.BatVolVeryHigh_Hysteresis_L;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != BatVolVeryHigh_Hysteresis_L[i] )
        {
            dest[offset+i] = BatVolVeryHigh_Hysteresis_L[i];
            wr_eep_by_list_req[EEP_CONTENT_BatVolVeryHigh_Hysteresis_L_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_BatVolVeryHigh_Hysteresis_L_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_BatVolVeryHigh_Hysteresis_L_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_BatVolVeryHigh_Hysteresis_L_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:BatVolVeryHigh_Hysteresis_L by Extend Api");
}

static void EepCopy_BatVolVeryHigh_Hysteresis_L_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_BatVolVeryHigh_Hysteresis_L_ITEM_LEN; i ++)
    {
        eep_hw_image.BatVolVeryHigh_Hysteresis_L[i] = eep_ram_image.BatVolVeryHigh_Hysteresis_L[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_BatVolVeryHigh_Hysteresis_L_IN_BLOCK_NUM);
}

#else 
uint16_t GenEep_Get_BatVolVeryHigh_Hysteresis_L(void)
{
    uint16_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.BatVolVeryHigh_Hysteresis_L;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_BatVolVeryHigh_Hysteresis_L(uint16_t const BatVolVeryHigh_Hysteresis_L)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.BatVolVeryHigh_Hysteresis_L != BatVolVeryHigh_Hysteresis_L )
    {
        eep_ram_image.BatVolVeryHigh_Hysteresis_L = BatVolVeryHigh_Hysteresis_L;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_BatVolVeryHigh_Hysteresis_L_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_BatVolVeryHigh_Hysteresis_L_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_BatVolVeryHigh_Hysteresis_L_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_BatVolVeryHigh_Hysteresis_L_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:BatVolVeryHigh_Hysteresis_L=%d",BatVolVeryHigh_Hysteresis_L);
}

static void EepCopy_BatVolVeryHigh_Hysteresis_L_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.BatVolVeryHigh_Hysteresis_L = eep_ram_image.BatVolVeryHigh_Hysteresis_L;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_BatVolVeryHigh_Hysteresis_L_IN_BLOCK_NUM);
}
#endif
//static void Proc_BatVolVeryHigh_Hysteresis_L_RestoreToDefault(void)
//{
//#if EEP_CONTENT_BatVolVeryHigh_Hysteresis_L_CONST_VARIBLE
//	GenEep_Set_BatVolVeryHigh_Hysteresis_L(dflt_eep_val.BatVolVeryHigh_Hysteresis_L);
//#endif	
//}
#if EEP_CONTENT_BatVolVeryHigh_Hysteresis_L_CONST_VARIBLE
#define Proc_BatVolVeryHigh_Hysteresis_L_RestoreToDefault()	do{GenEep_Set_BatVolVeryHigh_Hysteresis_L(dflt_eep_val.BatVolVeryHigh_Hysteresis_L);}while(0)
#else
#define Proc_BatVolVeryHigh_Hysteresis_L_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> BatVolVeryHigh_Hysteresis_L function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> BatVolHigh_Hysteresis_H function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_BatVolHigh_Hysteresis_H_ITEM_LEN > 1
void GenEep_Get_BatVolHigh_Hysteresis_H(uint16_t * const BatVolHigh_Hysteresis_H )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_BatVolHigh_Hysteresis_H_ITEM_LEN; i ++)
    {
        BatVolHigh_Hysteresis_H[i] = eep_ram_image.BatVolHigh_Hysteresis_H[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_BatVolHigh_Hysteresis_H_Ex(uint8_t * const BatVolHigh_Hysteresis_H,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.BatVolHigh_Hysteresis_H;	//EEP_CONTENT_BatVolHigh_Hysteresis_H_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_BatVolHigh_Hysteresis_H_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_BatVolHigh_Hysteresis_H_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        BatVolHigh_Hysteresis_H[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_BatVolHigh_Hysteresis_H(uint16_t const* BatVolHigh_Hysteresis_H )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_BatVolHigh_Hysteresis_H_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.BatVolHigh_Hysteresis_H[i] != BatVolHigh_Hysteresis_H[i] )
        {
            eep_ram_image.BatVolHigh_Hysteresis_H[i] = BatVolHigh_Hysteresis_H[i];
            wr_eep_by_list_req[EEP_CONTENT_BatVolHigh_Hysteresis_H_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_BatVolHigh_Hysteresis_H_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_BatVolHigh_Hysteresis_H_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_BatVolHigh_Hysteresis_H_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:BatVolHigh_Hysteresis_H firt item[0] = %d",BatVolHigh_Hysteresis_H[0]);
}

void GenEep_Set_BatVolHigh_Hysteresis_H_Ex(uint8_t const* BatVolHigh_Hysteresis_H,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_BatVolHigh_Hysteresis_H_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_BatVolHigh_Hysteresis_H_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.BatVolHigh_Hysteresis_H;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != BatVolHigh_Hysteresis_H[i] )
        {
            dest[offset+i] = BatVolHigh_Hysteresis_H[i];
            wr_eep_by_list_req[EEP_CONTENT_BatVolHigh_Hysteresis_H_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_BatVolHigh_Hysteresis_H_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_BatVolHigh_Hysteresis_H_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_BatVolHigh_Hysteresis_H_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:BatVolHigh_Hysteresis_H by Extend Api");
}

static void EepCopy_BatVolHigh_Hysteresis_H_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_BatVolHigh_Hysteresis_H_ITEM_LEN; i ++)
    {
        eep_hw_image.BatVolHigh_Hysteresis_H[i] = eep_ram_image.BatVolHigh_Hysteresis_H[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_BatVolHigh_Hysteresis_H_IN_BLOCK_NUM);
}

#else 
uint16_t GenEep_Get_BatVolHigh_Hysteresis_H(void)
{
    uint16_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.BatVolHigh_Hysteresis_H;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_BatVolHigh_Hysteresis_H(uint16_t const BatVolHigh_Hysteresis_H)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.BatVolHigh_Hysteresis_H != BatVolHigh_Hysteresis_H )
    {
        eep_ram_image.BatVolHigh_Hysteresis_H = BatVolHigh_Hysteresis_H;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_BatVolHigh_Hysteresis_H_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_BatVolHigh_Hysteresis_H_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_BatVolHigh_Hysteresis_H_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_BatVolHigh_Hysteresis_H_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:BatVolHigh_Hysteresis_H=%d",BatVolHigh_Hysteresis_H);
}

static void EepCopy_BatVolHigh_Hysteresis_H_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.BatVolHigh_Hysteresis_H = eep_ram_image.BatVolHigh_Hysteresis_H;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_BatVolHigh_Hysteresis_H_IN_BLOCK_NUM);
}
#endif
//static void Proc_BatVolHigh_Hysteresis_H_RestoreToDefault(void)
//{
//#if EEP_CONTENT_BatVolHigh_Hysteresis_H_CONST_VARIBLE
//	GenEep_Set_BatVolHigh_Hysteresis_H(dflt_eep_val.BatVolHigh_Hysteresis_H);
//#endif	
//}
#if EEP_CONTENT_BatVolHigh_Hysteresis_H_CONST_VARIBLE
#define Proc_BatVolHigh_Hysteresis_H_RestoreToDefault()	do{GenEep_Set_BatVolHigh_Hysteresis_H(dflt_eep_val.BatVolHigh_Hysteresis_H);}while(0)
#else
#define Proc_BatVolHigh_Hysteresis_H_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> BatVolHigh_Hysteresis_H function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> BatVolHigh_Hysteresis_L function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_BatVolHigh_Hysteresis_L_ITEM_LEN > 1
void GenEep_Get_BatVolHigh_Hysteresis_L(uint16_t * const BatVolHigh_Hysteresis_L )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_BatVolHigh_Hysteresis_L_ITEM_LEN; i ++)
    {
        BatVolHigh_Hysteresis_L[i] = eep_ram_image.BatVolHigh_Hysteresis_L[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_BatVolHigh_Hysteresis_L_Ex(uint8_t * const BatVolHigh_Hysteresis_L,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.BatVolHigh_Hysteresis_L;	//EEP_CONTENT_BatVolHigh_Hysteresis_L_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_BatVolHigh_Hysteresis_L_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_BatVolHigh_Hysteresis_L_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        BatVolHigh_Hysteresis_L[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_BatVolHigh_Hysteresis_L(uint16_t const* BatVolHigh_Hysteresis_L )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_BatVolHigh_Hysteresis_L_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.BatVolHigh_Hysteresis_L[i] != BatVolHigh_Hysteresis_L[i] )
        {
            eep_ram_image.BatVolHigh_Hysteresis_L[i] = BatVolHigh_Hysteresis_L[i];
            wr_eep_by_list_req[EEP_CONTENT_BatVolHigh_Hysteresis_L_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_BatVolHigh_Hysteresis_L_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_BatVolHigh_Hysteresis_L_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_BatVolHigh_Hysteresis_L_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:BatVolHigh_Hysteresis_L firt item[0] = %d",BatVolHigh_Hysteresis_L[0]);
}

void GenEep_Set_BatVolHigh_Hysteresis_L_Ex(uint8_t const* BatVolHigh_Hysteresis_L,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_BatVolHigh_Hysteresis_L_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_BatVolHigh_Hysteresis_L_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.BatVolHigh_Hysteresis_L;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != BatVolHigh_Hysteresis_L[i] )
        {
            dest[offset+i] = BatVolHigh_Hysteresis_L[i];
            wr_eep_by_list_req[EEP_CONTENT_BatVolHigh_Hysteresis_L_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_BatVolHigh_Hysteresis_L_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_BatVolHigh_Hysteresis_L_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_BatVolHigh_Hysteresis_L_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:BatVolHigh_Hysteresis_L by Extend Api");
}

static void EepCopy_BatVolHigh_Hysteresis_L_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_BatVolHigh_Hysteresis_L_ITEM_LEN; i ++)
    {
        eep_hw_image.BatVolHigh_Hysteresis_L[i] = eep_ram_image.BatVolHigh_Hysteresis_L[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_BatVolHigh_Hysteresis_L_IN_BLOCK_NUM);
}

#else 
uint16_t GenEep_Get_BatVolHigh_Hysteresis_L(void)
{
    uint16_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.BatVolHigh_Hysteresis_L;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_BatVolHigh_Hysteresis_L(uint16_t const BatVolHigh_Hysteresis_L)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.BatVolHigh_Hysteresis_L != BatVolHigh_Hysteresis_L )
    {
        eep_ram_image.BatVolHigh_Hysteresis_L = BatVolHigh_Hysteresis_L;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_BatVolHigh_Hysteresis_L_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_BatVolHigh_Hysteresis_L_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_BatVolHigh_Hysteresis_L_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_BatVolHigh_Hysteresis_L_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:BatVolHigh_Hysteresis_L=%d",BatVolHigh_Hysteresis_L);
}

static void EepCopy_BatVolHigh_Hysteresis_L_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.BatVolHigh_Hysteresis_L = eep_ram_image.BatVolHigh_Hysteresis_L;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_BatVolHigh_Hysteresis_L_IN_BLOCK_NUM);
}
#endif
//static void Proc_BatVolHigh_Hysteresis_L_RestoreToDefault(void)
//{
//#if EEP_CONTENT_BatVolHigh_Hysteresis_L_CONST_VARIBLE
//	GenEep_Set_BatVolHigh_Hysteresis_L(dflt_eep_val.BatVolHigh_Hysteresis_L);
//#endif	
//}
#if EEP_CONTENT_BatVolHigh_Hysteresis_L_CONST_VARIBLE
#define Proc_BatVolHigh_Hysteresis_L_RestoreToDefault()	do{GenEep_Set_BatVolHigh_Hysteresis_L(dflt_eep_val.BatVolHigh_Hysteresis_L);}while(0)
#else
#define Proc_BatVolHigh_Hysteresis_L_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> BatVolHigh_Hysteresis_L function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> BatVolLow_Hysteresis_H function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_BatVolLow_Hysteresis_H_ITEM_LEN > 1
void GenEep_Get_BatVolLow_Hysteresis_H(uint16_t * const BatVolLow_Hysteresis_H )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_BatVolLow_Hysteresis_H_ITEM_LEN; i ++)
    {
        BatVolLow_Hysteresis_H[i] = eep_ram_image.BatVolLow_Hysteresis_H[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_BatVolLow_Hysteresis_H_Ex(uint8_t * const BatVolLow_Hysteresis_H,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.BatVolLow_Hysteresis_H;	//EEP_CONTENT_BatVolLow_Hysteresis_H_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_BatVolLow_Hysteresis_H_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_BatVolLow_Hysteresis_H_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        BatVolLow_Hysteresis_H[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_BatVolLow_Hysteresis_H(uint16_t const* BatVolLow_Hysteresis_H )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_BatVolLow_Hysteresis_H_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.BatVolLow_Hysteresis_H[i] != BatVolLow_Hysteresis_H[i] )
        {
            eep_ram_image.BatVolLow_Hysteresis_H[i] = BatVolLow_Hysteresis_H[i];
            wr_eep_by_list_req[EEP_CONTENT_BatVolLow_Hysteresis_H_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_BatVolLow_Hysteresis_H_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_BatVolLow_Hysteresis_H_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_BatVolLow_Hysteresis_H_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:BatVolLow_Hysteresis_H firt item[0] = %d",BatVolLow_Hysteresis_H[0]);
}

void GenEep_Set_BatVolLow_Hysteresis_H_Ex(uint8_t const* BatVolLow_Hysteresis_H,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_BatVolLow_Hysteresis_H_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_BatVolLow_Hysteresis_H_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.BatVolLow_Hysteresis_H;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != BatVolLow_Hysteresis_H[i] )
        {
            dest[offset+i] = BatVolLow_Hysteresis_H[i];
            wr_eep_by_list_req[EEP_CONTENT_BatVolLow_Hysteresis_H_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_BatVolLow_Hysteresis_H_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_BatVolLow_Hysteresis_H_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_BatVolLow_Hysteresis_H_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:BatVolLow_Hysteresis_H by Extend Api");
}

static void EepCopy_BatVolLow_Hysteresis_H_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_BatVolLow_Hysteresis_H_ITEM_LEN; i ++)
    {
        eep_hw_image.BatVolLow_Hysteresis_H[i] = eep_ram_image.BatVolLow_Hysteresis_H[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_BatVolLow_Hysteresis_H_IN_BLOCK_NUM);
}

#else 
uint16_t GenEep_Get_BatVolLow_Hysteresis_H(void)
{
    uint16_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.BatVolLow_Hysteresis_H;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_BatVolLow_Hysteresis_H(uint16_t const BatVolLow_Hysteresis_H)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.BatVolLow_Hysteresis_H != BatVolLow_Hysteresis_H )
    {
        eep_ram_image.BatVolLow_Hysteresis_H = BatVolLow_Hysteresis_H;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_BatVolLow_Hysteresis_H_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_BatVolLow_Hysteresis_H_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_BatVolLow_Hysteresis_H_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_BatVolLow_Hysteresis_H_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:BatVolLow_Hysteresis_H=%d",BatVolLow_Hysteresis_H);
}

static void EepCopy_BatVolLow_Hysteresis_H_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.BatVolLow_Hysteresis_H = eep_ram_image.BatVolLow_Hysteresis_H;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_BatVolLow_Hysteresis_H_IN_BLOCK_NUM);
}
#endif
//static void Proc_BatVolLow_Hysteresis_H_RestoreToDefault(void)
//{
//#if EEP_CONTENT_BatVolLow_Hysteresis_H_CONST_VARIBLE
//	GenEep_Set_BatVolLow_Hysteresis_H(dflt_eep_val.BatVolLow_Hysteresis_H);
//#endif	
//}
#if EEP_CONTENT_BatVolLow_Hysteresis_H_CONST_VARIBLE
#define Proc_BatVolLow_Hysteresis_H_RestoreToDefault()	do{GenEep_Set_BatVolLow_Hysteresis_H(dflt_eep_val.BatVolLow_Hysteresis_H);}while(0)
#else
#define Proc_BatVolLow_Hysteresis_H_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> BatVolLow_Hysteresis_H function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> BatVolLow_Hysteresis_L function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_BatVolLow_Hysteresis_L_ITEM_LEN > 1
void GenEep_Get_BatVolLow_Hysteresis_L(uint16_t * const BatVolLow_Hysteresis_L )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_BatVolLow_Hysteresis_L_ITEM_LEN; i ++)
    {
        BatVolLow_Hysteresis_L[i] = eep_ram_image.BatVolLow_Hysteresis_L[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_BatVolLow_Hysteresis_L_Ex(uint8_t * const BatVolLow_Hysteresis_L,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.BatVolLow_Hysteresis_L;	//EEP_CONTENT_BatVolLow_Hysteresis_L_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_BatVolLow_Hysteresis_L_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_BatVolLow_Hysteresis_L_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        BatVolLow_Hysteresis_L[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_BatVolLow_Hysteresis_L(uint16_t const* BatVolLow_Hysteresis_L )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_BatVolLow_Hysteresis_L_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.BatVolLow_Hysteresis_L[i] != BatVolLow_Hysteresis_L[i] )
        {
            eep_ram_image.BatVolLow_Hysteresis_L[i] = BatVolLow_Hysteresis_L[i];
            wr_eep_by_list_req[EEP_CONTENT_BatVolLow_Hysteresis_L_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_BatVolLow_Hysteresis_L_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_BatVolLow_Hysteresis_L_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_BatVolLow_Hysteresis_L_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:BatVolLow_Hysteresis_L firt item[0] = %d",BatVolLow_Hysteresis_L[0]);
}

void GenEep_Set_BatVolLow_Hysteresis_L_Ex(uint8_t const* BatVolLow_Hysteresis_L,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_BatVolLow_Hysteresis_L_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_BatVolLow_Hysteresis_L_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.BatVolLow_Hysteresis_L;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != BatVolLow_Hysteresis_L[i] )
        {
            dest[offset+i] = BatVolLow_Hysteresis_L[i];
            wr_eep_by_list_req[EEP_CONTENT_BatVolLow_Hysteresis_L_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_BatVolLow_Hysteresis_L_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_BatVolLow_Hysteresis_L_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_BatVolLow_Hysteresis_L_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:BatVolLow_Hysteresis_L by Extend Api");
}

static void EepCopy_BatVolLow_Hysteresis_L_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_BatVolLow_Hysteresis_L_ITEM_LEN; i ++)
    {
        eep_hw_image.BatVolLow_Hysteresis_L[i] = eep_ram_image.BatVolLow_Hysteresis_L[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_BatVolLow_Hysteresis_L_IN_BLOCK_NUM);
}

#else 
uint16_t GenEep_Get_BatVolLow_Hysteresis_L(void)
{
    uint16_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.BatVolLow_Hysteresis_L;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_BatVolLow_Hysteresis_L(uint16_t const BatVolLow_Hysteresis_L)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.BatVolLow_Hysteresis_L != BatVolLow_Hysteresis_L )
    {
        eep_ram_image.BatVolLow_Hysteresis_L = BatVolLow_Hysteresis_L;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_BatVolLow_Hysteresis_L_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_BatVolLow_Hysteresis_L_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_BatVolLow_Hysteresis_L_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_BatVolLow_Hysteresis_L_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:BatVolLow_Hysteresis_L=%d",BatVolLow_Hysteresis_L);
}

static void EepCopy_BatVolLow_Hysteresis_L_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.BatVolLow_Hysteresis_L = eep_ram_image.BatVolLow_Hysteresis_L;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_BatVolLow_Hysteresis_L_IN_BLOCK_NUM);
}
#endif
//static void Proc_BatVolLow_Hysteresis_L_RestoreToDefault(void)
//{
//#if EEP_CONTENT_BatVolLow_Hysteresis_L_CONST_VARIBLE
//	GenEep_Set_BatVolLow_Hysteresis_L(dflt_eep_val.BatVolLow_Hysteresis_L);
//#endif	
//}
#if EEP_CONTENT_BatVolLow_Hysteresis_L_CONST_VARIBLE
#define Proc_BatVolLow_Hysteresis_L_RestoreToDefault()	do{GenEep_Set_BatVolLow_Hysteresis_L(dflt_eep_val.BatVolLow_Hysteresis_L);}while(0)
#else
#define Proc_BatVolLow_Hysteresis_L_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> BatVolLow_Hysteresis_L function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> BatVolVeryLow_Hysteresis_H function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_BatVolVeryLow_Hysteresis_H_ITEM_LEN > 1
void GenEep_Get_BatVolVeryLow_Hysteresis_H(uint16_t * const BatVolVeryLow_Hysteresis_H )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_BatVolVeryLow_Hysteresis_H_ITEM_LEN; i ++)
    {
        BatVolVeryLow_Hysteresis_H[i] = eep_ram_image.BatVolVeryLow_Hysteresis_H[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_BatVolVeryLow_Hysteresis_H_Ex(uint8_t * const BatVolVeryLow_Hysteresis_H,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.BatVolVeryLow_Hysteresis_H;	//EEP_CONTENT_BatVolVeryLow_Hysteresis_H_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_BatVolVeryLow_Hysteresis_H_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_BatVolVeryLow_Hysteresis_H_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        BatVolVeryLow_Hysteresis_H[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_BatVolVeryLow_Hysteresis_H(uint16_t const* BatVolVeryLow_Hysteresis_H )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_BatVolVeryLow_Hysteresis_H_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.BatVolVeryLow_Hysteresis_H[i] != BatVolVeryLow_Hysteresis_H[i] )
        {
            eep_ram_image.BatVolVeryLow_Hysteresis_H[i] = BatVolVeryLow_Hysteresis_H[i];
            wr_eep_by_list_req[EEP_CONTENT_BatVolVeryLow_Hysteresis_H_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_BatVolVeryLow_Hysteresis_H_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_BatVolVeryLow_Hysteresis_H_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_BatVolVeryLow_Hysteresis_H_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:BatVolVeryLow_Hysteresis_H firt item[0] = %d",BatVolVeryLow_Hysteresis_H[0]);
}

void GenEep_Set_BatVolVeryLow_Hysteresis_H_Ex(uint8_t const* BatVolVeryLow_Hysteresis_H,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_BatVolVeryLow_Hysteresis_H_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_BatVolVeryLow_Hysteresis_H_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.BatVolVeryLow_Hysteresis_H;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != BatVolVeryLow_Hysteresis_H[i] )
        {
            dest[offset+i] = BatVolVeryLow_Hysteresis_H[i];
            wr_eep_by_list_req[EEP_CONTENT_BatVolVeryLow_Hysteresis_H_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_BatVolVeryLow_Hysteresis_H_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_BatVolVeryLow_Hysteresis_H_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_BatVolVeryLow_Hysteresis_H_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:BatVolVeryLow_Hysteresis_H by Extend Api");
}

static void EepCopy_BatVolVeryLow_Hysteresis_H_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_BatVolVeryLow_Hysteresis_H_ITEM_LEN; i ++)
    {
        eep_hw_image.BatVolVeryLow_Hysteresis_H[i] = eep_ram_image.BatVolVeryLow_Hysteresis_H[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_BatVolVeryLow_Hysteresis_H_IN_BLOCK_NUM);
}

#else 
uint16_t GenEep_Get_BatVolVeryLow_Hysteresis_H(void)
{
    uint16_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.BatVolVeryLow_Hysteresis_H;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_BatVolVeryLow_Hysteresis_H(uint16_t const BatVolVeryLow_Hysteresis_H)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.BatVolVeryLow_Hysteresis_H != BatVolVeryLow_Hysteresis_H )
    {
        eep_ram_image.BatVolVeryLow_Hysteresis_H = BatVolVeryLow_Hysteresis_H;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_BatVolVeryLow_Hysteresis_H_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_BatVolVeryLow_Hysteresis_H_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_BatVolVeryLow_Hysteresis_H_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_BatVolVeryLow_Hysteresis_H_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:BatVolVeryLow_Hysteresis_H=%d",BatVolVeryLow_Hysteresis_H);
}

static void EepCopy_BatVolVeryLow_Hysteresis_H_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.BatVolVeryLow_Hysteresis_H = eep_ram_image.BatVolVeryLow_Hysteresis_H;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_BatVolVeryLow_Hysteresis_H_IN_BLOCK_NUM);
}
#endif
//static void Proc_BatVolVeryLow_Hysteresis_H_RestoreToDefault(void)
//{
//#if EEP_CONTENT_BatVolVeryLow_Hysteresis_H_CONST_VARIBLE
//	GenEep_Set_BatVolVeryLow_Hysteresis_H(dflt_eep_val.BatVolVeryLow_Hysteresis_H);
//#endif	
//}
#if EEP_CONTENT_BatVolVeryLow_Hysteresis_H_CONST_VARIBLE
#define Proc_BatVolVeryLow_Hysteresis_H_RestoreToDefault()	do{GenEep_Set_BatVolVeryLow_Hysteresis_H(dflt_eep_val.BatVolVeryLow_Hysteresis_H);}while(0)
#else
#define Proc_BatVolVeryLow_Hysteresis_H_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> BatVolVeryLow_Hysteresis_H function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> BatVolVeryLow_Hysteresis_L function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_BatVolVeryLow_Hysteresis_L_ITEM_LEN > 1
void GenEep_Get_BatVolVeryLow_Hysteresis_L(uint16_t * const BatVolVeryLow_Hysteresis_L )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_BatVolVeryLow_Hysteresis_L_ITEM_LEN; i ++)
    {
        BatVolVeryLow_Hysteresis_L[i] = eep_ram_image.BatVolVeryLow_Hysteresis_L[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_BatVolVeryLow_Hysteresis_L_Ex(uint8_t * const BatVolVeryLow_Hysteresis_L,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.BatVolVeryLow_Hysteresis_L;	//EEP_CONTENT_BatVolVeryLow_Hysteresis_L_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_BatVolVeryLow_Hysteresis_L_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_BatVolVeryLow_Hysteresis_L_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        BatVolVeryLow_Hysteresis_L[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_BatVolVeryLow_Hysteresis_L(uint16_t const* BatVolVeryLow_Hysteresis_L )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_BatVolVeryLow_Hysteresis_L_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.BatVolVeryLow_Hysteresis_L[i] != BatVolVeryLow_Hysteresis_L[i] )
        {
            eep_ram_image.BatVolVeryLow_Hysteresis_L[i] = BatVolVeryLow_Hysteresis_L[i];
            wr_eep_by_list_req[EEP_CONTENT_BatVolVeryLow_Hysteresis_L_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_BatVolVeryLow_Hysteresis_L_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_BatVolVeryLow_Hysteresis_L_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_BatVolVeryLow_Hysteresis_L_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:BatVolVeryLow_Hysteresis_L firt item[0] = %d",BatVolVeryLow_Hysteresis_L[0]);
}

void GenEep_Set_BatVolVeryLow_Hysteresis_L_Ex(uint8_t const* BatVolVeryLow_Hysteresis_L,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_BatVolVeryLow_Hysteresis_L_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_BatVolVeryLow_Hysteresis_L_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.BatVolVeryLow_Hysteresis_L;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != BatVolVeryLow_Hysteresis_L[i] )
        {
            dest[offset+i] = BatVolVeryLow_Hysteresis_L[i];
            wr_eep_by_list_req[EEP_CONTENT_BatVolVeryLow_Hysteresis_L_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_BatVolVeryLow_Hysteresis_L_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_BatVolVeryLow_Hysteresis_L_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_BatVolVeryLow_Hysteresis_L_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:BatVolVeryLow_Hysteresis_L by Extend Api");
}

static void EepCopy_BatVolVeryLow_Hysteresis_L_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_BatVolVeryLow_Hysteresis_L_ITEM_LEN; i ++)
    {
        eep_hw_image.BatVolVeryLow_Hysteresis_L[i] = eep_ram_image.BatVolVeryLow_Hysteresis_L[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_BatVolVeryLow_Hysteresis_L_IN_BLOCK_NUM);
}

#else 
uint16_t GenEep_Get_BatVolVeryLow_Hysteresis_L(void)
{
    uint16_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.BatVolVeryLow_Hysteresis_L;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_BatVolVeryLow_Hysteresis_L(uint16_t const BatVolVeryLow_Hysteresis_L)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.BatVolVeryLow_Hysteresis_L != BatVolVeryLow_Hysteresis_L )
    {
        eep_ram_image.BatVolVeryLow_Hysteresis_L = BatVolVeryLow_Hysteresis_L;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_BatVolVeryLow_Hysteresis_L_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_BatVolVeryLow_Hysteresis_L_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_BatVolVeryLow_Hysteresis_L_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_BatVolVeryLow_Hysteresis_L_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:BatVolVeryLow_Hysteresis_L=%d",BatVolVeryLow_Hysteresis_L);
}

static void EepCopy_BatVolVeryLow_Hysteresis_L_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.BatVolVeryLow_Hysteresis_L = eep_ram_image.BatVolVeryLow_Hysteresis_L;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_BatVolVeryLow_Hysteresis_L_IN_BLOCK_NUM);
}
#endif
//static void Proc_BatVolVeryLow_Hysteresis_L_RestoreToDefault(void)
//{
//#if EEP_CONTENT_BatVolVeryLow_Hysteresis_L_CONST_VARIBLE
//	GenEep_Set_BatVolVeryLow_Hysteresis_L(dflt_eep_val.BatVolVeryLow_Hysteresis_L);
//#endif	
//}
#if EEP_CONTENT_BatVolVeryLow_Hysteresis_L_CONST_VARIBLE
#define Proc_BatVolVeryLow_Hysteresis_L_RestoreToDefault()	do{GenEep_Set_BatVolVeryLow_Hysteresis_L(dflt_eep_val.BatVolVeryLow_Hysteresis_L);}while(0)
#else
#define Proc_BatVolVeryLow_Hysteresis_L_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> BatVolVeryLow_Hysteresis_L function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> TempState_ChgDly function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_TempState_ChgDly_ITEM_LEN > 1
void GenEep_Get_TempState_ChgDly(uint8_t * const TempState_ChgDly )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_TempState_ChgDly_ITEM_LEN; i ++)
    {
        TempState_ChgDly[i] = eep_ram_image.TempState_ChgDly[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_TempState_ChgDly_Ex(uint8_t * const TempState_ChgDly,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.TempState_ChgDly;	//EEP_CONTENT_TempState_ChgDly_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_TempState_ChgDly_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_TempState_ChgDly_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        TempState_ChgDly[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_TempState_ChgDly(uint8_t const* TempState_ChgDly )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_TempState_ChgDly_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.TempState_ChgDly[i] != TempState_ChgDly[i] )
        {
            eep_ram_image.TempState_ChgDly[i] = TempState_ChgDly[i];
            wr_eep_by_list_req[EEP_CONTENT_TempState_ChgDly_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_TempState_ChgDly_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_TempState_ChgDly_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_TempState_ChgDly_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:TempState_ChgDly firt item[0] = %d",TempState_ChgDly[0]);
}

void GenEep_Set_TempState_ChgDly_Ex(uint8_t const* TempState_ChgDly,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_TempState_ChgDly_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_TempState_ChgDly_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.TempState_ChgDly;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != TempState_ChgDly[i] )
        {
            dest[offset+i] = TempState_ChgDly[i];
            wr_eep_by_list_req[EEP_CONTENT_TempState_ChgDly_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_TempState_ChgDly_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_TempState_ChgDly_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_TempState_ChgDly_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:TempState_ChgDly by Extend Api");
}

static void EepCopy_TempState_ChgDly_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_TempState_ChgDly_ITEM_LEN; i ++)
    {
        eep_hw_image.TempState_ChgDly[i] = eep_ram_image.TempState_ChgDly[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_TempState_ChgDly_IN_BLOCK_NUM);
}

#else 
uint8_t GenEep_Get_TempState_ChgDly(void)
{
    uint8_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.TempState_ChgDly;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_TempState_ChgDly(uint8_t const TempState_ChgDly)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.TempState_ChgDly != TempState_ChgDly )
    {
        eep_ram_image.TempState_ChgDly = TempState_ChgDly;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_TempState_ChgDly_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_TempState_ChgDly_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_TempState_ChgDly_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_TempState_ChgDly_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:TempState_ChgDly=%d",TempState_ChgDly);
}

static void EepCopy_TempState_ChgDly_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.TempState_ChgDly = eep_ram_image.TempState_ChgDly;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_TempState_ChgDly_IN_BLOCK_NUM);
}
#endif
//static void Proc_TempState_ChgDly_RestoreToDefault(void)
//{
//#if EEP_CONTENT_TempState_ChgDly_CONST_VARIBLE
//	GenEep_Set_TempState_ChgDly(dflt_eep_val.TempState_ChgDly);
//#endif	
//}
#if EEP_CONTENT_TempState_ChgDly_CONST_VARIBLE
#define Proc_TempState_ChgDly_RestoreToDefault()	do{GenEep_Set_TempState_ChgDly(dflt_eep_val.TempState_ChgDly);}while(0)
#else
#define Proc_TempState_ChgDly_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> TempState_ChgDly function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> TempDegC_Low_Hysteresis_L function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_TempDegC_Low_Hysteresis_L_ITEM_LEN > 1
void GenEep_Get_TempDegC_Low_Hysteresis_L(uint8_t * const TempDegC_Low_Hysteresis_L )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_TempDegC_Low_Hysteresis_L_ITEM_LEN; i ++)
    {
        TempDegC_Low_Hysteresis_L[i] = eep_ram_image.TempDegC_Low_Hysteresis_L[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_TempDegC_Low_Hysteresis_L_Ex(uint8_t * const TempDegC_Low_Hysteresis_L,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.TempDegC_Low_Hysteresis_L;	//EEP_CONTENT_TempDegC_Low_Hysteresis_L_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_TempDegC_Low_Hysteresis_L_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_TempDegC_Low_Hysteresis_L_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        TempDegC_Low_Hysteresis_L[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_TempDegC_Low_Hysteresis_L(uint8_t const* TempDegC_Low_Hysteresis_L )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_TempDegC_Low_Hysteresis_L_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.TempDegC_Low_Hysteresis_L[i] != TempDegC_Low_Hysteresis_L[i] )
        {
            eep_ram_image.TempDegC_Low_Hysteresis_L[i] = TempDegC_Low_Hysteresis_L[i];
            wr_eep_by_list_req[EEP_CONTENT_TempDegC_Low_Hysteresis_L_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_TempDegC_Low_Hysteresis_L_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_TempDegC_Low_Hysteresis_L_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_TempDegC_Low_Hysteresis_L_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:TempDegC_Low_Hysteresis_L firt item[0] = %d",TempDegC_Low_Hysteresis_L[0]);
}

void GenEep_Set_TempDegC_Low_Hysteresis_L_Ex(uint8_t const* TempDegC_Low_Hysteresis_L,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_TempDegC_Low_Hysteresis_L_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_TempDegC_Low_Hysteresis_L_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.TempDegC_Low_Hysteresis_L;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != TempDegC_Low_Hysteresis_L[i] )
        {
            dest[offset+i] = TempDegC_Low_Hysteresis_L[i];
            wr_eep_by_list_req[EEP_CONTENT_TempDegC_Low_Hysteresis_L_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_TempDegC_Low_Hysteresis_L_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_TempDegC_Low_Hysteresis_L_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_TempDegC_Low_Hysteresis_L_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:TempDegC_Low_Hysteresis_L by Extend Api");
}

static void EepCopy_TempDegC_Low_Hysteresis_L_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_TempDegC_Low_Hysteresis_L_ITEM_LEN; i ++)
    {
        eep_hw_image.TempDegC_Low_Hysteresis_L[i] = eep_ram_image.TempDegC_Low_Hysteresis_L[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_TempDegC_Low_Hysteresis_L_IN_BLOCK_NUM);
}

#else 
uint8_t GenEep_Get_TempDegC_Low_Hysteresis_L(void)
{
    uint8_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.TempDegC_Low_Hysteresis_L;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_TempDegC_Low_Hysteresis_L(uint8_t const TempDegC_Low_Hysteresis_L)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.TempDegC_Low_Hysteresis_L != TempDegC_Low_Hysteresis_L )
    {
        eep_ram_image.TempDegC_Low_Hysteresis_L = TempDegC_Low_Hysteresis_L;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_TempDegC_Low_Hysteresis_L_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_TempDegC_Low_Hysteresis_L_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_TempDegC_Low_Hysteresis_L_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_TempDegC_Low_Hysteresis_L_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:TempDegC_Low_Hysteresis_L=%d",TempDegC_Low_Hysteresis_L);
}

static void EepCopy_TempDegC_Low_Hysteresis_L_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.TempDegC_Low_Hysteresis_L = eep_ram_image.TempDegC_Low_Hysteresis_L;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_TempDegC_Low_Hysteresis_L_IN_BLOCK_NUM);
}
#endif
//static void Proc_TempDegC_Low_Hysteresis_L_RestoreToDefault(void)
//{
//#if EEP_CONTENT_TempDegC_Low_Hysteresis_L_CONST_VARIBLE
//	GenEep_Set_TempDegC_Low_Hysteresis_L(dflt_eep_val.TempDegC_Low_Hysteresis_L);
//#endif	
//}
#if EEP_CONTENT_TempDegC_Low_Hysteresis_L_CONST_VARIBLE
#define Proc_TempDegC_Low_Hysteresis_L_RestoreToDefault()	do{GenEep_Set_TempDegC_Low_Hysteresis_L(dflt_eep_val.TempDegC_Low_Hysteresis_L);}while(0)
#else
#define Proc_TempDegC_Low_Hysteresis_L_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> TempDegC_Low_Hysteresis_L function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> TempDegC_Low_Hysteresis_H function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_TempDegC_Low_Hysteresis_H_ITEM_LEN > 1
void GenEep_Get_TempDegC_Low_Hysteresis_H(uint8_t * const TempDegC_Low_Hysteresis_H )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_TempDegC_Low_Hysteresis_H_ITEM_LEN; i ++)
    {
        TempDegC_Low_Hysteresis_H[i] = eep_ram_image.TempDegC_Low_Hysteresis_H[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_TempDegC_Low_Hysteresis_H_Ex(uint8_t * const TempDegC_Low_Hysteresis_H,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.TempDegC_Low_Hysteresis_H;	//EEP_CONTENT_TempDegC_Low_Hysteresis_H_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_TempDegC_Low_Hysteresis_H_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_TempDegC_Low_Hysteresis_H_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        TempDegC_Low_Hysteresis_H[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_TempDegC_Low_Hysteresis_H(uint8_t const* TempDegC_Low_Hysteresis_H )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_TempDegC_Low_Hysteresis_H_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.TempDegC_Low_Hysteresis_H[i] != TempDegC_Low_Hysteresis_H[i] )
        {
            eep_ram_image.TempDegC_Low_Hysteresis_H[i] = TempDegC_Low_Hysteresis_H[i];
            wr_eep_by_list_req[EEP_CONTENT_TempDegC_Low_Hysteresis_H_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_TempDegC_Low_Hysteresis_H_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_TempDegC_Low_Hysteresis_H_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_TempDegC_Low_Hysteresis_H_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:TempDegC_Low_Hysteresis_H firt item[0] = %d",TempDegC_Low_Hysteresis_H[0]);
}

void GenEep_Set_TempDegC_Low_Hysteresis_H_Ex(uint8_t const* TempDegC_Low_Hysteresis_H,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_TempDegC_Low_Hysteresis_H_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_TempDegC_Low_Hysteresis_H_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.TempDegC_Low_Hysteresis_H;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != TempDegC_Low_Hysteresis_H[i] )
        {
            dest[offset+i] = TempDegC_Low_Hysteresis_H[i];
            wr_eep_by_list_req[EEP_CONTENT_TempDegC_Low_Hysteresis_H_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_TempDegC_Low_Hysteresis_H_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_TempDegC_Low_Hysteresis_H_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_TempDegC_Low_Hysteresis_H_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:TempDegC_Low_Hysteresis_H by Extend Api");
}

static void EepCopy_TempDegC_Low_Hysteresis_H_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_TempDegC_Low_Hysteresis_H_ITEM_LEN; i ++)
    {
        eep_hw_image.TempDegC_Low_Hysteresis_H[i] = eep_ram_image.TempDegC_Low_Hysteresis_H[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_TempDegC_Low_Hysteresis_H_IN_BLOCK_NUM);
}

#else 
uint8_t GenEep_Get_TempDegC_Low_Hysteresis_H(void)
{
    uint8_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.TempDegC_Low_Hysteresis_H;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_TempDegC_Low_Hysteresis_H(uint8_t const TempDegC_Low_Hysteresis_H)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.TempDegC_Low_Hysteresis_H != TempDegC_Low_Hysteresis_H )
    {
        eep_ram_image.TempDegC_Low_Hysteresis_H = TempDegC_Low_Hysteresis_H;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_TempDegC_Low_Hysteresis_H_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_TempDegC_Low_Hysteresis_H_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_TempDegC_Low_Hysteresis_H_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_TempDegC_Low_Hysteresis_H_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:TempDegC_Low_Hysteresis_H=%d",TempDegC_Low_Hysteresis_H);
}

static void EepCopy_TempDegC_Low_Hysteresis_H_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.TempDegC_Low_Hysteresis_H = eep_ram_image.TempDegC_Low_Hysteresis_H;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_TempDegC_Low_Hysteresis_H_IN_BLOCK_NUM);
}
#endif
//static void Proc_TempDegC_Low_Hysteresis_H_RestoreToDefault(void)
//{
//#if EEP_CONTENT_TempDegC_Low_Hysteresis_H_CONST_VARIBLE
//	GenEep_Set_TempDegC_Low_Hysteresis_H(dflt_eep_val.TempDegC_Low_Hysteresis_H);
//#endif	
//}
#if EEP_CONTENT_TempDegC_Low_Hysteresis_H_CONST_VARIBLE
#define Proc_TempDegC_Low_Hysteresis_H_RestoreToDefault()	do{GenEep_Set_TempDegC_Low_Hysteresis_H(dflt_eep_val.TempDegC_Low_Hysteresis_H);}while(0)
#else
#define Proc_TempDegC_Low_Hysteresis_H_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> TempDegC_Low_Hysteresis_H function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> TempDegC_High_Hysteresis_L function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_TempDegC_High_Hysteresis_L_ITEM_LEN > 1
void GenEep_Get_TempDegC_High_Hysteresis_L(uint8_t * const TempDegC_High_Hysteresis_L )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_TempDegC_High_Hysteresis_L_ITEM_LEN; i ++)
    {
        TempDegC_High_Hysteresis_L[i] = eep_ram_image.TempDegC_High_Hysteresis_L[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_TempDegC_High_Hysteresis_L_Ex(uint8_t * const TempDegC_High_Hysteresis_L,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.TempDegC_High_Hysteresis_L;	//EEP_CONTENT_TempDegC_High_Hysteresis_L_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_TempDegC_High_Hysteresis_L_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_TempDegC_High_Hysteresis_L_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        TempDegC_High_Hysteresis_L[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_TempDegC_High_Hysteresis_L(uint8_t const* TempDegC_High_Hysteresis_L )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_TempDegC_High_Hysteresis_L_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.TempDegC_High_Hysteresis_L[i] != TempDegC_High_Hysteresis_L[i] )
        {
            eep_ram_image.TempDegC_High_Hysteresis_L[i] = TempDegC_High_Hysteresis_L[i];
            wr_eep_by_list_req[EEP_CONTENT_TempDegC_High_Hysteresis_L_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_TempDegC_High_Hysteresis_L_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_TempDegC_High_Hysteresis_L_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_TempDegC_High_Hysteresis_L_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:TempDegC_High_Hysteresis_L firt item[0] = %d",TempDegC_High_Hysteresis_L[0]);
}

void GenEep_Set_TempDegC_High_Hysteresis_L_Ex(uint8_t const* TempDegC_High_Hysteresis_L,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_TempDegC_High_Hysteresis_L_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_TempDegC_High_Hysteresis_L_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.TempDegC_High_Hysteresis_L;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != TempDegC_High_Hysteresis_L[i] )
        {
            dest[offset+i] = TempDegC_High_Hysteresis_L[i];
            wr_eep_by_list_req[EEP_CONTENT_TempDegC_High_Hysteresis_L_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_TempDegC_High_Hysteresis_L_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_TempDegC_High_Hysteresis_L_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_TempDegC_High_Hysteresis_L_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:TempDegC_High_Hysteresis_L by Extend Api");
}

static void EepCopy_TempDegC_High_Hysteresis_L_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_TempDegC_High_Hysteresis_L_ITEM_LEN; i ++)
    {
        eep_hw_image.TempDegC_High_Hysteresis_L[i] = eep_ram_image.TempDegC_High_Hysteresis_L[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_TempDegC_High_Hysteresis_L_IN_BLOCK_NUM);
}

#else 
uint8_t GenEep_Get_TempDegC_High_Hysteresis_L(void)
{
    uint8_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.TempDegC_High_Hysteresis_L;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_TempDegC_High_Hysteresis_L(uint8_t const TempDegC_High_Hysteresis_L)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.TempDegC_High_Hysteresis_L != TempDegC_High_Hysteresis_L )
    {
        eep_ram_image.TempDegC_High_Hysteresis_L = TempDegC_High_Hysteresis_L;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_TempDegC_High_Hysteresis_L_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_TempDegC_High_Hysteresis_L_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_TempDegC_High_Hysteresis_L_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_TempDegC_High_Hysteresis_L_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:TempDegC_High_Hysteresis_L=%d",TempDegC_High_Hysteresis_L);
}

static void EepCopy_TempDegC_High_Hysteresis_L_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.TempDegC_High_Hysteresis_L = eep_ram_image.TempDegC_High_Hysteresis_L;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_TempDegC_High_Hysteresis_L_IN_BLOCK_NUM);
}
#endif
//static void Proc_TempDegC_High_Hysteresis_L_RestoreToDefault(void)
//{
//#if EEP_CONTENT_TempDegC_High_Hysteresis_L_CONST_VARIBLE
//	GenEep_Set_TempDegC_High_Hysteresis_L(dflt_eep_val.TempDegC_High_Hysteresis_L);
//#endif	
//}
#if EEP_CONTENT_TempDegC_High_Hysteresis_L_CONST_VARIBLE
#define Proc_TempDegC_High_Hysteresis_L_RestoreToDefault()	do{GenEep_Set_TempDegC_High_Hysteresis_L(dflt_eep_val.TempDegC_High_Hysteresis_L);}while(0)
#else
#define Proc_TempDegC_High_Hysteresis_L_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> TempDegC_High_Hysteresis_L function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> TempDegC_High_Hysteresis_H function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_TempDegC_High_Hysteresis_H_ITEM_LEN > 1
void GenEep_Get_TempDegC_High_Hysteresis_H(uint8_t * const TempDegC_High_Hysteresis_H )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_TempDegC_High_Hysteresis_H_ITEM_LEN; i ++)
    {
        TempDegC_High_Hysteresis_H[i] = eep_ram_image.TempDegC_High_Hysteresis_H[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_TempDegC_High_Hysteresis_H_Ex(uint8_t * const TempDegC_High_Hysteresis_H,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.TempDegC_High_Hysteresis_H;	//EEP_CONTENT_TempDegC_High_Hysteresis_H_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_TempDegC_High_Hysteresis_H_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_TempDegC_High_Hysteresis_H_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        TempDegC_High_Hysteresis_H[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_TempDegC_High_Hysteresis_H(uint8_t const* TempDegC_High_Hysteresis_H )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_TempDegC_High_Hysteresis_H_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.TempDegC_High_Hysteresis_H[i] != TempDegC_High_Hysteresis_H[i] )
        {
            eep_ram_image.TempDegC_High_Hysteresis_H[i] = TempDegC_High_Hysteresis_H[i];
            wr_eep_by_list_req[EEP_CONTENT_TempDegC_High_Hysteresis_H_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_TempDegC_High_Hysteresis_H_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_TempDegC_High_Hysteresis_H_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_TempDegC_High_Hysteresis_H_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:TempDegC_High_Hysteresis_H firt item[0] = %d",TempDegC_High_Hysteresis_H[0]);
}

void GenEep_Set_TempDegC_High_Hysteresis_H_Ex(uint8_t const* TempDegC_High_Hysteresis_H,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_TempDegC_High_Hysteresis_H_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_TempDegC_High_Hysteresis_H_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.TempDegC_High_Hysteresis_H;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != TempDegC_High_Hysteresis_H[i] )
        {
            dest[offset+i] = TempDegC_High_Hysteresis_H[i];
            wr_eep_by_list_req[EEP_CONTENT_TempDegC_High_Hysteresis_H_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_TempDegC_High_Hysteresis_H_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_TempDegC_High_Hysteresis_H_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_TempDegC_High_Hysteresis_H_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:TempDegC_High_Hysteresis_H by Extend Api");
}

static void EepCopy_TempDegC_High_Hysteresis_H_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_TempDegC_High_Hysteresis_H_ITEM_LEN; i ++)
    {
        eep_hw_image.TempDegC_High_Hysteresis_H[i] = eep_ram_image.TempDegC_High_Hysteresis_H[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_TempDegC_High_Hysteresis_H_IN_BLOCK_NUM);
}

#else 
uint8_t GenEep_Get_TempDegC_High_Hysteresis_H(void)
{
    uint8_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.TempDegC_High_Hysteresis_H;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_TempDegC_High_Hysteresis_H(uint8_t const TempDegC_High_Hysteresis_H)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.TempDegC_High_Hysteresis_H != TempDegC_High_Hysteresis_H )
    {
        eep_ram_image.TempDegC_High_Hysteresis_H = TempDegC_High_Hysteresis_H;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_TempDegC_High_Hysteresis_H_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_TempDegC_High_Hysteresis_H_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_TempDegC_High_Hysteresis_H_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_TempDegC_High_Hysteresis_H_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:TempDegC_High_Hysteresis_H=%d",TempDegC_High_Hysteresis_H);
}

static void EepCopy_TempDegC_High_Hysteresis_H_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.TempDegC_High_Hysteresis_H = eep_ram_image.TempDegC_High_Hysteresis_H;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_TempDegC_High_Hysteresis_H_IN_BLOCK_NUM);
}
#endif
//static void Proc_TempDegC_High_Hysteresis_H_RestoreToDefault(void)
//{
//#if EEP_CONTENT_TempDegC_High_Hysteresis_H_CONST_VARIBLE
//	GenEep_Set_TempDegC_High_Hysteresis_H(dflt_eep_val.TempDegC_High_Hysteresis_H);
//#endif	
//}
#if EEP_CONTENT_TempDegC_High_Hysteresis_H_CONST_VARIBLE
#define Proc_TempDegC_High_Hysteresis_H_RestoreToDefault()	do{GenEep_Set_TempDegC_High_Hysteresis_H(dflt_eep_val.TempDegC_High_Hysteresis_H);}while(0)
#else
#define Proc_TempDegC_High_Hysteresis_H_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> TempDegC_High_Hysteresis_H function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> AmpHighTempProction function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_AmpHighTempProction_ITEM_LEN > 1
void GenEep_Get_AmpHighTempProction(uint32_t * const AmpHighTempProction )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_AmpHighTempProction_ITEM_LEN; i ++)
    {
        AmpHighTempProction[i] = eep_ram_image.AmpHighTempProction[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_AmpHighTempProction_Ex(uint8_t * const AmpHighTempProction,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.AmpHighTempProction;	//EEP_CONTENT_AmpHighTempProction_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_AmpHighTempProction_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_AmpHighTempProction_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        AmpHighTempProction[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_AmpHighTempProction(uint32_t const* AmpHighTempProction )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_AmpHighTempProction_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.AmpHighTempProction[i] != AmpHighTempProction[i] )
        {
            eep_ram_image.AmpHighTempProction[i] = AmpHighTempProction[i];
            wr_eep_by_list_req[EEP_CONTENT_AmpHighTempProction_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_AmpHighTempProction_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_AmpHighTempProction_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_AmpHighTempProction_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:AmpHighTempProction firt item[0] = %d",AmpHighTempProction[0]);
}

void GenEep_Set_AmpHighTempProction_Ex(uint8_t const* AmpHighTempProction,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_AmpHighTempProction_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_AmpHighTempProction_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.AmpHighTempProction;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != AmpHighTempProction[i] )
        {
            dest[offset+i] = AmpHighTempProction[i];
            wr_eep_by_list_req[EEP_CONTENT_AmpHighTempProction_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_AmpHighTempProction_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_AmpHighTempProction_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_AmpHighTempProction_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:AmpHighTempProction by Extend Api");
}

static void EepCopy_AmpHighTempProction_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_AmpHighTempProction_ITEM_LEN; i ++)
    {
        eep_hw_image.AmpHighTempProction[i] = eep_ram_image.AmpHighTempProction[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_AmpHighTempProction_IN_BLOCK_NUM);
}

#else 
uint32_t GenEep_Get_AmpHighTempProction(void)
{
    uint32_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.AmpHighTempProction;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_AmpHighTempProction(uint32_t const AmpHighTempProction)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.AmpHighTempProction != AmpHighTempProction )
    {
        eep_ram_image.AmpHighTempProction = AmpHighTempProction;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_AmpHighTempProction_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_AmpHighTempProction_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_AmpHighTempProction_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_AmpHighTempProction_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:AmpHighTempProction=%d",AmpHighTempProction);
}

static void EepCopy_AmpHighTempProction_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.AmpHighTempProction = eep_ram_image.AmpHighTempProction;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_AmpHighTempProction_IN_BLOCK_NUM);
}
#endif
//static void Proc_AmpHighTempProction_RestoreToDefault(void)
//{
//#if EEP_CONTENT_AmpHighTempProction_CONST_VARIBLE
//	GenEep_Set_AmpHighTempProction(dflt_eep_val.AmpHighTempProction);
//#endif	
//}
#if EEP_CONTENT_AmpHighTempProction_CONST_VARIBLE
#define Proc_AmpHighTempProction_RestoreToDefault()	do{GenEep_Set_AmpHighTempProction(dflt_eep_val.AmpHighTempProction);}while(0)
#else
#define Proc_AmpHighTempProction_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> AmpHighTempProction function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> CanNm_DA_S1_Delay_ms function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_CanNm_DA_S1_Delay_ms_ITEM_LEN > 1
void GenEep_Get_CanNm_DA_S1_Delay_ms(uint16_t * const CanNm_DA_S1_Delay_ms )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_CanNm_DA_S1_Delay_ms_ITEM_LEN; i ++)
    {
        CanNm_DA_S1_Delay_ms[i] = eep_ram_image.CanNm_DA_S1_Delay_ms[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_CanNm_DA_S1_Delay_ms_Ex(uint8_t * const CanNm_DA_S1_Delay_ms,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.CanNm_DA_S1_Delay_ms;	//EEP_CONTENT_CanNm_DA_S1_Delay_ms_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_CanNm_DA_S1_Delay_ms_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_CanNm_DA_S1_Delay_ms_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        CanNm_DA_S1_Delay_ms[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_CanNm_DA_S1_Delay_ms(uint16_t const* CanNm_DA_S1_Delay_ms )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_CanNm_DA_S1_Delay_ms_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.CanNm_DA_S1_Delay_ms[i] != CanNm_DA_S1_Delay_ms[i] )
        {
            eep_ram_image.CanNm_DA_S1_Delay_ms[i] = CanNm_DA_S1_Delay_ms[i];
            wr_eep_by_list_req[EEP_CONTENT_CanNm_DA_S1_Delay_ms_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_CanNm_DA_S1_Delay_ms_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_CanNm_DA_S1_Delay_ms_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_CanNm_DA_S1_Delay_ms_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:CanNm_DA_S1_Delay_ms firt item[0] = %d",CanNm_DA_S1_Delay_ms[0]);
}

void GenEep_Set_CanNm_DA_S1_Delay_ms_Ex(uint8_t const* CanNm_DA_S1_Delay_ms,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_CanNm_DA_S1_Delay_ms_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_CanNm_DA_S1_Delay_ms_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.CanNm_DA_S1_Delay_ms;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != CanNm_DA_S1_Delay_ms[i] )
        {
            dest[offset+i] = CanNm_DA_S1_Delay_ms[i];
            wr_eep_by_list_req[EEP_CONTENT_CanNm_DA_S1_Delay_ms_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_CanNm_DA_S1_Delay_ms_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_CanNm_DA_S1_Delay_ms_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_CanNm_DA_S1_Delay_ms_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:CanNm_DA_S1_Delay_ms by Extend Api");
}

static void EepCopy_CanNm_DA_S1_Delay_ms_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_CanNm_DA_S1_Delay_ms_ITEM_LEN; i ++)
    {
        eep_hw_image.CanNm_DA_S1_Delay_ms[i] = eep_ram_image.CanNm_DA_S1_Delay_ms[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_CanNm_DA_S1_Delay_ms_IN_BLOCK_NUM);
}

#else 
uint16_t GenEep_Get_CanNm_DA_S1_Delay_ms(void)
{
    uint16_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.CanNm_DA_S1_Delay_ms;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_CanNm_DA_S1_Delay_ms(uint16_t const CanNm_DA_S1_Delay_ms)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.CanNm_DA_S1_Delay_ms != CanNm_DA_S1_Delay_ms )
    {
        eep_ram_image.CanNm_DA_S1_Delay_ms = CanNm_DA_S1_Delay_ms;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_CanNm_DA_S1_Delay_ms_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_CanNm_DA_S1_Delay_ms_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_CanNm_DA_S1_Delay_ms_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_CanNm_DA_S1_Delay_ms_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:CanNm_DA_S1_Delay_ms=%d",CanNm_DA_S1_Delay_ms);
}

static void EepCopy_CanNm_DA_S1_Delay_ms_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.CanNm_DA_S1_Delay_ms = eep_ram_image.CanNm_DA_S1_Delay_ms;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_CanNm_DA_S1_Delay_ms_IN_BLOCK_NUM);
}
#endif
//static void Proc_CanNm_DA_S1_Delay_ms_RestoreToDefault(void)
//{
//#if EEP_CONTENT_CanNm_DA_S1_Delay_ms_CONST_VARIBLE
//	GenEep_Set_CanNm_DA_S1_Delay_ms(dflt_eep_val.CanNm_DA_S1_Delay_ms);
//#endif	
//}
#if EEP_CONTENT_CanNm_DA_S1_Delay_ms_CONST_VARIBLE
#define Proc_CanNm_DA_S1_Delay_ms_RestoreToDefault()	do{GenEep_Set_CanNm_DA_S1_Delay_ms(dflt_eep_val.CanNm_DA_S1_Delay_ms);}while(0)
#else
#define Proc_CanNm_DA_S1_Delay_ms_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> CanNm_DA_S1_Delay_ms function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

uint32_t GenEep_Get_OemSetting_write_count(void)
{
	return eep_ram_image.OemSetting_write_count;
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> Vin function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_Vin_ITEM_LEN > 1
void GenEep_Get_Vin(uint8_t * const Vin )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_Vin_ITEM_LEN; i ++)
    {
        Vin[i] = eep_ram_image.Vin[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_Vin_Ex(uint8_t * const Vin,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.Vin;	//EEP_CONTENT_Vin_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_Vin_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_Vin_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        Vin[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_Vin(uint8_t const* Vin )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_Vin_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.Vin[i] != Vin[i] )
        {
            eep_ram_image.Vin[i] = Vin[i];
            wr_eep_by_list_req[EEP_CONTENT_Vin_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_Vin_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_Vin_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_Vin_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:Vin firt item[0] = %d",Vin[0]);
}

void GenEep_Set_Vin_Ex(uint8_t const* Vin,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_Vin_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_Vin_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.Vin;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != Vin[i] )
        {
            dest[offset+i] = Vin[i];
            wr_eep_by_list_req[EEP_CONTENT_Vin_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_Vin_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_Vin_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_Vin_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:Vin by Extend Api");
}

static void EepCopy_Vin_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_Vin_ITEM_LEN; i ++)
    {
        eep_hw_image.Vin[i] = eep_ram_image.Vin[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_Vin_IN_BLOCK_NUM);
}

#else 
uint8_t GenEep_Get_Vin(void)
{
    uint8_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.Vin;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_Vin(uint8_t const Vin)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.Vin != Vin )
    {
        eep_ram_image.Vin = Vin;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_Vin_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_Vin_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_Vin_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_Vin_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:Vin=%d",Vin);
}

static void EepCopy_Vin_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.Vin = eep_ram_image.Vin;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_Vin_IN_BLOCK_NUM);
}
#endif
//static void Proc_Vin_RestoreToDefault(void)
//{
//#if EEP_CONTENT_Vin_CONST_VARIBLE
//	GenEep_Set_Vin(dflt_eep_val.Vin);
//#endif	
//}
#if EEP_CONTENT_Vin_CONST_VARIBLE
#define Proc_Vin_RestoreToDefault()	do{GenEep_Set_Vin(dflt_eep_val.Vin);}while(0)
#else
#define Proc_Vin_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> Vin function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

uint32_t GenEep_Get_user_setting_write_count(void)
{
	return eep_ram_image.user_setting_write_count;
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> AutoSyncTimeWithGps function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_AutoSyncTimeWithGps_ITEM_LEN > 1
void GenEep_Get_AutoSyncTimeWithGps(uint8_t * const AutoSyncTimeWithGps )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_AutoSyncTimeWithGps_ITEM_LEN; i ++)
    {
        AutoSyncTimeWithGps[i] = eep_ram_image.AutoSyncTimeWithGps[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_AutoSyncTimeWithGps_Ex(uint8_t * const AutoSyncTimeWithGps,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.AutoSyncTimeWithGps;	//EEP_CONTENT_AutoSyncTimeWithGps_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_AutoSyncTimeWithGps_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_AutoSyncTimeWithGps_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        AutoSyncTimeWithGps[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_AutoSyncTimeWithGps(uint8_t const* AutoSyncTimeWithGps )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_AutoSyncTimeWithGps_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.AutoSyncTimeWithGps[i] != AutoSyncTimeWithGps[i] )
        {
            eep_ram_image.AutoSyncTimeWithGps[i] = AutoSyncTimeWithGps[i];
            wr_eep_by_list_req[EEP_CONTENT_AutoSyncTimeWithGps_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_AutoSyncTimeWithGps_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_AutoSyncTimeWithGps_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_AutoSyncTimeWithGps_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:AutoSyncTimeWithGps firt item[0] = %d",AutoSyncTimeWithGps[0]);
}

void GenEep_Set_AutoSyncTimeWithGps_Ex(uint8_t const* AutoSyncTimeWithGps,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_AutoSyncTimeWithGps_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_AutoSyncTimeWithGps_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.AutoSyncTimeWithGps;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != AutoSyncTimeWithGps[i] )
        {
            dest[offset+i] = AutoSyncTimeWithGps[i];
            wr_eep_by_list_req[EEP_CONTENT_AutoSyncTimeWithGps_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_AutoSyncTimeWithGps_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_AutoSyncTimeWithGps_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_AutoSyncTimeWithGps_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:AutoSyncTimeWithGps by Extend Api");
}

static void EepCopy_AutoSyncTimeWithGps_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_AutoSyncTimeWithGps_ITEM_LEN; i ++)
    {
        eep_hw_image.AutoSyncTimeWithGps[i] = eep_ram_image.AutoSyncTimeWithGps[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_AutoSyncTimeWithGps_IN_BLOCK_NUM);
}

#else 
uint8_t GenEep_Get_AutoSyncTimeWithGps(void)
{
    uint8_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.AutoSyncTimeWithGps;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_AutoSyncTimeWithGps(uint8_t const AutoSyncTimeWithGps)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.AutoSyncTimeWithGps != AutoSyncTimeWithGps )
    {
        eep_ram_image.AutoSyncTimeWithGps = AutoSyncTimeWithGps;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_AutoSyncTimeWithGps_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_AutoSyncTimeWithGps_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_AutoSyncTimeWithGps_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_AutoSyncTimeWithGps_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:AutoSyncTimeWithGps=%d",AutoSyncTimeWithGps);
}

static void EepCopy_AutoSyncTimeWithGps_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.AutoSyncTimeWithGps = eep_ram_image.AutoSyncTimeWithGps;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_AutoSyncTimeWithGps_IN_BLOCK_NUM);
}
#endif
//static void Proc_AutoSyncTimeWithGps_RestoreToDefault(void)
//{
//#if EEP_CONTENT_AutoSyncTimeWithGps_CONST_VARIBLE
//	GenEep_Set_AutoSyncTimeWithGps(dflt_eep_val.AutoSyncTimeWithGps);
//#endif	
//}
#if EEP_CONTENT_AutoSyncTimeWithGps_CONST_VARIBLE
#define Proc_AutoSyncTimeWithGps_RestoreToDefault()	do{GenEep_Set_AutoSyncTimeWithGps(dflt_eep_val.AutoSyncTimeWithGps);}while(0)
#else
#define Proc_AutoSyncTimeWithGps_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> AutoSyncTimeWithGps function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> ScreenBackLightValOnDay function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_ScreenBackLightValOnDay_ITEM_LEN > 1
void GenEep_Get_ScreenBackLightValOnDay(uint8_t * const ScreenBackLightValOnDay )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_ScreenBackLightValOnDay_ITEM_LEN; i ++)
    {
        ScreenBackLightValOnDay[i] = eep_ram_image.ScreenBackLightValOnDay[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_ScreenBackLightValOnDay_Ex(uint8_t * const ScreenBackLightValOnDay,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.ScreenBackLightValOnDay;	//EEP_CONTENT_ScreenBackLightValOnDay_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_ScreenBackLightValOnDay_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_ScreenBackLightValOnDay_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        ScreenBackLightValOnDay[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_ScreenBackLightValOnDay(uint8_t const* ScreenBackLightValOnDay )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_ScreenBackLightValOnDay_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.ScreenBackLightValOnDay[i] != ScreenBackLightValOnDay[i] )
        {
            eep_ram_image.ScreenBackLightValOnDay[i] = ScreenBackLightValOnDay[i];
            wr_eep_by_list_req[EEP_CONTENT_ScreenBackLightValOnDay_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_ScreenBackLightValOnDay_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_ScreenBackLightValOnDay_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_ScreenBackLightValOnDay_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:ScreenBackLightValOnDay firt item[0] = %d",ScreenBackLightValOnDay[0]);
}

void GenEep_Set_ScreenBackLightValOnDay_Ex(uint8_t const* ScreenBackLightValOnDay,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_ScreenBackLightValOnDay_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_ScreenBackLightValOnDay_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.ScreenBackLightValOnDay;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != ScreenBackLightValOnDay[i] )
        {
            dest[offset+i] = ScreenBackLightValOnDay[i];
            wr_eep_by_list_req[EEP_CONTENT_ScreenBackLightValOnDay_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_ScreenBackLightValOnDay_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_ScreenBackLightValOnDay_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_ScreenBackLightValOnDay_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:ScreenBackLightValOnDay by Extend Api");
}

static void EepCopy_ScreenBackLightValOnDay_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_ScreenBackLightValOnDay_ITEM_LEN; i ++)
    {
        eep_hw_image.ScreenBackLightValOnDay[i] = eep_ram_image.ScreenBackLightValOnDay[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_ScreenBackLightValOnDay_IN_BLOCK_NUM);
}

#else 
uint8_t GenEep_Get_ScreenBackLightValOnDay(void)
{
    uint8_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.ScreenBackLightValOnDay;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_ScreenBackLightValOnDay(uint8_t const ScreenBackLightValOnDay)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.ScreenBackLightValOnDay != ScreenBackLightValOnDay )
    {
        eep_ram_image.ScreenBackLightValOnDay = ScreenBackLightValOnDay;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_ScreenBackLightValOnDay_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_ScreenBackLightValOnDay_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_ScreenBackLightValOnDay_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_ScreenBackLightValOnDay_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:ScreenBackLightValOnDay=%d",ScreenBackLightValOnDay);
}

static void EepCopy_ScreenBackLightValOnDay_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.ScreenBackLightValOnDay = eep_ram_image.ScreenBackLightValOnDay;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_ScreenBackLightValOnDay_IN_BLOCK_NUM);
}
#endif
//static void Proc_ScreenBackLightValOnDay_RestoreToDefault(void)
//{
//#if EEP_CONTENT_ScreenBackLightValOnDay_CONST_VARIBLE
//	GenEep_Set_ScreenBackLightValOnDay(dflt_eep_val.ScreenBackLightValOnDay);
//#endif	
//}
#if EEP_CONTENT_ScreenBackLightValOnDay_CONST_VARIBLE
#define Proc_ScreenBackLightValOnDay_RestoreToDefault()	do{GenEep_Set_ScreenBackLightValOnDay(dflt_eep_val.ScreenBackLightValOnDay);}while(0)
#else
#define Proc_ScreenBackLightValOnDay_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> ScreenBackLightValOnDay function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> ScreenBackLightValOnNight function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_ScreenBackLightValOnNight_ITEM_LEN > 1
void GenEep_Get_ScreenBackLightValOnNight(uint8_t * const ScreenBackLightValOnNight )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_ScreenBackLightValOnNight_ITEM_LEN; i ++)
    {
        ScreenBackLightValOnNight[i] = eep_ram_image.ScreenBackLightValOnNight[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_ScreenBackLightValOnNight_Ex(uint8_t * const ScreenBackLightValOnNight,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.ScreenBackLightValOnNight;	//EEP_CONTENT_ScreenBackLightValOnNight_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_ScreenBackLightValOnNight_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_ScreenBackLightValOnNight_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        ScreenBackLightValOnNight[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_ScreenBackLightValOnNight(uint8_t const* ScreenBackLightValOnNight )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_ScreenBackLightValOnNight_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.ScreenBackLightValOnNight[i] != ScreenBackLightValOnNight[i] )
        {
            eep_ram_image.ScreenBackLightValOnNight[i] = ScreenBackLightValOnNight[i];
            wr_eep_by_list_req[EEP_CONTENT_ScreenBackLightValOnNight_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_ScreenBackLightValOnNight_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_ScreenBackLightValOnNight_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_ScreenBackLightValOnNight_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:ScreenBackLightValOnNight firt item[0] = %d",ScreenBackLightValOnNight[0]);
}

void GenEep_Set_ScreenBackLightValOnNight_Ex(uint8_t const* ScreenBackLightValOnNight,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_ScreenBackLightValOnNight_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_ScreenBackLightValOnNight_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.ScreenBackLightValOnNight;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != ScreenBackLightValOnNight[i] )
        {
            dest[offset+i] = ScreenBackLightValOnNight[i];
            wr_eep_by_list_req[EEP_CONTENT_ScreenBackLightValOnNight_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_ScreenBackLightValOnNight_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_ScreenBackLightValOnNight_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_ScreenBackLightValOnNight_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:ScreenBackLightValOnNight by Extend Api");
}

static void EepCopy_ScreenBackLightValOnNight_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_ScreenBackLightValOnNight_ITEM_LEN; i ++)
    {
        eep_hw_image.ScreenBackLightValOnNight[i] = eep_ram_image.ScreenBackLightValOnNight[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_ScreenBackLightValOnNight_IN_BLOCK_NUM);
}

#else 
uint8_t GenEep_Get_ScreenBackLightValOnNight(void)
{
    uint8_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.ScreenBackLightValOnNight;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_ScreenBackLightValOnNight(uint8_t const ScreenBackLightValOnNight)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.ScreenBackLightValOnNight != ScreenBackLightValOnNight )
    {
        eep_ram_image.ScreenBackLightValOnNight = ScreenBackLightValOnNight;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_ScreenBackLightValOnNight_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_ScreenBackLightValOnNight_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_ScreenBackLightValOnNight_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_ScreenBackLightValOnNight_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:ScreenBackLightValOnNight=%d",ScreenBackLightValOnNight);
}

static void EepCopy_ScreenBackLightValOnNight_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.ScreenBackLightValOnNight = eep_ram_image.ScreenBackLightValOnNight;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_ScreenBackLightValOnNight_IN_BLOCK_NUM);
}
#endif
//static void Proc_ScreenBackLightValOnNight_RestoreToDefault(void)
//{
//#if EEP_CONTENT_ScreenBackLightValOnNight_CONST_VARIBLE
//	GenEep_Set_ScreenBackLightValOnNight(dflt_eep_val.ScreenBackLightValOnNight);
//#endif	
//}
#if EEP_CONTENT_ScreenBackLightValOnNight_CONST_VARIBLE
#define Proc_ScreenBackLightValOnNight_RestoreToDefault()	do{GenEep_Set_ScreenBackLightValOnNight(dflt_eep_val.ScreenBackLightValOnNight);}while(0)
#else
#define Proc_ScreenBackLightValOnNight_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> ScreenBackLightValOnNight function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> HistoryAverFuelCons function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_HistoryAverFuelCons_ITEM_LEN > 1
void GenEep_Get_HistoryAverFuelCons(uint8_t * const HistoryAverFuelCons )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_HistoryAverFuelCons_ITEM_LEN; i ++)
    {
        HistoryAverFuelCons[i] = eep_ram_image.HistoryAverFuelCons[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_HistoryAverFuelCons_Ex(uint8_t * const HistoryAverFuelCons,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.HistoryAverFuelCons;	//EEP_CONTENT_HistoryAverFuelCons_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_HistoryAverFuelCons_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_HistoryAverFuelCons_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        HistoryAverFuelCons[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_HistoryAverFuelCons(uint8_t const* HistoryAverFuelCons )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_HistoryAverFuelCons_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.HistoryAverFuelCons[i] != HistoryAverFuelCons[i] )
        {
            eep_ram_image.HistoryAverFuelCons[i] = HistoryAverFuelCons[i];
            wr_eep_by_list_req[EEP_CONTENT_HistoryAverFuelCons_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_HistoryAverFuelCons_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_HistoryAverFuelCons_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_HistoryAverFuelCons_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:HistoryAverFuelCons firt item[0] = %d",HistoryAverFuelCons[0]);
}

void GenEep_Set_HistoryAverFuelCons_Ex(uint8_t const* HistoryAverFuelCons,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_HistoryAverFuelCons_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_HistoryAverFuelCons_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.HistoryAverFuelCons;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != HistoryAverFuelCons[i] )
        {
            dest[offset+i] = HistoryAverFuelCons[i];
            wr_eep_by_list_req[EEP_CONTENT_HistoryAverFuelCons_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_HistoryAverFuelCons_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_HistoryAverFuelCons_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_HistoryAverFuelCons_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:HistoryAverFuelCons by Extend Api");
}

static void EepCopy_HistoryAverFuelCons_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_HistoryAverFuelCons_ITEM_LEN; i ++)
    {
        eep_hw_image.HistoryAverFuelCons[i] = eep_ram_image.HistoryAverFuelCons[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_HistoryAverFuelCons_IN_BLOCK_NUM);
}

#else 
uint8_t GenEep_Get_HistoryAverFuelCons(void)
{
    uint8_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.HistoryAverFuelCons;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_HistoryAverFuelCons(uint8_t const HistoryAverFuelCons)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.HistoryAverFuelCons != HistoryAverFuelCons )
    {
        eep_ram_image.HistoryAverFuelCons = HistoryAverFuelCons;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_HistoryAverFuelCons_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_HistoryAverFuelCons_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_HistoryAverFuelCons_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_HistoryAverFuelCons_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:HistoryAverFuelCons=%d",HistoryAverFuelCons);
}

static void EepCopy_HistoryAverFuelCons_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.HistoryAverFuelCons = eep_ram_image.HistoryAverFuelCons;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_HistoryAverFuelCons_IN_BLOCK_NUM);
}
#endif
//static void Proc_HistoryAverFuelCons_RestoreToDefault(void)
//{
//#if EEP_CONTENT_HistoryAverFuelCons_CONST_VARIBLE
//	GenEep_Set_HistoryAverFuelCons(dflt_eep_val.HistoryAverFuelCons);
//#endif	
//}
#if EEP_CONTENT_HistoryAverFuelCons_CONST_VARIBLE
#define Proc_HistoryAverFuelCons_RestoreToDefault()	do{GenEep_Set_HistoryAverFuelCons(dflt_eep_val.HistoryAverFuelCons);}while(0)
#else
#define Proc_HistoryAverFuelCons_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> HistoryAverFuelCons function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> FuelResistance function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_FuelResistance_ITEM_LEN > 1
void GenEep_Get_FuelResistance(uint32_t * const FuelResistance )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_FuelResistance_ITEM_LEN; i ++)
    {
        FuelResistance[i] = eep_ram_image.FuelResistance[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_FuelResistance_Ex(uint8_t * const FuelResistance,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.FuelResistance;	//EEP_CONTENT_FuelResistance_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_FuelResistance_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_FuelResistance_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        FuelResistance[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_FuelResistance(uint32_t const* FuelResistance )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_FuelResistance_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.FuelResistance[i] != FuelResistance[i] )
        {
            eep_ram_image.FuelResistance[i] = FuelResistance[i];
            wr_eep_by_list_req[EEP_CONTENT_FuelResistance_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_FuelResistance_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_FuelResistance_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_FuelResistance_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:FuelResistance firt item[0] = %d",FuelResistance[0]);
}

void GenEep_Set_FuelResistance_Ex(uint8_t const* FuelResistance,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_FuelResistance_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_FuelResistance_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.FuelResistance;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != FuelResistance[i] )
        {
            dest[offset+i] = FuelResistance[i];
            wr_eep_by_list_req[EEP_CONTENT_FuelResistance_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_FuelResistance_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_FuelResistance_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_FuelResistance_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:FuelResistance by Extend Api");
}

static void EepCopy_FuelResistance_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_FuelResistance_ITEM_LEN; i ++)
    {
        eep_hw_image.FuelResistance[i] = eep_ram_image.FuelResistance[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_FuelResistance_IN_BLOCK_NUM);
}

#else 
uint32_t GenEep_Get_FuelResistance(void)
{
    uint32_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.FuelResistance;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_FuelResistance(uint32_t const FuelResistance)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.FuelResistance != FuelResistance )
    {
        eep_ram_image.FuelResistance = FuelResistance;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_FuelResistance_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_FuelResistance_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_FuelResistance_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_FuelResistance_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:FuelResistance=%d",FuelResistance);
}

static void EepCopy_FuelResistance_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.FuelResistance = eep_ram_image.FuelResistance;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_FuelResistance_IN_BLOCK_NUM);
}
#endif
//static void Proc_FuelResistance_RestoreToDefault(void)
//{
//#if EEP_CONTENT_FuelResistance_CONST_VARIBLE
//	GenEep_Set_FuelResistance(dflt_eep_val.FuelResistance);
//#endif	
//}
#if EEP_CONTENT_FuelResistance_CONST_VARIBLE
#define Proc_FuelResistance_RestoreToDefault()	do{GenEep_Set_FuelResistance(dflt_eep_val.FuelResistance);}while(0)
#else
#define Proc_FuelResistance_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> FuelResistance function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

uint32_t GenEep_Get_DTC_INFO_write_count(void)
{
	return eep_ram_image.DTC_INFO_write_count;
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> dtc_example function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_dtc_example_ITEM_LEN > 1
void GenEep_Get_dtc_example(uint8_t * const dtc_example )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_dtc_example_ITEM_LEN; i ++)
    {
        dtc_example[i] = eep_ram_image.dtc_example[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_dtc_example_Ex(uint8_t * const dtc_example,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.dtc_example;	//EEP_CONTENT_dtc_example_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_dtc_example_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_dtc_example_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        dtc_example[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_dtc_example(uint8_t const* dtc_example )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_dtc_example_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.dtc_example[i] != dtc_example[i] )
        {
            eep_ram_image.dtc_example[i] = dtc_example[i];
            wr_eep_by_list_req[EEP_CONTENT_dtc_example_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_dtc_example_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_dtc_example_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_dtc_example_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:dtc_example firt item[0] = %d",dtc_example[0]);
}

void GenEep_Set_dtc_example_Ex(uint8_t const* dtc_example,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_dtc_example_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_dtc_example_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.dtc_example;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != dtc_example[i] )
        {
            dest[offset+i] = dtc_example[i];
            wr_eep_by_list_req[EEP_CONTENT_dtc_example_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_dtc_example_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_dtc_example_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_dtc_example_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:dtc_example by Extend Api");
}

static void EepCopy_dtc_example_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_dtc_example_ITEM_LEN; i ++)
    {
        eep_hw_image.dtc_example[i] = eep_ram_image.dtc_example[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_dtc_example_IN_BLOCK_NUM);
}

#else 
uint8_t GenEep_Get_dtc_example(void)
{
    uint8_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.dtc_example;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_dtc_example(uint8_t const dtc_example)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.dtc_example != dtc_example )
    {
        eep_ram_image.dtc_example = dtc_example;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_dtc_example_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_dtc_example_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_dtc_example_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_dtc_example_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:dtc_example=%d",dtc_example);
}

static void EepCopy_dtc_example_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.dtc_example = eep_ram_image.dtc_example;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_dtc_example_IN_BLOCK_NUM);
}
#endif
//static void Proc_dtc_example_RestoreToDefault(void)
//{
//#if EEP_CONTENT_dtc_example_CONST_VARIBLE
//	GenEep_Set_dtc_example(dflt_eep_val.dtc_example);
//#endif	
//}
#if EEP_CONTENT_dtc_example_CONST_VARIBLE
#define Proc_dtc_example_RestoreToDefault()	do{GenEep_Set_dtc_example(dflt_eep_val.dtc_example);}while(0)
#else
#define Proc_dtc_example_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> dtc_example function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

uint32_t GenEep_Get_OdoInfo_write_count(void)
{
	return eep_ram_image.OdoInfo_write_count;
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> TotalOdo function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_TotalOdo_ITEM_LEN > 1
void GenEep_Get_TotalOdo(uint32_t * const TotalOdo )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_TotalOdo_ITEM_LEN; i ++)
    {
        TotalOdo[i] = eep_ram_image.TotalOdo[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_TotalOdo_Ex(uint8_t * const TotalOdo,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.TotalOdo;	//EEP_CONTENT_TotalOdo_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_TotalOdo_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_TotalOdo_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        TotalOdo[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_TotalOdo(uint32_t const* TotalOdo )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_TotalOdo_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.TotalOdo[i] != TotalOdo[i] )
        {
            eep_ram_image.TotalOdo[i] = TotalOdo[i];
            wr_eep_by_list_req[EEP_CONTENT_TotalOdo_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_TotalOdo_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_TotalOdo_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_TotalOdo_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:TotalOdo firt item[0] = %d",TotalOdo[0]);
}

void GenEep_Set_TotalOdo_Ex(uint8_t const* TotalOdo,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_TotalOdo_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_TotalOdo_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.TotalOdo;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != TotalOdo[i] )
        {
            dest[offset+i] = TotalOdo[i];
            wr_eep_by_list_req[EEP_CONTENT_TotalOdo_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_TotalOdo_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_TotalOdo_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_TotalOdo_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:TotalOdo by Extend Api");
}

static void EepCopy_TotalOdo_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_TotalOdo_ITEM_LEN; i ++)
    {
        eep_hw_image.TotalOdo[i] = eep_ram_image.TotalOdo[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_TotalOdo_IN_BLOCK_NUM);
}

#else 
uint32_t GenEep_Get_TotalOdo(void)
{
    uint32_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.TotalOdo;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_TotalOdo(uint32_t const TotalOdo)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.TotalOdo != TotalOdo )
    {
        eep_ram_image.TotalOdo = TotalOdo;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_TotalOdo_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_TotalOdo_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_TotalOdo_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_TotalOdo_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:TotalOdo=%d",TotalOdo);
}

static void EepCopy_TotalOdo_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.TotalOdo = eep_ram_image.TotalOdo;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_TotalOdo_IN_BLOCK_NUM);
}
#endif
//static void Proc_TotalOdo_RestoreToDefault(void)
//{
//#if EEP_CONTENT_TotalOdo_CONST_VARIBLE
//	GenEep_Set_TotalOdo(dflt_eep_val.TotalOdo);
//#endif	
//}
#if EEP_CONTENT_TotalOdo_CONST_VARIBLE
#define Proc_TotalOdo_RestoreToDefault()	do{GenEep_Set_TotalOdo(dflt_eep_val.TotalOdo);}while(0)
#else
#define Proc_TotalOdo_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> TotalOdo function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> TotalTime function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_TotalTime_ITEM_LEN > 1
void GenEep_Get_TotalTime(uint32_t * const TotalTime )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_TotalTime_ITEM_LEN; i ++)
    {
        TotalTime[i] = eep_ram_image.TotalTime[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_TotalTime_Ex(uint8_t * const TotalTime,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.TotalTime;	//EEP_CONTENT_TotalTime_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_TotalTime_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_TotalTime_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        TotalTime[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_TotalTime(uint32_t const* TotalTime )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_TotalTime_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.TotalTime[i] != TotalTime[i] )
        {
            eep_ram_image.TotalTime[i] = TotalTime[i];
            wr_eep_by_list_req[EEP_CONTENT_TotalTime_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_TotalTime_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_TotalTime_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_TotalTime_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:TotalTime firt item[0] = %d",TotalTime[0]);
}

void GenEep_Set_TotalTime_Ex(uint8_t const* TotalTime,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_TotalTime_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_TotalTime_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.TotalTime;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != TotalTime[i] )
        {
            dest[offset+i] = TotalTime[i];
            wr_eep_by_list_req[EEP_CONTENT_TotalTime_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_TotalTime_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_TotalTime_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_TotalTime_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:TotalTime by Extend Api");
}

static void EepCopy_TotalTime_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_TotalTime_ITEM_LEN; i ++)
    {
        eep_hw_image.TotalTime[i] = eep_ram_image.TotalTime[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_TotalTime_IN_BLOCK_NUM);
}

#else 
uint32_t GenEep_Get_TotalTime(void)
{
    uint32_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.TotalTime;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_TotalTime(uint32_t const TotalTime)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.TotalTime != TotalTime )
    {
        eep_ram_image.TotalTime = TotalTime;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_TotalTime_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_TotalTime_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_TotalTime_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_TotalTime_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:TotalTime=%d",TotalTime);
}

static void EepCopy_TotalTime_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.TotalTime = eep_ram_image.TotalTime;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_TotalTime_IN_BLOCK_NUM);
}
#endif
//static void Proc_TotalTime_RestoreToDefault(void)
//{
//#if EEP_CONTENT_TotalTime_CONST_VARIBLE
//	GenEep_Set_TotalTime(dflt_eep_val.TotalTime);
//#endif	
//}
#if EEP_CONTENT_TotalTime_CONST_VARIBLE
#define Proc_TotalTime_RestoreToDefault()	do{GenEep_Set_TotalTime(dflt_eep_val.TotalTime);}while(0)
#else
#define Proc_TotalTime_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> TotalTime function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> TravelTime function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_TravelTime_ITEM_LEN > 1
void GenEep_Get_TravelTime(uint32_t * const TravelTime )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_TravelTime_ITEM_LEN; i ++)
    {
        TravelTime[i] = eep_ram_image.TravelTime[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_TravelTime_Ex(uint8_t * const TravelTime,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.TravelTime;	//EEP_CONTENT_TravelTime_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_TravelTime_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_TravelTime_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        TravelTime[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_TravelTime(uint32_t const* TravelTime )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_TravelTime_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.TravelTime[i] != TravelTime[i] )
        {
            eep_ram_image.TravelTime[i] = TravelTime[i];
            wr_eep_by_list_req[EEP_CONTENT_TravelTime_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_TravelTime_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_TravelTime_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_TravelTime_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:TravelTime firt item[0] = %d",TravelTime[0]);
}

void GenEep_Set_TravelTime_Ex(uint8_t const* TravelTime,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_TravelTime_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_TravelTime_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.TravelTime;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != TravelTime[i] )
        {
            dest[offset+i] = TravelTime[i];
            wr_eep_by_list_req[EEP_CONTENT_TravelTime_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_TravelTime_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_TravelTime_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_TravelTime_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:TravelTime by Extend Api");
}

static void EepCopy_TravelTime_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_TravelTime_ITEM_LEN; i ++)
    {
        eep_hw_image.TravelTime[i] = eep_ram_image.TravelTime[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_TravelTime_IN_BLOCK_NUM);
}

#else 
uint32_t GenEep_Get_TravelTime(void)
{
    uint32_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.TravelTime;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_TravelTime(uint32_t const TravelTime)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.TravelTime != TravelTime )
    {
        eep_ram_image.TravelTime = TravelTime;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_TravelTime_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_TravelTime_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_TravelTime_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_TravelTime_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:TravelTime=%d",TravelTime);
}

static void EepCopy_TravelTime_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.TravelTime = eep_ram_image.TravelTime;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_TravelTime_IN_BLOCK_NUM);
}
#endif
//static void Proc_TravelTime_RestoreToDefault(void)
//{
//#if EEP_CONTENT_TravelTime_CONST_VARIBLE
//	GenEep_Set_TravelTime(dflt_eep_val.TravelTime);
//#endif	
//}
#if EEP_CONTENT_TravelTime_CONST_VARIBLE
#define Proc_TravelTime_RestoreToDefault()	do{GenEep_Set_TravelTime(dflt_eep_val.TravelTime);}while(0)
#else
#define Proc_TravelTime_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> TravelTime function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> TravelOdo function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_TravelOdo_ITEM_LEN > 1
void GenEep_Get_TravelOdo(uint32_t * const TravelOdo )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_TravelOdo_ITEM_LEN; i ++)
    {
        TravelOdo[i] = eep_ram_image.TravelOdo[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_TravelOdo_Ex(uint8_t * const TravelOdo,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.TravelOdo;	//EEP_CONTENT_TravelOdo_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_TravelOdo_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_TravelOdo_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        TravelOdo[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_TravelOdo(uint32_t const* TravelOdo )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_TravelOdo_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.TravelOdo[i] != TravelOdo[i] )
        {
            eep_ram_image.TravelOdo[i] = TravelOdo[i];
            wr_eep_by_list_req[EEP_CONTENT_TravelOdo_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_TravelOdo_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_TravelOdo_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_TravelOdo_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:TravelOdo firt item[0] = %d",TravelOdo[0]);
}

void GenEep_Set_TravelOdo_Ex(uint8_t const* TravelOdo,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_TravelOdo_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_TravelOdo_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.TravelOdo;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != TravelOdo[i] )
        {
            dest[offset+i] = TravelOdo[i];
            wr_eep_by_list_req[EEP_CONTENT_TravelOdo_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_TravelOdo_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_TravelOdo_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_TravelOdo_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:TravelOdo by Extend Api");
}

static void EepCopy_TravelOdo_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_TravelOdo_ITEM_LEN; i ++)
    {
        eep_hw_image.TravelOdo[i] = eep_ram_image.TravelOdo[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_TravelOdo_IN_BLOCK_NUM);
}

#else 
uint32_t GenEep_Get_TravelOdo(void)
{
    uint32_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.TravelOdo;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_TravelOdo(uint32_t const TravelOdo)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.TravelOdo != TravelOdo )
    {
        eep_ram_image.TravelOdo = TravelOdo;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_TravelOdo_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_TravelOdo_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_TravelOdo_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_TravelOdo_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:TravelOdo=%d",TravelOdo);
}

static void EepCopy_TravelOdo_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.TravelOdo = eep_ram_image.TravelOdo;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_TravelOdo_IN_BLOCK_NUM);
}
#endif
//static void Proc_TravelOdo_RestoreToDefault(void)
//{
//#if EEP_CONTENT_TravelOdo_CONST_VARIBLE
//	GenEep_Set_TravelOdo(dflt_eep_val.TravelOdo);
//#endif	
//}
#if EEP_CONTENT_TravelOdo_CONST_VARIBLE
#define Proc_TravelOdo_RestoreToDefault()	do{GenEep_Set_TravelOdo(dflt_eep_val.TravelOdo);}while(0)
#else
#define Proc_TravelOdo_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> TravelOdo function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> TripAMeter function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_TripAMeter_ITEM_LEN > 1
void GenEep_Get_TripAMeter(uint32_t * const TripAMeter )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_TripAMeter_ITEM_LEN; i ++)
    {
        TripAMeter[i] = eep_ram_image.TripAMeter[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_TripAMeter_Ex(uint8_t * const TripAMeter,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.TripAMeter;	//EEP_CONTENT_TripAMeter_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_TripAMeter_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_TripAMeter_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        TripAMeter[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_TripAMeter(uint32_t const* TripAMeter )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_TripAMeter_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.TripAMeter[i] != TripAMeter[i] )
        {
            eep_ram_image.TripAMeter[i] = TripAMeter[i];
            wr_eep_by_list_req[EEP_CONTENT_TripAMeter_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_TripAMeter_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_TripAMeter_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_TripAMeter_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:TripAMeter firt item[0] = %d",TripAMeter[0]);
}

void GenEep_Set_TripAMeter_Ex(uint8_t const* TripAMeter,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_TripAMeter_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_TripAMeter_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.TripAMeter;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != TripAMeter[i] )
        {
            dest[offset+i] = TripAMeter[i];
            wr_eep_by_list_req[EEP_CONTENT_TripAMeter_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_TripAMeter_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_TripAMeter_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_TripAMeter_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:TripAMeter by Extend Api");
}

static void EepCopy_TripAMeter_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_TripAMeter_ITEM_LEN; i ++)
    {
        eep_hw_image.TripAMeter[i] = eep_ram_image.TripAMeter[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_TripAMeter_IN_BLOCK_NUM);
}

#else 
uint32_t GenEep_Get_TripAMeter(void)
{
    uint32_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.TripAMeter;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_TripAMeter(uint32_t const TripAMeter)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.TripAMeter != TripAMeter )
    {
        eep_ram_image.TripAMeter = TripAMeter;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_TripAMeter_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_TripAMeter_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_TripAMeter_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_TripAMeter_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:TripAMeter=%d",TripAMeter);
}

static void EepCopy_TripAMeter_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.TripAMeter = eep_ram_image.TripAMeter;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_TripAMeter_IN_BLOCK_NUM);
}
#endif
//static void Proc_TripAMeter_RestoreToDefault(void)
//{
//#if EEP_CONTENT_TripAMeter_CONST_VARIBLE
//	GenEep_Set_TripAMeter(dflt_eep_val.TripAMeter);
//#endif	
//}
#if EEP_CONTENT_TripAMeter_CONST_VARIBLE
#define Proc_TripAMeter_RestoreToDefault()	do{GenEep_Set_TripAMeter(dflt_eep_val.TripAMeter);}while(0)
#else
#define Proc_TripAMeter_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> TripAMeter function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> TripATime function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_TripATime_ITEM_LEN > 1
void GenEep_Get_TripATime(uint32_t * const TripATime )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_TripATime_ITEM_LEN; i ++)
    {
        TripATime[i] = eep_ram_image.TripATime[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_TripATime_Ex(uint8_t * const TripATime,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.TripATime;	//EEP_CONTENT_TripATime_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_TripATime_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_TripATime_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        TripATime[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_TripATime(uint32_t const* TripATime )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_TripATime_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.TripATime[i] != TripATime[i] )
        {
            eep_ram_image.TripATime[i] = TripATime[i];
            wr_eep_by_list_req[EEP_CONTENT_TripATime_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_TripATime_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_TripATime_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_TripATime_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:TripATime firt item[0] = %d",TripATime[0]);
}

void GenEep_Set_TripATime_Ex(uint8_t const* TripATime,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_TripATime_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_TripATime_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.TripATime;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != TripATime[i] )
        {
            dest[offset+i] = TripATime[i];
            wr_eep_by_list_req[EEP_CONTENT_TripATime_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_TripATime_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_TripATime_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_TripATime_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:TripATime by Extend Api");
}

static void EepCopy_TripATime_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_TripATime_ITEM_LEN; i ++)
    {
        eep_hw_image.TripATime[i] = eep_ram_image.TripATime[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_TripATime_IN_BLOCK_NUM);
}

#else 
uint32_t GenEep_Get_TripATime(void)
{
    uint32_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.TripATime;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_TripATime(uint32_t const TripATime)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.TripATime != TripATime )
    {
        eep_ram_image.TripATime = TripATime;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_TripATime_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_TripATime_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_TripATime_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_TripATime_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:TripATime=%d",TripATime);
}

static void EepCopy_TripATime_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.TripATime = eep_ram_image.TripATime;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_TripATime_IN_BLOCK_NUM);
}
#endif
//static void Proc_TripATime_RestoreToDefault(void)
//{
//#if EEP_CONTENT_TripATime_CONST_VARIBLE
//	GenEep_Set_TripATime(dflt_eep_val.TripATime);
//#endif	
//}
#if EEP_CONTENT_TripATime_CONST_VARIBLE
#define Proc_TripATime_RestoreToDefault()	do{GenEep_Set_TripATime(dflt_eep_val.TripATime);}while(0)
#else
#define Proc_TripATime_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> TripATime function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> TripBMeter function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_TripBMeter_ITEM_LEN > 1
void GenEep_Get_TripBMeter(uint32_t * const TripBMeter )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_TripBMeter_ITEM_LEN; i ++)
    {
        TripBMeter[i] = eep_ram_image.TripBMeter[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_TripBMeter_Ex(uint8_t * const TripBMeter,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.TripBMeter;	//EEP_CONTENT_TripBMeter_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_TripBMeter_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_TripBMeter_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        TripBMeter[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_TripBMeter(uint32_t const* TripBMeter )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_TripBMeter_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.TripBMeter[i] != TripBMeter[i] )
        {
            eep_ram_image.TripBMeter[i] = TripBMeter[i];
            wr_eep_by_list_req[EEP_CONTENT_TripBMeter_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_TripBMeter_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_TripBMeter_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_TripBMeter_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:TripBMeter firt item[0] = %d",TripBMeter[0]);
}

void GenEep_Set_TripBMeter_Ex(uint8_t const* TripBMeter,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_TripBMeter_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_TripBMeter_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.TripBMeter;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != TripBMeter[i] )
        {
            dest[offset+i] = TripBMeter[i];
            wr_eep_by_list_req[EEP_CONTENT_TripBMeter_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_TripBMeter_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_TripBMeter_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_TripBMeter_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:TripBMeter by Extend Api");
}

static void EepCopy_TripBMeter_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_TripBMeter_ITEM_LEN; i ++)
    {
        eep_hw_image.TripBMeter[i] = eep_ram_image.TripBMeter[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_TripBMeter_IN_BLOCK_NUM);
}

#else 
uint32_t GenEep_Get_TripBMeter(void)
{
    uint32_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.TripBMeter;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_TripBMeter(uint32_t const TripBMeter)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.TripBMeter != TripBMeter )
    {
        eep_ram_image.TripBMeter = TripBMeter;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_TripBMeter_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_TripBMeter_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_TripBMeter_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_TripBMeter_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:TripBMeter=%d",TripBMeter);
}

static void EepCopy_TripBMeter_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.TripBMeter = eep_ram_image.TripBMeter;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_TripBMeter_IN_BLOCK_NUM);
}
#endif
//static void Proc_TripBMeter_RestoreToDefault(void)
//{
//#if EEP_CONTENT_TripBMeter_CONST_VARIBLE
//	GenEep_Set_TripBMeter(dflt_eep_val.TripBMeter);
//#endif	
//}
#if EEP_CONTENT_TripBMeter_CONST_VARIBLE
#define Proc_TripBMeter_RestoreToDefault()	do{GenEep_Set_TripBMeter(dflt_eep_val.TripBMeter);}while(0)
#else
#define Proc_TripBMeter_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> TripBMeter function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> TripBTime function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_TripBTime_ITEM_LEN > 1
void GenEep_Get_TripBTime(uint32_t * const TripBTime )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_TripBTime_ITEM_LEN; i ++)
    {
        TripBTime[i] = eep_ram_image.TripBTime[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_TripBTime_Ex(uint8_t * const TripBTime,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.TripBTime;	//EEP_CONTENT_TripBTime_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_TripBTime_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_TripBTime_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        TripBTime[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_TripBTime(uint32_t const* TripBTime )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_TripBTime_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.TripBTime[i] != TripBTime[i] )
        {
            eep_ram_image.TripBTime[i] = TripBTime[i];
            wr_eep_by_list_req[EEP_CONTENT_TripBTime_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_TripBTime_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_TripBTime_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_TripBTime_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:TripBTime firt item[0] = %d",TripBTime[0]);
}

void GenEep_Set_TripBTime_Ex(uint8_t const* TripBTime,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_TripBTime_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_TripBTime_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.TripBTime;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != TripBTime[i] )
        {
            dest[offset+i] = TripBTime[i];
            wr_eep_by_list_req[EEP_CONTENT_TripBTime_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_TripBTime_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_TripBTime_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_TripBTime_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:TripBTime by Extend Api");
}

static void EepCopy_TripBTime_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_TripBTime_ITEM_LEN; i ++)
    {
        eep_hw_image.TripBTime[i] = eep_ram_image.TripBTime[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_TripBTime_IN_BLOCK_NUM);
}

#else 
uint32_t GenEep_Get_TripBTime(void)
{
    uint32_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.TripBTime;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_TripBTime(uint32_t const TripBTime)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.TripBTime != TripBTime )
    {
        eep_ram_image.TripBTime = TripBTime;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_TripBTime_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_TripBTime_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_TripBTime_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_TripBTime_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:TripBTime=%d",TripBTime);
}

static void EepCopy_TripBTime_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.TripBTime = eep_ram_image.TripBTime;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_TripBTime_IN_BLOCK_NUM);
}
#endif
//static void Proc_TripBTime_RestoreToDefault(void)
//{
//#if EEP_CONTENT_TripBTime_CONST_VARIBLE
//	GenEep_Set_TripBTime(dflt_eep_val.TripBTime);
//#endif	
//}
#if EEP_CONTENT_TripBTime_CONST_VARIBLE
#define Proc_TripBTime_RestoreToDefault()	do{GenEep_Set_TripBTime(dflt_eep_val.TripBTime);}while(0)
#else
#define Proc_TripBTime_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> TripBTime function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> CruiseDistance function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_CruiseDistance_ITEM_LEN > 1
void GenEep_Get_CruiseDistance(uint32_t * const CruiseDistance )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_CruiseDistance_ITEM_LEN; i ++)
    {
        CruiseDistance[i] = eep_ram_image.CruiseDistance[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_CruiseDistance_Ex(uint8_t * const CruiseDistance,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.CruiseDistance;	//EEP_CONTENT_CruiseDistance_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_CruiseDistance_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_CruiseDistance_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        CruiseDistance[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_CruiseDistance(uint32_t const* CruiseDistance )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_CruiseDistance_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.CruiseDistance[i] != CruiseDistance[i] )
        {
            eep_ram_image.CruiseDistance[i] = CruiseDistance[i];
            wr_eep_by_list_req[EEP_CONTENT_CruiseDistance_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_CruiseDistance_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_CruiseDistance_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_CruiseDistance_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:CruiseDistance firt item[0] = %d",CruiseDistance[0]);
}

void GenEep_Set_CruiseDistance_Ex(uint8_t const* CruiseDistance,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_CruiseDistance_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_CruiseDistance_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.CruiseDistance;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != CruiseDistance[i] )
        {
            dest[offset+i] = CruiseDistance[i];
            wr_eep_by_list_req[EEP_CONTENT_CruiseDistance_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_CruiseDistance_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_CruiseDistance_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_CruiseDistance_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:CruiseDistance by Extend Api");
}

static void EepCopy_CruiseDistance_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_CruiseDistance_ITEM_LEN; i ++)
    {
        eep_hw_image.CruiseDistance[i] = eep_ram_image.CruiseDistance[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_CruiseDistance_IN_BLOCK_NUM);
}

#else 
uint32_t GenEep_Get_CruiseDistance(void)
{
    uint32_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.CruiseDistance;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_CruiseDistance(uint32_t const CruiseDistance)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.CruiseDistance != CruiseDistance )
    {
        eep_ram_image.CruiseDistance = CruiseDistance;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_CruiseDistance_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_CruiseDistance_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_CruiseDistance_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_CruiseDistance_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:CruiseDistance=%d",CruiseDistance);
}

static void EepCopy_CruiseDistance_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.CruiseDistance = eep_ram_image.CruiseDistance;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_CruiseDistance_IN_BLOCK_NUM);
}
#endif
//static void Proc_CruiseDistance_RestoreToDefault(void)
//{
//#if EEP_CONTENT_CruiseDistance_CONST_VARIBLE
//	GenEep_Set_CruiseDistance(dflt_eep_val.CruiseDistance);
//#endif	
//}
#if EEP_CONTENT_CruiseDistance_CONST_VARIBLE
#define Proc_CruiseDistance_RestoreToDefault()	do{GenEep_Set_CruiseDistance(dflt_eep_val.CruiseDistance);}while(0)
#else
#define Proc_CruiseDistance_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> CruiseDistance function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

uint32_t GenEep_Get_share_eep_write_count(void)
{
	return eep_ram_image.share_eep_write_count;
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> VipSwdlShareMem function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_VipSwdlShareMem_ITEM_LEN > 1
void GenEep_Get_VipSwdlShareMem(uint8_t * const VipSwdlShareMem )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_VipSwdlShareMem_ITEM_LEN; i ++)
    {
        VipSwdlShareMem[i] = eep_ram_image.VipSwdlShareMem[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_VipSwdlShareMem_Ex(uint8_t * const VipSwdlShareMem,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.VipSwdlShareMem;	//EEP_CONTENT_VipSwdlShareMem_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_VipSwdlShareMem_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_VipSwdlShareMem_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        VipSwdlShareMem[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_VipSwdlShareMem(uint8_t const* VipSwdlShareMem )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_VipSwdlShareMem_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.VipSwdlShareMem[i] != VipSwdlShareMem[i] )
        {
            eep_ram_image.VipSwdlShareMem[i] = VipSwdlShareMem[i];
            wr_eep_by_list_req[EEP_CONTENT_VipSwdlShareMem_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_VipSwdlShareMem_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_VipSwdlShareMem_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_VipSwdlShareMem_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:VipSwdlShareMem firt item[0] = %d",VipSwdlShareMem[0]);
}

void GenEep_Set_VipSwdlShareMem_Ex(uint8_t const* VipSwdlShareMem,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_VipSwdlShareMem_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_VipSwdlShareMem_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.VipSwdlShareMem;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != VipSwdlShareMem[i] )
        {
            dest[offset+i] = VipSwdlShareMem[i];
            wr_eep_by_list_req[EEP_CONTENT_VipSwdlShareMem_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_VipSwdlShareMem_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_VipSwdlShareMem_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_VipSwdlShareMem_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:VipSwdlShareMem by Extend Api");
}

static void EepCopy_VipSwdlShareMem_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_VipSwdlShareMem_ITEM_LEN; i ++)
    {
        eep_hw_image.VipSwdlShareMem[i] = eep_ram_image.VipSwdlShareMem[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_VipSwdlShareMem_IN_BLOCK_NUM);
}

#else 
uint8_t GenEep_Get_VipSwdlShareMem(void)
{
    uint8_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.VipSwdlShareMem;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_VipSwdlShareMem(uint8_t const VipSwdlShareMem)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.VipSwdlShareMem != VipSwdlShareMem )
    {
        eep_ram_image.VipSwdlShareMem = VipSwdlShareMem;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_VipSwdlShareMem_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_VipSwdlShareMem_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_VipSwdlShareMem_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_VipSwdlShareMem_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:VipSwdlShareMem=%d",VipSwdlShareMem);
}

static void EepCopy_VipSwdlShareMem_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.VipSwdlShareMem = eep_ram_image.VipSwdlShareMem;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_VipSwdlShareMem_IN_BLOCK_NUM);
}
#endif
//static void Proc_VipSwdlShareMem_RestoreToDefault(void)
//{
//#if EEP_CONTENT_VipSwdlShareMem_CONST_VARIBLE
//	GenEep_Set_VipSwdlShareMem(dflt_eep_val.VipSwdlShareMem);
//#endif	
//}
#if EEP_CONTENT_VipSwdlShareMem_CONST_VARIBLE
#define Proc_VipSwdlShareMem_RestoreToDefault()	do{GenEep_Set_VipSwdlShareMem(dflt_eep_val.VipSwdlShareMem);}while(0)
#else
#define Proc_VipSwdlShareMem_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> VipSwdlShareMem function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> TotalImageSFailCnt function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_TotalImageSFailCnt_ITEM_LEN > 1
void GenEep_Get_TotalImageSFailCnt(uint32_t * const TotalImageSFailCnt )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_TotalImageSFailCnt_ITEM_LEN; i ++)
    {
        TotalImageSFailCnt[i] = eep_ram_image.TotalImageSFailCnt[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_TotalImageSFailCnt_Ex(uint8_t * const TotalImageSFailCnt,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.TotalImageSFailCnt;	//EEP_CONTENT_TotalImageSFailCnt_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_TotalImageSFailCnt_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_TotalImageSFailCnt_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        TotalImageSFailCnt[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_TotalImageSFailCnt(uint32_t const* TotalImageSFailCnt )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_TotalImageSFailCnt_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.TotalImageSFailCnt[i] != TotalImageSFailCnt[i] )
        {
            eep_ram_image.TotalImageSFailCnt[i] = TotalImageSFailCnt[i];
            wr_eep_by_list_req[EEP_CONTENT_TotalImageSFailCnt_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_TotalImageSFailCnt_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_TotalImageSFailCnt_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_TotalImageSFailCnt_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:TotalImageSFailCnt firt item[0] = %d",TotalImageSFailCnt[0]);
}

void GenEep_Set_TotalImageSFailCnt_Ex(uint8_t const* TotalImageSFailCnt,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_TotalImageSFailCnt_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_TotalImageSFailCnt_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.TotalImageSFailCnt;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != TotalImageSFailCnt[i] )
        {
            dest[offset+i] = TotalImageSFailCnt[i];
            wr_eep_by_list_req[EEP_CONTENT_TotalImageSFailCnt_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_TotalImageSFailCnt_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_TotalImageSFailCnt_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_TotalImageSFailCnt_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:TotalImageSFailCnt by Extend Api");
}

static void EepCopy_TotalImageSFailCnt_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_TotalImageSFailCnt_ITEM_LEN; i ++)
    {
        eep_hw_image.TotalImageSFailCnt[i] = eep_ram_image.TotalImageSFailCnt[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_TotalImageSFailCnt_IN_BLOCK_NUM);
}

#else 
uint32_t GenEep_Get_TotalImageSFailCnt(void)
{
    uint32_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.TotalImageSFailCnt;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_TotalImageSFailCnt(uint32_t const TotalImageSFailCnt)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.TotalImageSFailCnt != TotalImageSFailCnt )
    {
        eep_ram_image.TotalImageSFailCnt = TotalImageSFailCnt;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_TotalImageSFailCnt_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_TotalImageSFailCnt_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_TotalImageSFailCnt_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_TotalImageSFailCnt_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:TotalImageSFailCnt=%d",TotalImageSFailCnt);
}

static void EepCopy_TotalImageSFailCnt_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.TotalImageSFailCnt = eep_ram_image.TotalImageSFailCnt;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_TotalImageSFailCnt_IN_BLOCK_NUM);
}
#endif
//static void Proc_TotalImageSFailCnt_RestoreToDefault(void)
//{
//#if EEP_CONTENT_TotalImageSFailCnt_CONST_VARIBLE
//	GenEep_Set_TotalImageSFailCnt(dflt_eep_val.TotalImageSFailCnt);
//#endif	
//}
#if EEP_CONTENT_TotalImageSFailCnt_CONST_VARIBLE
#define Proc_TotalImageSFailCnt_RestoreToDefault()	do{GenEep_Set_TotalImageSFailCnt(dflt_eep_val.TotalImageSFailCnt);}while(0)
#else
#define Proc_TotalImageSFailCnt_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> TotalImageSFailCnt function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> trace_phy_en function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_trace_phy_en_ITEM_LEN > 1
void GenEep_Get_trace_phy_en(uint16_t * const trace_phy_en )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_trace_phy_en_ITEM_LEN; i ++)
    {
        trace_phy_en[i] = eep_ram_image.trace_phy_en[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_trace_phy_en_Ex(uint8_t * const trace_phy_en,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.trace_phy_en;	//EEP_CONTENT_trace_phy_en_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_trace_phy_en_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_trace_phy_en_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        trace_phy_en[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_trace_phy_en(uint16_t const* trace_phy_en )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_trace_phy_en_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.trace_phy_en[i] != trace_phy_en[i] )
        {
            eep_ram_image.trace_phy_en[i] = trace_phy_en[i];
            wr_eep_by_list_req[EEP_CONTENT_trace_phy_en_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_trace_phy_en_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_trace_phy_en_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_trace_phy_en_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:trace_phy_en firt item[0] = %d",trace_phy_en[0]);
}

void GenEep_Set_trace_phy_en_Ex(uint8_t const* trace_phy_en,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_trace_phy_en_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_trace_phy_en_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.trace_phy_en;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != trace_phy_en[i] )
        {
            dest[offset+i] = trace_phy_en[i];
            wr_eep_by_list_req[EEP_CONTENT_trace_phy_en_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_trace_phy_en_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_trace_phy_en_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_trace_phy_en_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:trace_phy_en by Extend Api");
}

static void EepCopy_trace_phy_en_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_trace_phy_en_ITEM_LEN; i ++)
    {
        eep_hw_image.trace_phy_en[i] = eep_ram_image.trace_phy_en[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_trace_phy_en_IN_BLOCK_NUM);
}

#else 
uint16_t GenEep_Get_trace_phy_en(void)
{
    uint16_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.trace_phy_en;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_trace_phy_en(uint16_t const trace_phy_en)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.trace_phy_en != trace_phy_en )
    {
        eep_ram_image.trace_phy_en = trace_phy_en;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_trace_phy_en_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_trace_phy_en_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_trace_phy_en_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_trace_phy_en_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:trace_phy_en=%d",trace_phy_en);
}

static void EepCopy_trace_phy_en_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.trace_phy_en = eep_ram_image.trace_phy_en;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_trace_phy_en_IN_BLOCK_NUM);
}
#endif
//static void Proc_trace_phy_en_RestoreToDefault(void)
//{
//#if EEP_CONTENT_trace_phy_en_CONST_VARIBLE
//	GenEep_Set_trace_phy_en(dflt_eep_val.trace_phy_en);
//#endif	
//}
#if EEP_CONTENT_trace_phy_en_CONST_VARIBLE
#define Proc_trace_phy_en_RestoreToDefault()	do{GenEep_Set_trace_phy_en(dflt_eep_val.trace_phy_en);}while(0)
#else
#define Proc_trace_phy_en_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> trace_phy_en function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> trace_phy function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_trace_phy_ITEM_LEN > 1
void GenEep_Get_trace_phy(uint8_t * const trace_phy )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_trace_phy_ITEM_LEN; i ++)
    {
        trace_phy[i] = eep_ram_image.trace_phy[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_trace_phy_Ex(uint8_t * const trace_phy,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.trace_phy;	//EEP_CONTENT_trace_phy_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_trace_phy_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_trace_phy_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        trace_phy[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_trace_phy(uint8_t const* trace_phy )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_trace_phy_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.trace_phy[i] != trace_phy[i] )
        {
            eep_ram_image.trace_phy[i] = trace_phy[i];
            wr_eep_by_list_req[EEP_CONTENT_trace_phy_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_trace_phy_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_trace_phy_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_trace_phy_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:trace_phy firt item[0] = %d",trace_phy[0]);
}

void GenEep_Set_trace_phy_Ex(uint8_t const* trace_phy,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_trace_phy_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_trace_phy_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.trace_phy;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != trace_phy[i] )
        {
            dest[offset+i] = trace_phy[i];
            wr_eep_by_list_req[EEP_CONTENT_trace_phy_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_trace_phy_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_trace_phy_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_trace_phy_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:trace_phy by Extend Api");
}

static void EepCopy_trace_phy_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_trace_phy_ITEM_LEN; i ++)
    {
        eep_hw_image.trace_phy[i] = eep_ram_image.trace_phy[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_trace_phy_IN_BLOCK_NUM);
}

#else 
uint8_t GenEep_Get_trace_phy(void)
{
    uint8_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.trace_phy;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_trace_phy(uint8_t const trace_phy)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.trace_phy != trace_phy )
    {
        eep_ram_image.trace_phy = trace_phy;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_trace_phy_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_trace_phy_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_trace_phy_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_trace_phy_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:trace_phy=%d",trace_phy);
}

static void EepCopy_trace_phy_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.trace_phy = eep_ram_image.trace_phy;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_trace_phy_IN_BLOCK_NUM);
}
#endif
//static void Proc_trace_phy_RestoreToDefault(void)
//{
//#if EEP_CONTENT_trace_phy_CONST_VARIBLE
//	GenEep_Set_trace_phy(dflt_eep_val.trace_phy);
//#endif	
//}
#if EEP_CONTENT_trace_phy_CONST_VARIBLE
#define Proc_trace_phy_RestoreToDefault()	do{GenEep_Set_trace_phy(dflt_eep_val.trace_phy);}while(0)
#else
#define Proc_trace_phy_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> trace_phy function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> VipLogModLvl function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_VipLogModLvl_ITEM_LEN > 1
void GenEep_Get_VipLogModLvl(uint8_t * const VipLogModLvl )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_VipLogModLvl_ITEM_LEN; i ++)
    {
        VipLogModLvl[i] = eep_ram_image.VipLogModLvl[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_VipLogModLvl_Ex(uint8_t * const VipLogModLvl,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.VipLogModLvl;	//EEP_CONTENT_VipLogModLvl_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_VipLogModLvl_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_VipLogModLvl_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        VipLogModLvl[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_VipLogModLvl(uint8_t const* VipLogModLvl )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_VipLogModLvl_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.VipLogModLvl[i] != VipLogModLvl[i] )
        {
            eep_ram_image.VipLogModLvl[i] = VipLogModLvl[i];
            wr_eep_by_list_req[EEP_CONTENT_VipLogModLvl_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_VipLogModLvl_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_VipLogModLvl_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_VipLogModLvl_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:VipLogModLvl firt item[0] = %d",VipLogModLvl[0]);
}

void GenEep_Set_VipLogModLvl_Ex(uint8_t const* VipLogModLvl,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_VipLogModLvl_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_VipLogModLvl_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.VipLogModLvl;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != VipLogModLvl[i] )
        {
            dest[offset+i] = VipLogModLvl[i];
            wr_eep_by_list_req[EEP_CONTENT_VipLogModLvl_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_VipLogModLvl_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_VipLogModLvl_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_VipLogModLvl_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:VipLogModLvl by Extend Api");
}

static void EepCopy_VipLogModLvl_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_VipLogModLvl_ITEM_LEN; i ++)
    {
        eep_hw_image.VipLogModLvl[i] = eep_ram_image.VipLogModLvl[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_VipLogModLvl_IN_BLOCK_NUM);
}

#else 
uint8_t GenEep_Get_VipLogModLvl(void)
{
    uint8_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.VipLogModLvl;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_VipLogModLvl(uint8_t const VipLogModLvl)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.VipLogModLvl != VipLogModLvl )
    {
        eep_ram_image.VipLogModLvl = VipLogModLvl;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_VipLogModLvl_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_VipLogModLvl_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_VipLogModLvl_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_VipLogModLvl_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:VipLogModLvl=%d",VipLogModLvl);
}

static void EepCopy_VipLogModLvl_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.VipLogModLvl = eep_ram_image.VipLogModLvl;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_VipLogModLvl_IN_BLOCK_NUM);
}
#endif
//static void Proc_VipLogModLvl_RestoreToDefault(void)
//{
//#if EEP_CONTENT_VipLogModLvl_CONST_VARIBLE
//	GenEep_Set_VipLogModLvl(dflt_eep_val.VipLogModLvl);
//#endif	
//}
#if EEP_CONTENT_VipLogModLvl_CONST_VARIBLE
#define Proc_VipLogModLvl_RestoreToDefault()	do{GenEep_Set_VipLogModLvl(dflt_eep_val.VipLogModLvl);}while(0)
#else
#define Proc_VipLogModLvl_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> VipLogModLvl function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> TraceModuleLvlCrc function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_TraceModuleLvlCrc_ITEM_LEN > 1
void GenEep_Get_TraceModuleLvlCrc(uint16_t * const TraceModuleLvlCrc )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_TraceModuleLvlCrc_ITEM_LEN; i ++)
    {
        TraceModuleLvlCrc[i] = eep_ram_image.TraceModuleLvlCrc[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_TraceModuleLvlCrc_Ex(uint8_t * const TraceModuleLvlCrc,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.TraceModuleLvlCrc;	//EEP_CONTENT_TraceModuleLvlCrc_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_TraceModuleLvlCrc_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_TraceModuleLvlCrc_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        TraceModuleLvlCrc[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_TraceModuleLvlCrc(uint16_t const* TraceModuleLvlCrc )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_TraceModuleLvlCrc_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.TraceModuleLvlCrc[i] != TraceModuleLvlCrc[i] )
        {
            eep_ram_image.TraceModuleLvlCrc[i] = TraceModuleLvlCrc[i];
            wr_eep_by_list_req[EEP_CONTENT_TraceModuleLvlCrc_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_TraceModuleLvlCrc_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_TraceModuleLvlCrc_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_TraceModuleLvlCrc_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:TraceModuleLvlCrc firt item[0] = %d",TraceModuleLvlCrc[0]);
}

void GenEep_Set_TraceModuleLvlCrc_Ex(uint8_t const* TraceModuleLvlCrc,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_TraceModuleLvlCrc_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_TraceModuleLvlCrc_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.TraceModuleLvlCrc;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != TraceModuleLvlCrc[i] )
        {
            dest[offset+i] = TraceModuleLvlCrc[i];
            wr_eep_by_list_req[EEP_CONTENT_TraceModuleLvlCrc_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_TraceModuleLvlCrc_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_TraceModuleLvlCrc_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_TraceModuleLvlCrc_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:TraceModuleLvlCrc by Extend Api");
}

static void EepCopy_TraceModuleLvlCrc_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_TraceModuleLvlCrc_ITEM_LEN; i ++)
    {
        eep_hw_image.TraceModuleLvlCrc[i] = eep_ram_image.TraceModuleLvlCrc[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_TraceModuleLvlCrc_IN_BLOCK_NUM);
}

#else 
uint16_t GenEep_Get_TraceModuleLvlCrc(void)
{
    uint16_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.TraceModuleLvlCrc;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_TraceModuleLvlCrc(uint16_t const TraceModuleLvlCrc)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.TraceModuleLvlCrc != TraceModuleLvlCrc )
    {
        eep_ram_image.TraceModuleLvlCrc = TraceModuleLvlCrc;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_TraceModuleLvlCrc_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_TraceModuleLvlCrc_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_TraceModuleLvlCrc_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_TraceModuleLvlCrc_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:TraceModuleLvlCrc=%d",TraceModuleLvlCrc);
}

static void EepCopy_TraceModuleLvlCrc_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.TraceModuleLvlCrc = eep_ram_image.TraceModuleLvlCrc;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_TraceModuleLvlCrc_IN_BLOCK_NUM);
}
#endif
//static void Proc_TraceModuleLvlCrc_RestoreToDefault(void)
//{
//#if EEP_CONTENT_TraceModuleLvlCrc_CONST_VARIBLE
//	GenEep_Set_TraceModuleLvlCrc(dflt_eep_val.TraceModuleLvlCrc);
//#endif	
//}
#if EEP_CONTENT_TraceModuleLvlCrc_CONST_VARIBLE
#define Proc_TraceModuleLvlCrc_RestoreToDefault()	do{GenEep_Set_TraceModuleLvlCrc(dflt_eep_val.TraceModuleLvlCrc);}while(0)
#else
#define Proc_TraceModuleLvlCrc_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> TraceModuleLvlCrc function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

uint32_t GenEep_Get_ResetInfo_write_count(void)
{
	return eep_ram_image.ResetInfo_write_count;
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> TotalReset_Cnt function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_TotalReset_Cnt_ITEM_LEN > 1
void GenEep_Get_TotalReset_Cnt(uint32_t * const TotalReset_Cnt )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_TotalReset_Cnt_ITEM_LEN; i ++)
    {
        TotalReset_Cnt[i] = eep_ram_image.TotalReset_Cnt[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_TotalReset_Cnt_Ex(uint8_t * const TotalReset_Cnt,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.TotalReset_Cnt;	//EEP_CONTENT_TotalReset_Cnt_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_TotalReset_Cnt_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_TotalReset_Cnt_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        TotalReset_Cnt[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_TotalReset_Cnt(uint32_t const* TotalReset_Cnt )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_TotalReset_Cnt_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.TotalReset_Cnt[i] != TotalReset_Cnt[i] )
        {
            eep_ram_image.TotalReset_Cnt[i] = TotalReset_Cnt[i];
            wr_eep_by_list_req[EEP_CONTENT_TotalReset_Cnt_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_TotalReset_Cnt_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_TotalReset_Cnt_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_TotalReset_Cnt_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:TotalReset_Cnt firt item[0] = %d",TotalReset_Cnt[0]);
}

void GenEep_Set_TotalReset_Cnt_Ex(uint8_t const* TotalReset_Cnt,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_TotalReset_Cnt_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_TotalReset_Cnt_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.TotalReset_Cnt;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != TotalReset_Cnt[i] )
        {
            dest[offset+i] = TotalReset_Cnt[i];
            wr_eep_by_list_req[EEP_CONTENT_TotalReset_Cnt_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_TotalReset_Cnt_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_TotalReset_Cnt_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_TotalReset_Cnt_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:TotalReset_Cnt by Extend Api");
}

static void EepCopy_TotalReset_Cnt_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_TotalReset_Cnt_ITEM_LEN; i ++)
    {
        eep_hw_image.TotalReset_Cnt[i] = eep_ram_image.TotalReset_Cnt[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_TotalReset_Cnt_IN_BLOCK_NUM);
}

#else 
uint32_t GenEep_Get_TotalReset_Cnt(void)
{
    uint32_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.TotalReset_Cnt;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_TotalReset_Cnt(uint32_t const TotalReset_Cnt)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.TotalReset_Cnt != TotalReset_Cnt )
    {
        eep_ram_image.TotalReset_Cnt = TotalReset_Cnt;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_TotalReset_Cnt_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_TotalReset_Cnt_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_TotalReset_Cnt_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_TotalReset_Cnt_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:TotalReset_Cnt=%d",TotalReset_Cnt);
}

static void EepCopy_TotalReset_Cnt_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.TotalReset_Cnt = eep_ram_image.TotalReset_Cnt;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_TotalReset_Cnt_IN_BLOCK_NUM);
}
#endif
//static void Proc_TotalReset_Cnt_RestoreToDefault(void)
//{
//#if EEP_CONTENT_TotalReset_Cnt_CONST_VARIBLE
//	GenEep_Set_TotalReset_Cnt(dflt_eep_val.TotalReset_Cnt);
//#endif	
//}
#if EEP_CONTENT_TotalReset_Cnt_CONST_VARIBLE
#define Proc_TotalReset_Cnt_RestoreToDefault()	do{GenEep_Set_TotalReset_Cnt(dflt_eep_val.TotalReset_Cnt);}while(0)
#else
#define Proc_TotalReset_Cnt_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> TotalReset_Cnt function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> DspPOR_cnt function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_DspPOR_cnt_ITEM_LEN > 1
void GenEep_Get_DspPOR_cnt(uint16_t * const DspPOR_cnt )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_DspPOR_cnt_ITEM_LEN; i ++)
    {
        DspPOR_cnt[i] = eep_ram_image.DspPOR_cnt[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_DspPOR_cnt_Ex(uint8_t * const DspPOR_cnt,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.DspPOR_cnt;	//EEP_CONTENT_DspPOR_cnt_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_DspPOR_cnt_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_DspPOR_cnt_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        DspPOR_cnt[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_DspPOR_cnt(uint16_t const* DspPOR_cnt )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_DspPOR_cnt_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.DspPOR_cnt[i] != DspPOR_cnt[i] )
        {
            eep_ram_image.DspPOR_cnt[i] = DspPOR_cnt[i];
            wr_eep_by_list_req[EEP_CONTENT_DspPOR_cnt_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_DspPOR_cnt_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_DspPOR_cnt_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_DspPOR_cnt_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:DspPOR_cnt firt item[0] = %d",DspPOR_cnt[0]);
}

void GenEep_Set_DspPOR_cnt_Ex(uint8_t const* DspPOR_cnt,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_DspPOR_cnt_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_DspPOR_cnt_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.DspPOR_cnt;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != DspPOR_cnt[i] )
        {
            dest[offset+i] = DspPOR_cnt[i];
            wr_eep_by_list_req[EEP_CONTENT_DspPOR_cnt_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_DspPOR_cnt_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_DspPOR_cnt_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_DspPOR_cnt_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:DspPOR_cnt by Extend Api");
}

static void EepCopy_DspPOR_cnt_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_DspPOR_cnt_ITEM_LEN; i ++)
    {
        eep_hw_image.DspPOR_cnt[i] = eep_ram_image.DspPOR_cnt[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_DspPOR_cnt_IN_BLOCK_NUM);
}

#else 
uint16_t GenEep_Get_DspPOR_cnt(void)
{
    uint16_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.DspPOR_cnt;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_DspPOR_cnt(uint16_t const DspPOR_cnt)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.DspPOR_cnt != DspPOR_cnt )
    {
        eep_ram_image.DspPOR_cnt = DspPOR_cnt;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_DspPOR_cnt_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_DspPOR_cnt_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_DspPOR_cnt_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_DspPOR_cnt_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:DspPOR_cnt=%d",DspPOR_cnt);
}

static void EepCopy_DspPOR_cnt_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.DspPOR_cnt = eep_ram_image.DspPOR_cnt;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_DspPOR_cnt_IN_BLOCK_NUM);
}
#endif
//static void Proc_DspPOR_cnt_RestoreToDefault(void)
//{
//#if EEP_CONTENT_DspPOR_cnt_CONST_VARIBLE
//	GenEep_Set_DspPOR_cnt(dflt_eep_val.DspPOR_cnt);
//#endif	
//}
#if EEP_CONTENT_DspPOR_cnt_CONST_VARIBLE
#define Proc_DspPOR_cnt_RestoreToDefault()	do{GenEep_Set_DspPOR_cnt(dflt_eep_val.DspPOR_cnt);}while(0)
#else
#define Proc_DspPOR_cnt_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> DspPOR_cnt function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> AudioCmdExecMaxTm function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_AudioCmdExecMaxTm_ITEM_LEN > 1
void GenEep_Get_AudioCmdExecMaxTm(uint16_t * const AudioCmdExecMaxTm )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_AudioCmdExecMaxTm_ITEM_LEN; i ++)
    {
        AudioCmdExecMaxTm[i] = eep_ram_image.AudioCmdExecMaxTm[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_AudioCmdExecMaxTm_Ex(uint8_t * const AudioCmdExecMaxTm,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.AudioCmdExecMaxTm;	//EEP_CONTENT_AudioCmdExecMaxTm_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_AudioCmdExecMaxTm_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_AudioCmdExecMaxTm_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        AudioCmdExecMaxTm[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_AudioCmdExecMaxTm(uint16_t const* AudioCmdExecMaxTm )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_AudioCmdExecMaxTm_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.AudioCmdExecMaxTm[i] != AudioCmdExecMaxTm[i] )
        {
            eep_ram_image.AudioCmdExecMaxTm[i] = AudioCmdExecMaxTm[i];
            wr_eep_by_list_req[EEP_CONTENT_AudioCmdExecMaxTm_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_AudioCmdExecMaxTm_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_AudioCmdExecMaxTm_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_AudioCmdExecMaxTm_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:AudioCmdExecMaxTm firt item[0] = %d",AudioCmdExecMaxTm[0]);
}

void GenEep_Set_AudioCmdExecMaxTm_Ex(uint8_t const* AudioCmdExecMaxTm,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_AudioCmdExecMaxTm_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_AudioCmdExecMaxTm_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.AudioCmdExecMaxTm;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != AudioCmdExecMaxTm[i] )
        {
            dest[offset+i] = AudioCmdExecMaxTm[i];
            wr_eep_by_list_req[EEP_CONTENT_AudioCmdExecMaxTm_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_AudioCmdExecMaxTm_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_AudioCmdExecMaxTm_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_AudioCmdExecMaxTm_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:AudioCmdExecMaxTm by Extend Api");
}

static void EepCopy_AudioCmdExecMaxTm_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_AudioCmdExecMaxTm_ITEM_LEN; i ++)
    {
        eep_hw_image.AudioCmdExecMaxTm[i] = eep_ram_image.AudioCmdExecMaxTm[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_AudioCmdExecMaxTm_IN_BLOCK_NUM);
}

#else 
uint16_t GenEep_Get_AudioCmdExecMaxTm(void)
{
    uint16_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.AudioCmdExecMaxTm;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_AudioCmdExecMaxTm(uint16_t const AudioCmdExecMaxTm)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.AudioCmdExecMaxTm != AudioCmdExecMaxTm )
    {
        eep_ram_image.AudioCmdExecMaxTm = AudioCmdExecMaxTm;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_AudioCmdExecMaxTm_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_AudioCmdExecMaxTm_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_AudioCmdExecMaxTm_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_AudioCmdExecMaxTm_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:AudioCmdExecMaxTm=%d",AudioCmdExecMaxTm);
}

static void EepCopy_AudioCmdExecMaxTm_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.AudioCmdExecMaxTm = eep_ram_image.AudioCmdExecMaxTm;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_AudioCmdExecMaxTm_IN_BLOCK_NUM);
}
#endif
//static void Proc_AudioCmdExecMaxTm_RestoreToDefault(void)
//{
//#if EEP_CONTENT_AudioCmdExecMaxTm_CONST_VARIBLE
//	GenEep_Set_AudioCmdExecMaxTm(dflt_eep_val.AudioCmdExecMaxTm);
//#endif	
//}
#if EEP_CONTENT_AudioCmdExecMaxTm_CONST_VARIBLE
#define Proc_AudioCmdExecMaxTm_RestoreToDefault()	do{GenEep_Set_AudioCmdExecMaxTm(dflt_eep_val.AudioCmdExecMaxTm);}while(0)
#else
#define Proc_AudioCmdExecMaxTm_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> AudioCmdExecMaxTm function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> RadioCmdExecMaxTm function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_RadioCmdExecMaxTm_ITEM_LEN > 1
void GenEep_Get_RadioCmdExecMaxTm(uint16_t * const RadioCmdExecMaxTm )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_RadioCmdExecMaxTm_ITEM_LEN; i ++)
    {
        RadioCmdExecMaxTm[i] = eep_ram_image.RadioCmdExecMaxTm[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_RadioCmdExecMaxTm_Ex(uint8_t * const RadioCmdExecMaxTm,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.RadioCmdExecMaxTm;	//EEP_CONTENT_RadioCmdExecMaxTm_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_RadioCmdExecMaxTm_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_RadioCmdExecMaxTm_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        RadioCmdExecMaxTm[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_RadioCmdExecMaxTm(uint16_t const* RadioCmdExecMaxTm )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_RadioCmdExecMaxTm_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.RadioCmdExecMaxTm[i] != RadioCmdExecMaxTm[i] )
        {
            eep_ram_image.RadioCmdExecMaxTm[i] = RadioCmdExecMaxTm[i];
            wr_eep_by_list_req[EEP_CONTENT_RadioCmdExecMaxTm_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_RadioCmdExecMaxTm_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_RadioCmdExecMaxTm_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_RadioCmdExecMaxTm_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:RadioCmdExecMaxTm firt item[0] = %d",RadioCmdExecMaxTm[0]);
}

void GenEep_Set_RadioCmdExecMaxTm_Ex(uint8_t const* RadioCmdExecMaxTm,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_RadioCmdExecMaxTm_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_RadioCmdExecMaxTm_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.RadioCmdExecMaxTm;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != RadioCmdExecMaxTm[i] )
        {
            dest[offset+i] = RadioCmdExecMaxTm[i];
            wr_eep_by_list_req[EEP_CONTENT_RadioCmdExecMaxTm_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_RadioCmdExecMaxTm_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_RadioCmdExecMaxTm_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_RadioCmdExecMaxTm_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:RadioCmdExecMaxTm by Extend Api");
}

static void EepCopy_RadioCmdExecMaxTm_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_RadioCmdExecMaxTm_ITEM_LEN; i ++)
    {
        eep_hw_image.RadioCmdExecMaxTm[i] = eep_ram_image.RadioCmdExecMaxTm[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_RadioCmdExecMaxTm_IN_BLOCK_NUM);
}

#else 
uint16_t GenEep_Get_RadioCmdExecMaxTm(void)
{
    uint16_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.RadioCmdExecMaxTm;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_RadioCmdExecMaxTm(uint16_t const RadioCmdExecMaxTm)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.RadioCmdExecMaxTm != RadioCmdExecMaxTm )
    {
        eep_ram_image.RadioCmdExecMaxTm = RadioCmdExecMaxTm;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_RadioCmdExecMaxTm_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_RadioCmdExecMaxTm_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_RadioCmdExecMaxTm_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_RadioCmdExecMaxTm_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:RadioCmdExecMaxTm=%d",RadioCmdExecMaxTm);
}

static void EepCopy_RadioCmdExecMaxTm_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.RadioCmdExecMaxTm = eep_ram_image.RadioCmdExecMaxTm;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_RadioCmdExecMaxTm_IN_BLOCK_NUM);
}
#endif
//static void Proc_RadioCmdExecMaxTm_RestoreToDefault(void)
//{
//#if EEP_CONTENT_RadioCmdExecMaxTm_CONST_VARIBLE
//	GenEep_Set_RadioCmdExecMaxTm(dflt_eep_val.RadioCmdExecMaxTm);
//#endif	
//}
#if EEP_CONTENT_RadioCmdExecMaxTm_CONST_VARIBLE
#define Proc_RadioCmdExecMaxTm_RestoreToDefault()	do{GenEep_Set_RadioCmdExecMaxTm(dflt_eep_val.RadioCmdExecMaxTm);}while(0)
#else
#define Proc_RadioCmdExecMaxTm_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> RadioCmdExecMaxTm function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> CpuDisIntMaxNest function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_CpuDisIntMaxNest_ITEM_LEN > 1
void GenEep_Get_CpuDisIntMaxNest(uint16_t * const CpuDisIntMaxNest )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_CpuDisIntMaxNest_ITEM_LEN; i ++)
    {
        CpuDisIntMaxNest[i] = eep_ram_image.CpuDisIntMaxNest[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_CpuDisIntMaxNest_Ex(uint8_t * const CpuDisIntMaxNest,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.CpuDisIntMaxNest;	//EEP_CONTENT_CpuDisIntMaxNest_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_CpuDisIntMaxNest_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_CpuDisIntMaxNest_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        CpuDisIntMaxNest[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_CpuDisIntMaxNest(uint16_t const* CpuDisIntMaxNest )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_CpuDisIntMaxNest_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.CpuDisIntMaxNest[i] != CpuDisIntMaxNest[i] )
        {
            eep_ram_image.CpuDisIntMaxNest[i] = CpuDisIntMaxNest[i];
            wr_eep_by_list_req[EEP_CONTENT_CpuDisIntMaxNest_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_CpuDisIntMaxNest_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_CpuDisIntMaxNest_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_CpuDisIntMaxNest_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:CpuDisIntMaxNest firt item[0] = %d",CpuDisIntMaxNest[0]);
}

void GenEep_Set_CpuDisIntMaxNest_Ex(uint8_t const* CpuDisIntMaxNest,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_CpuDisIntMaxNest_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_CpuDisIntMaxNest_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.CpuDisIntMaxNest;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != CpuDisIntMaxNest[i] )
        {
            dest[offset+i] = CpuDisIntMaxNest[i];
            wr_eep_by_list_req[EEP_CONTENT_CpuDisIntMaxNest_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_CpuDisIntMaxNest_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_CpuDisIntMaxNest_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_CpuDisIntMaxNest_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:CpuDisIntMaxNest by Extend Api");
}

static void EepCopy_CpuDisIntMaxNest_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_CpuDisIntMaxNest_ITEM_LEN; i ++)
    {
        eep_hw_image.CpuDisIntMaxNest[i] = eep_ram_image.CpuDisIntMaxNest[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_CpuDisIntMaxNest_IN_BLOCK_NUM);
}

#else 
uint16_t GenEep_Get_CpuDisIntMaxNest(void)
{
    uint16_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.CpuDisIntMaxNest;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_CpuDisIntMaxNest(uint16_t const CpuDisIntMaxNest)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.CpuDisIntMaxNest != CpuDisIntMaxNest )
    {
        eep_ram_image.CpuDisIntMaxNest = CpuDisIntMaxNest;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_CpuDisIntMaxNest_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_CpuDisIntMaxNest_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_CpuDisIntMaxNest_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_CpuDisIntMaxNest_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:CpuDisIntMaxNest=%d",CpuDisIntMaxNest);
}

static void EepCopy_CpuDisIntMaxNest_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.CpuDisIntMaxNest = eep_ram_image.CpuDisIntMaxNest;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_CpuDisIntMaxNest_IN_BLOCK_NUM);
}
#endif
//static void Proc_CpuDisIntMaxNest_RestoreToDefault(void)
//{
//#if EEP_CONTENT_CpuDisIntMaxNest_CONST_VARIBLE
//	GenEep_Set_CpuDisIntMaxNest(dflt_eep_val.CpuDisIntMaxNest);
//#endif	
//}
#if EEP_CONTENT_CpuDisIntMaxNest_CONST_VARIBLE
#define Proc_CpuDisIntMaxNest_RestoreToDefault()	do{GenEep_Set_CpuDisIntMaxNest(dflt_eep_val.CpuDisIntMaxNest);}while(0)
#else
#define Proc_CpuDisIntMaxNest_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> CpuDisIntMaxNest function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> CpuDisIntMaxTm function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_CpuDisIntMaxTm_ITEM_LEN > 1
void GenEep_Get_CpuDisIntMaxTm(uint16_t * const CpuDisIntMaxTm )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_CpuDisIntMaxTm_ITEM_LEN; i ++)
    {
        CpuDisIntMaxTm[i] = eep_ram_image.CpuDisIntMaxTm[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_CpuDisIntMaxTm_Ex(uint8_t * const CpuDisIntMaxTm,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.CpuDisIntMaxTm;	//EEP_CONTENT_CpuDisIntMaxTm_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_CpuDisIntMaxTm_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_CpuDisIntMaxTm_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        CpuDisIntMaxTm[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_CpuDisIntMaxTm(uint16_t const* CpuDisIntMaxTm )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_CpuDisIntMaxTm_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.CpuDisIntMaxTm[i] != CpuDisIntMaxTm[i] )
        {
            eep_ram_image.CpuDisIntMaxTm[i] = CpuDisIntMaxTm[i];
            wr_eep_by_list_req[EEP_CONTENT_CpuDisIntMaxTm_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_CpuDisIntMaxTm_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_CpuDisIntMaxTm_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_CpuDisIntMaxTm_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:CpuDisIntMaxTm firt item[0] = %d",CpuDisIntMaxTm[0]);
}

void GenEep_Set_CpuDisIntMaxTm_Ex(uint8_t const* CpuDisIntMaxTm,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_CpuDisIntMaxTm_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_CpuDisIntMaxTm_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.CpuDisIntMaxTm;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != CpuDisIntMaxTm[i] )
        {
            dest[offset+i] = CpuDisIntMaxTm[i];
            wr_eep_by_list_req[EEP_CONTENT_CpuDisIntMaxTm_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_CpuDisIntMaxTm_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_CpuDisIntMaxTm_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_CpuDisIntMaxTm_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:CpuDisIntMaxTm by Extend Api");
}

static void EepCopy_CpuDisIntMaxTm_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_CpuDisIntMaxTm_ITEM_LEN; i ++)
    {
        eep_hw_image.CpuDisIntMaxTm[i] = eep_ram_image.CpuDisIntMaxTm[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_CpuDisIntMaxTm_IN_BLOCK_NUM);
}

#else 
uint16_t GenEep_Get_CpuDisIntMaxTm(void)
{
    uint16_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.CpuDisIntMaxTm;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_CpuDisIntMaxTm(uint16_t const CpuDisIntMaxTm)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.CpuDisIntMaxTm != CpuDisIntMaxTm )
    {
        eep_ram_image.CpuDisIntMaxTm = CpuDisIntMaxTm;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_CpuDisIntMaxTm_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_CpuDisIntMaxTm_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_CpuDisIntMaxTm_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_CpuDisIntMaxTm_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:CpuDisIntMaxTm=%d",CpuDisIntMaxTm);
}

static void EepCopy_CpuDisIntMaxTm_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.CpuDisIntMaxTm = eep_ram_image.CpuDisIntMaxTm;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_CpuDisIntMaxTm_IN_BLOCK_NUM);
}
#endif
//static void Proc_CpuDisIntMaxTm_RestoreToDefault(void)
//{
//#if EEP_CONTENT_CpuDisIntMaxTm_CONST_VARIBLE
//	GenEep_Set_CpuDisIntMaxTm(dflt_eep_val.CpuDisIntMaxTm);
//#endif	
//}
#if EEP_CONTENT_CpuDisIntMaxTm_CONST_VARIBLE
#define Proc_CpuDisIntMaxTm_RestoreToDefault()	do{GenEep_Set_CpuDisIntMaxTm(dflt_eep_val.CpuDisIntMaxTm);}while(0)
#else
#define Proc_CpuDisIntMaxTm_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> CpuDisIntMaxTm function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> CpuDisIntTooLongCnt function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_CpuDisIntTooLongCnt_ITEM_LEN > 1
void GenEep_Get_CpuDisIntTooLongCnt(uint16_t * const CpuDisIntTooLongCnt )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_CpuDisIntTooLongCnt_ITEM_LEN; i ++)
    {
        CpuDisIntTooLongCnt[i] = eep_ram_image.CpuDisIntTooLongCnt[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_CpuDisIntTooLongCnt_Ex(uint8_t * const CpuDisIntTooLongCnt,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.CpuDisIntTooLongCnt;	//EEP_CONTENT_CpuDisIntTooLongCnt_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_CpuDisIntTooLongCnt_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_CpuDisIntTooLongCnt_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        CpuDisIntTooLongCnt[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_CpuDisIntTooLongCnt(uint16_t const* CpuDisIntTooLongCnt )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_CpuDisIntTooLongCnt_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.CpuDisIntTooLongCnt[i] != CpuDisIntTooLongCnt[i] )
        {
            eep_ram_image.CpuDisIntTooLongCnt[i] = CpuDisIntTooLongCnt[i];
            wr_eep_by_list_req[EEP_CONTENT_CpuDisIntTooLongCnt_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_CpuDisIntTooLongCnt_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_CpuDisIntTooLongCnt_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_CpuDisIntTooLongCnt_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:CpuDisIntTooLongCnt firt item[0] = %d",CpuDisIntTooLongCnt[0]);
}

void GenEep_Set_CpuDisIntTooLongCnt_Ex(uint8_t const* CpuDisIntTooLongCnt,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_CpuDisIntTooLongCnt_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_CpuDisIntTooLongCnt_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.CpuDisIntTooLongCnt;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != CpuDisIntTooLongCnt[i] )
        {
            dest[offset+i] = CpuDisIntTooLongCnt[i];
            wr_eep_by_list_req[EEP_CONTENT_CpuDisIntTooLongCnt_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_CpuDisIntTooLongCnt_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_CpuDisIntTooLongCnt_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_CpuDisIntTooLongCnt_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:CpuDisIntTooLongCnt by Extend Api");
}

static void EepCopy_CpuDisIntTooLongCnt_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_CpuDisIntTooLongCnt_ITEM_LEN; i ++)
    {
        eep_hw_image.CpuDisIntTooLongCnt[i] = eep_ram_image.CpuDisIntTooLongCnt[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_CpuDisIntTooLongCnt_IN_BLOCK_NUM);
}

#else 
uint16_t GenEep_Get_CpuDisIntTooLongCnt(void)
{
    uint16_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.CpuDisIntTooLongCnt;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_CpuDisIntTooLongCnt(uint16_t const CpuDisIntTooLongCnt)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.CpuDisIntTooLongCnt != CpuDisIntTooLongCnt )
    {
        eep_ram_image.CpuDisIntTooLongCnt = CpuDisIntTooLongCnt;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_CpuDisIntTooLongCnt_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_CpuDisIntTooLongCnt_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_CpuDisIntTooLongCnt_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_CpuDisIntTooLongCnt_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:CpuDisIntTooLongCnt=%d",CpuDisIntTooLongCnt);
}

static void EepCopy_CpuDisIntTooLongCnt_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.CpuDisIntTooLongCnt = eep_ram_image.CpuDisIntTooLongCnt;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_CpuDisIntTooLongCnt_IN_BLOCK_NUM);
}
#endif
//static void Proc_CpuDisIntTooLongCnt_RestoreToDefault(void)
//{
//#if EEP_CONTENT_CpuDisIntTooLongCnt_CONST_VARIBLE
//	GenEep_Set_CpuDisIntTooLongCnt(dflt_eep_val.CpuDisIntTooLongCnt);
//#endif	
//}
#if EEP_CONTENT_CpuDisIntTooLongCnt_CONST_VARIBLE
#define Proc_CpuDisIntTooLongCnt_RestoreToDefault()	do{GenEep_Set_CpuDisIntTooLongCnt(dflt_eep_val.CpuDisIntTooLongCnt);}while(0)
#else
#define Proc_CpuDisIntTooLongCnt_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> CpuDisIntTooLongCnt function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> SocResetReason function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_SocResetReason_ITEM_LEN > 1
void GenEep_Get_SocResetReason(uint8_t * const SocResetReason )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_SocResetReason_ITEM_LEN; i ++)
    {
        SocResetReason[i] = eep_ram_image.SocResetReason[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_SocResetReason_Ex(uint8_t * const SocResetReason,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.SocResetReason;	//EEP_CONTENT_SocResetReason_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_SocResetReason_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_SocResetReason_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        SocResetReason[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_SocResetReason(uint8_t const* SocResetReason )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_SocResetReason_ITEM_LEN; i ++)
    {
        if ( eep_ram_image.SocResetReason[i] != SocResetReason[i] )
        {
            eep_ram_image.SocResetReason[i] = SocResetReason[i];
            wr_eep_by_list_req[EEP_CONTENT_SocResetReason_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_SocResetReason_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_SocResetReason_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_SocResetReason_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:SocResetReason firt item[0] = %d",SocResetReason[0]);
}

void GenEep_Set_SocResetReason_Ex(uint8_t const* SocResetReason,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_SocResetReason_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_SocResetReason_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.SocResetReason;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != SocResetReason[i] )
        {
            dest[offset+i] = SocResetReason[i];
            wr_eep_by_list_req[EEP_CONTENT_SocResetReason_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_SocResetReason_LIST_ID&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[EEP_CONTENT_SocResetReason_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_SocResetReason_LIST_ID&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:SocResetReason by Extend Api");
}

static void EepCopy_SocResetReason_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < EEP_CONTENT_SocResetReason_ITEM_LEN; i ++)
    {
        eep_hw_image.SocResetReason[i] = eep_ram_image.SocResetReason[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_SocResetReason_IN_BLOCK_NUM);
}

#else 
uint8_t GenEep_Get_SocResetReason(void)
{
    uint8_t val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.SocResetReason;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_SocResetReason(uint8_t const SocResetReason)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.SocResetReason != SocResetReason )
    {
        eep_ram_image.SocResetReason = SocResetReason;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[EEP_CONTENT_SocResetReason_LIST_ID/32] 		|= (1u<<(EEP_CONTENT_SocResetReason_LIST_ID&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[EEP_CONTENT_SocResetReason_LIST_ID/32] 	|= (1u<<(EEP_CONTENT_SocResetReason_LIST_ID&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:SocResetReason=%d",SocResetReason);
}

static void EepCopy_SocResetReason_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.SocResetReason = eep_ram_image.SocResetReason;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << EEP_CONTENT_SocResetReason_IN_BLOCK_NUM);
}
#endif
//static void Proc_SocResetReason_RestoreToDefault(void)
//{
//#if EEP_CONTENT_SocResetReason_CONST_VARIBLE
//	GenEep_Set_SocResetReason(dflt_eep_val.SocResetReason);
//#endif	
//}
#if EEP_CONTENT_SocResetReason_CONST_VARIBLE
#define Proc_SocResetReason_RestoreToDefault()	do{GenEep_Set_SocResetReason(dflt_eep_val.SocResetReason);}while(0)
#else
#define Proc_SocResetReason_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> SocResetReason function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/



/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void GenEep_Get_ByOffset(uint16_t const offset, uint16_t len,uint8_t *const data)
{
    if ( offset < EEP_SIZE )
    {
        if ( offset + len > EEP_SIZE )
        {
            len = EEP_SIZE - offset;
        }
        uint8_t *eep = (uint8_t*)(&eep_ram_image) + offset;
        CPU_SR_ALLOC();
        CPU_CRITICAL_ENTER();
        for ( uint16_t i = 0; i < len; i ++ )
        {
            data[i] = eep[i];
        }
        CPU_CRITICAL_EXIT();
    }
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void GenEep_Set_ByOffset(uint16_t const offset, uint16_t len,uint8_t const* data)
{
    if ( offset < EEP_SIZE )
    {
        if ( offset + len > EEP_SIZE )
        {
            len = EEP_SIZE - offset;
        }
        uint8_t *eep = (uint8_t*)(&eep_ram_image) + offset;
        CPU_SR_ALLOC();
        CPU_CRITICAL_ENTER();
        for ( uint16_t i = 0; i < len; i ++ )
        {
            eep[i] = data[i];
        }
        CPU_CRITICAL_EXIT();
    }
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void EepBlock_manufacture_Init(void)
{	
	/*lint -save -e831 -e662 -e661*/
#if EEP_BLOCK_manufacture_IS_EEPROM
	uint32 crc ;
	uint8_t *src;
	uint8_t *desc;

	(void)HalEep_Read(EEP_BLOCK_manufacture_NORMAL_ADDR,(uint8 *)(&eep_ram_image.manufacture_write_count),EEP_BLOCK_manufacture_LEN);
	crc = Crc32_Calc((uint8_t*)(&eep_ram_image.manufacture_write_count),EEP_BLOCK_manufacture_LEN-4);
	if ( crc != eep_ram_image.manufacture_crc )
	{	// use backup data
		TRACE_TEXT(EEP_ERR,MOD_ALWAYS,"Eeprom Block:manufacture Normal Content CRC error..");
		(void)HalEep_Read(EEP_BLOCK_manufacture_BACKUP_ADDR,(uint8 *)(&eep_ram_image.manufacture_write_count),EEP_BLOCK_manufacture_LEN);
		crc = Crc32_Calc((uint8_t*)(&eep_ram_image.manufacture_write_count),EEP_BLOCK_manufacture_LEN-4);
		if ( crc !=  eep_ram_image.manufacture_crc )
		{	// use default value
			TRACE_TEXT(EEP_ERR,MOD_ALWAYS,"Eeprom Block:manufacture Backup Content CRC error, Use default value");
			desc = (uint8_t*)&eep_ram_image.manufacture_write_count;
			src  = (uint8_t*)&dflt_eep_val.manufacture_write_count;
			for( uint16 i = 0; i < EEP_BLOCK_manufacture_LEN-4; i ++ )
			{
				desc[i] = src[i];
			}
			eep_ram_image.manufacture_crc = Crc32_Calc((uint8_t*)(&eep_ram_image.manufacture_write_count),EEP_BLOCK_manufacture_LEN-4);
		}

		eep_write_block_req |= (1u << EEP_BLOCK_manufacture_BLOCK_ID);
	}

	src = (uint8_t*)&eep_ram_image.manufacture_write_count;
	desc= (uint8_t*)&eep_hw_image.manufacture_write_count;
	for( uint16 i = 0; i < EEP_BLOCK_manufacture_LEN; i ++ )
	{
		desc[i] = src[i];
	}
#else	//EEP_BLOCK_manufacture_IS_EEPROM
	uint32_t 	write_count = 0;
	uint16_t	dflash_block_avbl_idx=0;
	uint32_t	crc;
	for( uint16_t i = 0; i < EEP_BLOCK_manufacture_DFLASH_SECTOR_NUM*DFLASH_SECTOR_SIZE/EEP_BLOCK_manufacture_LEN; i ++ )
	{
		(void)HalDFlash_ReadData(EEP_BLOCK_manufacture_DFLASH_START_ADDR+i*EEP_BLOCK_manufacture_LEN,(uint8 *)(&eep_ram_image.manufacture_write_count),EEP_BLOCK_manufacture_LEN);
		crc = Crc32_Calc((uint8_t*)(&eep_ram_image.manufacture_write_count),EEP_BLOCK_manufacture_LEN-4);
		if ( crc == eep_ram_image.manufacture_crc )
		{	
			if ( write_count < eep_ram_image.manufacture_write_count )
			{
				write_count 			= 	eep_ram_image.manufacture_write_count;
				dflash_block_avbl_idx	=	i;
			}
		}
	}
	boolean dflash_restore_default = FALSE;
	if ( write_count )
	{
		dflash_manufacture_current_write_addr = EEP_BLOCK_manufacture_DFLASH_START_ADDR+dflash_block_avbl_idx*EEP_BLOCK_manufacture_LEN;
		(void)HalDFlash_ReadData(dflash_manufacture_current_write_addr,(uint8 *)(&eep_ram_image.manufacture_write_count),EEP_BLOCK_manufacture_LEN);
		/* check again */
		if(	eep_ram_image.manufacture_crc == Crc32_Calc((uint8_t*)(&eep_ram_image.manufacture_write_count),EEP_BLOCK_manufacture_LEN-4) )
		{	/* re-check ok, */
			uint8_t *src;
			uint8_t *desc;
			src = (uint8_t*)&eep_ram_image.manufacture_write_count;
			desc= (uint8_t*)&eep_hw_image.manufacture_write_count;
			for( uint16_t i = 0; i < EEP_BLOCK_manufacture_LEN; i ++ )
			{
				desc[i] = src[i];
			}
		}
		else
		{
			dflash_restore_default = TRUE;
		}
		
		dflash_manufacture_current_write_addr 	+= 	EEP_BLOCK_manufacture_LEN;	
		if ( dflash_manufacture_current_write_addr >= EEP_BLOCK_manufacture_DFLASH_END_ADDR )
		{
			dflash_manufacture_current_write_addr = EEP_BLOCK_manufacture_DFLASH_START_ADDR;
		}
	}
	else
	{
		dflash_restore_default = TRUE;
	}
	if ( dflash_restore_default )
	{
		uint8_t *src;
		uint8_t *desc;
		desc = (uint8_t*)&eep_ram_image.manufacture_write_count;
		src  = (uint8_t*)&dflt_eep_val.manufacture_write_count;
		for( uint16 i = 0; i < EEP_BLOCK_manufacture_LEN-4; i ++ )
		{
			desc[i] = src[i];
		}	
		eep_ram_image.manufacture_crc = Crc32_Calc((uint8_t*)(&eep_ram_image.manufacture_write_count),EEP_BLOCK_manufacture_LEN-4);
		src = (uint8_t*)&eep_ram_image.manufacture_write_count;
		desc= (uint8_t*)&eep_hw_image.manufacture_write_count;
		for( uint16_t i = 0; i < EEP_BLOCK_manufacture_LEN; i ++ )
		{
			desc[i] = src[i];
		}

		TRACE_TEXT(EEP_ERR,MOD_ALWAYS,"Eeprom Block:manufacture Dflash Error, Use default value");
		eep_write_block_req |= (1u << EEP_BLOCK_manufacture_BLOCK_ID);
		dflash_manufacture_current_write_addr = EEP_BLOCK_manufacture_DFLASH_START_ADDR;
	}
#endif	//EEP_BLOCK_manufacture_IS_EEPROM
	/*lint -restore */	
}

static void EepBlock_development_nvm_Init(void)
{	
	/*lint -save -e831 -e662 -e661*/
#if EEP_BLOCK_development_nvm_IS_EEPROM
	uint32 crc ;
	uint8_t *src;
	uint8_t *desc;

	(void)HalEep_Read(EEP_BLOCK_development_nvm_NORMAL_ADDR,(uint8 *)(&eep_ram_image.development_nvm_write_count),EEP_BLOCK_development_nvm_LEN);
	crc = Crc32_Calc((uint8_t*)(&eep_ram_image.development_nvm_write_count),EEP_BLOCK_development_nvm_LEN-4);
	if ( crc != eep_ram_image.development_nvm_crc )
	{	// use backup data
		TRACE_TEXT(EEP_ERR,MOD_ALWAYS,"Eeprom Block:development_nvm Normal Content CRC error..");
		(void)HalEep_Read(EEP_BLOCK_development_nvm_BACKUP_ADDR,(uint8 *)(&eep_ram_image.development_nvm_write_count),EEP_BLOCK_development_nvm_LEN);
		crc = Crc32_Calc((uint8_t*)(&eep_ram_image.development_nvm_write_count),EEP_BLOCK_development_nvm_LEN-4);
		if ( crc !=  eep_ram_image.development_nvm_crc )
		{	// use default value
			TRACE_TEXT(EEP_ERR,MOD_ALWAYS,"Eeprom Block:development_nvm Backup Content CRC error, Use default value");
			desc = (uint8_t*)&eep_ram_image.development_nvm_write_count;
			src  = (uint8_t*)&dflt_eep_val.development_nvm_write_count;
			for( uint16 i = 0; i < EEP_BLOCK_development_nvm_LEN-4; i ++ )
			{
				desc[i] = src[i];
			}
			eep_ram_image.development_nvm_crc = Crc32_Calc((uint8_t*)(&eep_ram_image.development_nvm_write_count),EEP_BLOCK_development_nvm_LEN-4);
		}

		eep_write_block_req |= (1u << EEP_BLOCK_development_nvm_BLOCK_ID);
	}

	src = (uint8_t*)&eep_ram_image.development_nvm_write_count;
	desc= (uint8_t*)&eep_hw_image.development_nvm_write_count;
	for( uint16 i = 0; i < EEP_BLOCK_development_nvm_LEN; i ++ )
	{
		desc[i] = src[i];
	}
#else	//EEP_BLOCK_development_nvm_IS_EEPROM
	uint32_t 	write_count = 0;
	uint16_t	dflash_block_avbl_idx=0;
	uint32_t	crc;
	for( uint16_t i = 0; i < EEP_BLOCK_development_nvm_DFLASH_SECTOR_NUM*DFLASH_SECTOR_SIZE/EEP_BLOCK_development_nvm_LEN; i ++ )
	{
		(void)HalDFlash_ReadData(EEP_BLOCK_development_nvm_DFLASH_START_ADDR+i*EEP_BLOCK_development_nvm_LEN,(uint8 *)(&eep_ram_image.development_nvm_write_count),EEP_BLOCK_development_nvm_LEN);
		crc = Crc32_Calc((uint8_t*)(&eep_ram_image.development_nvm_write_count),EEP_BLOCK_development_nvm_LEN-4);
		if ( crc == eep_ram_image.development_nvm_crc )
		{	
			if ( write_count < eep_ram_image.development_nvm_write_count )
			{
				write_count 			= 	eep_ram_image.development_nvm_write_count;
				dflash_block_avbl_idx	=	i;
			}
		}
	}
	boolean dflash_restore_default = FALSE;
	if ( write_count )
	{
		dflash_development_nvm_current_write_addr = EEP_BLOCK_development_nvm_DFLASH_START_ADDR+dflash_block_avbl_idx*EEP_BLOCK_development_nvm_LEN;
		(void)HalDFlash_ReadData(dflash_development_nvm_current_write_addr,(uint8 *)(&eep_ram_image.development_nvm_write_count),EEP_BLOCK_development_nvm_LEN);
		/* check again */
		if(	eep_ram_image.development_nvm_crc == Crc32_Calc((uint8_t*)(&eep_ram_image.development_nvm_write_count),EEP_BLOCK_development_nvm_LEN-4) )
		{	/* re-check ok, */
			uint8_t *src;
			uint8_t *desc;
			src = (uint8_t*)&eep_ram_image.development_nvm_write_count;
			desc= (uint8_t*)&eep_hw_image.development_nvm_write_count;
			for( uint16_t i = 0; i < EEP_BLOCK_development_nvm_LEN; i ++ )
			{
				desc[i] = src[i];
			}
		}
		else
		{
			dflash_restore_default = TRUE;
		}
		
		dflash_development_nvm_current_write_addr 	+= 	EEP_BLOCK_development_nvm_LEN;	
		if ( dflash_development_nvm_current_write_addr >= EEP_BLOCK_development_nvm_DFLASH_END_ADDR )
		{
			dflash_development_nvm_current_write_addr = EEP_BLOCK_development_nvm_DFLASH_START_ADDR;
		}
	}
	else
	{
		dflash_restore_default = TRUE;
	}
	if ( dflash_restore_default )
	{
		uint8_t *src;
		uint8_t *desc;
		desc = (uint8_t*)&eep_ram_image.development_nvm_write_count;
		src  = (uint8_t*)&dflt_eep_val.development_nvm_write_count;
		for( uint16 i = 0; i < EEP_BLOCK_development_nvm_LEN-4; i ++ )
		{
			desc[i] = src[i];
		}	
		eep_ram_image.development_nvm_crc = Crc32_Calc((uint8_t*)(&eep_ram_image.development_nvm_write_count),EEP_BLOCK_development_nvm_LEN-4);
		src = (uint8_t*)&eep_ram_image.development_nvm_write_count;
		desc= (uint8_t*)&eep_hw_image.development_nvm_write_count;
		for( uint16_t i = 0; i < EEP_BLOCK_development_nvm_LEN; i ++ )
		{
			desc[i] = src[i];
		}

		TRACE_TEXT(EEP_ERR,MOD_ALWAYS,"Eeprom Block:development_nvm Dflash Error, Use default value");
		eep_write_block_req |= (1u << EEP_BLOCK_development_nvm_BLOCK_ID);
		dflash_development_nvm_current_write_addr = EEP_BLOCK_development_nvm_DFLASH_START_ADDR;
	}
#endif	//EEP_BLOCK_development_nvm_IS_EEPROM
	/*lint -restore */	
}

static void EepBlock_OemSetting_Init(void)
{	
	/*lint -save -e831 -e662 -e661*/
#if EEP_BLOCK_OemSetting_IS_EEPROM
	uint32 crc ;
	uint8_t *src;
	uint8_t *desc;

	(void)HalEep_Read(EEP_BLOCK_OemSetting_NORMAL_ADDR,(uint8 *)(&eep_ram_image.OemSetting_write_count),EEP_BLOCK_OemSetting_LEN);
	crc = Crc32_Calc((uint8_t*)(&eep_ram_image.OemSetting_write_count),EEP_BLOCK_OemSetting_LEN-4);
	if ( crc != eep_ram_image.OemSetting_crc )
	{	// use backup data
		TRACE_TEXT(EEP_ERR,MOD_ALWAYS,"Eeprom Block:OemSetting Normal Content CRC error..");
		(void)HalEep_Read(EEP_BLOCK_OemSetting_BACKUP_ADDR,(uint8 *)(&eep_ram_image.OemSetting_write_count),EEP_BLOCK_OemSetting_LEN);
		crc = Crc32_Calc((uint8_t*)(&eep_ram_image.OemSetting_write_count),EEP_BLOCK_OemSetting_LEN-4);
		if ( crc !=  eep_ram_image.OemSetting_crc )
		{	// use default value
			TRACE_TEXT(EEP_ERR,MOD_ALWAYS,"Eeprom Block:OemSetting Backup Content CRC error, Use default value");
			desc = (uint8_t*)&eep_ram_image.OemSetting_write_count;
			src  = (uint8_t*)&dflt_eep_val.OemSetting_write_count;
			for( uint16 i = 0; i < EEP_BLOCK_OemSetting_LEN-4; i ++ )
			{
				desc[i] = src[i];
			}
			eep_ram_image.OemSetting_crc = Crc32_Calc((uint8_t*)(&eep_ram_image.OemSetting_write_count),EEP_BLOCK_OemSetting_LEN-4);
		}

		eep_write_block_req |= (1u << EEP_BLOCK_OemSetting_BLOCK_ID);
	}

	src = (uint8_t*)&eep_ram_image.OemSetting_write_count;
	desc= (uint8_t*)&eep_hw_image.OemSetting_write_count;
	for( uint16 i = 0; i < EEP_BLOCK_OemSetting_LEN; i ++ )
	{
		desc[i] = src[i];
	}
#else	//EEP_BLOCK_OemSetting_IS_EEPROM
	uint32_t 	write_count = 0;
	uint16_t	dflash_block_avbl_idx=0;
	uint32_t	crc;
	for( uint16_t i = 0; i < EEP_BLOCK_OemSetting_DFLASH_SECTOR_NUM*DFLASH_SECTOR_SIZE/EEP_BLOCK_OemSetting_LEN; i ++ )
	{
		(void)HalDFlash_ReadData(EEP_BLOCK_OemSetting_DFLASH_START_ADDR+i*EEP_BLOCK_OemSetting_LEN,(uint8 *)(&eep_ram_image.OemSetting_write_count),EEP_BLOCK_OemSetting_LEN);
		crc = Crc32_Calc((uint8_t*)(&eep_ram_image.OemSetting_write_count),EEP_BLOCK_OemSetting_LEN-4);
		if ( crc == eep_ram_image.OemSetting_crc )
		{	
			if ( write_count < eep_ram_image.OemSetting_write_count )
			{
				write_count 			= 	eep_ram_image.OemSetting_write_count;
				dflash_block_avbl_idx	=	i;
			}
		}
	}
	boolean dflash_restore_default = FALSE;
	if ( write_count )
	{
		dflash_OemSetting_current_write_addr = EEP_BLOCK_OemSetting_DFLASH_START_ADDR+dflash_block_avbl_idx*EEP_BLOCK_OemSetting_LEN;
		(void)HalDFlash_ReadData(dflash_OemSetting_current_write_addr,(uint8 *)(&eep_ram_image.OemSetting_write_count),EEP_BLOCK_OemSetting_LEN);
		/* check again */
		if(	eep_ram_image.OemSetting_crc == Crc32_Calc((uint8_t*)(&eep_ram_image.OemSetting_write_count),EEP_BLOCK_OemSetting_LEN-4) )
		{	/* re-check ok, */
			uint8_t *src;
			uint8_t *desc;
			src = (uint8_t*)&eep_ram_image.OemSetting_write_count;
			desc= (uint8_t*)&eep_hw_image.OemSetting_write_count;
			for( uint16_t i = 0; i < EEP_BLOCK_OemSetting_LEN; i ++ )
			{
				desc[i] = src[i];
			}
		}
		else
		{
			dflash_restore_default = TRUE;
		}
		
		dflash_OemSetting_current_write_addr 	+= 	EEP_BLOCK_OemSetting_LEN;	
		if ( dflash_OemSetting_current_write_addr >= EEP_BLOCK_OemSetting_DFLASH_END_ADDR )
		{
			dflash_OemSetting_current_write_addr = EEP_BLOCK_OemSetting_DFLASH_START_ADDR;
		}
	}
	else
	{
		dflash_restore_default = TRUE;
	}
	if ( dflash_restore_default )
	{
		uint8_t *src;
		uint8_t *desc;
		desc = (uint8_t*)&eep_ram_image.OemSetting_write_count;
		src  = (uint8_t*)&dflt_eep_val.OemSetting_write_count;
		for( uint16 i = 0; i < EEP_BLOCK_OemSetting_LEN-4; i ++ )
		{
			desc[i] = src[i];
		}	
		eep_ram_image.OemSetting_crc = Crc32_Calc((uint8_t*)(&eep_ram_image.OemSetting_write_count),EEP_BLOCK_OemSetting_LEN-4);
		src = (uint8_t*)&eep_ram_image.OemSetting_write_count;
		desc= (uint8_t*)&eep_hw_image.OemSetting_write_count;
		for( uint16_t i = 0; i < EEP_BLOCK_OemSetting_LEN; i ++ )
		{
			desc[i] = src[i];
		}

		TRACE_TEXT(EEP_ERR,MOD_ALWAYS,"Eeprom Block:OemSetting Dflash Error, Use default value");
		eep_write_block_req |= (1u << EEP_BLOCK_OemSetting_BLOCK_ID);
		dflash_OemSetting_current_write_addr = EEP_BLOCK_OemSetting_DFLASH_START_ADDR;
	}
#endif	//EEP_BLOCK_OemSetting_IS_EEPROM
	/*lint -restore */	
}

static void EepBlock_user_setting_Init(void)
{	
	/*lint -save -e831 -e662 -e661*/
#if EEP_BLOCK_user_setting_IS_EEPROM
	uint32 crc ;
	uint8_t *src;
	uint8_t *desc;

	(void)HalEep_Read(EEP_BLOCK_user_setting_NORMAL_ADDR,(uint8 *)(&eep_ram_image.user_setting_write_count),EEP_BLOCK_user_setting_LEN);
	crc = Crc32_Calc((uint8_t*)(&eep_ram_image.user_setting_write_count),EEP_BLOCK_user_setting_LEN-4);
	if ( crc != eep_ram_image.user_setting_crc )
	{	// use backup data
		TRACE_TEXT(EEP_ERR,MOD_ALWAYS,"Eeprom Block:user_setting Normal Content CRC error..");
		(void)HalEep_Read(EEP_BLOCK_user_setting_BACKUP_ADDR,(uint8 *)(&eep_ram_image.user_setting_write_count),EEP_BLOCK_user_setting_LEN);
		crc = Crc32_Calc((uint8_t*)(&eep_ram_image.user_setting_write_count),EEP_BLOCK_user_setting_LEN-4);
		if ( crc !=  eep_ram_image.user_setting_crc )
		{	// use default value
			TRACE_TEXT(EEP_ERR,MOD_ALWAYS,"Eeprom Block:user_setting Backup Content CRC error, Use default value");
			desc = (uint8_t*)&eep_ram_image.user_setting_write_count;
			src  = (uint8_t*)&dflt_eep_val.user_setting_write_count;
			for( uint16 i = 0; i < EEP_BLOCK_user_setting_LEN-4; i ++ )
			{
				desc[i] = src[i];
			}
			eep_ram_image.user_setting_crc = Crc32_Calc((uint8_t*)(&eep_ram_image.user_setting_write_count),EEP_BLOCK_user_setting_LEN-4);
		}

		eep_write_block_req |= (1u << EEP_BLOCK_user_setting_BLOCK_ID);
	}

	src = (uint8_t*)&eep_ram_image.user_setting_write_count;
	desc= (uint8_t*)&eep_hw_image.user_setting_write_count;
	for( uint16 i = 0; i < EEP_BLOCK_user_setting_LEN; i ++ )
	{
		desc[i] = src[i];
	}
#else	//EEP_BLOCK_user_setting_IS_EEPROM
	uint32_t 	write_count = 0;
	uint16_t	dflash_block_avbl_idx=0;
	uint32_t	crc;
	for( uint16_t i = 0; i < EEP_BLOCK_user_setting_DFLASH_SECTOR_NUM*DFLASH_SECTOR_SIZE/EEP_BLOCK_user_setting_LEN; i ++ )
	{
		(void)HalDFlash_ReadData(EEP_BLOCK_user_setting_DFLASH_START_ADDR+i*EEP_BLOCK_user_setting_LEN,(uint8 *)(&eep_ram_image.user_setting_write_count),EEP_BLOCK_user_setting_LEN);
		crc = Crc32_Calc((uint8_t*)(&eep_ram_image.user_setting_write_count),EEP_BLOCK_user_setting_LEN-4);
		if ( crc == eep_ram_image.user_setting_crc )
		{	
			if ( write_count < eep_ram_image.user_setting_write_count )
			{
				write_count 			= 	eep_ram_image.user_setting_write_count;
				dflash_block_avbl_idx	=	i;
			}
		}
	}
	boolean dflash_restore_default = FALSE;
	if ( write_count )
	{
		dflash_user_setting_current_write_addr = EEP_BLOCK_user_setting_DFLASH_START_ADDR+dflash_block_avbl_idx*EEP_BLOCK_user_setting_LEN;
		(void)HalDFlash_ReadData(dflash_user_setting_current_write_addr,(uint8 *)(&eep_ram_image.user_setting_write_count),EEP_BLOCK_user_setting_LEN);
		/* check again */
		if(	eep_ram_image.user_setting_crc == Crc32_Calc((uint8_t*)(&eep_ram_image.user_setting_write_count),EEP_BLOCK_user_setting_LEN-4) )
		{	/* re-check ok, */
			uint8_t *src;
			uint8_t *desc;
			src = (uint8_t*)&eep_ram_image.user_setting_write_count;
			desc= (uint8_t*)&eep_hw_image.user_setting_write_count;
			for( uint16_t i = 0; i < EEP_BLOCK_user_setting_LEN; i ++ )
			{
				desc[i] = src[i];
			}
		}
		else
		{
			dflash_restore_default = TRUE;
		}
		
		dflash_user_setting_current_write_addr 	+= 	EEP_BLOCK_user_setting_LEN;	
		if ( dflash_user_setting_current_write_addr >= EEP_BLOCK_user_setting_DFLASH_END_ADDR )
		{
			dflash_user_setting_current_write_addr = EEP_BLOCK_user_setting_DFLASH_START_ADDR;
		}
	}
	else
	{
		dflash_restore_default = TRUE;
	}
	if ( dflash_restore_default )
	{
		uint8_t *src;
		uint8_t *desc;
		desc = (uint8_t*)&eep_ram_image.user_setting_write_count;
		src  = (uint8_t*)&dflt_eep_val.user_setting_write_count;
		for( uint16 i = 0; i < EEP_BLOCK_user_setting_LEN-4; i ++ )
		{
			desc[i] = src[i];
		}	
		eep_ram_image.user_setting_crc = Crc32_Calc((uint8_t*)(&eep_ram_image.user_setting_write_count),EEP_BLOCK_user_setting_LEN-4);
		src = (uint8_t*)&eep_ram_image.user_setting_write_count;
		desc= (uint8_t*)&eep_hw_image.user_setting_write_count;
		for( uint16_t i = 0; i < EEP_BLOCK_user_setting_LEN; i ++ )
		{
			desc[i] = src[i];
		}

		TRACE_TEXT(EEP_ERR,MOD_ALWAYS,"Eeprom Block:user_setting Dflash Error, Use default value");
		eep_write_block_req |= (1u << EEP_BLOCK_user_setting_BLOCK_ID);
		dflash_user_setting_current_write_addr = EEP_BLOCK_user_setting_DFLASH_START_ADDR;
	}
#endif	//EEP_BLOCK_user_setting_IS_EEPROM
	/*lint -restore */	
}

static void EepBlock_DTC_INFO_Init(void)
{	
	/*lint -save -e831 -e662 -e661*/
#if EEP_BLOCK_DTC_INFO_IS_EEPROM
	uint32 crc ;
	uint8_t *src;
	uint8_t *desc;

	(void)HalEep_Read(EEP_BLOCK_DTC_INFO_NORMAL_ADDR,(uint8 *)(&eep_ram_image.DTC_INFO_write_count),EEP_BLOCK_DTC_INFO_LEN);
	crc = Crc32_Calc((uint8_t*)(&eep_ram_image.DTC_INFO_write_count),EEP_BLOCK_DTC_INFO_LEN-4);
	if ( crc != eep_ram_image.DTC_INFO_crc )
	{	// use backup data
		TRACE_TEXT(EEP_ERR,MOD_ALWAYS,"Eeprom Block:DTC_INFO Normal Content CRC error..");
		(void)HalEep_Read(EEP_BLOCK_DTC_INFO_BACKUP_ADDR,(uint8 *)(&eep_ram_image.DTC_INFO_write_count),EEP_BLOCK_DTC_INFO_LEN);
		crc = Crc32_Calc((uint8_t*)(&eep_ram_image.DTC_INFO_write_count),EEP_BLOCK_DTC_INFO_LEN-4);
		if ( crc !=  eep_ram_image.DTC_INFO_crc )
		{	// use default value
			TRACE_TEXT(EEP_ERR,MOD_ALWAYS,"Eeprom Block:DTC_INFO Backup Content CRC error, Use default value");
			desc = (uint8_t*)&eep_ram_image.DTC_INFO_write_count;
			src  = (uint8_t*)&dflt_eep_val.DTC_INFO_write_count;
			for( uint16 i = 0; i < EEP_BLOCK_DTC_INFO_LEN-4; i ++ )
			{
				desc[i] = src[i];
			}
			eep_ram_image.DTC_INFO_crc = Crc32_Calc((uint8_t*)(&eep_ram_image.DTC_INFO_write_count),EEP_BLOCK_DTC_INFO_LEN-4);
		}

		eep_write_block_req |= (1u << EEP_BLOCK_DTC_INFO_BLOCK_ID);
	}

	src = (uint8_t*)&eep_ram_image.DTC_INFO_write_count;
	desc= (uint8_t*)&eep_hw_image.DTC_INFO_write_count;
	for( uint16 i = 0; i < EEP_BLOCK_DTC_INFO_LEN; i ++ )
	{
		desc[i] = src[i];
	}
#else	//EEP_BLOCK_DTC_INFO_IS_EEPROM
	uint32_t 	write_count = 0;
	uint16_t	dflash_block_avbl_idx=0;
	uint32_t	crc;
	for( uint16_t i = 0; i < EEP_BLOCK_DTC_INFO_DFLASH_SECTOR_NUM*DFLASH_SECTOR_SIZE/EEP_BLOCK_DTC_INFO_LEN; i ++ )
	{
		(void)HalDFlash_ReadData(EEP_BLOCK_DTC_INFO_DFLASH_START_ADDR+i*EEP_BLOCK_DTC_INFO_LEN,(uint8 *)(&eep_ram_image.DTC_INFO_write_count),EEP_BLOCK_DTC_INFO_LEN);
		crc = Crc32_Calc((uint8_t*)(&eep_ram_image.DTC_INFO_write_count),EEP_BLOCK_DTC_INFO_LEN-4);
		if ( crc == eep_ram_image.DTC_INFO_crc )
		{	
			if ( write_count < eep_ram_image.DTC_INFO_write_count )
			{
				write_count 			= 	eep_ram_image.DTC_INFO_write_count;
				dflash_block_avbl_idx	=	i;
			}
		}
	}
	boolean dflash_restore_default = FALSE;
	if ( write_count )
	{
		dflash_DTC_INFO_current_write_addr = EEP_BLOCK_DTC_INFO_DFLASH_START_ADDR+dflash_block_avbl_idx*EEP_BLOCK_DTC_INFO_LEN;
		(void)HalDFlash_ReadData(dflash_DTC_INFO_current_write_addr,(uint8 *)(&eep_ram_image.DTC_INFO_write_count),EEP_BLOCK_DTC_INFO_LEN);
		/* check again */
		if(	eep_ram_image.DTC_INFO_crc == Crc32_Calc((uint8_t*)(&eep_ram_image.DTC_INFO_write_count),EEP_BLOCK_DTC_INFO_LEN-4) )
		{	/* re-check ok, */
			uint8_t *src;
			uint8_t *desc;
			src = (uint8_t*)&eep_ram_image.DTC_INFO_write_count;
			desc= (uint8_t*)&eep_hw_image.DTC_INFO_write_count;
			for( uint16_t i = 0; i < EEP_BLOCK_DTC_INFO_LEN; i ++ )
			{
				desc[i] = src[i];
			}
		}
		else
		{
			dflash_restore_default = TRUE;
		}
		
		dflash_DTC_INFO_current_write_addr 	+= 	EEP_BLOCK_DTC_INFO_LEN;	
		if ( dflash_DTC_INFO_current_write_addr >= EEP_BLOCK_DTC_INFO_DFLASH_END_ADDR )
		{
			dflash_DTC_INFO_current_write_addr = EEP_BLOCK_DTC_INFO_DFLASH_START_ADDR;
		}
	}
	else
	{
		dflash_restore_default = TRUE;
	}
	if ( dflash_restore_default )
	{
		uint8_t *src;
		uint8_t *desc;
		desc = (uint8_t*)&eep_ram_image.DTC_INFO_write_count;
		src  = (uint8_t*)&dflt_eep_val.DTC_INFO_write_count;
		for( uint16 i = 0; i < EEP_BLOCK_DTC_INFO_LEN-4; i ++ )
		{
			desc[i] = src[i];
		}	
		eep_ram_image.DTC_INFO_crc = Crc32_Calc((uint8_t*)(&eep_ram_image.DTC_INFO_write_count),EEP_BLOCK_DTC_INFO_LEN-4);
		src = (uint8_t*)&eep_ram_image.DTC_INFO_write_count;
		desc= (uint8_t*)&eep_hw_image.DTC_INFO_write_count;
		for( uint16_t i = 0; i < EEP_BLOCK_DTC_INFO_LEN; i ++ )
		{
			desc[i] = src[i];
		}

		TRACE_TEXT(EEP_ERR,MOD_ALWAYS,"Eeprom Block:DTC_INFO Dflash Error, Use default value");
		eep_write_block_req |= (1u << EEP_BLOCK_DTC_INFO_BLOCK_ID);
		dflash_DTC_INFO_current_write_addr = EEP_BLOCK_DTC_INFO_DFLASH_START_ADDR;
	}
#endif	//EEP_BLOCK_DTC_INFO_IS_EEPROM
	/*lint -restore */	
}

static void EepBlock_OdoInfo_Init(void)
{	
	/*lint -save -e831 -e662 -e661*/
#if EEP_BLOCK_OdoInfo_IS_EEPROM
	uint32 crc ;
	uint8_t *src;
	uint8_t *desc;

	(void)HalEep_Read(EEP_BLOCK_OdoInfo_NORMAL_ADDR,(uint8 *)(&eep_ram_image.OdoInfo_write_count),EEP_BLOCK_OdoInfo_LEN);
	crc = Crc32_Calc((uint8_t*)(&eep_ram_image.OdoInfo_write_count),EEP_BLOCK_OdoInfo_LEN-4);
	if ( crc != eep_ram_image.OdoInfo_crc )
	{	// use backup data
		TRACE_TEXT(EEP_ERR,MOD_ALWAYS,"Eeprom Block:OdoInfo Normal Content CRC error..");
		(void)HalEep_Read(EEP_BLOCK_OdoInfo_BACKUP_ADDR,(uint8 *)(&eep_ram_image.OdoInfo_write_count),EEP_BLOCK_OdoInfo_LEN);
		crc = Crc32_Calc((uint8_t*)(&eep_ram_image.OdoInfo_write_count),EEP_BLOCK_OdoInfo_LEN-4);
		if ( crc !=  eep_ram_image.OdoInfo_crc )
		{	// use default value
			TRACE_TEXT(EEP_ERR,MOD_ALWAYS,"Eeprom Block:OdoInfo Backup Content CRC error, Use default value");
			desc = (uint8_t*)&eep_ram_image.OdoInfo_write_count;
			src  = (uint8_t*)&dflt_eep_val.OdoInfo_write_count;
			for( uint16 i = 0; i < EEP_BLOCK_OdoInfo_LEN-4; i ++ )
			{
				desc[i] = src[i];
			}
			eep_ram_image.OdoInfo_crc = Crc32_Calc((uint8_t*)(&eep_ram_image.OdoInfo_write_count),EEP_BLOCK_OdoInfo_LEN-4);
		}

		eep_write_block_req |= (1u << EEP_BLOCK_OdoInfo_BLOCK_ID);
	}

	src = (uint8_t*)&eep_ram_image.OdoInfo_write_count;
	desc= (uint8_t*)&eep_hw_image.OdoInfo_write_count;
	for( uint16 i = 0; i < EEP_BLOCK_OdoInfo_LEN; i ++ )
	{
		desc[i] = src[i];
	}
#else	//EEP_BLOCK_OdoInfo_IS_EEPROM
	uint32_t 	write_count = 0;
	uint16_t	dflash_block_avbl_idx=0;
	uint32_t	crc;
	for( uint16_t i = 0; i < EEP_BLOCK_OdoInfo_DFLASH_SECTOR_NUM*DFLASH_SECTOR_SIZE/EEP_BLOCK_OdoInfo_LEN; i ++ )
	{
		(void)HalDFlash_ReadData(EEP_BLOCK_OdoInfo_DFLASH_START_ADDR+i*EEP_BLOCK_OdoInfo_LEN,(uint8 *)(&eep_ram_image.OdoInfo_write_count),EEP_BLOCK_OdoInfo_LEN);
		crc = Crc32_Calc((uint8_t*)(&eep_ram_image.OdoInfo_write_count),EEP_BLOCK_OdoInfo_LEN-4);
		if ( crc == eep_ram_image.OdoInfo_crc )
		{	
			if ( write_count < eep_ram_image.OdoInfo_write_count )
			{
				write_count 			= 	eep_ram_image.OdoInfo_write_count;
				dflash_block_avbl_idx	=	i;
			}
		}
	}
	boolean dflash_restore_default = FALSE;
	if ( write_count )
	{
		dflash_OdoInfo_current_write_addr = EEP_BLOCK_OdoInfo_DFLASH_START_ADDR+dflash_block_avbl_idx*EEP_BLOCK_OdoInfo_LEN;
		(void)HalDFlash_ReadData(dflash_OdoInfo_current_write_addr,(uint8 *)(&eep_ram_image.OdoInfo_write_count),EEP_BLOCK_OdoInfo_LEN);
		/* check again */
		if(	eep_ram_image.OdoInfo_crc == Crc32_Calc((uint8_t*)(&eep_ram_image.OdoInfo_write_count),EEP_BLOCK_OdoInfo_LEN-4) )
		{	/* re-check ok, */
			uint8_t *src;
			uint8_t *desc;
			src = (uint8_t*)&eep_ram_image.OdoInfo_write_count;
			desc= (uint8_t*)&eep_hw_image.OdoInfo_write_count;
			for( uint16_t i = 0; i < EEP_BLOCK_OdoInfo_LEN; i ++ )
			{
				desc[i] = src[i];
			}
		}
		else
		{
			dflash_restore_default = TRUE;
		}
		
		dflash_OdoInfo_current_write_addr 	+= 	EEP_BLOCK_OdoInfo_LEN;	
		if ( dflash_OdoInfo_current_write_addr >= EEP_BLOCK_OdoInfo_DFLASH_END_ADDR )
		{
			dflash_OdoInfo_current_write_addr = EEP_BLOCK_OdoInfo_DFLASH_START_ADDR;
		}
	}
	else
	{
		dflash_restore_default = TRUE;
	}
	if ( dflash_restore_default )
	{
		uint8_t *src;
		uint8_t *desc;
		desc = (uint8_t*)&eep_ram_image.OdoInfo_write_count;
		src  = (uint8_t*)&dflt_eep_val.OdoInfo_write_count;
		for( uint16 i = 0; i < EEP_BLOCK_OdoInfo_LEN-4; i ++ )
		{
			desc[i] = src[i];
		}	
		eep_ram_image.OdoInfo_crc = Crc32_Calc((uint8_t*)(&eep_ram_image.OdoInfo_write_count),EEP_BLOCK_OdoInfo_LEN-4);
		src = (uint8_t*)&eep_ram_image.OdoInfo_write_count;
		desc= (uint8_t*)&eep_hw_image.OdoInfo_write_count;
		for( uint16_t i = 0; i < EEP_BLOCK_OdoInfo_LEN; i ++ )
		{
			desc[i] = src[i];
		}

		TRACE_TEXT(EEP_ERR,MOD_ALWAYS,"Eeprom Block:OdoInfo Dflash Error, Use default value");
		eep_write_block_req |= (1u << EEP_BLOCK_OdoInfo_BLOCK_ID);
		dflash_OdoInfo_current_write_addr = EEP_BLOCK_OdoInfo_DFLASH_START_ADDR;
	}
#endif	//EEP_BLOCK_OdoInfo_IS_EEPROM
	/*lint -restore */	
}

static void EepBlock_share_eep_Init(void)
{	
	/*lint -save -e831 -e662 -e661*/
#if EEP_BLOCK_share_eep_IS_EEPROM
	uint32 crc ;
	uint8_t *src;
	uint8_t *desc;

	(void)HalEep_Read(EEP_BLOCK_share_eep_NORMAL_ADDR,(uint8 *)(&eep_ram_image.share_eep_write_count),EEP_BLOCK_share_eep_LEN);
	crc = Crc32_Calc((uint8_t*)(&eep_ram_image.share_eep_write_count),EEP_BLOCK_share_eep_LEN-4);
	if ( crc != eep_ram_image.share_eep_crc )
	{	// use backup data
		TRACE_TEXT(EEP_ERR,MOD_ALWAYS,"Eeprom Block:share_eep Normal Content CRC error..");
		(void)HalEep_Read(EEP_BLOCK_share_eep_BACKUP_ADDR,(uint8 *)(&eep_ram_image.share_eep_write_count),EEP_BLOCK_share_eep_LEN);
		crc = Crc32_Calc((uint8_t*)(&eep_ram_image.share_eep_write_count),EEP_BLOCK_share_eep_LEN-4);
		if ( crc !=  eep_ram_image.share_eep_crc )
		{	// use default value
			TRACE_TEXT(EEP_ERR,MOD_ALWAYS,"Eeprom Block:share_eep Backup Content CRC error, Use default value");
			desc = (uint8_t*)&eep_ram_image.share_eep_write_count;
			src  = (uint8_t*)&dflt_eep_val.share_eep_write_count;
			for( uint16 i = 0; i < EEP_BLOCK_share_eep_LEN-4; i ++ )
			{
				desc[i] = src[i];
			}
			eep_ram_image.share_eep_crc = Crc32_Calc((uint8_t*)(&eep_ram_image.share_eep_write_count),EEP_BLOCK_share_eep_LEN-4);
		}

		eep_write_block_req |= (1u << EEP_BLOCK_share_eep_BLOCK_ID);
	}

	src = (uint8_t*)&eep_ram_image.share_eep_write_count;
	desc= (uint8_t*)&eep_hw_image.share_eep_write_count;
	for( uint16 i = 0; i < EEP_BLOCK_share_eep_LEN; i ++ )
	{
		desc[i] = src[i];
	}
#else	//EEP_BLOCK_share_eep_IS_EEPROM
	uint32_t 	write_count = 0;
	uint16_t	dflash_block_avbl_idx=0;
	uint32_t	crc;
	for( uint16_t i = 0; i < EEP_BLOCK_share_eep_DFLASH_SECTOR_NUM*DFLASH_SECTOR_SIZE/EEP_BLOCK_share_eep_LEN; i ++ )
	{
		(void)HalDFlash_ReadData(EEP_BLOCK_share_eep_DFLASH_START_ADDR+i*EEP_BLOCK_share_eep_LEN,(uint8 *)(&eep_ram_image.share_eep_write_count),EEP_BLOCK_share_eep_LEN);
		crc = Crc32_Calc((uint8_t*)(&eep_ram_image.share_eep_write_count),EEP_BLOCK_share_eep_LEN-4);
		if ( crc == eep_ram_image.share_eep_crc )
		{	
			if ( write_count < eep_ram_image.share_eep_write_count )
			{
				write_count 			= 	eep_ram_image.share_eep_write_count;
				dflash_block_avbl_idx	=	i;
			}
		}
	}
	boolean dflash_restore_default = FALSE;
	if ( write_count )
	{
		dflash_share_eep_current_write_addr = EEP_BLOCK_share_eep_DFLASH_START_ADDR+dflash_block_avbl_idx*EEP_BLOCK_share_eep_LEN;
		(void)HalDFlash_ReadData(dflash_share_eep_current_write_addr,(uint8 *)(&eep_ram_image.share_eep_write_count),EEP_BLOCK_share_eep_LEN);
		/* check again */
		if(	eep_ram_image.share_eep_crc == Crc32_Calc((uint8_t*)(&eep_ram_image.share_eep_write_count),EEP_BLOCK_share_eep_LEN-4) )
		{	/* re-check ok, */
			uint8_t *src;
			uint8_t *desc;
			src = (uint8_t*)&eep_ram_image.share_eep_write_count;
			desc= (uint8_t*)&eep_hw_image.share_eep_write_count;
			for( uint16_t i = 0; i < EEP_BLOCK_share_eep_LEN; i ++ )
			{
				desc[i] = src[i];
			}
		}
		else
		{
			dflash_restore_default = TRUE;
		}
		
		dflash_share_eep_current_write_addr 	+= 	EEP_BLOCK_share_eep_LEN;	
		if ( dflash_share_eep_current_write_addr >= EEP_BLOCK_share_eep_DFLASH_END_ADDR )
		{
			dflash_share_eep_current_write_addr = EEP_BLOCK_share_eep_DFLASH_START_ADDR;
		}
	}
	else
	{
		dflash_restore_default = TRUE;
	}
	if ( dflash_restore_default )
	{
		uint8_t *src;
		uint8_t *desc;
		desc = (uint8_t*)&eep_ram_image.share_eep_write_count;
		src  = (uint8_t*)&dflt_eep_val.share_eep_write_count;
		for( uint16 i = 0; i < EEP_BLOCK_share_eep_LEN-4; i ++ )
		{
			desc[i] = src[i];
		}	
		eep_ram_image.share_eep_crc = Crc32_Calc((uint8_t*)(&eep_ram_image.share_eep_write_count),EEP_BLOCK_share_eep_LEN-4);
		src = (uint8_t*)&eep_ram_image.share_eep_write_count;
		desc= (uint8_t*)&eep_hw_image.share_eep_write_count;
		for( uint16_t i = 0; i < EEP_BLOCK_share_eep_LEN; i ++ )
		{
			desc[i] = src[i];
		}

		TRACE_TEXT(EEP_ERR,MOD_ALWAYS,"Eeprom Block:share_eep Dflash Error, Use default value");
		eep_write_block_req |= (1u << EEP_BLOCK_share_eep_BLOCK_ID);
		dflash_share_eep_current_write_addr = EEP_BLOCK_share_eep_DFLASH_START_ADDR;
	}
#endif	//EEP_BLOCK_share_eep_IS_EEPROM
	/*lint -restore */	
}

static void EepBlock_ResetInfo_Init(void)
{	
	/*lint -save -e831 -e662 -e661*/
#if EEP_BLOCK_ResetInfo_IS_EEPROM
	uint32 crc ;
	uint8_t *src;
	uint8_t *desc;

	(void)HalEep_Read(EEP_BLOCK_ResetInfo_NORMAL_ADDR,(uint8 *)(&eep_ram_image.ResetInfo_write_count),EEP_BLOCK_ResetInfo_LEN);
	crc = Crc32_Calc((uint8_t*)(&eep_ram_image.ResetInfo_write_count),EEP_BLOCK_ResetInfo_LEN-4);
	if ( crc != eep_ram_image.ResetInfo_crc )
	{	// use backup data
		TRACE_TEXT(EEP_ERR,MOD_ALWAYS,"Eeprom Block:ResetInfo Normal Content CRC error..");
		(void)HalEep_Read(EEP_BLOCK_ResetInfo_BACKUP_ADDR,(uint8 *)(&eep_ram_image.ResetInfo_write_count),EEP_BLOCK_ResetInfo_LEN);
		crc = Crc32_Calc((uint8_t*)(&eep_ram_image.ResetInfo_write_count),EEP_BLOCK_ResetInfo_LEN-4);
		if ( crc !=  eep_ram_image.ResetInfo_crc )
		{	// use default value
			TRACE_TEXT(EEP_ERR,MOD_ALWAYS,"Eeprom Block:ResetInfo Backup Content CRC error, Use default value");
			desc = (uint8_t*)&eep_ram_image.ResetInfo_write_count;
			src  = (uint8_t*)&dflt_eep_val.ResetInfo_write_count;
			for( uint16 i = 0; i < EEP_BLOCK_ResetInfo_LEN-4; i ++ )
			{
				desc[i] = src[i];
			}
			eep_ram_image.ResetInfo_crc = Crc32_Calc((uint8_t*)(&eep_ram_image.ResetInfo_write_count),EEP_BLOCK_ResetInfo_LEN-4);
		}

		eep_write_block_req |= (1u << EEP_BLOCK_ResetInfo_BLOCK_ID);
	}

	src = (uint8_t*)&eep_ram_image.ResetInfo_write_count;
	desc= (uint8_t*)&eep_hw_image.ResetInfo_write_count;
	for( uint16 i = 0; i < EEP_BLOCK_ResetInfo_LEN; i ++ )
	{
		desc[i] = src[i];
	}
#else	//EEP_BLOCK_ResetInfo_IS_EEPROM
	uint32_t 	write_count = 0;
	uint16_t	dflash_block_avbl_idx=0;
	uint32_t	crc;
	for( uint16_t i = 0; i < EEP_BLOCK_ResetInfo_DFLASH_SECTOR_NUM*DFLASH_SECTOR_SIZE/EEP_BLOCK_ResetInfo_LEN; i ++ )
	{
		(void)HalDFlash_ReadData(EEP_BLOCK_ResetInfo_DFLASH_START_ADDR+i*EEP_BLOCK_ResetInfo_LEN,(uint8 *)(&eep_ram_image.ResetInfo_write_count),EEP_BLOCK_ResetInfo_LEN);
		crc = Crc32_Calc((uint8_t*)(&eep_ram_image.ResetInfo_write_count),EEP_BLOCK_ResetInfo_LEN-4);
		if ( crc == eep_ram_image.ResetInfo_crc )
		{	
			if ( write_count < eep_ram_image.ResetInfo_write_count )
			{
				write_count 			= 	eep_ram_image.ResetInfo_write_count;
				dflash_block_avbl_idx	=	i;
			}
		}
	}
	boolean dflash_restore_default = FALSE;
	if ( write_count )
	{
		dflash_ResetInfo_current_write_addr = EEP_BLOCK_ResetInfo_DFLASH_START_ADDR+dflash_block_avbl_idx*EEP_BLOCK_ResetInfo_LEN;
		(void)HalDFlash_ReadData(dflash_ResetInfo_current_write_addr,(uint8 *)(&eep_ram_image.ResetInfo_write_count),EEP_BLOCK_ResetInfo_LEN);
		/* check again */
		if(	eep_ram_image.ResetInfo_crc == Crc32_Calc((uint8_t*)(&eep_ram_image.ResetInfo_write_count),EEP_BLOCK_ResetInfo_LEN-4) )
		{	/* re-check ok, */
			uint8_t *src;
			uint8_t *desc;
			src = (uint8_t*)&eep_ram_image.ResetInfo_write_count;
			desc= (uint8_t*)&eep_hw_image.ResetInfo_write_count;
			for( uint16_t i = 0; i < EEP_BLOCK_ResetInfo_LEN; i ++ )
			{
				desc[i] = src[i];
			}
		}
		else
		{
			dflash_restore_default = TRUE;
		}
		
		dflash_ResetInfo_current_write_addr 	+= 	EEP_BLOCK_ResetInfo_LEN;	
		if ( dflash_ResetInfo_current_write_addr >= EEP_BLOCK_ResetInfo_DFLASH_END_ADDR )
		{
			dflash_ResetInfo_current_write_addr = EEP_BLOCK_ResetInfo_DFLASH_START_ADDR;
		}
	}
	else
	{
		dflash_restore_default = TRUE;
	}
	if ( dflash_restore_default )
	{
		uint8_t *src;
		uint8_t *desc;
		desc = (uint8_t*)&eep_ram_image.ResetInfo_write_count;
		src  = (uint8_t*)&dflt_eep_val.ResetInfo_write_count;
		for( uint16 i = 0; i < EEP_BLOCK_ResetInfo_LEN-4; i ++ )
		{
			desc[i] = src[i];
		}	
		eep_ram_image.ResetInfo_crc = Crc32_Calc((uint8_t*)(&eep_ram_image.ResetInfo_write_count),EEP_BLOCK_ResetInfo_LEN-4);
		src = (uint8_t*)&eep_ram_image.ResetInfo_write_count;
		desc= (uint8_t*)&eep_hw_image.ResetInfo_write_count;
		for( uint16_t i = 0; i < EEP_BLOCK_ResetInfo_LEN; i ++ )
		{
			desc[i] = src[i];
		}

		TRACE_TEXT(EEP_ERR,MOD_ALWAYS,"Eeprom Block:ResetInfo Dflash Error, Use default value");
		eep_write_block_req |= (1u << EEP_BLOCK_ResetInfo_BLOCK_ID);
		dflash_ResetInfo_current_write_addr = EEP_BLOCK_ResetInfo_DFLASH_START_ADDR;
	}
#endif	//EEP_BLOCK_ResetInfo_IS_EEPROM
	/*lint -restore */	
}



/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static const EepCopyUserImageToHwImageFunc EepCopyUserImageToHwImageFuncTbl[EEP_CONTENT_LIST_NUMBER_MAX]=
{
	EepCopy_Manuf_Visteon_Part_Number_FromUserImageToHwImage,
	EepCopy_Manuf_EquippedPCB_VPN_Sub_FromUserImageToHwImage,
	EepCopy_Manuf_EquippedPCB_VPN_Main_FromUserImageToHwImage,
	EepCopy_Manuf_Product_Serial_Number_FromUserImageToHwImage,
	EepCopy_Manuf_EquippedPCB_Serial_Number_Sub_FromUserImageToHwImage,
	EepCopy_Manuf_EquippedPCB_Serial_Number_Main_FromUserImageToHwImage,
	EepCopy_NVM_Revision_FromUserImageToHwImage,
	EepCopy_Manuf_SMD_Date1_FromUserImageToHwImage,
	EepCopy_Manuf_SMD_Date2_FromUserImageToHwImage,
	EepCopy_Manuf_Assembly_Date_FromUserImageToHwImage,
	EepCopy_Traceability_Data_FromUserImageToHwImage,
	EepCopy_VehicleManufacturerSparePartNumber_FromUserImageToHwImage,
	EepCopy_VehicleManufacturerSparePartNumber_Assembly_FromUserImageToHwImage,
	EepCopy_Operational_Reference_FromUserImageToHwImage,
	EepCopy_Supplier_Number_FromUserImageToHwImage,
	EepCopy_ECU_Serial_Number_FromUserImageToHwImage,
	EepCopy_Manufacturing_Identification_Code_FromUserImageToHwImage,
	EepCopy_VDIAG_FromUserImageToHwImage,
	EepCopy_Config_EQ1_FromUserImageToHwImage,
	EepCopy_Vehicle_Type_FromUserImageToHwImage,
	EepCopy_UUID_FromUserImageToHwImage,
	EepCopy_NAVI_ID_FromUserImageToHwImage,
	EepCopy_DA_ID_FromUserImageToHwImage,
	EepCopy_DAMainBoardHwVer_FromUserImageToHwImage,
	EepCopy_SW_Version_Date_Format_FromUserImageToHwImage,
	EepCopy_SW_Edition_Version_FromUserImageToHwImage,
	EepCopy_VehicleManufacturerSparePartNumber_Nissan_FromUserImageToHwImage,
	EepCopy_VisteonProductPartNumber_FromUserImageToHwImage,
	EepCopy_EquippedPCBVisteonPartNumMainBorad_FromUserImageToHwImage,
	EepCopy_EquippedPCBVisteonPartNumSubBorad_FromUserImageToHwImage,
	EepCopy_DAUniqueID_FromUserImageToHwImage,
	EepCopy_ProductSerialNum_FromUserImageToHwImage,
	EepCopy_ProductSerialNumMainBoard_FromUserImageToHwImage,
	EepCopy_ProductSerialNumSubBoard_FromUserImageToHwImage,
	EepCopy_SMDManufacturingDate_FromUserImageToHwImage,
	EepCopy_AssemblyManufacturingDate_FromUserImageToHwImage,
	EepCopy_TraceabilityBytes_FromUserImageToHwImage,
	EepCopy_SMDPlantNum_FromUserImageToHwImage,
	EepCopy_AssemblyPlantNum_FromUserImageToHwImage,
	EepCopy_DEM_EVENT_CAN_COM_DATA_FromUserImageToHwImage,
	EepCopy_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_FromUserImageToHwImage,
	EepCopy_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_FromUserImageToHwImage,
	EepCopy_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_FromUserImageToHwImage,
	EepCopy_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_FromUserImageToHwImage,
	EepCopy_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_FromUserImageToHwImage,
	EepCopy_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_FromUserImageToHwImage,
	EepCopy_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_FromUserImageToHwImage,
	EepCopy_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_FromUserImageToHwImage,
	EepCopy_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_FromUserImageToHwImage,
	EepCopy_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_FromUserImageToHwImage,
	EepCopy_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_FromUserImageToHwImage,
	EepCopy_TachoUpValue_FromUserImageToHwImage,
	EepCopy_TachoLowValue_FromUserImageToHwImage,
	EepCopy_TachoLimitValue_FromUserImageToHwImage,
	EepCopy_TachoUDeltaLimit_FromUserImageToHwImage,
	EepCopy_TachoLDeltaLimit_FromUserImageToHwImage,
	EepCopy_TachoUpChangeValue_FromUserImageToHwImage,
	EepCopy_TachoLowChangeValue_FromUserImageToHwImage,
	EepCopy_TachoRun_FromUserImageToHwImage,
	EepCopy_Speed_Rm_1_FromUserImageToHwImage,
	EepCopy_Speed_Rm_2_FromUserImageToHwImage,
	EepCopy_Speed_Rc_FromUserImageToHwImage,
	EepCopy_FuelTelltaleOn_R_FromUserImageToHwImage,
	EepCopy_FuelTelltaleOff_R_FromUserImageToHwImage,
	EepCopy_FuelWarningOnPoint_R_FromUserImageToHwImage,
	EepCopy_Fuel_R_Open_FromUserImageToHwImage,
	EepCopy_Fuel_R_Short_FromUserImageToHwImage,
	EepCopy_PkbWarningJudgeV1_FromUserImageToHwImage,
	EepCopy_PkbWarningJudgeV2_FromUserImageToHwImage,
	EepCopy_AudioEepTest_FromUserImageToHwImage,
	EepCopy_DspKeyCodeOnOff_FromUserImageToHwImage,
	EepCopy_DspKeyCode_FromUserImageToHwImage,
	EepCopy_BatState_ChgDly_FromUserImageToHwImage,
	EepCopy_BatVolVeryHigh_Hysteresis_H_FromUserImageToHwImage,
	EepCopy_BatVolVeryHigh_Hysteresis_L_FromUserImageToHwImage,
	EepCopy_BatVolHigh_Hysteresis_H_FromUserImageToHwImage,
	EepCopy_BatVolHigh_Hysteresis_L_FromUserImageToHwImage,
	EepCopy_BatVolLow_Hysteresis_H_FromUserImageToHwImage,
	EepCopy_BatVolLow_Hysteresis_L_FromUserImageToHwImage,
	EepCopy_BatVolVeryLow_Hysteresis_H_FromUserImageToHwImage,
	EepCopy_BatVolVeryLow_Hysteresis_L_FromUserImageToHwImage,
	EepCopy_TempState_ChgDly_FromUserImageToHwImage,
	EepCopy_TempDegC_Low_Hysteresis_L_FromUserImageToHwImage,
	EepCopy_TempDegC_Low_Hysteresis_H_FromUserImageToHwImage,
	EepCopy_TempDegC_High_Hysteresis_L_FromUserImageToHwImage,
	EepCopy_TempDegC_High_Hysteresis_H_FromUserImageToHwImage,
	EepCopy_AmpHighTempProction_FromUserImageToHwImage,
	EepCopy_CanNm_DA_S1_Delay_ms_FromUserImageToHwImage,
	EepCopy_Vin_FromUserImageToHwImage,
	EepCopy_AutoSyncTimeWithGps_FromUserImageToHwImage,
	EepCopy_ScreenBackLightValOnDay_FromUserImageToHwImage,
	EepCopy_ScreenBackLightValOnNight_FromUserImageToHwImage,
	EepCopy_HistoryAverFuelCons_FromUserImageToHwImage,
	EepCopy_FuelResistance_FromUserImageToHwImage,
	EepCopy_dtc_example_FromUserImageToHwImage,
	EepCopy_TotalOdo_FromUserImageToHwImage,
	EepCopy_TotalTime_FromUserImageToHwImage,
	EepCopy_TravelTime_FromUserImageToHwImage,
	EepCopy_TravelOdo_FromUserImageToHwImage,
	EepCopy_TripAMeter_FromUserImageToHwImage,
	EepCopy_TripATime_FromUserImageToHwImage,
	EepCopy_TripBMeter_FromUserImageToHwImage,
	EepCopy_TripBTime_FromUserImageToHwImage,
	EepCopy_CruiseDistance_FromUserImageToHwImage,
	EepCopy_VipSwdlShareMem_FromUserImageToHwImage,
	EepCopy_TotalImageSFailCnt_FromUserImageToHwImage,
	EepCopy_trace_phy_en_FromUserImageToHwImage,
	EepCopy_trace_phy_FromUserImageToHwImage,
	EepCopy_VipLogModLvl_FromUserImageToHwImage,
	EepCopy_TraceModuleLvlCrc_FromUserImageToHwImage,
	EepCopy_TotalReset_Cnt_FromUserImageToHwImage,
	EepCopy_DspPOR_cnt_FromUserImageToHwImage,
	EepCopy_AudioCmdExecMaxTm_FromUserImageToHwImage,
	EepCopy_RadioCmdExecMaxTm_FromUserImageToHwImage,
	EepCopy_CpuDisIntMaxNest_FromUserImageToHwImage,
	EepCopy_CpuDisIntMaxTm_FromUserImageToHwImage,
	EepCopy_CpuDisIntTooLongCnt_FromUserImageToHwImage,
	EepCopy_SocResetReason_FromUserImageToHwImage,
};

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void EepMana_Init(void)
{
	process_block = 0xFF;

	for(uint16_t i = 0; i < EEP_CONT_LIST_FLAG_ARRAY_NUM; i ++ )
	{
		wr_eep_by_list_req[i] 		= 0x00;
		// tx_eep_by_list_to_imx_req[i]= 0x00;
	}

	EepBlock_manufacture_Init();
	EepBlock_development_nvm_Init();
	EepBlock_OemSetting_Init();
	EepBlock_user_setting_Init();
	EepBlock_DTC_INFO_Init();
	EepBlock_OdoInfo_Init();
	EepBlock_share_eep_Init();
	EepBlock_ResetInfo_Init();

	/*>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> eeprom restore to default value if it is a const varible 						*/
	Proc_Manuf_Visteon_Part_Number_RestoreToDefault();
	Proc_Manuf_EquippedPCB_VPN_Sub_RestoreToDefault();
	Proc_Manuf_EquippedPCB_VPN_Main_RestoreToDefault();
	Proc_Manuf_Product_Serial_Number_RestoreToDefault();
	Proc_Manuf_EquippedPCB_Serial_Number_Sub_RestoreToDefault();
	Proc_Manuf_EquippedPCB_Serial_Number_Main_RestoreToDefault();
	Proc_NVM_Revision_RestoreToDefault();
	Proc_Manuf_SMD_Date1_RestoreToDefault();
	Proc_Manuf_SMD_Date2_RestoreToDefault();
	Proc_Manuf_Assembly_Date_RestoreToDefault();
	Proc_Traceability_Data_RestoreToDefault();
	Proc_VehicleManufacturerSparePartNumber_RestoreToDefault();
	Proc_VehicleManufacturerSparePartNumber_Assembly_RestoreToDefault();
	Proc_Operational_Reference_RestoreToDefault();
	Proc_Supplier_Number_RestoreToDefault();
	Proc_ECU_Serial_Number_RestoreToDefault();
	Proc_Manufacturing_Identification_Code_RestoreToDefault();
	Proc_VDIAG_RestoreToDefault();
	Proc_Config_EQ1_RestoreToDefault();
	Proc_Vehicle_Type_RestoreToDefault();
	Proc_UUID_RestoreToDefault();
	Proc_NAVI_ID_RestoreToDefault();
	Proc_DA_ID_RestoreToDefault();
	Proc_DAMainBoardHwVer_RestoreToDefault();
	Proc_SW_Version_Date_Format_RestoreToDefault();
	Proc_SW_Edition_Version_RestoreToDefault();
	Proc_VehicleManufacturerSparePartNumber_Nissan_RestoreToDefault();
	Proc_VisteonProductPartNumber_RestoreToDefault();
	Proc_EquippedPCBVisteonPartNumMainBorad_RestoreToDefault();
	Proc_EquippedPCBVisteonPartNumSubBorad_RestoreToDefault();
	Proc_DAUniqueID_RestoreToDefault();
	Proc_ProductSerialNum_RestoreToDefault();
	Proc_ProductSerialNumMainBoard_RestoreToDefault();
	Proc_ProductSerialNumSubBoard_RestoreToDefault();
	Proc_SMDManufacturingDate_RestoreToDefault();
	Proc_AssemblyManufacturingDate_RestoreToDefault();
	Proc_TraceabilityBytes_RestoreToDefault();
	Proc_SMDPlantNum_RestoreToDefault();
	Proc_AssemblyPlantNum_RestoreToDefault();
	Proc_DEM_EVENT_CAN_COM_DATA_RestoreToDefault();
	Proc_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_RestoreToDefault();
	Proc_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_RestoreToDefault();
	Proc_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_RestoreToDefault();
	Proc_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_RestoreToDefault();
	Proc_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_RestoreToDefault();
	Proc_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_RestoreToDefault();
	Proc_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_RestoreToDefault();
	Proc_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_RestoreToDefault();
	Proc_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_RestoreToDefault();
	Proc_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_RestoreToDefault();
	Proc_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_RestoreToDefault();
	Proc_TachoUpValue_RestoreToDefault();
	Proc_TachoLowValue_RestoreToDefault();
	Proc_TachoLimitValue_RestoreToDefault();
	Proc_TachoUDeltaLimit_RestoreToDefault();
	Proc_TachoLDeltaLimit_RestoreToDefault();
	Proc_TachoUpChangeValue_RestoreToDefault();
	Proc_TachoLowChangeValue_RestoreToDefault();
	Proc_TachoRun_RestoreToDefault();
	Proc_Speed_Rm_1_RestoreToDefault();
	Proc_Speed_Rm_2_RestoreToDefault();
	Proc_Speed_Rc_RestoreToDefault();
	Proc_FuelTelltaleOn_R_RestoreToDefault();
	Proc_FuelTelltaleOff_R_RestoreToDefault();
	Proc_FuelWarningOnPoint_R_RestoreToDefault();
	Proc_Fuel_R_Open_RestoreToDefault();
	Proc_Fuel_R_Short_RestoreToDefault();
	Proc_PkbWarningJudgeV1_RestoreToDefault();
	Proc_PkbWarningJudgeV2_RestoreToDefault();
	Proc_AudioEepTest_RestoreToDefault();
	Proc_DspKeyCodeOnOff_RestoreToDefault();
	Proc_DspKeyCode_RestoreToDefault();
	Proc_BatState_ChgDly_RestoreToDefault();
	Proc_BatVolVeryHigh_Hysteresis_H_RestoreToDefault();
	Proc_BatVolVeryHigh_Hysteresis_L_RestoreToDefault();
	Proc_BatVolHigh_Hysteresis_H_RestoreToDefault();
	Proc_BatVolHigh_Hysteresis_L_RestoreToDefault();
	Proc_BatVolLow_Hysteresis_H_RestoreToDefault();
	Proc_BatVolLow_Hysteresis_L_RestoreToDefault();
	Proc_BatVolVeryLow_Hysteresis_H_RestoreToDefault();
	Proc_BatVolVeryLow_Hysteresis_L_RestoreToDefault();
	Proc_TempState_ChgDly_RestoreToDefault();
	Proc_TempDegC_Low_Hysteresis_L_RestoreToDefault();
	Proc_TempDegC_Low_Hysteresis_H_RestoreToDefault();
	Proc_TempDegC_High_Hysteresis_L_RestoreToDefault();
	Proc_TempDegC_High_Hysteresis_H_RestoreToDefault();
	Proc_AmpHighTempProction_RestoreToDefault();
	Proc_CanNm_DA_S1_Delay_ms_RestoreToDefault();
	Proc_Vin_RestoreToDefault();
	Proc_AutoSyncTimeWithGps_RestoreToDefault();
	Proc_ScreenBackLightValOnDay_RestoreToDefault();
	Proc_ScreenBackLightValOnNight_RestoreToDefault();
	Proc_HistoryAverFuelCons_RestoreToDefault();
	Proc_FuelResistance_RestoreToDefault();
	Proc_dtc_example_RestoreToDefault();
	Proc_TotalOdo_RestoreToDefault();
	Proc_TotalTime_RestoreToDefault();
	Proc_TravelTime_RestoreToDefault();
	Proc_TravelOdo_RestoreToDefault();
	Proc_TripAMeter_RestoreToDefault();
	Proc_TripATime_RestoreToDefault();
	Proc_TripBMeter_RestoreToDefault();
	Proc_TripBTime_RestoreToDefault();
	Proc_CruiseDistance_RestoreToDefault();
	Proc_VipSwdlShareMem_RestoreToDefault();
	Proc_TotalImageSFailCnt_RestoreToDefault();
	Proc_trace_phy_en_RestoreToDefault();
	Proc_trace_phy_RestoreToDefault();
	Proc_VipLogModLvl_RestoreToDefault();
	Proc_TraceModuleLvlCrc_RestoreToDefault();
	Proc_TotalReset_Cnt_RestoreToDefault();
	Proc_DspPOR_cnt_RestoreToDefault();
	Proc_AudioCmdExecMaxTm_RestoreToDefault();
	Proc_RadioCmdExecMaxTm_RestoreToDefault();
	Proc_CpuDisIntMaxNest_RestoreToDefault();
	Proc_CpuDisIntMaxTm_RestoreToDefault();
	Proc_CpuDisIntTooLongCnt_RestoreToDefault();
	Proc_SocResetReason_RestoreToDefault();

	/*>>>> Set initial complete flag  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>*/
	eep_init_comp = TRUE;
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
boolean EepMana_InitedComp(void)
{
	return eep_init_comp;
}
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void EepMana_Sync(void)
{
	//uint16 idx = 0;
	uint8_t cnt = 0;
	/* calc crc */
	CPU_SR_ALLOC();
	CPU_CRITICAL_ENTER();
	do
	{
		EepWrProcess();
		if ( process_block != 0xFF ){
			cnt = 0x00;
		}else{
			cnt ++;
		}
	}while( cnt < 5 );
	CPU_CRITICAL_EXIT();
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void EepMana_IdleTask(void)
{
	if ( eep_write_enable )
	{
		if ( BatMana_GetBatState() == BAT_NORMAL )
		{
			EepWrProcess();	// eeprom write process
		}
		// if ( eep_last_client )
		// {
		// 	EepIpcTxProcess();
		// }		
	}
	else
	{
		if ( HalOS_GetKernelTick() > 500 )
		{
			eep_write_enable = TRUE;
		}
	}
}



/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void EepWrProcess(void)
{
	/* write eeprom process */
	if ( process_block == 0xFF )
	{	// eeprom is idle 
		EepCopyRamImageToEepImage();
	}
	else if ( process_block < EEP_BLOCK_NUMBER_MAX ) 
	{	// write to real eeprom now 
		EepCopyEepImageToHw();
	}
	else
	{
		process_block = 0xFF;
	}
}


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void EepCopyRamImageToEepImage(void)
{
	for ( uint16 i = 0; i < EEP_CONTENT_LIST_NUMBER_MAX; i ++ )
	{
		CPU_SR_ALLOC();
		CPU_CRITICAL_ENTER();

		if ( wr_eep_by_list_req[i/32] & (1u<<(i&0x1F)) )
		{
			wr_eep_by_list_req[i/32] &= ~(1u<<(i&0x1F));

			EepCopyUserImageToHwImageFuncTbl[i]();
			
		}
		
		CPU_CRITICAL_EXIT();
	}
#if 0	
	/*check write eeprom by offset */
	for ( uint16 i = 0; i < OFFSET_FIELD_NUM; i ++ )
	{
		CPU_SR_ALLOC();
		CPU_CRITICAL_ENTER();

		if ( write_eep_offset_req[i/32] & (1u<<(i&0x1F)) )
		{
			write_eep_offset_req[i/32] &= ~(1u<<(i&0x1F));

			/* copy data now */
			uint16 offset =  (uint16)(i*TX_EEP_OFFSET_SIZE);
			
			for ( uint8 j = 1; j < EEP_BLOCK_NUMBER_MAX+1; j ++ )
			{ // need modify, offset -> write block 
				if ( offset <= eep_block_offset_info[j] )
				{
					eep_write_block_req |= (1u<<(j-1));	/*lint !e734*/
					break;
				}
			}
			
			uint16_t cp_len = TX_EEP_OFFSET_SIZE;
			for(uint8 j = 0; j < EEP_BLOCK_NUMBER_MAX; j ++)
			{
				if ( offset == eep_block_offset_info[j] )
				{
					offset += 4;
					cp_len -= 4;
					break;
				}
			}
			
			for ( uint16 j = 0; j < cp_len && offset < EEP_SIZE; j ++ )
			{	// copy to eeprom hw image 
				((uint8_t*)&eep_hw_image)[offset] = ((uint8_t*)&eep_ram_image)[offset];
				//*((uint8*)(&eeprom_image)+offset) = *((uint8*)(&eep_ram_image)+offset);
				offset ++;
			}	
		}

		CPU_CRITICAL_EXIT();
	}
#endif
	
	if ( eep_write_block_req )
	{
		for( uint8 i = 0; i < EEP_BLOCK_NUMBER_MAX; i ++ )
		{
			if ( eep_write_block_req & (1u<<i) )
			{
				eep_write_block_req &= ~(1u<<i);

				process_block = i;	// set process flag
				wr_eep_index  = 0;
				wr_eep_back   = FALSE;
				wr_hw_try_cnt = 0x00;
				
				switch(i)
				{
				case EEP_BLOCK_manufacture_BLOCK_ID:
					eep_hw_image.manufacture_write_count ++;
					TRACE_VALUE(EEP_WRITE_BLOCK,MOD_EEPROM,"Write Eep Block:manufacture require, write count=%d",eep_hw_image.manufacture_write_count);
					eep_hw_image.manufacture_crc = Crc32_Calc((uint8_t const *)&eep_hw_image.manufacture_write_count,EEP_BLOCK_manufacture_LEN-4);
					break;
				case EEP_BLOCK_development_nvm_BLOCK_ID:
					eep_hw_image.development_nvm_write_count ++;
					TRACE_VALUE(EEP_WRITE_BLOCK,MOD_EEPROM,"Write Eep Block:development_nvm require, write count=%d",eep_hw_image.development_nvm_write_count);
					eep_hw_image.development_nvm_crc = Crc32_Calc((uint8_t const *)&eep_hw_image.development_nvm_write_count,EEP_BLOCK_development_nvm_LEN-4);
					break;
				case EEP_BLOCK_OemSetting_BLOCK_ID:
					eep_hw_image.OemSetting_write_count ++;
					TRACE_VALUE(EEP_WRITE_BLOCK,MOD_EEPROM,"Write Eep Block:OemSetting require, write count=%d",eep_hw_image.OemSetting_write_count);
					eep_hw_image.OemSetting_crc = Crc32_Calc((uint8_t const *)&eep_hw_image.OemSetting_write_count,EEP_BLOCK_OemSetting_LEN-4);
					break;
				case EEP_BLOCK_user_setting_BLOCK_ID:
					eep_hw_image.user_setting_write_count ++;
					TRACE_VALUE(EEP_WRITE_BLOCK,MOD_EEPROM,"Write Eep Block:user_setting require, write count=%d",eep_hw_image.user_setting_write_count);
					eep_hw_image.user_setting_crc = Crc32_Calc((uint8_t const *)&eep_hw_image.user_setting_write_count,EEP_BLOCK_user_setting_LEN-4);
					break;
				case EEP_BLOCK_DTC_INFO_BLOCK_ID:
					eep_hw_image.DTC_INFO_write_count ++;
					TRACE_VALUE(EEP_WRITE_BLOCK,MOD_EEPROM,"Write Eep Block:DTC_INFO require, write count=%d",eep_hw_image.DTC_INFO_write_count);
					eep_hw_image.DTC_INFO_crc = Crc32_Calc((uint8_t const *)&eep_hw_image.DTC_INFO_write_count,EEP_BLOCK_DTC_INFO_LEN-4);
					break;
				case EEP_BLOCK_OdoInfo_BLOCK_ID:
					eep_hw_image.OdoInfo_write_count ++;
					TRACE_VALUE(EEP_WRITE_BLOCK,MOD_EEPROM,"Write Eep Block:OdoInfo require, write count=%d",eep_hw_image.OdoInfo_write_count);
					eep_hw_image.OdoInfo_crc = Crc32_Calc((uint8_t const *)&eep_hw_image.OdoInfo_write_count,EEP_BLOCK_OdoInfo_LEN-4);
					break;
				case EEP_BLOCK_share_eep_BLOCK_ID:
					eep_hw_image.share_eep_write_count ++;
					TRACE_VALUE(EEP_WRITE_BLOCK,MOD_EEPROM,"Write Eep Block:share_eep require, write count=%d",eep_hw_image.share_eep_write_count);
					eep_hw_image.share_eep_crc = Crc32_Calc((uint8_t const *)&eep_hw_image.share_eep_write_count,EEP_BLOCK_share_eep_LEN-4);
					break;
				case EEP_BLOCK_ResetInfo_BLOCK_ID:
					eep_hw_image.ResetInfo_write_count ++;
					TRACE_VALUE(EEP_WRITE_BLOCK,MOD_EEPROM,"Write Eep Block:ResetInfo require, write count=%d",eep_hw_image.ResetInfo_write_count);
					eep_hw_image.ResetInfo_crc = Crc32_Calc((uint8_t const *)&eep_hw_image.ResetInfo_write_count,EEP_BLOCK_ResetInfo_LEN-4);
					break;
	
				default:
					break;
				}				
				//EEP_BLOCK_INFO const* eep_info 	= eep_block_info	+	i;
				//uint8 *	eep 				 	= eep_info->eep_back;
				// increase write cnt;

				//uint32 *wr_cnt = (void*)eep;
				//*wr_cnt += 1;
				//*eep_info->eep_back_crc = Crc32_Calc(eep,eep_info->len-4);
	
				break;
			}
		}
	}	
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void EepCopyEepImageToHw(void)
{
	switch(process_block)
	{
	case EEP_BLOCK_manufacture_BLOCK_ID:
#if EEP_BLOCK_manufacture_IS_EEPROM
		if ( !HalEep_IsBusy() )
		{
			if ( wr_eep_index < EEP_BLOCK_manufacture_LEN )
			{
				uint8 	eep_hw[EEP_PROGRAM_SIZE];
				boolean error = FALSE;
				
				uint16 hw_addr;
				if ( wr_eep_back ){
					hw_addr = (uint16)(wr_eep_index+EEP_BLOCK_manufacture_BACKUP_ADDR);
				}else{
					hw_addr = (uint16)(wr_eep_index+EEP_BLOCK_manufacture_NORMAL_ADDR);
				}
				
				if ( EEP_PROGRAM_SIZE == HalEep_Read(hw_addr, eep_hw, EEP_PROGRAM_SIZE) )
				{	// read ok
					uint8 * eep_image 	= (uint8_t*)&eep_hw_image.manufacture_write_count + wr_eep_index;					
					for ( uint16 i = 0; i < EEP_PROGRAM_SIZE; i ++ )
					{	
						if ( eep_image[i] != eep_hw[i] )
						{	
							if ( EEP_PROGRAM_SIZE != HalEep_Write(hw_addr,eep_image, EEP_PROGRAM_SIZE) )
							{
								error = TRUE;
								TRACE_VALUE(EEP_ERR,MOD_EEPROM,"HalEep_Write Block:manufacture Error, retry count=%d",wr_hw_try_cnt);
							}	
							break;
						}
					}
				}
				else
				{
					error = TRUE;
					TRACE_VALUE(EEP_ERR,MOD_EEPROM,"HalEep_Read Block:manufacture Error, retry count=%d",wr_hw_try_cnt);
				}
				
				if ( error && wr_hw_try_cnt < 33 )
				{
					wr_eep_index ++;
				}
				else
				{
					wr_eep_index 	+=  EEP_PROGRAM_SIZE;
					wr_hw_try_cnt	= 	0x00;
				}
			}
			else if ( !wr_eep_back )
			{
				wr_eep_back  = 	TRUE;
				wr_eep_index =	0x00;
			}
			else
			{	// process success
				process_block = 0xFF;
				TRACE_VALUE(EEP_WRITE_BLOCK,MOD_EEPROM,"Write Eep Block:manufacture to hw complete, write count=%d",eep_hw_image.manufacture_write_count);
			}
		}	
#else	// !EEP_BLOCK_manufacture_IS_EEPROM
		if ( !HalDFlash_IsBusy() )
		{
			if ( wr_eep_index < EEP_BLOCK_manufacture_LEN )
			{
				boolean dflash_is_blank = TRUE;
				if ( wr_eep_index == 0x00 )
				{	/* check if need erase dflash sector */
					if ( (dflash_manufacture_current_write_addr - EEP_BLOCK_manufacture_DFLASH_START_ADDR) % DFLASH_SECTOR_SIZE == 0x00 )
					{	// new sector start, need erase 
						uint32 temp;
						if ( HalDFlash_ReadData(dflash_manufacture_current_write_addr, (uint8*)&temp, 4) != 4 )
						{	// this sector is error, change to next sector
							wr_eep_index				= 	0x00;
							wr_hw_try_cnt++;
							dflash_is_blank 			= 	FALSE;
							
							dflash_manufacture_current_write_addr 	+= 	DFLASH_SECTOR_SIZE;	
							if ( dflash_manufacture_current_write_addr >= EEP_BLOCK_manufacture_DFLASH_END_ADDR )
							{
								dflash_manufacture_current_write_addr = EEP_BLOCK_manufacture_DFLASH_START_ADDR;
							}
						}
						else if ( temp != 0xFFFFFFFF )
						{
							(void)HalDFlash_Erase(dflash_manufacture_current_write_addr, 0);	//len is not used
							dflash_is_blank = FALSE;
							TRACE_VALUE(EEP_ERR,MOD_EEPROM,"Dflash Erase Block: manufacture ,Address=0x%08x",dflash_manufacture_current_write_addr+wr_eep_back);
						}
						else{}/* sector is blank*/
					}
				}

				if ( wr_hw_try_cnt < 33 )
				{
					if ( dflash_is_blank )
					{
						boolean last_prog_err = FALSE;
						if ( wr_eep_index )
						{	/* check last program */
							uint8 eep_hw[DFLASH_PROGRAM_SIZE];
							if ( HalDFlash_ReadData(dflash_manufacture_current_write_addr+wr_eep_back, eep_hw, DFLASH_PROGRAM_SIZE) == DFLASH_PROGRAM_SIZE )
							{
								for ( uint8 i = 0; i < DFLASH_PROGRAM_SIZE; i ++ )
								{
									if ( eep_hw[i] != ((uint8_t*)&eep_hw_image.manufacture_write_count+wr_eep_back)[i] )
									{
										last_prog_err = TRUE;
										TRACE_VALUE(EEP_ERR,MOD_EEPROM,"Dflash Program Block: manufacture Error,Address=0x%08x",dflash_manufacture_current_write_addr+wr_eep_back);
										break;
									}
								}
							}
							else
							{	// this block read error, use next block
								last_prog_err = TRUE;
								TRACE_VALUE(EEP_ERR,MOD_EEPROM,"Dflash Program Block: manufacture Read error,Address=0x%08x",(dflash_manufacture_current_write_addr+wr_eep_back));
							}
						}
						if ( !last_prog_err )
						{
							if ( DFLASH_PROGRAM_SIZE == HalDFlash_ProgramData(dflash_manufacture_current_write_addr+wr_eep_index, (uint8_t*)&eep_hw_image.manufacture_write_count+wr_eep_index, DFLASH_PROGRAM_SIZE) )
							{
								wr_eep_back 	= 	wr_eep_index;	// for read compare 
								wr_eep_index 	+= 	DFLASH_PROGRAM_SIZE;
							}
							else
							{
								last_prog_err = TRUE;
								TRACE_VALUE(EEP_ERR,MOD_EEPROM,"Dflash Program Block: manufacture, HalDFlash_ProgramData error,Address=0x%08x",(dflash_manufacture_current_write_addr+wr_eep_back));
							}
						}
						if(last_prog_err)
						{	// error, this block is error, use next sector 
							wr_hw_try_cnt ++;
							wr_eep_index = 0x00;
							wr_eep_back  = 0x00;
							
							dflash_manufacture_current_write_addr 	+= 	EEP_BLOCK_manufacture_LEN;	
							if ( dflash_manufacture_current_write_addr >= EEP_BLOCK_manufacture_DFLASH_END_ADDR )
							{
								dflash_manufacture_current_write_addr = EEP_BLOCK_manufacture_DFLASH_START_ADDR;
							}
						}
					}
				}
				
				if ( wr_hw_try_cnt >= 33 )
				{	// error too much, delete this write require
					process_block = 0xFF;
					TRACE_VALUE(EEP_ERR,MOD_EEPROM,"Dflash Program Block: manufacture,addr=%08x error too much",dflash_manufacture_current_write_addr);
				}
			}
			else if ( wr_eep_index != wr_eep_back )
			{	// check this block last write 
				boolean last_prog_err = FALSE;
				uint8 	eep_hw[DFLASH_PROGRAM_SIZE];
				if ( HalDFlash_ReadData(dflash_manufacture_current_write_addr+wr_eep_back, eep_hw, DFLASH_PROGRAM_SIZE) == DFLASH_PROGRAM_SIZE )
				{
					for ( uint8 i = 0; i < DFLASH_PROGRAM_SIZE; i ++ )
					{
						if ( eep_hw[i] != ((uint8_t*)&eep_hw_image.manufacture_write_count+wr_eep_back)[i] )
						{
							last_prog_err = TRUE;
							TRACE_VALUE(EEP_ERR,MOD_EEPROM,"Dflash Program Block: manufacture, HalDFlash_ProgramData error,Address(last write)=0x%08x",(dflash_manufacture_current_write_addr+wr_eep_back));
							break;
						}
					}
				}
				else
				{	// this block read error, use next block
					last_prog_err = TRUE;
					TRACE_VALUE(EEP_ERR,MOD_EEPROM,"Dflash Program Block: manufacture Read(last read) error,Address=0x%08x",(dflash_manufacture_current_write_addr+wr_eep_back));
				}
				
				wr_eep_index = 0x00;
				wr_eep_back  = 0x00;
				dflash_manufacture_current_write_addr 	+= 	EEP_BLOCK_manufacture_LEN;	
				if ( dflash_manufacture_current_write_addr >= EEP_BLOCK_manufacture_DFLASH_END_ADDR )
				{
					dflash_manufacture_current_write_addr = EEP_BLOCK_manufacture_DFLASH_START_ADDR;
				}
					
				if ( last_prog_err )
				{
					wr_hw_try_cnt ++;
					if ( wr_hw_try_cnt >= 33 )
					{	// error too much, delete this write require
						process_block 	= 0xFF;		
					}
				}
				else
				{	// last program ok 
					process_block 	= 0xFF;	
					TRACE_VALUE(EEP_ERR,MOD_EEPROM,"Dflash Program Block: manufacture Complete, crc=0x%08x",eep_hw_image.manufacture_crc);			
				}
			}
			else
			{	// process success
				process_block = 0xFF;
			}
		}
#endif		// {EEP_BLOCK_manufacture_IS_EEPROM}
		break;
	case EEP_BLOCK_development_nvm_BLOCK_ID:
#if EEP_BLOCK_development_nvm_IS_EEPROM
		if ( !HalEep_IsBusy() )
		{
			if ( wr_eep_index < EEP_BLOCK_development_nvm_LEN )
			{
				uint8 	eep_hw[EEP_PROGRAM_SIZE];
				boolean error = FALSE;
				
				uint16 hw_addr;
				if ( wr_eep_back ){
					hw_addr = (uint16)(wr_eep_index+EEP_BLOCK_development_nvm_BACKUP_ADDR);
				}else{
					hw_addr = (uint16)(wr_eep_index+EEP_BLOCK_development_nvm_NORMAL_ADDR);
				}
				
				if ( EEP_PROGRAM_SIZE == HalEep_Read(hw_addr, eep_hw, EEP_PROGRAM_SIZE) )
				{	// read ok
					uint8 * eep_image 	= (uint8_t*)&eep_hw_image.development_nvm_write_count + wr_eep_index;					
					for ( uint16 i = 0; i < EEP_PROGRAM_SIZE; i ++ )
					{	
						if ( eep_image[i] != eep_hw[i] )
						{	
							if ( EEP_PROGRAM_SIZE != HalEep_Write(hw_addr,eep_image, EEP_PROGRAM_SIZE) )
							{
								error = TRUE;
								TRACE_VALUE(EEP_ERR,MOD_EEPROM,"HalEep_Write Block:development_nvm Error, retry count=%d",wr_hw_try_cnt);
							}	
							break;
						}
					}
				}
				else
				{
					error = TRUE;
					TRACE_VALUE(EEP_ERR,MOD_EEPROM,"HalEep_Read Block:development_nvm Error, retry count=%d",wr_hw_try_cnt);
				}
				
				if ( error && wr_hw_try_cnt < 33 )
				{
					wr_eep_index ++;
				}
				else
				{
					wr_eep_index 	+=  EEP_PROGRAM_SIZE;
					wr_hw_try_cnt	= 	0x00;
				}
			}
			else if ( !wr_eep_back )
			{
				wr_eep_back  = 	TRUE;
				wr_eep_index =	0x00;
			}
			else
			{	// process success
				process_block = 0xFF;
				TRACE_VALUE(EEP_WRITE_BLOCK,MOD_EEPROM,"Write Eep Block:development_nvm to hw complete, write count=%d",eep_hw_image.development_nvm_write_count);
			}
		}	
#else	// !EEP_BLOCK_development_nvm_IS_EEPROM
		if ( !HalDFlash_IsBusy() )
		{
			if ( wr_eep_index < EEP_BLOCK_development_nvm_LEN )
			{
				boolean dflash_is_blank = TRUE;
				if ( wr_eep_index == 0x00 )
				{	/* check if need erase dflash sector */
					if ( (dflash_development_nvm_current_write_addr - EEP_BLOCK_development_nvm_DFLASH_START_ADDR) % DFLASH_SECTOR_SIZE == 0x00 )
					{	// new sector start, need erase 
						uint32 temp;
						if ( HalDFlash_ReadData(dflash_development_nvm_current_write_addr, (uint8*)&temp, 4) != 4 )
						{	// this sector is error, change to next sector
							wr_eep_index				= 	0x00;
							wr_hw_try_cnt++;
							dflash_is_blank 			= 	FALSE;
							
							dflash_development_nvm_current_write_addr 	+= 	DFLASH_SECTOR_SIZE;	
							if ( dflash_development_nvm_current_write_addr >= EEP_BLOCK_development_nvm_DFLASH_END_ADDR )
							{
								dflash_development_nvm_current_write_addr = EEP_BLOCK_development_nvm_DFLASH_START_ADDR;
							}
						}
						else if ( temp != 0xFFFFFFFF )
						{
							(void)HalDFlash_Erase(dflash_development_nvm_current_write_addr, 0);	//len is not used
							dflash_is_blank = FALSE;
							TRACE_VALUE(EEP_ERR,MOD_EEPROM,"Dflash Erase Block: development_nvm ,Address=0x%08x",dflash_development_nvm_current_write_addr+wr_eep_back);
						}
						else{}/* sector is blank*/
					}
				}

				if ( wr_hw_try_cnt < 33 )
				{
					if ( dflash_is_blank )
					{
						boolean last_prog_err = FALSE;
						if ( wr_eep_index )
						{	/* check last program */
							uint8 eep_hw[DFLASH_PROGRAM_SIZE];
							if ( HalDFlash_ReadData(dflash_development_nvm_current_write_addr+wr_eep_back, eep_hw, DFLASH_PROGRAM_SIZE) == DFLASH_PROGRAM_SIZE )
							{
								for ( uint8 i = 0; i < DFLASH_PROGRAM_SIZE; i ++ )
								{
									if ( eep_hw[i] != ((uint8_t*)&eep_hw_image.development_nvm_write_count+wr_eep_back)[i] )
									{
										last_prog_err = TRUE;
										TRACE_VALUE(EEP_ERR,MOD_EEPROM,"Dflash Program Block: development_nvm Error,Address=0x%08x",dflash_development_nvm_current_write_addr+wr_eep_back);
										break;
									}
								}
							}
							else
							{	// this block read error, use next block
								last_prog_err = TRUE;
								TRACE_VALUE(EEP_ERR,MOD_EEPROM,"Dflash Program Block: development_nvm Read error,Address=0x%08x",(dflash_development_nvm_current_write_addr+wr_eep_back));
							}
						}
						if ( !last_prog_err )
						{
							if ( DFLASH_PROGRAM_SIZE == HalDFlash_ProgramData(dflash_development_nvm_current_write_addr+wr_eep_index, (uint8_t*)&eep_hw_image.development_nvm_write_count+wr_eep_index, DFLASH_PROGRAM_SIZE) )
							{
								wr_eep_back 	= 	wr_eep_index;	// for read compare 
								wr_eep_index 	+= 	DFLASH_PROGRAM_SIZE;
							}
							else
							{
								last_prog_err = TRUE;
								TRACE_VALUE(EEP_ERR,MOD_EEPROM,"Dflash Program Block: development_nvm, HalDFlash_ProgramData error,Address=0x%08x",(dflash_development_nvm_current_write_addr+wr_eep_back));
							}
						}
						if(last_prog_err)
						{	// error, this block is error, use next sector 
							wr_hw_try_cnt ++;
							wr_eep_index = 0x00;
							wr_eep_back  = 0x00;
							
							dflash_development_nvm_current_write_addr 	+= 	EEP_BLOCK_development_nvm_LEN;	
							if ( dflash_development_nvm_current_write_addr >= EEP_BLOCK_development_nvm_DFLASH_END_ADDR )
							{
								dflash_development_nvm_current_write_addr = EEP_BLOCK_development_nvm_DFLASH_START_ADDR;
							}
						}
					}
				}
				
				if ( wr_hw_try_cnt >= 33 )
				{	// error too much, delete this write require
					process_block = 0xFF;
					TRACE_VALUE(EEP_ERR,MOD_EEPROM,"Dflash Program Block: development_nvm,addr=%08x error too much",dflash_development_nvm_current_write_addr);
				}
			}
			else if ( wr_eep_index != wr_eep_back )
			{	// check this block last write 
				boolean last_prog_err = FALSE;
				uint8 	eep_hw[DFLASH_PROGRAM_SIZE];
				if ( HalDFlash_ReadData(dflash_development_nvm_current_write_addr+wr_eep_back, eep_hw, DFLASH_PROGRAM_SIZE) == DFLASH_PROGRAM_SIZE )
				{
					for ( uint8 i = 0; i < DFLASH_PROGRAM_SIZE; i ++ )
					{
						if ( eep_hw[i] != ((uint8_t*)&eep_hw_image.development_nvm_write_count+wr_eep_back)[i] )
						{
							last_prog_err = TRUE;
							TRACE_VALUE(EEP_ERR,MOD_EEPROM,"Dflash Program Block: development_nvm, HalDFlash_ProgramData error,Address(last write)=0x%08x",(dflash_development_nvm_current_write_addr+wr_eep_back));
							break;
						}
					}
				}
				else
				{	// this block read error, use next block
					last_prog_err = TRUE;
					TRACE_VALUE(EEP_ERR,MOD_EEPROM,"Dflash Program Block: development_nvm Read(last read) error,Address=0x%08x",(dflash_development_nvm_current_write_addr+wr_eep_back));
				}
				
				wr_eep_index = 0x00;
				wr_eep_back  = 0x00;
				dflash_development_nvm_current_write_addr 	+= 	EEP_BLOCK_development_nvm_LEN;	
				if ( dflash_development_nvm_current_write_addr >= EEP_BLOCK_development_nvm_DFLASH_END_ADDR )
				{
					dflash_development_nvm_current_write_addr = EEP_BLOCK_development_nvm_DFLASH_START_ADDR;
				}
					
				if ( last_prog_err )
				{
					wr_hw_try_cnt ++;
					if ( wr_hw_try_cnt >= 33 )
					{	// error too much, delete this write require
						process_block 	= 0xFF;		
					}
				}
				else
				{	// last program ok 
					process_block 	= 0xFF;	
					TRACE_VALUE(EEP_ERR,MOD_EEPROM,"Dflash Program Block: development_nvm Complete, crc=0x%08x",eep_hw_image.development_nvm_crc);			
				}
			}
			else
			{	// process success
				process_block = 0xFF;
			}
		}
#endif		// {EEP_BLOCK_development_nvm_IS_EEPROM}
		break;
	case EEP_BLOCK_OemSetting_BLOCK_ID:
#if EEP_BLOCK_OemSetting_IS_EEPROM
		if ( !HalEep_IsBusy() )
		{
			if ( wr_eep_index < EEP_BLOCK_OemSetting_LEN )
			{
				uint8 	eep_hw[EEP_PROGRAM_SIZE];
				boolean error = FALSE;
				
				uint16 hw_addr;
				if ( wr_eep_back ){
					hw_addr = (uint16)(wr_eep_index+EEP_BLOCK_OemSetting_BACKUP_ADDR);
				}else{
					hw_addr = (uint16)(wr_eep_index+EEP_BLOCK_OemSetting_NORMAL_ADDR);
				}
				
				if ( EEP_PROGRAM_SIZE == HalEep_Read(hw_addr, eep_hw, EEP_PROGRAM_SIZE) )
				{	// read ok
					uint8 * eep_image 	= (uint8_t*)&eep_hw_image.OemSetting_write_count + wr_eep_index;					
					for ( uint16 i = 0; i < EEP_PROGRAM_SIZE; i ++ )
					{	
						if ( eep_image[i] != eep_hw[i] )
						{	
							if ( EEP_PROGRAM_SIZE != HalEep_Write(hw_addr,eep_image, EEP_PROGRAM_SIZE) )
							{
								error = TRUE;
								TRACE_VALUE(EEP_ERR,MOD_EEPROM,"HalEep_Write Block:OemSetting Error, retry count=%d",wr_hw_try_cnt);
							}	
							break;
						}
					}
				}
				else
				{
					error = TRUE;
					TRACE_VALUE(EEP_ERR,MOD_EEPROM,"HalEep_Read Block:OemSetting Error, retry count=%d",wr_hw_try_cnt);
				}
				
				if ( error && wr_hw_try_cnt < 33 )
				{
					wr_eep_index ++;
				}
				else
				{
					wr_eep_index 	+=  EEP_PROGRAM_SIZE;
					wr_hw_try_cnt	= 	0x00;
				}
			}
			else if ( !wr_eep_back )
			{
				wr_eep_back  = 	TRUE;
				wr_eep_index =	0x00;
			}
			else
			{	// process success
				process_block = 0xFF;
				TRACE_VALUE(EEP_WRITE_BLOCK,MOD_EEPROM,"Write Eep Block:OemSetting to hw complete, write count=%d",eep_hw_image.OemSetting_write_count);
			}
		}	
#else	// !EEP_BLOCK_OemSetting_IS_EEPROM
		if ( !HalDFlash_IsBusy() )
		{
			if ( wr_eep_index < EEP_BLOCK_OemSetting_LEN )
			{
				boolean dflash_is_blank = TRUE;
				if ( wr_eep_index == 0x00 )
				{	/* check if need erase dflash sector */
					if ( (dflash_OemSetting_current_write_addr - EEP_BLOCK_OemSetting_DFLASH_START_ADDR) % DFLASH_SECTOR_SIZE == 0x00 )
					{	// new sector start, need erase 
						uint32 temp;
						if ( HalDFlash_ReadData(dflash_OemSetting_current_write_addr, (uint8*)&temp, 4) != 4 )
						{	// this sector is error, change to next sector
							wr_eep_index				= 	0x00;
							wr_hw_try_cnt++;
							dflash_is_blank 			= 	FALSE;
							
							dflash_OemSetting_current_write_addr 	+= 	DFLASH_SECTOR_SIZE;	
							if ( dflash_OemSetting_current_write_addr >= EEP_BLOCK_OemSetting_DFLASH_END_ADDR )
							{
								dflash_OemSetting_current_write_addr = EEP_BLOCK_OemSetting_DFLASH_START_ADDR;
							}
						}
						else if ( temp != 0xFFFFFFFF )
						{
							(void)HalDFlash_Erase(dflash_OemSetting_current_write_addr, 0);	//len is not used
							dflash_is_blank = FALSE;
							TRACE_VALUE(EEP_ERR,MOD_EEPROM,"Dflash Erase Block: OemSetting ,Address=0x%08x",dflash_OemSetting_current_write_addr+wr_eep_back);
						}
						else{}/* sector is blank*/
					}
				}

				if ( wr_hw_try_cnt < 33 )
				{
					if ( dflash_is_blank )
					{
						boolean last_prog_err = FALSE;
						if ( wr_eep_index )
						{	/* check last program */
							uint8 eep_hw[DFLASH_PROGRAM_SIZE];
							if ( HalDFlash_ReadData(dflash_OemSetting_current_write_addr+wr_eep_back, eep_hw, DFLASH_PROGRAM_SIZE) == DFLASH_PROGRAM_SIZE )
							{
								for ( uint8 i = 0; i < DFLASH_PROGRAM_SIZE; i ++ )
								{
									if ( eep_hw[i] != ((uint8_t*)&eep_hw_image.OemSetting_write_count+wr_eep_back)[i] )
									{
										last_prog_err = TRUE;
										TRACE_VALUE(EEP_ERR,MOD_EEPROM,"Dflash Program Block: OemSetting Error,Address=0x%08x",dflash_OemSetting_current_write_addr+wr_eep_back);
										break;
									}
								}
							}
							else
							{	// this block read error, use next block
								last_prog_err = TRUE;
								TRACE_VALUE(EEP_ERR,MOD_EEPROM,"Dflash Program Block: OemSetting Read error,Address=0x%08x",(dflash_OemSetting_current_write_addr+wr_eep_back));
							}
						}
						if ( !last_prog_err )
						{
							if ( DFLASH_PROGRAM_SIZE == HalDFlash_ProgramData(dflash_OemSetting_current_write_addr+wr_eep_index, (uint8_t*)&eep_hw_image.OemSetting_write_count+wr_eep_index, DFLASH_PROGRAM_SIZE) )
							{
								wr_eep_back 	= 	wr_eep_index;	// for read compare 
								wr_eep_index 	+= 	DFLASH_PROGRAM_SIZE;
							}
							else
							{
								last_prog_err = TRUE;
								TRACE_VALUE(EEP_ERR,MOD_EEPROM,"Dflash Program Block: OemSetting, HalDFlash_ProgramData error,Address=0x%08x",(dflash_OemSetting_current_write_addr+wr_eep_back));
							}
						}
						if(last_prog_err)
						{	// error, this block is error, use next sector 
							wr_hw_try_cnt ++;
							wr_eep_index = 0x00;
							wr_eep_back  = 0x00;
							
							dflash_OemSetting_current_write_addr 	+= 	EEP_BLOCK_OemSetting_LEN;	
							if ( dflash_OemSetting_current_write_addr >= EEP_BLOCK_OemSetting_DFLASH_END_ADDR )
							{
								dflash_OemSetting_current_write_addr = EEP_BLOCK_OemSetting_DFLASH_START_ADDR;
							}
						}
					}
				}
				
				if ( wr_hw_try_cnt >= 33 )
				{	// error too much, delete this write require
					process_block = 0xFF;
					TRACE_VALUE(EEP_ERR,MOD_EEPROM,"Dflash Program Block: OemSetting,addr=%08x error too much",dflash_OemSetting_current_write_addr);
				}
			}
			else if ( wr_eep_index != wr_eep_back )
			{	// check this block last write 
				boolean last_prog_err = FALSE;
				uint8 	eep_hw[DFLASH_PROGRAM_SIZE];
				if ( HalDFlash_ReadData(dflash_OemSetting_current_write_addr+wr_eep_back, eep_hw, DFLASH_PROGRAM_SIZE) == DFLASH_PROGRAM_SIZE )
				{
					for ( uint8 i = 0; i < DFLASH_PROGRAM_SIZE; i ++ )
					{
						if ( eep_hw[i] != ((uint8_t*)&eep_hw_image.OemSetting_write_count+wr_eep_back)[i] )
						{
							last_prog_err = TRUE;
							TRACE_VALUE(EEP_ERR,MOD_EEPROM,"Dflash Program Block: OemSetting, HalDFlash_ProgramData error,Address(last write)=0x%08x",(dflash_OemSetting_current_write_addr+wr_eep_back));
							break;
						}
					}
				}
				else
				{	// this block read error, use next block
					last_prog_err = TRUE;
					TRACE_VALUE(EEP_ERR,MOD_EEPROM,"Dflash Program Block: OemSetting Read(last read) error,Address=0x%08x",(dflash_OemSetting_current_write_addr+wr_eep_back));
				}
				
				wr_eep_index = 0x00;
				wr_eep_back  = 0x00;
				dflash_OemSetting_current_write_addr 	+= 	EEP_BLOCK_OemSetting_LEN;	
				if ( dflash_OemSetting_current_write_addr >= EEP_BLOCK_OemSetting_DFLASH_END_ADDR )
				{
					dflash_OemSetting_current_write_addr = EEP_BLOCK_OemSetting_DFLASH_START_ADDR;
				}
					
				if ( last_prog_err )
				{
					wr_hw_try_cnt ++;
					if ( wr_hw_try_cnt >= 33 )
					{	// error too much, delete this write require
						process_block 	= 0xFF;		
					}
				}
				else
				{	// last program ok 
					process_block 	= 0xFF;	
					TRACE_VALUE(EEP_ERR,MOD_EEPROM,"Dflash Program Block: OemSetting Complete, crc=0x%08x",eep_hw_image.OemSetting_crc);			
				}
			}
			else
			{	// process success
				process_block = 0xFF;
			}
		}
#endif		// {EEP_BLOCK_OemSetting_IS_EEPROM}
		break;
	case EEP_BLOCK_user_setting_BLOCK_ID:
#if EEP_BLOCK_user_setting_IS_EEPROM
		if ( !HalEep_IsBusy() )
		{
			if ( wr_eep_index < EEP_BLOCK_user_setting_LEN )
			{
				uint8 	eep_hw[EEP_PROGRAM_SIZE];
				boolean error = FALSE;
				
				uint16 hw_addr;
				if ( wr_eep_back ){
					hw_addr = (uint16)(wr_eep_index+EEP_BLOCK_user_setting_BACKUP_ADDR);
				}else{
					hw_addr = (uint16)(wr_eep_index+EEP_BLOCK_user_setting_NORMAL_ADDR);
				}
				
				if ( EEP_PROGRAM_SIZE == HalEep_Read(hw_addr, eep_hw, EEP_PROGRAM_SIZE) )
				{	// read ok
					uint8 * eep_image 	= (uint8_t*)&eep_hw_image.user_setting_write_count + wr_eep_index;					
					for ( uint16 i = 0; i < EEP_PROGRAM_SIZE; i ++ )
					{	
						if ( eep_image[i] != eep_hw[i] )
						{	
							if ( EEP_PROGRAM_SIZE != HalEep_Write(hw_addr,eep_image, EEP_PROGRAM_SIZE) )
							{
								error = TRUE;
								TRACE_VALUE(EEP_ERR,MOD_EEPROM,"HalEep_Write Block:user_setting Error, retry count=%d",wr_hw_try_cnt);
							}	
							break;
						}
					}
				}
				else
				{
					error = TRUE;
					TRACE_VALUE(EEP_ERR,MOD_EEPROM,"HalEep_Read Block:user_setting Error, retry count=%d",wr_hw_try_cnt);
				}
				
				if ( error && wr_hw_try_cnt < 33 )
				{
					wr_eep_index ++;
				}
				else
				{
					wr_eep_index 	+=  EEP_PROGRAM_SIZE;
					wr_hw_try_cnt	= 	0x00;
				}
			}
			else if ( !wr_eep_back )
			{
				wr_eep_back  = 	TRUE;
				wr_eep_index =	0x00;
			}
			else
			{	// process success
				process_block = 0xFF;
				TRACE_VALUE(EEP_WRITE_BLOCK,MOD_EEPROM,"Write Eep Block:user_setting to hw complete, write count=%d",eep_hw_image.user_setting_write_count);
			}
		}	
#else	// !EEP_BLOCK_user_setting_IS_EEPROM
		if ( !HalDFlash_IsBusy() )
		{
			if ( wr_eep_index < EEP_BLOCK_user_setting_LEN )
			{
				boolean dflash_is_blank = TRUE;
				if ( wr_eep_index == 0x00 )
				{	/* check if need erase dflash sector */
					if ( (dflash_user_setting_current_write_addr - EEP_BLOCK_user_setting_DFLASH_START_ADDR) % DFLASH_SECTOR_SIZE == 0x00 )
					{	// new sector start, need erase 
						uint32 temp;
						if ( HalDFlash_ReadData(dflash_user_setting_current_write_addr, (uint8*)&temp, 4) != 4 )
						{	// this sector is error, change to next sector
							wr_eep_index				= 	0x00;
							wr_hw_try_cnt++;
							dflash_is_blank 			= 	FALSE;
							
							dflash_user_setting_current_write_addr 	+= 	DFLASH_SECTOR_SIZE;	
							if ( dflash_user_setting_current_write_addr >= EEP_BLOCK_user_setting_DFLASH_END_ADDR )
							{
								dflash_user_setting_current_write_addr = EEP_BLOCK_user_setting_DFLASH_START_ADDR;
							}
						}
						else if ( temp != 0xFFFFFFFF )
						{
							(void)HalDFlash_Erase(dflash_user_setting_current_write_addr, 0);	//len is not used
							dflash_is_blank = FALSE;
							TRACE_VALUE(EEP_ERR,MOD_EEPROM,"Dflash Erase Block: user_setting ,Address=0x%08x",dflash_user_setting_current_write_addr+wr_eep_back);
						}
						else{}/* sector is blank*/
					}
				}

				if ( wr_hw_try_cnt < 33 )
				{
					if ( dflash_is_blank )
					{
						boolean last_prog_err = FALSE;
						if ( wr_eep_index )
						{	/* check last program */
							uint8 eep_hw[DFLASH_PROGRAM_SIZE];
							if ( HalDFlash_ReadData(dflash_user_setting_current_write_addr+wr_eep_back, eep_hw, DFLASH_PROGRAM_SIZE) == DFLASH_PROGRAM_SIZE )
							{
								for ( uint8 i = 0; i < DFLASH_PROGRAM_SIZE; i ++ )
								{
									if ( eep_hw[i] != ((uint8_t*)&eep_hw_image.user_setting_write_count+wr_eep_back)[i] )
									{
										last_prog_err = TRUE;
										TRACE_VALUE(EEP_ERR,MOD_EEPROM,"Dflash Program Block: user_setting Error,Address=0x%08x",dflash_user_setting_current_write_addr+wr_eep_back);
										break;
									}
								}
							}
							else
							{	// this block read error, use next block
								last_prog_err = TRUE;
								TRACE_VALUE(EEP_ERR,MOD_EEPROM,"Dflash Program Block: user_setting Read error,Address=0x%08x",(dflash_user_setting_current_write_addr+wr_eep_back));
							}
						}
						if ( !last_prog_err )
						{
							if ( DFLASH_PROGRAM_SIZE == HalDFlash_ProgramData(dflash_user_setting_current_write_addr+wr_eep_index, (uint8_t*)&eep_hw_image.user_setting_write_count+wr_eep_index, DFLASH_PROGRAM_SIZE) )
							{
								wr_eep_back 	= 	wr_eep_index;	// for read compare 
								wr_eep_index 	+= 	DFLASH_PROGRAM_SIZE;
							}
							else
							{
								last_prog_err = TRUE;
								TRACE_VALUE(EEP_ERR,MOD_EEPROM,"Dflash Program Block: user_setting, HalDFlash_ProgramData error,Address=0x%08x",(dflash_user_setting_current_write_addr+wr_eep_back));
							}
						}
						if(last_prog_err)
						{	// error, this block is error, use next sector 
							wr_hw_try_cnt ++;
							wr_eep_index = 0x00;
							wr_eep_back  = 0x00;
							
							dflash_user_setting_current_write_addr 	+= 	EEP_BLOCK_user_setting_LEN;	
							if ( dflash_user_setting_current_write_addr >= EEP_BLOCK_user_setting_DFLASH_END_ADDR )
							{
								dflash_user_setting_current_write_addr = EEP_BLOCK_user_setting_DFLASH_START_ADDR;
							}
						}
					}
				}
				
				if ( wr_hw_try_cnt >= 33 )
				{	// error too much, delete this write require
					process_block = 0xFF;
					TRACE_VALUE(EEP_ERR,MOD_EEPROM,"Dflash Program Block: user_setting,addr=%08x error too much",dflash_user_setting_current_write_addr);
				}
			}
			else if ( wr_eep_index != wr_eep_back )
			{	// check this block last write 
				boolean last_prog_err = FALSE;
				uint8 	eep_hw[DFLASH_PROGRAM_SIZE];
				if ( HalDFlash_ReadData(dflash_user_setting_current_write_addr+wr_eep_back, eep_hw, DFLASH_PROGRAM_SIZE) == DFLASH_PROGRAM_SIZE )
				{
					for ( uint8 i = 0; i < DFLASH_PROGRAM_SIZE; i ++ )
					{
						if ( eep_hw[i] != ((uint8_t*)&eep_hw_image.user_setting_write_count+wr_eep_back)[i] )
						{
							last_prog_err = TRUE;
							TRACE_VALUE(EEP_ERR,MOD_EEPROM,"Dflash Program Block: user_setting, HalDFlash_ProgramData error,Address(last write)=0x%08x",(dflash_user_setting_current_write_addr+wr_eep_back));
							break;
						}
					}
				}
				else
				{	// this block read error, use next block
					last_prog_err = TRUE;
					TRACE_VALUE(EEP_ERR,MOD_EEPROM,"Dflash Program Block: user_setting Read(last read) error,Address=0x%08x",(dflash_user_setting_current_write_addr+wr_eep_back));
				}
				
				wr_eep_index = 0x00;
				wr_eep_back  = 0x00;
				dflash_user_setting_current_write_addr 	+= 	EEP_BLOCK_user_setting_LEN;	
				if ( dflash_user_setting_current_write_addr >= EEP_BLOCK_user_setting_DFLASH_END_ADDR )
				{
					dflash_user_setting_current_write_addr = EEP_BLOCK_user_setting_DFLASH_START_ADDR;
				}
					
				if ( last_prog_err )
				{
					wr_hw_try_cnt ++;
					if ( wr_hw_try_cnt >= 33 )
					{	// error too much, delete this write require
						process_block 	= 0xFF;		
					}
				}
				else
				{	// last program ok 
					process_block 	= 0xFF;	
					TRACE_VALUE(EEP_ERR,MOD_EEPROM,"Dflash Program Block: user_setting Complete, crc=0x%08x",eep_hw_image.user_setting_crc);			
				}
			}
			else
			{	// process success
				process_block = 0xFF;
			}
		}
#endif		// {EEP_BLOCK_user_setting_IS_EEPROM}
		break;
	case EEP_BLOCK_DTC_INFO_BLOCK_ID:
#if EEP_BLOCK_DTC_INFO_IS_EEPROM
		if ( !HalEep_IsBusy() )
		{
			if ( wr_eep_index < EEP_BLOCK_DTC_INFO_LEN )
			{
				uint8 	eep_hw[EEP_PROGRAM_SIZE];
				boolean error = FALSE;
				
				uint16 hw_addr;
				if ( wr_eep_back ){
					hw_addr = (uint16)(wr_eep_index+EEP_BLOCK_DTC_INFO_BACKUP_ADDR);
				}else{
					hw_addr = (uint16)(wr_eep_index+EEP_BLOCK_DTC_INFO_NORMAL_ADDR);
				}
				
				if ( EEP_PROGRAM_SIZE == HalEep_Read(hw_addr, eep_hw, EEP_PROGRAM_SIZE) )
				{	// read ok
					uint8 * eep_image 	= (uint8_t*)&eep_hw_image.DTC_INFO_write_count + wr_eep_index;					
					for ( uint16 i = 0; i < EEP_PROGRAM_SIZE; i ++ )
					{	
						if ( eep_image[i] != eep_hw[i] )
						{	
							if ( EEP_PROGRAM_SIZE != HalEep_Write(hw_addr,eep_image, EEP_PROGRAM_SIZE) )
							{
								error = TRUE;
								TRACE_VALUE(EEP_ERR,MOD_EEPROM,"HalEep_Write Block:DTC_INFO Error, retry count=%d",wr_hw_try_cnt);
							}	
							break;
						}
					}
				}
				else
				{
					error = TRUE;
					TRACE_VALUE(EEP_ERR,MOD_EEPROM,"HalEep_Read Block:DTC_INFO Error, retry count=%d",wr_hw_try_cnt);
				}
				
				if ( error && wr_hw_try_cnt < 33 )
				{
					wr_eep_index ++;
				}
				else
				{
					wr_eep_index 	+=  EEP_PROGRAM_SIZE;
					wr_hw_try_cnt	= 	0x00;
				}
			}
			else if ( !wr_eep_back )
			{
				wr_eep_back  = 	TRUE;
				wr_eep_index =	0x00;
			}
			else
			{	// process success
				process_block = 0xFF;
				TRACE_VALUE(EEP_WRITE_BLOCK,MOD_EEPROM,"Write Eep Block:DTC_INFO to hw complete, write count=%d",eep_hw_image.DTC_INFO_write_count);
			}
		}	
#else	// !EEP_BLOCK_DTC_INFO_IS_EEPROM
		if ( !HalDFlash_IsBusy() )
		{
			if ( wr_eep_index < EEP_BLOCK_DTC_INFO_LEN )
			{
				boolean dflash_is_blank = TRUE;
				if ( wr_eep_index == 0x00 )
				{	/* check if need erase dflash sector */
					if ( (dflash_DTC_INFO_current_write_addr - EEP_BLOCK_DTC_INFO_DFLASH_START_ADDR) % DFLASH_SECTOR_SIZE == 0x00 )
					{	// new sector start, need erase 
						uint32 temp;
						if ( HalDFlash_ReadData(dflash_DTC_INFO_current_write_addr, (uint8*)&temp, 4) != 4 )
						{	// this sector is error, change to next sector
							wr_eep_index				= 	0x00;
							wr_hw_try_cnt++;
							dflash_is_blank 			= 	FALSE;
							
							dflash_DTC_INFO_current_write_addr 	+= 	DFLASH_SECTOR_SIZE;	
							if ( dflash_DTC_INFO_current_write_addr >= EEP_BLOCK_DTC_INFO_DFLASH_END_ADDR )
							{
								dflash_DTC_INFO_current_write_addr = EEP_BLOCK_DTC_INFO_DFLASH_START_ADDR;
							}
						}
						else if ( temp != 0xFFFFFFFF )
						{
							(void)HalDFlash_Erase(dflash_DTC_INFO_current_write_addr, 0);	//len is not used
							dflash_is_blank = FALSE;
							TRACE_VALUE(EEP_ERR,MOD_EEPROM,"Dflash Erase Block: DTC_INFO ,Address=0x%08x",dflash_DTC_INFO_current_write_addr+wr_eep_back);
						}
						else{}/* sector is blank*/
					}
				}

				if ( wr_hw_try_cnt < 33 )
				{
					if ( dflash_is_blank )
					{
						boolean last_prog_err = FALSE;
						if ( wr_eep_index )
						{	/* check last program */
							uint8 eep_hw[DFLASH_PROGRAM_SIZE];
							if ( HalDFlash_ReadData(dflash_DTC_INFO_current_write_addr+wr_eep_back, eep_hw, DFLASH_PROGRAM_SIZE) == DFLASH_PROGRAM_SIZE )
							{
								for ( uint8 i = 0; i < DFLASH_PROGRAM_SIZE; i ++ )
								{
									if ( eep_hw[i] != ((uint8_t*)&eep_hw_image.DTC_INFO_write_count+wr_eep_back)[i] )
									{
										last_prog_err = TRUE;
										TRACE_VALUE(EEP_ERR,MOD_EEPROM,"Dflash Program Block: DTC_INFO Error,Address=0x%08x",dflash_DTC_INFO_current_write_addr+wr_eep_back);
										break;
									}
								}
							}
							else
							{	// this block read error, use next block
								last_prog_err = TRUE;
								TRACE_VALUE(EEP_ERR,MOD_EEPROM,"Dflash Program Block: DTC_INFO Read error,Address=0x%08x",(dflash_DTC_INFO_current_write_addr+wr_eep_back));
							}
						}
						if ( !last_prog_err )
						{
							if ( DFLASH_PROGRAM_SIZE == HalDFlash_ProgramData(dflash_DTC_INFO_current_write_addr+wr_eep_index, (uint8_t*)&eep_hw_image.DTC_INFO_write_count+wr_eep_index, DFLASH_PROGRAM_SIZE) )
							{
								wr_eep_back 	= 	wr_eep_index;	// for read compare 
								wr_eep_index 	+= 	DFLASH_PROGRAM_SIZE;
							}
							else
							{
								last_prog_err = TRUE;
								TRACE_VALUE(EEP_ERR,MOD_EEPROM,"Dflash Program Block: DTC_INFO, HalDFlash_ProgramData error,Address=0x%08x",(dflash_DTC_INFO_current_write_addr+wr_eep_back));
							}
						}
						if(last_prog_err)
						{	// error, this block is error, use next sector 
							wr_hw_try_cnt ++;
							wr_eep_index = 0x00;
							wr_eep_back  = 0x00;
							
							dflash_DTC_INFO_current_write_addr 	+= 	EEP_BLOCK_DTC_INFO_LEN;	
							if ( dflash_DTC_INFO_current_write_addr >= EEP_BLOCK_DTC_INFO_DFLASH_END_ADDR )
							{
								dflash_DTC_INFO_current_write_addr = EEP_BLOCK_DTC_INFO_DFLASH_START_ADDR;
							}
						}
					}
				}
				
				if ( wr_hw_try_cnt >= 33 )
				{	// error too much, delete this write require
					process_block = 0xFF;
					TRACE_VALUE(EEP_ERR,MOD_EEPROM,"Dflash Program Block: DTC_INFO,addr=%08x error too much",dflash_DTC_INFO_current_write_addr);
				}
			}
			else if ( wr_eep_index != wr_eep_back )
			{	// check this block last write 
				boolean last_prog_err = FALSE;
				uint8 	eep_hw[DFLASH_PROGRAM_SIZE];
				if ( HalDFlash_ReadData(dflash_DTC_INFO_current_write_addr+wr_eep_back, eep_hw, DFLASH_PROGRAM_SIZE) == DFLASH_PROGRAM_SIZE )
				{
					for ( uint8 i = 0; i < DFLASH_PROGRAM_SIZE; i ++ )
					{
						if ( eep_hw[i] != ((uint8_t*)&eep_hw_image.DTC_INFO_write_count+wr_eep_back)[i] )
						{
							last_prog_err = TRUE;
							TRACE_VALUE(EEP_ERR,MOD_EEPROM,"Dflash Program Block: DTC_INFO, HalDFlash_ProgramData error,Address(last write)=0x%08x",(dflash_DTC_INFO_current_write_addr+wr_eep_back));
							break;
						}
					}
				}
				else
				{	// this block read error, use next block
					last_prog_err = TRUE;
					TRACE_VALUE(EEP_ERR,MOD_EEPROM,"Dflash Program Block: DTC_INFO Read(last read) error,Address=0x%08x",(dflash_DTC_INFO_current_write_addr+wr_eep_back));
				}
				
				wr_eep_index = 0x00;
				wr_eep_back  = 0x00;
				dflash_DTC_INFO_current_write_addr 	+= 	EEP_BLOCK_DTC_INFO_LEN;	
				if ( dflash_DTC_INFO_current_write_addr >= EEP_BLOCK_DTC_INFO_DFLASH_END_ADDR )
				{
					dflash_DTC_INFO_current_write_addr = EEP_BLOCK_DTC_INFO_DFLASH_START_ADDR;
				}
					
				if ( last_prog_err )
				{
					wr_hw_try_cnt ++;
					if ( wr_hw_try_cnt >= 33 )
					{	// error too much, delete this write require
						process_block 	= 0xFF;		
					}
				}
				else
				{	// last program ok 
					process_block 	= 0xFF;	
					TRACE_VALUE(EEP_ERR,MOD_EEPROM,"Dflash Program Block: DTC_INFO Complete, crc=0x%08x",eep_hw_image.DTC_INFO_crc);			
				}
			}
			else
			{	// process success
				process_block = 0xFF;
			}
		}
#endif		// {EEP_BLOCK_DTC_INFO_IS_EEPROM}
		break;
	case EEP_BLOCK_OdoInfo_BLOCK_ID:
#if EEP_BLOCK_OdoInfo_IS_EEPROM
		if ( !HalEep_IsBusy() )
		{
			if ( wr_eep_index < EEP_BLOCK_OdoInfo_LEN )
			{
				uint8 	eep_hw[EEP_PROGRAM_SIZE];
				boolean error = FALSE;
				
				uint16 hw_addr;
				if ( wr_eep_back ){
					hw_addr = (uint16)(wr_eep_index+EEP_BLOCK_OdoInfo_BACKUP_ADDR);
				}else{
					hw_addr = (uint16)(wr_eep_index+EEP_BLOCK_OdoInfo_NORMAL_ADDR);
				}
				
				if ( EEP_PROGRAM_SIZE == HalEep_Read(hw_addr, eep_hw, EEP_PROGRAM_SIZE) )
				{	// read ok
					uint8 * eep_image 	= (uint8_t*)&eep_hw_image.OdoInfo_write_count + wr_eep_index;					
					for ( uint16 i = 0; i < EEP_PROGRAM_SIZE; i ++ )
					{	
						if ( eep_image[i] != eep_hw[i] )
						{	
							if ( EEP_PROGRAM_SIZE != HalEep_Write(hw_addr,eep_image, EEP_PROGRAM_SIZE) )
							{
								error = TRUE;
								TRACE_VALUE(EEP_ERR,MOD_EEPROM,"HalEep_Write Block:OdoInfo Error, retry count=%d",wr_hw_try_cnt);
							}	
							break;
						}
					}
				}
				else
				{
					error = TRUE;
					TRACE_VALUE(EEP_ERR,MOD_EEPROM,"HalEep_Read Block:OdoInfo Error, retry count=%d",wr_hw_try_cnt);
				}
				
				if ( error && wr_hw_try_cnt < 33 )
				{
					wr_eep_index ++;
				}
				else
				{
					wr_eep_index 	+=  EEP_PROGRAM_SIZE;
					wr_hw_try_cnt	= 	0x00;
				}
			}
			else if ( !wr_eep_back )
			{
				wr_eep_back  = 	TRUE;
				wr_eep_index =	0x00;
			}
			else
			{	// process success
				process_block = 0xFF;
				TRACE_VALUE(EEP_WRITE_BLOCK,MOD_EEPROM,"Write Eep Block:OdoInfo to hw complete, write count=%d",eep_hw_image.OdoInfo_write_count);
			}
		}	
#else	// !EEP_BLOCK_OdoInfo_IS_EEPROM
		if ( !HalDFlash_IsBusy() )
		{
			if ( wr_eep_index < EEP_BLOCK_OdoInfo_LEN )
			{
				boolean dflash_is_blank = TRUE;
				if ( wr_eep_index == 0x00 )
				{	/* check if need erase dflash sector */
					if ( (dflash_OdoInfo_current_write_addr - EEP_BLOCK_OdoInfo_DFLASH_START_ADDR) % DFLASH_SECTOR_SIZE == 0x00 )
					{	// new sector start, need erase 
						uint32 temp;
						if ( HalDFlash_ReadData(dflash_OdoInfo_current_write_addr, (uint8*)&temp, 4) != 4 )
						{	// this sector is error, change to next sector
							wr_eep_index				= 	0x00;
							wr_hw_try_cnt++;
							dflash_is_blank 			= 	FALSE;
							
							dflash_OdoInfo_current_write_addr 	+= 	DFLASH_SECTOR_SIZE;	
							if ( dflash_OdoInfo_current_write_addr >= EEP_BLOCK_OdoInfo_DFLASH_END_ADDR )
							{
								dflash_OdoInfo_current_write_addr = EEP_BLOCK_OdoInfo_DFLASH_START_ADDR;
							}
						}
						else if ( temp != 0xFFFFFFFF )
						{
							(void)HalDFlash_Erase(dflash_OdoInfo_current_write_addr, 0);	//len is not used
							dflash_is_blank = FALSE;
							TRACE_VALUE(EEP_ERR,MOD_EEPROM,"Dflash Erase Block: OdoInfo ,Address=0x%08x",dflash_OdoInfo_current_write_addr+wr_eep_back);
						}
						else{}/* sector is blank*/
					}
				}

				if ( wr_hw_try_cnt < 33 )
				{
					if ( dflash_is_blank )
					{
						boolean last_prog_err = FALSE;
						if ( wr_eep_index )
						{	/* check last program */
							uint8 eep_hw[DFLASH_PROGRAM_SIZE];
							if ( HalDFlash_ReadData(dflash_OdoInfo_current_write_addr+wr_eep_back, eep_hw, DFLASH_PROGRAM_SIZE) == DFLASH_PROGRAM_SIZE )
							{
								for ( uint8 i = 0; i < DFLASH_PROGRAM_SIZE; i ++ )
								{
									if ( eep_hw[i] != ((uint8_t*)&eep_hw_image.OdoInfo_write_count+wr_eep_back)[i] )
									{
										last_prog_err = TRUE;
										TRACE_VALUE(EEP_ERR,MOD_EEPROM,"Dflash Program Block: OdoInfo Error,Address=0x%08x",dflash_OdoInfo_current_write_addr+wr_eep_back);
										break;
									}
								}
							}
							else
							{	// this block read error, use next block
								last_prog_err = TRUE;
								TRACE_VALUE(EEP_ERR,MOD_EEPROM,"Dflash Program Block: OdoInfo Read error,Address=0x%08x",(dflash_OdoInfo_current_write_addr+wr_eep_back));
							}
						}
						if ( !last_prog_err )
						{
							if ( DFLASH_PROGRAM_SIZE == HalDFlash_ProgramData(dflash_OdoInfo_current_write_addr+wr_eep_index, (uint8_t*)&eep_hw_image.OdoInfo_write_count+wr_eep_index, DFLASH_PROGRAM_SIZE) )
							{
								wr_eep_back 	= 	wr_eep_index;	// for read compare 
								wr_eep_index 	+= 	DFLASH_PROGRAM_SIZE;
							}
							else
							{
								last_prog_err = TRUE;
								TRACE_VALUE(EEP_ERR,MOD_EEPROM,"Dflash Program Block: OdoInfo, HalDFlash_ProgramData error,Address=0x%08x",(dflash_OdoInfo_current_write_addr+wr_eep_back));
							}
						}
						if(last_prog_err)
						{	// error, this block is error, use next sector 
							wr_hw_try_cnt ++;
							wr_eep_index = 0x00;
							wr_eep_back  = 0x00;
							
							dflash_OdoInfo_current_write_addr 	+= 	EEP_BLOCK_OdoInfo_LEN;	
							if ( dflash_OdoInfo_current_write_addr >= EEP_BLOCK_OdoInfo_DFLASH_END_ADDR )
							{
								dflash_OdoInfo_current_write_addr = EEP_BLOCK_OdoInfo_DFLASH_START_ADDR;
							}
						}
					}
				}
				
				if ( wr_hw_try_cnt >= 33 )
				{	// error too much, delete this write require
					process_block = 0xFF;
					TRACE_VALUE(EEP_ERR,MOD_EEPROM,"Dflash Program Block: OdoInfo,addr=%08x error too much",dflash_OdoInfo_current_write_addr);
				}
			}
			else if ( wr_eep_index != wr_eep_back )
			{	// check this block last write 
				boolean last_prog_err = FALSE;
				uint8 	eep_hw[DFLASH_PROGRAM_SIZE];
				if ( HalDFlash_ReadData(dflash_OdoInfo_current_write_addr+wr_eep_back, eep_hw, DFLASH_PROGRAM_SIZE) == DFLASH_PROGRAM_SIZE )
				{
					for ( uint8 i = 0; i < DFLASH_PROGRAM_SIZE; i ++ )
					{
						if ( eep_hw[i] != ((uint8_t*)&eep_hw_image.OdoInfo_write_count+wr_eep_back)[i] )
						{
							last_prog_err = TRUE;
							TRACE_VALUE(EEP_ERR,MOD_EEPROM,"Dflash Program Block: OdoInfo, HalDFlash_ProgramData error,Address(last write)=0x%08x",(dflash_OdoInfo_current_write_addr+wr_eep_back));
							break;
						}
					}
				}
				else
				{	// this block read error, use next block
					last_prog_err = TRUE;
					TRACE_VALUE(EEP_ERR,MOD_EEPROM,"Dflash Program Block: OdoInfo Read(last read) error,Address=0x%08x",(dflash_OdoInfo_current_write_addr+wr_eep_back));
				}
				
				wr_eep_index = 0x00;
				wr_eep_back  = 0x00;
				dflash_OdoInfo_current_write_addr 	+= 	EEP_BLOCK_OdoInfo_LEN;	
				if ( dflash_OdoInfo_current_write_addr >= EEP_BLOCK_OdoInfo_DFLASH_END_ADDR )
				{
					dflash_OdoInfo_current_write_addr = EEP_BLOCK_OdoInfo_DFLASH_START_ADDR;
				}
					
				if ( last_prog_err )
				{
					wr_hw_try_cnt ++;
					if ( wr_hw_try_cnt >= 33 )
					{	// error too much, delete this write require
						process_block 	= 0xFF;		
					}
				}
				else
				{	// last program ok 
					process_block 	= 0xFF;	
					TRACE_VALUE(EEP_ERR,MOD_EEPROM,"Dflash Program Block: OdoInfo Complete, crc=0x%08x",eep_hw_image.OdoInfo_crc);			
				}
			}
			else
			{	// process success
				process_block = 0xFF;
			}
		}
#endif		// {EEP_BLOCK_OdoInfo_IS_EEPROM}
		break;
	case EEP_BLOCK_share_eep_BLOCK_ID:
#if EEP_BLOCK_share_eep_IS_EEPROM
		if ( !HalEep_IsBusy() )
		{
			if ( wr_eep_index < EEP_BLOCK_share_eep_LEN )
			{
				uint8 	eep_hw[EEP_PROGRAM_SIZE];
				boolean error = FALSE;
				
				uint16 hw_addr;
				if ( wr_eep_back ){
					hw_addr = (uint16)(wr_eep_index+EEP_BLOCK_share_eep_BACKUP_ADDR);
				}else{
					hw_addr = (uint16)(wr_eep_index+EEP_BLOCK_share_eep_NORMAL_ADDR);
				}
				
				if ( EEP_PROGRAM_SIZE == HalEep_Read(hw_addr, eep_hw, EEP_PROGRAM_SIZE) )
				{	// read ok
					uint8 * eep_image 	= (uint8_t*)&eep_hw_image.share_eep_write_count + wr_eep_index;					
					for ( uint16 i = 0; i < EEP_PROGRAM_SIZE; i ++ )
					{	
						if ( eep_image[i] != eep_hw[i] )
						{	
							if ( EEP_PROGRAM_SIZE != HalEep_Write(hw_addr,eep_image, EEP_PROGRAM_SIZE) )
							{
								error = TRUE;
								TRACE_VALUE(EEP_ERR,MOD_EEPROM,"HalEep_Write Block:share_eep Error, retry count=%d",wr_hw_try_cnt);
							}	
							break;
						}
					}
				}
				else
				{
					error = TRUE;
					TRACE_VALUE(EEP_ERR,MOD_EEPROM,"HalEep_Read Block:share_eep Error, retry count=%d",wr_hw_try_cnt);
				}
				
				if ( error && wr_hw_try_cnt < 33 )
				{
					wr_eep_index ++;
				}
				else
				{
					wr_eep_index 	+=  EEP_PROGRAM_SIZE;
					wr_hw_try_cnt	= 	0x00;
				}
			}
			else if ( !wr_eep_back )
			{
				wr_eep_back  = 	TRUE;
				wr_eep_index =	0x00;
			}
			else
			{	// process success
				process_block = 0xFF;
				TRACE_VALUE(EEP_WRITE_BLOCK,MOD_EEPROM,"Write Eep Block:share_eep to hw complete, write count=%d",eep_hw_image.share_eep_write_count);
			}
		}	
#else	// !EEP_BLOCK_share_eep_IS_EEPROM
		if ( !HalDFlash_IsBusy() )
		{
			if ( wr_eep_index < EEP_BLOCK_share_eep_LEN )
			{
				boolean dflash_is_blank = TRUE;
				if ( wr_eep_index == 0x00 )
				{	/* check if need erase dflash sector */
					if ( (dflash_share_eep_current_write_addr - EEP_BLOCK_share_eep_DFLASH_START_ADDR) % DFLASH_SECTOR_SIZE == 0x00 )
					{	// new sector start, need erase 
						uint32 temp;
						if ( HalDFlash_ReadData(dflash_share_eep_current_write_addr, (uint8*)&temp, 4) != 4 )
						{	// this sector is error, change to next sector
							wr_eep_index				= 	0x00;
							wr_hw_try_cnt++;
							dflash_is_blank 			= 	FALSE;
							
							dflash_share_eep_current_write_addr 	+= 	DFLASH_SECTOR_SIZE;	
							if ( dflash_share_eep_current_write_addr >= EEP_BLOCK_share_eep_DFLASH_END_ADDR )
							{
								dflash_share_eep_current_write_addr = EEP_BLOCK_share_eep_DFLASH_START_ADDR;
							}
						}
						else if ( temp != 0xFFFFFFFF )
						{
							(void)HalDFlash_Erase(dflash_share_eep_current_write_addr, 0);	//len is not used
							dflash_is_blank = FALSE;
							TRACE_VALUE(EEP_ERR,MOD_EEPROM,"Dflash Erase Block: share_eep ,Address=0x%08x",dflash_share_eep_current_write_addr+wr_eep_back);
						}
						else{}/* sector is blank*/
					}
				}

				if ( wr_hw_try_cnt < 33 )
				{
					if ( dflash_is_blank )
					{
						boolean last_prog_err = FALSE;
						if ( wr_eep_index )
						{	/* check last program */
							uint8 eep_hw[DFLASH_PROGRAM_SIZE];
							if ( HalDFlash_ReadData(dflash_share_eep_current_write_addr+wr_eep_back, eep_hw, DFLASH_PROGRAM_SIZE) == DFLASH_PROGRAM_SIZE )
							{
								for ( uint8 i = 0; i < DFLASH_PROGRAM_SIZE; i ++ )
								{
									if ( eep_hw[i] != ((uint8_t*)&eep_hw_image.share_eep_write_count+wr_eep_back)[i] )
									{
										last_prog_err = TRUE;
										TRACE_VALUE(EEP_ERR,MOD_EEPROM,"Dflash Program Block: share_eep Error,Address=0x%08x",dflash_share_eep_current_write_addr+wr_eep_back);
										break;
									}
								}
							}
							else
							{	// this block read error, use next block
								last_prog_err = TRUE;
								TRACE_VALUE(EEP_ERR,MOD_EEPROM,"Dflash Program Block: share_eep Read error,Address=0x%08x",(dflash_share_eep_current_write_addr+wr_eep_back));
							}
						}
						if ( !last_prog_err )
						{
							if ( DFLASH_PROGRAM_SIZE == HalDFlash_ProgramData(dflash_share_eep_current_write_addr+wr_eep_index, (uint8_t*)&eep_hw_image.share_eep_write_count+wr_eep_index, DFLASH_PROGRAM_SIZE) )
							{
								wr_eep_back 	= 	wr_eep_index;	// for read compare 
								wr_eep_index 	+= 	DFLASH_PROGRAM_SIZE;
							}
							else
							{
								last_prog_err = TRUE;
								TRACE_VALUE(EEP_ERR,MOD_EEPROM,"Dflash Program Block: share_eep, HalDFlash_ProgramData error,Address=0x%08x",(dflash_share_eep_current_write_addr+wr_eep_back));
							}
						}
						if(last_prog_err)
						{	// error, this block is error, use next sector 
							wr_hw_try_cnt ++;
							wr_eep_index = 0x00;
							wr_eep_back  = 0x00;
							
							dflash_share_eep_current_write_addr 	+= 	EEP_BLOCK_share_eep_LEN;	
							if ( dflash_share_eep_current_write_addr >= EEP_BLOCK_share_eep_DFLASH_END_ADDR )
							{
								dflash_share_eep_current_write_addr = EEP_BLOCK_share_eep_DFLASH_START_ADDR;
							}
						}
					}
				}
				
				if ( wr_hw_try_cnt >= 33 )
				{	// error too much, delete this write require
					process_block = 0xFF;
					TRACE_VALUE(EEP_ERR,MOD_EEPROM,"Dflash Program Block: share_eep,addr=%08x error too much",dflash_share_eep_current_write_addr);
				}
			}
			else if ( wr_eep_index != wr_eep_back )
			{	// check this block last write 
				boolean last_prog_err = FALSE;
				uint8 	eep_hw[DFLASH_PROGRAM_SIZE];
				if ( HalDFlash_ReadData(dflash_share_eep_current_write_addr+wr_eep_back, eep_hw, DFLASH_PROGRAM_SIZE) == DFLASH_PROGRAM_SIZE )
				{
					for ( uint8 i = 0; i < DFLASH_PROGRAM_SIZE; i ++ )
					{
						if ( eep_hw[i] != ((uint8_t*)&eep_hw_image.share_eep_write_count+wr_eep_back)[i] )
						{
							last_prog_err = TRUE;
							TRACE_VALUE(EEP_ERR,MOD_EEPROM,"Dflash Program Block: share_eep, HalDFlash_ProgramData error,Address(last write)=0x%08x",(dflash_share_eep_current_write_addr+wr_eep_back));
							break;
						}
					}
				}
				else
				{	// this block read error, use next block
					last_prog_err = TRUE;
					TRACE_VALUE(EEP_ERR,MOD_EEPROM,"Dflash Program Block: share_eep Read(last read) error,Address=0x%08x",(dflash_share_eep_current_write_addr+wr_eep_back));
				}
				
				wr_eep_index = 0x00;
				wr_eep_back  = 0x00;
				dflash_share_eep_current_write_addr 	+= 	EEP_BLOCK_share_eep_LEN;	
				if ( dflash_share_eep_current_write_addr >= EEP_BLOCK_share_eep_DFLASH_END_ADDR )
				{
					dflash_share_eep_current_write_addr = EEP_BLOCK_share_eep_DFLASH_START_ADDR;
				}
					
				if ( last_prog_err )
				{
					wr_hw_try_cnt ++;
					if ( wr_hw_try_cnt >= 33 )
					{	// error too much, delete this write require
						process_block 	= 0xFF;		
					}
				}
				else
				{	// last program ok 
					process_block 	= 0xFF;	
					TRACE_VALUE(EEP_ERR,MOD_EEPROM,"Dflash Program Block: share_eep Complete, crc=0x%08x",eep_hw_image.share_eep_crc);			
				}
			}
			else
			{	// process success
				process_block = 0xFF;
			}
		}
#endif		// {EEP_BLOCK_share_eep_IS_EEPROM}
		break;
	case EEP_BLOCK_ResetInfo_BLOCK_ID:
#if EEP_BLOCK_ResetInfo_IS_EEPROM
		if ( !HalEep_IsBusy() )
		{
			if ( wr_eep_index < EEP_BLOCK_ResetInfo_LEN )
			{
				uint8 	eep_hw[EEP_PROGRAM_SIZE];
				boolean error = FALSE;
				
				uint16 hw_addr;
				if ( wr_eep_back ){
					hw_addr = (uint16)(wr_eep_index+EEP_BLOCK_ResetInfo_BACKUP_ADDR);
				}else{
					hw_addr = (uint16)(wr_eep_index+EEP_BLOCK_ResetInfo_NORMAL_ADDR);
				}
				
				if ( EEP_PROGRAM_SIZE == HalEep_Read(hw_addr, eep_hw, EEP_PROGRAM_SIZE) )
				{	// read ok
					uint8 * eep_image 	= (uint8_t*)&eep_hw_image.ResetInfo_write_count + wr_eep_index;					
					for ( uint16 i = 0; i < EEP_PROGRAM_SIZE; i ++ )
					{	
						if ( eep_image[i] != eep_hw[i] )
						{	
							if ( EEP_PROGRAM_SIZE != HalEep_Write(hw_addr,eep_image, EEP_PROGRAM_SIZE) )
							{
								error = TRUE;
								TRACE_VALUE(EEP_ERR,MOD_EEPROM,"HalEep_Write Block:ResetInfo Error, retry count=%d",wr_hw_try_cnt);
							}	
							break;
						}
					}
				}
				else
				{
					error = TRUE;
					TRACE_VALUE(EEP_ERR,MOD_EEPROM,"HalEep_Read Block:ResetInfo Error, retry count=%d",wr_hw_try_cnt);
				}
				
				if ( error && wr_hw_try_cnt < 33 )
				{
					wr_eep_index ++;
				}
				else
				{
					wr_eep_index 	+=  EEP_PROGRAM_SIZE;
					wr_hw_try_cnt	= 	0x00;
				}
			}
			else if ( !wr_eep_back )
			{
				wr_eep_back  = 	TRUE;
				wr_eep_index =	0x00;
			}
			else
			{	// process success
				process_block = 0xFF;
				TRACE_VALUE(EEP_WRITE_BLOCK,MOD_EEPROM,"Write Eep Block:ResetInfo to hw complete, write count=%d",eep_hw_image.ResetInfo_write_count);
			}
		}	
#else	// !EEP_BLOCK_ResetInfo_IS_EEPROM
		if ( !HalDFlash_IsBusy() )
		{
			if ( wr_eep_index < EEP_BLOCK_ResetInfo_LEN )
			{
				boolean dflash_is_blank = TRUE;
				if ( wr_eep_index == 0x00 )
				{	/* check if need erase dflash sector */
					if ( (dflash_ResetInfo_current_write_addr - EEP_BLOCK_ResetInfo_DFLASH_START_ADDR) % DFLASH_SECTOR_SIZE == 0x00 )
					{	// new sector start, need erase 
						uint32 temp;
						if ( HalDFlash_ReadData(dflash_ResetInfo_current_write_addr, (uint8*)&temp, 4) != 4 )
						{	// this sector is error, change to next sector
							wr_eep_index				= 	0x00;
							wr_hw_try_cnt++;
							dflash_is_blank 			= 	FALSE;
							
							dflash_ResetInfo_current_write_addr 	+= 	DFLASH_SECTOR_SIZE;	
							if ( dflash_ResetInfo_current_write_addr >= EEP_BLOCK_ResetInfo_DFLASH_END_ADDR )
							{
								dflash_ResetInfo_current_write_addr = EEP_BLOCK_ResetInfo_DFLASH_START_ADDR;
							}
						}
						else if ( temp != 0xFFFFFFFF )
						{
							(void)HalDFlash_Erase(dflash_ResetInfo_current_write_addr, 0);	//len is not used
							dflash_is_blank = FALSE;
							TRACE_VALUE(EEP_ERR,MOD_EEPROM,"Dflash Erase Block: ResetInfo ,Address=0x%08x",dflash_ResetInfo_current_write_addr+wr_eep_back);
						}
						else{}/* sector is blank*/
					}
				}

				if ( wr_hw_try_cnt < 33 )
				{
					if ( dflash_is_blank )
					{
						boolean last_prog_err = FALSE;
						if ( wr_eep_index )
						{	/* check last program */
							uint8 eep_hw[DFLASH_PROGRAM_SIZE];
							if ( HalDFlash_ReadData(dflash_ResetInfo_current_write_addr+wr_eep_back, eep_hw, DFLASH_PROGRAM_SIZE) == DFLASH_PROGRAM_SIZE )
							{
								for ( uint8 i = 0; i < DFLASH_PROGRAM_SIZE; i ++ )
								{
									if ( eep_hw[i] != ((uint8_t*)&eep_hw_image.ResetInfo_write_count+wr_eep_back)[i] )
									{
										last_prog_err = TRUE;
										TRACE_VALUE(EEP_ERR,MOD_EEPROM,"Dflash Program Block: ResetInfo Error,Address=0x%08x",dflash_ResetInfo_current_write_addr+wr_eep_back);
										break;
									}
								}
							}
							else
							{	// this block read error, use next block
								last_prog_err = TRUE;
								TRACE_VALUE(EEP_ERR,MOD_EEPROM,"Dflash Program Block: ResetInfo Read error,Address=0x%08x",(dflash_ResetInfo_current_write_addr+wr_eep_back));
							}
						}
						if ( !last_prog_err )
						{
							if ( DFLASH_PROGRAM_SIZE == HalDFlash_ProgramData(dflash_ResetInfo_current_write_addr+wr_eep_index, (uint8_t*)&eep_hw_image.ResetInfo_write_count+wr_eep_index, DFLASH_PROGRAM_SIZE) )
							{
								wr_eep_back 	= 	wr_eep_index;	// for read compare 
								wr_eep_index 	+= 	DFLASH_PROGRAM_SIZE;
							}
							else
							{
								last_prog_err = TRUE;
								TRACE_VALUE(EEP_ERR,MOD_EEPROM,"Dflash Program Block: ResetInfo, HalDFlash_ProgramData error,Address=0x%08x",(dflash_ResetInfo_current_write_addr+wr_eep_back));
							}
						}
						if(last_prog_err)
						{	// error, this block is error, use next sector 
							wr_hw_try_cnt ++;
							wr_eep_index = 0x00;
							wr_eep_back  = 0x00;
							
							dflash_ResetInfo_current_write_addr 	+= 	EEP_BLOCK_ResetInfo_LEN;	
							if ( dflash_ResetInfo_current_write_addr >= EEP_BLOCK_ResetInfo_DFLASH_END_ADDR )
							{
								dflash_ResetInfo_current_write_addr = EEP_BLOCK_ResetInfo_DFLASH_START_ADDR;
							}
						}
					}
				}
				
				if ( wr_hw_try_cnt >= 33 )
				{	// error too much, delete this write require
					process_block = 0xFF;
					TRACE_VALUE(EEP_ERR,MOD_EEPROM,"Dflash Program Block: ResetInfo,addr=%08x error too much",dflash_ResetInfo_current_write_addr);
				}
			}
			else if ( wr_eep_index != wr_eep_back )
			{	// check this block last write 
				boolean last_prog_err = FALSE;
				uint8 	eep_hw[DFLASH_PROGRAM_SIZE];
				if ( HalDFlash_ReadData(dflash_ResetInfo_current_write_addr+wr_eep_back, eep_hw, DFLASH_PROGRAM_SIZE) == DFLASH_PROGRAM_SIZE )
				{
					for ( uint8 i = 0; i < DFLASH_PROGRAM_SIZE; i ++ )
					{
						if ( eep_hw[i] != ((uint8_t*)&eep_hw_image.ResetInfo_write_count+wr_eep_back)[i] )
						{
							last_prog_err = TRUE;
							TRACE_VALUE(EEP_ERR,MOD_EEPROM,"Dflash Program Block: ResetInfo, HalDFlash_ProgramData error,Address(last write)=0x%08x",(dflash_ResetInfo_current_write_addr+wr_eep_back));
							break;
						}
					}
				}
				else
				{	// this block read error, use next block
					last_prog_err = TRUE;
					TRACE_VALUE(EEP_ERR,MOD_EEPROM,"Dflash Program Block: ResetInfo Read(last read) error,Address=0x%08x",(dflash_ResetInfo_current_write_addr+wr_eep_back));
				}
				
				wr_eep_index = 0x00;
				wr_eep_back  = 0x00;
				dflash_ResetInfo_current_write_addr 	+= 	EEP_BLOCK_ResetInfo_LEN;	
				if ( dflash_ResetInfo_current_write_addr >= EEP_BLOCK_ResetInfo_DFLASH_END_ADDR )
				{
					dflash_ResetInfo_current_write_addr = EEP_BLOCK_ResetInfo_DFLASH_START_ADDR;
				}
					
				if ( last_prog_err )
				{
					wr_hw_try_cnt ++;
					if ( wr_hw_try_cnt >= 33 )
					{	// error too much, delete this write require
						process_block 	= 0xFF;		
					}
				}
				else
				{	// last program ok 
					process_block 	= 0xFF;	
					TRACE_VALUE(EEP_ERR,MOD_EEPROM,"Dflash Program Block: ResetInfo Complete, crc=0x%08x",eep_hw_image.ResetInfo_crc);			
				}
			}
			else
			{	// process success
				process_block = 0xFF;
			}
		}
#endif		// {EEP_BLOCK_ResetInfo_IS_EEPROM}
		break;
		
	default:
		process_block = 0xFF;
		break;
	}
}



/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/



/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/




