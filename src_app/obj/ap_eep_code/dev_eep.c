/*******************************************************************************
 * Project         Panada Pluse
 * (c) Copyright   2017
 * Company         Visteon Asia Pacific (Shanghai), inc
 *                 All rights reserved.
 * @file           dev_eep.c
 * @module         device signal 
 * @author         Chao Wang
 * @brief          device handle process
 * @changelog      1. Creat File
********************************************************************************/
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <pthread.h>
#include <semaphore.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <string.h>
#include <stddef.h>
#include "dev_eep.h"
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

#define	AP_WRITE_EEP_LIST_CMD		(0x21)

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
typedef struct
{
	int 			dev_fd;
	pthread_t 		read_thread;
	pthread_t 		invoke_thread;
	
	pthread_mutex_t  mutex;     	/* mutex: for set callbk_invoke and callback handle   */
	sem_t            invoke_sem;	/* for activate invoke semphore 					  */
	uint8_t 		 invoke_task;
	
	uint8_t			all_eep_get;
	
	EEPROM_ST		current_eep;
	uint8_t			calbk_req[EEP_CONTENT_LIST_NUMBER_MAX];
	void * 			callback[EEP_CONTENT_LIST_NUMBER_MAX];	// callback eeprom

	EEPROM_ST		process_eep;
	uint8_t			proc_req[EEP_CONTENT_LIST_NUMBER_MAX];
	void * 			proc_calbk[EEP_CONTENT_LIST_NUMBER_MAX];	// callback eeprom
}DEV_EEP_AP;

typedef struct
{
	uint16_t list_id;
	uint16_t list_len;	// byte len 
	uint16_t offset;	// offset 

	void (*callback)(DEV_EEP_AP *ap,void *callbk);
}EEP_LIST_INFO;
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
/*****************************************************************************************************************************/
static void *dev_eep_thread_decode(void *arg);
static void *dev_eep_thread_execute(void *arg);

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
// api define for Manuf_Visteon_Part_Number /////////////////////////////////////////////////////////
#if EEP_CONTENT_Manuf_Visteon_Part_Number_ITEM_LEN == 1
uint8_t DevEep_Get_Manuf_Visteon_Part_Number(EEP_HANDLE eep_handle)
{
	uint8_t Manuf_Visteon_Part_Number_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		Manuf_Visteon_Part_Number_val = ap_eep->process_eep.Manuf_Visteon_Part_Number;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return Manuf_Visteon_Part_Number_val;
}
void DevEep_Set_Manuf_Visteon_Part_Number(EEP_HANDLE eep_handle,uint8_t const Manuf_Visteon_Part_Number_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_Manuf_Visteon_Part_Number_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_Manuf_Visteon_Part_Number_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_Manuf_Visteon_Part_Number_LIST_ID);
#if EEP_CONTENT_Manuf_Visteon_Part_Number_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = Manuf_Visteon_Part_Number_val;
#elif EEP_CONTENT_Manuf_Visteon_Part_Number_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(Manuf_Visteon_Part_Number_val);
	msg[len++] = (uint8_t)(Manuf_Visteon_Part_Number_val>>8);
#else 
	msg[len++] = (uint8_t)(Manuf_Visteon_Part_Number_val);
	msg[len++] = (uint8_t)(Manuf_Visteon_Part_Number_val>>8);
	msg[len++] = (uint8_t)(Manuf_Visteon_Part_Number_val>>16);
	msg[len++] = (uint8_t)(Manuf_Visteon_Part_Number_val>>24);
#endif // {EEP_CONTENT_Manuf_Visteon_Part_Number_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_Manuf_Visteon_Part_Number_ITEM_LEN > 1
void DevEep_Get_Manuf_Visteon_Part_Number(EEP_HANDLE eep_handle,uint8_t *Manuf_Visteon_Part_Number_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_Manuf_Visteon_Part_Number_ITEM_LEN; i ++)
		{
			Manuf_Visteon_Part_Number_val[i] = ap_eep->process_eep.Manuf_Visteon_Part_Number[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_Manuf_Visteon_Part_Number(EEP_HANDLE eep_handle,uint8_t const *Manuf_Visteon_Part_Number_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_Manuf_Visteon_Part_Number_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_Manuf_Visteon_Part_Number_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_Manuf_Visteon_Part_Number_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_Manuf_Visteon_Part_Number_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_Manuf_Visteon_Part_Number_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = Manuf_Visteon_Part_Number_val[i];
#elif EEP_CONTENT_Manuf_Visteon_Part_Number_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(Manuf_Visteon_Part_Number_val[i]);
		msg[len++] = (uint8_t)(Manuf_Visteon_Part_Number_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(Manuf_Visteon_Part_Number_val[i]);
		msg[len++] = (uint8_t)(Manuf_Visteon_Part_Number_val[i]>>8);
		msg[len++] = (uint8_t)(Manuf_Visteon_Part_Number_val[i]>>16);
		msg[len++] = (uint8_t)(Manuf_Visteon_Part_Number_val[i]>>24);
#endif // {EEP_CONTENT_Manuf_Visteon_Part_Number_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_Manuf_Visteon_Part_Number_ITEM_LEN}
void DevEep_Inject_ChgCalbk_Manuf_Visteon_Part_Number(EEP_HANDLE eep_handle,Manuf_Visteon_Part_Number_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_Manuf_Visteon_Part_Number_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_Manuf_Visteon_Part_Number_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_Manuf_Visteon_Part_Number(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		Manuf_Visteon_Part_Number_ChgCallBk Manuf_Visteon_Part_Number_callback =  (Manuf_Visteon_Part_Number_ChgCallBk)callbk;
		Manuf_Visteon_Part_Number_callback(ap,ap->process_eep.Manuf_Visteon_Part_Number);
	}
}

// api define for Manuf_EquippedPCB_VPN_Sub /////////////////////////////////////////////////////////
#if EEP_CONTENT_Manuf_EquippedPCB_VPN_Sub_ITEM_LEN == 1
uint8_t DevEep_Get_Manuf_EquippedPCB_VPN_Sub(EEP_HANDLE eep_handle)
{
	uint8_t Manuf_EquippedPCB_VPN_Sub_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		Manuf_EquippedPCB_VPN_Sub_val = ap_eep->process_eep.Manuf_EquippedPCB_VPN_Sub;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return Manuf_EquippedPCB_VPN_Sub_val;
}
void DevEep_Set_Manuf_EquippedPCB_VPN_Sub(EEP_HANDLE eep_handle,uint8_t const Manuf_EquippedPCB_VPN_Sub_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_Manuf_EquippedPCB_VPN_Sub_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_Manuf_EquippedPCB_VPN_Sub_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_Manuf_EquippedPCB_VPN_Sub_LIST_ID);
#if EEP_CONTENT_Manuf_EquippedPCB_VPN_Sub_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = Manuf_EquippedPCB_VPN_Sub_val;
#elif EEP_CONTENT_Manuf_EquippedPCB_VPN_Sub_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(Manuf_EquippedPCB_VPN_Sub_val);
	msg[len++] = (uint8_t)(Manuf_EquippedPCB_VPN_Sub_val>>8);
#else 
	msg[len++] = (uint8_t)(Manuf_EquippedPCB_VPN_Sub_val);
	msg[len++] = (uint8_t)(Manuf_EquippedPCB_VPN_Sub_val>>8);
	msg[len++] = (uint8_t)(Manuf_EquippedPCB_VPN_Sub_val>>16);
	msg[len++] = (uint8_t)(Manuf_EquippedPCB_VPN_Sub_val>>24);
#endif // {EEP_CONTENT_Manuf_EquippedPCB_VPN_Sub_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_Manuf_EquippedPCB_VPN_Sub_ITEM_LEN > 1
void DevEep_Get_Manuf_EquippedPCB_VPN_Sub(EEP_HANDLE eep_handle,uint8_t *Manuf_EquippedPCB_VPN_Sub_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_Manuf_EquippedPCB_VPN_Sub_ITEM_LEN; i ++)
		{
			Manuf_EquippedPCB_VPN_Sub_val[i] = ap_eep->process_eep.Manuf_EquippedPCB_VPN_Sub[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_Manuf_EquippedPCB_VPN_Sub(EEP_HANDLE eep_handle,uint8_t const *Manuf_EquippedPCB_VPN_Sub_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_Manuf_EquippedPCB_VPN_Sub_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_Manuf_EquippedPCB_VPN_Sub_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_Manuf_EquippedPCB_VPN_Sub_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_Manuf_EquippedPCB_VPN_Sub_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_Manuf_EquippedPCB_VPN_Sub_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = Manuf_EquippedPCB_VPN_Sub_val[i];
#elif EEP_CONTENT_Manuf_EquippedPCB_VPN_Sub_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(Manuf_EquippedPCB_VPN_Sub_val[i]);
		msg[len++] = (uint8_t)(Manuf_EquippedPCB_VPN_Sub_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(Manuf_EquippedPCB_VPN_Sub_val[i]);
		msg[len++] = (uint8_t)(Manuf_EquippedPCB_VPN_Sub_val[i]>>8);
		msg[len++] = (uint8_t)(Manuf_EquippedPCB_VPN_Sub_val[i]>>16);
		msg[len++] = (uint8_t)(Manuf_EquippedPCB_VPN_Sub_val[i]>>24);
#endif // {EEP_CONTENT_Manuf_EquippedPCB_VPN_Sub_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_Manuf_EquippedPCB_VPN_Sub_ITEM_LEN}
void DevEep_Inject_ChgCalbk_Manuf_EquippedPCB_VPN_Sub(EEP_HANDLE eep_handle,Manuf_EquippedPCB_VPN_Sub_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_Manuf_EquippedPCB_VPN_Sub_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_Manuf_EquippedPCB_VPN_Sub_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_Manuf_EquippedPCB_VPN_Sub(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		Manuf_EquippedPCB_VPN_Sub_ChgCallBk Manuf_EquippedPCB_VPN_Sub_callback =  (Manuf_EquippedPCB_VPN_Sub_ChgCallBk)callbk;
		Manuf_EquippedPCB_VPN_Sub_callback(ap,ap->process_eep.Manuf_EquippedPCB_VPN_Sub);
	}
}

// api define for Manuf_EquippedPCB_VPN_Main /////////////////////////////////////////////////////////
#if EEP_CONTENT_Manuf_EquippedPCB_VPN_Main_ITEM_LEN == 1
uint8_t DevEep_Get_Manuf_EquippedPCB_VPN_Main(EEP_HANDLE eep_handle)
{
	uint8_t Manuf_EquippedPCB_VPN_Main_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		Manuf_EquippedPCB_VPN_Main_val = ap_eep->process_eep.Manuf_EquippedPCB_VPN_Main;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return Manuf_EquippedPCB_VPN_Main_val;
}
void DevEep_Set_Manuf_EquippedPCB_VPN_Main(EEP_HANDLE eep_handle,uint8_t const Manuf_EquippedPCB_VPN_Main_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_Manuf_EquippedPCB_VPN_Main_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_Manuf_EquippedPCB_VPN_Main_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_Manuf_EquippedPCB_VPN_Main_LIST_ID);
#if EEP_CONTENT_Manuf_EquippedPCB_VPN_Main_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = Manuf_EquippedPCB_VPN_Main_val;
#elif EEP_CONTENT_Manuf_EquippedPCB_VPN_Main_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(Manuf_EquippedPCB_VPN_Main_val);
	msg[len++] = (uint8_t)(Manuf_EquippedPCB_VPN_Main_val>>8);
#else 
	msg[len++] = (uint8_t)(Manuf_EquippedPCB_VPN_Main_val);
	msg[len++] = (uint8_t)(Manuf_EquippedPCB_VPN_Main_val>>8);
	msg[len++] = (uint8_t)(Manuf_EquippedPCB_VPN_Main_val>>16);
	msg[len++] = (uint8_t)(Manuf_EquippedPCB_VPN_Main_val>>24);
#endif // {EEP_CONTENT_Manuf_EquippedPCB_VPN_Main_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_Manuf_EquippedPCB_VPN_Main_ITEM_LEN > 1
void DevEep_Get_Manuf_EquippedPCB_VPN_Main(EEP_HANDLE eep_handle,uint8_t *Manuf_EquippedPCB_VPN_Main_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_Manuf_EquippedPCB_VPN_Main_ITEM_LEN; i ++)
		{
			Manuf_EquippedPCB_VPN_Main_val[i] = ap_eep->process_eep.Manuf_EquippedPCB_VPN_Main[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_Manuf_EquippedPCB_VPN_Main(EEP_HANDLE eep_handle,uint8_t const *Manuf_EquippedPCB_VPN_Main_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_Manuf_EquippedPCB_VPN_Main_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_Manuf_EquippedPCB_VPN_Main_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_Manuf_EquippedPCB_VPN_Main_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_Manuf_EquippedPCB_VPN_Main_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_Manuf_EquippedPCB_VPN_Main_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = Manuf_EquippedPCB_VPN_Main_val[i];
#elif EEP_CONTENT_Manuf_EquippedPCB_VPN_Main_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(Manuf_EquippedPCB_VPN_Main_val[i]);
		msg[len++] = (uint8_t)(Manuf_EquippedPCB_VPN_Main_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(Manuf_EquippedPCB_VPN_Main_val[i]);
		msg[len++] = (uint8_t)(Manuf_EquippedPCB_VPN_Main_val[i]>>8);
		msg[len++] = (uint8_t)(Manuf_EquippedPCB_VPN_Main_val[i]>>16);
		msg[len++] = (uint8_t)(Manuf_EquippedPCB_VPN_Main_val[i]>>24);
#endif // {EEP_CONTENT_Manuf_EquippedPCB_VPN_Main_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_Manuf_EquippedPCB_VPN_Main_ITEM_LEN}
void DevEep_Inject_ChgCalbk_Manuf_EquippedPCB_VPN_Main(EEP_HANDLE eep_handle,Manuf_EquippedPCB_VPN_Main_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_Manuf_EquippedPCB_VPN_Main_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_Manuf_EquippedPCB_VPN_Main_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_Manuf_EquippedPCB_VPN_Main(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		Manuf_EquippedPCB_VPN_Main_ChgCallBk Manuf_EquippedPCB_VPN_Main_callback =  (Manuf_EquippedPCB_VPN_Main_ChgCallBk)callbk;
		Manuf_EquippedPCB_VPN_Main_callback(ap,ap->process_eep.Manuf_EquippedPCB_VPN_Main);
	}
}

// api define for Manuf_Product_Serial_Number /////////////////////////////////////////////////////////
#if EEP_CONTENT_Manuf_Product_Serial_Number_ITEM_LEN == 1
uint8_t DevEep_Get_Manuf_Product_Serial_Number(EEP_HANDLE eep_handle)
{
	uint8_t Manuf_Product_Serial_Number_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		Manuf_Product_Serial_Number_val = ap_eep->process_eep.Manuf_Product_Serial_Number;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return Manuf_Product_Serial_Number_val;
}
void DevEep_Set_Manuf_Product_Serial_Number(EEP_HANDLE eep_handle,uint8_t const Manuf_Product_Serial_Number_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_Manuf_Product_Serial_Number_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_Manuf_Product_Serial_Number_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_Manuf_Product_Serial_Number_LIST_ID);
#if EEP_CONTENT_Manuf_Product_Serial_Number_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = Manuf_Product_Serial_Number_val;
#elif EEP_CONTENT_Manuf_Product_Serial_Number_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(Manuf_Product_Serial_Number_val);
	msg[len++] = (uint8_t)(Manuf_Product_Serial_Number_val>>8);
#else 
	msg[len++] = (uint8_t)(Manuf_Product_Serial_Number_val);
	msg[len++] = (uint8_t)(Manuf_Product_Serial_Number_val>>8);
	msg[len++] = (uint8_t)(Manuf_Product_Serial_Number_val>>16);
	msg[len++] = (uint8_t)(Manuf_Product_Serial_Number_val>>24);
#endif // {EEP_CONTENT_Manuf_Product_Serial_Number_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_Manuf_Product_Serial_Number_ITEM_LEN > 1
void DevEep_Get_Manuf_Product_Serial_Number(EEP_HANDLE eep_handle,uint8_t *Manuf_Product_Serial_Number_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_Manuf_Product_Serial_Number_ITEM_LEN; i ++)
		{
			Manuf_Product_Serial_Number_val[i] = ap_eep->process_eep.Manuf_Product_Serial_Number[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_Manuf_Product_Serial_Number(EEP_HANDLE eep_handle,uint8_t const *Manuf_Product_Serial_Number_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_Manuf_Product_Serial_Number_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_Manuf_Product_Serial_Number_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_Manuf_Product_Serial_Number_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_Manuf_Product_Serial_Number_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_Manuf_Product_Serial_Number_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = Manuf_Product_Serial_Number_val[i];
#elif EEP_CONTENT_Manuf_Product_Serial_Number_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(Manuf_Product_Serial_Number_val[i]);
		msg[len++] = (uint8_t)(Manuf_Product_Serial_Number_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(Manuf_Product_Serial_Number_val[i]);
		msg[len++] = (uint8_t)(Manuf_Product_Serial_Number_val[i]>>8);
		msg[len++] = (uint8_t)(Manuf_Product_Serial_Number_val[i]>>16);
		msg[len++] = (uint8_t)(Manuf_Product_Serial_Number_val[i]>>24);
#endif // {EEP_CONTENT_Manuf_Product_Serial_Number_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_Manuf_Product_Serial_Number_ITEM_LEN}
void DevEep_Inject_ChgCalbk_Manuf_Product_Serial_Number(EEP_HANDLE eep_handle,Manuf_Product_Serial_Number_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_Manuf_Product_Serial_Number_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_Manuf_Product_Serial_Number_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_Manuf_Product_Serial_Number(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		Manuf_Product_Serial_Number_ChgCallBk Manuf_Product_Serial_Number_callback =  (Manuf_Product_Serial_Number_ChgCallBk)callbk;
		Manuf_Product_Serial_Number_callback(ap,ap->process_eep.Manuf_Product_Serial_Number);
	}
}

// api define for Manuf_EquippedPCB_Serial_Number_Sub /////////////////////////////////////////////////////////
#if EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Sub_ITEM_LEN == 1
uint8_t DevEep_Get_Manuf_EquippedPCB_Serial_Number_Sub(EEP_HANDLE eep_handle)
{
	uint8_t Manuf_EquippedPCB_Serial_Number_Sub_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		Manuf_EquippedPCB_Serial_Number_Sub_val = ap_eep->process_eep.Manuf_EquippedPCB_Serial_Number_Sub;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return Manuf_EquippedPCB_Serial_Number_Sub_val;
}
void DevEep_Set_Manuf_EquippedPCB_Serial_Number_Sub(EEP_HANDLE eep_handle,uint8_t const Manuf_EquippedPCB_Serial_Number_Sub_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Sub_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Sub_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Sub_LIST_ID);
#if EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Sub_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = Manuf_EquippedPCB_Serial_Number_Sub_val;
#elif EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Sub_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(Manuf_EquippedPCB_Serial_Number_Sub_val);
	msg[len++] = (uint8_t)(Manuf_EquippedPCB_Serial_Number_Sub_val>>8);
#else 
	msg[len++] = (uint8_t)(Manuf_EquippedPCB_Serial_Number_Sub_val);
	msg[len++] = (uint8_t)(Manuf_EquippedPCB_Serial_Number_Sub_val>>8);
	msg[len++] = (uint8_t)(Manuf_EquippedPCB_Serial_Number_Sub_val>>16);
	msg[len++] = (uint8_t)(Manuf_EquippedPCB_Serial_Number_Sub_val>>24);
#endif // {EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Sub_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Sub_ITEM_LEN > 1
void DevEep_Get_Manuf_EquippedPCB_Serial_Number_Sub(EEP_HANDLE eep_handle,uint8_t *Manuf_EquippedPCB_Serial_Number_Sub_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Sub_ITEM_LEN; i ++)
		{
			Manuf_EquippedPCB_Serial_Number_Sub_val[i] = ap_eep->process_eep.Manuf_EquippedPCB_Serial_Number_Sub[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_Manuf_EquippedPCB_Serial_Number_Sub(EEP_HANDLE eep_handle,uint8_t const *Manuf_EquippedPCB_Serial_Number_Sub_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Sub_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Sub_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Sub_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Sub_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Sub_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = Manuf_EquippedPCB_Serial_Number_Sub_val[i];
#elif EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Sub_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(Manuf_EquippedPCB_Serial_Number_Sub_val[i]);
		msg[len++] = (uint8_t)(Manuf_EquippedPCB_Serial_Number_Sub_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(Manuf_EquippedPCB_Serial_Number_Sub_val[i]);
		msg[len++] = (uint8_t)(Manuf_EquippedPCB_Serial_Number_Sub_val[i]>>8);
		msg[len++] = (uint8_t)(Manuf_EquippedPCB_Serial_Number_Sub_val[i]>>16);
		msg[len++] = (uint8_t)(Manuf_EquippedPCB_Serial_Number_Sub_val[i]>>24);
#endif // {EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Sub_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Sub_ITEM_LEN}
void DevEep_Inject_ChgCalbk_Manuf_EquippedPCB_Serial_Number_Sub(EEP_HANDLE eep_handle,Manuf_EquippedPCB_Serial_Number_Sub_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Sub_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Sub_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_Manuf_EquippedPCB_Serial_Number_Sub(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		Manuf_EquippedPCB_Serial_Number_Sub_ChgCallBk Manuf_EquippedPCB_Serial_Number_Sub_callback =  (Manuf_EquippedPCB_Serial_Number_Sub_ChgCallBk)callbk;
		Manuf_EquippedPCB_Serial_Number_Sub_callback(ap,ap->process_eep.Manuf_EquippedPCB_Serial_Number_Sub);
	}
}

// api define for Manuf_EquippedPCB_Serial_Number_Main /////////////////////////////////////////////////////////
#if EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Main_ITEM_LEN == 1
uint8_t DevEep_Get_Manuf_EquippedPCB_Serial_Number_Main(EEP_HANDLE eep_handle)
{
	uint8_t Manuf_EquippedPCB_Serial_Number_Main_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		Manuf_EquippedPCB_Serial_Number_Main_val = ap_eep->process_eep.Manuf_EquippedPCB_Serial_Number_Main;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return Manuf_EquippedPCB_Serial_Number_Main_val;
}
void DevEep_Set_Manuf_EquippedPCB_Serial_Number_Main(EEP_HANDLE eep_handle,uint8_t const Manuf_EquippedPCB_Serial_Number_Main_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Main_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Main_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Main_LIST_ID);
#if EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Main_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = Manuf_EquippedPCB_Serial_Number_Main_val;
#elif EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Main_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(Manuf_EquippedPCB_Serial_Number_Main_val);
	msg[len++] = (uint8_t)(Manuf_EquippedPCB_Serial_Number_Main_val>>8);
#else 
	msg[len++] = (uint8_t)(Manuf_EquippedPCB_Serial_Number_Main_val);
	msg[len++] = (uint8_t)(Manuf_EquippedPCB_Serial_Number_Main_val>>8);
	msg[len++] = (uint8_t)(Manuf_EquippedPCB_Serial_Number_Main_val>>16);
	msg[len++] = (uint8_t)(Manuf_EquippedPCB_Serial_Number_Main_val>>24);
#endif // {EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Main_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Main_ITEM_LEN > 1
void DevEep_Get_Manuf_EquippedPCB_Serial_Number_Main(EEP_HANDLE eep_handle,uint8_t *Manuf_EquippedPCB_Serial_Number_Main_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Main_ITEM_LEN; i ++)
		{
			Manuf_EquippedPCB_Serial_Number_Main_val[i] = ap_eep->process_eep.Manuf_EquippedPCB_Serial_Number_Main[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_Manuf_EquippedPCB_Serial_Number_Main(EEP_HANDLE eep_handle,uint8_t const *Manuf_EquippedPCB_Serial_Number_Main_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Main_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Main_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Main_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Main_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Main_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = Manuf_EquippedPCB_Serial_Number_Main_val[i];
#elif EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Main_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(Manuf_EquippedPCB_Serial_Number_Main_val[i]);
		msg[len++] = (uint8_t)(Manuf_EquippedPCB_Serial_Number_Main_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(Manuf_EquippedPCB_Serial_Number_Main_val[i]);
		msg[len++] = (uint8_t)(Manuf_EquippedPCB_Serial_Number_Main_val[i]>>8);
		msg[len++] = (uint8_t)(Manuf_EquippedPCB_Serial_Number_Main_val[i]>>16);
		msg[len++] = (uint8_t)(Manuf_EquippedPCB_Serial_Number_Main_val[i]>>24);
#endif // {EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Main_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Main_ITEM_LEN}
void DevEep_Inject_ChgCalbk_Manuf_EquippedPCB_Serial_Number_Main(EEP_HANDLE eep_handle,Manuf_EquippedPCB_Serial_Number_Main_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Main_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Main_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_Manuf_EquippedPCB_Serial_Number_Main(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		Manuf_EquippedPCB_Serial_Number_Main_ChgCallBk Manuf_EquippedPCB_Serial_Number_Main_callback =  (Manuf_EquippedPCB_Serial_Number_Main_ChgCallBk)callbk;
		Manuf_EquippedPCB_Serial_Number_Main_callback(ap,ap->process_eep.Manuf_EquippedPCB_Serial_Number_Main);
	}
}

// api define for NVM_Revision /////////////////////////////////////////////////////////
#if EEP_CONTENT_NVM_Revision_ITEM_LEN == 1
uint8_t DevEep_Get_NVM_Revision(EEP_HANDLE eep_handle)
{
	uint8_t NVM_Revision_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		NVM_Revision_val = ap_eep->process_eep.NVM_Revision;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return NVM_Revision_val;
}
void DevEep_Set_NVM_Revision(EEP_HANDLE eep_handle,uint8_t const NVM_Revision_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_NVM_Revision_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_NVM_Revision_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_NVM_Revision_LIST_ID);
#if EEP_CONTENT_NVM_Revision_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = NVM_Revision_val;
#elif EEP_CONTENT_NVM_Revision_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(NVM_Revision_val);
	msg[len++] = (uint8_t)(NVM_Revision_val>>8);
#else 
	msg[len++] = (uint8_t)(NVM_Revision_val);
	msg[len++] = (uint8_t)(NVM_Revision_val>>8);
	msg[len++] = (uint8_t)(NVM_Revision_val>>16);
	msg[len++] = (uint8_t)(NVM_Revision_val>>24);
#endif // {EEP_CONTENT_NVM_Revision_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_NVM_Revision_ITEM_LEN > 1
void DevEep_Get_NVM_Revision(EEP_HANDLE eep_handle,uint8_t *NVM_Revision_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_NVM_Revision_ITEM_LEN; i ++)
		{
			NVM_Revision_val[i] = ap_eep->process_eep.NVM_Revision[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_NVM_Revision(EEP_HANDLE eep_handle,uint8_t const *NVM_Revision_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_NVM_Revision_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_NVM_Revision_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_NVM_Revision_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_NVM_Revision_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_NVM_Revision_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = NVM_Revision_val[i];
#elif EEP_CONTENT_NVM_Revision_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(NVM_Revision_val[i]);
		msg[len++] = (uint8_t)(NVM_Revision_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(NVM_Revision_val[i]);
		msg[len++] = (uint8_t)(NVM_Revision_val[i]>>8);
		msg[len++] = (uint8_t)(NVM_Revision_val[i]>>16);
		msg[len++] = (uint8_t)(NVM_Revision_val[i]>>24);
#endif // {EEP_CONTENT_NVM_Revision_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_NVM_Revision_ITEM_LEN}
void DevEep_Inject_ChgCalbk_NVM_Revision(EEP_HANDLE eep_handle,NVM_Revision_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_NVM_Revision_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_NVM_Revision_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_NVM_Revision(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		NVM_Revision_ChgCallBk NVM_Revision_callback =  (NVM_Revision_ChgCallBk)callbk;
		NVM_Revision_callback(ap,ap->process_eep.NVM_Revision);
	}
}

// api define for Manuf_SMD_Date1 /////////////////////////////////////////////////////////
#if EEP_CONTENT_Manuf_SMD_Date1_ITEM_LEN == 1
uint8_t DevEep_Get_Manuf_SMD_Date1(EEP_HANDLE eep_handle)
{
	uint8_t Manuf_SMD_Date1_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		Manuf_SMD_Date1_val = ap_eep->process_eep.Manuf_SMD_Date1;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return Manuf_SMD_Date1_val;
}
void DevEep_Set_Manuf_SMD_Date1(EEP_HANDLE eep_handle,uint8_t const Manuf_SMD_Date1_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_Manuf_SMD_Date1_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_Manuf_SMD_Date1_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_Manuf_SMD_Date1_LIST_ID);
#if EEP_CONTENT_Manuf_SMD_Date1_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = Manuf_SMD_Date1_val;
#elif EEP_CONTENT_Manuf_SMD_Date1_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(Manuf_SMD_Date1_val);
	msg[len++] = (uint8_t)(Manuf_SMD_Date1_val>>8);
#else 
	msg[len++] = (uint8_t)(Manuf_SMD_Date1_val);
	msg[len++] = (uint8_t)(Manuf_SMD_Date1_val>>8);
	msg[len++] = (uint8_t)(Manuf_SMD_Date1_val>>16);
	msg[len++] = (uint8_t)(Manuf_SMD_Date1_val>>24);
#endif // {EEP_CONTENT_Manuf_SMD_Date1_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_Manuf_SMD_Date1_ITEM_LEN > 1
void DevEep_Get_Manuf_SMD_Date1(EEP_HANDLE eep_handle,uint8_t *Manuf_SMD_Date1_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_Manuf_SMD_Date1_ITEM_LEN; i ++)
		{
			Manuf_SMD_Date1_val[i] = ap_eep->process_eep.Manuf_SMD_Date1[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_Manuf_SMD_Date1(EEP_HANDLE eep_handle,uint8_t const *Manuf_SMD_Date1_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_Manuf_SMD_Date1_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_Manuf_SMD_Date1_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_Manuf_SMD_Date1_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_Manuf_SMD_Date1_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_Manuf_SMD_Date1_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = Manuf_SMD_Date1_val[i];
#elif EEP_CONTENT_Manuf_SMD_Date1_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(Manuf_SMD_Date1_val[i]);
		msg[len++] = (uint8_t)(Manuf_SMD_Date1_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(Manuf_SMD_Date1_val[i]);
		msg[len++] = (uint8_t)(Manuf_SMD_Date1_val[i]>>8);
		msg[len++] = (uint8_t)(Manuf_SMD_Date1_val[i]>>16);
		msg[len++] = (uint8_t)(Manuf_SMD_Date1_val[i]>>24);
#endif // {EEP_CONTENT_Manuf_SMD_Date1_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_Manuf_SMD_Date1_ITEM_LEN}
void DevEep_Inject_ChgCalbk_Manuf_SMD_Date1(EEP_HANDLE eep_handle,Manuf_SMD_Date1_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_Manuf_SMD_Date1_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_Manuf_SMD_Date1_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_Manuf_SMD_Date1(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		Manuf_SMD_Date1_ChgCallBk Manuf_SMD_Date1_callback =  (Manuf_SMD_Date1_ChgCallBk)callbk;
		Manuf_SMD_Date1_callback(ap,ap->process_eep.Manuf_SMD_Date1);
	}
}

// api define for Manuf_SMD_Date2 /////////////////////////////////////////////////////////
#if EEP_CONTENT_Manuf_SMD_Date2_ITEM_LEN == 1
uint8_t DevEep_Get_Manuf_SMD_Date2(EEP_HANDLE eep_handle)
{
	uint8_t Manuf_SMD_Date2_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		Manuf_SMD_Date2_val = ap_eep->process_eep.Manuf_SMD_Date2;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return Manuf_SMD_Date2_val;
}
void DevEep_Set_Manuf_SMD_Date2(EEP_HANDLE eep_handle,uint8_t const Manuf_SMD_Date2_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_Manuf_SMD_Date2_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_Manuf_SMD_Date2_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_Manuf_SMD_Date2_LIST_ID);
#if EEP_CONTENT_Manuf_SMD_Date2_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = Manuf_SMD_Date2_val;
#elif EEP_CONTENT_Manuf_SMD_Date2_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(Manuf_SMD_Date2_val);
	msg[len++] = (uint8_t)(Manuf_SMD_Date2_val>>8);
#else 
	msg[len++] = (uint8_t)(Manuf_SMD_Date2_val);
	msg[len++] = (uint8_t)(Manuf_SMD_Date2_val>>8);
	msg[len++] = (uint8_t)(Manuf_SMD_Date2_val>>16);
	msg[len++] = (uint8_t)(Manuf_SMD_Date2_val>>24);
#endif // {EEP_CONTENT_Manuf_SMD_Date2_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_Manuf_SMD_Date2_ITEM_LEN > 1
void DevEep_Get_Manuf_SMD_Date2(EEP_HANDLE eep_handle,uint8_t *Manuf_SMD_Date2_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_Manuf_SMD_Date2_ITEM_LEN; i ++)
		{
			Manuf_SMD_Date2_val[i] = ap_eep->process_eep.Manuf_SMD_Date2[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_Manuf_SMD_Date2(EEP_HANDLE eep_handle,uint8_t const *Manuf_SMD_Date2_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_Manuf_SMD_Date2_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_Manuf_SMD_Date2_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_Manuf_SMD_Date2_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_Manuf_SMD_Date2_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_Manuf_SMD_Date2_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = Manuf_SMD_Date2_val[i];
#elif EEP_CONTENT_Manuf_SMD_Date2_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(Manuf_SMD_Date2_val[i]);
		msg[len++] = (uint8_t)(Manuf_SMD_Date2_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(Manuf_SMD_Date2_val[i]);
		msg[len++] = (uint8_t)(Manuf_SMD_Date2_val[i]>>8);
		msg[len++] = (uint8_t)(Manuf_SMD_Date2_val[i]>>16);
		msg[len++] = (uint8_t)(Manuf_SMD_Date2_val[i]>>24);
#endif // {EEP_CONTENT_Manuf_SMD_Date2_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_Manuf_SMD_Date2_ITEM_LEN}
void DevEep_Inject_ChgCalbk_Manuf_SMD_Date2(EEP_HANDLE eep_handle,Manuf_SMD_Date2_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_Manuf_SMD_Date2_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_Manuf_SMD_Date2_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_Manuf_SMD_Date2(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		Manuf_SMD_Date2_ChgCallBk Manuf_SMD_Date2_callback =  (Manuf_SMD_Date2_ChgCallBk)callbk;
		Manuf_SMD_Date2_callback(ap,ap->process_eep.Manuf_SMD_Date2);
	}
}

// api define for Manuf_Assembly_Date /////////////////////////////////////////////////////////
#if EEP_CONTENT_Manuf_Assembly_Date_ITEM_LEN == 1
uint8_t DevEep_Get_Manuf_Assembly_Date(EEP_HANDLE eep_handle)
{
	uint8_t Manuf_Assembly_Date_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		Manuf_Assembly_Date_val = ap_eep->process_eep.Manuf_Assembly_Date;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return Manuf_Assembly_Date_val;
}
void DevEep_Set_Manuf_Assembly_Date(EEP_HANDLE eep_handle,uint8_t const Manuf_Assembly_Date_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_Manuf_Assembly_Date_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_Manuf_Assembly_Date_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_Manuf_Assembly_Date_LIST_ID);
#if EEP_CONTENT_Manuf_Assembly_Date_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = Manuf_Assembly_Date_val;
#elif EEP_CONTENT_Manuf_Assembly_Date_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(Manuf_Assembly_Date_val);
	msg[len++] = (uint8_t)(Manuf_Assembly_Date_val>>8);
#else 
	msg[len++] = (uint8_t)(Manuf_Assembly_Date_val);
	msg[len++] = (uint8_t)(Manuf_Assembly_Date_val>>8);
	msg[len++] = (uint8_t)(Manuf_Assembly_Date_val>>16);
	msg[len++] = (uint8_t)(Manuf_Assembly_Date_val>>24);
#endif // {EEP_CONTENT_Manuf_Assembly_Date_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_Manuf_Assembly_Date_ITEM_LEN > 1
void DevEep_Get_Manuf_Assembly_Date(EEP_HANDLE eep_handle,uint8_t *Manuf_Assembly_Date_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_Manuf_Assembly_Date_ITEM_LEN; i ++)
		{
			Manuf_Assembly_Date_val[i] = ap_eep->process_eep.Manuf_Assembly_Date[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_Manuf_Assembly_Date(EEP_HANDLE eep_handle,uint8_t const *Manuf_Assembly_Date_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_Manuf_Assembly_Date_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_Manuf_Assembly_Date_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_Manuf_Assembly_Date_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_Manuf_Assembly_Date_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_Manuf_Assembly_Date_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = Manuf_Assembly_Date_val[i];
#elif EEP_CONTENT_Manuf_Assembly_Date_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(Manuf_Assembly_Date_val[i]);
		msg[len++] = (uint8_t)(Manuf_Assembly_Date_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(Manuf_Assembly_Date_val[i]);
		msg[len++] = (uint8_t)(Manuf_Assembly_Date_val[i]>>8);
		msg[len++] = (uint8_t)(Manuf_Assembly_Date_val[i]>>16);
		msg[len++] = (uint8_t)(Manuf_Assembly_Date_val[i]>>24);
#endif // {EEP_CONTENT_Manuf_Assembly_Date_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_Manuf_Assembly_Date_ITEM_LEN}
void DevEep_Inject_ChgCalbk_Manuf_Assembly_Date(EEP_HANDLE eep_handle,Manuf_Assembly_Date_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_Manuf_Assembly_Date_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_Manuf_Assembly_Date_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_Manuf_Assembly_Date(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		Manuf_Assembly_Date_ChgCallBk Manuf_Assembly_Date_callback =  (Manuf_Assembly_Date_ChgCallBk)callbk;
		Manuf_Assembly_Date_callback(ap,ap->process_eep.Manuf_Assembly_Date);
	}
}

// api define for Traceability_Data /////////////////////////////////////////////////////////
#if EEP_CONTENT_Traceability_Data_ITEM_LEN == 1
uint8_t DevEep_Get_Traceability_Data(EEP_HANDLE eep_handle)
{
	uint8_t Traceability_Data_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		Traceability_Data_val = ap_eep->process_eep.Traceability_Data;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return Traceability_Data_val;
}
void DevEep_Set_Traceability_Data(EEP_HANDLE eep_handle,uint8_t const Traceability_Data_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_Traceability_Data_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_Traceability_Data_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_Traceability_Data_LIST_ID);
#if EEP_CONTENT_Traceability_Data_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = Traceability_Data_val;
#elif EEP_CONTENT_Traceability_Data_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(Traceability_Data_val);
	msg[len++] = (uint8_t)(Traceability_Data_val>>8);
#else 
	msg[len++] = (uint8_t)(Traceability_Data_val);
	msg[len++] = (uint8_t)(Traceability_Data_val>>8);
	msg[len++] = (uint8_t)(Traceability_Data_val>>16);
	msg[len++] = (uint8_t)(Traceability_Data_val>>24);
#endif // {EEP_CONTENT_Traceability_Data_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_Traceability_Data_ITEM_LEN > 1
void DevEep_Get_Traceability_Data(EEP_HANDLE eep_handle,uint8_t *Traceability_Data_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_Traceability_Data_ITEM_LEN; i ++)
		{
			Traceability_Data_val[i] = ap_eep->process_eep.Traceability_Data[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_Traceability_Data(EEP_HANDLE eep_handle,uint8_t const *Traceability_Data_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_Traceability_Data_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_Traceability_Data_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_Traceability_Data_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_Traceability_Data_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_Traceability_Data_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = Traceability_Data_val[i];
#elif EEP_CONTENT_Traceability_Data_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(Traceability_Data_val[i]);
		msg[len++] = (uint8_t)(Traceability_Data_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(Traceability_Data_val[i]);
		msg[len++] = (uint8_t)(Traceability_Data_val[i]>>8);
		msg[len++] = (uint8_t)(Traceability_Data_val[i]>>16);
		msg[len++] = (uint8_t)(Traceability_Data_val[i]>>24);
#endif // {EEP_CONTENT_Traceability_Data_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_Traceability_Data_ITEM_LEN}
void DevEep_Inject_ChgCalbk_Traceability_Data(EEP_HANDLE eep_handle,Traceability_Data_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_Traceability_Data_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_Traceability_Data_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_Traceability_Data(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		Traceability_Data_ChgCallBk Traceability_Data_callback =  (Traceability_Data_ChgCallBk)callbk;
		Traceability_Data_callback(ap,ap->process_eep.Traceability_Data);
	}
}

// api define for VehicleManufacturerSparePartNumber /////////////////////////////////////////////////////////
#if EEP_CONTENT_VehicleManufacturerSparePartNumber_ITEM_LEN == 1
uint8_t DevEep_Get_VehicleManufacturerSparePartNumber(EEP_HANDLE eep_handle)
{
	uint8_t VehicleManufacturerSparePartNumber_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		VehicleManufacturerSparePartNumber_val = ap_eep->process_eep.VehicleManufacturerSparePartNumber;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return VehicleManufacturerSparePartNumber_val;
}
void DevEep_Set_VehicleManufacturerSparePartNumber(EEP_HANDLE eep_handle,uint8_t const VehicleManufacturerSparePartNumber_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_VehicleManufacturerSparePartNumber_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_VehicleManufacturerSparePartNumber_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_VehicleManufacturerSparePartNumber_LIST_ID);
#if EEP_CONTENT_VehicleManufacturerSparePartNumber_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = VehicleManufacturerSparePartNumber_val;
#elif EEP_CONTENT_VehicleManufacturerSparePartNumber_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(VehicleManufacturerSparePartNumber_val);
	msg[len++] = (uint8_t)(VehicleManufacturerSparePartNumber_val>>8);
#else 
	msg[len++] = (uint8_t)(VehicleManufacturerSparePartNumber_val);
	msg[len++] = (uint8_t)(VehicleManufacturerSparePartNumber_val>>8);
	msg[len++] = (uint8_t)(VehicleManufacturerSparePartNumber_val>>16);
	msg[len++] = (uint8_t)(VehicleManufacturerSparePartNumber_val>>24);
#endif // {EEP_CONTENT_VehicleManufacturerSparePartNumber_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_VehicleManufacturerSparePartNumber_ITEM_LEN > 1
void DevEep_Get_VehicleManufacturerSparePartNumber(EEP_HANDLE eep_handle,uint8_t *VehicleManufacturerSparePartNumber_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_VehicleManufacturerSparePartNumber_ITEM_LEN; i ++)
		{
			VehicleManufacturerSparePartNumber_val[i] = ap_eep->process_eep.VehicleManufacturerSparePartNumber[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_VehicleManufacturerSparePartNumber(EEP_HANDLE eep_handle,uint8_t const *VehicleManufacturerSparePartNumber_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_VehicleManufacturerSparePartNumber_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_VehicleManufacturerSparePartNumber_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_VehicleManufacturerSparePartNumber_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_VehicleManufacturerSparePartNumber_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_VehicleManufacturerSparePartNumber_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = VehicleManufacturerSparePartNumber_val[i];
#elif EEP_CONTENT_VehicleManufacturerSparePartNumber_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(VehicleManufacturerSparePartNumber_val[i]);
		msg[len++] = (uint8_t)(VehicleManufacturerSparePartNumber_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(VehicleManufacturerSparePartNumber_val[i]);
		msg[len++] = (uint8_t)(VehicleManufacturerSparePartNumber_val[i]>>8);
		msg[len++] = (uint8_t)(VehicleManufacturerSparePartNumber_val[i]>>16);
		msg[len++] = (uint8_t)(VehicleManufacturerSparePartNumber_val[i]>>24);
#endif // {EEP_CONTENT_VehicleManufacturerSparePartNumber_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_VehicleManufacturerSparePartNumber_ITEM_LEN}
void DevEep_Inject_ChgCalbk_VehicleManufacturerSparePartNumber(EEP_HANDLE eep_handle,VehicleManufacturerSparePartNumber_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_VehicleManufacturerSparePartNumber_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_VehicleManufacturerSparePartNumber_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_VehicleManufacturerSparePartNumber(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		VehicleManufacturerSparePartNumber_ChgCallBk VehicleManufacturerSparePartNumber_callback =  (VehicleManufacturerSparePartNumber_ChgCallBk)callbk;
		VehicleManufacturerSparePartNumber_callback(ap,ap->process_eep.VehicleManufacturerSparePartNumber);
	}
}

// api define for VehicleManufacturerSparePartNumber_Assembly /////////////////////////////////////////////////////////
#if EEP_CONTENT_VehicleManufacturerSparePartNumber_Assembly_ITEM_LEN == 1
uint8_t DevEep_Get_VehicleManufacturerSparePartNumber_Assembly(EEP_HANDLE eep_handle)
{
	uint8_t VehicleManufacturerSparePartNumber_Assembly_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		VehicleManufacturerSparePartNumber_Assembly_val = ap_eep->process_eep.VehicleManufacturerSparePartNumber_Assembly;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return VehicleManufacturerSparePartNumber_Assembly_val;
}
void DevEep_Set_VehicleManufacturerSparePartNumber_Assembly(EEP_HANDLE eep_handle,uint8_t const VehicleManufacturerSparePartNumber_Assembly_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_VehicleManufacturerSparePartNumber_Assembly_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_VehicleManufacturerSparePartNumber_Assembly_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_VehicleManufacturerSparePartNumber_Assembly_LIST_ID);
#if EEP_CONTENT_VehicleManufacturerSparePartNumber_Assembly_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = VehicleManufacturerSparePartNumber_Assembly_val;
#elif EEP_CONTENT_VehicleManufacturerSparePartNumber_Assembly_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(VehicleManufacturerSparePartNumber_Assembly_val);
	msg[len++] = (uint8_t)(VehicleManufacturerSparePartNumber_Assembly_val>>8);
#else 
	msg[len++] = (uint8_t)(VehicleManufacturerSparePartNumber_Assembly_val);
	msg[len++] = (uint8_t)(VehicleManufacturerSparePartNumber_Assembly_val>>8);
	msg[len++] = (uint8_t)(VehicleManufacturerSparePartNumber_Assembly_val>>16);
	msg[len++] = (uint8_t)(VehicleManufacturerSparePartNumber_Assembly_val>>24);
#endif // {EEP_CONTENT_VehicleManufacturerSparePartNumber_Assembly_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_VehicleManufacturerSparePartNumber_Assembly_ITEM_LEN > 1
void DevEep_Get_VehicleManufacturerSparePartNumber_Assembly(EEP_HANDLE eep_handle,uint8_t *VehicleManufacturerSparePartNumber_Assembly_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_VehicleManufacturerSparePartNumber_Assembly_ITEM_LEN; i ++)
		{
			VehicleManufacturerSparePartNumber_Assembly_val[i] = ap_eep->process_eep.VehicleManufacturerSparePartNumber_Assembly[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_VehicleManufacturerSparePartNumber_Assembly(EEP_HANDLE eep_handle,uint8_t const *VehicleManufacturerSparePartNumber_Assembly_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_VehicleManufacturerSparePartNumber_Assembly_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_VehicleManufacturerSparePartNumber_Assembly_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_VehicleManufacturerSparePartNumber_Assembly_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_VehicleManufacturerSparePartNumber_Assembly_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_VehicleManufacturerSparePartNumber_Assembly_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = VehicleManufacturerSparePartNumber_Assembly_val[i];
#elif EEP_CONTENT_VehicleManufacturerSparePartNumber_Assembly_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(VehicleManufacturerSparePartNumber_Assembly_val[i]);
		msg[len++] = (uint8_t)(VehicleManufacturerSparePartNumber_Assembly_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(VehicleManufacturerSparePartNumber_Assembly_val[i]);
		msg[len++] = (uint8_t)(VehicleManufacturerSparePartNumber_Assembly_val[i]>>8);
		msg[len++] = (uint8_t)(VehicleManufacturerSparePartNumber_Assembly_val[i]>>16);
		msg[len++] = (uint8_t)(VehicleManufacturerSparePartNumber_Assembly_val[i]>>24);
#endif // {EEP_CONTENT_VehicleManufacturerSparePartNumber_Assembly_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_VehicleManufacturerSparePartNumber_Assembly_ITEM_LEN}
void DevEep_Inject_ChgCalbk_VehicleManufacturerSparePartNumber_Assembly(EEP_HANDLE eep_handle,VehicleManufacturerSparePartNumber_Assembly_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_VehicleManufacturerSparePartNumber_Assembly_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_VehicleManufacturerSparePartNumber_Assembly_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_VehicleManufacturerSparePartNumber_Assembly(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		VehicleManufacturerSparePartNumber_Assembly_ChgCallBk VehicleManufacturerSparePartNumber_Assembly_callback =  (VehicleManufacturerSparePartNumber_Assembly_ChgCallBk)callbk;
		VehicleManufacturerSparePartNumber_Assembly_callback(ap,ap->process_eep.VehicleManufacturerSparePartNumber_Assembly);
	}
}

// api define for Operational_Reference /////////////////////////////////////////////////////////
#if EEP_CONTENT_Operational_Reference_ITEM_LEN == 1
uint8_t DevEep_Get_Operational_Reference(EEP_HANDLE eep_handle)
{
	uint8_t Operational_Reference_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		Operational_Reference_val = ap_eep->process_eep.Operational_Reference;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return Operational_Reference_val;
}
void DevEep_Set_Operational_Reference(EEP_HANDLE eep_handle,uint8_t const Operational_Reference_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_Operational_Reference_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_Operational_Reference_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_Operational_Reference_LIST_ID);
#if EEP_CONTENT_Operational_Reference_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = Operational_Reference_val;
#elif EEP_CONTENT_Operational_Reference_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(Operational_Reference_val);
	msg[len++] = (uint8_t)(Operational_Reference_val>>8);
#else 
	msg[len++] = (uint8_t)(Operational_Reference_val);
	msg[len++] = (uint8_t)(Operational_Reference_val>>8);
	msg[len++] = (uint8_t)(Operational_Reference_val>>16);
	msg[len++] = (uint8_t)(Operational_Reference_val>>24);
#endif // {EEP_CONTENT_Operational_Reference_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_Operational_Reference_ITEM_LEN > 1
void DevEep_Get_Operational_Reference(EEP_HANDLE eep_handle,uint8_t *Operational_Reference_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_Operational_Reference_ITEM_LEN; i ++)
		{
			Operational_Reference_val[i] = ap_eep->process_eep.Operational_Reference[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_Operational_Reference(EEP_HANDLE eep_handle,uint8_t const *Operational_Reference_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_Operational_Reference_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_Operational_Reference_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_Operational_Reference_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_Operational_Reference_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_Operational_Reference_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = Operational_Reference_val[i];
#elif EEP_CONTENT_Operational_Reference_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(Operational_Reference_val[i]);
		msg[len++] = (uint8_t)(Operational_Reference_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(Operational_Reference_val[i]);
		msg[len++] = (uint8_t)(Operational_Reference_val[i]>>8);
		msg[len++] = (uint8_t)(Operational_Reference_val[i]>>16);
		msg[len++] = (uint8_t)(Operational_Reference_val[i]>>24);
#endif // {EEP_CONTENT_Operational_Reference_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_Operational_Reference_ITEM_LEN}
void DevEep_Inject_ChgCalbk_Operational_Reference(EEP_HANDLE eep_handle,Operational_Reference_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_Operational_Reference_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_Operational_Reference_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_Operational_Reference(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		Operational_Reference_ChgCallBk Operational_Reference_callback =  (Operational_Reference_ChgCallBk)callbk;
		Operational_Reference_callback(ap,ap->process_eep.Operational_Reference);
	}
}

// api define for Supplier_Number /////////////////////////////////////////////////////////
#if EEP_CONTENT_Supplier_Number_ITEM_LEN == 1
uint8_t DevEep_Get_Supplier_Number(EEP_HANDLE eep_handle)
{
	uint8_t Supplier_Number_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		Supplier_Number_val = ap_eep->process_eep.Supplier_Number;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return Supplier_Number_val;
}
void DevEep_Set_Supplier_Number(EEP_HANDLE eep_handle,uint8_t const Supplier_Number_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_Supplier_Number_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_Supplier_Number_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_Supplier_Number_LIST_ID);
#if EEP_CONTENT_Supplier_Number_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = Supplier_Number_val;
#elif EEP_CONTENT_Supplier_Number_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(Supplier_Number_val);
	msg[len++] = (uint8_t)(Supplier_Number_val>>8);
#else 
	msg[len++] = (uint8_t)(Supplier_Number_val);
	msg[len++] = (uint8_t)(Supplier_Number_val>>8);
	msg[len++] = (uint8_t)(Supplier_Number_val>>16);
	msg[len++] = (uint8_t)(Supplier_Number_val>>24);
#endif // {EEP_CONTENT_Supplier_Number_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_Supplier_Number_ITEM_LEN > 1
void DevEep_Get_Supplier_Number(EEP_HANDLE eep_handle,uint8_t *Supplier_Number_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_Supplier_Number_ITEM_LEN; i ++)
		{
			Supplier_Number_val[i] = ap_eep->process_eep.Supplier_Number[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_Supplier_Number(EEP_HANDLE eep_handle,uint8_t const *Supplier_Number_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_Supplier_Number_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_Supplier_Number_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_Supplier_Number_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_Supplier_Number_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_Supplier_Number_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = Supplier_Number_val[i];
#elif EEP_CONTENT_Supplier_Number_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(Supplier_Number_val[i]);
		msg[len++] = (uint8_t)(Supplier_Number_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(Supplier_Number_val[i]);
		msg[len++] = (uint8_t)(Supplier_Number_val[i]>>8);
		msg[len++] = (uint8_t)(Supplier_Number_val[i]>>16);
		msg[len++] = (uint8_t)(Supplier_Number_val[i]>>24);
#endif // {EEP_CONTENT_Supplier_Number_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_Supplier_Number_ITEM_LEN}
void DevEep_Inject_ChgCalbk_Supplier_Number(EEP_HANDLE eep_handle,Supplier_Number_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_Supplier_Number_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_Supplier_Number_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_Supplier_Number(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		Supplier_Number_ChgCallBk Supplier_Number_callback =  (Supplier_Number_ChgCallBk)callbk;
		Supplier_Number_callback(ap,ap->process_eep.Supplier_Number);
	}
}

// api define for ECU_Serial_Number /////////////////////////////////////////////////////////
#if EEP_CONTENT_ECU_Serial_Number_ITEM_LEN == 1
uint8_t DevEep_Get_ECU_Serial_Number(EEP_HANDLE eep_handle)
{
	uint8_t ECU_Serial_Number_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ECU_Serial_Number_val = ap_eep->process_eep.ECU_Serial_Number;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return ECU_Serial_Number_val;
}
void DevEep_Set_ECU_Serial_Number(EEP_HANDLE eep_handle,uint8_t const ECU_Serial_Number_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_ECU_Serial_Number_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_ECU_Serial_Number_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_ECU_Serial_Number_LIST_ID);
#if EEP_CONTENT_ECU_Serial_Number_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = ECU_Serial_Number_val;
#elif EEP_CONTENT_ECU_Serial_Number_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(ECU_Serial_Number_val);
	msg[len++] = (uint8_t)(ECU_Serial_Number_val>>8);
#else 
	msg[len++] = (uint8_t)(ECU_Serial_Number_val);
	msg[len++] = (uint8_t)(ECU_Serial_Number_val>>8);
	msg[len++] = (uint8_t)(ECU_Serial_Number_val>>16);
	msg[len++] = (uint8_t)(ECU_Serial_Number_val>>24);
#endif // {EEP_CONTENT_ECU_Serial_Number_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_ECU_Serial_Number_ITEM_LEN > 1
void DevEep_Get_ECU_Serial_Number(EEP_HANDLE eep_handle,uint8_t *ECU_Serial_Number_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_ECU_Serial_Number_ITEM_LEN; i ++)
		{
			ECU_Serial_Number_val[i] = ap_eep->process_eep.ECU_Serial_Number[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_ECU_Serial_Number(EEP_HANDLE eep_handle,uint8_t const *ECU_Serial_Number_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_ECU_Serial_Number_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_ECU_Serial_Number_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_ECU_Serial_Number_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_ECU_Serial_Number_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_ECU_Serial_Number_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = ECU_Serial_Number_val[i];
#elif EEP_CONTENT_ECU_Serial_Number_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(ECU_Serial_Number_val[i]);
		msg[len++] = (uint8_t)(ECU_Serial_Number_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(ECU_Serial_Number_val[i]);
		msg[len++] = (uint8_t)(ECU_Serial_Number_val[i]>>8);
		msg[len++] = (uint8_t)(ECU_Serial_Number_val[i]>>16);
		msg[len++] = (uint8_t)(ECU_Serial_Number_val[i]>>24);
#endif // {EEP_CONTENT_ECU_Serial_Number_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_ECU_Serial_Number_ITEM_LEN}
void DevEep_Inject_ChgCalbk_ECU_Serial_Number(EEP_HANDLE eep_handle,ECU_Serial_Number_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_ECU_Serial_Number_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_ECU_Serial_Number_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_ECU_Serial_Number(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		ECU_Serial_Number_ChgCallBk ECU_Serial_Number_callback =  (ECU_Serial_Number_ChgCallBk)callbk;
		ECU_Serial_Number_callback(ap,ap->process_eep.ECU_Serial_Number);
	}
}

// api define for Manufacturing_Identification_Code /////////////////////////////////////////////////////////
#if EEP_CONTENT_Manufacturing_Identification_Code_ITEM_LEN == 1
uint8_t DevEep_Get_Manufacturing_Identification_Code(EEP_HANDLE eep_handle)
{
	uint8_t Manufacturing_Identification_Code_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		Manufacturing_Identification_Code_val = ap_eep->process_eep.Manufacturing_Identification_Code;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return Manufacturing_Identification_Code_val;
}
void DevEep_Set_Manufacturing_Identification_Code(EEP_HANDLE eep_handle,uint8_t const Manufacturing_Identification_Code_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_Manufacturing_Identification_Code_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_Manufacturing_Identification_Code_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_Manufacturing_Identification_Code_LIST_ID);
#if EEP_CONTENT_Manufacturing_Identification_Code_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = Manufacturing_Identification_Code_val;
#elif EEP_CONTENT_Manufacturing_Identification_Code_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(Manufacturing_Identification_Code_val);
	msg[len++] = (uint8_t)(Manufacturing_Identification_Code_val>>8);
#else 
	msg[len++] = (uint8_t)(Manufacturing_Identification_Code_val);
	msg[len++] = (uint8_t)(Manufacturing_Identification_Code_val>>8);
	msg[len++] = (uint8_t)(Manufacturing_Identification_Code_val>>16);
	msg[len++] = (uint8_t)(Manufacturing_Identification_Code_val>>24);
#endif // {EEP_CONTENT_Manufacturing_Identification_Code_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_Manufacturing_Identification_Code_ITEM_LEN > 1
void DevEep_Get_Manufacturing_Identification_Code(EEP_HANDLE eep_handle,uint8_t *Manufacturing_Identification_Code_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_Manufacturing_Identification_Code_ITEM_LEN; i ++)
		{
			Manufacturing_Identification_Code_val[i] = ap_eep->process_eep.Manufacturing_Identification_Code[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_Manufacturing_Identification_Code(EEP_HANDLE eep_handle,uint8_t const *Manufacturing_Identification_Code_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_Manufacturing_Identification_Code_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_Manufacturing_Identification_Code_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_Manufacturing_Identification_Code_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_Manufacturing_Identification_Code_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_Manufacturing_Identification_Code_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = Manufacturing_Identification_Code_val[i];
#elif EEP_CONTENT_Manufacturing_Identification_Code_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(Manufacturing_Identification_Code_val[i]);
		msg[len++] = (uint8_t)(Manufacturing_Identification_Code_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(Manufacturing_Identification_Code_val[i]);
		msg[len++] = (uint8_t)(Manufacturing_Identification_Code_val[i]>>8);
		msg[len++] = (uint8_t)(Manufacturing_Identification_Code_val[i]>>16);
		msg[len++] = (uint8_t)(Manufacturing_Identification_Code_val[i]>>24);
#endif // {EEP_CONTENT_Manufacturing_Identification_Code_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_Manufacturing_Identification_Code_ITEM_LEN}
void DevEep_Inject_ChgCalbk_Manufacturing_Identification_Code(EEP_HANDLE eep_handle,Manufacturing_Identification_Code_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_Manufacturing_Identification_Code_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_Manufacturing_Identification_Code_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_Manufacturing_Identification_Code(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		Manufacturing_Identification_Code_ChgCallBk Manufacturing_Identification_Code_callback =  (Manufacturing_Identification_Code_ChgCallBk)callbk;
		Manufacturing_Identification_Code_callback(ap,ap->process_eep.Manufacturing_Identification_Code);
	}
}

// api define for VDIAG /////////////////////////////////////////////////////////
#if EEP_CONTENT_VDIAG_ITEM_LEN == 1
uint8_t DevEep_Get_VDIAG(EEP_HANDLE eep_handle)
{
	uint8_t VDIAG_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		VDIAG_val = ap_eep->process_eep.VDIAG;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return VDIAG_val;
}
void DevEep_Set_VDIAG(EEP_HANDLE eep_handle,uint8_t const VDIAG_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_VDIAG_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_VDIAG_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_VDIAG_LIST_ID);
#if EEP_CONTENT_VDIAG_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = VDIAG_val;
#elif EEP_CONTENT_VDIAG_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(VDIAG_val);
	msg[len++] = (uint8_t)(VDIAG_val>>8);
#else 
	msg[len++] = (uint8_t)(VDIAG_val);
	msg[len++] = (uint8_t)(VDIAG_val>>8);
	msg[len++] = (uint8_t)(VDIAG_val>>16);
	msg[len++] = (uint8_t)(VDIAG_val>>24);
#endif // {EEP_CONTENT_VDIAG_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_VDIAG_ITEM_LEN > 1
void DevEep_Get_VDIAG(EEP_HANDLE eep_handle,uint8_t *VDIAG_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_VDIAG_ITEM_LEN; i ++)
		{
			VDIAG_val[i] = ap_eep->process_eep.VDIAG[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_VDIAG(EEP_HANDLE eep_handle,uint8_t const *VDIAG_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_VDIAG_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_VDIAG_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_VDIAG_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_VDIAG_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_VDIAG_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = VDIAG_val[i];
#elif EEP_CONTENT_VDIAG_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(VDIAG_val[i]);
		msg[len++] = (uint8_t)(VDIAG_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(VDIAG_val[i]);
		msg[len++] = (uint8_t)(VDIAG_val[i]>>8);
		msg[len++] = (uint8_t)(VDIAG_val[i]>>16);
		msg[len++] = (uint8_t)(VDIAG_val[i]>>24);
#endif // {EEP_CONTENT_VDIAG_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_VDIAG_ITEM_LEN}
void DevEep_Inject_ChgCalbk_VDIAG(EEP_HANDLE eep_handle,VDIAG_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_VDIAG_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_VDIAG_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_VDIAG(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		VDIAG_ChgCallBk VDIAG_callback =  (VDIAG_ChgCallBk)callbk;
		VDIAG_callback(ap,ap->process_eep.VDIAG);
	}
}

// api define for Config_EQ1 /////////////////////////////////////////////////////////
#if EEP_CONTENT_Config_EQ1_ITEM_LEN == 1
uint8_t DevEep_Get_Config_EQ1(EEP_HANDLE eep_handle)
{
	uint8_t Config_EQ1_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		Config_EQ1_val = ap_eep->process_eep.Config_EQ1;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return Config_EQ1_val;
}
void DevEep_Set_Config_EQ1(EEP_HANDLE eep_handle,uint8_t const Config_EQ1_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_Config_EQ1_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_Config_EQ1_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_Config_EQ1_LIST_ID);
#if EEP_CONTENT_Config_EQ1_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = Config_EQ1_val;
#elif EEP_CONTENT_Config_EQ1_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(Config_EQ1_val);
	msg[len++] = (uint8_t)(Config_EQ1_val>>8);
#else 
	msg[len++] = (uint8_t)(Config_EQ1_val);
	msg[len++] = (uint8_t)(Config_EQ1_val>>8);
	msg[len++] = (uint8_t)(Config_EQ1_val>>16);
	msg[len++] = (uint8_t)(Config_EQ1_val>>24);
#endif // {EEP_CONTENT_Config_EQ1_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_Config_EQ1_ITEM_LEN > 1
void DevEep_Get_Config_EQ1(EEP_HANDLE eep_handle,uint8_t *Config_EQ1_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_Config_EQ1_ITEM_LEN; i ++)
		{
			Config_EQ1_val[i] = ap_eep->process_eep.Config_EQ1[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_Config_EQ1(EEP_HANDLE eep_handle,uint8_t const *Config_EQ1_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_Config_EQ1_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_Config_EQ1_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_Config_EQ1_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_Config_EQ1_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_Config_EQ1_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = Config_EQ1_val[i];
#elif EEP_CONTENT_Config_EQ1_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(Config_EQ1_val[i]);
		msg[len++] = (uint8_t)(Config_EQ1_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(Config_EQ1_val[i]);
		msg[len++] = (uint8_t)(Config_EQ1_val[i]>>8);
		msg[len++] = (uint8_t)(Config_EQ1_val[i]>>16);
		msg[len++] = (uint8_t)(Config_EQ1_val[i]>>24);
#endif // {EEP_CONTENT_Config_EQ1_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_Config_EQ1_ITEM_LEN}
void DevEep_Inject_ChgCalbk_Config_EQ1(EEP_HANDLE eep_handle,Config_EQ1_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_Config_EQ1_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_Config_EQ1_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_Config_EQ1(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		Config_EQ1_ChgCallBk Config_EQ1_callback =  (Config_EQ1_ChgCallBk)callbk;
		Config_EQ1_callback(ap,ap->process_eep.Config_EQ1);
	}
}

// api define for Vehicle_Type /////////////////////////////////////////////////////////
#if EEP_CONTENT_Vehicle_Type_ITEM_LEN == 1
uint8_t DevEep_Get_Vehicle_Type(EEP_HANDLE eep_handle)
{
	uint8_t Vehicle_Type_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		Vehicle_Type_val = ap_eep->process_eep.Vehicle_Type;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return Vehicle_Type_val;
}
void DevEep_Set_Vehicle_Type(EEP_HANDLE eep_handle,uint8_t const Vehicle_Type_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_Vehicle_Type_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_Vehicle_Type_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_Vehicle_Type_LIST_ID);
#if EEP_CONTENT_Vehicle_Type_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = Vehicle_Type_val;
#elif EEP_CONTENT_Vehicle_Type_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(Vehicle_Type_val);
	msg[len++] = (uint8_t)(Vehicle_Type_val>>8);
#else 
	msg[len++] = (uint8_t)(Vehicle_Type_val);
	msg[len++] = (uint8_t)(Vehicle_Type_val>>8);
	msg[len++] = (uint8_t)(Vehicle_Type_val>>16);
	msg[len++] = (uint8_t)(Vehicle_Type_val>>24);
#endif // {EEP_CONTENT_Vehicle_Type_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_Vehicle_Type_ITEM_LEN > 1
void DevEep_Get_Vehicle_Type(EEP_HANDLE eep_handle,uint8_t *Vehicle_Type_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_Vehicle_Type_ITEM_LEN; i ++)
		{
			Vehicle_Type_val[i] = ap_eep->process_eep.Vehicle_Type[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_Vehicle_Type(EEP_HANDLE eep_handle,uint8_t const *Vehicle_Type_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_Vehicle_Type_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_Vehicle_Type_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_Vehicle_Type_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_Vehicle_Type_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_Vehicle_Type_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = Vehicle_Type_val[i];
#elif EEP_CONTENT_Vehicle_Type_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(Vehicle_Type_val[i]);
		msg[len++] = (uint8_t)(Vehicle_Type_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(Vehicle_Type_val[i]);
		msg[len++] = (uint8_t)(Vehicle_Type_val[i]>>8);
		msg[len++] = (uint8_t)(Vehicle_Type_val[i]>>16);
		msg[len++] = (uint8_t)(Vehicle_Type_val[i]>>24);
#endif // {EEP_CONTENT_Vehicle_Type_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_Vehicle_Type_ITEM_LEN}
void DevEep_Inject_ChgCalbk_Vehicle_Type(EEP_HANDLE eep_handle,Vehicle_Type_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_Vehicle_Type_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_Vehicle_Type_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_Vehicle_Type(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		Vehicle_Type_ChgCallBk Vehicle_Type_callback =  (Vehicle_Type_ChgCallBk)callbk;
		Vehicle_Type_callback(ap,ap->process_eep.Vehicle_Type);
	}
}

// api define for UUID /////////////////////////////////////////////////////////
#if EEP_CONTENT_UUID_ITEM_LEN == 1
uint8_t DevEep_Get_UUID(EEP_HANDLE eep_handle)
{
	uint8_t UUID_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		UUID_val = ap_eep->process_eep.UUID;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return UUID_val;
}
void DevEep_Set_UUID(EEP_HANDLE eep_handle,uint8_t const UUID_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_UUID_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_UUID_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_UUID_LIST_ID);
#if EEP_CONTENT_UUID_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = UUID_val;
#elif EEP_CONTENT_UUID_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(UUID_val);
	msg[len++] = (uint8_t)(UUID_val>>8);
#else 
	msg[len++] = (uint8_t)(UUID_val);
	msg[len++] = (uint8_t)(UUID_val>>8);
	msg[len++] = (uint8_t)(UUID_val>>16);
	msg[len++] = (uint8_t)(UUID_val>>24);
#endif // {EEP_CONTENT_UUID_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_UUID_ITEM_LEN > 1
void DevEep_Get_UUID(EEP_HANDLE eep_handle,uint8_t *UUID_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_UUID_ITEM_LEN; i ++)
		{
			UUID_val[i] = ap_eep->process_eep.UUID[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_UUID(EEP_HANDLE eep_handle,uint8_t const *UUID_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_UUID_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_UUID_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_UUID_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_UUID_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_UUID_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = UUID_val[i];
#elif EEP_CONTENT_UUID_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(UUID_val[i]);
		msg[len++] = (uint8_t)(UUID_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(UUID_val[i]);
		msg[len++] = (uint8_t)(UUID_val[i]>>8);
		msg[len++] = (uint8_t)(UUID_val[i]>>16);
		msg[len++] = (uint8_t)(UUID_val[i]>>24);
#endif // {EEP_CONTENT_UUID_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_UUID_ITEM_LEN}
void DevEep_Inject_ChgCalbk_UUID(EEP_HANDLE eep_handle,UUID_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_UUID_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_UUID_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_UUID(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		UUID_ChgCallBk UUID_callback =  (UUID_ChgCallBk)callbk;
		UUID_callback(ap,ap->process_eep.UUID);
	}
}

// api define for NAVI_ID /////////////////////////////////////////////////////////
#if EEP_CONTENT_NAVI_ID_ITEM_LEN == 1
uint8_t DevEep_Get_NAVI_ID(EEP_HANDLE eep_handle)
{
	uint8_t NAVI_ID_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		NAVI_ID_val = ap_eep->process_eep.NAVI_ID;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return NAVI_ID_val;
}
void DevEep_Set_NAVI_ID(EEP_HANDLE eep_handle,uint8_t const NAVI_ID_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_NAVI_ID_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_NAVI_ID_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_NAVI_ID_LIST_ID);
#if EEP_CONTENT_NAVI_ID_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = NAVI_ID_val;
#elif EEP_CONTENT_NAVI_ID_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(NAVI_ID_val);
	msg[len++] = (uint8_t)(NAVI_ID_val>>8);
#else 
	msg[len++] = (uint8_t)(NAVI_ID_val);
	msg[len++] = (uint8_t)(NAVI_ID_val>>8);
	msg[len++] = (uint8_t)(NAVI_ID_val>>16);
	msg[len++] = (uint8_t)(NAVI_ID_val>>24);
#endif // {EEP_CONTENT_NAVI_ID_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_NAVI_ID_ITEM_LEN > 1
void DevEep_Get_NAVI_ID(EEP_HANDLE eep_handle,uint8_t *NAVI_ID_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_NAVI_ID_ITEM_LEN; i ++)
		{
			NAVI_ID_val[i] = ap_eep->process_eep.NAVI_ID[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_NAVI_ID(EEP_HANDLE eep_handle,uint8_t const *NAVI_ID_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_NAVI_ID_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_NAVI_ID_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_NAVI_ID_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_NAVI_ID_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_NAVI_ID_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = NAVI_ID_val[i];
#elif EEP_CONTENT_NAVI_ID_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(NAVI_ID_val[i]);
		msg[len++] = (uint8_t)(NAVI_ID_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(NAVI_ID_val[i]);
		msg[len++] = (uint8_t)(NAVI_ID_val[i]>>8);
		msg[len++] = (uint8_t)(NAVI_ID_val[i]>>16);
		msg[len++] = (uint8_t)(NAVI_ID_val[i]>>24);
#endif // {EEP_CONTENT_NAVI_ID_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_NAVI_ID_ITEM_LEN}
void DevEep_Inject_ChgCalbk_NAVI_ID(EEP_HANDLE eep_handle,NAVI_ID_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_NAVI_ID_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_NAVI_ID_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_NAVI_ID(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		NAVI_ID_ChgCallBk NAVI_ID_callback =  (NAVI_ID_ChgCallBk)callbk;
		NAVI_ID_callback(ap,ap->process_eep.NAVI_ID);
	}
}

// api define for DA_ID /////////////////////////////////////////////////////////
#if EEP_CONTENT_DA_ID_ITEM_LEN == 1
uint8_t DevEep_Get_DA_ID(EEP_HANDLE eep_handle)
{
	uint8_t DA_ID_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		DA_ID_val = ap_eep->process_eep.DA_ID;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return DA_ID_val;
}
void DevEep_Set_DA_ID(EEP_HANDLE eep_handle,uint8_t const DA_ID_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_DA_ID_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_DA_ID_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_DA_ID_LIST_ID);
#if EEP_CONTENT_DA_ID_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = DA_ID_val;
#elif EEP_CONTENT_DA_ID_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(DA_ID_val);
	msg[len++] = (uint8_t)(DA_ID_val>>8);
#else 
	msg[len++] = (uint8_t)(DA_ID_val);
	msg[len++] = (uint8_t)(DA_ID_val>>8);
	msg[len++] = (uint8_t)(DA_ID_val>>16);
	msg[len++] = (uint8_t)(DA_ID_val>>24);
#endif // {EEP_CONTENT_DA_ID_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_DA_ID_ITEM_LEN > 1
void DevEep_Get_DA_ID(EEP_HANDLE eep_handle,uint8_t *DA_ID_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_DA_ID_ITEM_LEN; i ++)
		{
			DA_ID_val[i] = ap_eep->process_eep.DA_ID[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_DA_ID(EEP_HANDLE eep_handle,uint8_t const *DA_ID_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_DA_ID_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_DA_ID_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_DA_ID_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_DA_ID_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_DA_ID_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = DA_ID_val[i];
#elif EEP_CONTENT_DA_ID_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(DA_ID_val[i]);
		msg[len++] = (uint8_t)(DA_ID_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(DA_ID_val[i]);
		msg[len++] = (uint8_t)(DA_ID_val[i]>>8);
		msg[len++] = (uint8_t)(DA_ID_val[i]>>16);
		msg[len++] = (uint8_t)(DA_ID_val[i]>>24);
#endif // {EEP_CONTENT_DA_ID_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_DA_ID_ITEM_LEN}
void DevEep_Inject_ChgCalbk_DA_ID(EEP_HANDLE eep_handle,DA_ID_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_DA_ID_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_DA_ID_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_DA_ID(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		DA_ID_ChgCallBk DA_ID_callback =  (DA_ID_ChgCallBk)callbk;
		DA_ID_callback(ap,ap->process_eep.DA_ID);
	}
}

// api define for DAMainBoardHwVer /////////////////////////////////////////////////////////
#if EEP_CONTENT_DAMainBoardHwVer_ITEM_LEN == 1
uint8_t DevEep_Get_DAMainBoardHwVer(EEP_HANDLE eep_handle)
{
	uint8_t DAMainBoardHwVer_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		DAMainBoardHwVer_val = ap_eep->process_eep.DAMainBoardHwVer;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return DAMainBoardHwVer_val;
}
void DevEep_Set_DAMainBoardHwVer(EEP_HANDLE eep_handle,uint8_t const DAMainBoardHwVer_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_DAMainBoardHwVer_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_DAMainBoardHwVer_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_DAMainBoardHwVer_LIST_ID);
#if EEP_CONTENT_DAMainBoardHwVer_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = DAMainBoardHwVer_val;
#elif EEP_CONTENT_DAMainBoardHwVer_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(DAMainBoardHwVer_val);
	msg[len++] = (uint8_t)(DAMainBoardHwVer_val>>8);
#else 
	msg[len++] = (uint8_t)(DAMainBoardHwVer_val);
	msg[len++] = (uint8_t)(DAMainBoardHwVer_val>>8);
	msg[len++] = (uint8_t)(DAMainBoardHwVer_val>>16);
	msg[len++] = (uint8_t)(DAMainBoardHwVer_val>>24);
#endif // {EEP_CONTENT_DAMainBoardHwVer_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_DAMainBoardHwVer_ITEM_LEN > 1
void DevEep_Get_DAMainBoardHwVer(EEP_HANDLE eep_handle,uint8_t *DAMainBoardHwVer_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_DAMainBoardHwVer_ITEM_LEN; i ++)
		{
			DAMainBoardHwVer_val[i] = ap_eep->process_eep.DAMainBoardHwVer[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_DAMainBoardHwVer(EEP_HANDLE eep_handle,uint8_t const *DAMainBoardHwVer_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_DAMainBoardHwVer_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_DAMainBoardHwVer_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_DAMainBoardHwVer_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_DAMainBoardHwVer_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_DAMainBoardHwVer_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = DAMainBoardHwVer_val[i];
#elif EEP_CONTENT_DAMainBoardHwVer_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(DAMainBoardHwVer_val[i]);
		msg[len++] = (uint8_t)(DAMainBoardHwVer_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(DAMainBoardHwVer_val[i]);
		msg[len++] = (uint8_t)(DAMainBoardHwVer_val[i]>>8);
		msg[len++] = (uint8_t)(DAMainBoardHwVer_val[i]>>16);
		msg[len++] = (uint8_t)(DAMainBoardHwVer_val[i]>>24);
#endif // {EEP_CONTENT_DAMainBoardHwVer_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_DAMainBoardHwVer_ITEM_LEN}
void DevEep_Inject_ChgCalbk_DAMainBoardHwVer(EEP_HANDLE eep_handle,DAMainBoardHwVer_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_DAMainBoardHwVer_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_DAMainBoardHwVer_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_DAMainBoardHwVer(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		DAMainBoardHwVer_ChgCallBk DAMainBoardHwVer_callback =  (DAMainBoardHwVer_ChgCallBk)callbk;
		DAMainBoardHwVer_callback(ap,ap->process_eep.DAMainBoardHwVer);
	}
}

// api define for SW_Version_Date_Format /////////////////////////////////////////////////////////
#if EEP_CONTENT_SW_Version_Date_Format_ITEM_LEN == 1
uint8_t DevEep_Get_SW_Version_Date_Format(EEP_HANDLE eep_handle)
{
	uint8_t SW_Version_Date_Format_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		SW_Version_Date_Format_val = ap_eep->process_eep.SW_Version_Date_Format;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return SW_Version_Date_Format_val;
}
void DevEep_Set_SW_Version_Date_Format(EEP_HANDLE eep_handle,uint8_t const SW_Version_Date_Format_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_SW_Version_Date_Format_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_SW_Version_Date_Format_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_SW_Version_Date_Format_LIST_ID);
#if EEP_CONTENT_SW_Version_Date_Format_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = SW_Version_Date_Format_val;
#elif EEP_CONTENT_SW_Version_Date_Format_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(SW_Version_Date_Format_val);
	msg[len++] = (uint8_t)(SW_Version_Date_Format_val>>8);
#else 
	msg[len++] = (uint8_t)(SW_Version_Date_Format_val);
	msg[len++] = (uint8_t)(SW_Version_Date_Format_val>>8);
	msg[len++] = (uint8_t)(SW_Version_Date_Format_val>>16);
	msg[len++] = (uint8_t)(SW_Version_Date_Format_val>>24);
#endif // {EEP_CONTENT_SW_Version_Date_Format_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_SW_Version_Date_Format_ITEM_LEN > 1
void DevEep_Get_SW_Version_Date_Format(EEP_HANDLE eep_handle,uint8_t *SW_Version_Date_Format_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_SW_Version_Date_Format_ITEM_LEN; i ++)
		{
			SW_Version_Date_Format_val[i] = ap_eep->process_eep.SW_Version_Date_Format[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_SW_Version_Date_Format(EEP_HANDLE eep_handle,uint8_t const *SW_Version_Date_Format_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_SW_Version_Date_Format_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_SW_Version_Date_Format_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_SW_Version_Date_Format_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_SW_Version_Date_Format_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_SW_Version_Date_Format_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = SW_Version_Date_Format_val[i];
#elif EEP_CONTENT_SW_Version_Date_Format_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(SW_Version_Date_Format_val[i]);
		msg[len++] = (uint8_t)(SW_Version_Date_Format_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(SW_Version_Date_Format_val[i]);
		msg[len++] = (uint8_t)(SW_Version_Date_Format_val[i]>>8);
		msg[len++] = (uint8_t)(SW_Version_Date_Format_val[i]>>16);
		msg[len++] = (uint8_t)(SW_Version_Date_Format_val[i]>>24);
#endif // {EEP_CONTENT_SW_Version_Date_Format_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_SW_Version_Date_Format_ITEM_LEN}
void DevEep_Inject_ChgCalbk_SW_Version_Date_Format(EEP_HANDLE eep_handle,SW_Version_Date_Format_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_SW_Version_Date_Format_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_SW_Version_Date_Format_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_SW_Version_Date_Format(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		SW_Version_Date_Format_ChgCallBk SW_Version_Date_Format_callback =  (SW_Version_Date_Format_ChgCallBk)callbk;
		SW_Version_Date_Format_callback(ap,ap->process_eep.SW_Version_Date_Format);
	}
}

// api define for SW_Edition_Version /////////////////////////////////////////////////////////
#if EEP_CONTENT_SW_Edition_Version_ITEM_LEN == 1
uint8_t DevEep_Get_SW_Edition_Version(EEP_HANDLE eep_handle)
{
	uint8_t SW_Edition_Version_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		SW_Edition_Version_val = ap_eep->process_eep.SW_Edition_Version;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return SW_Edition_Version_val;
}
void DevEep_Set_SW_Edition_Version(EEP_HANDLE eep_handle,uint8_t const SW_Edition_Version_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_SW_Edition_Version_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_SW_Edition_Version_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_SW_Edition_Version_LIST_ID);
#if EEP_CONTENT_SW_Edition_Version_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = SW_Edition_Version_val;
#elif EEP_CONTENT_SW_Edition_Version_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(SW_Edition_Version_val);
	msg[len++] = (uint8_t)(SW_Edition_Version_val>>8);
#else 
	msg[len++] = (uint8_t)(SW_Edition_Version_val);
	msg[len++] = (uint8_t)(SW_Edition_Version_val>>8);
	msg[len++] = (uint8_t)(SW_Edition_Version_val>>16);
	msg[len++] = (uint8_t)(SW_Edition_Version_val>>24);
#endif // {EEP_CONTENT_SW_Edition_Version_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_SW_Edition_Version_ITEM_LEN > 1
void DevEep_Get_SW_Edition_Version(EEP_HANDLE eep_handle,uint8_t *SW_Edition_Version_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_SW_Edition_Version_ITEM_LEN; i ++)
		{
			SW_Edition_Version_val[i] = ap_eep->process_eep.SW_Edition_Version[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_SW_Edition_Version(EEP_HANDLE eep_handle,uint8_t const *SW_Edition_Version_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_SW_Edition_Version_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_SW_Edition_Version_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_SW_Edition_Version_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_SW_Edition_Version_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_SW_Edition_Version_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = SW_Edition_Version_val[i];
#elif EEP_CONTENT_SW_Edition_Version_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(SW_Edition_Version_val[i]);
		msg[len++] = (uint8_t)(SW_Edition_Version_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(SW_Edition_Version_val[i]);
		msg[len++] = (uint8_t)(SW_Edition_Version_val[i]>>8);
		msg[len++] = (uint8_t)(SW_Edition_Version_val[i]>>16);
		msg[len++] = (uint8_t)(SW_Edition_Version_val[i]>>24);
#endif // {EEP_CONTENT_SW_Edition_Version_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_SW_Edition_Version_ITEM_LEN}
void DevEep_Inject_ChgCalbk_SW_Edition_Version(EEP_HANDLE eep_handle,SW_Edition_Version_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_SW_Edition_Version_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_SW_Edition_Version_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_SW_Edition_Version(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		SW_Edition_Version_ChgCallBk SW_Edition_Version_callback =  (SW_Edition_Version_ChgCallBk)callbk;
		SW_Edition_Version_callback(ap,ap->process_eep.SW_Edition_Version);
	}
}

// api define for VehicleManufacturerSparePartNumber_Nissan /////////////////////////////////////////////////////////
#if EEP_CONTENT_VehicleManufacturerSparePartNumber_Nissan_ITEM_LEN == 1
uint8_t DevEep_Get_VehicleManufacturerSparePartNumber_Nissan(EEP_HANDLE eep_handle)
{
	uint8_t VehicleManufacturerSparePartNumber_Nissan_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		VehicleManufacturerSparePartNumber_Nissan_val = ap_eep->process_eep.VehicleManufacturerSparePartNumber_Nissan;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return VehicleManufacturerSparePartNumber_Nissan_val;
}
void DevEep_Set_VehicleManufacturerSparePartNumber_Nissan(EEP_HANDLE eep_handle,uint8_t const VehicleManufacturerSparePartNumber_Nissan_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_VehicleManufacturerSparePartNumber_Nissan_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_VehicleManufacturerSparePartNumber_Nissan_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_VehicleManufacturerSparePartNumber_Nissan_LIST_ID);
#if EEP_CONTENT_VehicleManufacturerSparePartNumber_Nissan_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = VehicleManufacturerSparePartNumber_Nissan_val;
#elif EEP_CONTENT_VehicleManufacturerSparePartNumber_Nissan_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(VehicleManufacturerSparePartNumber_Nissan_val);
	msg[len++] = (uint8_t)(VehicleManufacturerSparePartNumber_Nissan_val>>8);
#else 
	msg[len++] = (uint8_t)(VehicleManufacturerSparePartNumber_Nissan_val);
	msg[len++] = (uint8_t)(VehicleManufacturerSparePartNumber_Nissan_val>>8);
	msg[len++] = (uint8_t)(VehicleManufacturerSparePartNumber_Nissan_val>>16);
	msg[len++] = (uint8_t)(VehicleManufacturerSparePartNumber_Nissan_val>>24);
#endif // {EEP_CONTENT_VehicleManufacturerSparePartNumber_Nissan_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_VehicleManufacturerSparePartNumber_Nissan_ITEM_LEN > 1
void DevEep_Get_VehicleManufacturerSparePartNumber_Nissan(EEP_HANDLE eep_handle,uint8_t *VehicleManufacturerSparePartNumber_Nissan_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_VehicleManufacturerSparePartNumber_Nissan_ITEM_LEN; i ++)
		{
			VehicleManufacturerSparePartNumber_Nissan_val[i] = ap_eep->process_eep.VehicleManufacturerSparePartNumber_Nissan[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_VehicleManufacturerSparePartNumber_Nissan(EEP_HANDLE eep_handle,uint8_t const *VehicleManufacturerSparePartNumber_Nissan_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_VehicleManufacturerSparePartNumber_Nissan_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_VehicleManufacturerSparePartNumber_Nissan_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_VehicleManufacturerSparePartNumber_Nissan_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_VehicleManufacturerSparePartNumber_Nissan_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_VehicleManufacturerSparePartNumber_Nissan_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = VehicleManufacturerSparePartNumber_Nissan_val[i];
#elif EEP_CONTENT_VehicleManufacturerSparePartNumber_Nissan_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(VehicleManufacturerSparePartNumber_Nissan_val[i]);
		msg[len++] = (uint8_t)(VehicleManufacturerSparePartNumber_Nissan_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(VehicleManufacturerSparePartNumber_Nissan_val[i]);
		msg[len++] = (uint8_t)(VehicleManufacturerSparePartNumber_Nissan_val[i]>>8);
		msg[len++] = (uint8_t)(VehicleManufacturerSparePartNumber_Nissan_val[i]>>16);
		msg[len++] = (uint8_t)(VehicleManufacturerSparePartNumber_Nissan_val[i]>>24);
#endif // {EEP_CONTENT_VehicleManufacturerSparePartNumber_Nissan_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_VehicleManufacturerSparePartNumber_Nissan_ITEM_LEN}
void DevEep_Inject_ChgCalbk_VehicleManufacturerSparePartNumber_Nissan(EEP_HANDLE eep_handle,VehicleManufacturerSparePartNumber_Nissan_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_VehicleManufacturerSparePartNumber_Nissan_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_VehicleManufacturerSparePartNumber_Nissan_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_VehicleManufacturerSparePartNumber_Nissan(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		VehicleManufacturerSparePartNumber_Nissan_ChgCallBk VehicleManufacturerSparePartNumber_Nissan_callback =  (VehicleManufacturerSparePartNumber_Nissan_ChgCallBk)callbk;
		VehicleManufacturerSparePartNumber_Nissan_callback(ap,ap->process_eep.VehicleManufacturerSparePartNumber_Nissan);
	}
}

// api define for VisteonProductPartNumber /////////////////////////////////////////////////////////
#if EEP_CONTENT_VisteonProductPartNumber_ITEM_LEN == 1
uint8_t DevEep_Get_VisteonProductPartNumber(EEP_HANDLE eep_handle)
{
	uint8_t VisteonProductPartNumber_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		VisteonProductPartNumber_val = ap_eep->process_eep.VisteonProductPartNumber;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return VisteonProductPartNumber_val;
}
void DevEep_Set_VisteonProductPartNumber(EEP_HANDLE eep_handle,uint8_t const VisteonProductPartNumber_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_VisteonProductPartNumber_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_VisteonProductPartNumber_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_VisteonProductPartNumber_LIST_ID);
#if EEP_CONTENT_VisteonProductPartNumber_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = VisteonProductPartNumber_val;
#elif EEP_CONTENT_VisteonProductPartNumber_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(VisteonProductPartNumber_val);
	msg[len++] = (uint8_t)(VisteonProductPartNumber_val>>8);
#else 
	msg[len++] = (uint8_t)(VisteonProductPartNumber_val);
	msg[len++] = (uint8_t)(VisteonProductPartNumber_val>>8);
	msg[len++] = (uint8_t)(VisteonProductPartNumber_val>>16);
	msg[len++] = (uint8_t)(VisteonProductPartNumber_val>>24);
#endif // {EEP_CONTENT_VisteonProductPartNumber_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_VisteonProductPartNumber_ITEM_LEN > 1
void DevEep_Get_VisteonProductPartNumber(EEP_HANDLE eep_handle,uint8_t *VisteonProductPartNumber_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_VisteonProductPartNumber_ITEM_LEN; i ++)
		{
			VisteonProductPartNumber_val[i] = ap_eep->process_eep.VisteonProductPartNumber[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_VisteonProductPartNumber(EEP_HANDLE eep_handle,uint8_t const *VisteonProductPartNumber_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_VisteonProductPartNumber_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_VisteonProductPartNumber_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_VisteonProductPartNumber_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_VisteonProductPartNumber_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_VisteonProductPartNumber_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = VisteonProductPartNumber_val[i];
#elif EEP_CONTENT_VisteonProductPartNumber_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(VisteonProductPartNumber_val[i]);
		msg[len++] = (uint8_t)(VisteonProductPartNumber_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(VisteonProductPartNumber_val[i]);
		msg[len++] = (uint8_t)(VisteonProductPartNumber_val[i]>>8);
		msg[len++] = (uint8_t)(VisteonProductPartNumber_val[i]>>16);
		msg[len++] = (uint8_t)(VisteonProductPartNumber_val[i]>>24);
#endif // {EEP_CONTENT_VisteonProductPartNumber_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_VisteonProductPartNumber_ITEM_LEN}
void DevEep_Inject_ChgCalbk_VisteonProductPartNumber(EEP_HANDLE eep_handle,VisteonProductPartNumber_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_VisteonProductPartNumber_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_VisteonProductPartNumber_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_VisteonProductPartNumber(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		VisteonProductPartNumber_ChgCallBk VisteonProductPartNumber_callback =  (VisteonProductPartNumber_ChgCallBk)callbk;
		VisteonProductPartNumber_callback(ap,ap->process_eep.VisteonProductPartNumber);
	}
}

// api define for EquippedPCBVisteonPartNumMainBorad /////////////////////////////////////////////////////////
#if EEP_CONTENT_EquippedPCBVisteonPartNumMainBorad_ITEM_LEN == 1
uint8_t DevEep_Get_EquippedPCBVisteonPartNumMainBorad(EEP_HANDLE eep_handle)
{
	uint8_t EquippedPCBVisteonPartNumMainBorad_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		EquippedPCBVisteonPartNumMainBorad_val = ap_eep->process_eep.EquippedPCBVisteonPartNumMainBorad;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return EquippedPCBVisteonPartNumMainBorad_val;
}
void DevEep_Set_EquippedPCBVisteonPartNumMainBorad(EEP_HANDLE eep_handle,uint8_t const EquippedPCBVisteonPartNumMainBorad_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_EquippedPCBVisteonPartNumMainBorad_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_EquippedPCBVisteonPartNumMainBorad_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_EquippedPCBVisteonPartNumMainBorad_LIST_ID);
#if EEP_CONTENT_EquippedPCBVisteonPartNumMainBorad_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = EquippedPCBVisteonPartNumMainBorad_val;
#elif EEP_CONTENT_EquippedPCBVisteonPartNumMainBorad_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(EquippedPCBVisteonPartNumMainBorad_val);
	msg[len++] = (uint8_t)(EquippedPCBVisteonPartNumMainBorad_val>>8);
#else 
	msg[len++] = (uint8_t)(EquippedPCBVisteonPartNumMainBorad_val);
	msg[len++] = (uint8_t)(EquippedPCBVisteonPartNumMainBorad_val>>8);
	msg[len++] = (uint8_t)(EquippedPCBVisteonPartNumMainBorad_val>>16);
	msg[len++] = (uint8_t)(EquippedPCBVisteonPartNumMainBorad_val>>24);
#endif // {EEP_CONTENT_EquippedPCBVisteonPartNumMainBorad_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_EquippedPCBVisteonPartNumMainBorad_ITEM_LEN > 1
void DevEep_Get_EquippedPCBVisteonPartNumMainBorad(EEP_HANDLE eep_handle,uint8_t *EquippedPCBVisteonPartNumMainBorad_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_EquippedPCBVisteonPartNumMainBorad_ITEM_LEN; i ++)
		{
			EquippedPCBVisteonPartNumMainBorad_val[i] = ap_eep->process_eep.EquippedPCBVisteonPartNumMainBorad[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_EquippedPCBVisteonPartNumMainBorad(EEP_HANDLE eep_handle,uint8_t const *EquippedPCBVisteonPartNumMainBorad_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_EquippedPCBVisteonPartNumMainBorad_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_EquippedPCBVisteonPartNumMainBorad_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_EquippedPCBVisteonPartNumMainBorad_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_EquippedPCBVisteonPartNumMainBorad_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_EquippedPCBVisteonPartNumMainBorad_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = EquippedPCBVisteonPartNumMainBorad_val[i];
#elif EEP_CONTENT_EquippedPCBVisteonPartNumMainBorad_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(EquippedPCBVisteonPartNumMainBorad_val[i]);
		msg[len++] = (uint8_t)(EquippedPCBVisteonPartNumMainBorad_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(EquippedPCBVisteonPartNumMainBorad_val[i]);
		msg[len++] = (uint8_t)(EquippedPCBVisteonPartNumMainBorad_val[i]>>8);
		msg[len++] = (uint8_t)(EquippedPCBVisteonPartNumMainBorad_val[i]>>16);
		msg[len++] = (uint8_t)(EquippedPCBVisteonPartNumMainBorad_val[i]>>24);
#endif // {EEP_CONTENT_EquippedPCBVisteonPartNumMainBorad_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_EquippedPCBVisteonPartNumMainBorad_ITEM_LEN}
void DevEep_Inject_ChgCalbk_EquippedPCBVisteonPartNumMainBorad(EEP_HANDLE eep_handle,EquippedPCBVisteonPartNumMainBorad_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_EquippedPCBVisteonPartNumMainBorad_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_EquippedPCBVisteonPartNumMainBorad_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_EquippedPCBVisteonPartNumMainBorad(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		EquippedPCBVisteonPartNumMainBorad_ChgCallBk EquippedPCBVisteonPartNumMainBorad_callback =  (EquippedPCBVisteonPartNumMainBorad_ChgCallBk)callbk;
		EquippedPCBVisteonPartNumMainBorad_callback(ap,ap->process_eep.EquippedPCBVisteonPartNumMainBorad);
	}
}

// api define for EquippedPCBVisteonPartNumSubBorad /////////////////////////////////////////////////////////
#if EEP_CONTENT_EquippedPCBVisteonPartNumSubBorad_ITEM_LEN == 1
uint8_t DevEep_Get_EquippedPCBVisteonPartNumSubBorad(EEP_HANDLE eep_handle)
{
	uint8_t EquippedPCBVisteonPartNumSubBorad_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		EquippedPCBVisteonPartNumSubBorad_val = ap_eep->process_eep.EquippedPCBVisteonPartNumSubBorad;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return EquippedPCBVisteonPartNumSubBorad_val;
}
void DevEep_Set_EquippedPCBVisteonPartNumSubBorad(EEP_HANDLE eep_handle,uint8_t const EquippedPCBVisteonPartNumSubBorad_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_EquippedPCBVisteonPartNumSubBorad_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_EquippedPCBVisteonPartNumSubBorad_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_EquippedPCBVisteonPartNumSubBorad_LIST_ID);
#if EEP_CONTENT_EquippedPCBVisteonPartNumSubBorad_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = EquippedPCBVisteonPartNumSubBorad_val;
#elif EEP_CONTENT_EquippedPCBVisteonPartNumSubBorad_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(EquippedPCBVisteonPartNumSubBorad_val);
	msg[len++] = (uint8_t)(EquippedPCBVisteonPartNumSubBorad_val>>8);
#else 
	msg[len++] = (uint8_t)(EquippedPCBVisteonPartNumSubBorad_val);
	msg[len++] = (uint8_t)(EquippedPCBVisteonPartNumSubBorad_val>>8);
	msg[len++] = (uint8_t)(EquippedPCBVisteonPartNumSubBorad_val>>16);
	msg[len++] = (uint8_t)(EquippedPCBVisteonPartNumSubBorad_val>>24);
#endif // {EEP_CONTENT_EquippedPCBVisteonPartNumSubBorad_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_EquippedPCBVisteonPartNumSubBorad_ITEM_LEN > 1
void DevEep_Get_EquippedPCBVisteonPartNumSubBorad(EEP_HANDLE eep_handle,uint8_t *EquippedPCBVisteonPartNumSubBorad_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_EquippedPCBVisteonPartNumSubBorad_ITEM_LEN; i ++)
		{
			EquippedPCBVisteonPartNumSubBorad_val[i] = ap_eep->process_eep.EquippedPCBVisteonPartNumSubBorad[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_EquippedPCBVisteonPartNumSubBorad(EEP_HANDLE eep_handle,uint8_t const *EquippedPCBVisteonPartNumSubBorad_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_EquippedPCBVisteonPartNumSubBorad_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_EquippedPCBVisteonPartNumSubBorad_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_EquippedPCBVisteonPartNumSubBorad_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_EquippedPCBVisteonPartNumSubBorad_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_EquippedPCBVisteonPartNumSubBorad_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = EquippedPCBVisteonPartNumSubBorad_val[i];
#elif EEP_CONTENT_EquippedPCBVisteonPartNumSubBorad_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(EquippedPCBVisteonPartNumSubBorad_val[i]);
		msg[len++] = (uint8_t)(EquippedPCBVisteonPartNumSubBorad_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(EquippedPCBVisteonPartNumSubBorad_val[i]);
		msg[len++] = (uint8_t)(EquippedPCBVisteonPartNumSubBorad_val[i]>>8);
		msg[len++] = (uint8_t)(EquippedPCBVisteonPartNumSubBorad_val[i]>>16);
		msg[len++] = (uint8_t)(EquippedPCBVisteonPartNumSubBorad_val[i]>>24);
#endif // {EEP_CONTENT_EquippedPCBVisteonPartNumSubBorad_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_EquippedPCBVisteonPartNumSubBorad_ITEM_LEN}
void DevEep_Inject_ChgCalbk_EquippedPCBVisteonPartNumSubBorad(EEP_HANDLE eep_handle,EquippedPCBVisteonPartNumSubBorad_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_EquippedPCBVisteonPartNumSubBorad_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_EquippedPCBVisteonPartNumSubBorad_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_EquippedPCBVisteonPartNumSubBorad(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		EquippedPCBVisteonPartNumSubBorad_ChgCallBk EquippedPCBVisteonPartNumSubBorad_callback =  (EquippedPCBVisteonPartNumSubBorad_ChgCallBk)callbk;
		EquippedPCBVisteonPartNumSubBorad_callback(ap,ap->process_eep.EquippedPCBVisteonPartNumSubBorad);
	}
}

// api define for DAUniqueID /////////////////////////////////////////////////////////
#if EEP_CONTENT_DAUniqueID_ITEM_LEN == 1
uint8_t DevEep_Get_DAUniqueID(EEP_HANDLE eep_handle)
{
	uint8_t DAUniqueID_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		DAUniqueID_val = ap_eep->process_eep.DAUniqueID;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return DAUniqueID_val;
}
void DevEep_Set_DAUniqueID(EEP_HANDLE eep_handle,uint8_t const DAUniqueID_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_DAUniqueID_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_DAUniqueID_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_DAUniqueID_LIST_ID);
#if EEP_CONTENT_DAUniqueID_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = DAUniqueID_val;
#elif EEP_CONTENT_DAUniqueID_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(DAUniqueID_val);
	msg[len++] = (uint8_t)(DAUniqueID_val>>8);
#else 
	msg[len++] = (uint8_t)(DAUniqueID_val);
	msg[len++] = (uint8_t)(DAUniqueID_val>>8);
	msg[len++] = (uint8_t)(DAUniqueID_val>>16);
	msg[len++] = (uint8_t)(DAUniqueID_val>>24);
#endif // {EEP_CONTENT_DAUniqueID_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_DAUniqueID_ITEM_LEN > 1
void DevEep_Get_DAUniqueID(EEP_HANDLE eep_handle,uint8_t *DAUniqueID_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_DAUniqueID_ITEM_LEN; i ++)
		{
			DAUniqueID_val[i] = ap_eep->process_eep.DAUniqueID[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_DAUniqueID(EEP_HANDLE eep_handle,uint8_t const *DAUniqueID_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_DAUniqueID_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_DAUniqueID_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_DAUniqueID_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_DAUniqueID_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_DAUniqueID_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = DAUniqueID_val[i];
#elif EEP_CONTENT_DAUniqueID_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(DAUniqueID_val[i]);
		msg[len++] = (uint8_t)(DAUniqueID_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(DAUniqueID_val[i]);
		msg[len++] = (uint8_t)(DAUniqueID_val[i]>>8);
		msg[len++] = (uint8_t)(DAUniqueID_val[i]>>16);
		msg[len++] = (uint8_t)(DAUniqueID_val[i]>>24);
#endif // {EEP_CONTENT_DAUniqueID_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_DAUniqueID_ITEM_LEN}
void DevEep_Inject_ChgCalbk_DAUniqueID(EEP_HANDLE eep_handle,DAUniqueID_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_DAUniqueID_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_DAUniqueID_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_DAUniqueID(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		DAUniqueID_ChgCallBk DAUniqueID_callback =  (DAUniqueID_ChgCallBk)callbk;
		DAUniqueID_callback(ap,ap->process_eep.DAUniqueID);
	}
}

// api define for ProductSerialNum /////////////////////////////////////////////////////////
#if EEP_CONTENT_ProductSerialNum_ITEM_LEN == 1
uint8_t DevEep_Get_ProductSerialNum(EEP_HANDLE eep_handle)
{
	uint8_t ProductSerialNum_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ProductSerialNum_val = ap_eep->process_eep.ProductSerialNum;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return ProductSerialNum_val;
}
void DevEep_Set_ProductSerialNum(EEP_HANDLE eep_handle,uint8_t const ProductSerialNum_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_ProductSerialNum_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_ProductSerialNum_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_ProductSerialNum_LIST_ID);
#if EEP_CONTENT_ProductSerialNum_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = ProductSerialNum_val;
#elif EEP_CONTENT_ProductSerialNum_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(ProductSerialNum_val);
	msg[len++] = (uint8_t)(ProductSerialNum_val>>8);
#else 
	msg[len++] = (uint8_t)(ProductSerialNum_val);
	msg[len++] = (uint8_t)(ProductSerialNum_val>>8);
	msg[len++] = (uint8_t)(ProductSerialNum_val>>16);
	msg[len++] = (uint8_t)(ProductSerialNum_val>>24);
#endif // {EEP_CONTENT_ProductSerialNum_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_ProductSerialNum_ITEM_LEN > 1
void DevEep_Get_ProductSerialNum(EEP_HANDLE eep_handle,uint8_t *ProductSerialNum_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_ProductSerialNum_ITEM_LEN; i ++)
		{
			ProductSerialNum_val[i] = ap_eep->process_eep.ProductSerialNum[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_ProductSerialNum(EEP_HANDLE eep_handle,uint8_t const *ProductSerialNum_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_ProductSerialNum_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_ProductSerialNum_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_ProductSerialNum_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_ProductSerialNum_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_ProductSerialNum_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = ProductSerialNum_val[i];
#elif EEP_CONTENT_ProductSerialNum_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(ProductSerialNum_val[i]);
		msg[len++] = (uint8_t)(ProductSerialNum_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(ProductSerialNum_val[i]);
		msg[len++] = (uint8_t)(ProductSerialNum_val[i]>>8);
		msg[len++] = (uint8_t)(ProductSerialNum_val[i]>>16);
		msg[len++] = (uint8_t)(ProductSerialNum_val[i]>>24);
#endif // {EEP_CONTENT_ProductSerialNum_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_ProductSerialNum_ITEM_LEN}
void DevEep_Inject_ChgCalbk_ProductSerialNum(EEP_HANDLE eep_handle,ProductSerialNum_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_ProductSerialNum_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_ProductSerialNum_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_ProductSerialNum(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		ProductSerialNum_ChgCallBk ProductSerialNum_callback =  (ProductSerialNum_ChgCallBk)callbk;
		ProductSerialNum_callback(ap,ap->process_eep.ProductSerialNum);
	}
}

// api define for ProductSerialNumMainBoard /////////////////////////////////////////////////////////
#if EEP_CONTENT_ProductSerialNumMainBoard_ITEM_LEN == 1
uint8_t DevEep_Get_ProductSerialNumMainBoard(EEP_HANDLE eep_handle)
{
	uint8_t ProductSerialNumMainBoard_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ProductSerialNumMainBoard_val = ap_eep->process_eep.ProductSerialNumMainBoard;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return ProductSerialNumMainBoard_val;
}
void DevEep_Set_ProductSerialNumMainBoard(EEP_HANDLE eep_handle,uint8_t const ProductSerialNumMainBoard_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_ProductSerialNumMainBoard_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_ProductSerialNumMainBoard_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_ProductSerialNumMainBoard_LIST_ID);
#if EEP_CONTENT_ProductSerialNumMainBoard_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = ProductSerialNumMainBoard_val;
#elif EEP_CONTENT_ProductSerialNumMainBoard_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(ProductSerialNumMainBoard_val);
	msg[len++] = (uint8_t)(ProductSerialNumMainBoard_val>>8);
#else 
	msg[len++] = (uint8_t)(ProductSerialNumMainBoard_val);
	msg[len++] = (uint8_t)(ProductSerialNumMainBoard_val>>8);
	msg[len++] = (uint8_t)(ProductSerialNumMainBoard_val>>16);
	msg[len++] = (uint8_t)(ProductSerialNumMainBoard_val>>24);
#endif // {EEP_CONTENT_ProductSerialNumMainBoard_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_ProductSerialNumMainBoard_ITEM_LEN > 1
void DevEep_Get_ProductSerialNumMainBoard(EEP_HANDLE eep_handle,uint8_t *ProductSerialNumMainBoard_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_ProductSerialNumMainBoard_ITEM_LEN; i ++)
		{
			ProductSerialNumMainBoard_val[i] = ap_eep->process_eep.ProductSerialNumMainBoard[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_ProductSerialNumMainBoard(EEP_HANDLE eep_handle,uint8_t const *ProductSerialNumMainBoard_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_ProductSerialNumMainBoard_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_ProductSerialNumMainBoard_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_ProductSerialNumMainBoard_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_ProductSerialNumMainBoard_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_ProductSerialNumMainBoard_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = ProductSerialNumMainBoard_val[i];
#elif EEP_CONTENT_ProductSerialNumMainBoard_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(ProductSerialNumMainBoard_val[i]);
		msg[len++] = (uint8_t)(ProductSerialNumMainBoard_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(ProductSerialNumMainBoard_val[i]);
		msg[len++] = (uint8_t)(ProductSerialNumMainBoard_val[i]>>8);
		msg[len++] = (uint8_t)(ProductSerialNumMainBoard_val[i]>>16);
		msg[len++] = (uint8_t)(ProductSerialNumMainBoard_val[i]>>24);
#endif // {EEP_CONTENT_ProductSerialNumMainBoard_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_ProductSerialNumMainBoard_ITEM_LEN}
void DevEep_Inject_ChgCalbk_ProductSerialNumMainBoard(EEP_HANDLE eep_handle,ProductSerialNumMainBoard_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_ProductSerialNumMainBoard_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_ProductSerialNumMainBoard_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_ProductSerialNumMainBoard(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		ProductSerialNumMainBoard_ChgCallBk ProductSerialNumMainBoard_callback =  (ProductSerialNumMainBoard_ChgCallBk)callbk;
		ProductSerialNumMainBoard_callback(ap,ap->process_eep.ProductSerialNumMainBoard);
	}
}

// api define for ProductSerialNumSubBoard /////////////////////////////////////////////////////////
#if EEP_CONTENT_ProductSerialNumSubBoard_ITEM_LEN == 1
uint8_t DevEep_Get_ProductSerialNumSubBoard(EEP_HANDLE eep_handle)
{
	uint8_t ProductSerialNumSubBoard_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ProductSerialNumSubBoard_val = ap_eep->process_eep.ProductSerialNumSubBoard;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return ProductSerialNumSubBoard_val;
}
void DevEep_Set_ProductSerialNumSubBoard(EEP_HANDLE eep_handle,uint8_t const ProductSerialNumSubBoard_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_ProductSerialNumSubBoard_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_ProductSerialNumSubBoard_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_ProductSerialNumSubBoard_LIST_ID);
#if EEP_CONTENT_ProductSerialNumSubBoard_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = ProductSerialNumSubBoard_val;
#elif EEP_CONTENT_ProductSerialNumSubBoard_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(ProductSerialNumSubBoard_val);
	msg[len++] = (uint8_t)(ProductSerialNumSubBoard_val>>8);
#else 
	msg[len++] = (uint8_t)(ProductSerialNumSubBoard_val);
	msg[len++] = (uint8_t)(ProductSerialNumSubBoard_val>>8);
	msg[len++] = (uint8_t)(ProductSerialNumSubBoard_val>>16);
	msg[len++] = (uint8_t)(ProductSerialNumSubBoard_val>>24);
#endif // {EEP_CONTENT_ProductSerialNumSubBoard_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_ProductSerialNumSubBoard_ITEM_LEN > 1
void DevEep_Get_ProductSerialNumSubBoard(EEP_HANDLE eep_handle,uint8_t *ProductSerialNumSubBoard_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_ProductSerialNumSubBoard_ITEM_LEN; i ++)
		{
			ProductSerialNumSubBoard_val[i] = ap_eep->process_eep.ProductSerialNumSubBoard[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_ProductSerialNumSubBoard(EEP_HANDLE eep_handle,uint8_t const *ProductSerialNumSubBoard_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_ProductSerialNumSubBoard_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_ProductSerialNumSubBoard_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_ProductSerialNumSubBoard_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_ProductSerialNumSubBoard_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_ProductSerialNumSubBoard_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = ProductSerialNumSubBoard_val[i];
#elif EEP_CONTENT_ProductSerialNumSubBoard_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(ProductSerialNumSubBoard_val[i]);
		msg[len++] = (uint8_t)(ProductSerialNumSubBoard_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(ProductSerialNumSubBoard_val[i]);
		msg[len++] = (uint8_t)(ProductSerialNumSubBoard_val[i]>>8);
		msg[len++] = (uint8_t)(ProductSerialNumSubBoard_val[i]>>16);
		msg[len++] = (uint8_t)(ProductSerialNumSubBoard_val[i]>>24);
#endif // {EEP_CONTENT_ProductSerialNumSubBoard_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_ProductSerialNumSubBoard_ITEM_LEN}
void DevEep_Inject_ChgCalbk_ProductSerialNumSubBoard(EEP_HANDLE eep_handle,ProductSerialNumSubBoard_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_ProductSerialNumSubBoard_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_ProductSerialNumSubBoard_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_ProductSerialNumSubBoard(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		ProductSerialNumSubBoard_ChgCallBk ProductSerialNumSubBoard_callback =  (ProductSerialNumSubBoard_ChgCallBk)callbk;
		ProductSerialNumSubBoard_callback(ap,ap->process_eep.ProductSerialNumSubBoard);
	}
}

// api define for SMDManufacturingDate /////////////////////////////////////////////////////////
#if EEP_CONTENT_SMDManufacturingDate_ITEM_LEN == 1
uint8_t DevEep_Get_SMDManufacturingDate(EEP_HANDLE eep_handle)
{
	uint8_t SMDManufacturingDate_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		SMDManufacturingDate_val = ap_eep->process_eep.SMDManufacturingDate;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return SMDManufacturingDate_val;
}
void DevEep_Set_SMDManufacturingDate(EEP_HANDLE eep_handle,uint8_t const SMDManufacturingDate_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_SMDManufacturingDate_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_SMDManufacturingDate_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_SMDManufacturingDate_LIST_ID);
#if EEP_CONTENT_SMDManufacturingDate_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = SMDManufacturingDate_val;
#elif EEP_CONTENT_SMDManufacturingDate_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(SMDManufacturingDate_val);
	msg[len++] = (uint8_t)(SMDManufacturingDate_val>>8);
#else 
	msg[len++] = (uint8_t)(SMDManufacturingDate_val);
	msg[len++] = (uint8_t)(SMDManufacturingDate_val>>8);
	msg[len++] = (uint8_t)(SMDManufacturingDate_val>>16);
	msg[len++] = (uint8_t)(SMDManufacturingDate_val>>24);
#endif // {EEP_CONTENT_SMDManufacturingDate_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_SMDManufacturingDate_ITEM_LEN > 1
void DevEep_Get_SMDManufacturingDate(EEP_HANDLE eep_handle,uint8_t *SMDManufacturingDate_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_SMDManufacturingDate_ITEM_LEN; i ++)
		{
			SMDManufacturingDate_val[i] = ap_eep->process_eep.SMDManufacturingDate[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_SMDManufacturingDate(EEP_HANDLE eep_handle,uint8_t const *SMDManufacturingDate_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_SMDManufacturingDate_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_SMDManufacturingDate_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_SMDManufacturingDate_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_SMDManufacturingDate_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_SMDManufacturingDate_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = SMDManufacturingDate_val[i];
#elif EEP_CONTENT_SMDManufacturingDate_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(SMDManufacturingDate_val[i]);
		msg[len++] = (uint8_t)(SMDManufacturingDate_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(SMDManufacturingDate_val[i]);
		msg[len++] = (uint8_t)(SMDManufacturingDate_val[i]>>8);
		msg[len++] = (uint8_t)(SMDManufacturingDate_val[i]>>16);
		msg[len++] = (uint8_t)(SMDManufacturingDate_val[i]>>24);
#endif // {EEP_CONTENT_SMDManufacturingDate_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_SMDManufacturingDate_ITEM_LEN}
void DevEep_Inject_ChgCalbk_SMDManufacturingDate(EEP_HANDLE eep_handle,SMDManufacturingDate_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_SMDManufacturingDate_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_SMDManufacturingDate_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_SMDManufacturingDate(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		SMDManufacturingDate_ChgCallBk SMDManufacturingDate_callback =  (SMDManufacturingDate_ChgCallBk)callbk;
		SMDManufacturingDate_callback(ap,ap->process_eep.SMDManufacturingDate);
	}
}

// api define for AssemblyManufacturingDate /////////////////////////////////////////////////////////
#if EEP_CONTENT_AssemblyManufacturingDate_ITEM_LEN == 1
uint8_t DevEep_Get_AssemblyManufacturingDate(EEP_HANDLE eep_handle)
{
	uint8_t AssemblyManufacturingDate_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		AssemblyManufacturingDate_val = ap_eep->process_eep.AssemblyManufacturingDate;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return AssemblyManufacturingDate_val;
}
void DevEep_Set_AssemblyManufacturingDate(EEP_HANDLE eep_handle,uint8_t const AssemblyManufacturingDate_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_AssemblyManufacturingDate_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_AssemblyManufacturingDate_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_AssemblyManufacturingDate_LIST_ID);
#if EEP_CONTENT_AssemblyManufacturingDate_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = AssemblyManufacturingDate_val;
#elif EEP_CONTENT_AssemblyManufacturingDate_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(AssemblyManufacturingDate_val);
	msg[len++] = (uint8_t)(AssemblyManufacturingDate_val>>8);
#else 
	msg[len++] = (uint8_t)(AssemblyManufacturingDate_val);
	msg[len++] = (uint8_t)(AssemblyManufacturingDate_val>>8);
	msg[len++] = (uint8_t)(AssemblyManufacturingDate_val>>16);
	msg[len++] = (uint8_t)(AssemblyManufacturingDate_val>>24);
#endif // {EEP_CONTENT_AssemblyManufacturingDate_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_AssemblyManufacturingDate_ITEM_LEN > 1
void DevEep_Get_AssemblyManufacturingDate(EEP_HANDLE eep_handle,uint8_t *AssemblyManufacturingDate_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_AssemblyManufacturingDate_ITEM_LEN; i ++)
		{
			AssemblyManufacturingDate_val[i] = ap_eep->process_eep.AssemblyManufacturingDate[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_AssemblyManufacturingDate(EEP_HANDLE eep_handle,uint8_t const *AssemblyManufacturingDate_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_AssemblyManufacturingDate_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_AssemblyManufacturingDate_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_AssemblyManufacturingDate_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_AssemblyManufacturingDate_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_AssemblyManufacturingDate_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = AssemblyManufacturingDate_val[i];
#elif EEP_CONTENT_AssemblyManufacturingDate_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(AssemblyManufacturingDate_val[i]);
		msg[len++] = (uint8_t)(AssemblyManufacturingDate_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(AssemblyManufacturingDate_val[i]);
		msg[len++] = (uint8_t)(AssemblyManufacturingDate_val[i]>>8);
		msg[len++] = (uint8_t)(AssemblyManufacturingDate_val[i]>>16);
		msg[len++] = (uint8_t)(AssemblyManufacturingDate_val[i]>>24);
#endif // {EEP_CONTENT_AssemblyManufacturingDate_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_AssemblyManufacturingDate_ITEM_LEN}
void DevEep_Inject_ChgCalbk_AssemblyManufacturingDate(EEP_HANDLE eep_handle,AssemblyManufacturingDate_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_AssemblyManufacturingDate_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_AssemblyManufacturingDate_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_AssemblyManufacturingDate(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		AssemblyManufacturingDate_ChgCallBk AssemblyManufacturingDate_callback =  (AssemblyManufacturingDate_ChgCallBk)callbk;
		AssemblyManufacturingDate_callback(ap,ap->process_eep.AssemblyManufacturingDate);
	}
}

// api define for TraceabilityBytes /////////////////////////////////////////////////////////
#if EEP_CONTENT_TraceabilityBytes_ITEM_LEN == 1
uint8_t DevEep_Get_TraceabilityBytes(EEP_HANDLE eep_handle)
{
	uint8_t TraceabilityBytes_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		TraceabilityBytes_val = ap_eep->process_eep.TraceabilityBytes;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return TraceabilityBytes_val;
}
void DevEep_Set_TraceabilityBytes(EEP_HANDLE eep_handle,uint8_t const TraceabilityBytes_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_TraceabilityBytes_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_TraceabilityBytes_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_TraceabilityBytes_LIST_ID);
#if EEP_CONTENT_TraceabilityBytes_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = TraceabilityBytes_val;
#elif EEP_CONTENT_TraceabilityBytes_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(TraceabilityBytes_val);
	msg[len++] = (uint8_t)(TraceabilityBytes_val>>8);
#else 
	msg[len++] = (uint8_t)(TraceabilityBytes_val);
	msg[len++] = (uint8_t)(TraceabilityBytes_val>>8);
	msg[len++] = (uint8_t)(TraceabilityBytes_val>>16);
	msg[len++] = (uint8_t)(TraceabilityBytes_val>>24);
#endif // {EEP_CONTENT_TraceabilityBytes_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_TraceabilityBytes_ITEM_LEN > 1
void DevEep_Get_TraceabilityBytes(EEP_HANDLE eep_handle,uint8_t *TraceabilityBytes_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_TraceabilityBytes_ITEM_LEN; i ++)
		{
			TraceabilityBytes_val[i] = ap_eep->process_eep.TraceabilityBytes[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_TraceabilityBytes(EEP_HANDLE eep_handle,uint8_t const *TraceabilityBytes_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_TraceabilityBytes_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_TraceabilityBytes_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_TraceabilityBytes_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_TraceabilityBytes_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_TraceabilityBytes_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = TraceabilityBytes_val[i];
#elif EEP_CONTENT_TraceabilityBytes_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(TraceabilityBytes_val[i]);
		msg[len++] = (uint8_t)(TraceabilityBytes_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(TraceabilityBytes_val[i]);
		msg[len++] = (uint8_t)(TraceabilityBytes_val[i]>>8);
		msg[len++] = (uint8_t)(TraceabilityBytes_val[i]>>16);
		msg[len++] = (uint8_t)(TraceabilityBytes_val[i]>>24);
#endif // {EEP_CONTENT_TraceabilityBytes_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_TraceabilityBytes_ITEM_LEN}
void DevEep_Inject_ChgCalbk_TraceabilityBytes(EEP_HANDLE eep_handle,TraceabilityBytes_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_TraceabilityBytes_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_TraceabilityBytes_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_TraceabilityBytes(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		TraceabilityBytes_ChgCallBk TraceabilityBytes_callback =  (TraceabilityBytes_ChgCallBk)callbk;
		TraceabilityBytes_callback(ap,ap->process_eep.TraceabilityBytes);
	}
}

// api define for SMDPlantNum /////////////////////////////////////////////////////////
#if EEP_CONTENT_SMDPlantNum_ITEM_LEN == 1
uint8_t DevEep_Get_SMDPlantNum(EEP_HANDLE eep_handle)
{
	uint8_t SMDPlantNum_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		SMDPlantNum_val = ap_eep->process_eep.SMDPlantNum;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return SMDPlantNum_val;
}
void DevEep_Set_SMDPlantNum(EEP_HANDLE eep_handle,uint8_t const SMDPlantNum_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_SMDPlantNum_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_SMDPlantNum_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_SMDPlantNum_LIST_ID);
#if EEP_CONTENT_SMDPlantNum_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = SMDPlantNum_val;
#elif EEP_CONTENT_SMDPlantNum_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(SMDPlantNum_val);
	msg[len++] = (uint8_t)(SMDPlantNum_val>>8);
#else 
	msg[len++] = (uint8_t)(SMDPlantNum_val);
	msg[len++] = (uint8_t)(SMDPlantNum_val>>8);
	msg[len++] = (uint8_t)(SMDPlantNum_val>>16);
	msg[len++] = (uint8_t)(SMDPlantNum_val>>24);
#endif // {EEP_CONTENT_SMDPlantNum_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_SMDPlantNum_ITEM_LEN > 1
void DevEep_Get_SMDPlantNum(EEP_HANDLE eep_handle,uint8_t *SMDPlantNum_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_SMDPlantNum_ITEM_LEN; i ++)
		{
			SMDPlantNum_val[i] = ap_eep->process_eep.SMDPlantNum[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_SMDPlantNum(EEP_HANDLE eep_handle,uint8_t const *SMDPlantNum_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_SMDPlantNum_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_SMDPlantNum_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_SMDPlantNum_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_SMDPlantNum_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_SMDPlantNum_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = SMDPlantNum_val[i];
#elif EEP_CONTENT_SMDPlantNum_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(SMDPlantNum_val[i]);
		msg[len++] = (uint8_t)(SMDPlantNum_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(SMDPlantNum_val[i]);
		msg[len++] = (uint8_t)(SMDPlantNum_val[i]>>8);
		msg[len++] = (uint8_t)(SMDPlantNum_val[i]>>16);
		msg[len++] = (uint8_t)(SMDPlantNum_val[i]>>24);
#endif // {EEP_CONTENT_SMDPlantNum_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_SMDPlantNum_ITEM_LEN}
void DevEep_Inject_ChgCalbk_SMDPlantNum(EEP_HANDLE eep_handle,SMDPlantNum_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_SMDPlantNum_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_SMDPlantNum_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_SMDPlantNum(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		SMDPlantNum_ChgCallBk SMDPlantNum_callback =  (SMDPlantNum_ChgCallBk)callbk;
		SMDPlantNum_callback(ap,ap->process_eep.SMDPlantNum);
	}
}

// api define for AssemblyPlantNum /////////////////////////////////////////////////////////
#if EEP_CONTENT_AssemblyPlantNum_ITEM_LEN == 1
uint8_t DevEep_Get_AssemblyPlantNum(EEP_HANDLE eep_handle)
{
	uint8_t AssemblyPlantNum_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		AssemblyPlantNum_val = ap_eep->process_eep.AssemblyPlantNum;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return AssemblyPlantNum_val;
}
void DevEep_Set_AssemblyPlantNum(EEP_HANDLE eep_handle,uint8_t const AssemblyPlantNum_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_AssemblyPlantNum_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_AssemblyPlantNum_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_AssemblyPlantNum_LIST_ID);
#if EEP_CONTENT_AssemblyPlantNum_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = AssemblyPlantNum_val;
#elif EEP_CONTENT_AssemblyPlantNum_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(AssemblyPlantNum_val);
	msg[len++] = (uint8_t)(AssemblyPlantNum_val>>8);
#else 
	msg[len++] = (uint8_t)(AssemblyPlantNum_val);
	msg[len++] = (uint8_t)(AssemblyPlantNum_val>>8);
	msg[len++] = (uint8_t)(AssemblyPlantNum_val>>16);
	msg[len++] = (uint8_t)(AssemblyPlantNum_val>>24);
#endif // {EEP_CONTENT_AssemblyPlantNum_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_AssemblyPlantNum_ITEM_LEN > 1
void DevEep_Get_AssemblyPlantNum(EEP_HANDLE eep_handle,uint8_t *AssemblyPlantNum_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_AssemblyPlantNum_ITEM_LEN; i ++)
		{
			AssemblyPlantNum_val[i] = ap_eep->process_eep.AssemblyPlantNum[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_AssemblyPlantNum(EEP_HANDLE eep_handle,uint8_t const *AssemblyPlantNum_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_AssemblyPlantNum_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_AssemblyPlantNum_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_AssemblyPlantNum_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_AssemblyPlantNum_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_AssemblyPlantNum_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = AssemblyPlantNum_val[i];
#elif EEP_CONTENT_AssemblyPlantNum_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(AssemblyPlantNum_val[i]);
		msg[len++] = (uint8_t)(AssemblyPlantNum_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(AssemblyPlantNum_val[i]);
		msg[len++] = (uint8_t)(AssemblyPlantNum_val[i]>>8);
		msg[len++] = (uint8_t)(AssemblyPlantNum_val[i]>>16);
		msg[len++] = (uint8_t)(AssemblyPlantNum_val[i]>>24);
#endif // {EEP_CONTENT_AssemblyPlantNum_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_AssemblyPlantNum_ITEM_LEN}
void DevEep_Inject_ChgCalbk_AssemblyPlantNum(EEP_HANDLE eep_handle,AssemblyPlantNum_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_AssemblyPlantNum_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_AssemblyPlantNum_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_AssemblyPlantNum(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		AssemblyPlantNum_ChgCallBk AssemblyPlantNum_callback =  (AssemblyPlantNum_ChgCallBk)callbk;
		AssemblyPlantNum_callback(ap,ap->process_eep.AssemblyPlantNum);
	}
}

// api define for DEM_EVENT_CAN_COM_DATA /////////////////////////////////////////////////////////
#if EEP_CONTENT_DEM_EVENT_CAN_COM_DATA_ITEM_LEN == 1
uint8_t DevEep_Get_DEM_EVENT_CAN_COM_DATA(EEP_HANDLE eep_handle)
{
	uint8_t DEM_EVENT_CAN_COM_DATA_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		DEM_EVENT_CAN_COM_DATA_val = ap_eep->process_eep.DEM_EVENT_CAN_COM_DATA;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return DEM_EVENT_CAN_COM_DATA_val;
}
void DevEep_Set_DEM_EVENT_CAN_COM_DATA(EEP_HANDLE eep_handle,uint8_t const DEM_EVENT_CAN_COM_DATA_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_DEM_EVENT_CAN_COM_DATA_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_DEM_EVENT_CAN_COM_DATA_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_DEM_EVENT_CAN_COM_DATA_LIST_ID);
#if EEP_CONTENT_DEM_EVENT_CAN_COM_DATA_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = DEM_EVENT_CAN_COM_DATA_val;
#elif EEP_CONTENT_DEM_EVENT_CAN_COM_DATA_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(DEM_EVENT_CAN_COM_DATA_val);
	msg[len++] = (uint8_t)(DEM_EVENT_CAN_COM_DATA_val>>8);
#else 
	msg[len++] = (uint8_t)(DEM_EVENT_CAN_COM_DATA_val);
	msg[len++] = (uint8_t)(DEM_EVENT_CAN_COM_DATA_val>>8);
	msg[len++] = (uint8_t)(DEM_EVENT_CAN_COM_DATA_val>>16);
	msg[len++] = (uint8_t)(DEM_EVENT_CAN_COM_DATA_val>>24);
#endif // {EEP_CONTENT_DEM_EVENT_CAN_COM_DATA_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_DEM_EVENT_CAN_COM_DATA_ITEM_LEN > 1
void DevEep_Get_DEM_EVENT_CAN_COM_DATA(EEP_HANDLE eep_handle,uint8_t *DEM_EVENT_CAN_COM_DATA_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_DEM_EVENT_CAN_COM_DATA_ITEM_LEN; i ++)
		{
			DEM_EVENT_CAN_COM_DATA_val[i] = ap_eep->process_eep.DEM_EVENT_CAN_COM_DATA[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_DEM_EVENT_CAN_COM_DATA(EEP_HANDLE eep_handle,uint8_t const *DEM_EVENT_CAN_COM_DATA_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_DEM_EVENT_CAN_COM_DATA_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_DEM_EVENT_CAN_COM_DATA_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_DEM_EVENT_CAN_COM_DATA_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_DEM_EVENT_CAN_COM_DATA_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_DEM_EVENT_CAN_COM_DATA_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = DEM_EVENT_CAN_COM_DATA_val[i];
#elif EEP_CONTENT_DEM_EVENT_CAN_COM_DATA_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(DEM_EVENT_CAN_COM_DATA_val[i]);
		msg[len++] = (uint8_t)(DEM_EVENT_CAN_COM_DATA_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(DEM_EVENT_CAN_COM_DATA_val[i]);
		msg[len++] = (uint8_t)(DEM_EVENT_CAN_COM_DATA_val[i]>>8);
		msg[len++] = (uint8_t)(DEM_EVENT_CAN_COM_DATA_val[i]>>16);
		msg[len++] = (uint8_t)(DEM_EVENT_CAN_COM_DATA_val[i]>>24);
#endif // {EEP_CONTENT_DEM_EVENT_CAN_COM_DATA_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_DEM_EVENT_CAN_COM_DATA_ITEM_LEN}
void DevEep_Inject_ChgCalbk_DEM_EVENT_CAN_COM_DATA(EEP_HANDLE eep_handle,DEM_EVENT_CAN_COM_DATA_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_DEM_EVENT_CAN_COM_DATA_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_DEM_EVENT_CAN_COM_DATA_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_DEM_EVENT_CAN_COM_DATA(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		DEM_EVENT_CAN_COM_DATA_ChgCallBk DEM_EVENT_CAN_COM_DATA_callback =  (DEM_EVENT_CAN_COM_DATA_ChgCallBk)callbk;
		DEM_EVENT_CAN_COM_DATA_callback(ap,ap->process_eep.DEM_EVENT_CAN_COM_DATA);
	}
}

// api define for DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA /////////////////////////////////////////////////////////
#if EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_LEN == 1
uint8_t DevEep_Get_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA(EEP_HANDLE eep_handle)
{
	uint8_t DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_val = ap_eep->process_eep.DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_val;
}
void DevEep_Set_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA(EEP_HANDLE eep_handle,uint8_t const DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_LIST_ID);
#if EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_val;
#elif EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_val);
	msg[len++] = (uint8_t)(DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_val>>8);
#else 
	msg[len++] = (uint8_t)(DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_val);
	msg[len++] = (uint8_t)(DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_val>>8);
	msg[len++] = (uint8_t)(DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_val>>16);
	msg[len++] = (uint8_t)(DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_val>>24);
#endif // {EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_LEN > 1
void DevEep_Get_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA(EEP_HANDLE eep_handle,uint8_t *DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_LEN; i ++)
		{
			DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_val[i] = ap_eep->process_eep.DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA(EEP_HANDLE eep_handle,uint8_t const *DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_val[i];
#elif EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_val[i]);
		msg[len++] = (uint8_t)(DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_val[i]);
		msg[len++] = (uint8_t)(DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_val[i]>>8);
		msg[len++] = (uint8_t)(DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_val[i]>>16);
		msg[len++] = (uint8_t)(DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_val[i]>>24);
#endif // {EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_LEN}
void DevEep_Inject_ChgCalbk_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA(EEP_HANDLE eep_handle,DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_ChgCallBk DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_callback =  (DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_ChgCallBk)callbk;
		DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_callback(ap,ap->process_eep.DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA);
	}
}

// api define for DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA /////////////////////////////////////////////////////////
#if EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_LEN == 1
uint8_t DevEep_Get_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA(EEP_HANDLE eep_handle)
{
	uint8_t DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_val = ap_eep->process_eep.DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_val;
}
void DevEep_Set_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA(EEP_HANDLE eep_handle,uint8_t const DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_LIST_ID);
#if EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_val;
#elif EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_val);
	msg[len++] = (uint8_t)(DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_val>>8);
#else 
	msg[len++] = (uint8_t)(DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_val);
	msg[len++] = (uint8_t)(DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_val>>8);
	msg[len++] = (uint8_t)(DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_val>>16);
	msg[len++] = (uint8_t)(DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_val>>24);
#endif // {EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_LEN > 1
void DevEep_Get_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA(EEP_HANDLE eep_handle,uint8_t *DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_LEN; i ++)
		{
			DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_val[i] = ap_eep->process_eep.DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA(EEP_HANDLE eep_handle,uint8_t const *DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_val[i];
#elif EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_val[i]);
		msg[len++] = (uint8_t)(DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_val[i]);
		msg[len++] = (uint8_t)(DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_val[i]>>8);
		msg[len++] = (uint8_t)(DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_val[i]>>16);
		msg[len++] = (uint8_t)(DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_val[i]>>24);
#endif // {EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_LEN}
void DevEep_Inject_ChgCalbk_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA(EEP_HANDLE eep_handle,DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_ChgCallBk DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_callback =  (DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_ChgCallBk)callbk;
		DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_callback(ap,ap->process_eep.DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA);
	}
}

// api define for DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA /////////////////////////////////////////////////////////
#if EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_LEN == 1
uint8_t DevEep_Get_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA(EEP_HANDLE eep_handle)
{
	uint8_t DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_val = ap_eep->process_eep.DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_val;
}
void DevEep_Set_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA(EEP_HANDLE eep_handle,uint8_t const DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_LIST_ID);
#if EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_val;
#elif EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_val);
	msg[len++] = (uint8_t)(DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_val>>8);
#else 
	msg[len++] = (uint8_t)(DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_val);
	msg[len++] = (uint8_t)(DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_val>>8);
	msg[len++] = (uint8_t)(DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_val>>16);
	msg[len++] = (uint8_t)(DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_val>>24);
#endif // {EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_LEN > 1
void DevEep_Get_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA(EEP_HANDLE eep_handle,uint8_t *DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_LEN; i ++)
		{
			DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_val[i] = ap_eep->process_eep.DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA(EEP_HANDLE eep_handle,uint8_t const *DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_val[i];
#elif EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_val[i]);
		msg[len++] = (uint8_t)(DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_val[i]);
		msg[len++] = (uint8_t)(DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_val[i]>>8);
		msg[len++] = (uint8_t)(DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_val[i]>>16);
		msg[len++] = (uint8_t)(DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_val[i]>>24);
#endif // {EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_LEN}
void DevEep_Inject_ChgCalbk_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA(EEP_HANDLE eep_handle,DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_ChgCallBk DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_callback =  (DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_ChgCallBk)callbk;
		DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_callback(ap,ap->process_eep.DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA);
	}
}

// api define for DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA /////////////////////////////////////////////////////////
#if EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_LEN == 1
uint8_t DevEep_Get_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA(EEP_HANDLE eep_handle)
{
	uint8_t DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_val = ap_eep->process_eep.DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_val;
}
void DevEep_Set_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA(EEP_HANDLE eep_handle,uint8_t const DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_LIST_ID);
#if EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_val;
#elif EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_val);
	msg[len++] = (uint8_t)(DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_val>>8);
#else 
	msg[len++] = (uint8_t)(DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_val);
	msg[len++] = (uint8_t)(DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_val>>8);
	msg[len++] = (uint8_t)(DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_val>>16);
	msg[len++] = (uint8_t)(DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_val>>24);
#endif // {EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_LEN > 1
void DevEep_Get_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA(EEP_HANDLE eep_handle,uint8_t *DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_LEN; i ++)
		{
			DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_val[i] = ap_eep->process_eep.DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA(EEP_HANDLE eep_handle,uint8_t const *DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_val[i];
#elif EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_val[i]);
		msg[len++] = (uint8_t)(DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_val[i]);
		msg[len++] = (uint8_t)(DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_val[i]>>8);
		msg[len++] = (uint8_t)(DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_val[i]>>16);
		msg[len++] = (uint8_t)(DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_val[i]>>24);
#endif // {EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_LEN}
void DevEep_Inject_ChgCalbk_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA(EEP_HANDLE eep_handle,DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_ChgCallBk DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_callback =  (DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_ChgCallBk)callbk;
		DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_callback(ap,ap->process_eep.DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA);
	}
}

// api define for DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA /////////////////////////////////////////////////////////
#if EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_LEN == 1
uint8_t DevEep_Get_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA(EEP_HANDLE eep_handle)
{
	uint8_t DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_val = ap_eep->process_eep.DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_val;
}
void DevEep_Set_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA(EEP_HANDLE eep_handle,uint8_t const DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_LIST_ID);
#if EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_val;
#elif EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_val);
	msg[len++] = (uint8_t)(DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_val>>8);
#else 
	msg[len++] = (uint8_t)(DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_val);
	msg[len++] = (uint8_t)(DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_val>>8);
	msg[len++] = (uint8_t)(DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_val>>16);
	msg[len++] = (uint8_t)(DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_val>>24);
#endif // {EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_LEN > 1
void DevEep_Get_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA(EEP_HANDLE eep_handle,uint8_t *DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_LEN; i ++)
		{
			DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_val[i] = ap_eep->process_eep.DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA(EEP_HANDLE eep_handle,uint8_t const *DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_val[i];
#elif EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_val[i]);
		msg[len++] = (uint8_t)(DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_val[i]);
		msg[len++] = (uint8_t)(DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_val[i]>>8);
		msg[len++] = (uint8_t)(DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_val[i]>>16);
		msg[len++] = (uint8_t)(DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_val[i]>>24);
#endif // {EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_LEN}
void DevEep_Inject_ChgCalbk_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA(EEP_HANDLE eep_handle,DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_ChgCallBk DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_callback =  (DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_ChgCallBk)callbk;
		DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_callback(ap,ap->process_eep.DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA);
	}
}

// api define for DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA /////////////////////////////////////////////////////////
#if EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_LEN == 1
uint8_t DevEep_Get_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA(EEP_HANDLE eep_handle)
{
	uint8_t DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_val = ap_eep->process_eep.DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_val;
}
void DevEep_Set_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA(EEP_HANDLE eep_handle,uint8_t const DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_LIST_ID);
#if EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_val;
#elif EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_val);
	msg[len++] = (uint8_t)(DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_val>>8);
#else 
	msg[len++] = (uint8_t)(DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_val);
	msg[len++] = (uint8_t)(DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_val>>8);
	msg[len++] = (uint8_t)(DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_val>>16);
	msg[len++] = (uint8_t)(DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_val>>24);
#endif // {EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_LEN > 1
void DevEep_Get_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA(EEP_HANDLE eep_handle,uint8_t *DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_LEN; i ++)
		{
			DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_val[i] = ap_eep->process_eep.DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA(EEP_HANDLE eep_handle,uint8_t const *DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_val[i];
#elif EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_val[i]);
		msg[len++] = (uint8_t)(DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_val[i]);
		msg[len++] = (uint8_t)(DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_val[i]>>8);
		msg[len++] = (uint8_t)(DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_val[i]>>16);
		msg[len++] = (uint8_t)(DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_val[i]>>24);
#endif // {EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_LEN}
void DevEep_Inject_ChgCalbk_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA(EEP_HANDLE eep_handle,DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_ChgCallBk DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_callback =  (DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_ChgCallBk)callbk;
		DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_callback(ap,ap->process_eep.DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA);
	}
}

// api define for DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA /////////////////////////////////////////////////////////
#if EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_LEN == 1
uint8_t DevEep_Get_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA(EEP_HANDLE eep_handle)
{
	uint8_t DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_val = ap_eep->process_eep.DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_val;
}
void DevEep_Set_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA(EEP_HANDLE eep_handle,uint8_t const DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_LIST_ID);
#if EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_val;
#elif EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_val);
	msg[len++] = (uint8_t)(DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_val>>8);
#else 
	msg[len++] = (uint8_t)(DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_val);
	msg[len++] = (uint8_t)(DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_val>>8);
	msg[len++] = (uint8_t)(DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_val>>16);
	msg[len++] = (uint8_t)(DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_val>>24);
#endif // {EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_LEN > 1
void DevEep_Get_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA(EEP_HANDLE eep_handle,uint8_t *DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_LEN; i ++)
		{
			DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_val[i] = ap_eep->process_eep.DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA(EEP_HANDLE eep_handle,uint8_t const *DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_val[i];
#elif EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_val[i]);
		msg[len++] = (uint8_t)(DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_val[i]);
		msg[len++] = (uint8_t)(DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_val[i]>>8);
		msg[len++] = (uint8_t)(DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_val[i]>>16);
		msg[len++] = (uint8_t)(DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_val[i]>>24);
#endif // {EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_LEN}
void DevEep_Inject_ChgCalbk_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA(EEP_HANDLE eep_handle,DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_ChgCallBk DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_callback =  (DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_ChgCallBk)callbk;
		DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_callback(ap,ap->process_eep.DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA);
	}
}

// api define for DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA /////////////////////////////////////////////////////////
#if EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_LEN == 1
uint8_t DevEep_Get_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA(EEP_HANDLE eep_handle)
{
	uint8_t DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_val = ap_eep->process_eep.DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_val;
}
void DevEep_Set_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA(EEP_HANDLE eep_handle,uint8_t const DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_LIST_ID);
#if EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_val;
#elif EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_val);
	msg[len++] = (uint8_t)(DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_val>>8);
#else 
	msg[len++] = (uint8_t)(DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_val);
	msg[len++] = (uint8_t)(DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_val>>8);
	msg[len++] = (uint8_t)(DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_val>>16);
	msg[len++] = (uint8_t)(DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_val>>24);
#endif // {EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_LEN > 1
void DevEep_Get_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA(EEP_HANDLE eep_handle,uint8_t *DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_LEN; i ++)
		{
			DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_val[i] = ap_eep->process_eep.DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA(EEP_HANDLE eep_handle,uint8_t const *DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_val[i];
#elif EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_val[i]);
		msg[len++] = (uint8_t)(DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_val[i]);
		msg[len++] = (uint8_t)(DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_val[i]>>8);
		msg[len++] = (uint8_t)(DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_val[i]>>16);
		msg[len++] = (uint8_t)(DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_val[i]>>24);
#endif // {EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_LEN}
void DevEep_Inject_ChgCalbk_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA(EEP_HANDLE eep_handle,DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_ChgCallBk DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_callback =  (DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_ChgCallBk)callbk;
		DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_callback(ap,ap->process_eep.DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA);
	}
}

// api define for DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA /////////////////////////////////////////////////////////
#if EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_ITEM_LEN == 1
uint8_t DevEep_Get_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA(EEP_HANDLE eep_handle)
{
	uint8_t DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_val = ap_eep->process_eep.DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_val;
}
void DevEep_Set_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA(EEP_HANDLE eep_handle,uint8_t const DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_LIST_ID);
#if EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_val;
#elif EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_val);
	msg[len++] = (uint8_t)(DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_val>>8);
#else 
	msg[len++] = (uint8_t)(DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_val);
	msg[len++] = (uint8_t)(DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_val>>8);
	msg[len++] = (uint8_t)(DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_val>>16);
	msg[len++] = (uint8_t)(DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_val>>24);
#endif // {EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_ITEM_LEN > 1
void DevEep_Get_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA(EEP_HANDLE eep_handle,uint8_t *DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_ITEM_LEN; i ++)
		{
			DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_val[i] = ap_eep->process_eep.DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA(EEP_HANDLE eep_handle,uint8_t const *DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_val[i];
#elif EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_val[i]);
		msg[len++] = (uint8_t)(DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_val[i]);
		msg[len++] = (uint8_t)(DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_val[i]>>8);
		msg[len++] = (uint8_t)(DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_val[i]>>16);
		msg[len++] = (uint8_t)(DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_val[i]>>24);
#endif // {EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_ITEM_LEN}
void DevEep_Inject_ChgCalbk_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA(EEP_HANDLE eep_handle,DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_ChgCallBk DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_callback =  (DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_ChgCallBk)callbk;
		DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_callback(ap,ap->process_eep.DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA);
	}
}

// api define for DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA /////////////////////////////////////////////////////////
#if EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_ITEM_LEN == 1
uint8_t DevEep_Get_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA(EEP_HANDLE eep_handle)
{
	uint8_t DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_val = ap_eep->process_eep.DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_val;
}
void DevEep_Set_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA(EEP_HANDLE eep_handle,uint8_t const DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_LIST_ID);
#if EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_val;
#elif EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_val);
	msg[len++] = (uint8_t)(DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_val>>8);
#else 
	msg[len++] = (uint8_t)(DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_val);
	msg[len++] = (uint8_t)(DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_val>>8);
	msg[len++] = (uint8_t)(DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_val>>16);
	msg[len++] = (uint8_t)(DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_val>>24);
#endif // {EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_ITEM_LEN > 1
void DevEep_Get_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA(EEP_HANDLE eep_handle,uint8_t *DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_ITEM_LEN; i ++)
		{
			DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_val[i] = ap_eep->process_eep.DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA(EEP_HANDLE eep_handle,uint8_t const *DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_val[i];
#elif EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_val[i]);
		msg[len++] = (uint8_t)(DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_val[i]);
		msg[len++] = (uint8_t)(DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_val[i]>>8);
		msg[len++] = (uint8_t)(DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_val[i]>>16);
		msg[len++] = (uint8_t)(DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_val[i]>>24);
#endif // {EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_ITEM_LEN}
void DevEep_Inject_ChgCalbk_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA(EEP_HANDLE eep_handle,DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_ChgCallBk DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_callback =  (DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_ChgCallBk)callbk;
		DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_callback(ap,ap->process_eep.DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA);
	}
}

// api define for DEM_EVENT_HVAC_PANEL_CONNECTION_DATA /////////////////////////////////////////////////////////
#if EEP_CONTENT_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_ITEM_LEN == 1
uint8_t DevEep_Get_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA(EEP_HANDLE eep_handle)
{
	uint8_t DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_val = ap_eep->process_eep.DEM_EVENT_HVAC_PANEL_CONNECTION_DATA;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_val;
}
void DevEep_Set_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA(EEP_HANDLE eep_handle,uint8_t const DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_LIST_ID);
#if EEP_CONTENT_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_val;
#elif EEP_CONTENT_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_val);
	msg[len++] = (uint8_t)(DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_val>>8);
#else 
	msg[len++] = (uint8_t)(DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_val);
	msg[len++] = (uint8_t)(DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_val>>8);
	msg[len++] = (uint8_t)(DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_val>>16);
	msg[len++] = (uint8_t)(DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_val>>24);
#endif // {EEP_CONTENT_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_ITEM_LEN > 1
void DevEep_Get_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA(EEP_HANDLE eep_handle,uint8_t *DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_ITEM_LEN; i ++)
		{
			DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_val[i] = ap_eep->process_eep.DEM_EVENT_HVAC_PANEL_CONNECTION_DATA[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA(EEP_HANDLE eep_handle,uint8_t const *DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_val[i];
#elif EEP_CONTENT_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_val[i]);
		msg[len++] = (uint8_t)(DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_val[i]);
		msg[len++] = (uint8_t)(DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_val[i]>>8);
		msg[len++] = (uint8_t)(DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_val[i]>>16);
		msg[len++] = (uint8_t)(DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_val[i]>>24);
#endif // {EEP_CONTENT_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_ITEM_LEN}
void DevEep_Inject_ChgCalbk_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA(EEP_HANDLE eep_handle,DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_ChgCallBk DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_callback =  (DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_ChgCallBk)callbk;
		DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_callback(ap,ap->process_eep.DEM_EVENT_HVAC_PANEL_CONNECTION_DATA);
	}
}

// api define for TachoUpValue /////////////////////////////////////////////////////////
#if EEP_CONTENT_TachoUpValue_ITEM_LEN == 1
uint16_t DevEep_Get_TachoUpValue(EEP_HANDLE eep_handle)
{
	uint16_t TachoUpValue_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		TachoUpValue_val = ap_eep->process_eep.TachoUpValue;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return TachoUpValue_val;
}
void DevEep_Set_TachoUpValue(EEP_HANDLE eep_handle,uint16_t const TachoUpValue_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_TachoUpValue_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_TachoUpValue_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_TachoUpValue_LIST_ID);
#if EEP_CONTENT_TachoUpValue_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = TachoUpValue_val;
#elif EEP_CONTENT_TachoUpValue_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(TachoUpValue_val);
	msg[len++] = (uint8_t)(TachoUpValue_val>>8);
#else 
	msg[len++] = (uint8_t)(TachoUpValue_val);
	msg[len++] = (uint8_t)(TachoUpValue_val>>8);
	msg[len++] = (uint8_t)(TachoUpValue_val>>16);
	msg[len++] = (uint8_t)(TachoUpValue_val>>24);
#endif // {EEP_CONTENT_TachoUpValue_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_TachoUpValue_ITEM_LEN > 1
void DevEep_Get_TachoUpValue(EEP_HANDLE eep_handle,uint16_t *TachoUpValue_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_TachoUpValue_ITEM_LEN; i ++)
		{
			TachoUpValue_val[i] = ap_eep->process_eep.TachoUpValue[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_TachoUpValue(EEP_HANDLE eep_handle,uint16_t const *TachoUpValue_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_TachoUpValue_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_TachoUpValue_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_TachoUpValue_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_TachoUpValue_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_TachoUpValue_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = TachoUpValue_val[i];
#elif EEP_CONTENT_TachoUpValue_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(TachoUpValue_val[i]);
		msg[len++] = (uint8_t)(TachoUpValue_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(TachoUpValue_val[i]);
		msg[len++] = (uint8_t)(TachoUpValue_val[i]>>8);
		msg[len++] = (uint8_t)(TachoUpValue_val[i]>>16);
		msg[len++] = (uint8_t)(TachoUpValue_val[i]>>24);
#endif // {EEP_CONTENT_TachoUpValue_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_TachoUpValue_ITEM_LEN}
void DevEep_Inject_ChgCalbk_TachoUpValue(EEP_HANDLE eep_handle,TachoUpValue_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_TachoUpValue_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_TachoUpValue_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_TachoUpValue(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		TachoUpValue_ChgCallBk TachoUpValue_callback =  (TachoUpValue_ChgCallBk)callbk;
		TachoUpValue_callback(ap,ap->process_eep.TachoUpValue);
	}
}

// api define for TachoLowValue /////////////////////////////////////////////////////////
#if EEP_CONTENT_TachoLowValue_ITEM_LEN == 1
uint16_t DevEep_Get_TachoLowValue(EEP_HANDLE eep_handle)
{
	uint16_t TachoLowValue_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		TachoLowValue_val = ap_eep->process_eep.TachoLowValue;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return TachoLowValue_val;
}
void DevEep_Set_TachoLowValue(EEP_HANDLE eep_handle,uint16_t const TachoLowValue_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_TachoLowValue_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_TachoLowValue_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_TachoLowValue_LIST_ID);
#if EEP_CONTENT_TachoLowValue_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = TachoLowValue_val;
#elif EEP_CONTENT_TachoLowValue_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(TachoLowValue_val);
	msg[len++] = (uint8_t)(TachoLowValue_val>>8);
#else 
	msg[len++] = (uint8_t)(TachoLowValue_val);
	msg[len++] = (uint8_t)(TachoLowValue_val>>8);
	msg[len++] = (uint8_t)(TachoLowValue_val>>16);
	msg[len++] = (uint8_t)(TachoLowValue_val>>24);
#endif // {EEP_CONTENT_TachoLowValue_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_TachoLowValue_ITEM_LEN > 1
void DevEep_Get_TachoLowValue(EEP_HANDLE eep_handle,uint16_t *TachoLowValue_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_TachoLowValue_ITEM_LEN; i ++)
		{
			TachoLowValue_val[i] = ap_eep->process_eep.TachoLowValue[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_TachoLowValue(EEP_HANDLE eep_handle,uint16_t const *TachoLowValue_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_TachoLowValue_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_TachoLowValue_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_TachoLowValue_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_TachoLowValue_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_TachoLowValue_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = TachoLowValue_val[i];
#elif EEP_CONTENT_TachoLowValue_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(TachoLowValue_val[i]);
		msg[len++] = (uint8_t)(TachoLowValue_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(TachoLowValue_val[i]);
		msg[len++] = (uint8_t)(TachoLowValue_val[i]>>8);
		msg[len++] = (uint8_t)(TachoLowValue_val[i]>>16);
		msg[len++] = (uint8_t)(TachoLowValue_val[i]>>24);
#endif // {EEP_CONTENT_TachoLowValue_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_TachoLowValue_ITEM_LEN}
void DevEep_Inject_ChgCalbk_TachoLowValue(EEP_HANDLE eep_handle,TachoLowValue_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_TachoLowValue_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_TachoLowValue_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_TachoLowValue(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		TachoLowValue_ChgCallBk TachoLowValue_callback =  (TachoLowValue_ChgCallBk)callbk;
		TachoLowValue_callback(ap,ap->process_eep.TachoLowValue);
	}
}

// api define for TachoLimitValue /////////////////////////////////////////////////////////
#if EEP_CONTENT_TachoLimitValue_ITEM_LEN == 1
uint16_t DevEep_Get_TachoLimitValue(EEP_HANDLE eep_handle)
{
	uint16_t TachoLimitValue_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		TachoLimitValue_val = ap_eep->process_eep.TachoLimitValue;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return TachoLimitValue_val;
}
void DevEep_Set_TachoLimitValue(EEP_HANDLE eep_handle,uint16_t const TachoLimitValue_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_TachoLimitValue_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_TachoLimitValue_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_TachoLimitValue_LIST_ID);
#if EEP_CONTENT_TachoLimitValue_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = TachoLimitValue_val;
#elif EEP_CONTENT_TachoLimitValue_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(TachoLimitValue_val);
	msg[len++] = (uint8_t)(TachoLimitValue_val>>8);
#else 
	msg[len++] = (uint8_t)(TachoLimitValue_val);
	msg[len++] = (uint8_t)(TachoLimitValue_val>>8);
	msg[len++] = (uint8_t)(TachoLimitValue_val>>16);
	msg[len++] = (uint8_t)(TachoLimitValue_val>>24);
#endif // {EEP_CONTENT_TachoLimitValue_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_TachoLimitValue_ITEM_LEN > 1
void DevEep_Get_TachoLimitValue(EEP_HANDLE eep_handle,uint16_t *TachoLimitValue_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_TachoLimitValue_ITEM_LEN; i ++)
		{
			TachoLimitValue_val[i] = ap_eep->process_eep.TachoLimitValue[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_TachoLimitValue(EEP_HANDLE eep_handle,uint16_t const *TachoLimitValue_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_TachoLimitValue_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_TachoLimitValue_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_TachoLimitValue_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_TachoLimitValue_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_TachoLimitValue_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = TachoLimitValue_val[i];
#elif EEP_CONTENT_TachoLimitValue_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(TachoLimitValue_val[i]);
		msg[len++] = (uint8_t)(TachoLimitValue_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(TachoLimitValue_val[i]);
		msg[len++] = (uint8_t)(TachoLimitValue_val[i]>>8);
		msg[len++] = (uint8_t)(TachoLimitValue_val[i]>>16);
		msg[len++] = (uint8_t)(TachoLimitValue_val[i]>>24);
#endif // {EEP_CONTENT_TachoLimitValue_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_TachoLimitValue_ITEM_LEN}
void DevEep_Inject_ChgCalbk_TachoLimitValue(EEP_HANDLE eep_handle,TachoLimitValue_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_TachoLimitValue_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_TachoLimitValue_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_TachoLimitValue(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		TachoLimitValue_ChgCallBk TachoLimitValue_callback =  (TachoLimitValue_ChgCallBk)callbk;
		TachoLimitValue_callback(ap,ap->process_eep.TachoLimitValue);
	}
}

// api define for TachoUDeltaLimit /////////////////////////////////////////////////////////
#if EEP_CONTENT_TachoUDeltaLimit_ITEM_LEN == 1
uint16_t DevEep_Get_TachoUDeltaLimit(EEP_HANDLE eep_handle)
{
	uint16_t TachoUDeltaLimit_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		TachoUDeltaLimit_val = ap_eep->process_eep.TachoUDeltaLimit;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return TachoUDeltaLimit_val;
}
void DevEep_Set_TachoUDeltaLimit(EEP_HANDLE eep_handle,uint16_t const TachoUDeltaLimit_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_TachoUDeltaLimit_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_TachoUDeltaLimit_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_TachoUDeltaLimit_LIST_ID);
#if EEP_CONTENT_TachoUDeltaLimit_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = TachoUDeltaLimit_val;
#elif EEP_CONTENT_TachoUDeltaLimit_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(TachoUDeltaLimit_val);
	msg[len++] = (uint8_t)(TachoUDeltaLimit_val>>8);
#else 
	msg[len++] = (uint8_t)(TachoUDeltaLimit_val);
	msg[len++] = (uint8_t)(TachoUDeltaLimit_val>>8);
	msg[len++] = (uint8_t)(TachoUDeltaLimit_val>>16);
	msg[len++] = (uint8_t)(TachoUDeltaLimit_val>>24);
#endif // {EEP_CONTENT_TachoUDeltaLimit_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_TachoUDeltaLimit_ITEM_LEN > 1
void DevEep_Get_TachoUDeltaLimit(EEP_HANDLE eep_handle,uint16_t *TachoUDeltaLimit_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_TachoUDeltaLimit_ITEM_LEN; i ++)
		{
			TachoUDeltaLimit_val[i] = ap_eep->process_eep.TachoUDeltaLimit[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_TachoUDeltaLimit(EEP_HANDLE eep_handle,uint16_t const *TachoUDeltaLimit_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_TachoUDeltaLimit_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_TachoUDeltaLimit_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_TachoUDeltaLimit_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_TachoUDeltaLimit_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_TachoUDeltaLimit_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = TachoUDeltaLimit_val[i];
#elif EEP_CONTENT_TachoUDeltaLimit_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(TachoUDeltaLimit_val[i]);
		msg[len++] = (uint8_t)(TachoUDeltaLimit_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(TachoUDeltaLimit_val[i]);
		msg[len++] = (uint8_t)(TachoUDeltaLimit_val[i]>>8);
		msg[len++] = (uint8_t)(TachoUDeltaLimit_val[i]>>16);
		msg[len++] = (uint8_t)(TachoUDeltaLimit_val[i]>>24);
#endif // {EEP_CONTENT_TachoUDeltaLimit_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_TachoUDeltaLimit_ITEM_LEN}
void DevEep_Inject_ChgCalbk_TachoUDeltaLimit(EEP_HANDLE eep_handle,TachoUDeltaLimit_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_TachoUDeltaLimit_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_TachoUDeltaLimit_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_TachoUDeltaLimit(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		TachoUDeltaLimit_ChgCallBk TachoUDeltaLimit_callback =  (TachoUDeltaLimit_ChgCallBk)callbk;
		TachoUDeltaLimit_callback(ap,ap->process_eep.TachoUDeltaLimit);
	}
}

// api define for TachoLDeltaLimit /////////////////////////////////////////////////////////
#if EEP_CONTENT_TachoLDeltaLimit_ITEM_LEN == 1
uint16_t DevEep_Get_TachoLDeltaLimit(EEP_HANDLE eep_handle)
{
	uint16_t TachoLDeltaLimit_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		TachoLDeltaLimit_val = ap_eep->process_eep.TachoLDeltaLimit;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return TachoLDeltaLimit_val;
}
void DevEep_Set_TachoLDeltaLimit(EEP_HANDLE eep_handle,uint16_t const TachoLDeltaLimit_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_TachoLDeltaLimit_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_TachoLDeltaLimit_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_TachoLDeltaLimit_LIST_ID);
#if EEP_CONTENT_TachoLDeltaLimit_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = TachoLDeltaLimit_val;
#elif EEP_CONTENT_TachoLDeltaLimit_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(TachoLDeltaLimit_val);
	msg[len++] = (uint8_t)(TachoLDeltaLimit_val>>8);
#else 
	msg[len++] = (uint8_t)(TachoLDeltaLimit_val);
	msg[len++] = (uint8_t)(TachoLDeltaLimit_val>>8);
	msg[len++] = (uint8_t)(TachoLDeltaLimit_val>>16);
	msg[len++] = (uint8_t)(TachoLDeltaLimit_val>>24);
#endif // {EEP_CONTENT_TachoLDeltaLimit_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_TachoLDeltaLimit_ITEM_LEN > 1
void DevEep_Get_TachoLDeltaLimit(EEP_HANDLE eep_handle,uint16_t *TachoLDeltaLimit_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_TachoLDeltaLimit_ITEM_LEN; i ++)
		{
			TachoLDeltaLimit_val[i] = ap_eep->process_eep.TachoLDeltaLimit[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_TachoLDeltaLimit(EEP_HANDLE eep_handle,uint16_t const *TachoLDeltaLimit_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_TachoLDeltaLimit_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_TachoLDeltaLimit_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_TachoLDeltaLimit_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_TachoLDeltaLimit_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_TachoLDeltaLimit_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = TachoLDeltaLimit_val[i];
#elif EEP_CONTENT_TachoLDeltaLimit_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(TachoLDeltaLimit_val[i]);
		msg[len++] = (uint8_t)(TachoLDeltaLimit_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(TachoLDeltaLimit_val[i]);
		msg[len++] = (uint8_t)(TachoLDeltaLimit_val[i]>>8);
		msg[len++] = (uint8_t)(TachoLDeltaLimit_val[i]>>16);
		msg[len++] = (uint8_t)(TachoLDeltaLimit_val[i]>>24);
#endif // {EEP_CONTENT_TachoLDeltaLimit_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_TachoLDeltaLimit_ITEM_LEN}
void DevEep_Inject_ChgCalbk_TachoLDeltaLimit(EEP_HANDLE eep_handle,TachoLDeltaLimit_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_TachoLDeltaLimit_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_TachoLDeltaLimit_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_TachoLDeltaLimit(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		TachoLDeltaLimit_ChgCallBk TachoLDeltaLimit_callback =  (TachoLDeltaLimit_ChgCallBk)callbk;
		TachoLDeltaLimit_callback(ap,ap->process_eep.TachoLDeltaLimit);
	}
}

// api define for TachoUpChangeValue /////////////////////////////////////////////////////////
#if EEP_CONTENT_TachoUpChangeValue_ITEM_LEN == 1
uint16_t DevEep_Get_TachoUpChangeValue(EEP_HANDLE eep_handle)
{
	uint16_t TachoUpChangeValue_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		TachoUpChangeValue_val = ap_eep->process_eep.TachoUpChangeValue;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return TachoUpChangeValue_val;
}
void DevEep_Set_TachoUpChangeValue(EEP_HANDLE eep_handle,uint16_t const TachoUpChangeValue_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_TachoUpChangeValue_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_TachoUpChangeValue_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_TachoUpChangeValue_LIST_ID);
#if EEP_CONTENT_TachoUpChangeValue_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = TachoUpChangeValue_val;
#elif EEP_CONTENT_TachoUpChangeValue_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(TachoUpChangeValue_val);
	msg[len++] = (uint8_t)(TachoUpChangeValue_val>>8);
#else 
	msg[len++] = (uint8_t)(TachoUpChangeValue_val);
	msg[len++] = (uint8_t)(TachoUpChangeValue_val>>8);
	msg[len++] = (uint8_t)(TachoUpChangeValue_val>>16);
	msg[len++] = (uint8_t)(TachoUpChangeValue_val>>24);
#endif // {EEP_CONTENT_TachoUpChangeValue_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_TachoUpChangeValue_ITEM_LEN > 1
void DevEep_Get_TachoUpChangeValue(EEP_HANDLE eep_handle,uint16_t *TachoUpChangeValue_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_TachoUpChangeValue_ITEM_LEN; i ++)
		{
			TachoUpChangeValue_val[i] = ap_eep->process_eep.TachoUpChangeValue[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_TachoUpChangeValue(EEP_HANDLE eep_handle,uint16_t const *TachoUpChangeValue_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_TachoUpChangeValue_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_TachoUpChangeValue_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_TachoUpChangeValue_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_TachoUpChangeValue_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_TachoUpChangeValue_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = TachoUpChangeValue_val[i];
#elif EEP_CONTENT_TachoUpChangeValue_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(TachoUpChangeValue_val[i]);
		msg[len++] = (uint8_t)(TachoUpChangeValue_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(TachoUpChangeValue_val[i]);
		msg[len++] = (uint8_t)(TachoUpChangeValue_val[i]>>8);
		msg[len++] = (uint8_t)(TachoUpChangeValue_val[i]>>16);
		msg[len++] = (uint8_t)(TachoUpChangeValue_val[i]>>24);
#endif // {EEP_CONTENT_TachoUpChangeValue_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_TachoUpChangeValue_ITEM_LEN}
void DevEep_Inject_ChgCalbk_TachoUpChangeValue(EEP_HANDLE eep_handle,TachoUpChangeValue_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_TachoUpChangeValue_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_TachoUpChangeValue_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_TachoUpChangeValue(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		TachoUpChangeValue_ChgCallBk TachoUpChangeValue_callback =  (TachoUpChangeValue_ChgCallBk)callbk;
		TachoUpChangeValue_callback(ap,ap->process_eep.TachoUpChangeValue);
	}
}

// api define for TachoLowChangeValue /////////////////////////////////////////////////////////
#if EEP_CONTENT_TachoLowChangeValue_ITEM_LEN == 1
uint16_t DevEep_Get_TachoLowChangeValue(EEP_HANDLE eep_handle)
{
	uint16_t TachoLowChangeValue_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		TachoLowChangeValue_val = ap_eep->process_eep.TachoLowChangeValue;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return TachoLowChangeValue_val;
}
void DevEep_Set_TachoLowChangeValue(EEP_HANDLE eep_handle,uint16_t const TachoLowChangeValue_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_TachoLowChangeValue_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_TachoLowChangeValue_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_TachoLowChangeValue_LIST_ID);
#if EEP_CONTENT_TachoLowChangeValue_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = TachoLowChangeValue_val;
#elif EEP_CONTENT_TachoLowChangeValue_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(TachoLowChangeValue_val);
	msg[len++] = (uint8_t)(TachoLowChangeValue_val>>8);
#else 
	msg[len++] = (uint8_t)(TachoLowChangeValue_val);
	msg[len++] = (uint8_t)(TachoLowChangeValue_val>>8);
	msg[len++] = (uint8_t)(TachoLowChangeValue_val>>16);
	msg[len++] = (uint8_t)(TachoLowChangeValue_val>>24);
#endif // {EEP_CONTENT_TachoLowChangeValue_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_TachoLowChangeValue_ITEM_LEN > 1
void DevEep_Get_TachoLowChangeValue(EEP_HANDLE eep_handle,uint16_t *TachoLowChangeValue_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_TachoLowChangeValue_ITEM_LEN; i ++)
		{
			TachoLowChangeValue_val[i] = ap_eep->process_eep.TachoLowChangeValue[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_TachoLowChangeValue(EEP_HANDLE eep_handle,uint16_t const *TachoLowChangeValue_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_TachoLowChangeValue_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_TachoLowChangeValue_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_TachoLowChangeValue_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_TachoLowChangeValue_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_TachoLowChangeValue_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = TachoLowChangeValue_val[i];
#elif EEP_CONTENT_TachoLowChangeValue_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(TachoLowChangeValue_val[i]);
		msg[len++] = (uint8_t)(TachoLowChangeValue_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(TachoLowChangeValue_val[i]);
		msg[len++] = (uint8_t)(TachoLowChangeValue_val[i]>>8);
		msg[len++] = (uint8_t)(TachoLowChangeValue_val[i]>>16);
		msg[len++] = (uint8_t)(TachoLowChangeValue_val[i]>>24);
#endif // {EEP_CONTENT_TachoLowChangeValue_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_TachoLowChangeValue_ITEM_LEN}
void DevEep_Inject_ChgCalbk_TachoLowChangeValue(EEP_HANDLE eep_handle,TachoLowChangeValue_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_TachoLowChangeValue_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_TachoLowChangeValue_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_TachoLowChangeValue(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		TachoLowChangeValue_ChgCallBk TachoLowChangeValue_callback =  (TachoLowChangeValue_ChgCallBk)callbk;
		TachoLowChangeValue_callback(ap,ap->process_eep.TachoLowChangeValue);
	}
}

// api define for TachoRun /////////////////////////////////////////////////////////
#if EEP_CONTENT_TachoRun_ITEM_LEN == 1
uint16_t DevEep_Get_TachoRun(EEP_HANDLE eep_handle)
{
	uint16_t TachoRun_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		TachoRun_val = ap_eep->process_eep.TachoRun;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return TachoRun_val;
}
void DevEep_Set_TachoRun(EEP_HANDLE eep_handle,uint16_t const TachoRun_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_TachoRun_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_TachoRun_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_TachoRun_LIST_ID);
#if EEP_CONTENT_TachoRun_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = TachoRun_val;
#elif EEP_CONTENT_TachoRun_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(TachoRun_val);
	msg[len++] = (uint8_t)(TachoRun_val>>8);
#else 
	msg[len++] = (uint8_t)(TachoRun_val);
	msg[len++] = (uint8_t)(TachoRun_val>>8);
	msg[len++] = (uint8_t)(TachoRun_val>>16);
	msg[len++] = (uint8_t)(TachoRun_val>>24);
#endif // {EEP_CONTENT_TachoRun_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_TachoRun_ITEM_LEN > 1
void DevEep_Get_TachoRun(EEP_HANDLE eep_handle,uint16_t *TachoRun_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_TachoRun_ITEM_LEN; i ++)
		{
			TachoRun_val[i] = ap_eep->process_eep.TachoRun[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_TachoRun(EEP_HANDLE eep_handle,uint16_t const *TachoRun_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_TachoRun_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_TachoRun_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_TachoRun_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_TachoRun_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_TachoRun_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = TachoRun_val[i];
#elif EEP_CONTENT_TachoRun_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(TachoRun_val[i]);
		msg[len++] = (uint8_t)(TachoRun_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(TachoRun_val[i]);
		msg[len++] = (uint8_t)(TachoRun_val[i]>>8);
		msg[len++] = (uint8_t)(TachoRun_val[i]>>16);
		msg[len++] = (uint8_t)(TachoRun_val[i]>>24);
#endif // {EEP_CONTENT_TachoRun_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_TachoRun_ITEM_LEN}
void DevEep_Inject_ChgCalbk_TachoRun(EEP_HANDLE eep_handle,TachoRun_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_TachoRun_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_TachoRun_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_TachoRun(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		TachoRun_ChgCallBk TachoRun_callback =  (TachoRun_ChgCallBk)callbk;
		TachoRun_callback(ap,ap->process_eep.TachoRun);
	}
}

// api define for Speed_Rm_1 /////////////////////////////////////////////////////////
#if EEP_CONTENT_Speed_Rm_1_ITEM_LEN == 1
uint16_t DevEep_Get_Speed_Rm_1(EEP_HANDLE eep_handle)
{
	uint16_t Speed_Rm_1_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		Speed_Rm_1_val = ap_eep->process_eep.Speed_Rm_1;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return Speed_Rm_1_val;
}
void DevEep_Set_Speed_Rm_1(EEP_HANDLE eep_handle,uint16_t const Speed_Rm_1_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_Speed_Rm_1_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_Speed_Rm_1_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_Speed_Rm_1_LIST_ID);
#if EEP_CONTENT_Speed_Rm_1_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = Speed_Rm_1_val;
#elif EEP_CONTENT_Speed_Rm_1_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(Speed_Rm_1_val);
	msg[len++] = (uint8_t)(Speed_Rm_1_val>>8);
#else 
	msg[len++] = (uint8_t)(Speed_Rm_1_val);
	msg[len++] = (uint8_t)(Speed_Rm_1_val>>8);
	msg[len++] = (uint8_t)(Speed_Rm_1_val>>16);
	msg[len++] = (uint8_t)(Speed_Rm_1_val>>24);
#endif // {EEP_CONTENT_Speed_Rm_1_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_Speed_Rm_1_ITEM_LEN > 1
void DevEep_Get_Speed_Rm_1(EEP_HANDLE eep_handle,uint16_t *Speed_Rm_1_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_Speed_Rm_1_ITEM_LEN; i ++)
		{
			Speed_Rm_1_val[i] = ap_eep->process_eep.Speed_Rm_1[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_Speed_Rm_1(EEP_HANDLE eep_handle,uint16_t const *Speed_Rm_1_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_Speed_Rm_1_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_Speed_Rm_1_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_Speed_Rm_1_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_Speed_Rm_1_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_Speed_Rm_1_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = Speed_Rm_1_val[i];
#elif EEP_CONTENT_Speed_Rm_1_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(Speed_Rm_1_val[i]);
		msg[len++] = (uint8_t)(Speed_Rm_1_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(Speed_Rm_1_val[i]);
		msg[len++] = (uint8_t)(Speed_Rm_1_val[i]>>8);
		msg[len++] = (uint8_t)(Speed_Rm_1_val[i]>>16);
		msg[len++] = (uint8_t)(Speed_Rm_1_val[i]>>24);
#endif // {EEP_CONTENT_Speed_Rm_1_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_Speed_Rm_1_ITEM_LEN}
void DevEep_Inject_ChgCalbk_Speed_Rm_1(EEP_HANDLE eep_handle,Speed_Rm_1_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_Speed_Rm_1_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_Speed_Rm_1_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_Speed_Rm_1(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		Speed_Rm_1_ChgCallBk Speed_Rm_1_callback =  (Speed_Rm_1_ChgCallBk)callbk;
		Speed_Rm_1_callback(ap,ap->process_eep.Speed_Rm_1);
	}
}

// api define for Speed_Rm_2 /////////////////////////////////////////////////////////
#if EEP_CONTENT_Speed_Rm_2_ITEM_LEN == 1
uint16_t DevEep_Get_Speed_Rm_2(EEP_HANDLE eep_handle)
{
	uint16_t Speed_Rm_2_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		Speed_Rm_2_val = ap_eep->process_eep.Speed_Rm_2;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return Speed_Rm_2_val;
}
void DevEep_Set_Speed_Rm_2(EEP_HANDLE eep_handle,uint16_t const Speed_Rm_2_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_Speed_Rm_2_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_Speed_Rm_2_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_Speed_Rm_2_LIST_ID);
#if EEP_CONTENT_Speed_Rm_2_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = Speed_Rm_2_val;
#elif EEP_CONTENT_Speed_Rm_2_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(Speed_Rm_2_val);
	msg[len++] = (uint8_t)(Speed_Rm_2_val>>8);
#else 
	msg[len++] = (uint8_t)(Speed_Rm_2_val);
	msg[len++] = (uint8_t)(Speed_Rm_2_val>>8);
	msg[len++] = (uint8_t)(Speed_Rm_2_val>>16);
	msg[len++] = (uint8_t)(Speed_Rm_2_val>>24);
#endif // {EEP_CONTENT_Speed_Rm_2_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_Speed_Rm_2_ITEM_LEN > 1
void DevEep_Get_Speed_Rm_2(EEP_HANDLE eep_handle,uint16_t *Speed_Rm_2_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_Speed_Rm_2_ITEM_LEN; i ++)
		{
			Speed_Rm_2_val[i] = ap_eep->process_eep.Speed_Rm_2[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_Speed_Rm_2(EEP_HANDLE eep_handle,uint16_t const *Speed_Rm_2_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_Speed_Rm_2_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_Speed_Rm_2_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_Speed_Rm_2_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_Speed_Rm_2_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_Speed_Rm_2_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = Speed_Rm_2_val[i];
#elif EEP_CONTENT_Speed_Rm_2_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(Speed_Rm_2_val[i]);
		msg[len++] = (uint8_t)(Speed_Rm_2_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(Speed_Rm_2_val[i]);
		msg[len++] = (uint8_t)(Speed_Rm_2_val[i]>>8);
		msg[len++] = (uint8_t)(Speed_Rm_2_val[i]>>16);
		msg[len++] = (uint8_t)(Speed_Rm_2_val[i]>>24);
#endif // {EEP_CONTENT_Speed_Rm_2_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_Speed_Rm_2_ITEM_LEN}
void DevEep_Inject_ChgCalbk_Speed_Rm_2(EEP_HANDLE eep_handle,Speed_Rm_2_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_Speed_Rm_2_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_Speed_Rm_2_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_Speed_Rm_2(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		Speed_Rm_2_ChgCallBk Speed_Rm_2_callback =  (Speed_Rm_2_ChgCallBk)callbk;
		Speed_Rm_2_callback(ap,ap->process_eep.Speed_Rm_2);
	}
}

// api define for Speed_Rc /////////////////////////////////////////////////////////
#if EEP_CONTENT_Speed_Rc_ITEM_LEN == 1
uint16_t DevEep_Get_Speed_Rc(EEP_HANDLE eep_handle)
{
	uint16_t Speed_Rc_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		Speed_Rc_val = ap_eep->process_eep.Speed_Rc;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return Speed_Rc_val;
}
void DevEep_Set_Speed_Rc(EEP_HANDLE eep_handle,uint16_t const Speed_Rc_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_Speed_Rc_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_Speed_Rc_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_Speed_Rc_LIST_ID);
#if EEP_CONTENT_Speed_Rc_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = Speed_Rc_val;
#elif EEP_CONTENT_Speed_Rc_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(Speed_Rc_val);
	msg[len++] = (uint8_t)(Speed_Rc_val>>8);
#else 
	msg[len++] = (uint8_t)(Speed_Rc_val);
	msg[len++] = (uint8_t)(Speed_Rc_val>>8);
	msg[len++] = (uint8_t)(Speed_Rc_val>>16);
	msg[len++] = (uint8_t)(Speed_Rc_val>>24);
#endif // {EEP_CONTENT_Speed_Rc_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_Speed_Rc_ITEM_LEN > 1
void DevEep_Get_Speed_Rc(EEP_HANDLE eep_handle,uint16_t *Speed_Rc_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_Speed_Rc_ITEM_LEN; i ++)
		{
			Speed_Rc_val[i] = ap_eep->process_eep.Speed_Rc[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_Speed_Rc(EEP_HANDLE eep_handle,uint16_t const *Speed_Rc_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_Speed_Rc_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_Speed_Rc_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_Speed_Rc_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_Speed_Rc_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_Speed_Rc_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = Speed_Rc_val[i];
#elif EEP_CONTENT_Speed_Rc_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(Speed_Rc_val[i]);
		msg[len++] = (uint8_t)(Speed_Rc_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(Speed_Rc_val[i]);
		msg[len++] = (uint8_t)(Speed_Rc_val[i]>>8);
		msg[len++] = (uint8_t)(Speed_Rc_val[i]>>16);
		msg[len++] = (uint8_t)(Speed_Rc_val[i]>>24);
#endif // {EEP_CONTENT_Speed_Rc_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_Speed_Rc_ITEM_LEN}
void DevEep_Inject_ChgCalbk_Speed_Rc(EEP_HANDLE eep_handle,Speed_Rc_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_Speed_Rc_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_Speed_Rc_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_Speed_Rc(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		Speed_Rc_ChgCallBk Speed_Rc_callback =  (Speed_Rc_ChgCallBk)callbk;
		Speed_Rc_callback(ap,ap->process_eep.Speed_Rc);
	}
}

// api define for FuelTelltaleOn_R /////////////////////////////////////////////////////////
#if EEP_CONTENT_FuelTelltaleOn_R_ITEM_LEN == 1
uint16_t DevEep_Get_FuelTelltaleOn_R(EEP_HANDLE eep_handle)
{
	uint16_t FuelTelltaleOn_R_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		FuelTelltaleOn_R_val = ap_eep->process_eep.FuelTelltaleOn_R;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return FuelTelltaleOn_R_val;
}
void DevEep_Set_FuelTelltaleOn_R(EEP_HANDLE eep_handle,uint16_t const FuelTelltaleOn_R_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_FuelTelltaleOn_R_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_FuelTelltaleOn_R_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_FuelTelltaleOn_R_LIST_ID);
#if EEP_CONTENT_FuelTelltaleOn_R_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = FuelTelltaleOn_R_val;
#elif EEP_CONTENT_FuelTelltaleOn_R_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(FuelTelltaleOn_R_val);
	msg[len++] = (uint8_t)(FuelTelltaleOn_R_val>>8);
#else 
	msg[len++] = (uint8_t)(FuelTelltaleOn_R_val);
	msg[len++] = (uint8_t)(FuelTelltaleOn_R_val>>8);
	msg[len++] = (uint8_t)(FuelTelltaleOn_R_val>>16);
	msg[len++] = (uint8_t)(FuelTelltaleOn_R_val>>24);
#endif // {EEP_CONTENT_FuelTelltaleOn_R_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_FuelTelltaleOn_R_ITEM_LEN > 1
void DevEep_Get_FuelTelltaleOn_R(EEP_HANDLE eep_handle,uint16_t *FuelTelltaleOn_R_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_FuelTelltaleOn_R_ITEM_LEN; i ++)
		{
			FuelTelltaleOn_R_val[i] = ap_eep->process_eep.FuelTelltaleOn_R[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_FuelTelltaleOn_R(EEP_HANDLE eep_handle,uint16_t const *FuelTelltaleOn_R_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_FuelTelltaleOn_R_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_FuelTelltaleOn_R_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_FuelTelltaleOn_R_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_FuelTelltaleOn_R_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_FuelTelltaleOn_R_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = FuelTelltaleOn_R_val[i];
#elif EEP_CONTENT_FuelTelltaleOn_R_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(FuelTelltaleOn_R_val[i]);
		msg[len++] = (uint8_t)(FuelTelltaleOn_R_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(FuelTelltaleOn_R_val[i]);
		msg[len++] = (uint8_t)(FuelTelltaleOn_R_val[i]>>8);
		msg[len++] = (uint8_t)(FuelTelltaleOn_R_val[i]>>16);
		msg[len++] = (uint8_t)(FuelTelltaleOn_R_val[i]>>24);
#endif // {EEP_CONTENT_FuelTelltaleOn_R_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_FuelTelltaleOn_R_ITEM_LEN}
void DevEep_Inject_ChgCalbk_FuelTelltaleOn_R(EEP_HANDLE eep_handle,FuelTelltaleOn_R_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_FuelTelltaleOn_R_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_FuelTelltaleOn_R_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_FuelTelltaleOn_R(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		FuelTelltaleOn_R_ChgCallBk FuelTelltaleOn_R_callback =  (FuelTelltaleOn_R_ChgCallBk)callbk;
		FuelTelltaleOn_R_callback(ap,ap->process_eep.FuelTelltaleOn_R);
	}
}

// api define for FuelTelltaleOff_R /////////////////////////////////////////////////////////
#if EEP_CONTENT_FuelTelltaleOff_R_ITEM_LEN == 1
uint16_t DevEep_Get_FuelTelltaleOff_R(EEP_HANDLE eep_handle)
{
	uint16_t FuelTelltaleOff_R_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		FuelTelltaleOff_R_val = ap_eep->process_eep.FuelTelltaleOff_R;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return FuelTelltaleOff_R_val;
}
void DevEep_Set_FuelTelltaleOff_R(EEP_HANDLE eep_handle,uint16_t const FuelTelltaleOff_R_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_FuelTelltaleOff_R_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_FuelTelltaleOff_R_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_FuelTelltaleOff_R_LIST_ID);
#if EEP_CONTENT_FuelTelltaleOff_R_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = FuelTelltaleOff_R_val;
#elif EEP_CONTENT_FuelTelltaleOff_R_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(FuelTelltaleOff_R_val);
	msg[len++] = (uint8_t)(FuelTelltaleOff_R_val>>8);
#else 
	msg[len++] = (uint8_t)(FuelTelltaleOff_R_val);
	msg[len++] = (uint8_t)(FuelTelltaleOff_R_val>>8);
	msg[len++] = (uint8_t)(FuelTelltaleOff_R_val>>16);
	msg[len++] = (uint8_t)(FuelTelltaleOff_R_val>>24);
#endif // {EEP_CONTENT_FuelTelltaleOff_R_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_FuelTelltaleOff_R_ITEM_LEN > 1
void DevEep_Get_FuelTelltaleOff_R(EEP_HANDLE eep_handle,uint16_t *FuelTelltaleOff_R_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_FuelTelltaleOff_R_ITEM_LEN; i ++)
		{
			FuelTelltaleOff_R_val[i] = ap_eep->process_eep.FuelTelltaleOff_R[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_FuelTelltaleOff_R(EEP_HANDLE eep_handle,uint16_t const *FuelTelltaleOff_R_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_FuelTelltaleOff_R_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_FuelTelltaleOff_R_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_FuelTelltaleOff_R_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_FuelTelltaleOff_R_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_FuelTelltaleOff_R_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = FuelTelltaleOff_R_val[i];
#elif EEP_CONTENT_FuelTelltaleOff_R_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(FuelTelltaleOff_R_val[i]);
		msg[len++] = (uint8_t)(FuelTelltaleOff_R_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(FuelTelltaleOff_R_val[i]);
		msg[len++] = (uint8_t)(FuelTelltaleOff_R_val[i]>>8);
		msg[len++] = (uint8_t)(FuelTelltaleOff_R_val[i]>>16);
		msg[len++] = (uint8_t)(FuelTelltaleOff_R_val[i]>>24);
#endif // {EEP_CONTENT_FuelTelltaleOff_R_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_FuelTelltaleOff_R_ITEM_LEN}
void DevEep_Inject_ChgCalbk_FuelTelltaleOff_R(EEP_HANDLE eep_handle,FuelTelltaleOff_R_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_FuelTelltaleOff_R_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_FuelTelltaleOff_R_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_FuelTelltaleOff_R(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		FuelTelltaleOff_R_ChgCallBk FuelTelltaleOff_R_callback =  (FuelTelltaleOff_R_ChgCallBk)callbk;
		FuelTelltaleOff_R_callback(ap,ap->process_eep.FuelTelltaleOff_R);
	}
}

// api define for FuelWarningOnPoint_R /////////////////////////////////////////////////////////
#if EEP_CONTENT_FuelWarningOnPoint_R_ITEM_LEN == 1
uint16_t DevEep_Get_FuelWarningOnPoint_R(EEP_HANDLE eep_handle)
{
	uint16_t FuelWarningOnPoint_R_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		FuelWarningOnPoint_R_val = ap_eep->process_eep.FuelWarningOnPoint_R;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return FuelWarningOnPoint_R_val;
}
void DevEep_Set_FuelWarningOnPoint_R(EEP_HANDLE eep_handle,uint16_t const FuelWarningOnPoint_R_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_FuelWarningOnPoint_R_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_FuelWarningOnPoint_R_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_FuelWarningOnPoint_R_LIST_ID);
#if EEP_CONTENT_FuelWarningOnPoint_R_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = FuelWarningOnPoint_R_val;
#elif EEP_CONTENT_FuelWarningOnPoint_R_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(FuelWarningOnPoint_R_val);
	msg[len++] = (uint8_t)(FuelWarningOnPoint_R_val>>8);
#else 
	msg[len++] = (uint8_t)(FuelWarningOnPoint_R_val);
	msg[len++] = (uint8_t)(FuelWarningOnPoint_R_val>>8);
	msg[len++] = (uint8_t)(FuelWarningOnPoint_R_val>>16);
	msg[len++] = (uint8_t)(FuelWarningOnPoint_R_val>>24);
#endif // {EEP_CONTENT_FuelWarningOnPoint_R_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_FuelWarningOnPoint_R_ITEM_LEN > 1
void DevEep_Get_FuelWarningOnPoint_R(EEP_HANDLE eep_handle,uint16_t *FuelWarningOnPoint_R_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_FuelWarningOnPoint_R_ITEM_LEN; i ++)
		{
			FuelWarningOnPoint_R_val[i] = ap_eep->process_eep.FuelWarningOnPoint_R[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_FuelWarningOnPoint_R(EEP_HANDLE eep_handle,uint16_t const *FuelWarningOnPoint_R_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_FuelWarningOnPoint_R_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_FuelWarningOnPoint_R_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_FuelWarningOnPoint_R_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_FuelWarningOnPoint_R_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_FuelWarningOnPoint_R_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = FuelWarningOnPoint_R_val[i];
#elif EEP_CONTENT_FuelWarningOnPoint_R_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(FuelWarningOnPoint_R_val[i]);
		msg[len++] = (uint8_t)(FuelWarningOnPoint_R_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(FuelWarningOnPoint_R_val[i]);
		msg[len++] = (uint8_t)(FuelWarningOnPoint_R_val[i]>>8);
		msg[len++] = (uint8_t)(FuelWarningOnPoint_R_val[i]>>16);
		msg[len++] = (uint8_t)(FuelWarningOnPoint_R_val[i]>>24);
#endif // {EEP_CONTENT_FuelWarningOnPoint_R_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_FuelWarningOnPoint_R_ITEM_LEN}
void DevEep_Inject_ChgCalbk_FuelWarningOnPoint_R(EEP_HANDLE eep_handle,FuelWarningOnPoint_R_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_FuelWarningOnPoint_R_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_FuelWarningOnPoint_R_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_FuelWarningOnPoint_R(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		FuelWarningOnPoint_R_ChgCallBk FuelWarningOnPoint_R_callback =  (FuelWarningOnPoint_R_ChgCallBk)callbk;
		FuelWarningOnPoint_R_callback(ap,ap->process_eep.FuelWarningOnPoint_R);
	}
}

// api define for Fuel_R_Open /////////////////////////////////////////////////////////
#if EEP_CONTENT_Fuel_R_Open_ITEM_LEN == 1
uint16_t DevEep_Get_Fuel_R_Open(EEP_HANDLE eep_handle)
{
	uint16_t Fuel_R_Open_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		Fuel_R_Open_val = ap_eep->process_eep.Fuel_R_Open;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return Fuel_R_Open_val;
}
void DevEep_Set_Fuel_R_Open(EEP_HANDLE eep_handle,uint16_t const Fuel_R_Open_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_Fuel_R_Open_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_Fuel_R_Open_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_Fuel_R_Open_LIST_ID);
#if EEP_CONTENT_Fuel_R_Open_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = Fuel_R_Open_val;
#elif EEP_CONTENT_Fuel_R_Open_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(Fuel_R_Open_val);
	msg[len++] = (uint8_t)(Fuel_R_Open_val>>8);
#else 
	msg[len++] = (uint8_t)(Fuel_R_Open_val);
	msg[len++] = (uint8_t)(Fuel_R_Open_val>>8);
	msg[len++] = (uint8_t)(Fuel_R_Open_val>>16);
	msg[len++] = (uint8_t)(Fuel_R_Open_val>>24);
#endif // {EEP_CONTENT_Fuel_R_Open_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_Fuel_R_Open_ITEM_LEN > 1
void DevEep_Get_Fuel_R_Open(EEP_HANDLE eep_handle,uint16_t *Fuel_R_Open_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_Fuel_R_Open_ITEM_LEN; i ++)
		{
			Fuel_R_Open_val[i] = ap_eep->process_eep.Fuel_R_Open[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_Fuel_R_Open(EEP_HANDLE eep_handle,uint16_t const *Fuel_R_Open_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_Fuel_R_Open_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_Fuel_R_Open_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_Fuel_R_Open_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_Fuel_R_Open_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_Fuel_R_Open_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = Fuel_R_Open_val[i];
#elif EEP_CONTENT_Fuel_R_Open_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(Fuel_R_Open_val[i]);
		msg[len++] = (uint8_t)(Fuel_R_Open_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(Fuel_R_Open_val[i]);
		msg[len++] = (uint8_t)(Fuel_R_Open_val[i]>>8);
		msg[len++] = (uint8_t)(Fuel_R_Open_val[i]>>16);
		msg[len++] = (uint8_t)(Fuel_R_Open_val[i]>>24);
#endif // {EEP_CONTENT_Fuel_R_Open_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_Fuel_R_Open_ITEM_LEN}
void DevEep_Inject_ChgCalbk_Fuel_R_Open(EEP_HANDLE eep_handle,Fuel_R_Open_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_Fuel_R_Open_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_Fuel_R_Open_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_Fuel_R_Open(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		Fuel_R_Open_ChgCallBk Fuel_R_Open_callback =  (Fuel_R_Open_ChgCallBk)callbk;
		Fuel_R_Open_callback(ap,ap->process_eep.Fuel_R_Open);
	}
}

// api define for Fuel_R_Short /////////////////////////////////////////////////////////
#if EEP_CONTENT_Fuel_R_Short_ITEM_LEN == 1
uint16_t DevEep_Get_Fuel_R_Short(EEP_HANDLE eep_handle)
{
	uint16_t Fuel_R_Short_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		Fuel_R_Short_val = ap_eep->process_eep.Fuel_R_Short;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return Fuel_R_Short_val;
}
void DevEep_Set_Fuel_R_Short(EEP_HANDLE eep_handle,uint16_t const Fuel_R_Short_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_Fuel_R_Short_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_Fuel_R_Short_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_Fuel_R_Short_LIST_ID);
#if EEP_CONTENT_Fuel_R_Short_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = Fuel_R_Short_val;
#elif EEP_CONTENT_Fuel_R_Short_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(Fuel_R_Short_val);
	msg[len++] = (uint8_t)(Fuel_R_Short_val>>8);
#else 
	msg[len++] = (uint8_t)(Fuel_R_Short_val);
	msg[len++] = (uint8_t)(Fuel_R_Short_val>>8);
	msg[len++] = (uint8_t)(Fuel_R_Short_val>>16);
	msg[len++] = (uint8_t)(Fuel_R_Short_val>>24);
#endif // {EEP_CONTENT_Fuel_R_Short_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_Fuel_R_Short_ITEM_LEN > 1
void DevEep_Get_Fuel_R_Short(EEP_HANDLE eep_handle,uint16_t *Fuel_R_Short_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_Fuel_R_Short_ITEM_LEN; i ++)
		{
			Fuel_R_Short_val[i] = ap_eep->process_eep.Fuel_R_Short[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_Fuel_R_Short(EEP_HANDLE eep_handle,uint16_t const *Fuel_R_Short_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_Fuel_R_Short_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_Fuel_R_Short_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_Fuel_R_Short_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_Fuel_R_Short_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_Fuel_R_Short_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = Fuel_R_Short_val[i];
#elif EEP_CONTENT_Fuel_R_Short_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(Fuel_R_Short_val[i]);
		msg[len++] = (uint8_t)(Fuel_R_Short_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(Fuel_R_Short_val[i]);
		msg[len++] = (uint8_t)(Fuel_R_Short_val[i]>>8);
		msg[len++] = (uint8_t)(Fuel_R_Short_val[i]>>16);
		msg[len++] = (uint8_t)(Fuel_R_Short_val[i]>>24);
#endif // {EEP_CONTENT_Fuel_R_Short_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_Fuel_R_Short_ITEM_LEN}
void DevEep_Inject_ChgCalbk_Fuel_R_Short(EEP_HANDLE eep_handle,Fuel_R_Short_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_Fuel_R_Short_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_Fuel_R_Short_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_Fuel_R_Short(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		Fuel_R_Short_ChgCallBk Fuel_R_Short_callback =  (Fuel_R_Short_ChgCallBk)callbk;
		Fuel_R_Short_callback(ap,ap->process_eep.Fuel_R_Short);
	}
}

// api define for PkbWarningJudgeV1 /////////////////////////////////////////////////////////
#if EEP_CONTENT_PkbWarningJudgeV1_ITEM_LEN == 1
uint8_t DevEep_Get_PkbWarningJudgeV1(EEP_HANDLE eep_handle)
{
	uint8_t PkbWarningJudgeV1_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		PkbWarningJudgeV1_val = ap_eep->process_eep.PkbWarningJudgeV1;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return PkbWarningJudgeV1_val;
}
void DevEep_Set_PkbWarningJudgeV1(EEP_HANDLE eep_handle,uint8_t const PkbWarningJudgeV1_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_PkbWarningJudgeV1_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_PkbWarningJudgeV1_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_PkbWarningJudgeV1_LIST_ID);
#if EEP_CONTENT_PkbWarningJudgeV1_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = PkbWarningJudgeV1_val;
#elif EEP_CONTENT_PkbWarningJudgeV1_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(PkbWarningJudgeV1_val);
	msg[len++] = (uint8_t)(PkbWarningJudgeV1_val>>8);
#else 
	msg[len++] = (uint8_t)(PkbWarningJudgeV1_val);
	msg[len++] = (uint8_t)(PkbWarningJudgeV1_val>>8);
	msg[len++] = (uint8_t)(PkbWarningJudgeV1_val>>16);
	msg[len++] = (uint8_t)(PkbWarningJudgeV1_val>>24);
#endif // {EEP_CONTENT_PkbWarningJudgeV1_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_PkbWarningJudgeV1_ITEM_LEN > 1
void DevEep_Get_PkbWarningJudgeV1(EEP_HANDLE eep_handle,uint8_t *PkbWarningJudgeV1_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_PkbWarningJudgeV1_ITEM_LEN; i ++)
		{
			PkbWarningJudgeV1_val[i] = ap_eep->process_eep.PkbWarningJudgeV1[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_PkbWarningJudgeV1(EEP_HANDLE eep_handle,uint8_t const *PkbWarningJudgeV1_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_PkbWarningJudgeV1_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_PkbWarningJudgeV1_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_PkbWarningJudgeV1_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_PkbWarningJudgeV1_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_PkbWarningJudgeV1_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = PkbWarningJudgeV1_val[i];
#elif EEP_CONTENT_PkbWarningJudgeV1_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(PkbWarningJudgeV1_val[i]);
		msg[len++] = (uint8_t)(PkbWarningJudgeV1_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(PkbWarningJudgeV1_val[i]);
		msg[len++] = (uint8_t)(PkbWarningJudgeV1_val[i]>>8);
		msg[len++] = (uint8_t)(PkbWarningJudgeV1_val[i]>>16);
		msg[len++] = (uint8_t)(PkbWarningJudgeV1_val[i]>>24);
#endif // {EEP_CONTENT_PkbWarningJudgeV1_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_PkbWarningJudgeV1_ITEM_LEN}
void DevEep_Inject_ChgCalbk_PkbWarningJudgeV1(EEP_HANDLE eep_handle,PkbWarningJudgeV1_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_PkbWarningJudgeV1_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_PkbWarningJudgeV1_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_PkbWarningJudgeV1(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		PkbWarningJudgeV1_ChgCallBk PkbWarningJudgeV1_callback =  (PkbWarningJudgeV1_ChgCallBk)callbk;
		PkbWarningJudgeV1_callback(ap,ap->process_eep.PkbWarningJudgeV1);
	}
}

// api define for PkbWarningJudgeV2 /////////////////////////////////////////////////////////
#if EEP_CONTENT_PkbWarningJudgeV2_ITEM_LEN == 1
uint8_t DevEep_Get_PkbWarningJudgeV2(EEP_HANDLE eep_handle)
{
	uint8_t PkbWarningJudgeV2_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		PkbWarningJudgeV2_val = ap_eep->process_eep.PkbWarningJudgeV2;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return PkbWarningJudgeV2_val;
}
void DevEep_Set_PkbWarningJudgeV2(EEP_HANDLE eep_handle,uint8_t const PkbWarningJudgeV2_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_PkbWarningJudgeV2_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_PkbWarningJudgeV2_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_PkbWarningJudgeV2_LIST_ID);
#if EEP_CONTENT_PkbWarningJudgeV2_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = PkbWarningJudgeV2_val;
#elif EEP_CONTENT_PkbWarningJudgeV2_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(PkbWarningJudgeV2_val);
	msg[len++] = (uint8_t)(PkbWarningJudgeV2_val>>8);
#else 
	msg[len++] = (uint8_t)(PkbWarningJudgeV2_val);
	msg[len++] = (uint8_t)(PkbWarningJudgeV2_val>>8);
	msg[len++] = (uint8_t)(PkbWarningJudgeV2_val>>16);
	msg[len++] = (uint8_t)(PkbWarningJudgeV2_val>>24);
#endif // {EEP_CONTENT_PkbWarningJudgeV2_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_PkbWarningJudgeV2_ITEM_LEN > 1
void DevEep_Get_PkbWarningJudgeV2(EEP_HANDLE eep_handle,uint8_t *PkbWarningJudgeV2_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_PkbWarningJudgeV2_ITEM_LEN; i ++)
		{
			PkbWarningJudgeV2_val[i] = ap_eep->process_eep.PkbWarningJudgeV2[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_PkbWarningJudgeV2(EEP_HANDLE eep_handle,uint8_t const *PkbWarningJudgeV2_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_PkbWarningJudgeV2_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_PkbWarningJudgeV2_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_PkbWarningJudgeV2_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_PkbWarningJudgeV2_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_PkbWarningJudgeV2_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = PkbWarningJudgeV2_val[i];
#elif EEP_CONTENT_PkbWarningJudgeV2_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(PkbWarningJudgeV2_val[i]);
		msg[len++] = (uint8_t)(PkbWarningJudgeV2_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(PkbWarningJudgeV2_val[i]);
		msg[len++] = (uint8_t)(PkbWarningJudgeV2_val[i]>>8);
		msg[len++] = (uint8_t)(PkbWarningJudgeV2_val[i]>>16);
		msg[len++] = (uint8_t)(PkbWarningJudgeV2_val[i]>>24);
#endif // {EEP_CONTENT_PkbWarningJudgeV2_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_PkbWarningJudgeV2_ITEM_LEN}
void DevEep_Inject_ChgCalbk_PkbWarningJudgeV2(EEP_HANDLE eep_handle,PkbWarningJudgeV2_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_PkbWarningJudgeV2_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_PkbWarningJudgeV2_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_PkbWarningJudgeV2(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		PkbWarningJudgeV2_ChgCallBk PkbWarningJudgeV2_callback =  (PkbWarningJudgeV2_ChgCallBk)callbk;
		PkbWarningJudgeV2_callback(ap,ap->process_eep.PkbWarningJudgeV2);
	}
}

// api define for AudioEepTest /////////////////////////////////////////////////////////
#if EEP_CONTENT_AudioEepTest_ITEM_LEN == 1
uint16_t DevEep_Get_AudioEepTest(EEP_HANDLE eep_handle)
{
	uint16_t AudioEepTest_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		AudioEepTest_val = ap_eep->process_eep.AudioEepTest;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return AudioEepTest_val;
}
void DevEep_Set_AudioEepTest(EEP_HANDLE eep_handle,uint16_t const AudioEepTest_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_AudioEepTest_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_AudioEepTest_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_AudioEepTest_LIST_ID);
#if EEP_CONTENT_AudioEepTest_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = AudioEepTest_val;
#elif EEP_CONTENT_AudioEepTest_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(AudioEepTest_val);
	msg[len++] = (uint8_t)(AudioEepTest_val>>8);
#else 
	msg[len++] = (uint8_t)(AudioEepTest_val);
	msg[len++] = (uint8_t)(AudioEepTest_val>>8);
	msg[len++] = (uint8_t)(AudioEepTest_val>>16);
	msg[len++] = (uint8_t)(AudioEepTest_val>>24);
#endif // {EEP_CONTENT_AudioEepTest_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_AudioEepTest_ITEM_LEN > 1
void DevEep_Get_AudioEepTest(EEP_HANDLE eep_handle,uint16_t *AudioEepTest_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_AudioEepTest_ITEM_LEN; i ++)
		{
			AudioEepTest_val[i] = ap_eep->process_eep.AudioEepTest[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_AudioEepTest(EEP_HANDLE eep_handle,uint16_t const *AudioEepTest_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_AudioEepTest_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_AudioEepTest_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_AudioEepTest_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_AudioEepTest_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_AudioEepTest_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = AudioEepTest_val[i];
#elif EEP_CONTENT_AudioEepTest_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(AudioEepTest_val[i]);
		msg[len++] = (uint8_t)(AudioEepTest_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(AudioEepTest_val[i]);
		msg[len++] = (uint8_t)(AudioEepTest_val[i]>>8);
		msg[len++] = (uint8_t)(AudioEepTest_val[i]>>16);
		msg[len++] = (uint8_t)(AudioEepTest_val[i]>>24);
#endif // {EEP_CONTENT_AudioEepTest_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_AudioEepTest_ITEM_LEN}
void DevEep_Inject_ChgCalbk_AudioEepTest(EEP_HANDLE eep_handle,AudioEepTest_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_AudioEepTest_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_AudioEepTest_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_AudioEepTest(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		AudioEepTest_ChgCallBk AudioEepTest_callback =  (AudioEepTest_ChgCallBk)callbk;
		AudioEepTest_callback(ap,ap->process_eep.AudioEepTest);
	}
}

// api define for DspKeyCodeOnOff /////////////////////////////////////////////////////////
#if EEP_CONTENT_DspKeyCodeOnOff_ITEM_LEN == 1
uint8_t DevEep_Get_DspKeyCodeOnOff(EEP_HANDLE eep_handle)
{
	uint8_t DspKeyCodeOnOff_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		DspKeyCodeOnOff_val = ap_eep->process_eep.DspKeyCodeOnOff;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return DspKeyCodeOnOff_val;
}
void DevEep_Set_DspKeyCodeOnOff(EEP_HANDLE eep_handle,uint8_t const DspKeyCodeOnOff_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_DspKeyCodeOnOff_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_DspKeyCodeOnOff_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_DspKeyCodeOnOff_LIST_ID);
#if EEP_CONTENT_DspKeyCodeOnOff_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = DspKeyCodeOnOff_val;
#elif EEP_CONTENT_DspKeyCodeOnOff_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(DspKeyCodeOnOff_val);
	msg[len++] = (uint8_t)(DspKeyCodeOnOff_val>>8);
#else 
	msg[len++] = (uint8_t)(DspKeyCodeOnOff_val);
	msg[len++] = (uint8_t)(DspKeyCodeOnOff_val>>8);
	msg[len++] = (uint8_t)(DspKeyCodeOnOff_val>>16);
	msg[len++] = (uint8_t)(DspKeyCodeOnOff_val>>24);
#endif // {EEP_CONTENT_DspKeyCodeOnOff_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_DspKeyCodeOnOff_ITEM_LEN > 1
void DevEep_Get_DspKeyCodeOnOff(EEP_HANDLE eep_handle,uint8_t *DspKeyCodeOnOff_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_DspKeyCodeOnOff_ITEM_LEN; i ++)
		{
			DspKeyCodeOnOff_val[i] = ap_eep->process_eep.DspKeyCodeOnOff[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_DspKeyCodeOnOff(EEP_HANDLE eep_handle,uint8_t const *DspKeyCodeOnOff_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_DspKeyCodeOnOff_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_DspKeyCodeOnOff_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_DspKeyCodeOnOff_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_DspKeyCodeOnOff_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_DspKeyCodeOnOff_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = DspKeyCodeOnOff_val[i];
#elif EEP_CONTENT_DspKeyCodeOnOff_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(DspKeyCodeOnOff_val[i]);
		msg[len++] = (uint8_t)(DspKeyCodeOnOff_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(DspKeyCodeOnOff_val[i]);
		msg[len++] = (uint8_t)(DspKeyCodeOnOff_val[i]>>8);
		msg[len++] = (uint8_t)(DspKeyCodeOnOff_val[i]>>16);
		msg[len++] = (uint8_t)(DspKeyCodeOnOff_val[i]>>24);
#endif // {EEP_CONTENT_DspKeyCodeOnOff_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_DspKeyCodeOnOff_ITEM_LEN}
void DevEep_Inject_ChgCalbk_DspKeyCodeOnOff(EEP_HANDLE eep_handle,DspKeyCodeOnOff_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_DspKeyCodeOnOff_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_DspKeyCodeOnOff_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_DspKeyCodeOnOff(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		DspKeyCodeOnOff_ChgCallBk DspKeyCodeOnOff_callback =  (DspKeyCodeOnOff_ChgCallBk)callbk;
		DspKeyCodeOnOff_callback(ap,ap->process_eep.DspKeyCodeOnOff);
	}
}

// api define for DspKeyCode /////////////////////////////////////////////////////////
#if EEP_CONTENT_DspKeyCode_ITEM_LEN == 1
uint32_t DevEep_Get_DspKeyCode(EEP_HANDLE eep_handle)
{
	uint32_t DspKeyCode_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		DspKeyCode_val = ap_eep->process_eep.DspKeyCode;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return DspKeyCode_val;
}
void DevEep_Set_DspKeyCode(EEP_HANDLE eep_handle,uint32_t const DspKeyCode_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_DspKeyCode_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_DspKeyCode_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_DspKeyCode_LIST_ID);
#if EEP_CONTENT_DspKeyCode_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = DspKeyCode_val;
#elif EEP_CONTENT_DspKeyCode_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(DspKeyCode_val);
	msg[len++] = (uint8_t)(DspKeyCode_val>>8);
#else 
	msg[len++] = (uint8_t)(DspKeyCode_val);
	msg[len++] = (uint8_t)(DspKeyCode_val>>8);
	msg[len++] = (uint8_t)(DspKeyCode_val>>16);
	msg[len++] = (uint8_t)(DspKeyCode_val>>24);
#endif // {EEP_CONTENT_DspKeyCode_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_DspKeyCode_ITEM_LEN > 1
void DevEep_Get_DspKeyCode(EEP_HANDLE eep_handle,uint32_t *DspKeyCode_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_DspKeyCode_ITEM_LEN; i ++)
		{
			DspKeyCode_val[i] = ap_eep->process_eep.DspKeyCode[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_DspKeyCode(EEP_HANDLE eep_handle,uint32_t const *DspKeyCode_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_DspKeyCode_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_DspKeyCode_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_DspKeyCode_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_DspKeyCode_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_DspKeyCode_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = DspKeyCode_val[i];
#elif EEP_CONTENT_DspKeyCode_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(DspKeyCode_val[i]);
		msg[len++] = (uint8_t)(DspKeyCode_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(DspKeyCode_val[i]);
		msg[len++] = (uint8_t)(DspKeyCode_val[i]>>8);
		msg[len++] = (uint8_t)(DspKeyCode_val[i]>>16);
		msg[len++] = (uint8_t)(DspKeyCode_val[i]>>24);
#endif // {EEP_CONTENT_DspKeyCode_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_DspKeyCode_ITEM_LEN}
void DevEep_Inject_ChgCalbk_DspKeyCode(EEP_HANDLE eep_handle,DspKeyCode_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_DspKeyCode_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_DspKeyCode_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_DspKeyCode(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		DspKeyCode_ChgCallBk DspKeyCode_callback =  (DspKeyCode_ChgCallBk)callbk;
		DspKeyCode_callback(ap,ap->process_eep.DspKeyCode);
	}
}

// api define for BatState_ChgDly /////////////////////////////////////////////////////////
#if EEP_CONTENT_BatState_ChgDly_ITEM_LEN == 1
uint16_t DevEep_Get_BatState_ChgDly(EEP_HANDLE eep_handle)
{
	uint16_t BatState_ChgDly_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		BatState_ChgDly_val = ap_eep->process_eep.BatState_ChgDly;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return BatState_ChgDly_val;
}
void DevEep_Set_BatState_ChgDly(EEP_HANDLE eep_handle,uint16_t const BatState_ChgDly_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_BatState_ChgDly_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_BatState_ChgDly_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_BatState_ChgDly_LIST_ID);
#if EEP_CONTENT_BatState_ChgDly_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = BatState_ChgDly_val;
#elif EEP_CONTENT_BatState_ChgDly_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(BatState_ChgDly_val);
	msg[len++] = (uint8_t)(BatState_ChgDly_val>>8);
#else 
	msg[len++] = (uint8_t)(BatState_ChgDly_val);
	msg[len++] = (uint8_t)(BatState_ChgDly_val>>8);
	msg[len++] = (uint8_t)(BatState_ChgDly_val>>16);
	msg[len++] = (uint8_t)(BatState_ChgDly_val>>24);
#endif // {EEP_CONTENT_BatState_ChgDly_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_BatState_ChgDly_ITEM_LEN > 1
void DevEep_Get_BatState_ChgDly(EEP_HANDLE eep_handle,uint16_t *BatState_ChgDly_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_BatState_ChgDly_ITEM_LEN; i ++)
		{
			BatState_ChgDly_val[i] = ap_eep->process_eep.BatState_ChgDly[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_BatState_ChgDly(EEP_HANDLE eep_handle,uint16_t const *BatState_ChgDly_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_BatState_ChgDly_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_BatState_ChgDly_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_BatState_ChgDly_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_BatState_ChgDly_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_BatState_ChgDly_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = BatState_ChgDly_val[i];
#elif EEP_CONTENT_BatState_ChgDly_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(BatState_ChgDly_val[i]);
		msg[len++] = (uint8_t)(BatState_ChgDly_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(BatState_ChgDly_val[i]);
		msg[len++] = (uint8_t)(BatState_ChgDly_val[i]>>8);
		msg[len++] = (uint8_t)(BatState_ChgDly_val[i]>>16);
		msg[len++] = (uint8_t)(BatState_ChgDly_val[i]>>24);
#endif // {EEP_CONTENT_BatState_ChgDly_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_BatState_ChgDly_ITEM_LEN}
void DevEep_Inject_ChgCalbk_BatState_ChgDly(EEP_HANDLE eep_handle,BatState_ChgDly_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_BatState_ChgDly_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_BatState_ChgDly_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_BatState_ChgDly(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		BatState_ChgDly_ChgCallBk BatState_ChgDly_callback =  (BatState_ChgDly_ChgCallBk)callbk;
		BatState_ChgDly_callback(ap,ap->process_eep.BatState_ChgDly);
	}
}

// api define for BatVolVeryHigh_Hysteresis_H /////////////////////////////////////////////////////////
#if EEP_CONTENT_BatVolVeryHigh_Hysteresis_H_ITEM_LEN == 1
uint16_t DevEep_Get_BatVolVeryHigh_Hysteresis_H(EEP_HANDLE eep_handle)
{
	uint16_t BatVolVeryHigh_Hysteresis_H_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		BatVolVeryHigh_Hysteresis_H_val = ap_eep->process_eep.BatVolVeryHigh_Hysteresis_H;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return BatVolVeryHigh_Hysteresis_H_val;
}
void DevEep_Set_BatVolVeryHigh_Hysteresis_H(EEP_HANDLE eep_handle,uint16_t const BatVolVeryHigh_Hysteresis_H_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_BatVolVeryHigh_Hysteresis_H_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_BatVolVeryHigh_Hysteresis_H_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_BatVolVeryHigh_Hysteresis_H_LIST_ID);
#if EEP_CONTENT_BatVolVeryHigh_Hysteresis_H_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = BatVolVeryHigh_Hysteresis_H_val;
#elif EEP_CONTENT_BatVolVeryHigh_Hysteresis_H_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(BatVolVeryHigh_Hysteresis_H_val);
	msg[len++] = (uint8_t)(BatVolVeryHigh_Hysteresis_H_val>>8);
#else 
	msg[len++] = (uint8_t)(BatVolVeryHigh_Hysteresis_H_val);
	msg[len++] = (uint8_t)(BatVolVeryHigh_Hysteresis_H_val>>8);
	msg[len++] = (uint8_t)(BatVolVeryHigh_Hysteresis_H_val>>16);
	msg[len++] = (uint8_t)(BatVolVeryHigh_Hysteresis_H_val>>24);
#endif // {EEP_CONTENT_BatVolVeryHigh_Hysteresis_H_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_BatVolVeryHigh_Hysteresis_H_ITEM_LEN > 1
void DevEep_Get_BatVolVeryHigh_Hysteresis_H(EEP_HANDLE eep_handle,uint16_t *BatVolVeryHigh_Hysteresis_H_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_BatVolVeryHigh_Hysteresis_H_ITEM_LEN; i ++)
		{
			BatVolVeryHigh_Hysteresis_H_val[i] = ap_eep->process_eep.BatVolVeryHigh_Hysteresis_H[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_BatVolVeryHigh_Hysteresis_H(EEP_HANDLE eep_handle,uint16_t const *BatVolVeryHigh_Hysteresis_H_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_BatVolVeryHigh_Hysteresis_H_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_BatVolVeryHigh_Hysteresis_H_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_BatVolVeryHigh_Hysteresis_H_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_BatVolVeryHigh_Hysteresis_H_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_BatVolVeryHigh_Hysteresis_H_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = BatVolVeryHigh_Hysteresis_H_val[i];
#elif EEP_CONTENT_BatVolVeryHigh_Hysteresis_H_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(BatVolVeryHigh_Hysteresis_H_val[i]);
		msg[len++] = (uint8_t)(BatVolVeryHigh_Hysteresis_H_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(BatVolVeryHigh_Hysteresis_H_val[i]);
		msg[len++] = (uint8_t)(BatVolVeryHigh_Hysteresis_H_val[i]>>8);
		msg[len++] = (uint8_t)(BatVolVeryHigh_Hysteresis_H_val[i]>>16);
		msg[len++] = (uint8_t)(BatVolVeryHigh_Hysteresis_H_val[i]>>24);
#endif // {EEP_CONTENT_BatVolVeryHigh_Hysteresis_H_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_BatVolVeryHigh_Hysteresis_H_ITEM_LEN}
void DevEep_Inject_ChgCalbk_BatVolVeryHigh_Hysteresis_H(EEP_HANDLE eep_handle,BatVolVeryHigh_Hysteresis_H_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_BatVolVeryHigh_Hysteresis_H_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_BatVolVeryHigh_Hysteresis_H_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_BatVolVeryHigh_Hysteresis_H(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		BatVolVeryHigh_Hysteresis_H_ChgCallBk BatVolVeryHigh_Hysteresis_H_callback =  (BatVolVeryHigh_Hysteresis_H_ChgCallBk)callbk;
		BatVolVeryHigh_Hysteresis_H_callback(ap,ap->process_eep.BatVolVeryHigh_Hysteresis_H);
	}
}

// api define for BatVolVeryHigh_Hysteresis_L /////////////////////////////////////////////////////////
#if EEP_CONTENT_BatVolVeryHigh_Hysteresis_L_ITEM_LEN == 1
uint16_t DevEep_Get_BatVolVeryHigh_Hysteresis_L(EEP_HANDLE eep_handle)
{
	uint16_t BatVolVeryHigh_Hysteresis_L_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		BatVolVeryHigh_Hysteresis_L_val = ap_eep->process_eep.BatVolVeryHigh_Hysteresis_L;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return BatVolVeryHigh_Hysteresis_L_val;
}
void DevEep_Set_BatVolVeryHigh_Hysteresis_L(EEP_HANDLE eep_handle,uint16_t const BatVolVeryHigh_Hysteresis_L_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_BatVolVeryHigh_Hysteresis_L_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_BatVolVeryHigh_Hysteresis_L_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_BatVolVeryHigh_Hysteresis_L_LIST_ID);
#if EEP_CONTENT_BatVolVeryHigh_Hysteresis_L_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = BatVolVeryHigh_Hysteresis_L_val;
#elif EEP_CONTENT_BatVolVeryHigh_Hysteresis_L_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(BatVolVeryHigh_Hysteresis_L_val);
	msg[len++] = (uint8_t)(BatVolVeryHigh_Hysteresis_L_val>>8);
#else 
	msg[len++] = (uint8_t)(BatVolVeryHigh_Hysteresis_L_val);
	msg[len++] = (uint8_t)(BatVolVeryHigh_Hysteresis_L_val>>8);
	msg[len++] = (uint8_t)(BatVolVeryHigh_Hysteresis_L_val>>16);
	msg[len++] = (uint8_t)(BatVolVeryHigh_Hysteresis_L_val>>24);
#endif // {EEP_CONTENT_BatVolVeryHigh_Hysteresis_L_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_BatVolVeryHigh_Hysteresis_L_ITEM_LEN > 1
void DevEep_Get_BatVolVeryHigh_Hysteresis_L(EEP_HANDLE eep_handle,uint16_t *BatVolVeryHigh_Hysteresis_L_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_BatVolVeryHigh_Hysteresis_L_ITEM_LEN; i ++)
		{
			BatVolVeryHigh_Hysteresis_L_val[i] = ap_eep->process_eep.BatVolVeryHigh_Hysteresis_L[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_BatVolVeryHigh_Hysteresis_L(EEP_HANDLE eep_handle,uint16_t const *BatVolVeryHigh_Hysteresis_L_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_BatVolVeryHigh_Hysteresis_L_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_BatVolVeryHigh_Hysteresis_L_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_BatVolVeryHigh_Hysteresis_L_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_BatVolVeryHigh_Hysteresis_L_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_BatVolVeryHigh_Hysteresis_L_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = BatVolVeryHigh_Hysteresis_L_val[i];
#elif EEP_CONTENT_BatVolVeryHigh_Hysteresis_L_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(BatVolVeryHigh_Hysteresis_L_val[i]);
		msg[len++] = (uint8_t)(BatVolVeryHigh_Hysteresis_L_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(BatVolVeryHigh_Hysteresis_L_val[i]);
		msg[len++] = (uint8_t)(BatVolVeryHigh_Hysteresis_L_val[i]>>8);
		msg[len++] = (uint8_t)(BatVolVeryHigh_Hysteresis_L_val[i]>>16);
		msg[len++] = (uint8_t)(BatVolVeryHigh_Hysteresis_L_val[i]>>24);
#endif // {EEP_CONTENT_BatVolVeryHigh_Hysteresis_L_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_BatVolVeryHigh_Hysteresis_L_ITEM_LEN}
void DevEep_Inject_ChgCalbk_BatVolVeryHigh_Hysteresis_L(EEP_HANDLE eep_handle,BatVolVeryHigh_Hysteresis_L_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_BatVolVeryHigh_Hysteresis_L_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_BatVolVeryHigh_Hysteresis_L_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_BatVolVeryHigh_Hysteresis_L(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		BatVolVeryHigh_Hysteresis_L_ChgCallBk BatVolVeryHigh_Hysteresis_L_callback =  (BatVolVeryHigh_Hysteresis_L_ChgCallBk)callbk;
		BatVolVeryHigh_Hysteresis_L_callback(ap,ap->process_eep.BatVolVeryHigh_Hysteresis_L);
	}
}

// api define for BatVolHigh_Hysteresis_H /////////////////////////////////////////////////////////
#if EEP_CONTENT_BatVolHigh_Hysteresis_H_ITEM_LEN == 1
uint16_t DevEep_Get_BatVolHigh_Hysteresis_H(EEP_HANDLE eep_handle)
{
	uint16_t BatVolHigh_Hysteresis_H_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		BatVolHigh_Hysteresis_H_val = ap_eep->process_eep.BatVolHigh_Hysteresis_H;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return BatVolHigh_Hysteresis_H_val;
}
void DevEep_Set_BatVolHigh_Hysteresis_H(EEP_HANDLE eep_handle,uint16_t const BatVolHigh_Hysteresis_H_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_BatVolHigh_Hysteresis_H_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_BatVolHigh_Hysteresis_H_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_BatVolHigh_Hysteresis_H_LIST_ID);
#if EEP_CONTENT_BatVolHigh_Hysteresis_H_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = BatVolHigh_Hysteresis_H_val;
#elif EEP_CONTENT_BatVolHigh_Hysteresis_H_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(BatVolHigh_Hysteresis_H_val);
	msg[len++] = (uint8_t)(BatVolHigh_Hysteresis_H_val>>8);
#else 
	msg[len++] = (uint8_t)(BatVolHigh_Hysteresis_H_val);
	msg[len++] = (uint8_t)(BatVolHigh_Hysteresis_H_val>>8);
	msg[len++] = (uint8_t)(BatVolHigh_Hysteresis_H_val>>16);
	msg[len++] = (uint8_t)(BatVolHigh_Hysteresis_H_val>>24);
#endif // {EEP_CONTENT_BatVolHigh_Hysteresis_H_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_BatVolHigh_Hysteresis_H_ITEM_LEN > 1
void DevEep_Get_BatVolHigh_Hysteresis_H(EEP_HANDLE eep_handle,uint16_t *BatVolHigh_Hysteresis_H_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_BatVolHigh_Hysteresis_H_ITEM_LEN; i ++)
		{
			BatVolHigh_Hysteresis_H_val[i] = ap_eep->process_eep.BatVolHigh_Hysteresis_H[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_BatVolHigh_Hysteresis_H(EEP_HANDLE eep_handle,uint16_t const *BatVolHigh_Hysteresis_H_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_BatVolHigh_Hysteresis_H_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_BatVolHigh_Hysteresis_H_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_BatVolHigh_Hysteresis_H_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_BatVolHigh_Hysteresis_H_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_BatVolHigh_Hysteresis_H_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = BatVolHigh_Hysteresis_H_val[i];
#elif EEP_CONTENT_BatVolHigh_Hysteresis_H_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(BatVolHigh_Hysteresis_H_val[i]);
		msg[len++] = (uint8_t)(BatVolHigh_Hysteresis_H_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(BatVolHigh_Hysteresis_H_val[i]);
		msg[len++] = (uint8_t)(BatVolHigh_Hysteresis_H_val[i]>>8);
		msg[len++] = (uint8_t)(BatVolHigh_Hysteresis_H_val[i]>>16);
		msg[len++] = (uint8_t)(BatVolHigh_Hysteresis_H_val[i]>>24);
#endif // {EEP_CONTENT_BatVolHigh_Hysteresis_H_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_BatVolHigh_Hysteresis_H_ITEM_LEN}
void DevEep_Inject_ChgCalbk_BatVolHigh_Hysteresis_H(EEP_HANDLE eep_handle,BatVolHigh_Hysteresis_H_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_BatVolHigh_Hysteresis_H_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_BatVolHigh_Hysteresis_H_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_BatVolHigh_Hysteresis_H(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		BatVolHigh_Hysteresis_H_ChgCallBk BatVolHigh_Hysteresis_H_callback =  (BatVolHigh_Hysteresis_H_ChgCallBk)callbk;
		BatVolHigh_Hysteresis_H_callback(ap,ap->process_eep.BatVolHigh_Hysteresis_H);
	}
}

// api define for BatVolHigh_Hysteresis_L /////////////////////////////////////////////////////////
#if EEP_CONTENT_BatVolHigh_Hysteresis_L_ITEM_LEN == 1
uint16_t DevEep_Get_BatVolHigh_Hysteresis_L(EEP_HANDLE eep_handle)
{
	uint16_t BatVolHigh_Hysteresis_L_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		BatVolHigh_Hysteresis_L_val = ap_eep->process_eep.BatVolHigh_Hysteresis_L;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return BatVolHigh_Hysteresis_L_val;
}
void DevEep_Set_BatVolHigh_Hysteresis_L(EEP_HANDLE eep_handle,uint16_t const BatVolHigh_Hysteresis_L_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_BatVolHigh_Hysteresis_L_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_BatVolHigh_Hysteresis_L_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_BatVolHigh_Hysteresis_L_LIST_ID);
#if EEP_CONTENT_BatVolHigh_Hysteresis_L_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = BatVolHigh_Hysteresis_L_val;
#elif EEP_CONTENT_BatVolHigh_Hysteresis_L_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(BatVolHigh_Hysteresis_L_val);
	msg[len++] = (uint8_t)(BatVolHigh_Hysteresis_L_val>>8);
#else 
	msg[len++] = (uint8_t)(BatVolHigh_Hysteresis_L_val);
	msg[len++] = (uint8_t)(BatVolHigh_Hysteresis_L_val>>8);
	msg[len++] = (uint8_t)(BatVolHigh_Hysteresis_L_val>>16);
	msg[len++] = (uint8_t)(BatVolHigh_Hysteresis_L_val>>24);
#endif // {EEP_CONTENT_BatVolHigh_Hysteresis_L_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_BatVolHigh_Hysteresis_L_ITEM_LEN > 1
void DevEep_Get_BatVolHigh_Hysteresis_L(EEP_HANDLE eep_handle,uint16_t *BatVolHigh_Hysteresis_L_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_BatVolHigh_Hysteresis_L_ITEM_LEN; i ++)
		{
			BatVolHigh_Hysteresis_L_val[i] = ap_eep->process_eep.BatVolHigh_Hysteresis_L[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_BatVolHigh_Hysteresis_L(EEP_HANDLE eep_handle,uint16_t const *BatVolHigh_Hysteresis_L_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_BatVolHigh_Hysteresis_L_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_BatVolHigh_Hysteresis_L_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_BatVolHigh_Hysteresis_L_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_BatVolHigh_Hysteresis_L_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_BatVolHigh_Hysteresis_L_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = BatVolHigh_Hysteresis_L_val[i];
#elif EEP_CONTENT_BatVolHigh_Hysteresis_L_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(BatVolHigh_Hysteresis_L_val[i]);
		msg[len++] = (uint8_t)(BatVolHigh_Hysteresis_L_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(BatVolHigh_Hysteresis_L_val[i]);
		msg[len++] = (uint8_t)(BatVolHigh_Hysteresis_L_val[i]>>8);
		msg[len++] = (uint8_t)(BatVolHigh_Hysteresis_L_val[i]>>16);
		msg[len++] = (uint8_t)(BatVolHigh_Hysteresis_L_val[i]>>24);
#endif // {EEP_CONTENT_BatVolHigh_Hysteresis_L_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_BatVolHigh_Hysteresis_L_ITEM_LEN}
void DevEep_Inject_ChgCalbk_BatVolHigh_Hysteresis_L(EEP_HANDLE eep_handle,BatVolHigh_Hysteresis_L_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_BatVolHigh_Hysteresis_L_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_BatVolHigh_Hysteresis_L_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_BatVolHigh_Hysteresis_L(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		BatVolHigh_Hysteresis_L_ChgCallBk BatVolHigh_Hysteresis_L_callback =  (BatVolHigh_Hysteresis_L_ChgCallBk)callbk;
		BatVolHigh_Hysteresis_L_callback(ap,ap->process_eep.BatVolHigh_Hysteresis_L);
	}
}

// api define for BatVolLow_Hysteresis_H /////////////////////////////////////////////////////////
#if EEP_CONTENT_BatVolLow_Hysteresis_H_ITEM_LEN == 1
uint16_t DevEep_Get_BatVolLow_Hysteresis_H(EEP_HANDLE eep_handle)
{
	uint16_t BatVolLow_Hysteresis_H_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		BatVolLow_Hysteresis_H_val = ap_eep->process_eep.BatVolLow_Hysteresis_H;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return BatVolLow_Hysteresis_H_val;
}
void DevEep_Set_BatVolLow_Hysteresis_H(EEP_HANDLE eep_handle,uint16_t const BatVolLow_Hysteresis_H_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_BatVolLow_Hysteresis_H_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_BatVolLow_Hysteresis_H_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_BatVolLow_Hysteresis_H_LIST_ID);
#if EEP_CONTENT_BatVolLow_Hysteresis_H_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = BatVolLow_Hysteresis_H_val;
#elif EEP_CONTENT_BatVolLow_Hysteresis_H_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(BatVolLow_Hysteresis_H_val);
	msg[len++] = (uint8_t)(BatVolLow_Hysteresis_H_val>>8);
#else 
	msg[len++] = (uint8_t)(BatVolLow_Hysteresis_H_val);
	msg[len++] = (uint8_t)(BatVolLow_Hysteresis_H_val>>8);
	msg[len++] = (uint8_t)(BatVolLow_Hysteresis_H_val>>16);
	msg[len++] = (uint8_t)(BatVolLow_Hysteresis_H_val>>24);
#endif // {EEP_CONTENT_BatVolLow_Hysteresis_H_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_BatVolLow_Hysteresis_H_ITEM_LEN > 1
void DevEep_Get_BatVolLow_Hysteresis_H(EEP_HANDLE eep_handle,uint16_t *BatVolLow_Hysteresis_H_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_BatVolLow_Hysteresis_H_ITEM_LEN; i ++)
		{
			BatVolLow_Hysteresis_H_val[i] = ap_eep->process_eep.BatVolLow_Hysteresis_H[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_BatVolLow_Hysteresis_H(EEP_HANDLE eep_handle,uint16_t const *BatVolLow_Hysteresis_H_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_BatVolLow_Hysteresis_H_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_BatVolLow_Hysteresis_H_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_BatVolLow_Hysteresis_H_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_BatVolLow_Hysteresis_H_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_BatVolLow_Hysteresis_H_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = BatVolLow_Hysteresis_H_val[i];
#elif EEP_CONTENT_BatVolLow_Hysteresis_H_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(BatVolLow_Hysteresis_H_val[i]);
		msg[len++] = (uint8_t)(BatVolLow_Hysteresis_H_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(BatVolLow_Hysteresis_H_val[i]);
		msg[len++] = (uint8_t)(BatVolLow_Hysteresis_H_val[i]>>8);
		msg[len++] = (uint8_t)(BatVolLow_Hysteresis_H_val[i]>>16);
		msg[len++] = (uint8_t)(BatVolLow_Hysteresis_H_val[i]>>24);
#endif // {EEP_CONTENT_BatVolLow_Hysteresis_H_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_BatVolLow_Hysteresis_H_ITEM_LEN}
void DevEep_Inject_ChgCalbk_BatVolLow_Hysteresis_H(EEP_HANDLE eep_handle,BatVolLow_Hysteresis_H_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_BatVolLow_Hysteresis_H_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_BatVolLow_Hysteresis_H_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_BatVolLow_Hysteresis_H(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		BatVolLow_Hysteresis_H_ChgCallBk BatVolLow_Hysteresis_H_callback =  (BatVolLow_Hysteresis_H_ChgCallBk)callbk;
		BatVolLow_Hysteresis_H_callback(ap,ap->process_eep.BatVolLow_Hysteresis_H);
	}
}

// api define for BatVolLow_Hysteresis_L /////////////////////////////////////////////////////////
#if EEP_CONTENT_BatVolLow_Hysteresis_L_ITEM_LEN == 1
uint16_t DevEep_Get_BatVolLow_Hysteresis_L(EEP_HANDLE eep_handle)
{
	uint16_t BatVolLow_Hysteresis_L_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		BatVolLow_Hysteresis_L_val = ap_eep->process_eep.BatVolLow_Hysteresis_L;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return BatVolLow_Hysteresis_L_val;
}
void DevEep_Set_BatVolLow_Hysteresis_L(EEP_HANDLE eep_handle,uint16_t const BatVolLow_Hysteresis_L_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_BatVolLow_Hysteresis_L_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_BatVolLow_Hysteresis_L_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_BatVolLow_Hysteresis_L_LIST_ID);
#if EEP_CONTENT_BatVolLow_Hysteresis_L_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = BatVolLow_Hysteresis_L_val;
#elif EEP_CONTENT_BatVolLow_Hysteresis_L_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(BatVolLow_Hysteresis_L_val);
	msg[len++] = (uint8_t)(BatVolLow_Hysteresis_L_val>>8);
#else 
	msg[len++] = (uint8_t)(BatVolLow_Hysteresis_L_val);
	msg[len++] = (uint8_t)(BatVolLow_Hysteresis_L_val>>8);
	msg[len++] = (uint8_t)(BatVolLow_Hysteresis_L_val>>16);
	msg[len++] = (uint8_t)(BatVolLow_Hysteresis_L_val>>24);
#endif // {EEP_CONTENT_BatVolLow_Hysteresis_L_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_BatVolLow_Hysteresis_L_ITEM_LEN > 1
void DevEep_Get_BatVolLow_Hysteresis_L(EEP_HANDLE eep_handle,uint16_t *BatVolLow_Hysteresis_L_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_BatVolLow_Hysteresis_L_ITEM_LEN; i ++)
		{
			BatVolLow_Hysteresis_L_val[i] = ap_eep->process_eep.BatVolLow_Hysteresis_L[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_BatVolLow_Hysteresis_L(EEP_HANDLE eep_handle,uint16_t const *BatVolLow_Hysteresis_L_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_BatVolLow_Hysteresis_L_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_BatVolLow_Hysteresis_L_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_BatVolLow_Hysteresis_L_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_BatVolLow_Hysteresis_L_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_BatVolLow_Hysteresis_L_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = BatVolLow_Hysteresis_L_val[i];
#elif EEP_CONTENT_BatVolLow_Hysteresis_L_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(BatVolLow_Hysteresis_L_val[i]);
		msg[len++] = (uint8_t)(BatVolLow_Hysteresis_L_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(BatVolLow_Hysteresis_L_val[i]);
		msg[len++] = (uint8_t)(BatVolLow_Hysteresis_L_val[i]>>8);
		msg[len++] = (uint8_t)(BatVolLow_Hysteresis_L_val[i]>>16);
		msg[len++] = (uint8_t)(BatVolLow_Hysteresis_L_val[i]>>24);
#endif // {EEP_CONTENT_BatVolLow_Hysteresis_L_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_BatVolLow_Hysteresis_L_ITEM_LEN}
void DevEep_Inject_ChgCalbk_BatVolLow_Hysteresis_L(EEP_HANDLE eep_handle,BatVolLow_Hysteresis_L_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_BatVolLow_Hysteresis_L_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_BatVolLow_Hysteresis_L_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_BatVolLow_Hysteresis_L(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		BatVolLow_Hysteresis_L_ChgCallBk BatVolLow_Hysteresis_L_callback =  (BatVolLow_Hysteresis_L_ChgCallBk)callbk;
		BatVolLow_Hysteresis_L_callback(ap,ap->process_eep.BatVolLow_Hysteresis_L);
	}
}

// api define for BatVolVeryLow_Hysteresis_H /////////////////////////////////////////////////////////
#if EEP_CONTENT_BatVolVeryLow_Hysteresis_H_ITEM_LEN == 1
uint16_t DevEep_Get_BatVolVeryLow_Hysteresis_H(EEP_HANDLE eep_handle)
{
	uint16_t BatVolVeryLow_Hysteresis_H_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		BatVolVeryLow_Hysteresis_H_val = ap_eep->process_eep.BatVolVeryLow_Hysteresis_H;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return BatVolVeryLow_Hysteresis_H_val;
}
void DevEep_Set_BatVolVeryLow_Hysteresis_H(EEP_HANDLE eep_handle,uint16_t const BatVolVeryLow_Hysteresis_H_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_BatVolVeryLow_Hysteresis_H_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_BatVolVeryLow_Hysteresis_H_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_BatVolVeryLow_Hysteresis_H_LIST_ID);
#if EEP_CONTENT_BatVolVeryLow_Hysteresis_H_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = BatVolVeryLow_Hysteresis_H_val;
#elif EEP_CONTENT_BatVolVeryLow_Hysteresis_H_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(BatVolVeryLow_Hysteresis_H_val);
	msg[len++] = (uint8_t)(BatVolVeryLow_Hysteresis_H_val>>8);
#else 
	msg[len++] = (uint8_t)(BatVolVeryLow_Hysteresis_H_val);
	msg[len++] = (uint8_t)(BatVolVeryLow_Hysteresis_H_val>>8);
	msg[len++] = (uint8_t)(BatVolVeryLow_Hysteresis_H_val>>16);
	msg[len++] = (uint8_t)(BatVolVeryLow_Hysteresis_H_val>>24);
#endif // {EEP_CONTENT_BatVolVeryLow_Hysteresis_H_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_BatVolVeryLow_Hysteresis_H_ITEM_LEN > 1
void DevEep_Get_BatVolVeryLow_Hysteresis_H(EEP_HANDLE eep_handle,uint16_t *BatVolVeryLow_Hysteresis_H_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_BatVolVeryLow_Hysteresis_H_ITEM_LEN; i ++)
		{
			BatVolVeryLow_Hysteresis_H_val[i] = ap_eep->process_eep.BatVolVeryLow_Hysteresis_H[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_BatVolVeryLow_Hysteresis_H(EEP_HANDLE eep_handle,uint16_t const *BatVolVeryLow_Hysteresis_H_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_BatVolVeryLow_Hysteresis_H_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_BatVolVeryLow_Hysteresis_H_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_BatVolVeryLow_Hysteresis_H_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_BatVolVeryLow_Hysteresis_H_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_BatVolVeryLow_Hysteresis_H_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = BatVolVeryLow_Hysteresis_H_val[i];
#elif EEP_CONTENT_BatVolVeryLow_Hysteresis_H_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(BatVolVeryLow_Hysteresis_H_val[i]);
		msg[len++] = (uint8_t)(BatVolVeryLow_Hysteresis_H_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(BatVolVeryLow_Hysteresis_H_val[i]);
		msg[len++] = (uint8_t)(BatVolVeryLow_Hysteresis_H_val[i]>>8);
		msg[len++] = (uint8_t)(BatVolVeryLow_Hysteresis_H_val[i]>>16);
		msg[len++] = (uint8_t)(BatVolVeryLow_Hysteresis_H_val[i]>>24);
#endif // {EEP_CONTENT_BatVolVeryLow_Hysteresis_H_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_BatVolVeryLow_Hysteresis_H_ITEM_LEN}
void DevEep_Inject_ChgCalbk_BatVolVeryLow_Hysteresis_H(EEP_HANDLE eep_handle,BatVolVeryLow_Hysteresis_H_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_BatVolVeryLow_Hysteresis_H_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_BatVolVeryLow_Hysteresis_H_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_BatVolVeryLow_Hysteresis_H(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		BatVolVeryLow_Hysteresis_H_ChgCallBk BatVolVeryLow_Hysteresis_H_callback =  (BatVolVeryLow_Hysteresis_H_ChgCallBk)callbk;
		BatVolVeryLow_Hysteresis_H_callback(ap,ap->process_eep.BatVolVeryLow_Hysteresis_H);
	}
}

// api define for BatVolVeryLow_Hysteresis_L /////////////////////////////////////////////////////////
#if EEP_CONTENT_BatVolVeryLow_Hysteresis_L_ITEM_LEN == 1
uint16_t DevEep_Get_BatVolVeryLow_Hysteresis_L(EEP_HANDLE eep_handle)
{
	uint16_t BatVolVeryLow_Hysteresis_L_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		BatVolVeryLow_Hysteresis_L_val = ap_eep->process_eep.BatVolVeryLow_Hysteresis_L;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return BatVolVeryLow_Hysteresis_L_val;
}
void DevEep_Set_BatVolVeryLow_Hysteresis_L(EEP_HANDLE eep_handle,uint16_t const BatVolVeryLow_Hysteresis_L_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_BatVolVeryLow_Hysteresis_L_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_BatVolVeryLow_Hysteresis_L_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_BatVolVeryLow_Hysteresis_L_LIST_ID);
#if EEP_CONTENT_BatVolVeryLow_Hysteresis_L_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = BatVolVeryLow_Hysteresis_L_val;
#elif EEP_CONTENT_BatVolVeryLow_Hysteresis_L_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(BatVolVeryLow_Hysteresis_L_val);
	msg[len++] = (uint8_t)(BatVolVeryLow_Hysteresis_L_val>>8);
#else 
	msg[len++] = (uint8_t)(BatVolVeryLow_Hysteresis_L_val);
	msg[len++] = (uint8_t)(BatVolVeryLow_Hysteresis_L_val>>8);
	msg[len++] = (uint8_t)(BatVolVeryLow_Hysteresis_L_val>>16);
	msg[len++] = (uint8_t)(BatVolVeryLow_Hysteresis_L_val>>24);
#endif // {EEP_CONTENT_BatVolVeryLow_Hysteresis_L_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_BatVolVeryLow_Hysteresis_L_ITEM_LEN > 1
void DevEep_Get_BatVolVeryLow_Hysteresis_L(EEP_HANDLE eep_handle,uint16_t *BatVolVeryLow_Hysteresis_L_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_BatVolVeryLow_Hysteresis_L_ITEM_LEN; i ++)
		{
			BatVolVeryLow_Hysteresis_L_val[i] = ap_eep->process_eep.BatVolVeryLow_Hysteresis_L[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_BatVolVeryLow_Hysteresis_L(EEP_HANDLE eep_handle,uint16_t const *BatVolVeryLow_Hysteresis_L_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_BatVolVeryLow_Hysteresis_L_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_BatVolVeryLow_Hysteresis_L_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_BatVolVeryLow_Hysteresis_L_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_BatVolVeryLow_Hysteresis_L_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_BatVolVeryLow_Hysteresis_L_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = BatVolVeryLow_Hysteresis_L_val[i];
#elif EEP_CONTENT_BatVolVeryLow_Hysteresis_L_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(BatVolVeryLow_Hysteresis_L_val[i]);
		msg[len++] = (uint8_t)(BatVolVeryLow_Hysteresis_L_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(BatVolVeryLow_Hysteresis_L_val[i]);
		msg[len++] = (uint8_t)(BatVolVeryLow_Hysteresis_L_val[i]>>8);
		msg[len++] = (uint8_t)(BatVolVeryLow_Hysteresis_L_val[i]>>16);
		msg[len++] = (uint8_t)(BatVolVeryLow_Hysteresis_L_val[i]>>24);
#endif // {EEP_CONTENT_BatVolVeryLow_Hysteresis_L_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_BatVolVeryLow_Hysteresis_L_ITEM_LEN}
void DevEep_Inject_ChgCalbk_BatVolVeryLow_Hysteresis_L(EEP_HANDLE eep_handle,BatVolVeryLow_Hysteresis_L_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_BatVolVeryLow_Hysteresis_L_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_BatVolVeryLow_Hysteresis_L_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_BatVolVeryLow_Hysteresis_L(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		BatVolVeryLow_Hysteresis_L_ChgCallBk BatVolVeryLow_Hysteresis_L_callback =  (BatVolVeryLow_Hysteresis_L_ChgCallBk)callbk;
		BatVolVeryLow_Hysteresis_L_callback(ap,ap->process_eep.BatVolVeryLow_Hysteresis_L);
	}
}

// api define for TempState_ChgDly /////////////////////////////////////////////////////////
#if EEP_CONTENT_TempState_ChgDly_ITEM_LEN == 1
uint8_t DevEep_Get_TempState_ChgDly(EEP_HANDLE eep_handle)
{
	uint8_t TempState_ChgDly_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		TempState_ChgDly_val = ap_eep->process_eep.TempState_ChgDly;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return TempState_ChgDly_val;
}
void DevEep_Set_TempState_ChgDly(EEP_HANDLE eep_handle,uint8_t const TempState_ChgDly_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_TempState_ChgDly_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_TempState_ChgDly_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_TempState_ChgDly_LIST_ID);
#if EEP_CONTENT_TempState_ChgDly_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = TempState_ChgDly_val;
#elif EEP_CONTENT_TempState_ChgDly_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(TempState_ChgDly_val);
	msg[len++] = (uint8_t)(TempState_ChgDly_val>>8);
#else 
	msg[len++] = (uint8_t)(TempState_ChgDly_val);
	msg[len++] = (uint8_t)(TempState_ChgDly_val>>8);
	msg[len++] = (uint8_t)(TempState_ChgDly_val>>16);
	msg[len++] = (uint8_t)(TempState_ChgDly_val>>24);
#endif // {EEP_CONTENT_TempState_ChgDly_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_TempState_ChgDly_ITEM_LEN > 1
void DevEep_Get_TempState_ChgDly(EEP_HANDLE eep_handle,uint8_t *TempState_ChgDly_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_TempState_ChgDly_ITEM_LEN; i ++)
		{
			TempState_ChgDly_val[i] = ap_eep->process_eep.TempState_ChgDly[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_TempState_ChgDly(EEP_HANDLE eep_handle,uint8_t const *TempState_ChgDly_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_TempState_ChgDly_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_TempState_ChgDly_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_TempState_ChgDly_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_TempState_ChgDly_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_TempState_ChgDly_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = TempState_ChgDly_val[i];
#elif EEP_CONTENT_TempState_ChgDly_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(TempState_ChgDly_val[i]);
		msg[len++] = (uint8_t)(TempState_ChgDly_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(TempState_ChgDly_val[i]);
		msg[len++] = (uint8_t)(TempState_ChgDly_val[i]>>8);
		msg[len++] = (uint8_t)(TempState_ChgDly_val[i]>>16);
		msg[len++] = (uint8_t)(TempState_ChgDly_val[i]>>24);
#endif // {EEP_CONTENT_TempState_ChgDly_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_TempState_ChgDly_ITEM_LEN}
void DevEep_Inject_ChgCalbk_TempState_ChgDly(EEP_HANDLE eep_handle,TempState_ChgDly_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_TempState_ChgDly_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_TempState_ChgDly_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_TempState_ChgDly(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		TempState_ChgDly_ChgCallBk TempState_ChgDly_callback =  (TempState_ChgDly_ChgCallBk)callbk;
		TempState_ChgDly_callback(ap,ap->process_eep.TempState_ChgDly);
	}
}

// api define for TempDegC_Low_Hysteresis_L /////////////////////////////////////////////////////////
#if EEP_CONTENT_TempDegC_Low_Hysteresis_L_ITEM_LEN == 1
uint8_t DevEep_Get_TempDegC_Low_Hysteresis_L(EEP_HANDLE eep_handle)
{
	uint8_t TempDegC_Low_Hysteresis_L_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		TempDegC_Low_Hysteresis_L_val = ap_eep->process_eep.TempDegC_Low_Hysteresis_L;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return TempDegC_Low_Hysteresis_L_val;
}
void DevEep_Set_TempDegC_Low_Hysteresis_L(EEP_HANDLE eep_handle,uint8_t const TempDegC_Low_Hysteresis_L_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_TempDegC_Low_Hysteresis_L_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_TempDegC_Low_Hysteresis_L_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_TempDegC_Low_Hysteresis_L_LIST_ID);
#if EEP_CONTENT_TempDegC_Low_Hysteresis_L_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = TempDegC_Low_Hysteresis_L_val;
#elif EEP_CONTENT_TempDegC_Low_Hysteresis_L_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(TempDegC_Low_Hysteresis_L_val);
	msg[len++] = (uint8_t)(TempDegC_Low_Hysteresis_L_val>>8);
#else 
	msg[len++] = (uint8_t)(TempDegC_Low_Hysteresis_L_val);
	msg[len++] = (uint8_t)(TempDegC_Low_Hysteresis_L_val>>8);
	msg[len++] = (uint8_t)(TempDegC_Low_Hysteresis_L_val>>16);
	msg[len++] = (uint8_t)(TempDegC_Low_Hysteresis_L_val>>24);
#endif // {EEP_CONTENT_TempDegC_Low_Hysteresis_L_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_TempDegC_Low_Hysteresis_L_ITEM_LEN > 1
void DevEep_Get_TempDegC_Low_Hysteresis_L(EEP_HANDLE eep_handle,uint8_t *TempDegC_Low_Hysteresis_L_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_TempDegC_Low_Hysteresis_L_ITEM_LEN; i ++)
		{
			TempDegC_Low_Hysteresis_L_val[i] = ap_eep->process_eep.TempDegC_Low_Hysteresis_L[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_TempDegC_Low_Hysteresis_L(EEP_HANDLE eep_handle,uint8_t const *TempDegC_Low_Hysteresis_L_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_TempDegC_Low_Hysteresis_L_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_TempDegC_Low_Hysteresis_L_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_TempDegC_Low_Hysteresis_L_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_TempDegC_Low_Hysteresis_L_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_TempDegC_Low_Hysteresis_L_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = TempDegC_Low_Hysteresis_L_val[i];
#elif EEP_CONTENT_TempDegC_Low_Hysteresis_L_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(TempDegC_Low_Hysteresis_L_val[i]);
		msg[len++] = (uint8_t)(TempDegC_Low_Hysteresis_L_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(TempDegC_Low_Hysteresis_L_val[i]);
		msg[len++] = (uint8_t)(TempDegC_Low_Hysteresis_L_val[i]>>8);
		msg[len++] = (uint8_t)(TempDegC_Low_Hysteresis_L_val[i]>>16);
		msg[len++] = (uint8_t)(TempDegC_Low_Hysteresis_L_val[i]>>24);
#endif // {EEP_CONTENT_TempDegC_Low_Hysteresis_L_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_TempDegC_Low_Hysteresis_L_ITEM_LEN}
void DevEep_Inject_ChgCalbk_TempDegC_Low_Hysteresis_L(EEP_HANDLE eep_handle,TempDegC_Low_Hysteresis_L_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_TempDegC_Low_Hysteresis_L_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_TempDegC_Low_Hysteresis_L_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_TempDegC_Low_Hysteresis_L(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		TempDegC_Low_Hysteresis_L_ChgCallBk TempDegC_Low_Hysteresis_L_callback =  (TempDegC_Low_Hysteresis_L_ChgCallBk)callbk;
		TempDegC_Low_Hysteresis_L_callback(ap,ap->process_eep.TempDegC_Low_Hysteresis_L);
	}
}

// api define for TempDegC_Low_Hysteresis_H /////////////////////////////////////////////////////////
#if EEP_CONTENT_TempDegC_Low_Hysteresis_H_ITEM_LEN == 1
uint8_t DevEep_Get_TempDegC_Low_Hysteresis_H(EEP_HANDLE eep_handle)
{
	uint8_t TempDegC_Low_Hysteresis_H_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		TempDegC_Low_Hysteresis_H_val = ap_eep->process_eep.TempDegC_Low_Hysteresis_H;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return TempDegC_Low_Hysteresis_H_val;
}
void DevEep_Set_TempDegC_Low_Hysteresis_H(EEP_HANDLE eep_handle,uint8_t const TempDegC_Low_Hysteresis_H_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_TempDegC_Low_Hysteresis_H_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_TempDegC_Low_Hysteresis_H_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_TempDegC_Low_Hysteresis_H_LIST_ID);
#if EEP_CONTENT_TempDegC_Low_Hysteresis_H_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = TempDegC_Low_Hysteresis_H_val;
#elif EEP_CONTENT_TempDegC_Low_Hysteresis_H_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(TempDegC_Low_Hysteresis_H_val);
	msg[len++] = (uint8_t)(TempDegC_Low_Hysteresis_H_val>>8);
#else 
	msg[len++] = (uint8_t)(TempDegC_Low_Hysteresis_H_val);
	msg[len++] = (uint8_t)(TempDegC_Low_Hysteresis_H_val>>8);
	msg[len++] = (uint8_t)(TempDegC_Low_Hysteresis_H_val>>16);
	msg[len++] = (uint8_t)(TempDegC_Low_Hysteresis_H_val>>24);
#endif // {EEP_CONTENT_TempDegC_Low_Hysteresis_H_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_TempDegC_Low_Hysteresis_H_ITEM_LEN > 1
void DevEep_Get_TempDegC_Low_Hysteresis_H(EEP_HANDLE eep_handle,uint8_t *TempDegC_Low_Hysteresis_H_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_TempDegC_Low_Hysteresis_H_ITEM_LEN; i ++)
		{
			TempDegC_Low_Hysteresis_H_val[i] = ap_eep->process_eep.TempDegC_Low_Hysteresis_H[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_TempDegC_Low_Hysteresis_H(EEP_HANDLE eep_handle,uint8_t const *TempDegC_Low_Hysteresis_H_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_TempDegC_Low_Hysteresis_H_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_TempDegC_Low_Hysteresis_H_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_TempDegC_Low_Hysteresis_H_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_TempDegC_Low_Hysteresis_H_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_TempDegC_Low_Hysteresis_H_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = TempDegC_Low_Hysteresis_H_val[i];
#elif EEP_CONTENT_TempDegC_Low_Hysteresis_H_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(TempDegC_Low_Hysteresis_H_val[i]);
		msg[len++] = (uint8_t)(TempDegC_Low_Hysteresis_H_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(TempDegC_Low_Hysteresis_H_val[i]);
		msg[len++] = (uint8_t)(TempDegC_Low_Hysteresis_H_val[i]>>8);
		msg[len++] = (uint8_t)(TempDegC_Low_Hysteresis_H_val[i]>>16);
		msg[len++] = (uint8_t)(TempDegC_Low_Hysteresis_H_val[i]>>24);
#endif // {EEP_CONTENT_TempDegC_Low_Hysteresis_H_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_TempDegC_Low_Hysteresis_H_ITEM_LEN}
void DevEep_Inject_ChgCalbk_TempDegC_Low_Hysteresis_H(EEP_HANDLE eep_handle,TempDegC_Low_Hysteresis_H_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_TempDegC_Low_Hysteresis_H_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_TempDegC_Low_Hysteresis_H_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_TempDegC_Low_Hysteresis_H(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		TempDegC_Low_Hysteresis_H_ChgCallBk TempDegC_Low_Hysteresis_H_callback =  (TempDegC_Low_Hysteresis_H_ChgCallBk)callbk;
		TempDegC_Low_Hysteresis_H_callback(ap,ap->process_eep.TempDegC_Low_Hysteresis_H);
	}
}

// api define for TempDegC_High_Hysteresis_L /////////////////////////////////////////////////////////
#if EEP_CONTENT_TempDegC_High_Hysteresis_L_ITEM_LEN == 1
uint8_t DevEep_Get_TempDegC_High_Hysteresis_L(EEP_HANDLE eep_handle)
{
	uint8_t TempDegC_High_Hysteresis_L_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		TempDegC_High_Hysteresis_L_val = ap_eep->process_eep.TempDegC_High_Hysteresis_L;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return TempDegC_High_Hysteresis_L_val;
}
void DevEep_Set_TempDegC_High_Hysteresis_L(EEP_HANDLE eep_handle,uint8_t const TempDegC_High_Hysteresis_L_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_TempDegC_High_Hysteresis_L_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_TempDegC_High_Hysteresis_L_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_TempDegC_High_Hysteresis_L_LIST_ID);
#if EEP_CONTENT_TempDegC_High_Hysteresis_L_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = TempDegC_High_Hysteresis_L_val;
#elif EEP_CONTENT_TempDegC_High_Hysteresis_L_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(TempDegC_High_Hysteresis_L_val);
	msg[len++] = (uint8_t)(TempDegC_High_Hysteresis_L_val>>8);
#else 
	msg[len++] = (uint8_t)(TempDegC_High_Hysteresis_L_val);
	msg[len++] = (uint8_t)(TempDegC_High_Hysteresis_L_val>>8);
	msg[len++] = (uint8_t)(TempDegC_High_Hysteresis_L_val>>16);
	msg[len++] = (uint8_t)(TempDegC_High_Hysteresis_L_val>>24);
#endif // {EEP_CONTENT_TempDegC_High_Hysteresis_L_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_TempDegC_High_Hysteresis_L_ITEM_LEN > 1
void DevEep_Get_TempDegC_High_Hysteresis_L(EEP_HANDLE eep_handle,uint8_t *TempDegC_High_Hysteresis_L_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_TempDegC_High_Hysteresis_L_ITEM_LEN; i ++)
		{
			TempDegC_High_Hysteresis_L_val[i] = ap_eep->process_eep.TempDegC_High_Hysteresis_L[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_TempDegC_High_Hysteresis_L(EEP_HANDLE eep_handle,uint8_t const *TempDegC_High_Hysteresis_L_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_TempDegC_High_Hysteresis_L_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_TempDegC_High_Hysteresis_L_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_TempDegC_High_Hysteresis_L_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_TempDegC_High_Hysteresis_L_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_TempDegC_High_Hysteresis_L_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = TempDegC_High_Hysteresis_L_val[i];
#elif EEP_CONTENT_TempDegC_High_Hysteresis_L_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(TempDegC_High_Hysteresis_L_val[i]);
		msg[len++] = (uint8_t)(TempDegC_High_Hysteresis_L_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(TempDegC_High_Hysteresis_L_val[i]);
		msg[len++] = (uint8_t)(TempDegC_High_Hysteresis_L_val[i]>>8);
		msg[len++] = (uint8_t)(TempDegC_High_Hysteresis_L_val[i]>>16);
		msg[len++] = (uint8_t)(TempDegC_High_Hysteresis_L_val[i]>>24);
#endif // {EEP_CONTENT_TempDegC_High_Hysteresis_L_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_TempDegC_High_Hysteresis_L_ITEM_LEN}
void DevEep_Inject_ChgCalbk_TempDegC_High_Hysteresis_L(EEP_HANDLE eep_handle,TempDegC_High_Hysteresis_L_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_TempDegC_High_Hysteresis_L_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_TempDegC_High_Hysteresis_L_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_TempDegC_High_Hysteresis_L(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		TempDegC_High_Hysteresis_L_ChgCallBk TempDegC_High_Hysteresis_L_callback =  (TempDegC_High_Hysteresis_L_ChgCallBk)callbk;
		TempDegC_High_Hysteresis_L_callback(ap,ap->process_eep.TempDegC_High_Hysteresis_L);
	}
}

// api define for TempDegC_High_Hysteresis_H /////////////////////////////////////////////////////////
#if EEP_CONTENT_TempDegC_High_Hysteresis_H_ITEM_LEN == 1
uint8_t DevEep_Get_TempDegC_High_Hysteresis_H(EEP_HANDLE eep_handle)
{
	uint8_t TempDegC_High_Hysteresis_H_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		TempDegC_High_Hysteresis_H_val = ap_eep->process_eep.TempDegC_High_Hysteresis_H;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return TempDegC_High_Hysteresis_H_val;
}
void DevEep_Set_TempDegC_High_Hysteresis_H(EEP_HANDLE eep_handle,uint8_t const TempDegC_High_Hysteresis_H_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_TempDegC_High_Hysteresis_H_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_TempDegC_High_Hysteresis_H_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_TempDegC_High_Hysteresis_H_LIST_ID);
#if EEP_CONTENT_TempDegC_High_Hysteresis_H_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = TempDegC_High_Hysteresis_H_val;
#elif EEP_CONTENT_TempDegC_High_Hysteresis_H_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(TempDegC_High_Hysteresis_H_val);
	msg[len++] = (uint8_t)(TempDegC_High_Hysteresis_H_val>>8);
#else 
	msg[len++] = (uint8_t)(TempDegC_High_Hysteresis_H_val);
	msg[len++] = (uint8_t)(TempDegC_High_Hysteresis_H_val>>8);
	msg[len++] = (uint8_t)(TempDegC_High_Hysteresis_H_val>>16);
	msg[len++] = (uint8_t)(TempDegC_High_Hysteresis_H_val>>24);
#endif // {EEP_CONTENT_TempDegC_High_Hysteresis_H_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_TempDegC_High_Hysteresis_H_ITEM_LEN > 1
void DevEep_Get_TempDegC_High_Hysteresis_H(EEP_HANDLE eep_handle,uint8_t *TempDegC_High_Hysteresis_H_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_TempDegC_High_Hysteresis_H_ITEM_LEN; i ++)
		{
			TempDegC_High_Hysteresis_H_val[i] = ap_eep->process_eep.TempDegC_High_Hysteresis_H[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_TempDegC_High_Hysteresis_H(EEP_HANDLE eep_handle,uint8_t const *TempDegC_High_Hysteresis_H_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_TempDegC_High_Hysteresis_H_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_TempDegC_High_Hysteresis_H_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_TempDegC_High_Hysteresis_H_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_TempDegC_High_Hysteresis_H_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_TempDegC_High_Hysteresis_H_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = TempDegC_High_Hysteresis_H_val[i];
#elif EEP_CONTENT_TempDegC_High_Hysteresis_H_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(TempDegC_High_Hysteresis_H_val[i]);
		msg[len++] = (uint8_t)(TempDegC_High_Hysteresis_H_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(TempDegC_High_Hysteresis_H_val[i]);
		msg[len++] = (uint8_t)(TempDegC_High_Hysteresis_H_val[i]>>8);
		msg[len++] = (uint8_t)(TempDegC_High_Hysteresis_H_val[i]>>16);
		msg[len++] = (uint8_t)(TempDegC_High_Hysteresis_H_val[i]>>24);
#endif // {EEP_CONTENT_TempDegC_High_Hysteresis_H_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_TempDegC_High_Hysteresis_H_ITEM_LEN}
void DevEep_Inject_ChgCalbk_TempDegC_High_Hysteresis_H(EEP_HANDLE eep_handle,TempDegC_High_Hysteresis_H_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_TempDegC_High_Hysteresis_H_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_TempDegC_High_Hysteresis_H_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_TempDegC_High_Hysteresis_H(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		TempDegC_High_Hysteresis_H_ChgCallBk TempDegC_High_Hysteresis_H_callback =  (TempDegC_High_Hysteresis_H_ChgCallBk)callbk;
		TempDegC_High_Hysteresis_H_callback(ap,ap->process_eep.TempDegC_High_Hysteresis_H);
	}
}

// api define for AmpHighTempProction /////////////////////////////////////////////////////////
#if EEP_CONTENT_AmpHighTempProction_ITEM_LEN == 1
uint32_t DevEep_Get_AmpHighTempProction(EEP_HANDLE eep_handle)
{
	uint32_t AmpHighTempProction_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		AmpHighTempProction_val = ap_eep->process_eep.AmpHighTempProction;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return AmpHighTempProction_val;
}
void DevEep_Set_AmpHighTempProction(EEP_HANDLE eep_handle,uint32_t const AmpHighTempProction_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_AmpHighTempProction_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_AmpHighTempProction_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_AmpHighTempProction_LIST_ID);
#if EEP_CONTENT_AmpHighTempProction_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = AmpHighTempProction_val;
#elif EEP_CONTENT_AmpHighTempProction_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(AmpHighTempProction_val);
	msg[len++] = (uint8_t)(AmpHighTempProction_val>>8);
#else 
	msg[len++] = (uint8_t)(AmpHighTempProction_val);
	msg[len++] = (uint8_t)(AmpHighTempProction_val>>8);
	msg[len++] = (uint8_t)(AmpHighTempProction_val>>16);
	msg[len++] = (uint8_t)(AmpHighTempProction_val>>24);
#endif // {EEP_CONTENT_AmpHighTempProction_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_AmpHighTempProction_ITEM_LEN > 1
void DevEep_Get_AmpHighTempProction(EEP_HANDLE eep_handle,uint32_t *AmpHighTempProction_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_AmpHighTempProction_ITEM_LEN; i ++)
		{
			AmpHighTempProction_val[i] = ap_eep->process_eep.AmpHighTempProction[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_AmpHighTempProction(EEP_HANDLE eep_handle,uint32_t const *AmpHighTempProction_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_AmpHighTempProction_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_AmpHighTempProction_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_AmpHighTempProction_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_AmpHighTempProction_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_AmpHighTempProction_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = AmpHighTempProction_val[i];
#elif EEP_CONTENT_AmpHighTempProction_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(AmpHighTempProction_val[i]);
		msg[len++] = (uint8_t)(AmpHighTempProction_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(AmpHighTempProction_val[i]);
		msg[len++] = (uint8_t)(AmpHighTempProction_val[i]>>8);
		msg[len++] = (uint8_t)(AmpHighTempProction_val[i]>>16);
		msg[len++] = (uint8_t)(AmpHighTempProction_val[i]>>24);
#endif // {EEP_CONTENT_AmpHighTempProction_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_AmpHighTempProction_ITEM_LEN}
void DevEep_Inject_ChgCalbk_AmpHighTempProction(EEP_HANDLE eep_handle,AmpHighTempProction_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_AmpHighTempProction_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_AmpHighTempProction_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_AmpHighTempProction(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		AmpHighTempProction_ChgCallBk AmpHighTempProction_callback =  (AmpHighTempProction_ChgCallBk)callbk;
		AmpHighTempProction_callback(ap,ap->process_eep.AmpHighTempProction);
	}
}

// api define for CanNm_DA_S1_Delay_ms /////////////////////////////////////////////////////////
#if EEP_CONTENT_CanNm_DA_S1_Delay_ms_ITEM_LEN == 1
uint16_t DevEep_Get_CanNm_DA_S1_Delay_ms(EEP_HANDLE eep_handle)
{
	uint16_t CanNm_DA_S1_Delay_ms_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		CanNm_DA_S1_Delay_ms_val = ap_eep->process_eep.CanNm_DA_S1_Delay_ms;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return CanNm_DA_S1_Delay_ms_val;
}
void DevEep_Set_CanNm_DA_S1_Delay_ms(EEP_HANDLE eep_handle,uint16_t const CanNm_DA_S1_Delay_ms_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_CanNm_DA_S1_Delay_ms_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_CanNm_DA_S1_Delay_ms_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_CanNm_DA_S1_Delay_ms_LIST_ID);
#if EEP_CONTENT_CanNm_DA_S1_Delay_ms_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = CanNm_DA_S1_Delay_ms_val;
#elif EEP_CONTENT_CanNm_DA_S1_Delay_ms_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(CanNm_DA_S1_Delay_ms_val);
	msg[len++] = (uint8_t)(CanNm_DA_S1_Delay_ms_val>>8);
#else 
	msg[len++] = (uint8_t)(CanNm_DA_S1_Delay_ms_val);
	msg[len++] = (uint8_t)(CanNm_DA_S1_Delay_ms_val>>8);
	msg[len++] = (uint8_t)(CanNm_DA_S1_Delay_ms_val>>16);
	msg[len++] = (uint8_t)(CanNm_DA_S1_Delay_ms_val>>24);
#endif // {EEP_CONTENT_CanNm_DA_S1_Delay_ms_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_CanNm_DA_S1_Delay_ms_ITEM_LEN > 1
void DevEep_Get_CanNm_DA_S1_Delay_ms(EEP_HANDLE eep_handle,uint16_t *CanNm_DA_S1_Delay_ms_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_CanNm_DA_S1_Delay_ms_ITEM_LEN; i ++)
		{
			CanNm_DA_S1_Delay_ms_val[i] = ap_eep->process_eep.CanNm_DA_S1_Delay_ms[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_CanNm_DA_S1_Delay_ms(EEP_HANDLE eep_handle,uint16_t const *CanNm_DA_S1_Delay_ms_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_CanNm_DA_S1_Delay_ms_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_CanNm_DA_S1_Delay_ms_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_CanNm_DA_S1_Delay_ms_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_CanNm_DA_S1_Delay_ms_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_CanNm_DA_S1_Delay_ms_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = CanNm_DA_S1_Delay_ms_val[i];
#elif EEP_CONTENT_CanNm_DA_S1_Delay_ms_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(CanNm_DA_S1_Delay_ms_val[i]);
		msg[len++] = (uint8_t)(CanNm_DA_S1_Delay_ms_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(CanNm_DA_S1_Delay_ms_val[i]);
		msg[len++] = (uint8_t)(CanNm_DA_S1_Delay_ms_val[i]>>8);
		msg[len++] = (uint8_t)(CanNm_DA_S1_Delay_ms_val[i]>>16);
		msg[len++] = (uint8_t)(CanNm_DA_S1_Delay_ms_val[i]>>24);
#endif // {EEP_CONTENT_CanNm_DA_S1_Delay_ms_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_CanNm_DA_S1_Delay_ms_ITEM_LEN}
void DevEep_Inject_ChgCalbk_CanNm_DA_S1_Delay_ms(EEP_HANDLE eep_handle,CanNm_DA_S1_Delay_ms_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_CanNm_DA_S1_Delay_ms_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_CanNm_DA_S1_Delay_ms_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_CanNm_DA_S1_Delay_ms(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		CanNm_DA_S1_Delay_ms_ChgCallBk CanNm_DA_S1_Delay_ms_callback =  (CanNm_DA_S1_Delay_ms_ChgCallBk)callbk;
		CanNm_DA_S1_Delay_ms_callback(ap,ap->process_eep.CanNm_DA_S1_Delay_ms);
	}
}

// api define for Vin /////////////////////////////////////////////////////////
#if EEP_CONTENT_Vin_ITEM_LEN == 1
uint8_t DevEep_Get_Vin(EEP_HANDLE eep_handle)
{
	uint8_t Vin_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		Vin_val = ap_eep->process_eep.Vin;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return Vin_val;
}
void DevEep_Set_Vin(EEP_HANDLE eep_handle,uint8_t const Vin_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_Vin_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_Vin_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_Vin_LIST_ID);
#if EEP_CONTENT_Vin_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = Vin_val;
#elif EEP_CONTENT_Vin_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(Vin_val);
	msg[len++] = (uint8_t)(Vin_val>>8);
#else 
	msg[len++] = (uint8_t)(Vin_val);
	msg[len++] = (uint8_t)(Vin_val>>8);
	msg[len++] = (uint8_t)(Vin_val>>16);
	msg[len++] = (uint8_t)(Vin_val>>24);
#endif // {EEP_CONTENT_Vin_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_Vin_ITEM_LEN > 1
void DevEep_Get_Vin(EEP_HANDLE eep_handle,uint8_t *Vin_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_Vin_ITEM_LEN; i ++)
		{
			Vin_val[i] = ap_eep->process_eep.Vin[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_Vin(EEP_HANDLE eep_handle,uint8_t const *Vin_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_Vin_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_Vin_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_Vin_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_Vin_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_Vin_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = Vin_val[i];
#elif EEP_CONTENT_Vin_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(Vin_val[i]);
		msg[len++] = (uint8_t)(Vin_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(Vin_val[i]);
		msg[len++] = (uint8_t)(Vin_val[i]>>8);
		msg[len++] = (uint8_t)(Vin_val[i]>>16);
		msg[len++] = (uint8_t)(Vin_val[i]>>24);
#endif // {EEP_CONTENT_Vin_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_Vin_ITEM_LEN}
void DevEep_Inject_ChgCalbk_Vin(EEP_HANDLE eep_handle,Vin_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_Vin_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_Vin_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_Vin(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		Vin_ChgCallBk Vin_callback =  (Vin_ChgCallBk)callbk;
		Vin_callback(ap,ap->process_eep.Vin);
	}
}

// api define for AutoSyncTimeWithGps /////////////////////////////////////////////////////////
#if EEP_CONTENT_AutoSyncTimeWithGps_ITEM_LEN == 1
uint8_t DevEep_Get_AutoSyncTimeWithGps(EEP_HANDLE eep_handle)
{
	uint8_t AutoSyncTimeWithGps_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		AutoSyncTimeWithGps_val = ap_eep->process_eep.AutoSyncTimeWithGps;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return AutoSyncTimeWithGps_val;
}
void DevEep_Set_AutoSyncTimeWithGps(EEP_HANDLE eep_handle,uint8_t const AutoSyncTimeWithGps_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_AutoSyncTimeWithGps_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_AutoSyncTimeWithGps_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_AutoSyncTimeWithGps_LIST_ID);
#if EEP_CONTENT_AutoSyncTimeWithGps_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = AutoSyncTimeWithGps_val;
#elif EEP_CONTENT_AutoSyncTimeWithGps_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(AutoSyncTimeWithGps_val);
	msg[len++] = (uint8_t)(AutoSyncTimeWithGps_val>>8);
#else 
	msg[len++] = (uint8_t)(AutoSyncTimeWithGps_val);
	msg[len++] = (uint8_t)(AutoSyncTimeWithGps_val>>8);
	msg[len++] = (uint8_t)(AutoSyncTimeWithGps_val>>16);
	msg[len++] = (uint8_t)(AutoSyncTimeWithGps_val>>24);
#endif // {EEP_CONTENT_AutoSyncTimeWithGps_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_AutoSyncTimeWithGps_ITEM_LEN > 1
void DevEep_Get_AutoSyncTimeWithGps(EEP_HANDLE eep_handle,uint8_t *AutoSyncTimeWithGps_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_AutoSyncTimeWithGps_ITEM_LEN; i ++)
		{
			AutoSyncTimeWithGps_val[i] = ap_eep->process_eep.AutoSyncTimeWithGps[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_AutoSyncTimeWithGps(EEP_HANDLE eep_handle,uint8_t const *AutoSyncTimeWithGps_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_AutoSyncTimeWithGps_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_AutoSyncTimeWithGps_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_AutoSyncTimeWithGps_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_AutoSyncTimeWithGps_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_AutoSyncTimeWithGps_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = AutoSyncTimeWithGps_val[i];
#elif EEP_CONTENT_AutoSyncTimeWithGps_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(AutoSyncTimeWithGps_val[i]);
		msg[len++] = (uint8_t)(AutoSyncTimeWithGps_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(AutoSyncTimeWithGps_val[i]);
		msg[len++] = (uint8_t)(AutoSyncTimeWithGps_val[i]>>8);
		msg[len++] = (uint8_t)(AutoSyncTimeWithGps_val[i]>>16);
		msg[len++] = (uint8_t)(AutoSyncTimeWithGps_val[i]>>24);
#endif // {EEP_CONTENT_AutoSyncTimeWithGps_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_AutoSyncTimeWithGps_ITEM_LEN}
void DevEep_Inject_ChgCalbk_AutoSyncTimeWithGps(EEP_HANDLE eep_handle,AutoSyncTimeWithGps_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_AutoSyncTimeWithGps_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_AutoSyncTimeWithGps_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_AutoSyncTimeWithGps(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		AutoSyncTimeWithGps_ChgCallBk AutoSyncTimeWithGps_callback =  (AutoSyncTimeWithGps_ChgCallBk)callbk;
		AutoSyncTimeWithGps_callback(ap,ap->process_eep.AutoSyncTimeWithGps);
	}
}

// api define for ScreenBackLightValOnDay /////////////////////////////////////////////////////////
#if EEP_CONTENT_ScreenBackLightValOnDay_ITEM_LEN == 1
uint8_t DevEep_Get_ScreenBackLightValOnDay(EEP_HANDLE eep_handle)
{
	uint8_t ScreenBackLightValOnDay_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ScreenBackLightValOnDay_val = ap_eep->process_eep.ScreenBackLightValOnDay;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return ScreenBackLightValOnDay_val;
}
void DevEep_Set_ScreenBackLightValOnDay(EEP_HANDLE eep_handle,uint8_t const ScreenBackLightValOnDay_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_ScreenBackLightValOnDay_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_ScreenBackLightValOnDay_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_ScreenBackLightValOnDay_LIST_ID);
#if EEP_CONTENT_ScreenBackLightValOnDay_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = ScreenBackLightValOnDay_val;
#elif EEP_CONTENT_ScreenBackLightValOnDay_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(ScreenBackLightValOnDay_val);
	msg[len++] = (uint8_t)(ScreenBackLightValOnDay_val>>8);
#else 
	msg[len++] = (uint8_t)(ScreenBackLightValOnDay_val);
	msg[len++] = (uint8_t)(ScreenBackLightValOnDay_val>>8);
	msg[len++] = (uint8_t)(ScreenBackLightValOnDay_val>>16);
	msg[len++] = (uint8_t)(ScreenBackLightValOnDay_val>>24);
#endif // {EEP_CONTENT_ScreenBackLightValOnDay_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_ScreenBackLightValOnDay_ITEM_LEN > 1
void DevEep_Get_ScreenBackLightValOnDay(EEP_HANDLE eep_handle,uint8_t *ScreenBackLightValOnDay_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_ScreenBackLightValOnDay_ITEM_LEN; i ++)
		{
			ScreenBackLightValOnDay_val[i] = ap_eep->process_eep.ScreenBackLightValOnDay[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_ScreenBackLightValOnDay(EEP_HANDLE eep_handle,uint8_t const *ScreenBackLightValOnDay_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_ScreenBackLightValOnDay_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_ScreenBackLightValOnDay_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_ScreenBackLightValOnDay_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_ScreenBackLightValOnDay_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_ScreenBackLightValOnDay_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = ScreenBackLightValOnDay_val[i];
#elif EEP_CONTENT_ScreenBackLightValOnDay_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(ScreenBackLightValOnDay_val[i]);
		msg[len++] = (uint8_t)(ScreenBackLightValOnDay_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(ScreenBackLightValOnDay_val[i]);
		msg[len++] = (uint8_t)(ScreenBackLightValOnDay_val[i]>>8);
		msg[len++] = (uint8_t)(ScreenBackLightValOnDay_val[i]>>16);
		msg[len++] = (uint8_t)(ScreenBackLightValOnDay_val[i]>>24);
#endif // {EEP_CONTENT_ScreenBackLightValOnDay_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_ScreenBackLightValOnDay_ITEM_LEN}
void DevEep_Inject_ChgCalbk_ScreenBackLightValOnDay(EEP_HANDLE eep_handle,ScreenBackLightValOnDay_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_ScreenBackLightValOnDay_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_ScreenBackLightValOnDay_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_ScreenBackLightValOnDay(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		ScreenBackLightValOnDay_ChgCallBk ScreenBackLightValOnDay_callback =  (ScreenBackLightValOnDay_ChgCallBk)callbk;
		ScreenBackLightValOnDay_callback(ap,ap->process_eep.ScreenBackLightValOnDay);
	}
}

// api define for ScreenBackLightValOnNight /////////////////////////////////////////////////////////
#if EEP_CONTENT_ScreenBackLightValOnNight_ITEM_LEN == 1
uint8_t DevEep_Get_ScreenBackLightValOnNight(EEP_HANDLE eep_handle)
{
	uint8_t ScreenBackLightValOnNight_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ScreenBackLightValOnNight_val = ap_eep->process_eep.ScreenBackLightValOnNight;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return ScreenBackLightValOnNight_val;
}
void DevEep_Set_ScreenBackLightValOnNight(EEP_HANDLE eep_handle,uint8_t const ScreenBackLightValOnNight_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_ScreenBackLightValOnNight_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_ScreenBackLightValOnNight_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_ScreenBackLightValOnNight_LIST_ID);
#if EEP_CONTENT_ScreenBackLightValOnNight_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = ScreenBackLightValOnNight_val;
#elif EEP_CONTENT_ScreenBackLightValOnNight_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(ScreenBackLightValOnNight_val);
	msg[len++] = (uint8_t)(ScreenBackLightValOnNight_val>>8);
#else 
	msg[len++] = (uint8_t)(ScreenBackLightValOnNight_val);
	msg[len++] = (uint8_t)(ScreenBackLightValOnNight_val>>8);
	msg[len++] = (uint8_t)(ScreenBackLightValOnNight_val>>16);
	msg[len++] = (uint8_t)(ScreenBackLightValOnNight_val>>24);
#endif // {EEP_CONTENT_ScreenBackLightValOnNight_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_ScreenBackLightValOnNight_ITEM_LEN > 1
void DevEep_Get_ScreenBackLightValOnNight(EEP_HANDLE eep_handle,uint8_t *ScreenBackLightValOnNight_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_ScreenBackLightValOnNight_ITEM_LEN; i ++)
		{
			ScreenBackLightValOnNight_val[i] = ap_eep->process_eep.ScreenBackLightValOnNight[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_ScreenBackLightValOnNight(EEP_HANDLE eep_handle,uint8_t const *ScreenBackLightValOnNight_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_ScreenBackLightValOnNight_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_ScreenBackLightValOnNight_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_ScreenBackLightValOnNight_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_ScreenBackLightValOnNight_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_ScreenBackLightValOnNight_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = ScreenBackLightValOnNight_val[i];
#elif EEP_CONTENT_ScreenBackLightValOnNight_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(ScreenBackLightValOnNight_val[i]);
		msg[len++] = (uint8_t)(ScreenBackLightValOnNight_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(ScreenBackLightValOnNight_val[i]);
		msg[len++] = (uint8_t)(ScreenBackLightValOnNight_val[i]>>8);
		msg[len++] = (uint8_t)(ScreenBackLightValOnNight_val[i]>>16);
		msg[len++] = (uint8_t)(ScreenBackLightValOnNight_val[i]>>24);
#endif // {EEP_CONTENT_ScreenBackLightValOnNight_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_ScreenBackLightValOnNight_ITEM_LEN}
void DevEep_Inject_ChgCalbk_ScreenBackLightValOnNight(EEP_HANDLE eep_handle,ScreenBackLightValOnNight_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_ScreenBackLightValOnNight_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_ScreenBackLightValOnNight_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_ScreenBackLightValOnNight(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		ScreenBackLightValOnNight_ChgCallBk ScreenBackLightValOnNight_callback =  (ScreenBackLightValOnNight_ChgCallBk)callbk;
		ScreenBackLightValOnNight_callback(ap,ap->process_eep.ScreenBackLightValOnNight);
	}
}

// api define for HistoryAverFuelCons /////////////////////////////////////////////////////////
#if EEP_CONTENT_HistoryAverFuelCons_ITEM_LEN == 1
uint8_t DevEep_Get_HistoryAverFuelCons(EEP_HANDLE eep_handle)
{
	uint8_t HistoryAverFuelCons_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		HistoryAverFuelCons_val = ap_eep->process_eep.HistoryAverFuelCons;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return HistoryAverFuelCons_val;
}
void DevEep_Set_HistoryAverFuelCons(EEP_HANDLE eep_handle,uint8_t const HistoryAverFuelCons_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_HistoryAverFuelCons_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_HistoryAverFuelCons_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_HistoryAverFuelCons_LIST_ID);
#if EEP_CONTENT_HistoryAverFuelCons_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = HistoryAverFuelCons_val;
#elif EEP_CONTENT_HistoryAverFuelCons_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(HistoryAverFuelCons_val);
	msg[len++] = (uint8_t)(HistoryAverFuelCons_val>>8);
#else 
	msg[len++] = (uint8_t)(HistoryAverFuelCons_val);
	msg[len++] = (uint8_t)(HistoryAverFuelCons_val>>8);
	msg[len++] = (uint8_t)(HistoryAverFuelCons_val>>16);
	msg[len++] = (uint8_t)(HistoryAverFuelCons_val>>24);
#endif // {EEP_CONTENT_HistoryAverFuelCons_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_HistoryAverFuelCons_ITEM_LEN > 1
void DevEep_Get_HistoryAverFuelCons(EEP_HANDLE eep_handle,uint8_t *HistoryAverFuelCons_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_HistoryAverFuelCons_ITEM_LEN; i ++)
		{
			HistoryAverFuelCons_val[i] = ap_eep->process_eep.HistoryAverFuelCons[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_HistoryAverFuelCons(EEP_HANDLE eep_handle,uint8_t const *HistoryAverFuelCons_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_HistoryAverFuelCons_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_HistoryAverFuelCons_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_HistoryAverFuelCons_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_HistoryAverFuelCons_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_HistoryAverFuelCons_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = HistoryAverFuelCons_val[i];
#elif EEP_CONTENT_HistoryAverFuelCons_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(HistoryAverFuelCons_val[i]);
		msg[len++] = (uint8_t)(HistoryAverFuelCons_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(HistoryAverFuelCons_val[i]);
		msg[len++] = (uint8_t)(HistoryAverFuelCons_val[i]>>8);
		msg[len++] = (uint8_t)(HistoryAverFuelCons_val[i]>>16);
		msg[len++] = (uint8_t)(HistoryAverFuelCons_val[i]>>24);
#endif // {EEP_CONTENT_HistoryAverFuelCons_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_HistoryAverFuelCons_ITEM_LEN}
void DevEep_Inject_ChgCalbk_HistoryAverFuelCons(EEP_HANDLE eep_handle,HistoryAverFuelCons_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_HistoryAverFuelCons_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_HistoryAverFuelCons_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_HistoryAverFuelCons(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		HistoryAverFuelCons_ChgCallBk HistoryAverFuelCons_callback =  (HistoryAverFuelCons_ChgCallBk)callbk;
		HistoryAverFuelCons_callback(ap,ap->process_eep.HistoryAverFuelCons);
	}
}

// api define for FuelResistance /////////////////////////////////////////////////////////
#if EEP_CONTENT_FuelResistance_ITEM_LEN == 1
uint32_t DevEep_Get_FuelResistance(EEP_HANDLE eep_handle)
{
	uint32_t FuelResistance_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		FuelResistance_val = ap_eep->process_eep.FuelResistance;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return FuelResistance_val;
}
void DevEep_Set_FuelResistance(EEP_HANDLE eep_handle,uint32_t const FuelResistance_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_FuelResistance_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_FuelResistance_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_FuelResistance_LIST_ID);
#if EEP_CONTENT_FuelResistance_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = FuelResistance_val;
#elif EEP_CONTENT_FuelResistance_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(FuelResistance_val);
	msg[len++] = (uint8_t)(FuelResistance_val>>8);
#else 
	msg[len++] = (uint8_t)(FuelResistance_val);
	msg[len++] = (uint8_t)(FuelResistance_val>>8);
	msg[len++] = (uint8_t)(FuelResistance_val>>16);
	msg[len++] = (uint8_t)(FuelResistance_val>>24);
#endif // {EEP_CONTENT_FuelResistance_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_FuelResistance_ITEM_LEN > 1
void DevEep_Get_FuelResistance(EEP_HANDLE eep_handle,uint32_t *FuelResistance_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_FuelResistance_ITEM_LEN; i ++)
		{
			FuelResistance_val[i] = ap_eep->process_eep.FuelResistance[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_FuelResistance(EEP_HANDLE eep_handle,uint32_t const *FuelResistance_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_FuelResistance_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_FuelResistance_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_FuelResistance_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_FuelResistance_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_FuelResistance_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = FuelResistance_val[i];
#elif EEP_CONTENT_FuelResistance_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(FuelResistance_val[i]);
		msg[len++] = (uint8_t)(FuelResistance_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(FuelResistance_val[i]);
		msg[len++] = (uint8_t)(FuelResistance_val[i]>>8);
		msg[len++] = (uint8_t)(FuelResistance_val[i]>>16);
		msg[len++] = (uint8_t)(FuelResistance_val[i]>>24);
#endif // {EEP_CONTENT_FuelResistance_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_FuelResistance_ITEM_LEN}
void DevEep_Inject_ChgCalbk_FuelResistance(EEP_HANDLE eep_handle,FuelResistance_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_FuelResistance_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_FuelResistance_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_FuelResistance(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		FuelResistance_ChgCallBk FuelResistance_callback =  (FuelResistance_ChgCallBk)callbk;
		FuelResistance_callback(ap,ap->process_eep.FuelResistance);
	}
}

// api define for dtc_example /////////////////////////////////////////////////////////
#if EEP_CONTENT_dtc_example_ITEM_LEN == 1
uint8_t DevEep_Get_dtc_example(EEP_HANDLE eep_handle)
{
	uint8_t dtc_example_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		dtc_example_val = ap_eep->process_eep.dtc_example;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return dtc_example_val;
}
void DevEep_Set_dtc_example(EEP_HANDLE eep_handle,uint8_t const dtc_example_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_dtc_example_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_dtc_example_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_dtc_example_LIST_ID);
#if EEP_CONTENT_dtc_example_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = dtc_example_val;
#elif EEP_CONTENT_dtc_example_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(dtc_example_val);
	msg[len++] = (uint8_t)(dtc_example_val>>8);
#else 
	msg[len++] = (uint8_t)(dtc_example_val);
	msg[len++] = (uint8_t)(dtc_example_val>>8);
	msg[len++] = (uint8_t)(dtc_example_val>>16);
	msg[len++] = (uint8_t)(dtc_example_val>>24);
#endif // {EEP_CONTENT_dtc_example_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_dtc_example_ITEM_LEN > 1
void DevEep_Get_dtc_example(EEP_HANDLE eep_handle,uint8_t *dtc_example_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_dtc_example_ITEM_LEN; i ++)
		{
			dtc_example_val[i] = ap_eep->process_eep.dtc_example[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_dtc_example(EEP_HANDLE eep_handle,uint8_t const *dtc_example_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_dtc_example_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_dtc_example_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_dtc_example_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_dtc_example_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_dtc_example_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = dtc_example_val[i];
#elif EEP_CONTENT_dtc_example_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(dtc_example_val[i]);
		msg[len++] = (uint8_t)(dtc_example_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(dtc_example_val[i]);
		msg[len++] = (uint8_t)(dtc_example_val[i]>>8);
		msg[len++] = (uint8_t)(dtc_example_val[i]>>16);
		msg[len++] = (uint8_t)(dtc_example_val[i]>>24);
#endif // {EEP_CONTENT_dtc_example_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_dtc_example_ITEM_LEN}
void DevEep_Inject_ChgCalbk_dtc_example(EEP_HANDLE eep_handle,dtc_example_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_dtc_example_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_dtc_example_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_dtc_example(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		dtc_example_ChgCallBk dtc_example_callback =  (dtc_example_ChgCallBk)callbk;
		dtc_example_callback(ap,ap->process_eep.dtc_example);
	}
}

// api define for TotalOdo /////////////////////////////////////////////////////////
#if EEP_CONTENT_TotalOdo_ITEM_LEN == 1
uint32_t DevEep_Get_TotalOdo(EEP_HANDLE eep_handle)
{
	uint32_t TotalOdo_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		TotalOdo_val = ap_eep->process_eep.TotalOdo;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return TotalOdo_val;
}
void DevEep_Set_TotalOdo(EEP_HANDLE eep_handle,uint32_t const TotalOdo_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_TotalOdo_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_TotalOdo_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_TotalOdo_LIST_ID);
#if EEP_CONTENT_TotalOdo_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = TotalOdo_val;
#elif EEP_CONTENT_TotalOdo_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(TotalOdo_val);
	msg[len++] = (uint8_t)(TotalOdo_val>>8);
#else 
	msg[len++] = (uint8_t)(TotalOdo_val);
	msg[len++] = (uint8_t)(TotalOdo_val>>8);
	msg[len++] = (uint8_t)(TotalOdo_val>>16);
	msg[len++] = (uint8_t)(TotalOdo_val>>24);
#endif // {EEP_CONTENT_TotalOdo_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_TotalOdo_ITEM_LEN > 1
void DevEep_Get_TotalOdo(EEP_HANDLE eep_handle,uint32_t *TotalOdo_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_TotalOdo_ITEM_LEN; i ++)
		{
			TotalOdo_val[i] = ap_eep->process_eep.TotalOdo[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_TotalOdo(EEP_HANDLE eep_handle,uint32_t const *TotalOdo_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_TotalOdo_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_TotalOdo_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_TotalOdo_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_TotalOdo_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_TotalOdo_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = TotalOdo_val[i];
#elif EEP_CONTENT_TotalOdo_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(TotalOdo_val[i]);
		msg[len++] = (uint8_t)(TotalOdo_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(TotalOdo_val[i]);
		msg[len++] = (uint8_t)(TotalOdo_val[i]>>8);
		msg[len++] = (uint8_t)(TotalOdo_val[i]>>16);
		msg[len++] = (uint8_t)(TotalOdo_val[i]>>24);
#endif // {EEP_CONTENT_TotalOdo_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_TotalOdo_ITEM_LEN}
void DevEep_Inject_ChgCalbk_TotalOdo(EEP_HANDLE eep_handle,TotalOdo_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_TotalOdo_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_TotalOdo_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_TotalOdo(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		TotalOdo_ChgCallBk TotalOdo_callback =  (TotalOdo_ChgCallBk)callbk;
		TotalOdo_callback(ap,ap->process_eep.TotalOdo);
	}
}

// api define for TotalTime /////////////////////////////////////////////////////////
#if EEP_CONTENT_TotalTime_ITEM_LEN == 1
uint32_t DevEep_Get_TotalTime(EEP_HANDLE eep_handle)
{
	uint32_t TotalTime_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		TotalTime_val = ap_eep->process_eep.TotalTime;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return TotalTime_val;
}
void DevEep_Set_TotalTime(EEP_HANDLE eep_handle,uint32_t const TotalTime_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_TotalTime_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_TotalTime_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_TotalTime_LIST_ID);
#if EEP_CONTENT_TotalTime_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = TotalTime_val;
#elif EEP_CONTENT_TotalTime_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(TotalTime_val);
	msg[len++] = (uint8_t)(TotalTime_val>>8);
#else 
	msg[len++] = (uint8_t)(TotalTime_val);
	msg[len++] = (uint8_t)(TotalTime_val>>8);
	msg[len++] = (uint8_t)(TotalTime_val>>16);
	msg[len++] = (uint8_t)(TotalTime_val>>24);
#endif // {EEP_CONTENT_TotalTime_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_TotalTime_ITEM_LEN > 1
void DevEep_Get_TotalTime(EEP_HANDLE eep_handle,uint32_t *TotalTime_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_TotalTime_ITEM_LEN; i ++)
		{
			TotalTime_val[i] = ap_eep->process_eep.TotalTime[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_TotalTime(EEP_HANDLE eep_handle,uint32_t const *TotalTime_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_TotalTime_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_TotalTime_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_TotalTime_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_TotalTime_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_TotalTime_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = TotalTime_val[i];
#elif EEP_CONTENT_TotalTime_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(TotalTime_val[i]);
		msg[len++] = (uint8_t)(TotalTime_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(TotalTime_val[i]);
		msg[len++] = (uint8_t)(TotalTime_val[i]>>8);
		msg[len++] = (uint8_t)(TotalTime_val[i]>>16);
		msg[len++] = (uint8_t)(TotalTime_val[i]>>24);
#endif // {EEP_CONTENT_TotalTime_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_TotalTime_ITEM_LEN}
void DevEep_Inject_ChgCalbk_TotalTime(EEP_HANDLE eep_handle,TotalTime_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_TotalTime_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_TotalTime_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_TotalTime(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		TotalTime_ChgCallBk TotalTime_callback =  (TotalTime_ChgCallBk)callbk;
		TotalTime_callback(ap,ap->process_eep.TotalTime);
	}
}

// api define for TravelTime /////////////////////////////////////////////////////////
#if EEP_CONTENT_TravelTime_ITEM_LEN == 1
uint32_t DevEep_Get_TravelTime(EEP_HANDLE eep_handle)
{
	uint32_t TravelTime_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		TravelTime_val = ap_eep->process_eep.TravelTime;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return TravelTime_val;
}
void DevEep_Set_TravelTime(EEP_HANDLE eep_handle,uint32_t const TravelTime_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_TravelTime_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_TravelTime_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_TravelTime_LIST_ID);
#if EEP_CONTENT_TravelTime_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = TravelTime_val;
#elif EEP_CONTENT_TravelTime_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(TravelTime_val);
	msg[len++] = (uint8_t)(TravelTime_val>>8);
#else 
	msg[len++] = (uint8_t)(TravelTime_val);
	msg[len++] = (uint8_t)(TravelTime_val>>8);
	msg[len++] = (uint8_t)(TravelTime_val>>16);
	msg[len++] = (uint8_t)(TravelTime_val>>24);
#endif // {EEP_CONTENT_TravelTime_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_TravelTime_ITEM_LEN > 1
void DevEep_Get_TravelTime(EEP_HANDLE eep_handle,uint32_t *TravelTime_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_TravelTime_ITEM_LEN; i ++)
		{
			TravelTime_val[i] = ap_eep->process_eep.TravelTime[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_TravelTime(EEP_HANDLE eep_handle,uint32_t const *TravelTime_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_TravelTime_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_TravelTime_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_TravelTime_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_TravelTime_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_TravelTime_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = TravelTime_val[i];
#elif EEP_CONTENT_TravelTime_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(TravelTime_val[i]);
		msg[len++] = (uint8_t)(TravelTime_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(TravelTime_val[i]);
		msg[len++] = (uint8_t)(TravelTime_val[i]>>8);
		msg[len++] = (uint8_t)(TravelTime_val[i]>>16);
		msg[len++] = (uint8_t)(TravelTime_val[i]>>24);
#endif // {EEP_CONTENT_TravelTime_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_TravelTime_ITEM_LEN}
void DevEep_Inject_ChgCalbk_TravelTime(EEP_HANDLE eep_handle,TravelTime_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_TravelTime_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_TravelTime_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_TravelTime(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		TravelTime_ChgCallBk TravelTime_callback =  (TravelTime_ChgCallBk)callbk;
		TravelTime_callback(ap,ap->process_eep.TravelTime);
	}
}

// api define for TravelOdo /////////////////////////////////////////////////////////
#if EEP_CONTENT_TravelOdo_ITEM_LEN == 1
uint32_t DevEep_Get_TravelOdo(EEP_HANDLE eep_handle)
{
	uint32_t TravelOdo_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		TravelOdo_val = ap_eep->process_eep.TravelOdo;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return TravelOdo_val;
}
void DevEep_Set_TravelOdo(EEP_HANDLE eep_handle,uint32_t const TravelOdo_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_TravelOdo_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_TravelOdo_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_TravelOdo_LIST_ID);
#if EEP_CONTENT_TravelOdo_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = TravelOdo_val;
#elif EEP_CONTENT_TravelOdo_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(TravelOdo_val);
	msg[len++] = (uint8_t)(TravelOdo_val>>8);
#else 
	msg[len++] = (uint8_t)(TravelOdo_val);
	msg[len++] = (uint8_t)(TravelOdo_val>>8);
	msg[len++] = (uint8_t)(TravelOdo_val>>16);
	msg[len++] = (uint8_t)(TravelOdo_val>>24);
#endif // {EEP_CONTENT_TravelOdo_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_TravelOdo_ITEM_LEN > 1
void DevEep_Get_TravelOdo(EEP_HANDLE eep_handle,uint32_t *TravelOdo_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_TravelOdo_ITEM_LEN; i ++)
		{
			TravelOdo_val[i] = ap_eep->process_eep.TravelOdo[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_TravelOdo(EEP_HANDLE eep_handle,uint32_t const *TravelOdo_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_TravelOdo_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_TravelOdo_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_TravelOdo_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_TravelOdo_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_TravelOdo_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = TravelOdo_val[i];
#elif EEP_CONTENT_TravelOdo_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(TravelOdo_val[i]);
		msg[len++] = (uint8_t)(TravelOdo_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(TravelOdo_val[i]);
		msg[len++] = (uint8_t)(TravelOdo_val[i]>>8);
		msg[len++] = (uint8_t)(TravelOdo_val[i]>>16);
		msg[len++] = (uint8_t)(TravelOdo_val[i]>>24);
#endif // {EEP_CONTENT_TravelOdo_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_TravelOdo_ITEM_LEN}
void DevEep_Inject_ChgCalbk_TravelOdo(EEP_HANDLE eep_handle,TravelOdo_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_TravelOdo_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_TravelOdo_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_TravelOdo(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		TravelOdo_ChgCallBk TravelOdo_callback =  (TravelOdo_ChgCallBk)callbk;
		TravelOdo_callback(ap,ap->process_eep.TravelOdo);
	}
}

// api define for TripAMeter /////////////////////////////////////////////////////////
#if EEP_CONTENT_TripAMeter_ITEM_LEN == 1
uint32_t DevEep_Get_TripAMeter(EEP_HANDLE eep_handle)
{
	uint32_t TripAMeter_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		TripAMeter_val = ap_eep->process_eep.TripAMeter;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return TripAMeter_val;
}
void DevEep_Set_TripAMeter(EEP_HANDLE eep_handle,uint32_t const TripAMeter_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_TripAMeter_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_TripAMeter_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_TripAMeter_LIST_ID);
#if EEP_CONTENT_TripAMeter_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = TripAMeter_val;
#elif EEP_CONTENT_TripAMeter_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(TripAMeter_val);
	msg[len++] = (uint8_t)(TripAMeter_val>>8);
#else 
	msg[len++] = (uint8_t)(TripAMeter_val);
	msg[len++] = (uint8_t)(TripAMeter_val>>8);
	msg[len++] = (uint8_t)(TripAMeter_val>>16);
	msg[len++] = (uint8_t)(TripAMeter_val>>24);
#endif // {EEP_CONTENT_TripAMeter_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_TripAMeter_ITEM_LEN > 1
void DevEep_Get_TripAMeter(EEP_HANDLE eep_handle,uint32_t *TripAMeter_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_TripAMeter_ITEM_LEN; i ++)
		{
			TripAMeter_val[i] = ap_eep->process_eep.TripAMeter[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_TripAMeter(EEP_HANDLE eep_handle,uint32_t const *TripAMeter_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_TripAMeter_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_TripAMeter_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_TripAMeter_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_TripAMeter_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_TripAMeter_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = TripAMeter_val[i];
#elif EEP_CONTENT_TripAMeter_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(TripAMeter_val[i]);
		msg[len++] = (uint8_t)(TripAMeter_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(TripAMeter_val[i]);
		msg[len++] = (uint8_t)(TripAMeter_val[i]>>8);
		msg[len++] = (uint8_t)(TripAMeter_val[i]>>16);
		msg[len++] = (uint8_t)(TripAMeter_val[i]>>24);
#endif // {EEP_CONTENT_TripAMeter_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_TripAMeter_ITEM_LEN}
void DevEep_Inject_ChgCalbk_TripAMeter(EEP_HANDLE eep_handle,TripAMeter_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_TripAMeter_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_TripAMeter_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_TripAMeter(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		TripAMeter_ChgCallBk TripAMeter_callback =  (TripAMeter_ChgCallBk)callbk;
		TripAMeter_callback(ap,ap->process_eep.TripAMeter);
	}
}

// api define for TripATime /////////////////////////////////////////////////////////
#if EEP_CONTENT_TripATime_ITEM_LEN == 1
uint32_t DevEep_Get_TripATime(EEP_HANDLE eep_handle)
{
	uint32_t TripATime_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		TripATime_val = ap_eep->process_eep.TripATime;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return TripATime_val;
}
void DevEep_Set_TripATime(EEP_HANDLE eep_handle,uint32_t const TripATime_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_TripATime_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_TripATime_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_TripATime_LIST_ID);
#if EEP_CONTENT_TripATime_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = TripATime_val;
#elif EEP_CONTENT_TripATime_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(TripATime_val);
	msg[len++] = (uint8_t)(TripATime_val>>8);
#else 
	msg[len++] = (uint8_t)(TripATime_val);
	msg[len++] = (uint8_t)(TripATime_val>>8);
	msg[len++] = (uint8_t)(TripATime_val>>16);
	msg[len++] = (uint8_t)(TripATime_val>>24);
#endif // {EEP_CONTENT_TripATime_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_TripATime_ITEM_LEN > 1
void DevEep_Get_TripATime(EEP_HANDLE eep_handle,uint32_t *TripATime_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_TripATime_ITEM_LEN; i ++)
		{
			TripATime_val[i] = ap_eep->process_eep.TripATime[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_TripATime(EEP_HANDLE eep_handle,uint32_t const *TripATime_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_TripATime_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_TripATime_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_TripATime_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_TripATime_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_TripATime_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = TripATime_val[i];
#elif EEP_CONTENT_TripATime_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(TripATime_val[i]);
		msg[len++] = (uint8_t)(TripATime_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(TripATime_val[i]);
		msg[len++] = (uint8_t)(TripATime_val[i]>>8);
		msg[len++] = (uint8_t)(TripATime_val[i]>>16);
		msg[len++] = (uint8_t)(TripATime_val[i]>>24);
#endif // {EEP_CONTENT_TripATime_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_TripATime_ITEM_LEN}
void DevEep_Inject_ChgCalbk_TripATime(EEP_HANDLE eep_handle,TripATime_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_TripATime_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_TripATime_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_TripATime(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		TripATime_ChgCallBk TripATime_callback =  (TripATime_ChgCallBk)callbk;
		TripATime_callback(ap,ap->process_eep.TripATime);
	}
}

// api define for TripBMeter /////////////////////////////////////////////////////////
#if EEP_CONTENT_TripBMeter_ITEM_LEN == 1
uint32_t DevEep_Get_TripBMeter(EEP_HANDLE eep_handle)
{
	uint32_t TripBMeter_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		TripBMeter_val = ap_eep->process_eep.TripBMeter;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return TripBMeter_val;
}
void DevEep_Set_TripBMeter(EEP_HANDLE eep_handle,uint32_t const TripBMeter_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_TripBMeter_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_TripBMeter_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_TripBMeter_LIST_ID);
#if EEP_CONTENT_TripBMeter_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = TripBMeter_val;
#elif EEP_CONTENT_TripBMeter_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(TripBMeter_val);
	msg[len++] = (uint8_t)(TripBMeter_val>>8);
#else 
	msg[len++] = (uint8_t)(TripBMeter_val);
	msg[len++] = (uint8_t)(TripBMeter_val>>8);
	msg[len++] = (uint8_t)(TripBMeter_val>>16);
	msg[len++] = (uint8_t)(TripBMeter_val>>24);
#endif // {EEP_CONTENT_TripBMeter_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_TripBMeter_ITEM_LEN > 1
void DevEep_Get_TripBMeter(EEP_HANDLE eep_handle,uint32_t *TripBMeter_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_TripBMeter_ITEM_LEN; i ++)
		{
			TripBMeter_val[i] = ap_eep->process_eep.TripBMeter[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_TripBMeter(EEP_HANDLE eep_handle,uint32_t const *TripBMeter_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_TripBMeter_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_TripBMeter_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_TripBMeter_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_TripBMeter_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_TripBMeter_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = TripBMeter_val[i];
#elif EEP_CONTENT_TripBMeter_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(TripBMeter_val[i]);
		msg[len++] = (uint8_t)(TripBMeter_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(TripBMeter_val[i]);
		msg[len++] = (uint8_t)(TripBMeter_val[i]>>8);
		msg[len++] = (uint8_t)(TripBMeter_val[i]>>16);
		msg[len++] = (uint8_t)(TripBMeter_val[i]>>24);
#endif // {EEP_CONTENT_TripBMeter_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_TripBMeter_ITEM_LEN}
void DevEep_Inject_ChgCalbk_TripBMeter(EEP_HANDLE eep_handle,TripBMeter_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_TripBMeter_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_TripBMeter_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_TripBMeter(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		TripBMeter_ChgCallBk TripBMeter_callback =  (TripBMeter_ChgCallBk)callbk;
		TripBMeter_callback(ap,ap->process_eep.TripBMeter);
	}
}

// api define for TripBTime /////////////////////////////////////////////////////////
#if EEP_CONTENT_TripBTime_ITEM_LEN == 1
uint32_t DevEep_Get_TripBTime(EEP_HANDLE eep_handle)
{
	uint32_t TripBTime_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		TripBTime_val = ap_eep->process_eep.TripBTime;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return TripBTime_val;
}
void DevEep_Set_TripBTime(EEP_HANDLE eep_handle,uint32_t const TripBTime_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_TripBTime_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_TripBTime_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_TripBTime_LIST_ID);
#if EEP_CONTENT_TripBTime_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = TripBTime_val;
#elif EEP_CONTENT_TripBTime_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(TripBTime_val);
	msg[len++] = (uint8_t)(TripBTime_val>>8);
#else 
	msg[len++] = (uint8_t)(TripBTime_val);
	msg[len++] = (uint8_t)(TripBTime_val>>8);
	msg[len++] = (uint8_t)(TripBTime_val>>16);
	msg[len++] = (uint8_t)(TripBTime_val>>24);
#endif // {EEP_CONTENT_TripBTime_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_TripBTime_ITEM_LEN > 1
void DevEep_Get_TripBTime(EEP_HANDLE eep_handle,uint32_t *TripBTime_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_TripBTime_ITEM_LEN; i ++)
		{
			TripBTime_val[i] = ap_eep->process_eep.TripBTime[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_TripBTime(EEP_HANDLE eep_handle,uint32_t const *TripBTime_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_TripBTime_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_TripBTime_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_TripBTime_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_TripBTime_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_TripBTime_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = TripBTime_val[i];
#elif EEP_CONTENT_TripBTime_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(TripBTime_val[i]);
		msg[len++] = (uint8_t)(TripBTime_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(TripBTime_val[i]);
		msg[len++] = (uint8_t)(TripBTime_val[i]>>8);
		msg[len++] = (uint8_t)(TripBTime_val[i]>>16);
		msg[len++] = (uint8_t)(TripBTime_val[i]>>24);
#endif // {EEP_CONTENT_TripBTime_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_TripBTime_ITEM_LEN}
void DevEep_Inject_ChgCalbk_TripBTime(EEP_HANDLE eep_handle,TripBTime_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_TripBTime_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_TripBTime_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_TripBTime(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		TripBTime_ChgCallBk TripBTime_callback =  (TripBTime_ChgCallBk)callbk;
		TripBTime_callback(ap,ap->process_eep.TripBTime);
	}
}

// api define for CruiseDistance /////////////////////////////////////////////////////////
#if EEP_CONTENT_CruiseDistance_ITEM_LEN == 1
uint32_t DevEep_Get_CruiseDistance(EEP_HANDLE eep_handle)
{
	uint32_t CruiseDistance_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		CruiseDistance_val = ap_eep->process_eep.CruiseDistance;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return CruiseDistance_val;
}
void DevEep_Set_CruiseDistance(EEP_HANDLE eep_handle,uint32_t const CruiseDistance_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_CruiseDistance_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_CruiseDistance_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_CruiseDistance_LIST_ID);
#if EEP_CONTENT_CruiseDistance_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = CruiseDistance_val;
#elif EEP_CONTENT_CruiseDistance_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(CruiseDistance_val);
	msg[len++] = (uint8_t)(CruiseDistance_val>>8);
#else 
	msg[len++] = (uint8_t)(CruiseDistance_val);
	msg[len++] = (uint8_t)(CruiseDistance_val>>8);
	msg[len++] = (uint8_t)(CruiseDistance_val>>16);
	msg[len++] = (uint8_t)(CruiseDistance_val>>24);
#endif // {EEP_CONTENT_CruiseDistance_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_CruiseDistance_ITEM_LEN > 1
void DevEep_Get_CruiseDistance(EEP_HANDLE eep_handle,uint32_t *CruiseDistance_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_CruiseDistance_ITEM_LEN; i ++)
		{
			CruiseDistance_val[i] = ap_eep->process_eep.CruiseDistance[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_CruiseDistance(EEP_HANDLE eep_handle,uint32_t const *CruiseDistance_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_CruiseDistance_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_CruiseDistance_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_CruiseDistance_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_CruiseDistance_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_CruiseDistance_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = CruiseDistance_val[i];
#elif EEP_CONTENT_CruiseDistance_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(CruiseDistance_val[i]);
		msg[len++] = (uint8_t)(CruiseDistance_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(CruiseDistance_val[i]);
		msg[len++] = (uint8_t)(CruiseDistance_val[i]>>8);
		msg[len++] = (uint8_t)(CruiseDistance_val[i]>>16);
		msg[len++] = (uint8_t)(CruiseDistance_val[i]>>24);
#endif // {EEP_CONTENT_CruiseDistance_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_CruiseDistance_ITEM_LEN}
void DevEep_Inject_ChgCalbk_CruiseDistance(EEP_HANDLE eep_handle,CruiseDistance_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_CruiseDistance_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_CruiseDistance_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_CruiseDistance(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		CruiseDistance_ChgCallBk CruiseDistance_callback =  (CruiseDistance_ChgCallBk)callbk;
		CruiseDistance_callback(ap,ap->process_eep.CruiseDistance);
	}
}

// api define for VipSwdlShareMem /////////////////////////////////////////////////////////
#if EEP_CONTENT_VipSwdlShareMem_ITEM_LEN == 1
uint8_t DevEep_Get_VipSwdlShareMem(EEP_HANDLE eep_handle)
{
	uint8_t VipSwdlShareMem_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		VipSwdlShareMem_val = ap_eep->process_eep.VipSwdlShareMem;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return VipSwdlShareMem_val;
}
void DevEep_Set_VipSwdlShareMem(EEP_HANDLE eep_handle,uint8_t const VipSwdlShareMem_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_VipSwdlShareMem_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_VipSwdlShareMem_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_VipSwdlShareMem_LIST_ID);
#if EEP_CONTENT_VipSwdlShareMem_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = VipSwdlShareMem_val;
#elif EEP_CONTENT_VipSwdlShareMem_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(VipSwdlShareMem_val);
	msg[len++] = (uint8_t)(VipSwdlShareMem_val>>8);
#else 
	msg[len++] = (uint8_t)(VipSwdlShareMem_val);
	msg[len++] = (uint8_t)(VipSwdlShareMem_val>>8);
	msg[len++] = (uint8_t)(VipSwdlShareMem_val>>16);
	msg[len++] = (uint8_t)(VipSwdlShareMem_val>>24);
#endif // {EEP_CONTENT_VipSwdlShareMem_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_VipSwdlShareMem_ITEM_LEN > 1
void DevEep_Get_VipSwdlShareMem(EEP_HANDLE eep_handle,uint8_t *VipSwdlShareMem_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_VipSwdlShareMem_ITEM_LEN; i ++)
		{
			VipSwdlShareMem_val[i] = ap_eep->process_eep.VipSwdlShareMem[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_VipSwdlShareMem(EEP_HANDLE eep_handle,uint8_t const *VipSwdlShareMem_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_VipSwdlShareMem_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_VipSwdlShareMem_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_VipSwdlShareMem_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_VipSwdlShareMem_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_VipSwdlShareMem_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = VipSwdlShareMem_val[i];
#elif EEP_CONTENT_VipSwdlShareMem_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(VipSwdlShareMem_val[i]);
		msg[len++] = (uint8_t)(VipSwdlShareMem_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(VipSwdlShareMem_val[i]);
		msg[len++] = (uint8_t)(VipSwdlShareMem_val[i]>>8);
		msg[len++] = (uint8_t)(VipSwdlShareMem_val[i]>>16);
		msg[len++] = (uint8_t)(VipSwdlShareMem_val[i]>>24);
#endif // {EEP_CONTENT_VipSwdlShareMem_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_VipSwdlShareMem_ITEM_LEN}
void DevEep_Inject_ChgCalbk_VipSwdlShareMem(EEP_HANDLE eep_handle,VipSwdlShareMem_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_VipSwdlShareMem_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_VipSwdlShareMem_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_VipSwdlShareMem(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		VipSwdlShareMem_ChgCallBk VipSwdlShareMem_callback =  (VipSwdlShareMem_ChgCallBk)callbk;
		VipSwdlShareMem_callback(ap,ap->process_eep.VipSwdlShareMem);
	}
}

// api define for TotalImageSFailCnt /////////////////////////////////////////////////////////
#if EEP_CONTENT_TotalImageSFailCnt_ITEM_LEN == 1
uint32_t DevEep_Get_TotalImageSFailCnt(EEP_HANDLE eep_handle)
{
	uint32_t TotalImageSFailCnt_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		TotalImageSFailCnt_val = ap_eep->process_eep.TotalImageSFailCnt;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return TotalImageSFailCnt_val;
}
void DevEep_Set_TotalImageSFailCnt(EEP_HANDLE eep_handle,uint32_t const TotalImageSFailCnt_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_TotalImageSFailCnt_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_TotalImageSFailCnt_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_TotalImageSFailCnt_LIST_ID);
#if EEP_CONTENT_TotalImageSFailCnt_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = TotalImageSFailCnt_val;
#elif EEP_CONTENT_TotalImageSFailCnt_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(TotalImageSFailCnt_val);
	msg[len++] = (uint8_t)(TotalImageSFailCnt_val>>8);
#else 
	msg[len++] = (uint8_t)(TotalImageSFailCnt_val);
	msg[len++] = (uint8_t)(TotalImageSFailCnt_val>>8);
	msg[len++] = (uint8_t)(TotalImageSFailCnt_val>>16);
	msg[len++] = (uint8_t)(TotalImageSFailCnt_val>>24);
#endif // {EEP_CONTENT_TotalImageSFailCnt_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_TotalImageSFailCnt_ITEM_LEN > 1
void DevEep_Get_TotalImageSFailCnt(EEP_HANDLE eep_handle,uint32_t *TotalImageSFailCnt_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_TotalImageSFailCnt_ITEM_LEN; i ++)
		{
			TotalImageSFailCnt_val[i] = ap_eep->process_eep.TotalImageSFailCnt[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_TotalImageSFailCnt(EEP_HANDLE eep_handle,uint32_t const *TotalImageSFailCnt_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_TotalImageSFailCnt_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_TotalImageSFailCnt_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_TotalImageSFailCnt_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_TotalImageSFailCnt_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_TotalImageSFailCnt_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = TotalImageSFailCnt_val[i];
#elif EEP_CONTENT_TotalImageSFailCnt_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(TotalImageSFailCnt_val[i]);
		msg[len++] = (uint8_t)(TotalImageSFailCnt_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(TotalImageSFailCnt_val[i]);
		msg[len++] = (uint8_t)(TotalImageSFailCnt_val[i]>>8);
		msg[len++] = (uint8_t)(TotalImageSFailCnt_val[i]>>16);
		msg[len++] = (uint8_t)(TotalImageSFailCnt_val[i]>>24);
#endif // {EEP_CONTENT_TotalImageSFailCnt_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_TotalImageSFailCnt_ITEM_LEN}
void DevEep_Inject_ChgCalbk_TotalImageSFailCnt(EEP_HANDLE eep_handle,TotalImageSFailCnt_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_TotalImageSFailCnt_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_TotalImageSFailCnt_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_TotalImageSFailCnt(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		TotalImageSFailCnt_ChgCallBk TotalImageSFailCnt_callback =  (TotalImageSFailCnt_ChgCallBk)callbk;
		TotalImageSFailCnt_callback(ap,ap->process_eep.TotalImageSFailCnt);
	}
}

// api define for trace_phy_en /////////////////////////////////////////////////////////
#if EEP_CONTENT_trace_phy_en_ITEM_LEN == 1
uint16_t DevEep_Get_trace_phy_en(EEP_HANDLE eep_handle)
{
	uint16_t trace_phy_en_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		trace_phy_en_val = ap_eep->process_eep.trace_phy_en;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return trace_phy_en_val;
}
void DevEep_Set_trace_phy_en(EEP_HANDLE eep_handle,uint16_t const trace_phy_en_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_trace_phy_en_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_trace_phy_en_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_trace_phy_en_LIST_ID);
#if EEP_CONTENT_trace_phy_en_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = trace_phy_en_val;
#elif EEP_CONTENT_trace_phy_en_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(trace_phy_en_val);
	msg[len++] = (uint8_t)(trace_phy_en_val>>8);
#else 
	msg[len++] = (uint8_t)(trace_phy_en_val);
	msg[len++] = (uint8_t)(trace_phy_en_val>>8);
	msg[len++] = (uint8_t)(trace_phy_en_val>>16);
	msg[len++] = (uint8_t)(trace_phy_en_val>>24);
#endif // {EEP_CONTENT_trace_phy_en_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_trace_phy_en_ITEM_LEN > 1
void DevEep_Get_trace_phy_en(EEP_HANDLE eep_handle,uint16_t *trace_phy_en_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_trace_phy_en_ITEM_LEN; i ++)
		{
			trace_phy_en_val[i] = ap_eep->process_eep.trace_phy_en[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_trace_phy_en(EEP_HANDLE eep_handle,uint16_t const *trace_phy_en_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_trace_phy_en_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_trace_phy_en_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_trace_phy_en_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_trace_phy_en_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_trace_phy_en_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = trace_phy_en_val[i];
#elif EEP_CONTENT_trace_phy_en_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(trace_phy_en_val[i]);
		msg[len++] = (uint8_t)(trace_phy_en_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(trace_phy_en_val[i]);
		msg[len++] = (uint8_t)(trace_phy_en_val[i]>>8);
		msg[len++] = (uint8_t)(trace_phy_en_val[i]>>16);
		msg[len++] = (uint8_t)(trace_phy_en_val[i]>>24);
#endif // {EEP_CONTENT_trace_phy_en_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_trace_phy_en_ITEM_LEN}
void DevEep_Inject_ChgCalbk_trace_phy_en(EEP_HANDLE eep_handle,trace_phy_en_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_trace_phy_en_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_trace_phy_en_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_trace_phy_en(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		trace_phy_en_ChgCallBk trace_phy_en_callback =  (trace_phy_en_ChgCallBk)callbk;
		trace_phy_en_callback(ap,ap->process_eep.trace_phy_en);
	}
}

// api define for trace_phy /////////////////////////////////////////////////////////
#if EEP_CONTENT_trace_phy_ITEM_LEN == 1
uint8_t DevEep_Get_trace_phy(EEP_HANDLE eep_handle)
{
	uint8_t trace_phy_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		trace_phy_val = ap_eep->process_eep.trace_phy;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return trace_phy_val;
}
void DevEep_Set_trace_phy(EEP_HANDLE eep_handle,uint8_t const trace_phy_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_trace_phy_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_trace_phy_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_trace_phy_LIST_ID);
#if EEP_CONTENT_trace_phy_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = trace_phy_val;
#elif EEP_CONTENT_trace_phy_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(trace_phy_val);
	msg[len++] = (uint8_t)(trace_phy_val>>8);
#else 
	msg[len++] = (uint8_t)(trace_phy_val);
	msg[len++] = (uint8_t)(trace_phy_val>>8);
	msg[len++] = (uint8_t)(trace_phy_val>>16);
	msg[len++] = (uint8_t)(trace_phy_val>>24);
#endif // {EEP_CONTENT_trace_phy_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_trace_phy_ITEM_LEN > 1
void DevEep_Get_trace_phy(EEP_HANDLE eep_handle,uint8_t *trace_phy_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_trace_phy_ITEM_LEN; i ++)
		{
			trace_phy_val[i] = ap_eep->process_eep.trace_phy[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_trace_phy(EEP_HANDLE eep_handle,uint8_t const *trace_phy_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_trace_phy_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_trace_phy_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_trace_phy_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_trace_phy_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_trace_phy_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = trace_phy_val[i];
#elif EEP_CONTENT_trace_phy_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(trace_phy_val[i]);
		msg[len++] = (uint8_t)(trace_phy_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(trace_phy_val[i]);
		msg[len++] = (uint8_t)(trace_phy_val[i]>>8);
		msg[len++] = (uint8_t)(trace_phy_val[i]>>16);
		msg[len++] = (uint8_t)(trace_phy_val[i]>>24);
#endif // {EEP_CONTENT_trace_phy_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_trace_phy_ITEM_LEN}
void DevEep_Inject_ChgCalbk_trace_phy(EEP_HANDLE eep_handle,trace_phy_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_trace_phy_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_trace_phy_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_trace_phy(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		trace_phy_ChgCallBk trace_phy_callback =  (trace_phy_ChgCallBk)callbk;
		trace_phy_callback(ap,ap->process_eep.trace_phy);
	}
}

// api define for VipLogModLvl /////////////////////////////////////////////////////////
#if EEP_CONTENT_VipLogModLvl_ITEM_LEN == 1
uint8_t DevEep_Get_VipLogModLvl(EEP_HANDLE eep_handle)
{
	uint8_t VipLogModLvl_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		VipLogModLvl_val = ap_eep->process_eep.VipLogModLvl;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return VipLogModLvl_val;
}
void DevEep_Set_VipLogModLvl(EEP_HANDLE eep_handle,uint8_t const VipLogModLvl_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_VipLogModLvl_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_VipLogModLvl_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_VipLogModLvl_LIST_ID);
#if EEP_CONTENT_VipLogModLvl_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = VipLogModLvl_val;
#elif EEP_CONTENT_VipLogModLvl_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(VipLogModLvl_val);
	msg[len++] = (uint8_t)(VipLogModLvl_val>>8);
#else 
	msg[len++] = (uint8_t)(VipLogModLvl_val);
	msg[len++] = (uint8_t)(VipLogModLvl_val>>8);
	msg[len++] = (uint8_t)(VipLogModLvl_val>>16);
	msg[len++] = (uint8_t)(VipLogModLvl_val>>24);
#endif // {EEP_CONTENT_VipLogModLvl_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_VipLogModLvl_ITEM_LEN > 1
void DevEep_Get_VipLogModLvl(EEP_HANDLE eep_handle,uint8_t *VipLogModLvl_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_VipLogModLvl_ITEM_LEN; i ++)
		{
			VipLogModLvl_val[i] = ap_eep->process_eep.VipLogModLvl[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_VipLogModLvl(EEP_HANDLE eep_handle,uint8_t const *VipLogModLvl_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_VipLogModLvl_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_VipLogModLvl_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_VipLogModLvl_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_VipLogModLvl_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_VipLogModLvl_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = VipLogModLvl_val[i];
#elif EEP_CONTENT_VipLogModLvl_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(VipLogModLvl_val[i]);
		msg[len++] = (uint8_t)(VipLogModLvl_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(VipLogModLvl_val[i]);
		msg[len++] = (uint8_t)(VipLogModLvl_val[i]>>8);
		msg[len++] = (uint8_t)(VipLogModLvl_val[i]>>16);
		msg[len++] = (uint8_t)(VipLogModLvl_val[i]>>24);
#endif // {EEP_CONTENT_VipLogModLvl_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_VipLogModLvl_ITEM_LEN}
void DevEep_Inject_ChgCalbk_VipLogModLvl(EEP_HANDLE eep_handle,VipLogModLvl_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_VipLogModLvl_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_VipLogModLvl_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_VipLogModLvl(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		VipLogModLvl_ChgCallBk VipLogModLvl_callback =  (VipLogModLvl_ChgCallBk)callbk;
		VipLogModLvl_callback(ap,ap->process_eep.VipLogModLvl);
	}
}

// api define for TraceModuleLvlCrc /////////////////////////////////////////////////////////
#if EEP_CONTENT_TraceModuleLvlCrc_ITEM_LEN == 1
uint16_t DevEep_Get_TraceModuleLvlCrc(EEP_HANDLE eep_handle)
{
	uint16_t TraceModuleLvlCrc_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		TraceModuleLvlCrc_val = ap_eep->process_eep.TraceModuleLvlCrc;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return TraceModuleLvlCrc_val;
}
void DevEep_Set_TraceModuleLvlCrc(EEP_HANDLE eep_handle,uint16_t const TraceModuleLvlCrc_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_TraceModuleLvlCrc_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_TraceModuleLvlCrc_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_TraceModuleLvlCrc_LIST_ID);
#if EEP_CONTENT_TraceModuleLvlCrc_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = TraceModuleLvlCrc_val;
#elif EEP_CONTENT_TraceModuleLvlCrc_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(TraceModuleLvlCrc_val);
	msg[len++] = (uint8_t)(TraceModuleLvlCrc_val>>8);
#else 
	msg[len++] = (uint8_t)(TraceModuleLvlCrc_val);
	msg[len++] = (uint8_t)(TraceModuleLvlCrc_val>>8);
	msg[len++] = (uint8_t)(TraceModuleLvlCrc_val>>16);
	msg[len++] = (uint8_t)(TraceModuleLvlCrc_val>>24);
#endif // {EEP_CONTENT_TraceModuleLvlCrc_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_TraceModuleLvlCrc_ITEM_LEN > 1
void DevEep_Get_TraceModuleLvlCrc(EEP_HANDLE eep_handle,uint16_t *TraceModuleLvlCrc_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_TraceModuleLvlCrc_ITEM_LEN; i ++)
		{
			TraceModuleLvlCrc_val[i] = ap_eep->process_eep.TraceModuleLvlCrc[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_TraceModuleLvlCrc(EEP_HANDLE eep_handle,uint16_t const *TraceModuleLvlCrc_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_TraceModuleLvlCrc_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_TraceModuleLvlCrc_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_TraceModuleLvlCrc_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_TraceModuleLvlCrc_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_TraceModuleLvlCrc_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = TraceModuleLvlCrc_val[i];
#elif EEP_CONTENT_TraceModuleLvlCrc_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(TraceModuleLvlCrc_val[i]);
		msg[len++] = (uint8_t)(TraceModuleLvlCrc_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(TraceModuleLvlCrc_val[i]);
		msg[len++] = (uint8_t)(TraceModuleLvlCrc_val[i]>>8);
		msg[len++] = (uint8_t)(TraceModuleLvlCrc_val[i]>>16);
		msg[len++] = (uint8_t)(TraceModuleLvlCrc_val[i]>>24);
#endif // {EEP_CONTENT_TraceModuleLvlCrc_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_TraceModuleLvlCrc_ITEM_LEN}
void DevEep_Inject_ChgCalbk_TraceModuleLvlCrc(EEP_HANDLE eep_handle,TraceModuleLvlCrc_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_TraceModuleLvlCrc_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_TraceModuleLvlCrc_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_TraceModuleLvlCrc(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		TraceModuleLvlCrc_ChgCallBk TraceModuleLvlCrc_callback =  (TraceModuleLvlCrc_ChgCallBk)callbk;
		TraceModuleLvlCrc_callback(ap,ap->process_eep.TraceModuleLvlCrc);
	}
}

// api define for TotalReset_Cnt /////////////////////////////////////////////////////////
#if EEP_CONTENT_TotalReset_Cnt_ITEM_LEN == 1
uint32_t DevEep_Get_TotalReset_Cnt(EEP_HANDLE eep_handle)
{
	uint32_t TotalReset_Cnt_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		TotalReset_Cnt_val = ap_eep->process_eep.TotalReset_Cnt;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return TotalReset_Cnt_val;
}
void DevEep_Set_TotalReset_Cnt(EEP_HANDLE eep_handle,uint32_t const TotalReset_Cnt_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_TotalReset_Cnt_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_TotalReset_Cnt_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_TotalReset_Cnt_LIST_ID);
#if EEP_CONTENT_TotalReset_Cnt_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = TotalReset_Cnt_val;
#elif EEP_CONTENT_TotalReset_Cnt_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(TotalReset_Cnt_val);
	msg[len++] = (uint8_t)(TotalReset_Cnt_val>>8);
#else 
	msg[len++] = (uint8_t)(TotalReset_Cnt_val);
	msg[len++] = (uint8_t)(TotalReset_Cnt_val>>8);
	msg[len++] = (uint8_t)(TotalReset_Cnt_val>>16);
	msg[len++] = (uint8_t)(TotalReset_Cnt_val>>24);
#endif // {EEP_CONTENT_TotalReset_Cnt_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_TotalReset_Cnt_ITEM_LEN > 1
void DevEep_Get_TotalReset_Cnt(EEP_HANDLE eep_handle,uint32_t *TotalReset_Cnt_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_TotalReset_Cnt_ITEM_LEN; i ++)
		{
			TotalReset_Cnt_val[i] = ap_eep->process_eep.TotalReset_Cnt[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_TotalReset_Cnt(EEP_HANDLE eep_handle,uint32_t const *TotalReset_Cnt_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_TotalReset_Cnt_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_TotalReset_Cnt_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_TotalReset_Cnt_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_TotalReset_Cnt_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_TotalReset_Cnt_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = TotalReset_Cnt_val[i];
#elif EEP_CONTENT_TotalReset_Cnt_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(TotalReset_Cnt_val[i]);
		msg[len++] = (uint8_t)(TotalReset_Cnt_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(TotalReset_Cnt_val[i]);
		msg[len++] = (uint8_t)(TotalReset_Cnt_val[i]>>8);
		msg[len++] = (uint8_t)(TotalReset_Cnt_val[i]>>16);
		msg[len++] = (uint8_t)(TotalReset_Cnt_val[i]>>24);
#endif // {EEP_CONTENT_TotalReset_Cnt_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_TotalReset_Cnt_ITEM_LEN}
void DevEep_Inject_ChgCalbk_TotalReset_Cnt(EEP_HANDLE eep_handle,TotalReset_Cnt_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_TotalReset_Cnt_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_TotalReset_Cnt_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_TotalReset_Cnt(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		TotalReset_Cnt_ChgCallBk TotalReset_Cnt_callback =  (TotalReset_Cnt_ChgCallBk)callbk;
		TotalReset_Cnt_callback(ap,ap->process_eep.TotalReset_Cnt);
	}
}

// api define for DspPOR_cnt /////////////////////////////////////////////////////////
#if EEP_CONTENT_DspPOR_cnt_ITEM_LEN == 1
uint16_t DevEep_Get_DspPOR_cnt(EEP_HANDLE eep_handle)
{
	uint16_t DspPOR_cnt_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		DspPOR_cnt_val = ap_eep->process_eep.DspPOR_cnt;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return DspPOR_cnt_val;
}
void DevEep_Set_DspPOR_cnt(EEP_HANDLE eep_handle,uint16_t const DspPOR_cnt_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_DspPOR_cnt_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_DspPOR_cnt_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_DspPOR_cnt_LIST_ID);
#if EEP_CONTENT_DspPOR_cnt_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = DspPOR_cnt_val;
#elif EEP_CONTENT_DspPOR_cnt_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(DspPOR_cnt_val);
	msg[len++] = (uint8_t)(DspPOR_cnt_val>>8);
#else 
	msg[len++] = (uint8_t)(DspPOR_cnt_val);
	msg[len++] = (uint8_t)(DspPOR_cnt_val>>8);
	msg[len++] = (uint8_t)(DspPOR_cnt_val>>16);
	msg[len++] = (uint8_t)(DspPOR_cnt_val>>24);
#endif // {EEP_CONTENT_DspPOR_cnt_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_DspPOR_cnt_ITEM_LEN > 1
void DevEep_Get_DspPOR_cnt(EEP_HANDLE eep_handle,uint16_t *DspPOR_cnt_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_DspPOR_cnt_ITEM_LEN; i ++)
		{
			DspPOR_cnt_val[i] = ap_eep->process_eep.DspPOR_cnt[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_DspPOR_cnt(EEP_HANDLE eep_handle,uint16_t const *DspPOR_cnt_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_DspPOR_cnt_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_DspPOR_cnt_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_DspPOR_cnt_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_DspPOR_cnt_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_DspPOR_cnt_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = DspPOR_cnt_val[i];
#elif EEP_CONTENT_DspPOR_cnt_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(DspPOR_cnt_val[i]);
		msg[len++] = (uint8_t)(DspPOR_cnt_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(DspPOR_cnt_val[i]);
		msg[len++] = (uint8_t)(DspPOR_cnt_val[i]>>8);
		msg[len++] = (uint8_t)(DspPOR_cnt_val[i]>>16);
		msg[len++] = (uint8_t)(DspPOR_cnt_val[i]>>24);
#endif // {EEP_CONTENT_DspPOR_cnt_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_DspPOR_cnt_ITEM_LEN}
void DevEep_Inject_ChgCalbk_DspPOR_cnt(EEP_HANDLE eep_handle,DspPOR_cnt_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_DspPOR_cnt_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_DspPOR_cnt_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_DspPOR_cnt(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		DspPOR_cnt_ChgCallBk DspPOR_cnt_callback =  (DspPOR_cnt_ChgCallBk)callbk;
		DspPOR_cnt_callback(ap,ap->process_eep.DspPOR_cnt);
	}
}

// api define for AudioCmdExecMaxTm /////////////////////////////////////////////////////////
#if EEP_CONTENT_AudioCmdExecMaxTm_ITEM_LEN == 1
uint16_t DevEep_Get_AudioCmdExecMaxTm(EEP_HANDLE eep_handle)
{
	uint16_t AudioCmdExecMaxTm_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		AudioCmdExecMaxTm_val = ap_eep->process_eep.AudioCmdExecMaxTm;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return AudioCmdExecMaxTm_val;
}
void DevEep_Set_AudioCmdExecMaxTm(EEP_HANDLE eep_handle,uint16_t const AudioCmdExecMaxTm_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_AudioCmdExecMaxTm_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_AudioCmdExecMaxTm_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_AudioCmdExecMaxTm_LIST_ID);
#if EEP_CONTENT_AudioCmdExecMaxTm_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = AudioCmdExecMaxTm_val;
#elif EEP_CONTENT_AudioCmdExecMaxTm_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(AudioCmdExecMaxTm_val);
	msg[len++] = (uint8_t)(AudioCmdExecMaxTm_val>>8);
#else 
	msg[len++] = (uint8_t)(AudioCmdExecMaxTm_val);
	msg[len++] = (uint8_t)(AudioCmdExecMaxTm_val>>8);
	msg[len++] = (uint8_t)(AudioCmdExecMaxTm_val>>16);
	msg[len++] = (uint8_t)(AudioCmdExecMaxTm_val>>24);
#endif // {EEP_CONTENT_AudioCmdExecMaxTm_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_AudioCmdExecMaxTm_ITEM_LEN > 1
void DevEep_Get_AudioCmdExecMaxTm(EEP_HANDLE eep_handle,uint16_t *AudioCmdExecMaxTm_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_AudioCmdExecMaxTm_ITEM_LEN; i ++)
		{
			AudioCmdExecMaxTm_val[i] = ap_eep->process_eep.AudioCmdExecMaxTm[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_AudioCmdExecMaxTm(EEP_HANDLE eep_handle,uint16_t const *AudioCmdExecMaxTm_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_AudioCmdExecMaxTm_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_AudioCmdExecMaxTm_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_AudioCmdExecMaxTm_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_AudioCmdExecMaxTm_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_AudioCmdExecMaxTm_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = AudioCmdExecMaxTm_val[i];
#elif EEP_CONTENT_AudioCmdExecMaxTm_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(AudioCmdExecMaxTm_val[i]);
		msg[len++] = (uint8_t)(AudioCmdExecMaxTm_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(AudioCmdExecMaxTm_val[i]);
		msg[len++] = (uint8_t)(AudioCmdExecMaxTm_val[i]>>8);
		msg[len++] = (uint8_t)(AudioCmdExecMaxTm_val[i]>>16);
		msg[len++] = (uint8_t)(AudioCmdExecMaxTm_val[i]>>24);
#endif // {EEP_CONTENT_AudioCmdExecMaxTm_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_AudioCmdExecMaxTm_ITEM_LEN}
void DevEep_Inject_ChgCalbk_AudioCmdExecMaxTm(EEP_HANDLE eep_handle,AudioCmdExecMaxTm_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_AudioCmdExecMaxTm_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_AudioCmdExecMaxTm_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_AudioCmdExecMaxTm(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		AudioCmdExecMaxTm_ChgCallBk AudioCmdExecMaxTm_callback =  (AudioCmdExecMaxTm_ChgCallBk)callbk;
		AudioCmdExecMaxTm_callback(ap,ap->process_eep.AudioCmdExecMaxTm);
	}
}

// api define for RadioCmdExecMaxTm /////////////////////////////////////////////////////////
#if EEP_CONTENT_RadioCmdExecMaxTm_ITEM_LEN == 1
uint16_t DevEep_Get_RadioCmdExecMaxTm(EEP_HANDLE eep_handle)
{
	uint16_t RadioCmdExecMaxTm_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		RadioCmdExecMaxTm_val = ap_eep->process_eep.RadioCmdExecMaxTm;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return RadioCmdExecMaxTm_val;
}
void DevEep_Set_RadioCmdExecMaxTm(EEP_HANDLE eep_handle,uint16_t const RadioCmdExecMaxTm_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_RadioCmdExecMaxTm_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_RadioCmdExecMaxTm_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_RadioCmdExecMaxTm_LIST_ID);
#if EEP_CONTENT_RadioCmdExecMaxTm_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = RadioCmdExecMaxTm_val;
#elif EEP_CONTENT_RadioCmdExecMaxTm_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(RadioCmdExecMaxTm_val);
	msg[len++] = (uint8_t)(RadioCmdExecMaxTm_val>>8);
#else 
	msg[len++] = (uint8_t)(RadioCmdExecMaxTm_val);
	msg[len++] = (uint8_t)(RadioCmdExecMaxTm_val>>8);
	msg[len++] = (uint8_t)(RadioCmdExecMaxTm_val>>16);
	msg[len++] = (uint8_t)(RadioCmdExecMaxTm_val>>24);
#endif // {EEP_CONTENT_RadioCmdExecMaxTm_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_RadioCmdExecMaxTm_ITEM_LEN > 1
void DevEep_Get_RadioCmdExecMaxTm(EEP_HANDLE eep_handle,uint16_t *RadioCmdExecMaxTm_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_RadioCmdExecMaxTm_ITEM_LEN; i ++)
		{
			RadioCmdExecMaxTm_val[i] = ap_eep->process_eep.RadioCmdExecMaxTm[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_RadioCmdExecMaxTm(EEP_HANDLE eep_handle,uint16_t const *RadioCmdExecMaxTm_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_RadioCmdExecMaxTm_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_RadioCmdExecMaxTm_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_RadioCmdExecMaxTm_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_RadioCmdExecMaxTm_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_RadioCmdExecMaxTm_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = RadioCmdExecMaxTm_val[i];
#elif EEP_CONTENT_RadioCmdExecMaxTm_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(RadioCmdExecMaxTm_val[i]);
		msg[len++] = (uint8_t)(RadioCmdExecMaxTm_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(RadioCmdExecMaxTm_val[i]);
		msg[len++] = (uint8_t)(RadioCmdExecMaxTm_val[i]>>8);
		msg[len++] = (uint8_t)(RadioCmdExecMaxTm_val[i]>>16);
		msg[len++] = (uint8_t)(RadioCmdExecMaxTm_val[i]>>24);
#endif // {EEP_CONTENT_RadioCmdExecMaxTm_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_RadioCmdExecMaxTm_ITEM_LEN}
void DevEep_Inject_ChgCalbk_RadioCmdExecMaxTm(EEP_HANDLE eep_handle,RadioCmdExecMaxTm_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_RadioCmdExecMaxTm_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_RadioCmdExecMaxTm_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_RadioCmdExecMaxTm(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		RadioCmdExecMaxTm_ChgCallBk RadioCmdExecMaxTm_callback =  (RadioCmdExecMaxTm_ChgCallBk)callbk;
		RadioCmdExecMaxTm_callback(ap,ap->process_eep.RadioCmdExecMaxTm);
	}
}

// api define for CpuDisIntMaxNest /////////////////////////////////////////////////////////
#if EEP_CONTENT_CpuDisIntMaxNest_ITEM_LEN == 1
uint16_t DevEep_Get_CpuDisIntMaxNest(EEP_HANDLE eep_handle)
{
	uint16_t CpuDisIntMaxNest_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		CpuDisIntMaxNest_val = ap_eep->process_eep.CpuDisIntMaxNest;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return CpuDisIntMaxNest_val;
}
void DevEep_Set_CpuDisIntMaxNest(EEP_HANDLE eep_handle,uint16_t const CpuDisIntMaxNest_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_CpuDisIntMaxNest_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_CpuDisIntMaxNest_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_CpuDisIntMaxNest_LIST_ID);
#if EEP_CONTENT_CpuDisIntMaxNest_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = CpuDisIntMaxNest_val;
#elif EEP_CONTENT_CpuDisIntMaxNest_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(CpuDisIntMaxNest_val);
	msg[len++] = (uint8_t)(CpuDisIntMaxNest_val>>8);
#else 
	msg[len++] = (uint8_t)(CpuDisIntMaxNest_val);
	msg[len++] = (uint8_t)(CpuDisIntMaxNest_val>>8);
	msg[len++] = (uint8_t)(CpuDisIntMaxNest_val>>16);
	msg[len++] = (uint8_t)(CpuDisIntMaxNest_val>>24);
#endif // {EEP_CONTENT_CpuDisIntMaxNest_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_CpuDisIntMaxNest_ITEM_LEN > 1
void DevEep_Get_CpuDisIntMaxNest(EEP_HANDLE eep_handle,uint16_t *CpuDisIntMaxNest_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_CpuDisIntMaxNest_ITEM_LEN; i ++)
		{
			CpuDisIntMaxNest_val[i] = ap_eep->process_eep.CpuDisIntMaxNest[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_CpuDisIntMaxNest(EEP_HANDLE eep_handle,uint16_t const *CpuDisIntMaxNest_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_CpuDisIntMaxNest_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_CpuDisIntMaxNest_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_CpuDisIntMaxNest_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_CpuDisIntMaxNest_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_CpuDisIntMaxNest_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = CpuDisIntMaxNest_val[i];
#elif EEP_CONTENT_CpuDisIntMaxNest_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(CpuDisIntMaxNest_val[i]);
		msg[len++] = (uint8_t)(CpuDisIntMaxNest_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(CpuDisIntMaxNest_val[i]);
		msg[len++] = (uint8_t)(CpuDisIntMaxNest_val[i]>>8);
		msg[len++] = (uint8_t)(CpuDisIntMaxNest_val[i]>>16);
		msg[len++] = (uint8_t)(CpuDisIntMaxNest_val[i]>>24);
#endif // {EEP_CONTENT_CpuDisIntMaxNest_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_CpuDisIntMaxNest_ITEM_LEN}
void DevEep_Inject_ChgCalbk_CpuDisIntMaxNest(EEP_HANDLE eep_handle,CpuDisIntMaxNest_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_CpuDisIntMaxNest_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_CpuDisIntMaxNest_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_CpuDisIntMaxNest(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		CpuDisIntMaxNest_ChgCallBk CpuDisIntMaxNest_callback =  (CpuDisIntMaxNest_ChgCallBk)callbk;
		CpuDisIntMaxNest_callback(ap,ap->process_eep.CpuDisIntMaxNest);
	}
}

// api define for CpuDisIntMaxTm /////////////////////////////////////////////////////////
#if EEP_CONTENT_CpuDisIntMaxTm_ITEM_LEN == 1
uint16_t DevEep_Get_CpuDisIntMaxTm(EEP_HANDLE eep_handle)
{
	uint16_t CpuDisIntMaxTm_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		CpuDisIntMaxTm_val = ap_eep->process_eep.CpuDisIntMaxTm;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return CpuDisIntMaxTm_val;
}
void DevEep_Set_CpuDisIntMaxTm(EEP_HANDLE eep_handle,uint16_t const CpuDisIntMaxTm_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_CpuDisIntMaxTm_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_CpuDisIntMaxTm_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_CpuDisIntMaxTm_LIST_ID);
#if EEP_CONTENT_CpuDisIntMaxTm_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = CpuDisIntMaxTm_val;
#elif EEP_CONTENT_CpuDisIntMaxTm_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(CpuDisIntMaxTm_val);
	msg[len++] = (uint8_t)(CpuDisIntMaxTm_val>>8);
#else 
	msg[len++] = (uint8_t)(CpuDisIntMaxTm_val);
	msg[len++] = (uint8_t)(CpuDisIntMaxTm_val>>8);
	msg[len++] = (uint8_t)(CpuDisIntMaxTm_val>>16);
	msg[len++] = (uint8_t)(CpuDisIntMaxTm_val>>24);
#endif // {EEP_CONTENT_CpuDisIntMaxTm_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_CpuDisIntMaxTm_ITEM_LEN > 1
void DevEep_Get_CpuDisIntMaxTm(EEP_HANDLE eep_handle,uint16_t *CpuDisIntMaxTm_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_CpuDisIntMaxTm_ITEM_LEN; i ++)
		{
			CpuDisIntMaxTm_val[i] = ap_eep->process_eep.CpuDisIntMaxTm[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_CpuDisIntMaxTm(EEP_HANDLE eep_handle,uint16_t const *CpuDisIntMaxTm_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_CpuDisIntMaxTm_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_CpuDisIntMaxTm_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_CpuDisIntMaxTm_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_CpuDisIntMaxTm_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_CpuDisIntMaxTm_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = CpuDisIntMaxTm_val[i];
#elif EEP_CONTENT_CpuDisIntMaxTm_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(CpuDisIntMaxTm_val[i]);
		msg[len++] = (uint8_t)(CpuDisIntMaxTm_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(CpuDisIntMaxTm_val[i]);
		msg[len++] = (uint8_t)(CpuDisIntMaxTm_val[i]>>8);
		msg[len++] = (uint8_t)(CpuDisIntMaxTm_val[i]>>16);
		msg[len++] = (uint8_t)(CpuDisIntMaxTm_val[i]>>24);
#endif // {EEP_CONTENT_CpuDisIntMaxTm_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_CpuDisIntMaxTm_ITEM_LEN}
void DevEep_Inject_ChgCalbk_CpuDisIntMaxTm(EEP_HANDLE eep_handle,CpuDisIntMaxTm_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_CpuDisIntMaxTm_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_CpuDisIntMaxTm_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_CpuDisIntMaxTm(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		CpuDisIntMaxTm_ChgCallBk CpuDisIntMaxTm_callback =  (CpuDisIntMaxTm_ChgCallBk)callbk;
		CpuDisIntMaxTm_callback(ap,ap->process_eep.CpuDisIntMaxTm);
	}
}

// api define for CpuDisIntTooLongCnt /////////////////////////////////////////////////////////
#if EEP_CONTENT_CpuDisIntTooLongCnt_ITEM_LEN == 1
uint16_t DevEep_Get_CpuDisIntTooLongCnt(EEP_HANDLE eep_handle)
{
	uint16_t CpuDisIntTooLongCnt_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		CpuDisIntTooLongCnt_val = ap_eep->process_eep.CpuDisIntTooLongCnt;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return CpuDisIntTooLongCnt_val;
}
void DevEep_Set_CpuDisIntTooLongCnt(EEP_HANDLE eep_handle,uint16_t const CpuDisIntTooLongCnt_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_CpuDisIntTooLongCnt_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_CpuDisIntTooLongCnt_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_CpuDisIntTooLongCnt_LIST_ID);
#if EEP_CONTENT_CpuDisIntTooLongCnt_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = CpuDisIntTooLongCnt_val;
#elif EEP_CONTENT_CpuDisIntTooLongCnt_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(CpuDisIntTooLongCnt_val);
	msg[len++] = (uint8_t)(CpuDisIntTooLongCnt_val>>8);
#else 
	msg[len++] = (uint8_t)(CpuDisIntTooLongCnt_val);
	msg[len++] = (uint8_t)(CpuDisIntTooLongCnt_val>>8);
	msg[len++] = (uint8_t)(CpuDisIntTooLongCnt_val>>16);
	msg[len++] = (uint8_t)(CpuDisIntTooLongCnt_val>>24);
#endif // {EEP_CONTENT_CpuDisIntTooLongCnt_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_CpuDisIntTooLongCnt_ITEM_LEN > 1
void DevEep_Get_CpuDisIntTooLongCnt(EEP_HANDLE eep_handle,uint16_t *CpuDisIntTooLongCnt_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_CpuDisIntTooLongCnt_ITEM_LEN; i ++)
		{
			CpuDisIntTooLongCnt_val[i] = ap_eep->process_eep.CpuDisIntTooLongCnt[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_CpuDisIntTooLongCnt(EEP_HANDLE eep_handle,uint16_t const *CpuDisIntTooLongCnt_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_CpuDisIntTooLongCnt_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_CpuDisIntTooLongCnt_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_CpuDisIntTooLongCnt_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_CpuDisIntTooLongCnt_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_CpuDisIntTooLongCnt_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = CpuDisIntTooLongCnt_val[i];
#elif EEP_CONTENT_CpuDisIntTooLongCnt_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(CpuDisIntTooLongCnt_val[i]);
		msg[len++] = (uint8_t)(CpuDisIntTooLongCnt_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(CpuDisIntTooLongCnt_val[i]);
		msg[len++] = (uint8_t)(CpuDisIntTooLongCnt_val[i]>>8);
		msg[len++] = (uint8_t)(CpuDisIntTooLongCnt_val[i]>>16);
		msg[len++] = (uint8_t)(CpuDisIntTooLongCnt_val[i]>>24);
#endif // {EEP_CONTENT_CpuDisIntTooLongCnt_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_CpuDisIntTooLongCnt_ITEM_LEN}
void DevEep_Inject_ChgCalbk_CpuDisIntTooLongCnt(EEP_HANDLE eep_handle,CpuDisIntTooLongCnt_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_CpuDisIntTooLongCnt_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_CpuDisIntTooLongCnt_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_CpuDisIntTooLongCnt(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		CpuDisIntTooLongCnt_ChgCallBk CpuDisIntTooLongCnt_callback =  (CpuDisIntTooLongCnt_ChgCallBk)callbk;
		CpuDisIntTooLongCnt_callback(ap,ap->process_eep.CpuDisIntTooLongCnt);
	}
}

// api define for SocResetReason /////////////////////////////////////////////////////////
#if EEP_CONTENT_SocResetReason_ITEM_LEN == 1
uint8_t DevEep_Get_SocResetReason(EEP_HANDLE eep_handle)
{
	uint8_t SocResetReason_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		SocResetReason_val = ap_eep->process_eep.SocResetReason;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return SocResetReason_val;
}
void DevEep_Set_SocResetReason(EEP_HANDLE eep_handle,uint8_t const SocResetReason_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_SocResetReason_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_SocResetReason_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_SocResetReason_LIST_ID);
#if EEP_CONTENT_SocResetReason_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = SocResetReason_val;
#elif EEP_CONTENT_SocResetReason_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(SocResetReason_val);
	msg[len++] = (uint8_t)(SocResetReason_val>>8);
#else 
	msg[len++] = (uint8_t)(SocResetReason_val);
	msg[len++] = (uint8_t)(SocResetReason_val>>8);
	msg[len++] = (uint8_t)(SocResetReason_val>>16);
	msg[len++] = (uint8_t)(SocResetReason_val>>24);
#endif // {EEP_CONTENT_SocResetReason_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_SocResetReason_ITEM_LEN > 1
void DevEep_Get_SocResetReason(EEP_HANDLE eep_handle,uint8_t *SocResetReason_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_SocResetReason_ITEM_LEN; i ++)
		{
			SocResetReason_val[i] = ap_eep->process_eep.SocResetReason[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_SocResetReason(EEP_HANDLE eep_handle,uint8_t const *SocResetReason_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_SocResetReason_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_SocResetReason_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_SocResetReason_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_SocResetReason_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_SocResetReason_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = SocResetReason_val[i];
#elif EEP_CONTENT_SocResetReason_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(SocResetReason_val[i]);
		msg[len++] = (uint8_t)(SocResetReason_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(SocResetReason_val[i]);
		msg[len++] = (uint8_t)(SocResetReason_val[i]>>8);
		msg[len++] = (uint8_t)(SocResetReason_val[i]>>16);
		msg[len++] = (uint8_t)(SocResetReason_val[i]>>24);
#endif // {EEP_CONTENT_SocResetReason_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_SocResetReason_ITEM_LEN}
void DevEep_Inject_ChgCalbk_SocResetReason(EEP_HANDLE eep_handle,SocResetReason_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_SocResetReason_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_SocResetReason_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_SocResetReason(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		SocResetReason_ChgCallBk SocResetReason_callback =  (SocResetReason_ChgCallBk)callbk;
		SocResetReason_callback(ap,ap->process_eep.SocResetReason);
	}
}


//<<SIG_TX_FUNCTION_DEFINE>>

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static EEP_LIST_INFO const eep_list_info[EEP_CONTENT_LIST_NUMBER_MAX]=
{
 {EEP_CONTENT_Manuf_Visteon_Part_Number_LIST_ID,EEP_CONTENT_Manuf_Visteon_Part_Number_ITEM_BYTE_LEN,offsetof(EEPROM_ST,Manuf_Visteon_Part_Number),DevEep_InvokeCalbk_Manuf_Visteon_Part_Number},
 {EEP_CONTENT_Manuf_EquippedPCB_VPN_Sub_LIST_ID,EEP_CONTENT_Manuf_EquippedPCB_VPN_Sub_ITEM_BYTE_LEN,offsetof(EEPROM_ST,Manuf_EquippedPCB_VPN_Sub),DevEep_InvokeCalbk_Manuf_EquippedPCB_VPN_Sub},
 {EEP_CONTENT_Manuf_EquippedPCB_VPN_Main_LIST_ID,EEP_CONTENT_Manuf_EquippedPCB_VPN_Main_ITEM_BYTE_LEN,offsetof(EEPROM_ST,Manuf_EquippedPCB_VPN_Main),DevEep_InvokeCalbk_Manuf_EquippedPCB_VPN_Main},
 {EEP_CONTENT_Manuf_Product_Serial_Number_LIST_ID,EEP_CONTENT_Manuf_Product_Serial_Number_ITEM_BYTE_LEN,offsetof(EEPROM_ST,Manuf_Product_Serial_Number),DevEep_InvokeCalbk_Manuf_Product_Serial_Number},
 {EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Sub_LIST_ID,EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Sub_ITEM_BYTE_LEN,offsetof(EEPROM_ST,Manuf_EquippedPCB_Serial_Number_Sub),DevEep_InvokeCalbk_Manuf_EquippedPCB_Serial_Number_Sub},
 {EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Main_LIST_ID,EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Main_ITEM_BYTE_LEN,offsetof(EEPROM_ST,Manuf_EquippedPCB_Serial_Number_Main),DevEep_InvokeCalbk_Manuf_EquippedPCB_Serial_Number_Main},
 {EEP_CONTENT_NVM_Revision_LIST_ID,EEP_CONTENT_NVM_Revision_ITEM_BYTE_LEN,offsetof(EEPROM_ST,NVM_Revision),DevEep_InvokeCalbk_NVM_Revision},
 {EEP_CONTENT_Manuf_SMD_Date1_LIST_ID,EEP_CONTENT_Manuf_SMD_Date1_ITEM_BYTE_LEN,offsetof(EEPROM_ST,Manuf_SMD_Date1),DevEep_InvokeCalbk_Manuf_SMD_Date1},
 {EEP_CONTENT_Manuf_SMD_Date2_LIST_ID,EEP_CONTENT_Manuf_SMD_Date2_ITEM_BYTE_LEN,offsetof(EEPROM_ST,Manuf_SMD_Date2),DevEep_InvokeCalbk_Manuf_SMD_Date2},
 {EEP_CONTENT_Manuf_Assembly_Date_LIST_ID,EEP_CONTENT_Manuf_Assembly_Date_ITEM_BYTE_LEN,offsetof(EEPROM_ST,Manuf_Assembly_Date),DevEep_InvokeCalbk_Manuf_Assembly_Date},
 {EEP_CONTENT_Traceability_Data_LIST_ID,EEP_CONTENT_Traceability_Data_ITEM_BYTE_LEN,offsetof(EEPROM_ST,Traceability_Data),DevEep_InvokeCalbk_Traceability_Data},
 {EEP_CONTENT_VehicleManufacturerSparePartNumber_LIST_ID,EEP_CONTENT_VehicleManufacturerSparePartNumber_ITEM_BYTE_LEN,offsetof(EEPROM_ST,VehicleManufacturerSparePartNumber),DevEep_InvokeCalbk_VehicleManufacturerSparePartNumber},
 {EEP_CONTENT_VehicleManufacturerSparePartNumber_Assembly_LIST_ID,EEP_CONTENT_VehicleManufacturerSparePartNumber_Assembly_ITEM_BYTE_LEN,offsetof(EEPROM_ST,VehicleManufacturerSparePartNumber_Assembly),DevEep_InvokeCalbk_VehicleManufacturerSparePartNumber_Assembly},
 {EEP_CONTENT_Operational_Reference_LIST_ID,EEP_CONTENT_Operational_Reference_ITEM_BYTE_LEN,offsetof(EEPROM_ST,Operational_Reference),DevEep_InvokeCalbk_Operational_Reference},
 {EEP_CONTENT_Supplier_Number_LIST_ID,EEP_CONTENT_Supplier_Number_ITEM_BYTE_LEN,offsetof(EEPROM_ST,Supplier_Number),DevEep_InvokeCalbk_Supplier_Number},
 {EEP_CONTENT_ECU_Serial_Number_LIST_ID,EEP_CONTENT_ECU_Serial_Number_ITEM_BYTE_LEN,offsetof(EEPROM_ST,ECU_Serial_Number),DevEep_InvokeCalbk_ECU_Serial_Number},
 {EEP_CONTENT_Manufacturing_Identification_Code_LIST_ID,EEP_CONTENT_Manufacturing_Identification_Code_ITEM_BYTE_LEN,offsetof(EEPROM_ST,Manufacturing_Identification_Code),DevEep_InvokeCalbk_Manufacturing_Identification_Code},
 {EEP_CONTENT_VDIAG_LIST_ID,EEP_CONTENT_VDIAG_ITEM_BYTE_LEN,offsetof(EEPROM_ST,VDIAG),DevEep_InvokeCalbk_VDIAG},
 {EEP_CONTENT_Config_EQ1_LIST_ID,EEP_CONTENT_Config_EQ1_ITEM_BYTE_LEN,offsetof(EEPROM_ST,Config_EQ1),DevEep_InvokeCalbk_Config_EQ1},
 {EEP_CONTENT_Vehicle_Type_LIST_ID,EEP_CONTENT_Vehicle_Type_ITEM_BYTE_LEN,offsetof(EEPROM_ST,Vehicle_Type),DevEep_InvokeCalbk_Vehicle_Type},
 {EEP_CONTENT_UUID_LIST_ID,EEP_CONTENT_UUID_ITEM_BYTE_LEN,offsetof(EEPROM_ST,UUID),DevEep_InvokeCalbk_UUID},
 {EEP_CONTENT_NAVI_ID_LIST_ID,EEP_CONTENT_NAVI_ID_ITEM_BYTE_LEN,offsetof(EEPROM_ST,NAVI_ID),DevEep_InvokeCalbk_NAVI_ID},
 {EEP_CONTENT_DA_ID_LIST_ID,EEP_CONTENT_DA_ID_ITEM_BYTE_LEN,offsetof(EEPROM_ST,DA_ID),DevEep_InvokeCalbk_DA_ID},
 {EEP_CONTENT_DAMainBoardHwVer_LIST_ID,EEP_CONTENT_DAMainBoardHwVer_ITEM_BYTE_LEN,offsetof(EEPROM_ST,DAMainBoardHwVer),DevEep_InvokeCalbk_DAMainBoardHwVer},
 {EEP_CONTENT_SW_Version_Date_Format_LIST_ID,EEP_CONTENT_SW_Version_Date_Format_ITEM_BYTE_LEN,offsetof(EEPROM_ST,SW_Version_Date_Format),DevEep_InvokeCalbk_SW_Version_Date_Format},
 {EEP_CONTENT_SW_Edition_Version_LIST_ID,EEP_CONTENT_SW_Edition_Version_ITEM_BYTE_LEN,offsetof(EEPROM_ST,SW_Edition_Version),DevEep_InvokeCalbk_SW_Edition_Version},
 {EEP_CONTENT_VehicleManufacturerSparePartNumber_Nissan_LIST_ID,EEP_CONTENT_VehicleManufacturerSparePartNumber_Nissan_ITEM_BYTE_LEN,offsetof(EEPROM_ST,VehicleManufacturerSparePartNumber_Nissan),DevEep_InvokeCalbk_VehicleManufacturerSparePartNumber_Nissan},
 {EEP_CONTENT_VisteonProductPartNumber_LIST_ID,EEP_CONTENT_VisteonProductPartNumber_ITEM_BYTE_LEN,offsetof(EEPROM_ST,VisteonProductPartNumber),DevEep_InvokeCalbk_VisteonProductPartNumber},
 {EEP_CONTENT_EquippedPCBVisteonPartNumMainBorad_LIST_ID,EEP_CONTENT_EquippedPCBVisteonPartNumMainBorad_ITEM_BYTE_LEN,offsetof(EEPROM_ST,EquippedPCBVisteonPartNumMainBorad),DevEep_InvokeCalbk_EquippedPCBVisteonPartNumMainBorad},
 {EEP_CONTENT_EquippedPCBVisteonPartNumSubBorad_LIST_ID,EEP_CONTENT_EquippedPCBVisteonPartNumSubBorad_ITEM_BYTE_LEN,offsetof(EEPROM_ST,EquippedPCBVisteonPartNumSubBorad),DevEep_InvokeCalbk_EquippedPCBVisteonPartNumSubBorad},
 {EEP_CONTENT_DAUniqueID_LIST_ID,EEP_CONTENT_DAUniqueID_ITEM_BYTE_LEN,offsetof(EEPROM_ST,DAUniqueID),DevEep_InvokeCalbk_DAUniqueID},
 {EEP_CONTENT_ProductSerialNum_LIST_ID,EEP_CONTENT_ProductSerialNum_ITEM_BYTE_LEN,offsetof(EEPROM_ST,ProductSerialNum),DevEep_InvokeCalbk_ProductSerialNum},
 {EEP_CONTENT_ProductSerialNumMainBoard_LIST_ID,EEP_CONTENT_ProductSerialNumMainBoard_ITEM_BYTE_LEN,offsetof(EEPROM_ST,ProductSerialNumMainBoard),DevEep_InvokeCalbk_ProductSerialNumMainBoard},
 {EEP_CONTENT_ProductSerialNumSubBoard_LIST_ID,EEP_CONTENT_ProductSerialNumSubBoard_ITEM_BYTE_LEN,offsetof(EEPROM_ST,ProductSerialNumSubBoard),DevEep_InvokeCalbk_ProductSerialNumSubBoard},
 {EEP_CONTENT_SMDManufacturingDate_LIST_ID,EEP_CONTENT_SMDManufacturingDate_ITEM_BYTE_LEN,offsetof(EEPROM_ST,SMDManufacturingDate),DevEep_InvokeCalbk_SMDManufacturingDate},
 {EEP_CONTENT_AssemblyManufacturingDate_LIST_ID,EEP_CONTENT_AssemblyManufacturingDate_ITEM_BYTE_LEN,offsetof(EEPROM_ST,AssemblyManufacturingDate),DevEep_InvokeCalbk_AssemblyManufacturingDate},
 {EEP_CONTENT_TraceabilityBytes_LIST_ID,EEP_CONTENT_TraceabilityBytes_ITEM_BYTE_LEN,offsetof(EEPROM_ST,TraceabilityBytes),DevEep_InvokeCalbk_TraceabilityBytes},
 {EEP_CONTENT_SMDPlantNum_LIST_ID,EEP_CONTENT_SMDPlantNum_ITEM_BYTE_LEN,offsetof(EEPROM_ST,SMDPlantNum),DevEep_InvokeCalbk_SMDPlantNum},
 {EEP_CONTENT_AssemblyPlantNum_LIST_ID,EEP_CONTENT_AssemblyPlantNum_ITEM_BYTE_LEN,offsetof(EEPROM_ST,AssemblyPlantNum),DevEep_InvokeCalbk_AssemblyPlantNum},
 {EEP_CONTENT_DEM_EVENT_CAN_COM_DATA_LIST_ID,EEP_CONTENT_DEM_EVENT_CAN_COM_DATA_ITEM_BYTE_LEN,offsetof(EEPROM_ST,DEM_EVENT_CAN_COM_DATA),DevEep_InvokeCalbk_DEM_EVENT_CAN_COM_DATA},
 {EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_LIST_ID,EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_BYTE_LEN,offsetof(EEPROM_ST,DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA),DevEep_InvokeCalbk_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA},
 {EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_LIST_ID,EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_BYTE_LEN,offsetof(EEPROM_ST,DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA),DevEep_InvokeCalbk_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA},
 {EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_LIST_ID,EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_BYTE_LEN,offsetof(EEPROM_ST,DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA),DevEep_InvokeCalbk_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA},
 {EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_LIST_ID,EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_BYTE_LEN,offsetof(EEPROM_ST,DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA),DevEep_InvokeCalbk_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA},
 {EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_LIST_ID,EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_BYTE_LEN,offsetof(EEPROM_ST,DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA),DevEep_InvokeCalbk_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA},
 {EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_LIST_ID,EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_BYTE_LEN,offsetof(EEPROM_ST,DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA),DevEep_InvokeCalbk_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA},
 {EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_LIST_ID,EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_BYTE_LEN,offsetof(EEPROM_ST,DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA),DevEep_InvokeCalbk_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA},
 {EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_LIST_ID,EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_BYTE_LEN,offsetof(EEPROM_ST,DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA),DevEep_InvokeCalbk_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA},
 {EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_LIST_ID,EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_ITEM_BYTE_LEN,offsetof(EEPROM_ST,DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA),DevEep_InvokeCalbk_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA},
 {EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_LIST_ID,EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_ITEM_BYTE_LEN,offsetof(EEPROM_ST,DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA),DevEep_InvokeCalbk_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA},
 {EEP_CONTENT_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_LIST_ID,EEP_CONTENT_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_ITEM_BYTE_LEN,offsetof(EEPROM_ST,DEM_EVENT_HVAC_PANEL_CONNECTION_DATA),DevEep_InvokeCalbk_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA},
 {EEP_CONTENT_TachoUpValue_LIST_ID,EEP_CONTENT_TachoUpValue_ITEM_BYTE_LEN,offsetof(EEPROM_ST,TachoUpValue),DevEep_InvokeCalbk_TachoUpValue},
 {EEP_CONTENT_TachoLowValue_LIST_ID,EEP_CONTENT_TachoLowValue_ITEM_BYTE_LEN,offsetof(EEPROM_ST,TachoLowValue),DevEep_InvokeCalbk_TachoLowValue},
 {EEP_CONTENT_TachoLimitValue_LIST_ID,EEP_CONTENT_TachoLimitValue_ITEM_BYTE_LEN,offsetof(EEPROM_ST,TachoLimitValue),DevEep_InvokeCalbk_TachoLimitValue},
 {EEP_CONTENT_TachoUDeltaLimit_LIST_ID,EEP_CONTENT_TachoUDeltaLimit_ITEM_BYTE_LEN,offsetof(EEPROM_ST,TachoUDeltaLimit),DevEep_InvokeCalbk_TachoUDeltaLimit},
 {EEP_CONTENT_TachoLDeltaLimit_LIST_ID,EEP_CONTENT_TachoLDeltaLimit_ITEM_BYTE_LEN,offsetof(EEPROM_ST,TachoLDeltaLimit),DevEep_InvokeCalbk_TachoLDeltaLimit},
 {EEP_CONTENT_TachoUpChangeValue_LIST_ID,EEP_CONTENT_TachoUpChangeValue_ITEM_BYTE_LEN,offsetof(EEPROM_ST,TachoUpChangeValue),DevEep_InvokeCalbk_TachoUpChangeValue},
 {EEP_CONTENT_TachoLowChangeValue_LIST_ID,EEP_CONTENT_TachoLowChangeValue_ITEM_BYTE_LEN,offsetof(EEPROM_ST,TachoLowChangeValue),DevEep_InvokeCalbk_TachoLowChangeValue},
 {EEP_CONTENT_TachoRun_LIST_ID,EEP_CONTENT_TachoRun_ITEM_BYTE_LEN,offsetof(EEPROM_ST,TachoRun),DevEep_InvokeCalbk_TachoRun},
 {EEP_CONTENT_Speed_Rm_1_LIST_ID,EEP_CONTENT_Speed_Rm_1_ITEM_BYTE_LEN,offsetof(EEPROM_ST,Speed_Rm_1),DevEep_InvokeCalbk_Speed_Rm_1},
 {EEP_CONTENT_Speed_Rm_2_LIST_ID,EEP_CONTENT_Speed_Rm_2_ITEM_BYTE_LEN,offsetof(EEPROM_ST,Speed_Rm_2),DevEep_InvokeCalbk_Speed_Rm_2},
 {EEP_CONTENT_Speed_Rc_LIST_ID,EEP_CONTENT_Speed_Rc_ITEM_BYTE_LEN,offsetof(EEPROM_ST,Speed_Rc),DevEep_InvokeCalbk_Speed_Rc},
 {EEP_CONTENT_FuelTelltaleOn_R_LIST_ID,EEP_CONTENT_FuelTelltaleOn_R_ITEM_BYTE_LEN,offsetof(EEPROM_ST,FuelTelltaleOn_R),DevEep_InvokeCalbk_FuelTelltaleOn_R},
 {EEP_CONTENT_FuelTelltaleOff_R_LIST_ID,EEP_CONTENT_FuelTelltaleOff_R_ITEM_BYTE_LEN,offsetof(EEPROM_ST,FuelTelltaleOff_R),DevEep_InvokeCalbk_FuelTelltaleOff_R},
 {EEP_CONTENT_FuelWarningOnPoint_R_LIST_ID,EEP_CONTENT_FuelWarningOnPoint_R_ITEM_BYTE_LEN,offsetof(EEPROM_ST,FuelWarningOnPoint_R),DevEep_InvokeCalbk_FuelWarningOnPoint_R},
 {EEP_CONTENT_Fuel_R_Open_LIST_ID,EEP_CONTENT_Fuel_R_Open_ITEM_BYTE_LEN,offsetof(EEPROM_ST,Fuel_R_Open),DevEep_InvokeCalbk_Fuel_R_Open},
 {EEP_CONTENT_Fuel_R_Short_LIST_ID,EEP_CONTENT_Fuel_R_Short_ITEM_BYTE_LEN,offsetof(EEPROM_ST,Fuel_R_Short),DevEep_InvokeCalbk_Fuel_R_Short},
 {EEP_CONTENT_PkbWarningJudgeV1_LIST_ID,EEP_CONTENT_PkbWarningJudgeV1_ITEM_BYTE_LEN,offsetof(EEPROM_ST,PkbWarningJudgeV1),DevEep_InvokeCalbk_PkbWarningJudgeV1},
 {EEP_CONTENT_PkbWarningJudgeV2_LIST_ID,EEP_CONTENT_PkbWarningJudgeV2_ITEM_BYTE_LEN,offsetof(EEPROM_ST,PkbWarningJudgeV2),DevEep_InvokeCalbk_PkbWarningJudgeV2},
 {EEP_CONTENT_AudioEepTest_LIST_ID,EEP_CONTENT_AudioEepTest_ITEM_BYTE_LEN,offsetof(EEPROM_ST,AudioEepTest),DevEep_InvokeCalbk_AudioEepTest},
 {EEP_CONTENT_DspKeyCodeOnOff_LIST_ID,EEP_CONTENT_DspKeyCodeOnOff_ITEM_BYTE_LEN,offsetof(EEPROM_ST,DspKeyCodeOnOff),DevEep_InvokeCalbk_DspKeyCodeOnOff},
 {EEP_CONTENT_DspKeyCode_LIST_ID,EEP_CONTENT_DspKeyCode_ITEM_BYTE_LEN,offsetof(EEPROM_ST,DspKeyCode),DevEep_InvokeCalbk_DspKeyCode},
 {EEP_CONTENT_BatState_ChgDly_LIST_ID,EEP_CONTENT_BatState_ChgDly_ITEM_BYTE_LEN,offsetof(EEPROM_ST,BatState_ChgDly),DevEep_InvokeCalbk_BatState_ChgDly},
 {EEP_CONTENT_BatVolVeryHigh_Hysteresis_H_LIST_ID,EEP_CONTENT_BatVolVeryHigh_Hysteresis_H_ITEM_BYTE_LEN,offsetof(EEPROM_ST,BatVolVeryHigh_Hysteresis_H),DevEep_InvokeCalbk_BatVolVeryHigh_Hysteresis_H},
 {EEP_CONTENT_BatVolVeryHigh_Hysteresis_L_LIST_ID,EEP_CONTENT_BatVolVeryHigh_Hysteresis_L_ITEM_BYTE_LEN,offsetof(EEPROM_ST,BatVolVeryHigh_Hysteresis_L),DevEep_InvokeCalbk_BatVolVeryHigh_Hysteresis_L},
 {EEP_CONTENT_BatVolHigh_Hysteresis_H_LIST_ID,EEP_CONTENT_BatVolHigh_Hysteresis_H_ITEM_BYTE_LEN,offsetof(EEPROM_ST,BatVolHigh_Hysteresis_H),DevEep_InvokeCalbk_BatVolHigh_Hysteresis_H},
 {EEP_CONTENT_BatVolHigh_Hysteresis_L_LIST_ID,EEP_CONTENT_BatVolHigh_Hysteresis_L_ITEM_BYTE_LEN,offsetof(EEPROM_ST,BatVolHigh_Hysteresis_L),DevEep_InvokeCalbk_BatVolHigh_Hysteresis_L},
 {EEP_CONTENT_BatVolLow_Hysteresis_H_LIST_ID,EEP_CONTENT_BatVolLow_Hysteresis_H_ITEM_BYTE_LEN,offsetof(EEPROM_ST,BatVolLow_Hysteresis_H),DevEep_InvokeCalbk_BatVolLow_Hysteresis_H},
 {EEP_CONTENT_BatVolLow_Hysteresis_L_LIST_ID,EEP_CONTENT_BatVolLow_Hysteresis_L_ITEM_BYTE_LEN,offsetof(EEPROM_ST,BatVolLow_Hysteresis_L),DevEep_InvokeCalbk_BatVolLow_Hysteresis_L},
 {EEP_CONTENT_BatVolVeryLow_Hysteresis_H_LIST_ID,EEP_CONTENT_BatVolVeryLow_Hysteresis_H_ITEM_BYTE_LEN,offsetof(EEPROM_ST,BatVolVeryLow_Hysteresis_H),DevEep_InvokeCalbk_BatVolVeryLow_Hysteresis_H},
 {EEP_CONTENT_BatVolVeryLow_Hysteresis_L_LIST_ID,EEP_CONTENT_BatVolVeryLow_Hysteresis_L_ITEM_BYTE_LEN,offsetof(EEPROM_ST,BatVolVeryLow_Hysteresis_L),DevEep_InvokeCalbk_BatVolVeryLow_Hysteresis_L},
 {EEP_CONTENT_TempState_ChgDly_LIST_ID,EEP_CONTENT_TempState_ChgDly_ITEM_BYTE_LEN,offsetof(EEPROM_ST,TempState_ChgDly),DevEep_InvokeCalbk_TempState_ChgDly},
 {EEP_CONTENT_TempDegC_Low_Hysteresis_L_LIST_ID,EEP_CONTENT_TempDegC_Low_Hysteresis_L_ITEM_BYTE_LEN,offsetof(EEPROM_ST,TempDegC_Low_Hysteresis_L),DevEep_InvokeCalbk_TempDegC_Low_Hysteresis_L},
 {EEP_CONTENT_TempDegC_Low_Hysteresis_H_LIST_ID,EEP_CONTENT_TempDegC_Low_Hysteresis_H_ITEM_BYTE_LEN,offsetof(EEPROM_ST,TempDegC_Low_Hysteresis_H),DevEep_InvokeCalbk_TempDegC_Low_Hysteresis_H},
 {EEP_CONTENT_TempDegC_High_Hysteresis_L_LIST_ID,EEP_CONTENT_TempDegC_High_Hysteresis_L_ITEM_BYTE_LEN,offsetof(EEPROM_ST,TempDegC_High_Hysteresis_L),DevEep_InvokeCalbk_TempDegC_High_Hysteresis_L},
 {EEP_CONTENT_TempDegC_High_Hysteresis_H_LIST_ID,EEP_CONTENT_TempDegC_High_Hysteresis_H_ITEM_BYTE_LEN,offsetof(EEPROM_ST,TempDegC_High_Hysteresis_H),DevEep_InvokeCalbk_TempDegC_High_Hysteresis_H},
 {EEP_CONTENT_AmpHighTempProction_LIST_ID,EEP_CONTENT_AmpHighTempProction_ITEM_BYTE_LEN,offsetof(EEPROM_ST,AmpHighTempProction),DevEep_InvokeCalbk_AmpHighTempProction},
 {EEP_CONTENT_CanNm_DA_S1_Delay_ms_LIST_ID,EEP_CONTENT_CanNm_DA_S1_Delay_ms_ITEM_BYTE_LEN,offsetof(EEPROM_ST,CanNm_DA_S1_Delay_ms),DevEep_InvokeCalbk_CanNm_DA_S1_Delay_ms},
 {EEP_CONTENT_Vin_LIST_ID,EEP_CONTENT_Vin_ITEM_BYTE_LEN,offsetof(EEPROM_ST,Vin),DevEep_InvokeCalbk_Vin},
 {EEP_CONTENT_AutoSyncTimeWithGps_LIST_ID,EEP_CONTENT_AutoSyncTimeWithGps_ITEM_BYTE_LEN,offsetof(EEPROM_ST,AutoSyncTimeWithGps),DevEep_InvokeCalbk_AutoSyncTimeWithGps},
 {EEP_CONTENT_ScreenBackLightValOnDay_LIST_ID,EEP_CONTENT_ScreenBackLightValOnDay_ITEM_BYTE_LEN,offsetof(EEPROM_ST,ScreenBackLightValOnDay),DevEep_InvokeCalbk_ScreenBackLightValOnDay},
 {EEP_CONTENT_ScreenBackLightValOnNight_LIST_ID,EEP_CONTENT_ScreenBackLightValOnNight_ITEM_BYTE_LEN,offsetof(EEPROM_ST,ScreenBackLightValOnNight),DevEep_InvokeCalbk_ScreenBackLightValOnNight},
 {EEP_CONTENT_HistoryAverFuelCons_LIST_ID,EEP_CONTENT_HistoryAverFuelCons_ITEM_BYTE_LEN,offsetof(EEPROM_ST,HistoryAverFuelCons),DevEep_InvokeCalbk_HistoryAverFuelCons},
 {EEP_CONTENT_FuelResistance_LIST_ID,EEP_CONTENT_FuelResistance_ITEM_BYTE_LEN,offsetof(EEPROM_ST,FuelResistance),DevEep_InvokeCalbk_FuelResistance},
 {EEP_CONTENT_dtc_example_LIST_ID,EEP_CONTENT_dtc_example_ITEM_BYTE_LEN,offsetof(EEPROM_ST,dtc_example),DevEep_InvokeCalbk_dtc_example},
 {EEP_CONTENT_TotalOdo_LIST_ID,EEP_CONTENT_TotalOdo_ITEM_BYTE_LEN,offsetof(EEPROM_ST,TotalOdo),DevEep_InvokeCalbk_TotalOdo},
 {EEP_CONTENT_TotalTime_LIST_ID,EEP_CONTENT_TotalTime_ITEM_BYTE_LEN,offsetof(EEPROM_ST,TotalTime),DevEep_InvokeCalbk_TotalTime},
 {EEP_CONTENT_TravelTime_LIST_ID,EEP_CONTENT_TravelTime_ITEM_BYTE_LEN,offsetof(EEPROM_ST,TravelTime),DevEep_InvokeCalbk_TravelTime},
 {EEP_CONTENT_TravelOdo_LIST_ID,EEP_CONTENT_TravelOdo_ITEM_BYTE_LEN,offsetof(EEPROM_ST,TravelOdo),DevEep_InvokeCalbk_TravelOdo},
 {EEP_CONTENT_TripAMeter_LIST_ID,EEP_CONTENT_TripAMeter_ITEM_BYTE_LEN,offsetof(EEPROM_ST,TripAMeter),DevEep_InvokeCalbk_TripAMeter},
 {EEP_CONTENT_TripATime_LIST_ID,EEP_CONTENT_TripATime_ITEM_BYTE_LEN,offsetof(EEPROM_ST,TripATime),DevEep_InvokeCalbk_TripATime},
 {EEP_CONTENT_TripBMeter_LIST_ID,EEP_CONTENT_TripBMeter_ITEM_BYTE_LEN,offsetof(EEPROM_ST,TripBMeter),DevEep_InvokeCalbk_TripBMeter},
 {EEP_CONTENT_TripBTime_LIST_ID,EEP_CONTENT_TripBTime_ITEM_BYTE_LEN,offsetof(EEPROM_ST,TripBTime),DevEep_InvokeCalbk_TripBTime},
 {EEP_CONTENT_CruiseDistance_LIST_ID,EEP_CONTENT_CruiseDistance_ITEM_BYTE_LEN,offsetof(EEPROM_ST,CruiseDistance),DevEep_InvokeCalbk_CruiseDistance},
 {EEP_CONTENT_VipSwdlShareMem_LIST_ID,EEP_CONTENT_VipSwdlShareMem_ITEM_BYTE_LEN,offsetof(EEPROM_ST,VipSwdlShareMem),DevEep_InvokeCalbk_VipSwdlShareMem},
 {EEP_CONTENT_TotalImageSFailCnt_LIST_ID,EEP_CONTENT_TotalImageSFailCnt_ITEM_BYTE_LEN,offsetof(EEPROM_ST,TotalImageSFailCnt),DevEep_InvokeCalbk_TotalImageSFailCnt},
 {EEP_CONTENT_trace_phy_en_LIST_ID,EEP_CONTENT_trace_phy_en_ITEM_BYTE_LEN,offsetof(EEPROM_ST,trace_phy_en),DevEep_InvokeCalbk_trace_phy_en},
 {EEP_CONTENT_trace_phy_LIST_ID,EEP_CONTENT_trace_phy_ITEM_BYTE_LEN,offsetof(EEPROM_ST,trace_phy),DevEep_InvokeCalbk_trace_phy},
 {EEP_CONTENT_VipLogModLvl_LIST_ID,EEP_CONTENT_VipLogModLvl_ITEM_BYTE_LEN,offsetof(EEPROM_ST,VipLogModLvl),DevEep_InvokeCalbk_VipLogModLvl},
 {EEP_CONTENT_TraceModuleLvlCrc_LIST_ID,EEP_CONTENT_TraceModuleLvlCrc_ITEM_BYTE_LEN,offsetof(EEPROM_ST,TraceModuleLvlCrc),DevEep_InvokeCalbk_TraceModuleLvlCrc},
 {EEP_CONTENT_TotalReset_Cnt_LIST_ID,EEP_CONTENT_TotalReset_Cnt_ITEM_BYTE_LEN,offsetof(EEPROM_ST,TotalReset_Cnt),DevEep_InvokeCalbk_TotalReset_Cnt},
 {EEP_CONTENT_DspPOR_cnt_LIST_ID,EEP_CONTENT_DspPOR_cnt_ITEM_BYTE_LEN,offsetof(EEPROM_ST,DspPOR_cnt),DevEep_InvokeCalbk_DspPOR_cnt},
 {EEP_CONTENT_AudioCmdExecMaxTm_LIST_ID,EEP_CONTENT_AudioCmdExecMaxTm_ITEM_BYTE_LEN,offsetof(EEPROM_ST,AudioCmdExecMaxTm),DevEep_InvokeCalbk_AudioCmdExecMaxTm},
 {EEP_CONTENT_RadioCmdExecMaxTm_LIST_ID,EEP_CONTENT_RadioCmdExecMaxTm_ITEM_BYTE_LEN,offsetof(EEPROM_ST,RadioCmdExecMaxTm),DevEep_InvokeCalbk_RadioCmdExecMaxTm},
 {EEP_CONTENT_CpuDisIntMaxNest_LIST_ID,EEP_CONTENT_CpuDisIntMaxNest_ITEM_BYTE_LEN,offsetof(EEPROM_ST,CpuDisIntMaxNest),DevEep_InvokeCalbk_CpuDisIntMaxNest},
 {EEP_CONTENT_CpuDisIntMaxTm_LIST_ID,EEP_CONTENT_CpuDisIntMaxTm_ITEM_BYTE_LEN,offsetof(EEPROM_ST,CpuDisIntMaxTm),DevEep_InvokeCalbk_CpuDisIntMaxTm},
 {EEP_CONTENT_CpuDisIntTooLongCnt_LIST_ID,EEP_CONTENT_CpuDisIntTooLongCnt_ITEM_BYTE_LEN,offsetof(EEPROM_ST,CpuDisIntTooLongCnt),DevEep_InvokeCalbk_CpuDisIntTooLongCnt},
 {EEP_CONTENT_SocResetReason_LIST_ID,EEP_CONTENT_SocResetReason_ITEM_BYTE_LEN,offsetof(EEPROM_ST,SocResetReason),DevEep_InvokeCalbk_SocResetReason},
};
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
EEP_HANDLE DevEep_Init(void)
{
	DEV_EEP_AP * ap_rx =(DEV_EEP_AP*)malloc(sizeof(DEV_EEP_AP));

	if ( ap_rx != NULL )
	{
		memset(ap_rx,0,sizeof(DEV_EEP_AP));
		
		(void)sem_init(&(ap_rx->invoke_sem), 0, 0);	
		pthread_mutex_init(&(ap_rx->mutex),NULL);
		
		ap_rx->dev_fd = open("/dev/ipc/eeprom", O_RDWR);
		
		(void)pthread_create(&(ap_rx->read_thread),  NULL,dev_eep_thread_decode, ap_rx);
	}
	
	return ap_rx;
}
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void *dev_eep_thread_decode(void *arg)
{
	DEV_EEP_AP *ap_rx = (DEV_EEP_AP*)arg;
	uint8_t *rx_msg 	 = 	malloc(8192);
	while(1)
	{
		int len = read(ap_rx->dev_fd,rx_msg,4096);
		
		if ( len > 2 )
		{
			if ( rx_msg[0] == 0x83 )
			{	//(vip->imx), transfer all eeprom to imx
				if ( len == sizeof(EEPROM_ST)+1 )
				{
					/* process all eeprom content */
					if ( pthread_mutex_lock(&(ap_rx->mutex)) == 0 )
					{
						for(uint16_t i = 0; i < EEP_CONTENT_LIST_NUMBER_MAX; i++)
						{
							EEP_LIST_INFO const *info = eep_list_info +i;
							uint8_t *src    = rx_msg + 1 + info->offset;
							uint8_t *desc   = (uint8_t*)&ap_rx->current_eep + info->offset;
							for(uint16_t j=0; j < info->list_len; j++)
							{
								if ( desc[j] != src[j] )
								{
									desc[j] = src[j];
									
									ap_rx->calbk_req[i] = 0x01;
									if ( ap_rx->invoke_task == 0x00 )
									{
										ap_rx->invoke_task = 0x01;
										(void)sem_post(&(ap_rx->invoke_sem));
									}									
								}
							}
						}
						memcpy(&ap_rx->current_eep,rx_msg+1,sizeof(EEPROM_ST));
						(void)pthread_mutex_unlock(&(ap_rx->mutex));
					}
				}
			}
			else if ( rx_msg[0] == 0xA0 )
			{	// transfer eeprom to imx by multi eeprom list id
				/* check list and len is ok */
				uint8_t 		cmd_ok 		= 0x01;
				uint16_t  	  	check_rx 	= len-1;
				uint8_t const * cmd 	    = rx_msg+1;
				uint16_t	 	list;
				/* check command ok first*/
				while ( check_rx > 2 && cmd_ok )
				{
					list = 		*cmd++;
					list <<= 	8;
					list += 	*cmd++;
					check_rx  -= 	2;
					if ( list < EEP_CONTENT_LIST_NUMBER_MAX )
					{
						EEP_LIST_INFO const * info = eep_list_info + list; 
						if ( info->list_len <= check_rx )
						{
							check_rx 	-= info->list_len;
							cmd 	+= info->list_len;
						}
						else
						{
							cmd_ok = 0x00;
						}
					}
					else
					{
						cmd_ok = 0x00;
					}
				}
				
				if ( check_rx == 0x00 && cmd_ok )
				{
					if ( pthread_mutex_lock(&(ap_rx->mutex)) == 0 )
					{
						check_rx = len-1;
						cmd 	 = rx_msg+1;
						while ( check_rx > 2  )
						{
							list 		= 	*cmd++;
							list 		<<= 8;
							list 		+= 	*cmd++;
							check_rx  	-= 	2;

							if ( list < EEP_CONTENT_LIST_NUMBER_MAX )
							{
								EEP_LIST_INFO const * info = eep_list_info + list; 
								if ( info->list_len <= check_rx )
								{
									check_rx -= info->list_len;

									uint8_t *desc   = (uint8_t*)&ap_rx->current_eep + info->offset;
									for(uint16_t j=0; j < info->list_len; j++)
									{
										uint8_t val = *cmd++;
										if ( desc[j] != val )
										{
											desc[j] = val;
											ap_rx->calbk_req[list] = 0x01;
											if ( ap_rx->invoke_task == 0x00 )
											{
												ap_rx->invoke_task = 0x01;
												(void)sem_post(&(ap_rx->invoke_sem));
											}									
										}
									}
								}
								else
								{
									check_rx = 0x00;
								}
							}
							else
							{
								check_rx = 0x00;
							}
						}
						(void)pthread_mutex_unlock(&(ap_rx->mutex));
					}
				}
			}
			else
			{}//other command, not support now 
			
			/* check if 0x83 command received: all eeprom content */
			if ( ap_rx->all_eep_get == 0x00 && rx_msg[0] == 0x83 && len == sizeof(EEPROM_ST)+1 )
			{
				ap_rx->all_eep_get = 0x01;
				if ( pthread_mutex_lock(&(ap_rx->mutex)) == 0 )
				{	
					/* set all callback require */
					memset(ap_rx->calbk_req,0x01,EEP_CONTENT_LIST_NUMBER_MAX);
					(void)pthread_mutex_unlock(&(ap_rx->mutex));	
				}
				(void)sem_post(&(ap_rx->invoke_sem));
				(void)pthread_create(&(ap_rx->invoke_thread),NULL,dev_eep_thread_execute,ap_rx);
			}
		}
	}

	return NULL;
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void *dev_eep_thread_execute(void *arg)
{
	DEV_EEP_AP *ap_rx 	= (DEV_EEP_AP*)arg;

	while(1)
	{
		(void)sem_wait(&(ap_rx->invoke_sem));
		

		if ( pthread_mutex_lock(&(ap_rx->mutex)) == 0 )
		{
			ap_rx->invoke_task = 0x00;
			
			for( uint16_t i = 0; i < EEP_CONTENT_LIST_NUMBER_MAX; i++ )
			{
				if ( ap_rx->calbk_req[i] )
				{	/*  copy callback information */
					ap_rx->proc_req[i]   = ap_rx->calbk_req[i];
					ap_rx->calbk_req[i]  = 0x00;
					ap_rx->proc_calbk[i] = ap_rx->callback[i];
					
					EEP_LIST_INFO const * info = eep_list_info + i; 
					uint8_t *src  = (uint8_t*)&ap_rx->current_eep + info->offset;
					uint8_t *desc = (uint8_t*)&ap_rx->process_eep + info->offset;
					memcpy(desc, src, info->list_len);
				}
				else
				{
					ap_rx->proc_req[i]   = 0x00; 
				}
			}
			(void)pthread_mutex_unlock(&(ap_rx->mutex));
			
			for ( uint16_t i = 0; i < EEP_CONTENT_LIST_NUMBER_MAX; i++ )
			{
				if( ap_rx->proc_req[i] )
				{	// invoke callback 
					//invoke_callback_handl[i](ap_rx);
					eep_list_info[i].callback(ap_rx,ap_rx->proc_calbk[i]);
				}
			}
		}
	}
	return NULL;
}
/** >>>>>>>> end of file  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


