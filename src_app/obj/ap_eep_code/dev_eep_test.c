/*******************************************************************************
 * Project         Panada Pluse
 * (c) Copyright   2017
 * Company         Visteon Asia Pacific (Shanghai), inc
 *                 All rights reserved.
 * @file           dev_sig_test.c   
 * @module         device signal 
 * @author         Chao Wang
 * @brief          device handle process
 * @changelog      1. Creat File
********************************************************************************/

#include "stdint.h"
#include "dev_sig.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <dlfcn.h>
#include <fcntl.h>
#include <pthread.h>
#include <sys/ioctl.h>
#include "dev_eep.h"
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

//<<DEV_EEP_CALLBACK_DEFINE>>

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
EEP_HANDLE eep_handle;

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void DevEepTest_Manuf_Visteon_Part_Number(int argc, char **argv)
{
#if EEP_CONTENT_Manuf_Visteon_Part_Number_ITEM_LEN == 1
	#if EEP_CONTENT_Manuf_Visteon_Part_Number_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t Manuf_Visteon_Part_Number_val;
	#elif EEP_CONTENT_Manuf_Visteon_Part_Number_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t Manuf_Visteon_Part_Number_val;
	#else
	uint32_t Manuf_Visteon_Part_Number_val;
	#endif
	Manuf_Visteon_Part_Number_val = atoi(argv[2]);
#else // EEP_CONTENT_Manuf_Visteon_Part_Number_ITEM_LEN > 1
	#if EEP_CONTENT_Manuf_Visteon_Part_Number_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t Manuf_Visteon_Part_Number_val[EEP_CONTENT_Manuf_Visteon_Part_Number_ITEM_LEN];
	#elif EEP_CONTENT_Manuf_Visteon_Part_Number_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t Manuf_Visteon_Part_Number_val[EEP_CONTENT_Manuf_Visteon_Part_Number_ITEM_LEN];
	#else
	uint32_t Manuf_Visteon_Part_Number_val[EEP_CONTENT_Manuf_Visteon_Part_Number_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_Manuf_Visteon_Part_Number_ITEM_LEN; i ++ )
	{
		Manuf_Visteon_Part_Number_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_Manuf_Visteon_Part_Number_ITEM_LEN}	
	
	DevEep_Set_Manuf_Visteon_Part_Number(eep_handle,Manuf_Visteon_Part_Number_val);
}


#if EEP_CONTENT_Manuf_Visteon_Part_Number_ITEM_LEN == 1
#if EEP_CONTENT_Manuf_Visteon_Part_Number_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_Manuf_Visteon_Part_Number_Callback(EEP_HANDLE eep_handle,uint8_t const Manuf_Visteon_Part_Number_val)
#elif EEP_CONTENT_Manuf_Visteon_Part_Number_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_Manuf_Visteon_Part_Number_Callback(EEP_HANDLE eep_handle,uint16_t const Manuf_Visteon_Part_Number_val)
#else
void DevEepTest_Manuf_Visteon_Part_Number_Callback(EEP_HANDLE eep_handle,uint32_t const Manuf_Visteon_Part_Number_val)
#endif	
{
	
	printf("dev eep test: Manuf_Visteon_Part_Number, change to dec=%d,hex=0x%08x\r\n",Manuf_Visteon_Part_Number_val,Manuf_Visteon_Part_Number_val);
}
#else // EEP_CONTENT_Manuf_Visteon_Part_Number_ITEM_LEN > 1
#if EEP_CONTENT_Manuf_Visteon_Part_Number_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_Manuf_Visteon_Part_Number_Callback(EEP_HANDLE eep_handle,uint8_t const * Manuf_Visteon_Part_Number_val)
#elif EEP_CONTENT_Manuf_Visteon_Part_Number_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_Manuf_Visteon_Part_Number_Callback(EEP_HANDLE eep_handle,uint16_t const * Manuf_Visteon_Part_Number_val)
#else
void DevEepTest_Manuf_Visteon_Part_Number_Callback(EEP_HANDLE eep_handle,uint32_t const * Manuf_Visteon_Part_Number_val)
#endif
{
	printf("dev eep test: Manuf_Visteon_Part_Number, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_Manuf_Visteon_Part_Number_ITEM_LEN; i ++)
	{
		printf("%d ", Manuf_Visteon_Part_Number_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_Manuf_Visteon_Part_Number_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_Manuf_Visteon_Part_Number_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", Manuf_Visteon_Part_Number_val[i]);
		#elif EEP_CONTENT_Manuf_Visteon_Part_Number_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", Manuf_Visteon_Part_Number_val[i]);
		#else
		printf("%08x ", Manuf_Visteon_Part_Number_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_Manuf_EquippedPCB_VPN_Sub(int argc, char **argv)
{
#if EEP_CONTENT_Manuf_EquippedPCB_VPN_Sub_ITEM_LEN == 1
	#if EEP_CONTENT_Manuf_EquippedPCB_VPN_Sub_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t Manuf_EquippedPCB_VPN_Sub_val;
	#elif EEP_CONTENT_Manuf_EquippedPCB_VPN_Sub_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t Manuf_EquippedPCB_VPN_Sub_val;
	#else
	uint32_t Manuf_EquippedPCB_VPN_Sub_val;
	#endif
	Manuf_EquippedPCB_VPN_Sub_val = atoi(argv[2]);
#else // EEP_CONTENT_Manuf_EquippedPCB_VPN_Sub_ITEM_LEN > 1
	#if EEP_CONTENT_Manuf_EquippedPCB_VPN_Sub_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t Manuf_EquippedPCB_VPN_Sub_val[EEP_CONTENT_Manuf_EquippedPCB_VPN_Sub_ITEM_LEN];
	#elif EEP_CONTENT_Manuf_EquippedPCB_VPN_Sub_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t Manuf_EquippedPCB_VPN_Sub_val[EEP_CONTENT_Manuf_EquippedPCB_VPN_Sub_ITEM_LEN];
	#else
	uint32_t Manuf_EquippedPCB_VPN_Sub_val[EEP_CONTENT_Manuf_EquippedPCB_VPN_Sub_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_Manuf_EquippedPCB_VPN_Sub_ITEM_LEN; i ++ )
	{
		Manuf_EquippedPCB_VPN_Sub_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_Manuf_EquippedPCB_VPN_Sub_ITEM_LEN}	
	
	DevEep_Set_Manuf_EquippedPCB_VPN_Sub(eep_handle,Manuf_EquippedPCB_VPN_Sub_val);
}


#if EEP_CONTENT_Manuf_EquippedPCB_VPN_Sub_ITEM_LEN == 1
#if EEP_CONTENT_Manuf_EquippedPCB_VPN_Sub_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_Manuf_EquippedPCB_VPN_Sub_Callback(EEP_HANDLE eep_handle,uint8_t const Manuf_EquippedPCB_VPN_Sub_val)
#elif EEP_CONTENT_Manuf_EquippedPCB_VPN_Sub_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_Manuf_EquippedPCB_VPN_Sub_Callback(EEP_HANDLE eep_handle,uint16_t const Manuf_EquippedPCB_VPN_Sub_val)
#else
void DevEepTest_Manuf_EquippedPCB_VPN_Sub_Callback(EEP_HANDLE eep_handle,uint32_t const Manuf_EquippedPCB_VPN_Sub_val)
#endif	
{
	
	printf("dev eep test: Manuf_EquippedPCB_VPN_Sub, change to dec=%d,hex=0x%08x\r\n",Manuf_EquippedPCB_VPN_Sub_val,Manuf_EquippedPCB_VPN_Sub_val);
}
#else // EEP_CONTENT_Manuf_EquippedPCB_VPN_Sub_ITEM_LEN > 1
#if EEP_CONTENT_Manuf_EquippedPCB_VPN_Sub_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_Manuf_EquippedPCB_VPN_Sub_Callback(EEP_HANDLE eep_handle,uint8_t const * Manuf_EquippedPCB_VPN_Sub_val)
#elif EEP_CONTENT_Manuf_EquippedPCB_VPN_Sub_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_Manuf_EquippedPCB_VPN_Sub_Callback(EEP_HANDLE eep_handle,uint16_t const * Manuf_EquippedPCB_VPN_Sub_val)
#else
void DevEepTest_Manuf_EquippedPCB_VPN_Sub_Callback(EEP_HANDLE eep_handle,uint32_t const * Manuf_EquippedPCB_VPN_Sub_val)
#endif
{
	printf("dev eep test: Manuf_EquippedPCB_VPN_Sub, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_Manuf_EquippedPCB_VPN_Sub_ITEM_LEN; i ++)
	{
		printf("%d ", Manuf_EquippedPCB_VPN_Sub_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_Manuf_EquippedPCB_VPN_Sub_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_Manuf_EquippedPCB_VPN_Sub_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", Manuf_EquippedPCB_VPN_Sub_val[i]);
		#elif EEP_CONTENT_Manuf_EquippedPCB_VPN_Sub_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", Manuf_EquippedPCB_VPN_Sub_val[i]);
		#else
		printf("%08x ", Manuf_EquippedPCB_VPN_Sub_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_Manuf_EquippedPCB_VPN_Main(int argc, char **argv)
{
#if EEP_CONTENT_Manuf_EquippedPCB_VPN_Main_ITEM_LEN == 1
	#if EEP_CONTENT_Manuf_EquippedPCB_VPN_Main_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t Manuf_EquippedPCB_VPN_Main_val;
	#elif EEP_CONTENT_Manuf_EquippedPCB_VPN_Main_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t Manuf_EquippedPCB_VPN_Main_val;
	#else
	uint32_t Manuf_EquippedPCB_VPN_Main_val;
	#endif
	Manuf_EquippedPCB_VPN_Main_val = atoi(argv[2]);
#else // EEP_CONTENT_Manuf_EquippedPCB_VPN_Main_ITEM_LEN > 1
	#if EEP_CONTENT_Manuf_EquippedPCB_VPN_Main_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t Manuf_EquippedPCB_VPN_Main_val[EEP_CONTENT_Manuf_EquippedPCB_VPN_Main_ITEM_LEN];
	#elif EEP_CONTENT_Manuf_EquippedPCB_VPN_Main_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t Manuf_EquippedPCB_VPN_Main_val[EEP_CONTENT_Manuf_EquippedPCB_VPN_Main_ITEM_LEN];
	#else
	uint32_t Manuf_EquippedPCB_VPN_Main_val[EEP_CONTENT_Manuf_EquippedPCB_VPN_Main_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_Manuf_EquippedPCB_VPN_Main_ITEM_LEN; i ++ )
	{
		Manuf_EquippedPCB_VPN_Main_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_Manuf_EquippedPCB_VPN_Main_ITEM_LEN}	
	
	DevEep_Set_Manuf_EquippedPCB_VPN_Main(eep_handle,Manuf_EquippedPCB_VPN_Main_val);
}


#if EEP_CONTENT_Manuf_EquippedPCB_VPN_Main_ITEM_LEN == 1
#if EEP_CONTENT_Manuf_EquippedPCB_VPN_Main_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_Manuf_EquippedPCB_VPN_Main_Callback(EEP_HANDLE eep_handle,uint8_t const Manuf_EquippedPCB_VPN_Main_val)
#elif EEP_CONTENT_Manuf_EquippedPCB_VPN_Main_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_Manuf_EquippedPCB_VPN_Main_Callback(EEP_HANDLE eep_handle,uint16_t const Manuf_EquippedPCB_VPN_Main_val)
#else
void DevEepTest_Manuf_EquippedPCB_VPN_Main_Callback(EEP_HANDLE eep_handle,uint32_t const Manuf_EquippedPCB_VPN_Main_val)
#endif	
{
	
	printf("dev eep test: Manuf_EquippedPCB_VPN_Main, change to dec=%d,hex=0x%08x\r\n",Manuf_EquippedPCB_VPN_Main_val,Manuf_EquippedPCB_VPN_Main_val);
}
#else // EEP_CONTENT_Manuf_EquippedPCB_VPN_Main_ITEM_LEN > 1
#if EEP_CONTENT_Manuf_EquippedPCB_VPN_Main_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_Manuf_EquippedPCB_VPN_Main_Callback(EEP_HANDLE eep_handle,uint8_t const * Manuf_EquippedPCB_VPN_Main_val)
#elif EEP_CONTENT_Manuf_EquippedPCB_VPN_Main_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_Manuf_EquippedPCB_VPN_Main_Callback(EEP_HANDLE eep_handle,uint16_t const * Manuf_EquippedPCB_VPN_Main_val)
#else
void DevEepTest_Manuf_EquippedPCB_VPN_Main_Callback(EEP_HANDLE eep_handle,uint32_t const * Manuf_EquippedPCB_VPN_Main_val)
#endif
{
	printf("dev eep test: Manuf_EquippedPCB_VPN_Main, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_Manuf_EquippedPCB_VPN_Main_ITEM_LEN; i ++)
	{
		printf("%d ", Manuf_EquippedPCB_VPN_Main_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_Manuf_EquippedPCB_VPN_Main_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_Manuf_EquippedPCB_VPN_Main_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", Manuf_EquippedPCB_VPN_Main_val[i]);
		#elif EEP_CONTENT_Manuf_EquippedPCB_VPN_Main_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", Manuf_EquippedPCB_VPN_Main_val[i]);
		#else
		printf("%08x ", Manuf_EquippedPCB_VPN_Main_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_Manuf_Product_Serial_Number(int argc, char **argv)
{
#if EEP_CONTENT_Manuf_Product_Serial_Number_ITEM_LEN == 1
	#if EEP_CONTENT_Manuf_Product_Serial_Number_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t Manuf_Product_Serial_Number_val;
	#elif EEP_CONTENT_Manuf_Product_Serial_Number_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t Manuf_Product_Serial_Number_val;
	#else
	uint32_t Manuf_Product_Serial_Number_val;
	#endif
	Manuf_Product_Serial_Number_val = atoi(argv[2]);
#else // EEP_CONTENT_Manuf_Product_Serial_Number_ITEM_LEN > 1
	#if EEP_CONTENT_Manuf_Product_Serial_Number_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t Manuf_Product_Serial_Number_val[EEP_CONTENT_Manuf_Product_Serial_Number_ITEM_LEN];
	#elif EEP_CONTENT_Manuf_Product_Serial_Number_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t Manuf_Product_Serial_Number_val[EEP_CONTENT_Manuf_Product_Serial_Number_ITEM_LEN];
	#else
	uint32_t Manuf_Product_Serial_Number_val[EEP_CONTENT_Manuf_Product_Serial_Number_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_Manuf_Product_Serial_Number_ITEM_LEN; i ++ )
	{
		Manuf_Product_Serial_Number_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_Manuf_Product_Serial_Number_ITEM_LEN}	
	
	DevEep_Set_Manuf_Product_Serial_Number(eep_handle,Manuf_Product_Serial_Number_val);
}


#if EEP_CONTENT_Manuf_Product_Serial_Number_ITEM_LEN == 1
#if EEP_CONTENT_Manuf_Product_Serial_Number_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_Manuf_Product_Serial_Number_Callback(EEP_HANDLE eep_handle,uint8_t const Manuf_Product_Serial_Number_val)
#elif EEP_CONTENT_Manuf_Product_Serial_Number_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_Manuf_Product_Serial_Number_Callback(EEP_HANDLE eep_handle,uint16_t const Manuf_Product_Serial_Number_val)
#else
void DevEepTest_Manuf_Product_Serial_Number_Callback(EEP_HANDLE eep_handle,uint32_t const Manuf_Product_Serial_Number_val)
#endif	
{
	
	printf("dev eep test: Manuf_Product_Serial_Number, change to dec=%d,hex=0x%08x\r\n",Manuf_Product_Serial_Number_val,Manuf_Product_Serial_Number_val);
}
#else // EEP_CONTENT_Manuf_Product_Serial_Number_ITEM_LEN > 1
#if EEP_CONTENT_Manuf_Product_Serial_Number_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_Manuf_Product_Serial_Number_Callback(EEP_HANDLE eep_handle,uint8_t const * Manuf_Product_Serial_Number_val)
#elif EEP_CONTENT_Manuf_Product_Serial_Number_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_Manuf_Product_Serial_Number_Callback(EEP_HANDLE eep_handle,uint16_t const * Manuf_Product_Serial_Number_val)
#else
void DevEepTest_Manuf_Product_Serial_Number_Callback(EEP_HANDLE eep_handle,uint32_t const * Manuf_Product_Serial_Number_val)
#endif
{
	printf("dev eep test: Manuf_Product_Serial_Number, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_Manuf_Product_Serial_Number_ITEM_LEN; i ++)
	{
		printf("%d ", Manuf_Product_Serial_Number_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_Manuf_Product_Serial_Number_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_Manuf_Product_Serial_Number_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", Manuf_Product_Serial_Number_val[i]);
		#elif EEP_CONTENT_Manuf_Product_Serial_Number_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", Manuf_Product_Serial_Number_val[i]);
		#else
		printf("%08x ", Manuf_Product_Serial_Number_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_Manuf_EquippedPCB_Serial_Number_Sub(int argc, char **argv)
{
#if EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Sub_ITEM_LEN == 1
	#if EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Sub_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t Manuf_EquippedPCB_Serial_Number_Sub_val;
	#elif EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Sub_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t Manuf_EquippedPCB_Serial_Number_Sub_val;
	#else
	uint32_t Manuf_EquippedPCB_Serial_Number_Sub_val;
	#endif
	Manuf_EquippedPCB_Serial_Number_Sub_val = atoi(argv[2]);
#else // EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Sub_ITEM_LEN > 1
	#if EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Sub_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t Manuf_EquippedPCB_Serial_Number_Sub_val[EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Sub_ITEM_LEN];
	#elif EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Sub_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t Manuf_EquippedPCB_Serial_Number_Sub_val[EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Sub_ITEM_LEN];
	#else
	uint32_t Manuf_EquippedPCB_Serial_Number_Sub_val[EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Sub_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Sub_ITEM_LEN; i ++ )
	{
		Manuf_EquippedPCB_Serial_Number_Sub_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Sub_ITEM_LEN}	
	
	DevEep_Set_Manuf_EquippedPCB_Serial_Number_Sub(eep_handle,Manuf_EquippedPCB_Serial_Number_Sub_val);
}


#if EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Sub_ITEM_LEN == 1
#if EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Sub_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_Manuf_EquippedPCB_Serial_Number_Sub_Callback(EEP_HANDLE eep_handle,uint8_t const Manuf_EquippedPCB_Serial_Number_Sub_val)
#elif EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Sub_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_Manuf_EquippedPCB_Serial_Number_Sub_Callback(EEP_HANDLE eep_handle,uint16_t const Manuf_EquippedPCB_Serial_Number_Sub_val)
#else
void DevEepTest_Manuf_EquippedPCB_Serial_Number_Sub_Callback(EEP_HANDLE eep_handle,uint32_t const Manuf_EquippedPCB_Serial_Number_Sub_val)
#endif	
{
	
	printf("dev eep test: Manuf_EquippedPCB_Serial_Number_Sub, change to dec=%d,hex=0x%08x\r\n",Manuf_EquippedPCB_Serial_Number_Sub_val,Manuf_EquippedPCB_Serial_Number_Sub_val);
}
#else // EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Sub_ITEM_LEN > 1
#if EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Sub_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_Manuf_EquippedPCB_Serial_Number_Sub_Callback(EEP_HANDLE eep_handle,uint8_t const * Manuf_EquippedPCB_Serial_Number_Sub_val)
#elif EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Sub_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_Manuf_EquippedPCB_Serial_Number_Sub_Callback(EEP_HANDLE eep_handle,uint16_t const * Manuf_EquippedPCB_Serial_Number_Sub_val)
#else
void DevEepTest_Manuf_EquippedPCB_Serial_Number_Sub_Callback(EEP_HANDLE eep_handle,uint32_t const * Manuf_EquippedPCB_Serial_Number_Sub_val)
#endif
{
	printf("dev eep test: Manuf_EquippedPCB_Serial_Number_Sub, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Sub_ITEM_LEN; i ++)
	{
		printf("%d ", Manuf_EquippedPCB_Serial_Number_Sub_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Sub_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Sub_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", Manuf_EquippedPCB_Serial_Number_Sub_val[i]);
		#elif EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Sub_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", Manuf_EquippedPCB_Serial_Number_Sub_val[i]);
		#else
		printf("%08x ", Manuf_EquippedPCB_Serial_Number_Sub_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_Manuf_EquippedPCB_Serial_Number_Main(int argc, char **argv)
{
#if EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Main_ITEM_LEN == 1
	#if EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Main_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t Manuf_EquippedPCB_Serial_Number_Main_val;
	#elif EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Main_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t Manuf_EquippedPCB_Serial_Number_Main_val;
	#else
	uint32_t Manuf_EquippedPCB_Serial_Number_Main_val;
	#endif
	Manuf_EquippedPCB_Serial_Number_Main_val = atoi(argv[2]);
#else // EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Main_ITEM_LEN > 1
	#if EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Main_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t Manuf_EquippedPCB_Serial_Number_Main_val[EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Main_ITEM_LEN];
	#elif EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Main_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t Manuf_EquippedPCB_Serial_Number_Main_val[EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Main_ITEM_LEN];
	#else
	uint32_t Manuf_EquippedPCB_Serial_Number_Main_val[EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Main_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Main_ITEM_LEN; i ++ )
	{
		Manuf_EquippedPCB_Serial_Number_Main_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Main_ITEM_LEN}	
	
	DevEep_Set_Manuf_EquippedPCB_Serial_Number_Main(eep_handle,Manuf_EquippedPCB_Serial_Number_Main_val);
}


#if EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Main_ITEM_LEN == 1
#if EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Main_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_Manuf_EquippedPCB_Serial_Number_Main_Callback(EEP_HANDLE eep_handle,uint8_t const Manuf_EquippedPCB_Serial_Number_Main_val)
#elif EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Main_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_Manuf_EquippedPCB_Serial_Number_Main_Callback(EEP_HANDLE eep_handle,uint16_t const Manuf_EquippedPCB_Serial_Number_Main_val)
#else
void DevEepTest_Manuf_EquippedPCB_Serial_Number_Main_Callback(EEP_HANDLE eep_handle,uint32_t const Manuf_EquippedPCB_Serial_Number_Main_val)
#endif	
{
	
	printf("dev eep test: Manuf_EquippedPCB_Serial_Number_Main, change to dec=%d,hex=0x%08x\r\n",Manuf_EquippedPCB_Serial_Number_Main_val,Manuf_EquippedPCB_Serial_Number_Main_val);
}
#else // EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Main_ITEM_LEN > 1
#if EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Main_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_Manuf_EquippedPCB_Serial_Number_Main_Callback(EEP_HANDLE eep_handle,uint8_t const * Manuf_EquippedPCB_Serial_Number_Main_val)
#elif EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Main_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_Manuf_EquippedPCB_Serial_Number_Main_Callback(EEP_HANDLE eep_handle,uint16_t const * Manuf_EquippedPCB_Serial_Number_Main_val)
#else
void DevEepTest_Manuf_EquippedPCB_Serial_Number_Main_Callback(EEP_HANDLE eep_handle,uint32_t const * Manuf_EquippedPCB_Serial_Number_Main_val)
#endif
{
	printf("dev eep test: Manuf_EquippedPCB_Serial_Number_Main, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Main_ITEM_LEN; i ++)
	{
		printf("%d ", Manuf_EquippedPCB_Serial_Number_Main_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Main_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Main_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", Manuf_EquippedPCB_Serial_Number_Main_val[i]);
		#elif EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Main_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", Manuf_EquippedPCB_Serial_Number_Main_val[i]);
		#else
		printf("%08x ", Manuf_EquippedPCB_Serial_Number_Main_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_NVM_Revision(int argc, char **argv)
{
#if EEP_CONTENT_NVM_Revision_ITEM_LEN == 1
	#if EEP_CONTENT_NVM_Revision_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t NVM_Revision_val;
	#elif EEP_CONTENT_NVM_Revision_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t NVM_Revision_val;
	#else
	uint32_t NVM_Revision_val;
	#endif
	NVM_Revision_val = atoi(argv[2]);
#else // EEP_CONTENT_NVM_Revision_ITEM_LEN > 1
	#if EEP_CONTENT_NVM_Revision_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t NVM_Revision_val[EEP_CONTENT_NVM_Revision_ITEM_LEN];
	#elif EEP_CONTENT_NVM_Revision_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t NVM_Revision_val[EEP_CONTENT_NVM_Revision_ITEM_LEN];
	#else
	uint32_t NVM_Revision_val[EEP_CONTENT_NVM_Revision_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_NVM_Revision_ITEM_LEN; i ++ )
	{
		NVM_Revision_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_NVM_Revision_ITEM_LEN}	
	
	DevEep_Set_NVM_Revision(eep_handle,NVM_Revision_val);
}


#if EEP_CONTENT_NVM_Revision_ITEM_LEN == 1
#if EEP_CONTENT_NVM_Revision_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_NVM_Revision_Callback(EEP_HANDLE eep_handle,uint8_t const NVM_Revision_val)
#elif EEP_CONTENT_NVM_Revision_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_NVM_Revision_Callback(EEP_HANDLE eep_handle,uint16_t const NVM_Revision_val)
#else
void DevEepTest_NVM_Revision_Callback(EEP_HANDLE eep_handle,uint32_t const NVM_Revision_val)
#endif	
{
	
	printf("dev eep test: NVM_Revision, change to dec=%d,hex=0x%08x\r\n",NVM_Revision_val,NVM_Revision_val);
}
#else // EEP_CONTENT_NVM_Revision_ITEM_LEN > 1
#if EEP_CONTENT_NVM_Revision_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_NVM_Revision_Callback(EEP_HANDLE eep_handle,uint8_t const * NVM_Revision_val)
#elif EEP_CONTENT_NVM_Revision_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_NVM_Revision_Callback(EEP_HANDLE eep_handle,uint16_t const * NVM_Revision_val)
#else
void DevEepTest_NVM_Revision_Callback(EEP_HANDLE eep_handle,uint32_t const * NVM_Revision_val)
#endif
{
	printf("dev eep test: NVM_Revision, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_NVM_Revision_ITEM_LEN; i ++)
	{
		printf("%d ", NVM_Revision_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_NVM_Revision_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_NVM_Revision_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", NVM_Revision_val[i]);
		#elif EEP_CONTENT_NVM_Revision_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", NVM_Revision_val[i]);
		#else
		printf("%08x ", NVM_Revision_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_Manuf_SMD_Date1(int argc, char **argv)
{
#if EEP_CONTENT_Manuf_SMD_Date1_ITEM_LEN == 1
	#if EEP_CONTENT_Manuf_SMD_Date1_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t Manuf_SMD_Date1_val;
	#elif EEP_CONTENT_Manuf_SMD_Date1_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t Manuf_SMD_Date1_val;
	#else
	uint32_t Manuf_SMD_Date1_val;
	#endif
	Manuf_SMD_Date1_val = atoi(argv[2]);
#else // EEP_CONTENT_Manuf_SMD_Date1_ITEM_LEN > 1
	#if EEP_CONTENT_Manuf_SMD_Date1_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t Manuf_SMD_Date1_val[EEP_CONTENT_Manuf_SMD_Date1_ITEM_LEN];
	#elif EEP_CONTENT_Manuf_SMD_Date1_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t Manuf_SMD_Date1_val[EEP_CONTENT_Manuf_SMD_Date1_ITEM_LEN];
	#else
	uint32_t Manuf_SMD_Date1_val[EEP_CONTENT_Manuf_SMD_Date1_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_Manuf_SMD_Date1_ITEM_LEN; i ++ )
	{
		Manuf_SMD_Date1_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_Manuf_SMD_Date1_ITEM_LEN}	
	
	DevEep_Set_Manuf_SMD_Date1(eep_handle,Manuf_SMD_Date1_val);
}


#if EEP_CONTENT_Manuf_SMD_Date1_ITEM_LEN == 1
#if EEP_CONTENT_Manuf_SMD_Date1_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_Manuf_SMD_Date1_Callback(EEP_HANDLE eep_handle,uint8_t const Manuf_SMD_Date1_val)
#elif EEP_CONTENT_Manuf_SMD_Date1_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_Manuf_SMD_Date1_Callback(EEP_HANDLE eep_handle,uint16_t const Manuf_SMD_Date1_val)
#else
void DevEepTest_Manuf_SMD_Date1_Callback(EEP_HANDLE eep_handle,uint32_t const Manuf_SMD_Date1_val)
#endif	
{
	
	printf("dev eep test: Manuf_SMD_Date1, change to dec=%d,hex=0x%08x\r\n",Manuf_SMD_Date1_val,Manuf_SMD_Date1_val);
}
#else // EEP_CONTENT_Manuf_SMD_Date1_ITEM_LEN > 1
#if EEP_CONTENT_Manuf_SMD_Date1_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_Manuf_SMD_Date1_Callback(EEP_HANDLE eep_handle,uint8_t const * Manuf_SMD_Date1_val)
#elif EEP_CONTENT_Manuf_SMD_Date1_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_Manuf_SMD_Date1_Callback(EEP_HANDLE eep_handle,uint16_t const * Manuf_SMD_Date1_val)
#else
void DevEepTest_Manuf_SMD_Date1_Callback(EEP_HANDLE eep_handle,uint32_t const * Manuf_SMD_Date1_val)
#endif
{
	printf("dev eep test: Manuf_SMD_Date1, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_Manuf_SMD_Date1_ITEM_LEN; i ++)
	{
		printf("%d ", Manuf_SMD_Date1_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_Manuf_SMD_Date1_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_Manuf_SMD_Date1_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", Manuf_SMD_Date1_val[i]);
		#elif EEP_CONTENT_Manuf_SMD_Date1_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", Manuf_SMD_Date1_val[i]);
		#else
		printf("%08x ", Manuf_SMD_Date1_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_Manuf_SMD_Date2(int argc, char **argv)
{
#if EEP_CONTENT_Manuf_SMD_Date2_ITEM_LEN == 1
	#if EEP_CONTENT_Manuf_SMD_Date2_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t Manuf_SMD_Date2_val;
	#elif EEP_CONTENT_Manuf_SMD_Date2_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t Manuf_SMD_Date2_val;
	#else
	uint32_t Manuf_SMD_Date2_val;
	#endif
	Manuf_SMD_Date2_val = atoi(argv[2]);
#else // EEP_CONTENT_Manuf_SMD_Date2_ITEM_LEN > 1
	#if EEP_CONTENT_Manuf_SMD_Date2_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t Manuf_SMD_Date2_val[EEP_CONTENT_Manuf_SMD_Date2_ITEM_LEN];
	#elif EEP_CONTENT_Manuf_SMD_Date2_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t Manuf_SMD_Date2_val[EEP_CONTENT_Manuf_SMD_Date2_ITEM_LEN];
	#else
	uint32_t Manuf_SMD_Date2_val[EEP_CONTENT_Manuf_SMD_Date2_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_Manuf_SMD_Date2_ITEM_LEN; i ++ )
	{
		Manuf_SMD_Date2_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_Manuf_SMD_Date2_ITEM_LEN}	
	
	DevEep_Set_Manuf_SMD_Date2(eep_handle,Manuf_SMD_Date2_val);
}


#if EEP_CONTENT_Manuf_SMD_Date2_ITEM_LEN == 1
#if EEP_CONTENT_Manuf_SMD_Date2_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_Manuf_SMD_Date2_Callback(EEP_HANDLE eep_handle,uint8_t const Manuf_SMD_Date2_val)
#elif EEP_CONTENT_Manuf_SMD_Date2_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_Manuf_SMD_Date2_Callback(EEP_HANDLE eep_handle,uint16_t const Manuf_SMD_Date2_val)
#else
void DevEepTest_Manuf_SMD_Date2_Callback(EEP_HANDLE eep_handle,uint32_t const Manuf_SMD_Date2_val)
#endif	
{
	
	printf("dev eep test: Manuf_SMD_Date2, change to dec=%d,hex=0x%08x\r\n",Manuf_SMD_Date2_val,Manuf_SMD_Date2_val);
}
#else // EEP_CONTENT_Manuf_SMD_Date2_ITEM_LEN > 1
#if EEP_CONTENT_Manuf_SMD_Date2_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_Manuf_SMD_Date2_Callback(EEP_HANDLE eep_handle,uint8_t const * Manuf_SMD_Date2_val)
#elif EEP_CONTENT_Manuf_SMD_Date2_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_Manuf_SMD_Date2_Callback(EEP_HANDLE eep_handle,uint16_t const * Manuf_SMD_Date2_val)
#else
void DevEepTest_Manuf_SMD_Date2_Callback(EEP_HANDLE eep_handle,uint32_t const * Manuf_SMD_Date2_val)
#endif
{
	printf("dev eep test: Manuf_SMD_Date2, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_Manuf_SMD_Date2_ITEM_LEN; i ++)
	{
		printf("%d ", Manuf_SMD_Date2_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_Manuf_SMD_Date2_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_Manuf_SMD_Date2_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", Manuf_SMD_Date2_val[i]);
		#elif EEP_CONTENT_Manuf_SMD_Date2_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", Manuf_SMD_Date2_val[i]);
		#else
		printf("%08x ", Manuf_SMD_Date2_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_Manuf_Assembly_Date(int argc, char **argv)
{
#if EEP_CONTENT_Manuf_Assembly_Date_ITEM_LEN == 1
	#if EEP_CONTENT_Manuf_Assembly_Date_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t Manuf_Assembly_Date_val;
	#elif EEP_CONTENT_Manuf_Assembly_Date_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t Manuf_Assembly_Date_val;
	#else
	uint32_t Manuf_Assembly_Date_val;
	#endif
	Manuf_Assembly_Date_val = atoi(argv[2]);
#else // EEP_CONTENT_Manuf_Assembly_Date_ITEM_LEN > 1
	#if EEP_CONTENT_Manuf_Assembly_Date_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t Manuf_Assembly_Date_val[EEP_CONTENT_Manuf_Assembly_Date_ITEM_LEN];
	#elif EEP_CONTENT_Manuf_Assembly_Date_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t Manuf_Assembly_Date_val[EEP_CONTENT_Manuf_Assembly_Date_ITEM_LEN];
	#else
	uint32_t Manuf_Assembly_Date_val[EEP_CONTENT_Manuf_Assembly_Date_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_Manuf_Assembly_Date_ITEM_LEN; i ++ )
	{
		Manuf_Assembly_Date_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_Manuf_Assembly_Date_ITEM_LEN}	
	
	DevEep_Set_Manuf_Assembly_Date(eep_handle,Manuf_Assembly_Date_val);
}


#if EEP_CONTENT_Manuf_Assembly_Date_ITEM_LEN == 1
#if EEP_CONTENT_Manuf_Assembly_Date_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_Manuf_Assembly_Date_Callback(EEP_HANDLE eep_handle,uint8_t const Manuf_Assembly_Date_val)
#elif EEP_CONTENT_Manuf_Assembly_Date_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_Manuf_Assembly_Date_Callback(EEP_HANDLE eep_handle,uint16_t const Manuf_Assembly_Date_val)
#else
void DevEepTest_Manuf_Assembly_Date_Callback(EEP_HANDLE eep_handle,uint32_t const Manuf_Assembly_Date_val)
#endif	
{
	
	printf("dev eep test: Manuf_Assembly_Date, change to dec=%d,hex=0x%08x\r\n",Manuf_Assembly_Date_val,Manuf_Assembly_Date_val);
}
#else // EEP_CONTENT_Manuf_Assembly_Date_ITEM_LEN > 1
#if EEP_CONTENT_Manuf_Assembly_Date_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_Manuf_Assembly_Date_Callback(EEP_HANDLE eep_handle,uint8_t const * Manuf_Assembly_Date_val)
#elif EEP_CONTENT_Manuf_Assembly_Date_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_Manuf_Assembly_Date_Callback(EEP_HANDLE eep_handle,uint16_t const * Manuf_Assembly_Date_val)
#else
void DevEepTest_Manuf_Assembly_Date_Callback(EEP_HANDLE eep_handle,uint32_t const * Manuf_Assembly_Date_val)
#endif
{
	printf("dev eep test: Manuf_Assembly_Date, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_Manuf_Assembly_Date_ITEM_LEN; i ++)
	{
		printf("%d ", Manuf_Assembly_Date_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_Manuf_Assembly_Date_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_Manuf_Assembly_Date_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", Manuf_Assembly_Date_val[i]);
		#elif EEP_CONTENT_Manuf_Assembly_Date_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", Manuf_Assembly_Date_val[i]);
		#else
		printf("%08x ", Manuf_Assembly_Date_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_Traceability_Data(int argc, char **argv)
{
#if EEP_CONTENT_Traceability_Data_ITEM_LEN == 1
	#if EEP_CONTENT_Traceability_Data_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t Traceability_Data_val;
	#elif EEP_CONTENT_Traceability_Data_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t Traceability_Data_val;
	#else
	uint32_t Traceability_Data_val;
	#endif
	Traceability_Data_val = atoi(argv[2]);
#else // EEP_CONTENT_Traceability_Data_ITEM_LEN > 1
	#if EEP_CONTENT_Traceability_Data_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t Traceability_Data_val[EEP_CONTENT_Traceability_Data_ITEM_LEN];
	#elif EEP_CONTENT_Traceability_Data_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t Traceability_Data_val[EEP_CONTENT_Traceability_Data_ITEM_LEN];
	#else
	uint32_t Traceability_Data_val[EEP_CONTENT_Traceability_Data_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_Traceability_Data_ITEM_LEN; i ++ )
	{
		Traceability_Data_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_Traceability_Data_ITEM_LEN}	
	
	DevEep_Set_Traceability_Data(eep_handle,Traceability_Data_val);
}


#if EEP_CONTENT_Traceability_Data_ITEM_LEN == 1
#if EEP_CONTENT_Traceability_Data_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_Traceability_Data_Callback(EEP_HANDLE eep_handle,uint8_t const Traceability_Data_val)
#elif EEP_CONTENT_Traceability_Data_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_Traceability_Data_Callback(EEP_HANDLE eep_handle,uint16_t const Traceability_Data_val)
#else
void DevEepTest_Traceability_Data_Callback(EEP_HANDLE eep_handle,uint32_t const Traceability_Data_val)
#endif	
{
	
	printf("dev eep test: Traceability_Data, change to dec=%d,hex=0x%08x\r\n",Traceability_Data_val,Traceability_Data_val);
}
#else // EEP_CONTENT_Traceability_Data_ITEM_LEN > 1
#if EEP_CONTENT_Traceability_Data_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_Traceability_Data_Callback(EEP_HANDLE eep_handle,uint8_t const * Traceability_Data_val)
#elif EEP_CONTENT_Traceability_Data_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_Traceability_Data_Callback(EEP_HANDLE eep_handle,uint16_t const * Traceability_Data_val)
#else
void DevEepTest_Traceability_Data_Callback(EEP_HANDLE eep_handle,uint32_t const * Traceability_Data_val)
#endif
{
	printf("dev eep test: Traceability_Data, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_Traceability_Data_ITEM_LEN; i ++)
	{
		printf("%d ", Traceability_Data_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_Traceability_Data_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_Traceability_Data_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", Traceability_Data_val[i]);
		#elif EEP_CONTENT_Traceability_Data_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", Traceability_Data_val[i]);
		#else
		printf("%08x ", Traceability_Data_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_VehicleManufacturerSparePartNumber(int argc, char **argv)
{
#if EEP_CONTENT_VehicleManufacturerSparePartNumber_ITEM_LEN == 1
	#if EEP_CONTENT_VehicleManufacturerSparePartNumber_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t VehicleManufacturerSparePartNumber_val;
	#elif EEP_CONTENT_VehicleManufacturerSparePartNumber_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t VehicleManufacturerSparePartNumber_val;
	#else
	uint32_t VehicleManufacturerSparePartNumber_val;
	#endif
	VehicleManufacturerSparePartNumber_val = atoi(argv[2]);
#else // EEP_CONTENT_VehicleManufacturerSparePartNumber_ITEM_LEN > 1
	#if EEP_CONTENT_VehicleManufacturerSparePartNumber_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t VehicleManufacturerSparePartNumber_val[EEP_CONTENT_VehicleManufacturerSparePartNumber_ITEM_LEN];
	#elif EEP_CONTENT_VehicleManufacturerSparePartNumber_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t VehicleManufacturerSparePartNumber_val[EEP_CONTENT_VehicleManufacturerSparePartNumber_ITEM_LEN];
	#else
	uint32_t VehicleManufacturerSparePartNumber_val[EEP_CONTENT_VehicleManufacturerSparePartNumber_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_VehicleManufacturerSparePartNumber_ITEM_LEN; i ++ )
	{
		VehicleManufacturerSparePartNumber_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_VehicleManufacturerSparePartNumber_ITEM_LEN}	
	
	DevEep_Set_VehicleManufacturerSparePartNumber(eep_handle,VehicleManufacturerSparePartNumber_val);
}


#if EEP_CONTENT_VehicleManufacturerSparePartNumber_ITEM_LEN == 1
#if EEP_CONTENT_VehicleManufacturerSparePartNumber_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_VehicleManufacturerSparePartNumber_Callback(EEP_HANDLE eep_handle,uint8_t const VehicleManufacturerSparePartNumber_val)
#elif EEP_CONTENT_VehicleManufacturerSparePartNumber_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_VehicleManufacturerSparePartNumber_Callback(EEP_HANDLE eep_handle,uint16_t const VehicleManufacturerSparePartNumber_val)
#else
void DevEepTest_VehicleManufacturerSparePartNumber_Callback(EEP_HANDLE eep_handle,uint32_t const VehicleManufacturerSparePartNumber_val)
#endif	
{
	
	printf("dev eep test: VehicleManufacturerSparePartNumber, change to dec=%d,hex=0x%08x\r\n",VehicleManufacturerSparePartNumber_val,VehicleManufacturerSparePartNumber_val);
}
#else // EEP_CONTENT_VehicleManufacturerSparePartNumber_ITEM_LEN > 1
#if EEP_CONTENT_VehicleManufacturerSparePartNumber_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_VehicleManufacturerSparePartNumber_Callback(EEP_HANDLE eep_handle,uint8_t const * VehicleManufacturerSparePartNumber_val)
#elif EEP_CONTENT_VehicleManufacturerSparePartNumber_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_VehicleManufacturerSparePartNumber_Callback(EEP_HANDLE eep_handle,uint16_t const * VehicleManufacturerSparePartNumber_val)
#else
void DevEepTest_VehicleManufacturerSparePartNumber_Callback(EEP_HANDLE eep_handle,uint32_t const * VehicleManufacturerSparePartNumber_val)
#endif
{
	printf("dev eep test: VehicleManufacturerSparePartNumber, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_VehicleManufacturerSparePartNumber_ITEM_LEN; i ++)
	{
		printf("%d ", VehicleManufacturerSparePartNumber_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_VehicleManufacturerSparePartNumber_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_VehicleManufacturerSparePartNumber_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", VehicleManufacturerSparePartNumber_val[i]);
		#elif EEP_CONTENT_VehicleManufacturerSparePartNumber_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", VehicleManufacturerSparePartNumber_val[i]);
		#else
		printf("%08x ", VehicleManufacturerSparePartNumber_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_VehicleManufacturerSparePartNumber_Assembly(int argc, char **argv)
{
#if EEP_CONTENT_VehicleManufacturerSparePartNumber_Assembly_ITEM_LEN == 1
	#if EEP_CONTENT_VehicleManufacturerSparePartNumber_Assembly_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t VehicleManufacturerSparePartNumber_Assembly_val;
	#elif EEP_CONTENT_VehicleManufacturerSparePartNumber_Assembly_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t VehicleManufacturerSparePartNumber_Assembly_val;
	#else
	uint32_t VehicleManufacturerSparePartNumber_Assembly_val;
	#endif
	VehicleManufacturerSparePartNumber_Assembly_val = atoi(argv[2]);
#else // EEP_CONTENT_VehicleManufacturerSparePartNumber_Assembly_ITEM_LEN > 1
	#if EEP_CONTENT_VehicleManufacturerSparePartNumber_Assembly_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t VehicleManufacturerSparePartNumber_Assembly_val[EEP_CONTENT_VehicleManufacturerSparePartNumber_Assembly_ITEM_LEN];
	#elif EEP_CONTENT_VehicleManufacturerSparePartNumber_Assembly_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t VehicleManufacturerSparePartNumber_Assembly_val[EEP_CONTENT_VehicleManufacturerSparePartNumber_Assembly_ITEM_LEN];
	#else
	uint32_t VehicleManufacturerSparePartNumber_Assembly_val[EEP_CONTENT_VehicleManufacturerSparePartNumber_Assembly_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_VehicleManufacturerSparePartNumber_Assembly_ITEM_LEN; i ++ )
	{
		VehicleManufacturerSparePartNumber_Assembly_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_VehicleManufacturerSparePartNumber_Assembly_ITEM_LEN}	
	
	DevEep_Set_VehicleManufacturerSparePartNumber_Assembly(eep_handle,VehicleManufacturerSparePartNumber_Assembly_val);
}


#if EEP_CONTENT_VehicleManufacturerSparePartNumber_Assembly_ITEM_LEN == 1
#if EEP_CONTENT_VehicleManufacturerSparePartNumber_Assembly_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_VehicleManufacturerSparePartNumber_Assembly_Callback(EEP_HANDLE eep_handle,uint8_t const VehicleManufacturerSparePartNumber_Assembly_val)
#elif EEP_CONTENT_VehicleManufacturerSparePartNumber_Assembly_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_VehicleManufacturerSparePartNumber_Assembly_Callback(EEP_HANDLE eep_handle,uint16_t const VehicleManufacturerSparePartNumber_Assembly_val)
#else
void DevEepTest_VehicleManufacturerSparePartNumber_Assembly_Callback(EEP_HANDLE eep_handle,uint32_t const VehicleManufacturerSparePartNumber_Assembly_val)
#endif	
{
	
	printf("dev eep test: VehicleManufacturerSparePartNumber_Assembly, change to dec=%d,hex=0x%08x\r\n",VehicleManufacturerSparePartNumber_Assembly_val,VehicleManufacturerSparePartNumber_Assembly_val);
}
#else // EEP_CONTENT_VehicleManufacturerSparePartNumber_Assembly_ITEM_LEN > 1
#if EEP_CONTENT_VehicleManufacturerSparePartNumber_Assembly_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_VehicleManufacturerSparePartNumber_Assembly_Callback(EEP_HANDLE eep_handle,uint8_t const * VehicleManufacturerSparePartNumber_Assembly_val)
#elif EEP_CONTENT_VehicleManufacturerSparePartNumber_Assembly_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_VehicleManufacturerSparePartNumber_Assembly_Callback(EEP_HANDLE eep_handle,uint16_t const * VehicleManufacturerSparePartNumber_Assembly_val)
#else
void DevEepTest_VehicleManufacturerSparePartNumber_Assembly_Callback(EEP_HANDLE eep_handle,uint32_t const * VehicleManufacturerSparePartNumber_Assembly_val)
#endif
{
	printf("dev eep test: VehicleManufacturerSparePartNumber_Assembly, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_VehicleManufacturerSparePartNumber_Assembly_ITEM_LEN; i ++)
	{
		printf("%d ", VehicleManufacturerSparePartNumber_Assembly_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_VehicleManufacturerSparePartNumber_Assembly_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_VehicleManufacturerSparePartNumber_Assembly_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", VehicleManufacturerSparePartNumber_Assembly_val[i]);
		#elif EEP_CONTENT_VehicleManufacturerSparePartNumber_Assembly_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", VehicleManufacturerSparePartNumber_Assembly_val[i]);
		#else
		printf("%08x ", VehicleManufacturerSparePartNumber_Assembly_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_Operational_Reference(int argc, char **argv)
{
#if EEP_CONTENT_Operational_Reference_ITEM_LEN == 1
	#if EEP_CONTENT_Operational_Reference_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t Operational_Reference_val;
	#elif EEP_CONTENT_Operational_Reference_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t Operational_Reference_val;
	#else
	uint32_t Operational_Reference_val;
	#endif
	Operational_Reference_val = atoi(argv[2]);
#else // EEP_CONTENT_Operational_Reference_ITEM_LEN > 1
	#if EEP_CONTENT_Operational_Reference_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t Operational_Reference_val[EEP_CONTENT_Operational_Reference_ITEM_LEN];
	#elif EEP_CONTENT_Operational_Reference_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t Operational_Reference_val[EEP_CONTENT_Operational_Reference_ITEM_LEN];
	#else
	uint32_t Operational_Reference_val[EEP_CONTENT_Operational_Reference_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_Operational_Reference_ITEM_LEN; i ++ )
	{
		Operational_Reference_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_Operational_Reference_ITEM_LEN}	
	
	DevEep_Set_Operational_Reference(eep_handle,Operational_Reference_val);
}


#if EEP_CONTENT_Operational_Reference_ITEM_LEN == 1
#if EEP_CONTENT_Operational_Reference_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_Operational_Reference_Callback(EEP_HANDLE eep_handle,uint8_t const Operational_Reference_val)
#elif EEP_CONTENT_Operational_Reference_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_Operational_Reference_Callback(EEP_HANDLE eep_handle,uint16_t const Operational_Reference_val)
#else
void DevEepTest_Operational_Reference_Callback(EEP_HANDLE eep_handle,uint32_t const Operational_Reference_val)
#endif	
{
	
	printf("dev eep test: Operational_Reference, change to dec=%d,hex=0x%08x\r\n",Operational_Reference_val,Operational_Reference_val);
}
#else // EEP_CONTENT_Operational_Reference_ITEM_LEN > 1
#if EEP_CONTENT_Operational_Reference_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_Operational_Reference_Callback(EEP_HANDLE eep_handle,uint8_t const * Operational_Reference_val)
#elif EEP_CONTENT_Operational_Reference_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_Operational_Reference_Callback(EEP_HANDLE eep_handle,uint16_t const * Operational_Reference_val)
#else
void DevEepTest_Operational_Reference_Callback(EEP_HANDLE eep_handle,uint32_t const * Operational_Reference_val)
#endif
{
	printf("dev eep test: Operational_Reference, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_Operational_Reference_ITEM_LEN; i ++)
	{
		printf("%d ", Operational_Reference_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_Operational_Reference_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_Operational_Reference_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", Operational_Reference_val[i]);
		#elif EEP_CONTENT_Operational_Reference_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", Operational_Reference_val[i]);
		#else
		printf("%08x ", Operational_Reference_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_Supplier_Number(int argc, char **argv)
{
#if EEP_CONTENT_Supplier_Number_ITEM_LEN == 1
	#if EEP_CONTENT_Supplier_Number_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t Supplier_Number_val;
	#elif EEP_CONTENT_Supplier_Number_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t Supplier_Number_val;
	#else
	uint32_t Supplier_Number_val;
	#endif
	Supplier_Number_val = atoi(argv[2]);
#else // EEP_CONTENT_Supplier_Number_ITEM_LEN > 1
	#if EEP_CONTENT_Supplier_Number_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t Supplier_Number_val[EEP_CONTENT_Supplier_Number_ITEM_LEN];
	#elif EEP_CONTENT_Supplier_Number_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t Supplier_Number_val[EEP_CONTENT_Supplier_Number_ITEM_LEN];
	#else
	uint32_t Supplier_Number_val[EEP_CONTENT_Supplier_Number_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_Supplier_Number_ITEM_LEN; i ++ )
	{
		Supplier_Number_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_Supplier_Number_ITEM_LEN}	
	
	DevEep_Set_Supplier_Number(eep_handle,Supplier_Number_val);
}


#if EEP_CONTENT_Supplier_Number_ITEM_LEN == 1
#if EEP_CONTENT_Supplier_Number_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_Supplier_Number_Callback(EEP_HANDLE eep_handle,uint8_t const Supplier_Number_val)
#elif EEP_CONTENT_Supplier_Number_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_Supplier_Number_Callback(EEP_HANDLE eep_handle,uint16_t const Supplier_Number_val)
#else
void DevEepTest_Supplier_Number_Callback(EEP_HANDLE eep_handle,uint32_t const Supplier_Number_val)
#endif	
{
	
	printf("dev eep test: Supplier_Number, change to dec=%d,hex=0x%08x\r\n",Supplier_Number_val,Supplier_Number_val);
}
#else // EEP_CONTENT_Supplier_Number_ITEM_LEN > 1
#if EEP_CONTENT_Supplier_Number_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_Supplier_Number_Callback(EEP_HANDLE eep_handle,uint8_t const * Supplier_Number_val)
#elif EEP_CONTENT_Supplier_Number_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_Supplier_Number_Callback(EEP_HANDLE eep_handle,uint16_t const * Supplier_Number_val)
#else
void DevEepTest_Supplier_Number_Callback(EEP_HANDLE eep_handle,uint32_t const * Supplier_Number_val)
#endif
{
	printf("dev eep test: Supplier_Number, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_Supplier_Number_ITEM_LEN; i ++)
	{
		printf("%d ", Supplier_Number_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_Supplier_Number_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_Supplier_Number_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", Supplier_Number_val[i]);
		#elif EEP_CONTENT_Supplier_Number_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", Supplier_Number_val[i]);
		#else
		printf("%08x ", Supplier_Number_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_ECU_Serial_Number(int argc, char **argv)
{
#if EEP_CONTENT_ECU_Serial_Number_ITEM_LEN == 1
	#if EEP_CONTENT_ECU_Serial_Number_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t ECU_Serial_Number_val;
	#elif EEP_CONTENT_ECU_Serial_Number_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t ECU_Serial_Number_val;
	#else
	uint32_t ECU_Serial_Number_val;
	#endif
	ECU_Serial_Number_val = atoi(argv[2]);
#else // EEP_CONTENT_ECU_Serial_Number_ITEM_LEN > 1
	#if EEP_CONTENT_ECU_Serial_Number_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t ECU_Serial_Number_val[EEP_CONTENT_ECU_Serial_Number_ITEM_LEN];
	#elif EEP_CONTENT_ECU_Serial_Number_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t ECU_Serial_Number_val[EEP_CONTENT_ECU_Serial_Number_ITEM_LEN];
	#else
	uint32_t ECU_Serial_Number_val[EEP_CONTENT_ECU_Serial_Number_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_ECU_Serial_Number_ITEM_LEN; i ++ )
	{
		ECU_Serial_Number_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_ECU_Serial_Number_ITEM_LEN}	
	
	DevEep_Set_ECU_Serial_Number(eep_handle,ECU_Serial_Number_val);
}


#if EEP_CONTENT_ECU_Serial_Number_ITEM_LEN == 1
#if EEP_CONTENT_ECU_Serial_Number_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_ECU_Serial_Number_Callback(EEP_HANDLE eep_handle,uint8_t const ECU_Serial_Number_val)
#elif EEP_CONTENT_ECU_Serial_Number_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_ECU_Serial_Number_Callback(EEP_HANDLE eep_handle,uint16_t const ECU_Serial_Number_val)
#else
void DevEepTest_ECU_Serial_Number_Callback(EEP_HANDLE eep_handle,uint32_t const ECU_Serial_Number_val)
#endif	
{
	
	printf("dev eep test: ECU_Serial_Number, change to dec=%d,hex=0x%08x\r\n",ECU_Serial_Number_val,ECU_Serial_Number_val);
}
#else // EEP_CONTENT_ECU_Serial_Number_ITEM_LEN > 1
#if EEP_CONTENT_ECU_Serial_Number_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_ECU_Serial_Number_Callback(EEP_HANDLE eep_handle,uint8_t const * ECU_Serial_Number_val)
#elif EEP_CONTENT_ECU_Serial_Number_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_ECU_Serial_Number_Callback(EEP_HANDLE eep_handle,uint16_t const * ECU_Serial_Number_val)
#else
void DevEepTest_ECU_Serial_Number_Callback(EEP_HANDLE eep_handle,uint32_t const * ECU_Serial_Number_val)
#endif
{
	printf("dev eep test: ECU_Serial_Number, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_ECU_Serial_Number_ITEM_LEN; i ++)
	{
		printf("%d ", ECU_Serial_Number_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_ECU_Serial_Number_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_ECU_Serial_Number_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", ECU_Serial_Number_val[i]);
		#elif EEP_CONTENT_ECU_Serial_Number_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", ECU_Serial_Number_val[i]);
		#else
		printf("%08x ", ECU_Serial_Number_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_Manufacturing_Identification_Code(int argc, char **argv)
{
#if EEP_CONTENT_Manufacturing_Identification_Code_ITEM_LEN == 1
	#if EEP_CONTENT_Manufacturing_Identification_Code_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t Manufacturing_Identification_Code_val;
	#elif EEP_CONTENT_Manufacturing_Identification_Code_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t Manufacturing_Identification_Code_val;
	#else
	uint32_t Manufacturing_Identification_Code_val;
	#endif
	Manufacturing_Identification_Code_val = atoi(argv[2]);
#else // EEP_CONTENT_Manufacturing_Identification_Code_ITEM_LEN > 1
	#if EEP_CONTENT_Manufacturing_Identification_Code_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t Manufacturing_Identification_Code_val[EEP_CONTENT_Manufacturing_Identification_Code_ITEM_LEN];
	#elif EEP_CONTENT_Manufacturing_Identification_Code_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t Manufacturing_Identification_Code_val[EEP_CONTENT_Manufacturing_Identification_Code_ITEM_LEN];
	#else
	uint32_t Manufacturing_Identification_Code_val[EEP_CONTENT_Manufacturing_Identification_Code_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_Manufacturing_Identification_Code_ITEM_LEN; i ++ )
	{
		Manufacturing_Identification_Code_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_Manufacturing_Identification_Code_ITEM_LEN}	
	
	DevEep_Set_Manufacturing_Identification_Code(eep_handle,Manufacturing_Identification_Code_val);
}


#if EEP_CONTENT_Manufacturing_Identification_Code_ITEM_LEN == 1
#if EEP_CONTENT_Manufacturing_Identification_Code_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_Manufacturing_Identification_Code_Callback(EEP_HANDLE eep_handle,uint8_t const Manufacturing_Identification_Code_val)
#elif EEP_CONTENT_Manufacturing_Identification_Code_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_Manufacturing_Identification_Code_Callback(EEP_HANDLE eep_handle,uint16_t const Manufacturing_Identification_Code_val)
#else
void DevEepTest_Manufacturing_Identification_Code_Callback(EEP_HANDLE eep_handle,uint32_t const Manufacturing_Identification_Code_val)
#endif	
{
	
	printf("dev eep test: Manufacturing_Identification_Code, change to dec=%d,hex=0x%08x\r\n",Manufacturing_Identification_Code_val,Manufacturing_Identification_Code_val);
}
#else // EEP_CONTENT_Manufacturing_Identification_Code_ITEM_LEN > 1
#if EEP_CONTENT_Manufacturing_Identification_Code_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_Manufacturing_Identification_Code_Callback(EEP_HANDLE eep_handle,uint8_t const * Manufacturing_Identification_Code_val)
#elif EEP_CONTENT_Manufacturing_Identification_Code_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_Manufacturing_Identification_Code_Callback(EEP_HANDLE eep_handle,uint16_t const * Manufacturing_Identification_Code_val)
#else
void DevEepTest_Manufacturing_Identification_Code_Callback(EEP_HANDLE eep_handle,uint32_t const * Manufacturing_Identification_Code_val)
#endif
{
	printf("dev eep test: Manufacturing_Identification_Code, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_Manufacturing_Identification_Code_ITEM_LEN; i ++)
	{
		printf("%d ", Manufacturing_Identification_Code_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_Manufacturing_Identification_Code_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_Manufacturing_Identification_Code_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", Manufacturing_Identification_Code_val[i]);
		#elif EEP_CONTENT_Manufacturing_Identification_Code_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", Manufacturing_Identification_Code_val[i]);
		#else
		printf("%08x ", Manufacturing_Identification_Code_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_VDIAG(int argc, char **argv)
{
#if EEP_CONTENT_VDIAG_ITEM_LEN == 1
	#if EEP_CONTENT_VDIAG_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t VDIAG_val;
	#elif EEP_CONTENT_VDIAG_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t VDIAG_val;
	#else
	uint32_t VDIAG_val;
	#endif
	VDIAG_val = atoi(argv[2]);
#else // EEP_CONTENT_VDIAG_ITEM_LEN > 1
	#if EEP_CONTENT_VDIAG_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t VDIAG_val[EEP_CONTENT_VDIAG_ITEM_LEN];
	#elif EEP_CONTENT_VDIAG_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t VDIAG_val[EEP_CONTENT_VDIAG_ITEM_LEN];
	#else
	uint32_t VDIAG_val[EEP_CONTENT_VDIAG_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_VDIAG_ITEM_LEN; i ++ )
	{
		VDIAG_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_VDIAG_ITEM_LEN}	
	
	DevEep_Set_VDIAG(eep_handle,VDIAG_val);
}


#if EEP_CONTENT_VDIAG_ITEM_LEN == 1
#if EEP_CONTENT_VDIAG_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_VDIAG_Callback(EEP_HANDLE eep_handle,uint8_t const VDIAG_val)
#elif EEP_CONTENT_VDIAG_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_VDIAG_Callback(EEP_HANDLE eep_handle,uint16_t const VDIAG_val)
#else
void DevEepTest_VDIAG_Callback(EEP_HANDLE eep_handle,uint32_t const VDIAG_val)
#endif	
{
	
	printf("dev eep test: VDIAG, change to dec=%d,hex=0x%08x\r\n",VDIAG_val,VDIAG_val);
}
#else // EEP_CONTENT_VDIAG_ITEM_LEN > 1
#if EEP_CONTENT_VDIAG_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_VDIAG_Callback(EEP_HANDLE eep_handle,uint8_t const * VDIAG_val)
#elif EEP_CONTENT_VDIAG_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_VDIAG_Callback(EEP_HANDLE eep_handle,uint16_t const * VDIAG_val)
#else
void DevEepTest_VDIAG_Callback(EEP_HANDLE eep_handle,uint32_t const * VDIAG_val)
#endif
{
	printf("dev eep test: VDIAG, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_VDIAG_ITEM_LEN; i ++)
	{
		printf("%d ", VDIAG_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_VDIAG_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_VDIAG_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", VDIAG_val[i]);
		#elif EEP_CONTENT_VDIAG_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", VDIAG_val[i]);
		#else
		printf("%08x ", VDIAG_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_Config_EQ1(int argc, char **argv)
{
#if EEP_CONTENT_Config_EQ1_ITEM_LEN == 1
	#if EEP_CONTENT_Config_EQ1_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t Config_EQ1_val;
	#elif EEP_CONTENT_Config_EQ1_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t Config_EQ1_val;
	#else
	uint32_t Config_EQ1_val;
	#endif
	Config_EQ1_val = atoi(argv[2]);
#else // EEP_CONTENT_Config_EQ1_ITEM_LEN > 1
	#if EEP_CONTENT_Config_EQ1_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t Config_EQ1_val[EEP_CONTENT_Config_EQ1_ITEM_LEN];
	#elif EEP_CONTENT_Config_EQ1_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t Config_EQ1_val[EEP_CONTENT_Config_EQ1_ITEM_LEN];
	#else
	uint32_t Config_EQ1_val[EEP_CONTENT_Config_EQ1_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_Config_EQ1_ITEM_LEN; i ++ )
	{
		Config_EQ1_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_Config_EQ1_ITEM_LEN}	
	
	DevEep_Set_Config_EQ1(eep_handle,Config_EQ1_val);
}


#if EEP_CONTENT_Config_EQ1_ITEM_LEN == 1
#if EEP_CONTENT_Config_EQ1_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_Config_EQ1_Callback(EEP_HANDLE eep_handle,uint8_t const Config_EQ1_val)
#elif EEP_CONTENT_Config_EQ1_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_Config_EQ1_Callback(EEP_HANDLE eep_handle,uint16_t const Config_EQ1_val)
#else
void DevEepTest_Config_EQ1_Callback(EEP_HANDLE eep_handle,uint32_t const Config_EQ1_val)
#endif	
{
	
	printf("dev eep test: Config_EQ1, change to dec=%d,hex=0x%08x\r\n",Config_EQ1_val,Config_EQ1_val);
}
#else // EEP_CONTENT_Config_EQ1_ITEM_LEN > 1
#if EEP_CONTENT_Config_EQ1_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_Config_EQ1_Callback(EEP_HANDLE eep_handle,uint8_t const * Config_EQ1_val)
#elif EEP_CONTENT_Config_EQ1_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_Config_EQ1_Callback(EEP_HANDLE eep_handle,uint16_t const * Config_EQ1_val)
#else
void DevEepTest_Config_EQ1_Callback(EEP_HANDLE eep_handle,uint32_t const * Config_EQ1_val)
#endif
{
	printf("dev eep test: Config_EQ1, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_Config_EQ1_ITEM_LEN; i ++)
	{
		printf("%d ", Config_EQ1_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_Config_EQ1_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_Config_EQ1_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", Config_EQ1_val[i]);
		#elif EEP_CONTENT_Config_EQ1_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", Config_EQ1_val[i]);
		#else
		printf("%08x ", Config_EQ1_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_Vehicle_Type(int argc, char **argv)
{
#if EEP_CONTENT_Vehicle_Type_ITEM_LEN == 1
	#if EEP_CONTENT_Vehicle_Type_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t Vehicle_Type_val;
	#elif EEP_CONTENT_Vehicle_Type_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t Vehicle_Type_val;
	#else
	uint32_t Vehicle_Type_val;
	#endif
	Vehicle_Type_val = atoi(argv[2]);
#else // EEP_CONTENT_Vehicle_Type_ITEM_LEN > 1
	#if EEP_CONTENT_Vehicle_Type_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t Vehicle_Type_val[EEP_CONTENT_Vehicle_Type_ITEM_LEN];
	#elif EEP_CONTENT_Vehicle_Type_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t Vehicle_Type_val[EEP_CONTENT_Vehicle_Type_ITEM_LEN];
	#else
	uint32_t Vehicle_Type_val[EEP_CONTENT_Vehicle_Type_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_Vehicle_Type_ITEM_LEN; i ++ )
	{
		Vehicle_Type_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_Vehicle_Type_ITEM_LEN}	
	
	DevEep_Set_Vehicle_Type(eep_handle,Vehicle_Type_val);
}


#if EEP_CONTENT_Vehicle_Type_ITEM_LEN == 1
#if EEP_CONTENT_Vehicle_Type_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_Vehicle_Type_Callback(EEP_HANDLE eep_handle,uint8_t const Vehicle_Type_val)
#elif EEP_CONTENT_Vehicle_Type_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_Vehicle_Type_Callback(EEP_HANDLE eep_handle,uint16_t const Vehicle_Type_val)
#else
void DevEepTest_Vehicle_Type_Callback(EEP_HANDLE eep_handle,uint32_t const Vehicle_Type_val)
#endif	
{
	
	printf("dev eep test: Vehicle_Type, change to dec=%d,hex=0x%08x\r\n",Vehicle_Type_val,Vehicle_Type_val);
}
#else // EEP_CONTENT_Vehicle_Type_ITEM_LEN > 1
#if EEP_CONTENT_Vehicle_Type_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_Vehicle_Type_Callback(EEP_HANDLE eep_handle,uint8_t const * Vehicle_Type_val)
#elif EEP_CONTENT_Vehicle_Type_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_Vehicle_Type_Callback(EEP_HANDLE eep_handle,uint16_t const * Vehicle_Type_val)
#else
void DevEepTest_Vehicle_Type_Callback(EEP_HANDLE eep_handle,uint32_t const * Vehicle_Type_val)
#endif
{
	printf("dev eep test: Vehicle_Type, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_Vehicle_Type_ITEM_LEN; i ++)
	{
		printf("%d ", Vehicle_Type_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_Vehicle_Type_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_Vehicle_Type_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", Vehicle_Type_val[i]);
		#elif EEP_CONTENT_Vehicle_Type_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", Vehicle_Type_val[i]);
		#else
		printf("%08x ", Vehicle_Type_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_UUID(int argc, char **argv)
{
#if EEP_CONTENT_UUID_ITEM_LEN == 1
	#if EEP_CONTENT_UUID_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t UUID_val;
	#elif EEP_CONTENT_UUID_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t UUID_val;
	#else
	uint32_t UUID_val;
	#endif
	UUID_val = atoi(argv[2]);
#else // EEP_CONTENT_UUID_ITEM_LEN > 1
	#if EEP_CONTENT_UUID_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t UUID_val[EEP_CONTENT_UUID_ITEM_LEN];
	#elif EEP_CONTENT_UUID_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t UUID_val[EEP_CONTENT_UUID_ITEM_LEN];
	#else
	uint32_t UUID_val[EEP_CONTENT_UUID_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_UUID_ITEM_LEN; i ++ )
	{
		UUID_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_UUID_ITEM_LEN}	
	
	DevEep_Set_UUID(eep_handle,UUID_val);
}


#if EEP_CONTENT_UUID_ITEM_LEN == 1
#if EEP_CONTENT_UUID_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_UUID_Callback(EEP_HANDLE eep_handle,uint8_t const UUID_val)
#elif EEP_CONTENT_UUID_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_UUID_Callback(EEP_HANDLE eep_handle,uint16_t const UUID_val)
#else
void DevEepTest_UUID_Callback(EEP_HANDLE eep_handle,uint32_t const UUID_val)
#endif	
{
	
	printf("dev eep test: UUID, change to dec=%d,hex=0x%08x\r\n",UUID_val,UUID_val);
}
#else // EEP_CONTENT_UUID_ITEM_LEN > 1
#if EEP_CONTENT_UUID_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_UUID_Callback(EEP_HANDLE eep_handle,uint8_t const * UUID_val)
#elif EEP_CONTENT_UUID_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_UUID_Callback(EEP_HANDLE eep_handle,uint16_t const * UUID_val)
#else
void DevEepTest_UUID_Callback(EEP_HANDLE eep_handle,uint32_t const * UUID_val)
#endif
{
	printf("dev eep test: UUID, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_UUID_ITEM_LEN; i ++)
	{
		printf("%d ", UUID_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_UUID_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_UUID_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", UUID_val[i]);
		#elif EEP_CONTENT_UUID_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", UUID_val[i]);
		#else
		printf("%08x ", UUID_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_NAVI_ID(int argc, char **argv)
{
#if EEP_CONTENT_NAVI_ID_ITEM_LEN == 1
	#if EEP_CONTENT_NAVI_ID_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t NAVI_ID_val;
	#elif EEP_CONTENT_NAVI_ID_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t NAVI_ID_val;
	#else
	uint32_t NAVI_ID_val;
	#endif
	NAVI_ID_val = atoi(argv[2]);
#else // EEP_CONTENT_NAVI_ID_ITEM_LEN > 1
	#if EEP_CONTENT_NAVI_ID_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t NAVI_ID_val[EEP_CONTENT_NAVI_ID_ITEM_LEN];
	#elif EEP_CONTENT_NAVI_ID_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t NAVI_ID_val[EEP_CONTENT_NAVI_ID_ITEM_LEN];
	#else
	uint32_t NAVI_ID_val[EEP_CONTENT_NAVI_ID_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_NAVI_ID_ITEM_LEN; i ++ )
	{
		NAVI_ID_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_NAVI_ID_ITEM_LEN}	
	
	DevEep_Set_NAVI_ID(eep_handle,NAVI_ID_val);
}


#if EEP_CONTENT_NAVI_ID_ITEM_LEN == 1
#if EEP_CONTENT_NAVI_ID_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_NAVI_ID_Callback(EEP_HANDLE eep_handle,uint8_t const NAVI_ID_val)
#elif EEP_CONTENT_NAVI_ID_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_NAVI_ID_Callback(EEP_HANDLE eep_handle,uint16_t const NAVI_ID_val)
#else
void DevEepTest_NAVI_ID_Callback(EEP_HANDLE eep_handle,uint32_t const NAVI_ID_val)
#endif	
{
	
	printf("dev eep test: NAVI_ID, change to dec=%d,hex=0x%08x\r\n",NAVI_ID_val,NAVI_ID_val);
}
#else // EEP_CONTENT_NAVI_ID_ITEM_LEN > 1
#if EEP_CONTENT_NAVI_ID_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_NAVI_ID_Callback(EEP_HANDLE eep_handle,uint8_t const * NAVI_ID_val)
#elif EEP_CONTENT_NAVI_ID_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_NAVI_ID_Callback(EEP_HANDLE eep_handle,uint16_t const * NAVI_ID_val)
#else
void DevEepTest_NAVI_ID_Callback(EEP_HANDLE eep_handle,uint32_t const * NAVI_ID_val)
#endif
{
	printf("dev eep test: NAVI_ID, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_NAVI_ID_ITEM_LEN; i ++)
	{
		printf("%d ", NAVI_ID_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_NAVI_ID_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_NAVI_ID_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", NAVI_ID_val[i]);
		#elif EEP_CONTENT_NAVI_ID_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", NAVI_ID_val[i]);
		#else
		printf("%08x ", NAVI_ID_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_DA_ID(int argc, char **argv)
{
#if EEP_CONTENT_DA_ID_ITEM_LEN == 1
	#if EEP_CONTENT_DA_ID_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t DA_ID_val;
	#elif EEP_CONTENT_DA_ID_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t DA_ID_val;
	#else
	uint32_t DA_ID_val;
	#endif
	DA_ID_val = atoi(argv[2]);
#else // EEP_CONTENT_DA_ID_ITEM_LEN > 1
	#if EEP_CONTENT_DA_ID_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t DA_ID_val[EEP_CONTENT_DA_ID_ITEM_LEN];
	#elif EEP_CONTENT_DA_ID_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t DA_ID_val[EEP_CONTENT_DA_ID_ITEM_LEN];
	#else
	uint32_t DA_ID_val[EEP_CONTENT_DA_ID_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_DA_ID_ITEM_LEN; i ++ )
	{
		DA_ID_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_DA_ID_ITEM_LEN}	
	
	DevEep_Set_DA_ID(eep_handle,DA_ID_val);
}


#if EEP_CONTENT_DA_ID_ITEM_LEN == 1
#if EEP_CONTENT_DA_ID_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_DA_ID_Callback(EEP_HANDLE eep_handle,uint8_t const DA_ID_val)
#elif EEP_CONTENT_DA_ID_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_DA_ID_Callback(EEP_HANDLE eep_handle,uint16_t const DA_ID_val)
#else
void DevEepTest_DA_ID_Callback(EEP_HANDLE eep_handle,uint32_t const DA_ID_val)
#endif	
{
	
	printf("dev eep test: DA_ID, change to dec=%d,hex=0x%08x\r\n",DA_ID_val,DA_ID_val);
}
#else // EEP_CONTENT_DA_ID_ITEM_LEN > 1
#if EEP_CONTENT_DA_ID_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_DA_ID_Callback(EEP_HANDLE eep_handle,uint8_t const * DA_ID_val)
#elif EEP_CONTENT_DA_ID_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_DA_ID_Callback(EEP_HANDLE eep_handle,uint16_t const * DA_ID_val)
#else
void DevEepTest_DA_ID_Callback(EEP_HANDLE eep_handle,uint32_t const * DA_ID_val)
#endif
{
	printf("dev eep test: DA_ID, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_DA_ID_ITEM_LEN; i ++)
	{
		printf("%d ", DA_ID_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_DA_ID_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_DA_ID_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", DA_ID_val[i]);
		#elif EEP_CONTENT_DA_ID_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", DA_ID_val[i]);
		#else
		printf("%08x ", DA_ID_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_DAMainBoardHwVer(int argc, char **argv)
{
#if EEP_CONTENT_DAMainBoardHwVer_ITEM_LEN == 1
	#if EEP_CONTENT_DAMainBoardHwVer_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t DAMainBoardHwVer_val;
	#elif EEP_CONTENT_DAMainBoardHwVer_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t DAMainBoardHwVer_val;
	#else
	uint32_t DAMainBoardHwVer_val;
	#endif
	DAMainBoardHwVer_val = atoi(argv[2]);
#else // EEP_CONTENT_DAMainBoardHwVer_ITEM_LEN > 1
	#if EEP_CONTENT_DAMainBoardHwVer_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t DAMainBoardHwVer_val[EEP_CONTENT_DAMainBoardHwVer_ITEM_LEN];
	#elif EEP_CONTENT_DAMainBoardHwVer_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t DAMainBoardHwVer_val[EEP_CONTENT_DAMainBoardHwVer_ITEM_LEN];
	#else
	uint32_t DAMainBoardHwVer_val[EEP_CONTENT_DAMainBoardHwVer_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_DAMainBoardHwVer_ITEM_LEN; i ++ )
	{
		DAMainBoardHwVer_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_DAMainBoardHwVer_ITEM_LEN}	
	
	DevEep_Set_DAMainBoardHwVer(eep_handle,DAMainBoardHwVer_val);
}


#if EEP_CONTENT_DAMainBoardHwVer_ITEM_LEN == 1
#if EEP_CONTENT_DAMainBoardHwVer_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_DAMainBoardHwVer_Callback(EEP_HANDLE eep_handle,uint8_t const DAMainBoardHwVer_val)
#elif EEP_CONTENT_DAMainBoardHwVer_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_DAMainBoardHwVer_Callback(EEP_HANDLE eep_handle,uint16_t const DAMainBoardHwVer_val)
#else
void DevEepTest_DAMainBoardHwVer_Callback(EEP_HANDLE eep_handle,uint32_t const DAMainBoardHwVer_val)
#endif	
{
	
	printf("dev eep test: DAMainBoardHwVer, change to dec=%d,hex=0x%08x\r\n",DAMainBoardHwVer_val,DAMainBoardHwVer_val);
}
#else // EEP_CONTENT_DAMainBoardHwVer_ITEM_LEN > 1
#if EEP_CONTENT_DAMainBoardHwVer_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_DAMainBoardHwVer_Callback(EEP_HANDLE eep_handle,uint8_t const * DAMainBoardHwVer_val)
#elif EEP_CONTENT_DAMainBoardHwVer_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_DAMainBoardHwVer_Callback(EEP_HANDLE eep_handle,uint16_t const * DAMainBoardHwVer_val)
#else
void DevEepTest_DAMainBoardHwVer_Callback(EEP_HANDLE eep_handle,uint32_t const * DAMainBoardHwVer_val)
#endif
{
	printf("dev eep test: DAMainBoardHwVer, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_DAMainBoardHwVer_ITEM_LEN; i ++)
	{
		printf("%d ", DAMainBoardHwVer_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_DAMainBoardHwVer_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_DAMainBoardHwVer_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", DAMainBoardHwVer_val[i]);
		#elif EEP_CONTENT_DAMainBoardHwVer_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", DAMainBoardHwVer_val[i]);
		#else
		printf("%08x ", DAMainBoardHwVer_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_SW_Version_Date_Format(int argc, char **argv)
{
#if EEP_CONTENT_SW_Version_Date_Format_ITEM_LEN == 1
	#if EEP_CONTENT_SW_Version_Date_Format_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t SW_Version_Date_Format_val;
	#elif EEP_CONTENT_SW_Version_Date_Format_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t SW_Version_Date_Format_val;
	#else
	uint32_t SW_Version_Date_Format_val;
	#endif
	SW_Version_Date_Format_val = atoi(argv[2]);
#else // EEP_CONTENT_SW_Version_Date_Format_ITEM_LEN > 1
	#if EEP_CONTENT_SW_Version_Date_Format_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t SW_Version_Date_Format_val[EEP_CONTENT_SW_Version_Date_Format_ITEM_LEN];
	#elif EEP_CONTENT_SW_Version_Date_Format_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t SW_Version_Date_Format_val[EEP_CONTENT_SW_Version_Date_Format_ITEM_LEN];
	#else
	uint32_t SW_Version_Date_Format_val[EEP_CONTENT_SW_Version_Date_Format_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_SW_Version_Date_Format_ITEM_LEN; i ++ )
	{
		SW_Version_Date_Format_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_SW_Version_Date_Format_ITEM_LEN}	
	
	DevEep_Set_SW_Version_Date_Format(eep_handle,SW_Version_Date_Format_val);
}


#if EEP_CONTENT_SW_Version_Date_Format_ITEM_LEN == 1
#if EEP_CONTENT_SW_Version_Date_Format_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_SW_Version_Date_Format_Callback(EEP_HANDLE eep_handle,uint8_t const SW_Version_Date_Format_val)
#elif EEP_CONTENT_SW_Version_Date_Format_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_SW_Version_Date_Format_Callback(EEP_HANDLE eep_handle,uint16_t const SW_Version_Date_Format_val)
#else
void DevEepTest_SW_Version_Date_Format_Callback(EEP_HANDLE eep_handle,uint32_t const SW_Version_Date_Format_val)
#endif	
{
	
	printf("dev eep test: SW_Version_Date_Format, change to dec=%d,hex=0x%08x\r\n",SW_Version_Date_Format_val,SW_Version_Date_Format_val);
}
#else // EEP_CONTENT_SW_Version_Date_Format_ITEM_LEN > 1
#if EEP_CONTENT_SW_Version_Date_Format_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_SW_Version_Date_Format_Callback(EEP_HANDLE eep_handle,uint8_t const * SW_Version_Date_Format_val)
#elif EEP_CONTENT_SW_Version_Date_Format_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_SW_Version_Date_Format_Callback(EEP_HANDLE eep_handle,uint16_t const * SW_Version_Date_Format_val)
#else
void DevEepTest_SW_Version_Date_Format_Callback(EEP_HANDLE eep_handle,uint32_t const * SW_Version_Date_Format_val)
#endif
{
	printf("dev eep test: SW_Version_Date_Format, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_SW_Version_Date_Format_ITEM_LEN; i ++)
	{
		printf("%d ", SW_Version_Date_Format_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_SW_Version_Date_Format_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_SW_Version_Date_Format_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", SW_Version_Date_Format_val[i]);
		#elif EEP_CONTENT_SW_Version_Date_Format_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", SW_Version_Date_Format_val[i]);
		#else
		printf("%08x ", SW_Version_Date_Format_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_SW_Edition_Version(int argc, char **argv)
{
#if EEP_CONTENT_SW_Edition_Version_ITEM_LEN == 1
	#if EEP_CONTENT_SW_Edition_Version_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t SW_Edition_Version_val;
	#elif EEP_CONTENT_SW_Edition_Version_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t SW_Edition_Version_val;
	#else
	uint32_t SW_Edition_Version_val;
	#endif
	SW_Edition_Version_val = atoi(argv[2]);
#else // EEP_CONTENT_SW_Edition_Version_ITEM_LEN > 1
	#if EEP_CONTENT_SW_Edition_Version_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t SW_Edition_Version_val[EEP_CONTENT_SW_Edition_Version_ITEM_LEN];
	#elif EEP_CONTENT_SW_Edition_Version_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t SW_Edition_Version_val[EEP_CONTENT_SW_Edition_Version_ITEM_LEN];
	#else
	uint32_t SW_Edition_Version_val[EEP_CONTENT_SW_Edition_Version_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_SW_Edition_Version_ITEM_LEN; i ++ )
	{
		SW_Edition_Version_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_SW_Edition_Version_ITEM_LEN}	
	
	DevEep_Set_SW_Edition_Version(eep_handle,SW_Edition_Version_val);
}


#if EEP_CONTENT_SW_Edition_Version_ITEM_LEN == 1
#if EEP_CONTENT_SW_Edition_Version_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_SW_Edition_Version_Callback(EEP_HANDLE eep_handle,uint8_t const SW_Edition_Version_val)
#elif EEP_CONTENT_SW_Edition_Version_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_SW_Edition_Version_Callback(EEP_HANDLE eep_handle,uint16_t const SW_Edition_Version_val)
#else
void DevEepTest_SW_Edition_Version_Callback(EEP_HANDLE eep_handle,uint32_t const SW_Edition_Version_val)
#endif	
{
	
	printf("dev eep test: SW_Edition_Version, change to dec=%d,hex=0x%08x\r\n",SW_Edition_Version_val,SW_Edition_Version_val);
}
#else // EEP_CONTENT_SW_Edition_Version_ITEM_LEN > 1
#if EEP_CONTENT_SW_Edition_Version_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_SW_Edition_Version_Callback(EEP_HANDLE eep_handle,uint8_t const * SW_Edition_Version_val)
#elif EEP_CONTENT_SW_Edition_Version_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_SW_Edition_Version_Callback(EEP_HANDLE eep_handle,uint16_t const * SW_Edition_Version_val)
#else
void DevEepTest_SW_Edition_Version_Callback(EEP_HANDLE eep_handle,uint32_t const * SW_Edition_Version_val)
#endif
{
	printf("dev eep test: SW_Edition_Version, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_SW_Edition_Version_ITEM_LEN; i ++)
	{
		printf("%d ", SW_Edition_Version_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_SW_Edition_Version_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_SW_Edition_Version_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", SW_Edition_Version_val[i]);
		#elif EEP_CONTENT_SW_Edition_Version_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", SW_Edition_Version_val[i]);
		#else
		printf("%08x ", SW_Edition_Version_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_VehicleManufacturerSparePartNumber_Nissan(int argc, char **argv)
{
#if EEP_CONTENT_VehicleManufacturerSparePartNumber_Nissan_ITEM_LEN == 1
	#if EEP_CONTENT_VehicleManufacturerSparePartNumber_Nissan_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t VehicleManufacturerSparePartNumber_Nissan_val;
	#elif EEP_CONTENT_VehicleManufacturerSparePartNumber_Nissan_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t VehicleManufacturerSparePartNumber_Nissan_val;
	#else
	uint32_t VehicleManufacturerSparePartNumber_Nissan_val;
	#endif
	VehicleManufacturerSparePartNumber_Nissan_val = atoi(argv[2]);
#else // EEP_CONTENT_VehicleManufacturerSparePartNumber_Nissan_ITEM_LEN > 1
	#if EEP_CONTENT_VehicleManufacturerSparePartNumber_Nissan_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t VehicleManufacturerSparePartNumber_Nissan_val[EEP_CONTENT_VehicleManufacturerSparePartNumber_Nissan_ITEM_LEN];
	#elif EEP_CONTENT_VehicleManufacturerSparePartNumber_Nissan_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t VehicleManufacturerSparePartNumber_Nissan_val[EEP_CONTENT_VehicleManufacturerSparePartNumber_Nissan_ITEM_LEN];
	#else
	uint32_t VehicleManufacturerSparePartNumber_Nissan_val[EEP_CONTENT_VehicleManufacturerSparePartNumber_Nissan_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_VehicleManufacturerSparePartNumber_Nissan_ITEM_LEN; i ++ )
	{
		VehicleManufacturerSparePartNumber_Nissan_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_VehicleManufacturerSparePartNumber_Nissan_ITEM_LEN}	
	
	DevEep_Set_VehicleManufacturerSparePartNumber_Nissan(eep_handle,VehicleManufacturerSparePartNumber_Nissan_val);
}


#if EEP_CONTENT_VehicleManufacturerSparePartNumber_Nissan_ITEM_LEN == 1
#if EEP_CONTENT_VehicleManufacturerSparePartNumber_Nissan_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_VehicleManufacturerSparePartNumber_Nissan_Callback(EEP_HANDLE eep_handle,uint8_t const VehicleManufacturerSparePartNumber_Nissan_val)
#elif EEP_CONTENT_VehicleManufacturerSparePartNumber_Nissan_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_VehicleManufacturerSparePartNumber_Nissan_Callback(EEP_HANDLE eep_handle,uint16_t const VehicleManufacturerSparePartNumber_Nissan_val)
#else
void DevEepTest_VehicleManufacturerSparePartNumber_Nissan_Callback(EEP_HANDLE eep_handle,uint32_t const VehicleManufacturerSparePartNumber_Nissan_val)
#endif	
{
	
	printf("dev eep test: VehicleManufacturerSparePartNumber_Nissan, change to dec=%d,hex=0x%08x\r\n",VehicleManufacturerSparePartNumber_Nissan_val,VehicleManufacturerSparePartNumber_Nissan_val);
}
#else // EEP_CONTENT_VehicleManufacturerSparePartNumber_Nissan_ITEM_LEN > 1
#if EEP_CONTENT_VehicleManufacturerSparePartNumber_Nissan_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_VehicleManufacturerSparePartNumber_Nissan_Callback(EEP_HANDLE eep_handle,uint8_t const * VehicleManufacturerSparePartNumber_Nissan_val)
#elif EEP_CONTENT_VehicleManufacturerSparePartNumber_Nissan_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_VehicleManufacturerSparePartNumber_Nissan_Callback(EEP_HANDLE eep_handle,uint16_t const * VehicleManufacturerSparePartNumber_Nissan_val)
#else
void DevEepTest_VehicleManufacturerSparePartNumber_Nissan_Callback(EEP_HANDLE eep_handle,uint32_t const * VehicleManufacturerSparePartNumber_Nissan_val)
#endif
{
	printf("dev eep test: VehicleManufacturerSparePartNumber_Nissan, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_VehicleManufacturerSparePartNumber_Nissan_ITEM_LEN; i ++)
	{
		printf("%d ", VehicleManufacturerSparePartNumber_Nissan_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_VehicleManufacturerSparePartNumber_Nissan_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_VehicleManufacturerSparePartNumber_Nissan_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", VehicleManufacturerSparePartNumber_Nissan_val[i]);
		#elif EEP_CONTENT_VehicleManufacturerSparePartNumber_Nissan_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", VehicleManufacturerSparePartNumber_Nissan_val[i]);
		#else
		printf("%08x ", VehicleManufacturerSparePartNumber_Nissan_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_VisteonProductPartNumber(int argc, char **argv)
{
#if EEP_CONTENT_VisteonProductPartNumber_ITEM_LEN == 1
	#if EEP_CONTENT_VisteonProductPartNumber_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t VisteonProductPartNumber_val;
	#elif EEP_CONTENT_VisteonProductPartNumber_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t VisteonProductPartNumber_val;
	#else
	uint32_t VisteonProductPartNumber_val;
	#endif
	VisteonProductPartNumber_val = atoi(argv[2]);
#else // EEP_CONTENT_VisteonProductPartNumber_ITEM_LEN > 1
	#if EEP_CONTENT_VisteonProductPartNumber_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t VisteonProductPartNumber_val[EEP_CONTENT_VisteonProductPartNumber_ITEM_LEN];
	#elif EEP_CONTENT_VisteonProductPartNumber_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t VisteonProductPartNumber_val[EEP_CONTENT_VisteonProductPartNumber_ITEM_LEN];
	#else
	uint32_t VisteonProductPartNumber_val[EEP_CONTENT_VisteonProductPartNumber_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_VisteonProductPartNumber_ITEM_LEN; i ++ )
	{
		VisteonProductPartNumber_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_VisteonProductPartNumber_ITEM_LEN}	
	
	DevEep_Set_VisteonProductPartNumber(eep_handle,VisteonProductPartNumber_val);
}


#if EEP_CONTENT_VisteonProductPartNumber_ITEM_LEN == 1
#if EEP_CONTENT_VisteonProductPartNumber_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_VisteonProductPartNumber_Callback(EEP_HANDLE eep_handle,uint8_t const VisteonProductPartNumber_val)
#elif EEP_CONTENT_VisteonProductPartNumber_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_VisteonProductPartNumber_Callback(EEP_HANDLE eep_handle,uint16_t const VisteonProductPartNumber_val)
#else
void DevEepTest_VisteonProductPartNumber_Callback(EEP_HANDLE eep_handle,uint32_t const VisteonProductPartNumber_val)
#endif	
{
	
	printf("dev eep test: VisteonProductPartNumber, change to dec=%d,hex=0x%08x\r\n",VisteonProductPartNumber_val,VisteonProductPartNumber_val);
}
#else // EEP_CONTENT_VisteonProductPartNumber_ITEM_LEN > 1
#if EEP_CONTENT_VisteonProductPartNumber_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_VisteonProductPartNumber_Callback(EEP_HANDLE eep_handle,uint8_t const * VisteonProductPartNumber_val)
#elif EEP_CONTENT_VisteonProductPartNumber_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_VisteonProductPartNumber_Callback(EEP_HANDLE eep_handle,uint16_t const * VisteonProductPartNumber_val)
#else
void DevEepTest_VisteonProductPartNumber_Callback(EEP_HANDLE eep_handle,uint32_t const * VisteonProductPartNumber_val)
#endif
{
	printf("dev eep test: VisteonProductPartNumber, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_VisteonProductPartNumber_ITEM_LEN; i ++)
	{
		printf("%d ", VisteonProductPartNumber_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_VisteonProductPartNumber_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_VisteonProductPartNumber_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", VisteonProductPartNumber_val[i]);
		#elif EEP_CONTENT_VisteonProductPartNumber_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", VisteonProductPartNumber_val[i]);
		#else
		printf("%08x ", VisteonProductPartNumber_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_EquippedPCBVisteonPartNumMainBorad(int argc, char **argv)
{
#if EEP_CONTENT_EquippedPCBVisteonPartNumMainBorad_ITEM_LEN == 1
	#if EEP_CONTENT_EquippedPCBVisteonPartNumMainBorad_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t EquippedPCBVisteonPartNumMainBorad_val;
	#elif EEP_CONTENT_EquippedPCBVisteonPartNumMainBorad_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t EquippedPCBVisteonPartNumMainBorad_val;
	#else
	uint32_t EquippedPCBVisteonPartNumMainBorad_val;
	#endif
	EquippedPCBVisteonPartNumMainBorad_val = atoi(argv[2]);
#else // EEP_CONTENT_EquippedPCBVisteonPartNumMainBorad_ITEM_LEN > 1
	#if EEP_CONTENT_EquippedPCBVisteonPartNumMainBorad_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t EquippedPCBVisteonPartNumMainBorad_val[EEP_CONTENT_EquippedPCBVisteonPartNumMainBorad_ITEM_LEN];
	#elif EEP_CONTENT_EquippedPCBVisteonPartNumMainBorad_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t EquippedPCBVisteonPartNumMainBorad_val[EEP_CONTENT_EquippedPCBVisteonPartNumMainBorad_ITEM_LEN];
	#else
	uint32_t EquippedPCBVisteonPartNumMainBorad_val[EEP_CONTENT_EquippedPCBVisteonPartNumMainBorad_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_EquippedPCBVisteonPartNumMainBorad_ITEM_LEN; i ++ )
	{
		EquippedPCBVisteonPartNumMainBorad_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_EquippedPCBVisteonPartNumMainBorad_ITEM_LEN}	
	
	DevEep_Set_EquippedPCBVisteonPartNumMainBorad(eep_handle,EquippedPCBVisteonPartNumMainBorad_val);
}


#if EEP_CONTENT_EquippedPCBVisteonPartNumMainBorad_ITEM_LEN == 1
#if EEP_CONTENT_EquippedPCBVisteonPartNumMainBorad_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_EquippedPCBVisteonPartNumMainBorad_Callback(EEP_HANDLE eep_handle,uint8_t const EquippedPCBVisteonPartNumMainBorad_val)
#elif EEP_CONTENT_EquippedPCBVisteonPartNumMainBorad_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_EquippedPCBVisteonPartNumMainBorad_Callback(EEP_HANDLE eep_handle,uint16_t const EquippedPCBVisteonPartNumMainBorad_val)
#else
void DevEepTest_EquippedPCBVisteonPartNumMainBorad_Callback(EEP_HANDLE eep_handle,uint32_t const EquippedPCBVisteonPartNumMainBorad_val)
#endif	
{
	
	printf("dev eep test: EquippedPCBVisteonPartNumMainBorad, change to dec=%d,hex=0x%08x\r\n",EquippedPCBVisteonPartNumMainBorad_val,EquippedPCBVisteonPartNumMainBorad_val);
}
#else // EEP_CONTENT_EquippedPCBVisteonPartNumMainBorad_ITEM_LEN > 1
#if EEP_CONTENT_EquippedPCBVisteonPartNumMainBorad_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_EquippedPCBVisteonPartNumMainBorad_Callback(EEP_HANDLE eep_handle,uint8_t const * EquippedPCBVisteonPartNumMainBorad_val)
#elif EEP_CONTENT_EquippedPCBVisteonPartNumMainBorad_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_EquippedPCBVisteonPartNumMainBorad_Callback(EEP_HANDLE eep_handle,uint16_t const * EquippedPCBVisteonPartNumMainBorad_val)
#else
void DevEepTest_EquippedPCBVisteonPartNumMainBorad_Callback(EEP_HANDLE eep_handle,uint32_t const * EquippedPCBVisteonPartNumMainBorad_val)
#endif
{
	printf("dev eep test: EquippedPCBVisteonPartNumMainBorad, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_EquippedPCBVisteonPartNumMainBorad_ITEM_LEN; i ++)
	{
		printf("%d ", EquippedPCBVisteonPartNumMainBorad_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_EquippedPCBVisteonPartNumMainBorad_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_EquippedPCBVisteonPartNumMainBorad_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", EquippedPCBVisteonPartNumMainBorad_val[i]);
		#elif EEP_CONTENT_EquippedPCBVisteonPartNumMainBorad_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", EquippedPCBVisteonPartNumMainBorad_val[i]);
		#else
		printf("%08x ", EquippedPCBVisteonPartNumMainBorad_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_EquippedPCBVisteonPartNumSubBorad(int argc, char **argv)
{
#if EEP_CONTENT_EquippedPCBVisteonPartNumSubBorad_ITEM_LEN == 1
	#if EEP_CONTENT_EquippedPCBVisteonPartNumSubBorad_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t EquippedPCBVisteonPartNumSubBorad_val;
	#elif EEP_CONTENT_EquippedPCBVisteonPartNumSubBorad_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t EquippedPCBVisteonPartNumSubBorad_val;
	#else
	uint32_t EquippedPCBVisteonPartNumSubBorad_val;
	#endif
	EquippedPCBVisteonPartNumSubBorad_val = atoi(argv[2]);
#else // EEP_CONTENT_EquippedPCBVisteonPartNumSubBorad_ITEM_LEN > 1
	#if EEP_CONTENT_EquippedPCBVisteonPartNumSubBorad_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t EquippedPCBVisteonPartNumSubBorad_val[EEP_CONTENT_EquippedPCBVisteonPartNumSubBorad_ITEM_LEN];
	#elif EEP_CONTENT_EquippedPCBVisteonPartNumSubBorad_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t EquippedPCBVisteonPartNumSubBorad_val[EEP_CONTENT_EquippedPCBVisteonPartNumSubBorad_ITEM_LEN];
	#else
	uint32_t EquippedPCBVisteonPartNumSubBorad_val[EEP_CONTENT_EquippedPCBVisteonPartNumSubBorad_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_EquippedPCBVisteonPartNumSubBorad_ITEM_LEN; i ++ )
	{
		EquippedPCBVisteonPartNumSubBorad_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_EquippedPCBVisteonPartNumSubBorad_ITEM_LEN}	
	
	DevEep_Set_EquippedPCBVisteonPartNumSubBorad(eep_handle,EquippedPCBVisteonPartNumSubBorad_val);
}


#if EEP_CONTENT_EquippedPCBVisteonPartNumSubBorad_ITEM_LEN == 1
#if EEP_CONTENT_EquippedPCBVisteonPartNumSubBorad_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_EquippedPCBVisteonPartNumSubBorad_Callback(EEP_HANDLE eep_handle,uint8_t const EquippedPCBVisteonPartNumSubBorad_val)
#elif EEP_CONTENT_EquippedPCBVisteonPartNumSubBorad_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_EquippedPCBVisteonPartNumSubBorad_Callback(EEP_HANDLE eep_handle,uint16_t const EquippedPCBVisteonPartNumSubBorad_val)
#else
void DevEepTest_EquippedPCBVisteonPartNumSubBorad_Callback(EEP_HANDLE eep_handle,uint32_t const EquippedPCBVisteonPartNumSubBorad_val)
#endif	
{
	
	printf("dev eep test: EquippedPCBVisteonPartNumSubBorad, change to dec=%d,hex=0x%08x\r\n",EquippedPCBVisteonPartNumSubBorad_val,EquippedPCBVisteonPartNumSubBorad_val);
}
#else // EEP_CONTENT_EquippedPCBVisteonPartNumSubBorad_ITEM_LEN > 1
#if EEP_CONTENT_EquippedPCBVisteonPartNumSubBorad_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_EquippedPCBVisteonPartNumSubBorad_Callback(EEP_HANDLE eep_handle,uint8_t const * EquippedPCBVisteonPartNumSubBorad_val)
#elif EEP_CONTENT_EquippedPCBVisteonPartNumSubBorad_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_EquippedPCBVisteonPartNumSubBorad_Callback(EEP_HANDLE eep_handle,uint16_t const * EquippedPCBVisteonPartNumSubBorad_val)
#else
void DevEepTest_EquippedPCBVisteonPartNumSubBorad_Callback(EEP_HANDLE eep_handle,uint32_t const * EquippedPCBVisteonPartNumSubBorad_val)
#endif
{
	printf("dev eep test: EquippedPCBVisteonPartNumSubBorad, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_EquippedPCBVisteonPartNumSubBorad_ITEM_LEN; i ++)
	{
		printf("%d ", EquippedPCBVisteonPartNumSubBorad_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_EquippedPCBVisteonPartNumSubBorad_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_EquippedPCBVisteonPartNumSubBorad_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", EquippedPCBVisteonPartNumSubBorad_val[i]);
		#elif EEP_CONTENT_EquippedPCBVisteonPartNumSubBorad_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", EquippedPCBVisteonPartNumSubBorad_val[i]);
		#else
		printf("%08x ", EquippedPCBVisteonPartNumSubBorad_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_DAUniqueID(int argc, char **argv)
{
#if EEP_CONTENT_DAUniqueID_ITEM_LEN == 1
	#if EEP_CONTENT_DAUniqueID_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t DAUniqueID_val;
	#elif EEP_CONTENT_DAUniqueID_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t DAUniqueID_val;
	#else
	uint32_t DAUniqueID_val;
	#endif
	DAUniqueID_val = atoi(argv[2]);
#else // EEP_CONTENT_DAUniqueID_ITEM_LEN > 1
	#if EEP_CONTENT_DAUniqueID_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t DAUniqueID_val[EEP_CONTENT_DAUniqueID_ITEM_LEN];
	#elif EEP_CONTENT_DAUniqueID_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t DAUniqueID_val[EEP_CONTENT_DAUniqueID_ITEM_LEN];
	#else
	uint32_t DAUniqueID_val[EEP_CONTENT_DAUniqueID_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_DAUniqueID_ITEM_LEN; i ++ )
	{
		DAUniqueID_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_DAUniqueID_ITEM_LEN}	
	
	DevEep_Set_DAUniqueID(eep_handle,DAUniqueID_val);
}


#if EEP_CONTENT_DAUniqueID_ITEM_LEN == 1
#if EEP_CONTENT_DAUniqueID_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_DAUniqueID_Callback(EEP_HANDLE eep_handle,uint8_t const DAUniqueID_val)
#elif EEP_CONTENT_DAUniqueID_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_DAUniqueID_Callback(EEP_HANDLE eep_handle,uint16_t const DAUniqueID_val)
#else
void DevEepTest_DAUniqueID_Callback(EEP_HANDLE eep_handle,uint32_t const DAUniqueID_val)
#endif	
{
	
	printf("dev eep test: DAUniqueID, change to dec=%d,hex=0x%08x\r\n",DAUniqueID_val,DAUniqueID_val);
}
#else // EEP_CONTENT_DAUniqueID_ITEM_LEN > 1
#if EEP_CONTENT_DAUniqueID_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_DAUniqueID_Callback(EEP_HANDLE eep_handle,uint8_t const * DAUniqueID_val)
#elif EEP_CONTENT_DAUniqueID_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_DAUniqueID_Callback(EEP_HANDLE eep_handle,uint16_t const * DAUniqueID_val)
#else
void DevEepTest_DAUniqueID_Callback(EEP_HANDLE eep_handle,uint32_t const * DAUniqueID_val)
#endif
{
	printf("dev eep test: DAUniqueID, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_DAUniqueID_ITEM_LEN; i ++)
	{
		printf("%d ", DAUniqueID_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_DAUniqueID_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_DAUniqueID_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", DAUniqueID_val[i]);
		#elif EEP_CONTENT_DAUniqueID_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", DAUniqueID_val[i]);
		#else
		printf("%08x ", DAUniqueID_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_ProductSerialNum(int argc, char **argv)
{
#if EEP_CONTENT_ProductSerialNum_ITEM_LEN == 1
	#if EEP_CONTENT_ProductSerialNum_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t ProductSerialNum_val;
	#elif EEP_CONTENT_ProductSerialNum_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t ProductSerialNum_val;
	#else
	uint32_t ProductSerialNum_val;
	#endif
	ProductSerialNum_val = atoi(argv[2]);
#else // EEP_CONTENT_ProductSerialNum_ITEM_LEN > 1
	#if EEP_CONTENT_ProductSerialNum_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t ProductSerialNum_val[EEP_CONTENT_ProductSerialNum_ITEM_LEN];
	#elif EEP_CONTENT_ProductSerialNum_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t ProductSerialNum_val[EEP_CONTENT_ProductSerialNum_ITEM_LEN];
	#else
	uint32_t ProductSerialNum_val[EEP_CONTENT_ProductSerialNum_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_ProductSerialNum_ITEM_LEN; i ++ )
	{
		ProductSerialNum_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_ProductSerialNum_ITEM_LEN}	
	
	DevEep_Set_ProductSerialNum(eep_handle,ProductSerialNum_val);
}


#if EEP_CONTENT_ProductSerialNum_ITEM_LEN == 1
#if EEP_CONTENT_ProductSerialNum_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_ProductSerialNum_Callback(EEP_HANDLE eep_handle,uint8_t const ProductSerialNum_val)
#elif EEP_CONTENT_ProductSerialNum_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_ProductSerialNum_Callback(EEP_HANDLE eep_handle,uint16_t const ProductSerialNum_val)
#else
void DevEepTest_ProductSerialNum_Callback(EEP_HANDLE eep_handle,uint32_t const ProductSerialNum_val)
#endif	
{
	
	printf("dev eep test: ProductSerialNum, change to dec=%d,hex=0x%08x\r\n",ProductSerialNum_val,ProductSerialNum_val);
}
#else // EEP_CONTENT_ProductSerialNum_ITEM_LEN > 1
#if EEP_CONTENT_ProductSerialNum_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_ProductSerialNum_Callback(EEP_HANDLE eep_handle,uint8_t const * ProductSerialNum_val)
#elif EEP_CONTENT_ProductSerialNum_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_ProductSerialNum_Callback(EEP_HANDLE eep_handle,uint16_t const * ProductSerialNum_val)
#else
void DevEepTest_ProductSerialNum_Callback(EEP_HANDLE eep_handle,uint32_t const * ProductSerialNum_val)
#endif
{
	printf("dev eep test: ProductSerialNum, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_ProductSerialNum_ITEM_LEN; i ++)
	{
		printf("%d ", ProductSerialNum_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_ProductSerialNum_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_ProductSerialNum_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", ProductSerialNum_val[i]);
		#elif EEP_CONTENT_ProductSerialNum_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", ProductSerialNum_val[i]);
		#else
		printf("%08x ", ProductSerialNum_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_ProductSerialNumMainBoard(int argc, char **argv)
{
#if EEP_CONTENT_ProductSerialNumMainBoard_ITEM_LEN == 1
	#if EEP_CONTENT_ProductSerialNumMainBoard_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t ProductSerialNumMainBoard_val;
	#elif EEP_CONTENT_ProductSerialNumMainBoard_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t ProductSerialNumMainBoard_val;
	#else
	uint32_t ProductSerialNumMainBoard_val;
	#endif
	ProductSerialNumMainBoard_val = atoi(argv[2]);
#else // EEP_CONTENT_ProductSerialNumMainBoard_ITEM_LEN > 1
	#if EEP_CONTENT_ProductSerialNumMainBoard_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t ProductSerialNumMainBoard_val[EEP_CONTENT_ProductSerialNumMainBoard_ITEM_LEN];
	#elif EEP_CONTENT_ProductSerialNumMainBoard_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t ProductSerialNumMainBoard_val[EEP_CONTENT_ProductSerialNumMainBoard_ITEM_LEN];
	#else
	uint32_t ProductSerialNumMainBoard_val[EEP_CONTENT_ProductSerialNumMainBoard_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_ProductSerialNumMainBoard_ITEM_LEN; i ++ )
	{
		ProductSerialNumMainBoard_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_ProductSerialNumMainBoard_ITEM_LEN}	
	
	DevEep_Set_ProductSerialNumMainBoard(eep_handle,ProductSerialNumMainBoard_val);
}


#if EEP_CONTENT_ProductSerialNumMainBoard_ITEM_LEN == 1
#if EEP_CONTENT_ProductSerialNumMainBoard_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_ProductSerialNumMainBoard_Callback(EEP_HANDLE eep_handle,uint8_t const ProductSerialNumMainBoard_val)
#elif EEP_CONTENT_ProductSerialNumMainBoard_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_ProductSerialNumMainBoard_Callback(EEP_HANDLE eep_handle,uint16_t const ProductSerialNumMainBoard_val)
#else
void DevEepTest_ProductSerialNumMainBoard_Callback(EEP_HANDLE eep_handle,uint32_t const ProductSerialNumMainBoard_val)
#endif	
{
	
	printf("dev eep test: ProductSerialNumMainBoard, change to dec=%d,hex=0x%08x\r\n",ProductSerialNumMainBoard_val,ProductSerialNumMainBoard_val);
}
#else // EEP_CONTENT_ProductSerialNumMainBoard_ITEM_LEN > 1
#if EEP_CONTENT_ProductSerialNumMainBoard_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_ProductSerialNumMainBoard_Callback(EEP_HANDLE eep_handle,uint8_t const * ProductSerialNumMainBoard_val)
#elif EEP_CONTENT_ProductSerialNumMainBoard_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_ProductSerialNumMainBoard_Callback(EEP_HANDLE eep_handle,uint16_t const * ProductSerialNumMainBoard_val)
#else
void DevEepTest_ProductSerialNumMainBoard_Callback(EEP_HANDLE eep_handle,uint32_t const * ProductSerialNumMainBoard_val)
#endif
{
	printf("dev eep test: ProductSerialNumMainBoard, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_ProductSerialNumMainBoard_ITEM_LEN; i ++)
	{
		printf("%d ", ProductSerialNumMainBoard_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_ProductSerialNumMainBoard_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_ProductSerialNumMainBoard_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", ProductSerialNumMainBoard_val[i]);
		#elif EEP_CONTENT_ProductSerialNumMainBoard_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", ProductSerialNumMainBoard_val[i]);
		#else
		printf("%08x ", ProductSerialNumMainBoard_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_ProductSerialNumSubBoard(int argc, char **argv)
{
#if EEP_CONTENT_ProductSerialNumSubBoard_ITEM_LEN == 1
	#if EEP_CONTENT_ProductSerialNumSubBoard_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t ProductSerialNumSubBoard_val;
	#elif EEP_CONTENT_ProductSerialNumSubBoard_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t ProductSerialNumSubBoard_val;
	#else
	uint32_t ProductSerialNumSubBoard_val;
	#endif
	ProductSerialNumSubBoard_val = atoi(argv[2]);
#else // EEP_CONTENT_ProductSerialNumSubBoard_ITEM_LEN > 1
	#if EEP_CONTENT_ProductSerialNumSubBoard_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t ProductSerialNumSubBoard_val[EEP_CONTENT_ProductSerialNumSubBoard_ITEM_LEN];
	#elif EEP_CONTENT_ProductSerialNumSubBoard_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t ProductSerialNumSubBoard_val[EEP_CONTENT_ProductSerialNumSubBoard_ITEM_LEN];
	#else
	uint32_t ProductSerialNumSubBoard_val[EEP_CONTENT_ProductSerialNumSubBoard_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_ProductSerialNumSubBoard_ITEM_LEN; i ++ )
	{
		ProductSerialNumSubBoard_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_ProductSerialNumSubBoard_ITEM_LEN}	
	
	DevEep_Set_ProductSerialNumSubBoard(eep_handle,ProductSerialNumSubBoard_val);
}


#if EEP_CONTENT_ProductSerialNumSubBoard_ITEM_LEN == 1
#if EEP_CONTENT_ProductSerialNumSubBoard_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_ProductSerialNumSubBoard_Callback(EEP_HANDLE eep_handle,uint8_t const ProductSerialNumSubBoard_val)
#elif EEP_CONTENT_ProductSerialNumSubBoard_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_ProductSerialNumSubBoard_Callback(EEP_HANDLE eep_handle,uint16_t const ProductSerialNumSubBoard_val)
#else
void DevEepTest_ProductSerialNumSubBoard_Callback(EEP_HANDLE eep_handle,uint32_t const ProductSerialNumSubBoard_val)
#endif	
{
	
	printf("dev eep test: ProductSerialNumSubBoard, change to dec=%d,hex=0x%08x\r\n",ProductSerialNumSubBoard_val,ProductSerialNumSubBoard_val);
}
#else // EEP_CONTENT_ProductSerialNumSubBoard_ITEM_LEN > 1
#if EEP_CONTENT_ProductSerialNumSubBoard_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_ProductSerialNumSubBoard_Callback(EEP_HANDLE eep_handle,uint8_t const * ProductSerialNumSubBoard_val)
#elif EEP_CONTENT_ProductSerialNumSubBoard_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_ProductSerialNumSubBoard_Callback(EEP_HANDLE eep_handle,uint16_t const * ProductSerialNumSubBoard_val)
#else
void DevEepTest_ProductSerialNumSubBoard_Callback(EEP_HANDLE eep_handle,uint32_t const * ProductSerialNumSubBoard_val)
#endif
{
	printf("dev eep test: ProductSerialNumSubBoard, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_ProductSerialNumSubBoard_ITEM_LEN; i ++)
	{
		printf("%d ", ProductSerialNumSubBoard_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_ProductSerialNumSubBoard_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_ProductSerialNumSubBoard_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", ProductSerialNumSubBoard_val[i]);
		#elif EEP_CONTENT_ProductSerialNumSubBoard_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", ProductSerialNumSubBoard_val[i]);
		#else
		printf("%08x ", ProductSerialNumSubBoard_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_SMDManufacturingDate(int argc, char **argv)
{
#if EEP_CONTENT_SMDManufacturingDate_ITEM_LEN == 1
	#if EEP_CONTENT_SMDManufacturingDate_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t SMDManufacturingDate_val;
	#elif EEP_CONTENT_SMDManufacturingDate_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t SMDManufacturingDate_val;
	#else
	uint32_t SMDManufacturingDate_val;
	#endif
	SMDManufacturingDate_val = atoi(argv[2]);
#else // EEP_CONTENT_SMDManufacturingDate_ITEM_LEN > 1
	#if EEP_CONTENT_SMDManufacturingDate_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t SMDManufacturingDate_val[EEP_CONTENT_SMDManufacturingDate_ITEM_LEN];
	#elif EEP_CONTENT_SMDManufacturingDate_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t SMDManufacturingDate_val[EEP_CONTENT_SMDManufacturingDate_ITEM_LEN];
	#else
	uint32_t SMDManufacturingDate_val[EEP_CONTENT_SMDManufacturingDate_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_SMDManufacturingDate_ITEM_LEN; i ++ )
	{
		SMDManufacturingDate_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_SMDManufacturingDate_ITEM_LEN}	
	
	DevEep_Set_SMDManufacturingDate(eep_handle,SMDManufacturingDate_val);
}


#if EEP_CONTENT_SMDManufacturingDate_ITEM_LEN == 1
#if EEP_CONTENT_SMDManufacturingDate_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_SMDManufacturingDate_Callback(EEP_HANDLE eep_handle,uint8_t const SMDManufacturingDate_val)
#elif EEP_CONTENT_SMDManufacturingDate_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_SMDManufacturingDate_Callback(EEP_HANDLE eep_handle,uint16_t const SMDManufacturingDate_val)
#else
void DevEepTest_SMDManufacturingDate_Callback(EEP_HANDLE eep_handle,uint32_t const SMDManufacturingDate_val)
#endif	
{
	
	printf("dev eep test: SMDManufacturingDate, change to dec=%d,hex=0x%08x\r\n",SMDManufacturingDate_val,SMDManufacturingDate_val);
}
#else // EEP_CONTENT_SMDManufacturingDate_ITEM_LEN > 1
#if EEP_CONTENT_SMDManufacturingDate_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_SMDManufacturingDate_Callback(EEP_HANDLE eep_handle,uint8_t const * SMDManufacturingDate_val)
#elif EEP_CONTENT_SMDManufacturingDate_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_SMDManufacturingDate_Callback(EEP_HANDLE eep_handle,uint16_t const * SMDManufacturingDate_val)
#else
void DevEepTest_SMDManufacturingDate_Callback(EEP_HANDLE eep_handle,uint32_t const * SMDManufacturingDate_val)
#endif
{
	printf("dev eep test: SMDManufacturingDate, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_SMDManufacturingDate_ITEM_LEN; i ++)
	{
		printf("%d ", SMDManufacturingDate_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_SMDManufacturingDate_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_SMDManufacturingDate_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", SMDManufacturingDate_val[i]);
		#elif EEP_CONTENT_SMDManufacturingDate_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", SMDManufacturingDate_val[i]);
		#else
		printf("%08x ", SMDManufacturingDate_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_AssemblyManufacturingDate(int argc, char **argv)
{
#if EEP_CONTENT_AssemblyManufacturingDate_ITEM_LEN == 1
	#if EEP_CONTENT_AssemblyManufacturingDate_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t AssemblyManufacturingDate_val;
	#elif EEP_CONTENT_AssemblyManufacturingDate_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t AssemblyManufacturingDate_val;
	#else
	uint32_t AssemblyManufacturingDate_val;
	#endif
	AssemblyManufacturingDate_val = atoi(argv[2]);
#else // EEP_CONTENT_AssemblyManufacturingDate_ITEM_LEN > 1
	#if EEP_CONTENT_AssemblyManufacturingDate_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t AssemblyManufacturingDate_val[EEP_CONTENT_AssemblyManufacturingDate_ITEM_LEN];
	#elif EEP_CONTENT_AssemblyManufacturingDate_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t AssemblyManufacturingDate_val[EEP_CONTENT_AssemblyManufacturingDate_ITEM_LEN];
	#else
	uint32_t AssemblyManufacturingDate_val[EEP_CONTENT_AssemblyManufacturingDate_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_AssemblyManufacturingDate_ITEM_LEN; i ++ )
	{
		AssemblyManufacturingDate_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_AssemblyManufacturingDate_ITEM_LEN}	
	
	DevEep_Set_AssemblyManufacturingDate(eep_handle,AssemblyManufacturingDate_val);
}


#if EEP_CONTENT_AssemblyManufacturingDate_ITEM_LEN == 1
#if EEP_CONTENT_AssemblyManufacturingDate_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_AssemblyManufacturingDate_Callback(EEP_HANDLE eep_handle,uint8_t const AssemblyManufacturingDate_val)
#elif EEP_CONTENT_AssemblyManufacturingDate_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_AssemblyManufacturingDate_Callback(EEP_HANDLE eep_handle,uint16_t const AssemblyManufacturingDate_val)
#else
void DevEepTest_AssemblyManufacturingDate_Callback(EEP_HANDLE eep_handle,uint32_t const AssemblyManufacturingDate_val)
#endif	
{
	
	printf("dev eep test: AssemblyManufacturingDate, change to dec=%d,hex=0x%08x\r\n",AssemblyManufacturingDate_val,AssemblyManufacturingDate_val);
}
#else // EEP_CONTENT_AssemblyManufacturingDate_ITEM_LEN > 1
#if EEP_CONTENT_AssemblyManufacturingDate_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_AssemblyManufacturingDate_Callback(EEP_HANDLE eep_handle,uint8_t const * AssemblyManufacturingDate_val)
#elif EEP_CONTENT_AssemblyManufacturingDate_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_AssemblyManufacturingDate_Callback(EEP_HANDLE eep_handle,uint16_t const * AssemblyManufacturingDate_val)
#else
void DevEepTest_AssemblyManufacturingDate_Callback(EEP_HANDLE eep_handle,uint32_t const * AssemblyManufacturingDate_val)
#endif
{
	printf("dev eep test: AssemblyManufacturingDate, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_AssemblyManufacturingDate_ITEM_LEN; i ++)
	{
		printf("%d ", AssemblyManufacturingDate_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_AssemblyManufacturingDate_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_AssemblyManufacturingDate_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", AssemblyManufacturingDate_val[i]);
		#elif EEP_CONTENT_AssemblyManufacturingDate_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", AssemblyManufacturingDate_val[i]);
		#else
		printf("%08x ", AssemblyManufacturingDate_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_TraceabilityBytes(int argc, char **argv)
{
#if EEP_CONTENT_TraceabilityBytes_ITEM_LEN == 1
	#if EEP_CONTENT_TraceabilityBytes_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t TraceabilityBytes_val;
	#elif EEP_CONTENT_TraceabilityBytes_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t TraceabilityBytes_val;
	#else
	uint32_t TraceabilityBytes_val;
	#endif
	TraceabilityBytes_val = atoi(argv[2]);
#else // EEP_CONTENT_TraceabilityBytes_ITEM_LEN > 1
	#if EEP_CONTENT_TraceabilityBytes_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t TraceabilityBytes_val[EEP_CONTENT_TraceabilityBytes_ITEM_LEN];
	#elif EEP_CONTENT_TraceabilityBytes_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t TraceabilityBytes_val[EEP_CONTENT_TraceabilityBytes_ITEM_LEN];
	#else
	uint32_t TraceabilityBytes_val[EEP_CONTENT_TraceabilityBytes_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_TraceabilityBytes_ITEM_LEN; i ++ )
	{
		TraceabilityBytes_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_TraceabilityBytes_ITEM_LEN}	
	
	DevEep_Set_TraceabilityBytes(eep_handle,TraceabilityBytes_val);
}


#if EEP_CONTENT_TraceabilityBytes_ITEM_LEN == 1
#if EEP_CONTENT_TraceabilityBytes_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_TraceabilityBytes_Callback(EEP_HANDLE eep_handle,uint8_t const TraceabilityBytes_val)
#elif EEP_CONTENT_TraceabilityBytes_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_TraceabilityBytes_Callback(EEP_HANDLE eep_handle,uint16_t const TraceabilityBytes_val)
#else
void DevEepTest_TraceabilityBytes_Callback(EEP_HANDLE eep_handle,uint32_t const TraceabilityBytes_val)
#endif	
{
	
	printf("dev eep test: TraceabilityBytes, change to dec=%d,hex=0x%08x\r\n",TraceabilityBytes_val,TraceabilityBytes_val);
}
#else // EEP_CONTENT_TraceabilityBytes_ITEM_LEN > 1
#if EEP_CONTENT_TraceabilityBytes_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_TraceabilityBytes_Callback(EEP_HANDLE eep_handle,uint8_t const * TraceabilityBytes_val)
#elif EEP_CONTENT_TraceabilityBytes_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_TraceabilityBytes_Callback(EEP_HANDLE eep_handle,uint16_t const * TraceabilityBytes_val)
#else
void DevEepTest_TraceabilityBytes_Callback(EEP_HANDLE eep_handle,uint32_t const * TraceabilityBytes_val)
#endif
{
	printf("dev eep test: TraceabilityBytes, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_TraceabilityBytes_ITEM_LEN; i ++)
	{
		printf("%d ", TraceabilityBytes_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_TraceabilityBytes_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_TraceabilityBytes_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", TraceabilityBytes_val[i]);
		#elif EEP_CONTENT_TraceabilityBytes_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", TraceabilityBytes_val[i]);
		#else
		printf("%08x ", TraceabilityBytes_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_SMDPlantNum(int argc, char **argv)
{
#if EEP_CONTENT_SMDPlantNum_ITEM_LEN == 1
	#if EEP_CONTENT_SMDPlantNum_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t SMDPlantNum_val;
	#elif EEP_CONTENT_SMDPlantNum_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t SMDPlantNum_val;
	#else
	uint32_t SMDPlantNum_val;
	#endif
	SMDPlantNum_val = atoi(argv[2]);
#else // EEP_CONTENT_SMDPlantNum_ITEM_LEN > 1
	#if EEP_CONTENT_SMDPlantNum_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t SMDPlantNum_val[EEP_CONTENT_SMDPlantNum_ITEM_LEN];
	#elif EEP_CONTENT_SMDPlantNum_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t SMDPlantNum_val[EEP_CONTENT_SMDPlantNum_ITEM_LEN];
	#else
	uint32_t SMDPlantNum_val[EEP_CONTENT_SMDPlantNum_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_SMDPlantNum_ITEM_LEN; i ++ )
	{
		SMDPlantNum_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_SMDPlantNum_ITEM_LEN}	
	
	DevEep_Set_SMDPlantNum(eep_handle,SMDPlantNum_val);
}


#if EEP_CONTENT_SMDPlantNum_ITEM_LEN == 1
#if EEP_CONTENT_SMDPlantNum_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_SMDPlantNum_Callback(EEP_HANDLE eep_handle,uint8_t const SMDPlantNum_val)
#elif EEP_CONTENT_SMDPlantNum_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_SMDPlantNum_Callback(EEP_HANDLE eep_handle,uint16_t const SMDPlantNum_val)
#else
void DevEepTest_SMDPlantNum_Callback(EEP_HANDLE eep_handle,uint32_t const SMDPlantNum_val)
#endif	
{
	
	printf("dev eep test: SMDPlantNum, change to dec=%d,hex=0x%08x\r\n",SMDPlantNum_val,SMDPlantNum_val);
}
#else // EEP_CONTENT_SMDPlantNum_ITEM_LEN > 1
#if EEP_CONTENT_SMDPlantNum_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_SMDPlantNum_Callback(EEP_HANDLE eep_handle,uint8_t const * SMDPlantNum_val)
#elif EEP_CONTENT_SMDPlantNum_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_SMDPlantNum_Callback(EEP_HANDLE eep_handle,uint16_t const * SMDPlantNum_val)
#else
void DevEepTest_SMDPlantNum_Callback(EEP_HANDLE eep_handle,uint32_t const * SMDPlantNum_val)
#endif
{
	printf("dev eep test: SMDPlantNum, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_SMDPlantNum_ITEM_LEN; i ++)
	{
		printf("%d ", SMDPlantNum_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_SMDPlantNum_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_SMDPlantNum_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", SMDPlantNum_val[i]);
		#elif EEP_CONTENT_SMDPlantNum_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", SMDPlantNum_val[i]);
		#else
		printf("%08x ", SMDPlantNum_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_AssemblyPlantNum(int argc, char **argv)
{
#if EEP_CONTENT_AssemblyPlantNum_ITEM_LEN == 1
	#if EEP_CONTENT_AssemblyPlantNum_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t AssemblyPlantNum_val;
	#elif EEP_CONTENT_AssemblyPlantNum_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t AssemblyPlantNum_val;
	#else
	uint32_t AssemblyPlantNum_val;
	#endif
	AssemblyPlantNum_val = atoi(argv[2]);
#else // EEP_CONTENT_AssemblyPlantNum_ITEM_LEN > 1
	#if EEP_CONTENT_AssemblyPlantNum_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t AssemblyPlantNum_val[EEP_CONTENT_AssemblyPlantNum_ITEM_LEN];
	#elif EEP_CONTENT_AssemblyPlantNum_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t AssemblyPlantNum_val[EEP_CONTENT_AssemblyPlantNum_ITEM_LEN];
	#else
	uint32_t AssemblyPlantNum_val[EEP_CONTENT_AssemblyPlantNum_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_AssemblyPlantNum_ITEM_LEN; i ++ )
	{
		AssemblyPlantNum_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_AssemblyPlantNum_ITEM_LEN}	
	
	DevEep_Set_AssemblyPlantNum(eep_handle,AssemblyPlantNum_val);
}


#if EEP_CONTENT_AssemblyPlantNum_ITEM_LEN == 1
#if EEP_CONTENT_AssemblyPlantNum_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_AssemblyPlantNum_Callback(EEP_HANDLE eep_handle,uint8_t const AssemblyPlantNum_val)
#elif EEP_CONTENT_AssemblyPlantNum_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_AssemblyPlantNum_Callback(EEP_HANDLE eep_handle,uint16_t const AssemblyPlantNum_val)
#else
void DevEepTest_AssemblyPlantNum_Callback(EEP_HANDLE eep_handle,uint32_t const AssemblyPlantNum_val)
#endif	
{
	
	printf("dev eep test: AssemblyPlantNum, change to dec=%d,hex=0x%08x\r\n",AssemblyPlantNum_val,AssemblyPlantNum_val);
}
#else // EEP_CONTENT_AssemblyPlantNum_ITEM_LEN > 1
#if EEP_CONTENT_AssemblyPlantNum_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_AssemblyPlantNum_Callback(EEP_HANDLE eep_handle,uint8_t const * AssemblyPlantNum_val)
#elif EEP_CONTENT_AssemblyPlantNum_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_AssemblyPlantNum_Callback(EEP_HANDLE eep_handle,uint16_t const * AssemblyPlantNum_val)
#else
void DevEepTest_AssemblyPlantNum_Callback(EEP_HANDLE eep_handle,uint32_t const * AssemblyPlantNum_val)
#endif
{
	printf("dev eep test: AssemblyPlantNum, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_AssemblyPlantNum_ITEM_LEN; i ++)
	{
		printf("%d ", AssemblyPlantNum_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_AssemblyPlantNum_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_AssemblyPlantNum_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", AssemblyPlantNum_val[i]);
		#elif EEP_CONTENT_AssemblyPlantNum_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", AssemblyPlantNum_val[i]);
		#else
		printf("%08x ", AssemblyPlantNum_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_DEM_EVENT_CAN_COM_DATA(int argc, char **argv)
{
#if EEP_CONTENT_DEM_EVENT_CAN_COM_DATA_ITEM_LEN == 1
	#if EEP_CONTENT_DEM_EVENT_CAN_COM_DATA_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t DEM_EVENT_CAN_COM_DATA_val;
	#elif EEP_CONTENT_DEM_EVENT_CAN_COM_DATA_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t DEM_EVENT_CAN_COM_DATA_val;
	#else
	uint32_t DEM_EVENT_CAN_COM_DATA_val;
	#endif
	DEM_EVENT_CAN_COM_DATA_val = atoi(argv[2]);
#else // EEP_CONTENT_DEM_EVENT_CAN_COM_DATA_ITEM_LEN > 1
	#if EEP_CONTENT_DEM_EVENT_CAN_COM_DATA_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t DEM_EVENT_CAN_COM_DATA_val[EEP_CONTENT_DEM_EVENT_CAN_COM_DATA_ITEM_LEN];
	#elif EEP_CONTENT_DEM_EVENT_CAN_COM_DATA_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t DEM_EVENT_CAN_COM_DATA_val[EEP_CONTENT_DEM_EVENT_CAN_COM_DATA_ITEM_LEN];
	#else
	uint32_t DEM_EVENT_CAN_COM_DATA_val[EEP_CONTENT_DEM_EVENT_CAN_COM_DATA_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_DEM_EVENT_CAN_COM_DATA_ITEM_LEN; i ++ )
	{
		DEM_EVENT_CAN_COM_DATA_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_DEM_EVENT_CAN_COM_DATA_ITEM_LEN}	
	
	DevEep_Set_DEM_EVENT_CAN_COM_DATA(eep_handle,DEM_EVENT_CAN_COM_DATA_val);
}


#if EEP_CONTENT_DEM_EVENT_CAN_COM_DATA_ITEM_LEN == 1
#if EEP_CONTENT_DEM_EVENT_CAN_COM_DATA_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_DEM_EVENT_CAN_COM_DATA_Callback(EEP_HANDLE eep_handle,uint8_t const DEM_EVENT_CAN_COM_DATA_val)
#elif EEP_CONTENT_DEM_EVENT_CAN_COM_DATA_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_DEM_EVENT_CAN_COM_DATA_Callback(EEP_HANDLE eep_handle,uint16_t const DEM_EVENT_CAN_COM_DATA_val)
#else
void DevEepTest_DEM_EVENT_CAN_COM_DATA_Callback(EEP_HANDLE eep_handle,uint32_t const DEM_EVENT_CAN_COM_DATA_val)
#endif	
{
	
	printf("dev eep test: DEM_EVENT_CAN_COM_DATA, change to dec=%d,hex=0x%08x\r\n",DEM_EVENT_CAN_COM_DATA_val,DEM_EVENT_CAN_COM_DATA_val);
}
#else // EEP_CONTENT_DEM_EVENT_CAN_COM_DATA_ITEM_LEN > 1
#if EEP_CONTENT_DEM_EVENT_CAN_COM_DATA_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_DEM_EVENT_CAN_COM_DATA_Callback(EEP_HANDLE eep_handle,uint8_t const * DEM_EVENT_CAN_COM_DATA_val)
#elif EEP_CONTENT_DEM_EVENT_CAN_COM_DATA_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_DEM_EVENT_CAN_COM_DATA_Callback(EEP_HANDLE eep_handle,uint16_t const * DEM_EVENT_CAN_COM_DATA_val)
#else
void DevEepTest_DEM_EVENT_CAN_COM_DATA_Callback(EEP_HANDLE eep_handle,uint32_t const * DEM_EVENT_CAN_COM_DATA_val)
#endif
{
	printf("dev eep test: DEM_EVENT_CAN_COM_DATA, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_DEM_EVENT_CAN_COM_DATA_ITEM_LEN; i ++)
	{
		printf("%d ", DEM_EVENT_CAN_COM_DATA_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_DEM_EVENT_CAN_COM_DATA_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_DEM_EVENT_CAN_COM_DATA_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", DEM_EVENT_CAN_COM_DATA_val[i]);
		#elif EEP_CONTENT_DEM_EVENT_CAN_COM_DATA_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", DEM_EVENT_CAN_COM_DATA_val[i]);
		#else
		printf("%08x ", DEM_EVENT_CAN_COM_DATA_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA(int argc, char **argv)
{
#if EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_LEN == 1
	#if EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_val;
	#elif EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_val;
	#else
	uint32_t DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_val;
	#endif
	DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_val = atoi(argv[2]);
#else // EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_LEN > 1
	#if EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_val[EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_LEN];
	#elif EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_val[EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_LEN];
	#else
	uint32_t DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_val[EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_LEN; i ++ )
	{
		DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_LEN}	
	
	DevEep_Set_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA(eep_handle,DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_val);
}


#if EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_LEN == 1
#if EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_Callback(EEP_HANDLE eep_handle,uint8_t const DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_val)
#elif EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_Callback(EEP_HANDLE eep_handle,uint16_t const DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_val)
#else
void DevEepTest_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_Callback(EEP_HANDLE eep_handle,uint32_t const DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_val)
#endif	
{
	
	printf("dev eep test: DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA, change to dec=%d,hex=0x%08x\r\n",DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_val,DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_val);
}
#else // EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_LEN > 1
#if EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_Callback(EEP_HANDLE eep_handle,uint8_t const * DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_val)
#elif EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_Callback(EEP_HANDLE eep_handle,uint16_t const * DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_val)
#else
void DevEepTest_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_Callback(EEP_HANDLE eep_handle,uint32_t const * DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_val)
#endif
{
	printf("dev eep test: DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_LEN; i ++)
	{
		printf("%d ", DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_val[i]);
		#elif EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_val[i]);
		#else
		printf("%08x ", DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA(int argc, char **argv)
{
#if EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_LEN == 1
	#if EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_val;
	#elif EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_val;
	#else
	uint32_t DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_val;
	#endif
	DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_val = atoi(argv[2]);
#else // EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_LEN > 1
	#if EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_val[EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_LEN];
	#elif EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_val[EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_LEN];
	#else
	uint32_t DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_val[EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_LEN; i ++ )
	{
		DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_LEN}	
	
	DevEep_Set_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA(eep_handle,DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_val);
}


#if EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_LEN == 1
#if EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_Callback(EEP_HANDLE eep_handle,uint8_t const DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_val)
#elif EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_Callback(EEP_HANDLE eep_handle,uint16_t const DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_val)
#else
void DevEepTest_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_Callback(EEP_HANDLE eep_handle,uint32_t const DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_val)
#endif	
{
	
	printf("dev eep test: DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA, change to dec=%d,hex=0x%08x\r\n",DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_val,DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_val);
}
#else // EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_LEN > 1
#if EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_Callback(EEP_HANDLE eep_handle,uint8_t const * DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_val)
#elif EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_Callback(EEP_HANDLE eep_handle,uint16_t const * DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_val)
#else
void DevEepTest_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_Callback(EEP_HANDLE eep_handle,uint32_t const * DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_val)
#endif
{
	printf("dev eep test: DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_LEN; i ++)
	{
		printf("%d ", DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_val[i]);
		#elif EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_val[i]);
		#else
		printf("%08x ", DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA(int argc, char **argv)
{
#if EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_LEN == 1
	#if EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_val;
	#elif EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_val;
	#else
	uint32_t DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_val;
	#endif
	DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_val = atoi(argv[2]);
#else // EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_LEN > 1
	#if EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_val[EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_LEN];
	#elif EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_val[EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_LEN];
	#else
	uint32_t DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_val[EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_LEN; i ++ )
	{
		DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_LEN}	
	
	DevEep_Set_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA(eep_handle,DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_val);
}


#if EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_LEN == 1
#if EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_Callback(EEP_HANDLE eep_handle,uint8_t const DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_val)
#elif EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_Callback(EEP_HANDLE eep_handle,uint16_t const DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_val)
#else
void DevEepTest_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_Callback(EEP_HANDLE eep_handle,uint32_t const DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_val)
#endif	
{
	
	printf("dev eep test: DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA, change to dec=%d,hex=0x%08x\r\n",DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_val,DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_val);
}
#else // EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_LEN > 1
#if EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_Callback(EEP_HANDLE eep_handle,uint8_t const * DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_val)
#elif EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_Callback(EEP_HANDLE eep_handle,uint16_t const * DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_val)
#else
void DevEepTest_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_Callback(EEP_HANDLE eep_handle,uint32_t const * DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_val)
#endif
{
	printf("dev eep test: DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_LEN; i ++)
	{
		printf("%d ", DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_val[i]);
		#elif EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_val[i]);
		#else
		printf("%08x ", DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA(int argc, char **argv)
{
#if EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_LEN == 1
	#if EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_val;
	#elif EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_val;
	#else
	uint32_t DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_val;
	#endif
	DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_val = atoi(argv[2]);
#else // EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_LEN > 1
	#if EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_val[EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_LEN];
	#elif EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_val[EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_LEN];
	#else
	uint32_t DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_val[EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_LEN; i ++ )
	{
		DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_LEN}	
	
	DevEep_Set_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA(eep_handle,DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_val);
}


#if EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_LEN == 1
#if EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_Callback(EEP_HANDLE eep_handle,uint8_t const DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_val)
#elif EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_Callback(EEP_HANDLE eep_handle,uint16_t const DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_val)
#else
void DevEepTest_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_Callback(EEP_HANDLE eep_handle,uint32_t const DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_val)
#endif	
{
	
	printf("dev eep test: DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA, change to dec=%d,hex=0x%08x\r\n",DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_val,DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_val);
}
#else // EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_LEN > 1
#if EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_Callback(EEP_HANDLE eep_handle,uint8_t const * DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_val)
#elif EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_Callback(EEP_HANDLE eep_handle,uint16_t const * DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_val)
#else
void DevEepTest_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_Callback(EEP_HANDLE eep_handle,uint32_t const * DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_val)
#endif
{
	printf("dev eep test: DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_LEN; i ++)
	{
		printf("%d ", DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_val[i]);
		#elif EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_val[i]);
		#else
		printf("%08x ", DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA(int argc, char **argv)
{
#if EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_LEN == 1
	#if EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_val;
	#elif EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_val;
	#else
	uint32_t DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_val;
	#endif
	DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_val = atoi(argv[2]);
#else // EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_LEN > 1
	#if EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_val[EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_LEN];
	#elif EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_val[EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_LEN];
	#else
	uint32_t DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_val[EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_LEN; i ++ )
	{
		DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_LEN}	
	
	DevEep_Set_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA(eep_handle,DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_val);
}


#if EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_LEN == 1
#if EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_Callback(EEP_HANDLE eep_handle,uint8_t const DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_val)
#elif EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_Callback(EEP_HANDLE eep_handle,uint16_t const DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_val)
#else
void DevEepTest_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_Callback(EEP_HANDLE eep_handle,uint32_t const DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_val)
#endif	
{
	
	printf("dev eep test: DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA, change to dec=%d,hex=0x%08x\r\n",DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_val,DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_val);
}
#else // EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_LEN > 1
#if EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_Callback(EEP_HANDLE eep_handle,uint8_t const * DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_val)
#elif EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_Callback(EEP_HANDLE eep_handle,uint16_t const * DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_val)
#else
void DevEepTest_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_Callback(EEP_HANDLE eep_handle,uint32_t const * DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_val)
#endif
{
	printf("dev eep test: DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_LEN; i ++)
	{
		printf("%d ", DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_val[i]);
		#elif EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_val[i]);
		#else
		printf("%08x ", DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA(int argc, char **argv)
{
#if EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_LEN == 1
	#if EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_val;
	#elif EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_val;
	#else
	uint32_t DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_val;
	#endif
	DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_val = atoi(argv[2]);
#else // EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_LEN > 1
	#if EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_val[EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_LEN];
	#elif EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_val[EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_LEN];
	#else
	uint32_t DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_val[EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_LEN; i ++ )
	{
		DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_LEN}	
	
	DevEep_Set_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA(eep_handle,DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_val);
}


#if EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_LEN == 1
#if EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_Callback(EEP_HANDLE eep_handle,uint8_t const DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_val)
#elif EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_Callback(EEP_HANDLE eep_handle,uint16_t const DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_val)
#else
void DevEepTest_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_Callback(EEP_HANDLE eep_handle,uint32_t const DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_val)
#endif	
{
	
	printf("dev eep test: DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA, change to dec=%d,hex=0x%08x\r\n",DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_val,DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_val);
}
#else // EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_LEN > 1
#if EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_Callback(EEP_HANDLE eep_handle,uint8_t const * DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_val)
#elif EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_Callback(EEP_HANDLE eep_handle,uint16_t const * DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_val)
#else
void DevEepTest_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_Callback(EEP_HANDLE eep_handle,uint32_t const * DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_val)
#endif
{
	printf("dev eep test: DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_LEN; i ++)
	{
		printf("%d ", DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_val[i]);
		#elif EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_val[i]);
		#else
		printf("%08x ", DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA(int argc, char **argv)
{
#if EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_LEN == 1
	#if EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_val;
	#elif EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_val;
	#else
	uint32_t DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_val;
	#endif
	DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_val = atoi(argv[2]);
#else // EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_LEN > 1
	#if EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_val[EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_LEN];
	#elif EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_val[EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_LEN];
	#else
	uint32_t DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_val[EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_LEN; i ++ )
	{
		DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_LEN}	
	
	DevEep_Set_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA(eep_handle,DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_val);
}


#if EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_LEN == 1
#if EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_Callback(EEP_HANDLE eep_handle,uint8_t const DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_val)
#elif EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_Callback(EEP_HANDLE eep_handle,uint16_t const DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_val)
#else
void DevEepTest_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_Callback(EEP_HANDLE eep_handle,uint32_t const DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_val)
#endif	
{
	
	printf("dev eep test: DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA, change to dec=%d,hex=0x%08x\r\n",DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_val,DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_val);
}
#else // EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_LEN > 1
#if EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_Callback(EEP_HANDLE eep_handle,uint8_t const * DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_val)
#elif EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_Callback(EEP_HANDLE eep_handle,uint16_t const * DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_val)
#else
void DevEepTest_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_Callback(EEP_HANDLE eep_handle,uint32_t const * DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_val)
#endif
{
	printf("dev eep test: DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_LEN; i ++)
	{
		printf("%d ", DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_val[i]);
		#elif EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_val[i]);
		#else
		printf("%08x ", DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA(int argc, char **argv)
{
#if EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_LEN == 1
	#if EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_val;
	#elif EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_val;
	#else
	uint32_t DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_val;
	#endif
	DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_val = atoi(argv[2]);
#else // EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_LEN > 1
	#if EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_val[EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_LEN];
	#elif EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_val[EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_LEN];
	#else
	uint32_t DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_val[EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_LEN; i ++ )
	{
		DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_LEN}	
	
	DevEep_Set_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA(eep_handle,DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_val);
}


#if EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_LEN == 1
#if EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_Callback(EEP_HANDLE eep_handle,uint8_t const DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_val)
#elif EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_Callback(EEP_HANDLE eep_handle,uint16_t const DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_val)
#else
void DevEepTest_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_Callback(EEP_HANDLE eep_handle,uint32_t const DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_val)
#endif	
{
	
	printf("dev eep test: DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA, change to dec=%d,hex=0x%08x\r\n",DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_val,DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_val);
}
#else // EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_LEN > 1
#if EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_Callback(EEP_HANDLE eep_handle,uint8_t const * DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_val)
#elif EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_Callback(EEP_HANDLE eep_handle,uint16_t const * DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_val)
#else
void DevEepTest_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_Callback(EEP_HANDLE eep_handle,uint32_t const * DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_val)
#endif
{
	printf("dev eep test: DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_LEN; i ++)
	{
		printf("%d ", DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_val[i]);
		#elif EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_val[i]);
		#else
		printf("%08x ", DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA(int argc, char **argv)
{
#if EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_ITEM_LEN == 1
	#if EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_val;
	#elif EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_val;
	#else
	uint32_t DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_val;
	#endif
	DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_val = atoi(argv[2]);
#else // EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_ITEM_LEN > 1
	#if EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_val[EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_ITEM_LEN];
	#elif EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_val[EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_ITEM_LEN];
	#else
	uint32_t DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_val[EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_ITEM_LEN; i ++ )
	{
		DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_ITEM_LEN}	
	
	DevEep_Set_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA(eep_handle,DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_val);
}


#if EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_ITEM_LEN == 1
#if EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_Callback(EEP_HANDLE eep_handle,uint8_t const DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_val)
#elif EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_Callback(EEP_HANDLE eep_handle,uint16_t const DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_val)
#else
void DevEepTest_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_Callback(EEP_HANDLE eep_handle,uint32_t const DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_val)
#endif	
{
	
	printf("dev eep test: DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA, change to dec=%d,hex=0x%08x\r\n",DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_val,DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_val);
}
#else // EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_ITEM_LEN > 1
#if EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_Callback(EEP_HANDLE eep_handle,uint8_t const * DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_val)
#elif EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_Callback(EEP_HANDLE eep_handle,uint16_t const * DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_val)
#else
void DevEepTest_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_Callback(EEP_HANDLE eep_handle,uint32_t const * DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_val)
#endif
{
	printf("dev eep test: DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_ITEM_LEN; i ++)
	{
		printf("%d ", DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_val[i]);
		#elif EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_val[i]);
		#else
		printf("%08x ", DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA(int argc, char **argv)
{
#if EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_ITEM_LEN == 1
	#if EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_val;
	#elif EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_val;
	#else
	uint32_t DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_val;
	#endif
	DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_val = atoi(argv[2]);
#else // EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_ITEM_LEN > 1
	#if EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_val[EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_ITEM_LEN];
	#elif EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_val[EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_ITEM_LEN];
	#else
	uint32_t DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_val[EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_ITEM_LEN; i ++ )
	{
		DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_ITEM_LEN}	
	
	DevEep_Set_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA(eep_handle,DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_val);
}


#if EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_ITEM_LEN == 1
#if EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_Callback(EEP_HANDLE eep_handle,uint8_t const DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_val)
#elif EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_Callback(EEP_HANDLE eep_handle,uint16_t const DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_val)
#else
void DevEepTest_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_Callback(EEP_HANDLE eep_handle,uint32_t const DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_val)
#endif	
{
	
	printf("dev eep test: DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA, change to dec=%d,hex=0x%08x\r\n",DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_val,DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_val);
}
#else // EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_ITEM_LEN > 1
#if EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_Callback(EEP_HANDLE eep_handle,uint8_t const * DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_val)
#elif EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_Callback(EEP_HANDLE eep_handle,uint16_t const * DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_val)
#else
void DevEepTest_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_Callback(EEP_HANDLE eep_handle,uint32_t const * DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_val)
#endif
{
	printf("dev eep test: DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_ITEM_LEN; i ++)
	{
		printf("%d ", DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_val[i]);
		#elif EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_val[i]);
		#else
		printf("%08x ", DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA(int argc, char **argv)
{
#if EEP_CONTENT_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_ITEM_LEN == 1
	#if EEP_CONTENT_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_val;
	#elif EEP_CONTENT_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_val;
	#else
	uint32_t DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_val;
	#endif
	DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_val = atoi(argv[2]);
#else // EEP_CONTENT_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_ITEM_LEN > 1
	#if EEP_CONTENT_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_val[EEP_CONTENT_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_ITEM_LEN];
	#elif EEP_CONTENT_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_val[EEP_CONTENT_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_ITEM_LEN];
	#else
	uint32_t DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_val[EEP_CONTENT_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_ITEM_LEN; i ++ )
	{
		DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_ITEM_LEN}	
	
	DevEep_Set_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA(eep_handle,DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_val);
}


#if EEP_CONTENT_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_ITEM_LEN == 1
#if EEP_CONTENT_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_Callback(EEP_HANDLE eep_handle,uint8_t const DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_val)
#elif EEP_CONTENT_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_Callback(EEP_HANDLE eep_handle,uint16_t const DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_val)
#else
void DevEepTest_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_Callback(EEP_HANDLE eep_handle,uint32_t const DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_val)
#endif	
{
	
	printf("dev eep test: DEM_EVENT_HVAC_PANEL_CONNECTION_DATA, change to dec=%d,hex=0x%08x\r\n",DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_val,DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_val);
}
#else // EEP_CONTENT_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_ITEM_LEN > 1
#if EEP_CONTENT_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_Callback(EEP_HANDLE eep_handle,uint8_t const * DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_val)
#elif EEP_CONTENT_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_Callback(EEP_HANDLE eep_handle,uint16_t const * DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_val)
#else
void DevEepTest_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_Callback(EEP_HANDLE eep_handle,uint32_t const * DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_val)
#endif
{
	printf("dev eep test: DEM_EVENT_HVAC_PANEL_CONNECTION_DATA, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_ITEM_LEN; i ++)
	{
		printf("%d ", DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_val[i]);
		#elif EEP_CONTENT_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_val[i]);
		#else
		printf("%08x ", DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_TachoUpValue(int argc, char **argv)
{
#if EEP_CONTENT_TachoUpValue_ITEM_LEN == 1
	#if EEP_CONTENT_TachoUpValue_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t TachoUpValue_val;
	#elif EEP_CONTENT_TachoUpValue_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t TachoUpValue_val;
	#else
	uint32_t TachoUpValue_val;
	#endif
	TachoUpValue_val = atoi(argv[2]);
#else // EEP_CONTENT_TachoUpValue_ITEM_LEN > 1
	#if EEP_CONTENT_TachoUpValue_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t TachoUpValue_val[EEP_CONTENT_TachoUpValue_ITEM_LEN];
	#elif EEP_CONTENT_TachoUpValue_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t TachoUpValue_val[EEP_CONTENT_TachoUpValue_ITEM_LEN];
	#else
	uint32_t TachoUpValue_val[EEP_CONTENT_TachoUpValue_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_TachoUpValue_ITEM_LEN; i ++ )
	{
		TachoUpValue_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_TachoUpValue_ITEM_LEN}	
	
	DevEep_Set_TachoUpValue(eep_handle,TachoUpValue_val);
}


#if EEP_CONTENT_TachoUpValue_ITEM_LEN == 1
#if EEP_CONTENT_TachoUpValue_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_TachoUpValue_Callback(EEP_HANDLE eep_handle,uint8_t const TachoUpValue_val)
#elif EEP_CONTENT_TachoUpValue_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_TachoUpValue_Callback(EEP_HANDLE eep_handle,uint16_t const TachoUpValue_val)
#else
void DevEepTest_TachoUpValue_Callback(EEP_HANDLE eep_handle,uint32_t const TachoUpValue_val)
#endif	
{
	
	printf("dev eep test: TachoUpValue, change to dec=%d,hex=0x%08x\r\n",TachoUpValue_val,TachoUpValue_val);
}
#else // EEP_CONTENT_TachoUpValue_ITEM_LEN > 1
#if EEP_CONTENT_TachoUpValue_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_TachoUpValue_Callback(EEP_HANDLE eep_handle,uint8_t const * TachoUpValue_val)
#elif EEP_CONTENT_TachoUpValue_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_TachoUpValue_Callback(EEP_HANDLE eep_handle,uint16_t const * TachoUpValue_val)
#else
void DevEepTest_TachoUpValue_Callback(EEP_HANDLE eep_handle,uint32_t const * TachoUpValue_val)
#endif
{
	printf("dev eep test: TachoUpValue, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_TachoUpValue_ITEM_LEN; i ++)
	{
		printf("%d ", TachoUpValue_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_TachoUpValue_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_TachoUpValue_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", TachoUpValue_val[i]);
		#elif EEP_CONTENT_TachoUpValue_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", TachoUpValue_val[i]);
		#else
		printf("%08x ", TachoUpValue_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_TachoLowValue(int argc, char **argv)
{
#if EEP_CONTENT_TachoLowValue_ITEM_LEN == 1
	#if EEP_CONTENT_TachoLowValue_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t TachoLowValue_val;
	#elif EEP_CONTENT_TachoLowValue_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t TachoLowValue_val;
	#else
	uint32_t TachoLowValue_val;
	#endif
	TachoLowValue_val = atoi(argv[2]);
#else // EEP_CONTENT_TachoLowValue_ITEM_LEN > 1
	#if EEP_CONTENT_TachoLowValue_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t TachoLowValue_val[EEP_CONTENT_TachoLowValue_ITEM_LEN];
	#elif EEP_CONTENT_TachoLowValue_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t TachoLowValue_val[EEP_CONTENT_TachoLowValue_ITEM_LEN];
	#else
	uint32_t TachoLowValue_val[EEP_CONTENT_TachoLowValue_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_TachoLowValue_ITEM_LEN; i ++ )
	{
		TachoLowValue_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_TachoLowValue_ITEM_LEN}	
	
	DevEep_Set_TachoLowValue(eep_handle,TachoLowValue_val);
}


#if EEP_CONTENT_TachoLowValue_ITEM_LEN == 1
#if EEP_CONTENT_TachoLowValue_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_TachoLowValue_Callback(EEP_HANDLE eep_handle,uint8_t const TachoLowValue_val)
#elif EEP_CONTENT_TachoLowValue_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_TachoLowValue_Callback(EEP_HANDLE eep_handle,uint16_t const TachoLowValue_val)
#else
void DevEepTest_TachoLowValue_Callback(EEP_HANDLE eep_handle,uint32_t const TachoLowValue_val)
#endif	
{
	
	printf("dev eep test: TachoLowValue, change to dec=%d,hex=0x%08x\r\n",TachoLowValue_val,TachoLowValue_val);
}
#else // EEP_CONTENT_TachoLowValue_ITEM_LEN > 1
#if EEP_CONTENT_TachoLowValue_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_TachoLowValue_Callback(EEP_HANDLE eep_handle,uint8_t const * TachoLowValue_val)
#elif EEP_CONTENT_TachoLowValue_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_TachoLowValue_Callback(EEP_HANDLE eep_handle,uint16_t const * TachoLowValue_val)
#else
void DevEepTest_TachoLowValue_Callback(EEP_HANDLE eep_handle,uint32_t const * TachoLowValue_val)
#endif
{
	printf("dev eep test: TachoLowValue, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_TachoLowValue_ITEM_LEN; i ++)
	{
		printf("%d ", TachoLowValue_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_TachoLowValue_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_TachoLowValue_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", TachoLowValue_val[i]);
		#elif EEP_CONTENT_TachoLowValue_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", TachoLowValue_val[i]);
		#else
		printf("%08x ", TachoLowValue_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_TachoLimitValue(int argc, char **argv)
{
#if EEP_CONTENT_TachoLimitValue_ITEM_LEN == 1
	#if EEP_CONTENT_TachoLimitValue_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t TachoLimitValue_val;
	#elif EEP_CONTENT_TachoLimitValue_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t TachoLimitValue_val;
	#else
	uint32_t TachoLimitValue_val;
	#endif
	TachoLimitValue_val = atoi(argv[2]);
#else // EEP_CONTENT_TachoLimitValue_ITEM_LEN > 1
	#if EEP_CONTENT_TachoLimitValue_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t TachoLimitValue_val[EEP_CONTENT_TachoLimitValue_ITEM_LEN];
	#elif EEP_CONTENT_TachoLimitValue_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t TachoLimitValue_val[EEP_CONTENT_TachoLimitValue_ITEM_LEN];
	#else
	uint32_t TachoLimitValue_val[EEP_CONTENT_TachoLimitValue_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_TachoLimitValue_ITEM_LEN; i ++ )
	{
		TachoLimitValue_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_TachoLimitValue_ITEM_LEN}	
	
	DevEep_Set_TachoLimitValue(eep_handle,TachoLimitValue_val);
}


#if EEP_CONTENT_TachoLimitValue_ITEM_LEN == 1
#if EEP_CONTENT_TachoLimitValue_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_TachoLimitValue_Callback(EEP_HANDLE eep_handle,uint8_t const TachoLimitValue_val)
#elif EEP_CONTENT_TachoLimitValue_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_TachoLimitValue_Callback(EEP_HANDLE eep_handle,uint16_t const TachoLimitValue_val)
#else
void DevEepTest_TachoLimitValue_Callback(EEP_HANDLE eep_handle,uint32_t const TachoLimitValue_val)
#endif	
{
	
	printf("dev eep test: TachoLimitValue, change to dec=%d,hex=0x%08x\r\n",TachoLimitValue_val,TachoLimitValue_val);
}
#else // EEP_CONTENT_TachoLimitValue_ITEM_LEN > 1
#if EEP_CONTENT_TachoLimitValue_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_TachoLimitValue_Callback(EEP_HANDLE eep_handle,uint8_t const * TachoLimitValue_val)
#elif EEP_CONTENT_TachoLimitValue_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_TachoLimitValue_Callback(EEP_HANDLE eep_handle,uint16_t const * TachoLimitValue_val)
#else
void DevEepTest_TachoLimitValue_Callback(EEP_HANDLE eep_handle,uint32_t const * TachoLimitValue_val)
#endif
{
	printf("dev eep test: TachoLimitValue, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_TachoLimitValue_ITEM_LEN; i ++)
	{
		printf("%d ", TachoLimitValue_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_TachoLimitValue_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_TachoLimitValue_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", TachoLimitValue_val[i]);
		#elif EEP_CONTENT_TachoLimitValue_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", TachoLimitValue_val[i]);
		#else
		printf("%08x ", TachoLimitValue_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_TachoUDeltaLimit(int argc, char **argv)
{
#if EEP_CONTENT_TachoUDeltaLimit_ITEM_LEN == 1
	#if EEP_CONTENT_TachoUDeltaLimit_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t TachoUDeltaLimit_val;
	#elif EEP_CONTENT_TachoUDeltaLimit_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t TachoUDeltaLimit_val;
	#else
	uint32_t TachoUDeltaLimit_val;
	#endif
	TachoUDeltaLimit_val = atoi(argv[2]);
#else // EEP_CONTENT_TachoUDeltaLimit_ITEM_LEN > 1
	#if EEP_CONTENT_TachoUDeltaLimit_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t TachoUDeltaLimit_val[EEP_CONTENT_TachoUDeltaLimit_ITEM_LEN];
	#elif EEP_CONTENT_TachoUDeltaLimit_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t TachoUDeltaLimit_val[EEP_CONTENT_TachoUDeltaLimit_ITEM_LEN];
	#else
	uint32_t TachoUDeltaLimit_val[EEP_CONTENT_TachoUDeltaLimit_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_TachoUDeltaLimit_ITEM_LEN; i ++ )
	{
		TachoUDeltaLimit_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_TachoUDeltaLimit_ITEM_LEN}	
	
	DevEep_Set_TachoUDeltaLimit(eep_handle,TachoUDeltaLimit_val);
}


#if EEP_CONTENT_TachoUDeltaLimit_ITEM_LEN == 1
#if EEP_CONTENT_TachoUDeltaLimit_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_TachoUDeltaLimit_Callback(EEP_HANDLE eep_handle,uint8_t const TachoUDeltaLimit_val)
#elif EEP_CONTENT_TachoUDeltaLimit_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_TachoUDeltaLimit_Callback(EEP_HANDLE eep_handle,uint16_t const TachoUDeltaLimit_val)
#else
void DevEepTest_TachoUDeltaLimit_Callback(EEP_HANDLE eep_handle,uint32_t const TachoUDeltaLimit_val)
#endif	
{
	
	printf("dev eep test: TachoUDeltaLimit, change to dec=%d,hex=0x%08x\r\n",TachoUDeltaLimit_val,TachoUDeltaLimit_val);
}
#else // EEP_CONTENT_TachoUDeltaLimit_ITEM_LEN > 1
#if EEP_CONTENT_TachoUDeltaLimit_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_TachoUDeltaLimit_Callback(EEP_HANDLE eep_handle,uint8_t const * TachoUDeltaLimit_val)
#elif EEP_CONTENT_TachoUDeltaLimit_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_TachoUDeltaLimit_Callback(EEP_HANDLE eep_handle,uint16_t const * TachoUDeltaLimit_val)
#else
void DevEepTest_TachoUDeltaLimit_Callback(EEP_HANDLE eep_handle,uint32_t const * TachoUDeltaLimit_val)
#endif
{
	printf("dev eep test: TachoUDeltaLimit, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_TachoUDeltaLimit_ITEM_LEN; i ++)
	{
		printf("%d ", TachoUDeltaLimit_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_TachoUDeltaLimit_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_TachoUDeltaLimit_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", TachoUDeltaLimit_val[i]);
		#elif EEP_CONTENT_TachoUDeltaLimit_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", TachoUDeltaLimit_val[i]);
		#else
		printf("%08x ", TachoUDeltaLimit_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_TachoLDeltaLimit(int argc, char **argv)
{
#if EEP_CONTENT_TachoLDeltaLimit_ITEM_LEN == 1
	#if EEP_CONTENT_TachoLDeltaLimit_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t TachoLDeltaLimit_val;
	#elif EEP_CONTENT_TachoLDeltaLimit_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t TachoLDeltaLimit_val;
	#else
	uint32_t TachoLDeltaLimit_val;
	#endif
	TachoLDeltaLimit_val = atoi(argv[2]);
#else // EEP_CONTENT_TachoLDeltaLimit_ITEM_LEN > 1
	#if EEP_CONTENT_TachoLDeltaLimit_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t TachoLDeltaLimit_val[EEP_CONTENT_TachoLDeltaLimit_ITEM_LEN];
	#elif EEP_CONTENT_TachoLDeltaLimit_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t TachoLDeltaLimit_val[EEP_CONTENT_TachoLDeltaLimit_ITEM_LEN];
	#else
	uint32_t TachoLDeltaLimit_val[EEP_CONTENT_TachoLDeltaLimit_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_TachoLDeltaLimit_ITEM_LEN; i ++ )
	{
		TachoLDeltaLimit_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_TachoLDeltaLimit_ITEM_LEN}	
	
	DevEep_Set_TachoLDeltaLimit(eep_handle,TachoLDeltaLimit_val);
}


#if EEP_CONTENT_TachoLDeltaLimit_ITEM_LEN == 1
#if EEP_CONTENT_TachoLDeltaLimit_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_TachoLDeltaLimit_Callback(EEP_HANDLE eep_handle,uint8_t const TachoLDeltaLimit_val)
#elif EEP_CONTENT_TachoLDeltaLimit_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_TachoLDeltaLimit_Callback(EEP_HANDLE eep_handle,uint16_t const TachoLDeltaLimit_val)
#else
void DevEepTest_TachoLDeltaLimit_Callback(EEP_HANDLE eep_handle,uint32_t const TachoLDeltaLimit_val)
#endif	
{
	
	printf("dev eep test: TachoLDeltaLimit, change to dec=%d,hex=0x%08x\r\n",TachoLDeltaLimit_val,TachoLDeltaLimit_val);
}
#else // EEP_CONTENT_TachoLDeltaLimit_ITEM_LEN > 1
#if EEP_CONTENT_TachoLDeltaLimit_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_TachoLDeltaLimit_Callback(EEP_HANDLE eep_handle,uint8_t const * TachoLDeltaLimit_val)
#elif EEP_CONTENT_TachoLDeltaLimit_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_TachoLDeltaLimit_Callback(EEP_HANDLE eep_handle,uint16_t const * TachoLDeltaLimit_val)
#else
void DevEepTest_TachoLDeltaLimit_Callback(EEP_HANDLE eep_handle,uint32_t const * TachoLDeltaLimit_val)
#endif
{
	printf("dev eep test: TachoLDeltaLimit, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_TachoLDeltaLimit_ITEM_LEN; i ++)
	{
		printf("%d ", TachoLDeltaLimit_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_TachoLDeltaLimit_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_TachoLDeltaLimit_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", TachoLDeltaLimit_val[i]);
		#elif EEP_CONTENT_TachoLDeltaLimit_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", TachoLDeltaLimit_val[i]);
		#else
		printf("%08x ", TachoLDeltaLimit_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_TachoUpChangeValue(int argc, char **argv)
{
#if EEP_CONTENT_TachoUpChangeValue_ITEM_LEN == 1
	#if EEP_CONTENT_TachoUpChangeValue_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t TachoUpChangeValue_val;
	#elif EEP_CONTENT_TachoUpChangeValue_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t TachoUpChangeValue_val;
	#else
	uint32_t TachoUpChangeValue_val;
	#endif
	TachoUpChangeValue_val = atoi(argv[2]);
#else // EEP_CONTENT_TachoUpChangeValue_ITEM_LEN > 1
	#if EEP_CONTENT_TachoUpChangeValue_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t TachoUpChangeValue_val[EEP_CONTENT_TachoUpChangeValue_ITEM_LEN];
	#elif EEP_CONTENT_TachoUpChangeValue_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t TachoUpChangeValue_val[EEP_CONTENT_TachoUpChangeValue_ITEM_LEN];
	#else
	uint32_t TachoUpChangeValue_val[EEP_CONTENT_TachoUpChangeValue_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_TachoUpChangeValue_ITEM_LEN; i ++ )
	{
		TachoUpChangeValue_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_TachoUpChangeValue_ITEM_LEN}	
	
	DevEep_Set_TachoUpChangeValue(eep_handle,TachoUpChangeValue_val);
}


#if EEP_CONTENT_TachoUpChangeValue_ITEM_LEN == 1
#if EEP_CONTENT_TachoUpChangeValue_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_TachoUpChangeValue_Callback(EEP_HANDLE eep_handle,uint8_t const TachoUpChangeValue_val)
#elif EEP_CONTENT_TachoUpChangeValue_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_TachoUpChangeValue_Callback(EEP_HANDLE eep_handle,uint16_t const TachoUpChangeValue_val)
#else
void DevEepTest_TachoUpChangeValue_Callback(EEP_HANDLE eep_handle,uint32_t const TachoUpChangeValue_val)
#endif	
{
	
	printf("dev eep test: TachoUpChangeValue, change to dec=%d,hex=0x%08x\r\n",TachoUpChangeValue_val,TachoUpChangeValue_val);
}
#else // EEP_CONTENT_TachoUpChangeValue_ITEM_LEN > 1
#if EEP_CONTENT_TachoUpChangeValue_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_TachoUpChangeValue_Callback(EEP_HANDLE eep_handle,uint8_t const * TachoUpChangeValue_val)
#elif EEP_CONTENT_TachoUpChangeValue_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_TachoUpChangeValue_Callback(EEP_HANDLE eep_handle,uint16_t const * TachoUpChangeValue_val)
#else
void DevEepTest_TachoUpChangeValue_Callback(EEP_HANDLE eep_handle,uint32_t const * TachoUpChangeValue_val)
#endif
{
	printf("dev eep test: TachoUpChangeValue, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_TachoUpChangeValue_ITEM_LEN; i ++)
	{
		printf("%d ", TachoUpChangeValue_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_TachoUpChangeValue_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_TachoUpChangeValue_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", TachoUpChangeValue_val[i]);
		#elif EEP_CONTENT_TachoUpChangeValue_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", TachoUpChangeValue_val[i]);
		#else
		printf("%08x ", TachoUpChangeValue_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_TachoLowChangeValue(int argc, char **argv)
{
#if EEP_CONTENT_TachoLowChangeValue_ITEM_LEN == 1
	#if EEP_CONTENT_TachoLowChangeValue_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t TachoLowChangeValue_val;
	#elif EEP_CONTENT_TachoLowChangeValue_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t TachoLowChangeValue_val;
	#else
	uint32_t TachoLowChangeValue_val;
	#endif
	TachoLowChangeValue_val = atoi(argv[2]);
#else // EEP_CONTENT_TachoLowChangeValue_ITEM_LEN > 1
	#if EEP_CONTENT_TachoLowChangeValue_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t TachoLowChangeValue_val[EEP_CONTENT_TachoLowChangeValue_ITEM_LEN];
	#elif EEP_CONTENT_TachoLowChangeValue_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t TachoLowChangeValue_val[EEP_CONTENT_TachoLowChangeValue_ITEM_LEN];
	#else
	uint32_t TachoLowChangeValue_val[EEP_CONTENT_TachoLowChangeValue_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_TachoLowChangeValue_ITEM_LEN; i ++ )
	{
		TachoLowChangeValue_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_TachoLowChangeValue_ITEM_LEN}	
	
	DevEep_Set_TachoLowChangeValue(eep_handle,TachoLowChangeValue_val);
}


#if EEP_CONTENT_TachoLowChangeValue_ITEM_LEN == 1
#if EEP_CONTENT_TachoLowChangeValue_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_TachoLowChangeValue_Callback(EEP_HANDLE eep_handle,uint8_t const TachoLowChangeValue_val)
#elif EEP_CONTENT_TachoLowChangeValue_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_TachoLowChangeValue_Callback(EEP_HANDLE eep_handle,uint16_t const TachoLowChangeValue_val)
#else
void DevEepTest_TachoLowChangeValue_Callback(EEP_HANDLE eep_handle,uint32_t const TachoLowChangeValue_val)
#endif	
{
	
	printf("dev eep test: TachoLowChangeValue, change to dec=%d,hex=0x%08x\r\n",TachoLowChangeValue_val,TachoLowChangeValue_val);
}
#else // EEP_CONTENT_TachoLowChangeValue_ITEM_LEN > 1
#if EEP_CONTENT_TachoLowChangeValue_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_TachoLowChangeValue_Callback(EEP_HANDLE eep_handle,uint8_t const * TachoLowChangeValue_val)
#elif EEP_CONTENT_TachoLowChangeValue_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_TachoLowChangeValue_Callback(EEP_HANDLE eep_handle,uint16_t const * TachoLowChangeValue_val)
#else
void DevEepTest_TachoLowChangeValue_Callback(EEP_HANDLE eep_handle,uint32_t const * TachoLowChangeValue_val)
#endif
{
	printf("dev eep test: TachoLowChangeValue, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_TachoLowChangeValue_ITEM_LEN; i ++)
	{
		printf("%d ", TachoLowChangeValue_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_TachoLowChangeValue_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_TachoLowChangeValue_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", TachoLowChangeValue_val[i]);
		#elif EEP_CONTENT_TachoLowChangeValue_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", TachoLowChangeValue_val[i]);
		#else
		printf("%08x ", TachoLowChangeValue_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_TachoRun(int argc, char **argv)
{
#if EEP_CONTENT_TachoRun_ITEM_LEN == 1
	#if EEP_CONTENT_TachoRun_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t TachoRun_val;
	#elif EEP_CONTENT_TachoRun_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t TachoRun_val;
	#else
	uint32_t TachoRun_val;
	#endif
	TachoRun_val = atoi(argv[2]);
#else // EEP_CONTENT_TachoRun_ITEM_LEN > 1
	#if EEP_CONTENT_TachoRun_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t TachoRun_val[EEP_CONTENT_TachoRun_ITEM_LEN];
	#elif EEP_CONTENT_TachoRun_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t TachoRun_val[EEP_CONTENT_TachoRun_ITEM_LEN];
	#else
	uint32_t TachoRun_val[EEP_CONTENT_TachoRun_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_TachoRun_ITEM_LEN; i ++ )
	{
		TachoRun_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_TachoRun_ITEM_LEN}	
	
	DevEep_Set_TachoRun(eep_handle,TachoRun_val);
}


#if EEP_CONTENT_TachoRun_ITEM_LEN == 1
#if EEP_CONTENT_TachoRun_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_TachoRun_Callback(EEP_HANDLE eep_handle,uint8_t const TachoRun_val)
#elif EEP_CONTENT_TachoRun_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_TachoRun_Callback(EEP_HANDLE eep_handle,uint16_t const TachoRun_val)
#else
void DevEepTest_TachoRun_Callback(EEP_HANDLE eep_handle,uint32_t const TachoRun_val)
#endif	
{
	
	printf("dev eep test: TachoRun, change to dec=%d,hex=0x%08x\r\n",TachoRun_val,TachoRun_val);
}
#else // EEP_CONTENT_TachoRun_ITEM_LEN > 1
#if EEP_CONTENT_TachoRun_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_TachoRun_Callback(EEP_HANDLE eep_handle,uint8_t const * TachoRun_val)
#elif EEP_CONTENT_TachoRun_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_TachoRun_Callback(EEP_HANDLE eep_handle,uint16_t const * TachoRun_val)
#else
void DevEepTest_TachoRun_Callback(EEP_HANDLE eep_handle,uint32_t const * TachoRun_val)
#endif
{
	printf("dev eep test: TachoRun, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_TachoRun_ITEM_LEN; i ++)
	{
		printf("%d ", TachoRun_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_TachoRun_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_TachoRun_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", TachoRun_val[i]);
		#elif EEP_CONTENT_TachoRun_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", TachoRun_val[i]);
		#else
		printf("%08x ", TachoRun_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_Speed_Rm_1(int argc, char **argv)
{
#if EEP_CONTENT_Speed_Rm_1_ITEM_LEN == 1
	#if EEP_CONTENT_Speed_Rm_1_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t Speed_Rm_1_val;
	#elif EEP_CONTENT_Speed_Rm_1_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t Speed_Rm_1_val;
	#else
	uint32_t Speed_Rm_1_val;
	#endif
	Speed_Rm_1_val = atoi(argv[2]);
#else // EEP_CONTENT_Speed_Rm_1_ITEM_LEN > 1
	#if EEP_CONTENT_Speed_Rm_1_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t Speed_Rm_1_val[EEP_CONTENT_Speed_Rm_1_ITEM_LEN];
	#elif EEP_CONTENT_Speed_Rm_1_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t Speed_Rm_1_val[EEP_CONTENT_Speed_Rm_1_ITEM_LEN];
	#else
	uint32_t Speed_Rm_1_val[EEP_CONTENT_Speed_Rm_1_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_Speed_Rm_1_ITEM_LEN; i ++ )
	{
		Speed_Rm_1_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_Speed_Rm_1_ITEM_LEN}	
	
	DevEep_Set_Speed_Rm_1(eep_handle,Speed_Rm_1_val);
}


#if EEP_CONTENT_Speed_Rm_1_ITEM_LEN == 1
#if EEP_CONTENT_Speed_Rm_1_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_Speed_Rm_1_Callback(EEP_HANDLE eep_handle,uint8_t const Speed_Rm_1_val)
#elif EEP_CONTENT_Speed_Rm_1_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_Speed_Rm_1_Callback(EEP_HANDLE eep_handle,uint16_t const Speed_Rm_1_val)
#else
void DevEepTest_Speed_Rm_1_Callback(EEP_HANDLE eep_handle,uint32_t const Speed_Rm_1_val)
#endif	
{
	
	printf("dev eep test: Speed_Rm_1, change to dec=%d,hex=0x%08x\r\n",Speed_Rm_1_val,Speed_Rm_1_val);
}
#else // EEP_CONTENT_Speed_Rm_1_ITEM_LEN > 1
#if EEP_CONTENT_Speed_Rm_1_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_Speed_Rm_1_Callback(EEP_HANDLE eep_handle,uint8_t const * Speed_Rm_1_val)
#elif EEP_CONTENT_Speed_Rm_1_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_Speed_Rm_1_Callback(EEP_HANDLE eep_handle,uint16_t const * Speed_Rm_1_val)
#else
void DevEepTest_Speed_Rm_1_Callback(EEP_HANDLE eep_handle,uint32_t const * Speed_Rm_1_val)
#endif
{
	printf("dev eep test: Speed_Rm_1, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_Speed_Rm_1_ITEM_LEN; i ++)
	{
		printf("%d ", Speed_Rm_1_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_Speed_Rm_1_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_Speed_Rm_1_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", Speed_Rm_1_val[i]);
		#elif EEP_CONTENT_Speed_Rm_1_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", Speed_Rm_1_val[i]);
		#else
		printf("%08x ", Speed_Rm_1_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_Speed_Rm_2(int argc, char **argv)
{
#if EEP_CONTENT_Speed_Rm_2_ITEM_LEN == 1
	#if EEP_CONTENT_Speed_Rm_2_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t Speed_Rm_2_val;
	#elif EEP_CONTENT_Speed_Rm_2_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t Speed_Rm_2_val;
	#else
	uint32_t Speed_Rm_2_val;
	#endif
	Speed_Rm_2_val = atoi(argv[2]);
#else // EEP_CONTENT_Speed_Rm_2_ITEM_LEN > 1
	#if EEP_CONTENT_Speed_Rm_2_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t Speed_Rm_2_val[EEP_CONTENT_Speed_Rm_2_ITEM_LEN];
	#elif EEP_CONTENT_Speed_Rm_2_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t Speed_Rm_2_val[EEP_CONTENT_Speed_Rm_2_ITEM_LEN];
	#else
	uint32_t Speed_Rm_2_val[EEP_CONTENT_Speed_Rm_2_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_Speed_Rm_2_ITEM_LEN; i ++ )
	{
		Speed_Rm_2_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_Speed_Rm_2_ITEM_LEN}	
	
	DevEep_Set_Speed_Rm_2(eep_handle,Speed_Rm_2_val);
}


#if EEP_CONTENT_Speed_Rm_2_ITEM_LEN == 1
#if EEP_CONTENT_Speed_Rm_2_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_Speed_Rm_2_Callback(EEP_HANDLE eep_handle,uint8_t const Speed_Rm_2_val)
#elif EEP_CONTENT_Speed_Rm_2_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_Speed_Rm_2_Callback(EEP_HANDLE eep_handle,uint16_t const Speed_Rm_2_val)
#else
void DevEepTest_Speed_Rm_2_Callback(EEP_HANDLE eep_handle,uint32_t const Speed_Rm_2_val)
#endif	
{
	
	printf("dev eep test: Speed_Rm_2, change to dec=%d,hex=0x%08x\r\n",Speed_Rm_2_val,Speed_Rm_2_val);
}
#else // EEP_CONTENT_Speed_Rm_2_ITEM_LEN > 1
#if EEP_CONTENT_Speed_Rm_2_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_Speed_Rm_2_Callback(EEP_HANDLE eep_handle,uint8_t const * Speed_Rm_2_val)
#elif EEP_CONTENT_Speed_Rm_2_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_Speed_Rm_2_Callback(EEP_HANDLE eep_handle,uint16_t const * Speed_Rm_2_val)
#else
void DevEepTest_Speed_Rm_2_Callback(EEP_HANDLE eep_handle,uint32_t const * Speed_Rm_2_val)
#endif
{
	printf("dev eep test: Speed_Rm_2, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_Speed_Rm_2_ITEM_LEN; i ++)
	{
		printf("%d ", Speed_Rm_2_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_Speed_Rm_2_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_Speed_Rm_2_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", Speed_Rm_2_val[i]);
		#elif EEP_CONTENT_Speed_Rm_2_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", Speed_Rm_2_val[i]);
		#else
		printf("%08x ", Speed_Rm_2_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_Speed_Rc(int argc, char **argv)
{
#if EEP_CONTENT_Speed_Rc_ITEM_LEN == 1
	#if EEP_CONTENT_Speed_Rc_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t Speed_Rc_val;
	#elif EEP_CONTENT_Speed_Rc_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t Speed_Rc_val;
	#else
	uint32_t Speed_Rc_val;
	#endif
	Speed_Rc_val = atoi(argv[2]);
#else // EEP_CONTENT_Speed_Rc_ITEM_LEN > 1
	#if EEP_CONTENT_Speed_Rc_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t Speed_Rc_val[EEP_CONTENT_Speed_Rc_ITEM_LEN];
	#elif EEP_CONTENT_Speed_Rc_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t Speed_Rc_val[EEP_CONTENT_Speed_Rc_ITEM_LEN];
	#else
	uint32_t Speed_Rc_val[EEP_CONTENT_Speed_Rc_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_Speed_Rc_ITEM_LEN; i ++ )
	{
		Speed_Rc_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_Speed_Rc_ITEM_LEN}	
	
	DevEep_Set_Speed_Rc(eep_handle,Speed_Rc_val);
}


#if EEP_CONTENT_Speed_Rc_ITEM_LEN == 1
#if EEP_CONTENT_Speed_Rc_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_Speed_Rc_Callback(EEP_HANDLE eep_handle,uint8_t const Speed_Rc_val)
#elif EEP_CONTENT_Speed_Rc_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_Speed_Rc_Callback(EEP_HANDLE eep_handle,uint16_t const Speed_Rc_val)
#else
void DevEepTest_Speed_Rc_Callback(EEP_HANDLE eep_handle,uint32_t const Speed_Rc_val)
#endif	
{
	
	printf("dev eep test: Speed_Rc, change to dec=%d,hex=0x%08x\r\n",Speed_Rc_val,Speed_Rc_val);
}
#else // EEP_CONTENT_Speed_Rc_ITEM_LEN > 1
#if EEP_CONTENT_Speed_Rc_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_Speed_Rc_Callback(EEP_HANDLE eep_handle,uint8_t const * Speed_Rc_val)
#elif EEP_CONTENT_Speed_Rc_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_Speed_Rc_Callback(EEP_HANDLE eep_handle,uint16_t const * Speed_Rc_val)
#else
void DevEepTest_Speed_Rc_Callback(EEP_HANDLE eep_handle,uint32_t const * Speed_Rc_val)
#endif
{
	printf("dev eep test: Speed_Rc, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_Speed_Rc_ITEM_LEN; i ++)
	{
		printf("%d ", Speed_Rc_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_Speed_Rc_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_Speed_Rc_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", Speed_Rc_val[i]);
		#elif EEP_CONTENT_Speed_Rc_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", Speed_Rc_val[i]);
		#else
		printf("%08x ", Speed_Rc_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_FuelTelltaleOn_R(int argc, char **argv)
{
#if EEP_CONTENT_FuelTelltaleOn_R_ITEM_LEN == 1
	#if EEP_CONTENT_FuelTelltaleOn_R_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t FuelTelltaleOn_R_val;
	#elif EEP_CONTENT_FuelTelltaleOn_R_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t FuelTelltaleOn_R_val;
	#else
	uint32_t FuelTelltaleOn_R_val;
	#endif
	FuelTelltaleOn_R_val = atoi(argv[2]);
#else // EEP_CONTENT_FuelTelltaleOn_R_ITEM_LEN > 1
	#if EEP_CONTENT_FuelTelltaleOn_R_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t FuelTelltaleOn_R_val[EEP_CONTENT_FuelTelltaleOn_R_ITEM_LEN];
	#elif EEP_CONTENT_FuelTelltaleOn_R_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t FuelTelltaleOn_R_val[EEP_CONTENT_FuelTelltaleOn_R_ITEM_LEN];
	#else
	uint32_t FuelTelltaleOn_R_val[EEP_CONTENT_FuelTelltaleOn_R_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_FuelTelltaleOn_R_ITEM_LEN; i ++ )
	{
		FuelTelltaleOn_R_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_FuelTelltaleOn_R_ITEM_LEN}	
	
	DevEep_Set_FuelTelltaleOn_R(eep_handle,FuelTelltaleOn_R_val);
}


#if EEP_CONTENT_FuelTelltaleOn_R_ITEM_LEN == 1
#if EEP_CONTENT_FuelTelltaleOn_R_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_FuelTelltaleOn_R_Callback(EEP_HANDLE eep_handle,uint8_t const FuelTelltaleOn_R_val)
#elif EEP_CONTENT_FuelTelltaleOn_R_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_FuelTelltaleOn_R_Callback(EEP_HANDLE eep_handle,uint16_t const FuelTelltaleOn_R_val)
#else
void DevEepTest_FuelTelltaleOn_R_Callback(EEP_HANDLE eep_handle,uint32_t const FuelTelltaleOn_R_val)
#endif	
{
	
	printf("dev eep test: FuelTelltaleOn_R, change to dec=%d,hex=0x%08x\r\n",FuelTelltaleOn_R_val,FuelTelltaleOn_R_val);
}
#else // EEP_CONTENT_FuelTelltaleOn_R_ITEM_LEN > 1
#if EEP_CONTENT_FuelTelltaleOn_R_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_FuelTelltaleOn_R_Callback(EEP_HANDLE eep_handle,uint8_t const * FuelTelltaleOn_R_val)
#elif EEP_CONTENT_FuelTelltaleOn_R_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_FuelTelltaleOn_R_Callback(EEP_HANDLE eep_handle,uint16_t const * FuelTelltaleOn_R_val)
#else
void DevEepTest_FuelTelltaleOn_R_Callback(EEP_HANDLE eep_handle,uint32_t const * FuelTelltaleOn_R_val)
#endif
{
	printf("dev eep test: FuelTelltaleOn_R, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_FuelTelltaleOn_R_ITEM_LEN; i ++)
	{
		printf("%d ", FuelTelltaleOn_R_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_FuelTelltaleOn_R_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_FuelTelltaleOn_R_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", FuelTelltaleOn_R_val[i]);
		#elif EEP_CONTENT_FuelTelltaleOn_R_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", FuelTelltaleOn_R_val[i]);
		#else
		printf("%08x ", FuelTelltaleOn_R_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_FuelTelltaleOff_R(int argc, char **argv)
{
#if EEP_CONTENT_FuelTelltaleOff_R_ITEM_LEN == 1
	#if EEP_CONTENT_FuelTelltaleOff_R_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t FuelTelltaleOff_R_val;
	#elif EEP_CONTENT_FuelTelltaleOff_R_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t FuelTelltaleOff_R_val;
	#else
	uint32_t FuelTelltaleOff_R_val;
	#endif
	FuelTelltaleOff_R_val = atoi(argv[2]);
#else // EEP_CONTENT_FuelTelltaleOff_R_ITEM_LEN > 1
	#if EEP_CONTENT_FuelTelltaleOff_R_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t FuelTelltaleOff_R_val[EEP_CONTENT_FuelTelltaleOff_R_ITEM_LEN];
	#elif EEP_CONTENT_FuelTelltaleOff_R_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t FuelTelltaleOff_R_val[EEP_CONTENT_FuelTelltaleOff_R_ITEM_LEN];
	#else
	uint32_t FuelTelltaleOff_R_val[EEP_CONTENT_FuelTelltaleOff_R_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_FuelTelltaleOff_R_ITEM_LEN; i ++ )
	{
		FuelTelltaleOff_R_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_FuelTelltaleOff_R_ITEM_LEN}	
	
	DevEep_Set_FuelTelltaleOff_R(eep_handle,FuelTelltaleOff_R_val);
}


#if EEP_CONTENT_FuelTelltaleOff_R_ITEM_LEN == 1
#if EEP_CONTENT_FuelTelltaleOff_R_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_FuelTelltaleOff_R_Callback(EEP_HANDLE eep_handle,uint8_t const FuelTelltaleOff_R_val)
#elif EEP_CONTENT_FuelTelltaleOff_R_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_FuelTelltaleOff_R_Callback(EEP_HANDLE eep_handle,uint16_t const FuelTelltaleOff_R_val)
#else
void DevEepTest_FuelTelltaleOff_R_Callback(EEP_HANDLE eep_handle,uint32_t const FuelTelltaleOff_R_val)
#endif	
{
	
	printf("dev eep test: FuelTelltaleOff_R, change to dec=%d,hex=0x%08x\r\n",FuelTelltaleOff_R_val,FuelTelltaleOff_R_val);
}
#else // EEP_CONTENT_FuelTelltaleOff_R_ITEM_LEN > 1
#if EEP_CONTENT_FuelTelltaleOff_R_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_FuelTelltaleOff_R_Callback(EEP_HANDLE eep_handle,uint8_t const * FuelTelltaleOff_R_val)
#elif EEP_CONTENT_FuelTelltaleOff_R_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_FuelTelltaleOff_R_Callback(EEP_HANDLE eep_handle,uint16_t const * FuelTelltaleOff_R_val)
#else
void DevEepTest_FuelTelltaleOff_R_Callback(EEP_HANDLE eep_handle,uint32_t const * FuelTelltaleOff_R_val)
#endif
{
	printf("dev eep test: FuelTelltaleOff_R, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_FuelTelltaleOff_R_ITEM_LEN; i ++)
	{
		printf("%d ", FuelTelltaleOff_R_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_FuelTelltaleOff_R_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_FuelTelltaleOff_R_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", FuelTelltaleOff_R_val[i]);
		#elif EEP_CONTENT_FuelTelltaleOff_R_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", FuelTelltaleOff_R_val[i]);
		#else
		printf("%08x ", FuelTelltaleOff_R_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_FuelWarningOnPoint_R(int argc, char **argv)
{
#if EEP_CONTENT_FuelWarningOnPoint_R_ITEM_LEN == 1
	#if EEP_CONTENT_FuelWarningOnPoint_R_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t FuelWarningOnPoint_R_val;
	#elif EEP_CONTENT_FuelWarningOnPoint_R_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t FuelWarningOnPoint_R_val;
	#else
	uint32_t FuelWarningOnPoint_R_val;
	#endif
	FuelWarningOnPoint_R_val = atoi(argv[2]);
#else // EEP_CONTENT_FuelWarningOnPoint_R_ITEM_LEN > 1
	#if EEP_CONTENT_FuelWarningOnPoint_R_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t FuelWarningOnPoint_R_val[EEP_CONTENT_FuelWarningOnPoint_R_ITEM_LEN];
	#elif EEP_CONTENT_FuelWarningOnPoint_R_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t FuelWarningOnPoint_R_val[EEP_CONTENT_FuelWarningOnPoint_R_ITEM_LEN];
	#else
	uint32_t FuelWarningOnPoint_R_val[EEP_CONTENT_FuelWarningOnPoint_R_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_FuelWarningOnPoint_R_ITEM_LEN; i ++ )
	{
		FuelWarningOnPoint_R_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_FuelWarningOnPoint_R_ITEM_LEN}	
	
	DevEep_Set_FuelWarningOnPoint_R(eep_handle,FuelWarningOnPoint_R_val);
}


#if EEP_CONTENT_FuelWarningOnPoint_R_ITEM_LEN == 1
#if EEP_CONTENT_FuelWarningOnPoint_R_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_FuelWarningOnPoint_R_Callback(EEP_HANDLE eep_handle,uint8_t const FuelWarningOnPoint_R_val)
#elif EEP_CONTENT_FuelWarningOnPoint_R_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_FuelWarningOnPoint_R_Callback(EEP_HANDLE eep_handle,uint16_t const FuelWarningOnPoint_R_val)
#else
void DevEepTest_FuelWarningOnPoint_R_Callback(EEP_HANDLE eep_handle,uint32_t const FuelWarningOnPoint_R_val)
#endif	
{
	
	printf("dev eep test: FuelWarningOnPoint_R, change to dec=%d,hex=0x%08x\r\n",FuelWarningOnPoint_R_val,FuelWarningOnPoint_R_val);
}
#else // EEP_CONTENT_FuelWarningOnPoint_R_ITEM_LEN > 1
#if EEP_CONTENT_FuelWarningOnPoint_R_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_FuelWarningOnPoint_R_Callback(EEP_HANDLE eep_handle,uint8_t const * FuelWarningOnPoint_R_val)
#elif EEP_CONTENT_FuelWarningOnPoint_R_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_FuelWarningOnPoint_R_Callback(EEP_HANDLE eep_handle,uint16_t const * FuelWarningOnPoint_R_val)
#else
void DevEepTest_FuelWarningOnPoint_R_Callback(EEP_HANDLE eep_handle,uint32_t const * FuelWarningOnPoint_R_val)
#endif
{
	printf("dev eep test: FuelWarningOnPoint_R, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_FuelWarningOnPoint_R_ITEM_LEN; i ++)
	{
		printf("%d ", FuelWarningOnPoint_R_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_FuelWarningOnPoint_R_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_FuelWarningOnPoint_R_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", FuelWarningOnPoint_R_val[i]);
		#elif EEP_CONTENT_FuelWarningOnPoint_R_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", FuelWarningOnPoint_R_val[i]);
		#else
		printf("%08x ", FuelWarningOnPoint_R_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_Fuel_R_Open(int argc, char **argv)
{
#if EEP_CONTENT_Fuel_R_Open_ITEM_LEN == 1
	#if EEP_CONTENT_Fuel_R_Open_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t Fuel_R_Open_val;
	#elif EEP_CONTENT_Fuel_R_Open_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t Fuel_R_Open_val;
	#else
	uint32_t Fuel_R_Open_val;
	#endif
	Fuel_R_Open_val = atoi(argv[2]);
#else // EEP_CONTENT_Fuel_R_Open_ITEM_LEN > 1
	#if EEP_CONTENT_Fuel_R_Open_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t Fuel_R_Open_val[EEP_CONTENT_Fuel_R_Open_ITEM_LEN];
	#elif EEP_CONTENT_Fuel_R_Open_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t Fuel_R_Open_val[EEP_CONTENT_Fuel_R_Open_ITEM_LEN];
	#else
	uint32_t Fuel_R_Open_val[EEP_CONTENT_Fuel_R_Open_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_Fuel_R_Open_ITEM_LEN; i ++ )
	{
		Fuel_R_Open_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_Fuel_R_Open_ITEM_LEN}	
	
	DevEep_Set_Fuel_R_Open(eep_handle,Fuel_R_Open_val);
}


#if EEP_CONTENT_Fuel_R_Open_ITEM_LEN == 1
#if EEP_CONTENT_Fuel_R_Open_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_Fuel_R_Open_Callback(EEP_HANDLE eep_handle,uint8_t const Fuel_R_Open_val)
#elif EEP_CONTENT_Fuel_R_Open_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_Fuel_R_Open_Callback(EEP_HANDLE eep_handle,uint16_t const Fuel_R_Open_val)
#else
void DevEepTest_Fuel_R_Open_Callback(EEP_HANDLE eep_handle,uint32_t const Fuel_R_Open_val)
#endif	
{
	
	printf("dev eep test: Fuel_R_Open, change to dec=%d,hex=0x%08x\r\n",Fuel_R_Open_val,Fuel_R_Open_val);
}
#else // EEP_CONTENT_Fuel_R_Open_ITEM_LEN > 1
#if EEP_CONTENT_Fuel_R_Open_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_Fuel_R_Open_Callback(EEP_HANDLE eep_handle,uint8_t const * Fuel_R_Open_val)
#elif EEP_CONTENT_Fuel_R_Open_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_Fuel_R_Open_Callback(EEP_HANDLE eep_handle,uint16_t const * Fuel_R_Open_val)
#else
void DevEepTest_Fuel_R_Open_Callback(EEP_HANDLE eep_handle,uint32_t const * Fuel_R_Open_val)
#endif
{
	printf("dev eep test: Fuel_R_Open, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_Fuel_R_Open_ITEM_LEN; i ++)
	{
		printf("%d ", Fuel_R_Open_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_Fuel_R_Open_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_Fuel_R_Open_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", Fuel_R_Open_val[i]);
		#elif EEP_CONTENT_Fuel_R_Open_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", Fuel_R_Open_val[i]);
		#else
		printf("%08x ", Fuel_R_Open_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_Fuel_R_Short(int argc, char **argv)
{
#if EEP_CONTENT_Fuel_R_Short_ITEM_LEN == 1
	#if EEP_CONTENT_Fuel_R_Short_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t Fuel_R_Short_val;
	#elif EEP_CONTENT_Fuel_R_Short_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t Fuel_R_Short_val;
	#else
	uint32_t Fuel_R_Short_val;
	#endif
	Fuel_R_Short_val = atoi(argv[2]);
#else // EEP_CONTENT_Fuel_R_Short_ITEM_LEN > 1
	#if EEP_CONTENT_Fuel_R_Short_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t Fuel_R_Short_val[EEP_CONTENT_Fuel_R_Short_ITEM_LEN];
	#elif EEP_CONTENT_Fuel_R_Short_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t Fuel_R_Short_val[EEP_CONTENT_Fuel_R_Short_ITEM_LEN];
	#else
	uint32_t Fuel_R_Short_val[EEP_CONTENT_Fuel_R_Short_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_Fuel_R_Short_ITEM_LEN; i ++ )
	{
		Fuel_R_Short_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_Fuel_R_Short_ITEM_LEN}	
	
	DevEep_Set_Fuel_R_Short(eep_handle,Fuel_R_Short_val);
}


#if EEP_CONTENT_Fuel_R_Short_ITEM_LEN == 1
#if EEP_CONTENT_Fuel_R_Short_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_Fuel_R_Short_Callback(EEP_HANDLE eep_handle,uint8_t const Fuel_R_Short_val)
#elif EEP_CONTENT_Fuel_R_Short_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_Fuel_R_Short_Callback(EEP_HANDLE eep_handle,uint16_t const Fuel_R_Short_val)
#else
void DevEepTest_Fuel_R_Short_Callback(EEP_HANDLE eep_handle,uint32_t const Fuel_R_Short_val)
#endif	
{
	
	printf("dev eep test: Fuel_R_Short, change to dec=%d,hex=0x%08x\r\n",Fuel_R_Short_val,Fuel_R_Short_val);
}
#else // EEP_CONTENT_Fuel_R_Short_ITEM_LEN > 1
#if EEP_CONTENT_Fuel_R_Short_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_Fuel_R_Short_Callback(EEP_HANDLE eep_handle,uint8_t const * Fuel_R_Short_val)
#elif EEP_CONTENT_Fuel_R_Short_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_Fuel_R_Short_Callback(EEP_HANDLE eep_handle,uint16_t const * Fuel_R_Short_val)
#else
void DevEepTest_Fuel_R_Short_Callback(EEP_HANDLE eep_handle,uint32_t const * Fuel_R_Short_val)
#endif
{
	printf("dev eep test: Fuel_R_Short, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_Fuel_R_Short_ITEM_LEN; i ++)
	{
		printf("%d ", Fuel_R_Short_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_Fuel_R_Short_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_Fuel_R_Short_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", Fuel_R_Short_val[i]);
		#elif EEP_CONTENT_Fuel_R_Short_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", Fuel_R_Short_val[i]);
		#else
		printf("%08x ", Fuel_R_Short_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_PkbWarningJudgeV1(int argc, char **argv)
{
#if EEP_CONTENT_PkbWarningJudgeV1_ITEM_LEN == 1
	#if EEP_CONTENT_PkbWarningJudgeV1_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t PkbWarningJudgeV1_val;
	#elif EEP_CONTENT_PkbWarningJudgeV1_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t PkbWarningJudgeV1_val;
	#else
	uint32_t PkbWarningJudgeV1_val;
	#endif
	PkbWarningJudgeV1_val = atoi(argv[2]);
#else // EEP_CONTENT_PkbWarningJudgeV1_ITEM_LEN > 1
	#if EEP_CONTENT_PkbWarningJudgeV1_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t PkbWarningJudgeV1_val[EEP_CONTENT_PkbWarningJudgeV1_ITEM_LEN];
	#elif EEP_CONTENT_PkbWarningJudgeV1_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t PkbWarningJudgeV1_val[EEP_CONTENT_PkbWarningJudgeV1_ITEM_LEN];
	#else
	uint32_t PkbWarningJudgeV1_val[EEP_CONTENT_PkbWarningJudgeV1_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_PkbWarningJudgeV1_ITEM_LEN; i ++ )
	{
		PkbWarningJudgeV1_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_PkbWarningJudgeV1_ITEM_LEN}	
	
	DevEep_Set_PkbWarningJudgeV1(eep_handle,PkbWarningJudgeV1_val);
}


#if EEP_CONTENT_PkbWarningJudgeV1_ITEM_LEN == 1
#if EEP_CONTENT_PkbWarningJudgeV1_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_PkbWarningJudgeV1_Callback(EEP_HANDLE eep_handle,uint8_t const PkbWarningJudgeV1_val)
#elif EEP_CONTENT_PkbWarningJudgeV1_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_PkbWarningJudgeV1_Callback(EEP_HANDLE eep_handle,uint16_t const PkbWarningJudgeV1_val)
#else
void DevEepTest_PkbWarningJudgeV1_Callback(EEP_HANDLE eep_handle,uint32_t const PkbWarningJudgeV1_val)
#endif	
{
	
	printf("dev eep test: PkbWarningJudgeV1, change to dec=%d,hex=0x%08x\r\n",PkbWarningJudgeV1_val,PkbWarningJudgeV1_val);
}
#else // EEP_CONTENT_PkbWarningJudgeV1_ITEM_LEN > 1
#if EEP_CONTENT_PkbWarningJudgeV1_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_PkbWarningJudgeV1_Callback(EEP_HANDLE eep_handle,uint8_t const * PkbWarningJudgeV1_val)
#elif EEP_CONTENT_PkbWarningJudgeV1_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_PkbWarningJudgeV1_Callback(EEP_HANDLE eep_handle,uint16_t const * PkbWarningJudgeV1_val)
#else
void DevEepTest_PkbWarningJudgeV1_Callback(EEP_HANDLE eep_handle,uint32_t const * PkbWarningJudgeV1_val)
#endif
{
	printf("dev eep test: PkbWarningJudgeV1, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_PkbWarningJudgeV1_ITEM_LEN; i ++)
	{
		printf("%d ", PkbWarningJudgeV1_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_PkbWarningJudgeV1_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_PkbWarningJudgeV1_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", PkbWarningJudgeV1_val[i]);
		#elif EEP_CONTENT_PkbWarningJudgeV1_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", PkbWarningJudgeV1_val[i]);
		#else
		printf("%08x ", PkbWarningJudgeV1_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_PkbWarningJudgeV2(int argc, char **argv)
{
#if EEP_CONTENT_PkbWarningJudgeV2_ITEM_LEN == 1
	#if EEP_CONTENT_PkbWarningJudgeV2_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t PkbWarningJudgeV2_val;
	#elif EEP_CONTENT_PkbWarningJudgeV2_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t PkbWarningJudgeV2_val;
	#else
	uint32_t PkbWarningJudgeV2_val;
	#endif
	PkbWarningJudgeV2_val = atoi(argv[2]);
#else // EEP_CONTENT_PkbWarningJudgeV2_ITEM_LEN > 1
	#if EEP_CONTENT_PkbWarningJudgeV2_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t PkbWarningJudgeV2_val[EEP_CONTENT_PkbWarningJudgeV2_ITEM_LEN];
	#elif EEP_CONTENT_PkbWarningJudgeV2_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t PkbWarningJudgeV2_val[EEP_CONTENT_PkbWarningJudgeV2_ITEM_LEN];
	#else
	uint32_t PkbWarningJudgeV2_val[EEP_CONTENT_PkbWarningJudgeV2_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_PkbWarningJudgeV2_ITEM_LEN; i ++ )
	{
		PkbWarningJudgeV2_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_PkbWarningJudgeV2_ITEM_LEN}	
	
	DevEep_Set_PkbWarningJudgeV2(eep_handle,PkbWarningJudgeV2_val);
}


#if EEP_CONTENT_PkbWarningJudgeV2_ITEM_LEN == 1
#if EEP_CONTENT_PkbWarningJudgeV2_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_PkbWarningJudgeV2_Callback(EEP_HANDLE eep_handle,uint8_t const PkbWarningJudgeV2_val)
#elif EEP_CONTENT_PkbWarningJudgeV2_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_PkbWarningJudgeV2_Callback(EEP_HANDLE eep_handle,uint16_t const PkbWarningJudgeV2_val)
#else
void DevEepTest_PkbWarningJudgeV2_Callback(EEP_HANDLE eep_handle,uint32_t const PkbWarningJudgeV2_val)
#endif	
{
	
	printf("dev eep test: PkbWarningJudgeV2, change to dec=%d,hex=0x%08x\r\n",PkbWarningJudgeV2_val,PkbWarningJudgeV2_val);
}
#else // EEP_CONTENT_PkbWarningJudgeV2_ITEM_LEN > 1
#if EEP_CONTENT_PkbWarningJudgeV2_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_PkbWarningJudgeV2_Callback(EEP_HANDLE eep_handle,uint8_t const * PkbWarningJudgeV2_val)
#elif EEP_CONTENT_PkbWarningJudgeV2_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_PkbWarningJudgeV2_Callback(EEP_HANDLE eep_handle,uint16_t const * PkbWarningJudgeV2_val)
#else
void DevEepTest_PkbWarningJudgeV2_Callback(EEP_HANDLE eep_handle,uint32_t const * PkbWarningJudgeV2_val)
#endif
{
	printf("dev eep test: PkbWarningJudgeV2, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_PkbWarningJudgeV2_ITEM_LEN; i ++)
	{
		printf("%d ", PkbWarningJudgeV2_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_PkbWarningJudgeV2_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_PkbWarningJudgeV2_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", PkbWarningJudgeV2_val[i]);
		#elif EEP_CONTENT_PkbWarningJudgeV2_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", PkbWarningJudgeV2_val[i]);
		#else
		printf("%08x ", PkbWarningJudgeV2_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_AudioEepTest(int argc, char **argv)
{
#if EEP_CONTENT_AudioEepTest_ITEM_LEN == 1
	#if EEP_CONTENT_AudioEepTest_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t AudioEepTest_val;
	#elif EEP_CONTENT_AudioEepTest_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t AudioEepTest_val;
	#else
	uint32_t AudioEepTest_val;
	#endif
	AudioEepTest_val = atoi(argv[2]);
#else // EEP_CONTENT_AudioEepTest_ITEM_LEN > 1
	#if EEP_CONTENT_AudioEepTest_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t AudioEepTest_val[EEP_CONTENT_AudioEepTest_ITEM_LEN];
	#elif EEP_CONTENT_AudioEepTest_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t AudioEepTest_val[EEP_CONTENT_AudioEepTest_ITEM_LEN];
	#else
	uint32_t AudioEepTest_val[EEP_CONTENT_AudioEepTest_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_AudioEepTest_ITEM_LEN; i ++ )
	{
		AudioEepTest_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_AudioEepTest_ITEM_LEN}	
	
	DevEep_Set_AudioEepTest(eep_handle,AudioEepTest_val);
}


#if EEP_CONTENT_AudioEepTest_ITEM_LEN == 1
#if EEP_CONTENT_AudioEepTest_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_AudioEepTest_Callback(EEP_HANDLE eep_handle,uint8_t const AudioEepTest_val)
#elif EEP_CONTENT_AudioEepTest_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_AudioEepTest_Callback(EEP_HANDLE eep_handle,uint16_t const AudioEepTest_val)
#else
void DevEepTest_AudioEepTest_Callback(EEP_HANDLE eep_handle,uint32_t const AudioEepTest_val)
#endif	
{
	
	printf("dev eep test: AudioEepTest, change to dec=%d,hex=0x%08x\r\n",AudioEepTest_val,AudioEepTest_val);
}
#else // EEP_CONTENT_AudioEepTest_ITEM_LEN > 1
#if EEP_CONTENT_AudioEepTest_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_AudioEepTest_Callback(EEP_HANDLE eep_handle,uint8_t const * AudioEepTest_val)
#elif EEP_CONTENT_AudioEepTest_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_AudioEepTest_Callback(EEP_HANDLE eep_handle,uint16_t const * AudioEepTest_val)
#else
void DevEepTest_AudioEepTest_Callback(EEP_HANDLE eep_handle,uint32_t const * AudioEepTest_val)
#endif
{
	printf("dev eep test: AudioEepTest, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_AudioEepTest_ITEM_LEN; i ++)
	{
		printf("%d ", AudioEepTest_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_AudioEepTest_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_AudioEepTest_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", AudioEepTest_val[i]);
		#elif EEP_CONTENT_AudioEepTest_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", AudioEepTest_val[i]);
		#else
		printf("%08x ", AudioEepTest_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_DspKeyCodeOnOff(int argc, char **argv)
{
#if EEP_CONTENT_DspKeyCodeOnOff_ITEM_LEN == 1
	#if EEP_CONTENT_DspKeyCodeOnOff_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t DspKeyCodeOnOff_val;
	#elif EEP_CONTENT_DspKeyCodeOnOff_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t DspKeyCodeOnOff_val;
	#else
	uint32_t DspKeyCodeOnOff_val;
	#endif
	DspKeyCodeOnOff_val = atoi(argv[2]);
#else // EEP_CONTENT_DspKeyCodeOnOff_ITEM_LEN > 1
	#if EEP_CONTENT_DspKeyCodeOnOff_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t DspKeyCodeOnOff_val[EEP_CONTENT_DspKeyCodeOnOff_ITEM_LEN];
	#elif EEP_CONTENT_DspKeyCodeOnOff_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t DspKeyCodeOnOff_val[EEP_CONTENT_DspKeyCodeOnOff_ITEM_LEN];
	#else
	uint32_t DspKeyCodeOnOff_val[EEP_CONTENT_DspKeyCodeOnOff_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_DspKeyCodeOnOff_ITEM_LEN; i ++ )
	{
		DspKeyCodeOnOff_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_DspKeyCodeOnOff_ITEM_LEN}	
	
	DevEep_Set_DspKeyCodeOnOff(eep_handle,DspKeyCodeOnOff_val);
}


#if EEP_CONTENT_DspKeyCodeOnOff_ITEM_LEN == 1
#if EEP_CONTENT_DspKeyCodeOnOff_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_DspKeyCodeOnOff_Callback(EEP_HANDLE eep_handle,uint8_t const DspKeyCodeOnOff_val)
#elif EEP_CONTENT_DspKeyCodeOnOff_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_DspKeyCodeOnOff_Callback(EEP_HANDLE eep_handle,uint16_t const DspKeyCodeOnOff_val)
#else
void DevEepTest_DspKeyCodeOnOff_Callback(EEP_HANDLE eep_handle,uint32_t const DspKeyCodeOnOff_val)
#endif	
{
	
	printf("dev eep test: DspKeyCodeOnOff, change to dec=%d,hex=0x%08x\r\n",DspKeyCodeOnOff_val,DspKeyCodeOnOff_val);
}
#else // EEP_CONTENT_DspKeyCodeOnOff_ITEM_LEN > 1
#if EEP_CONTENT_DspKeyCodeOnOff_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_DspKeyCodeOnOff_Callback(EEP_HANDLE eep_handle,uint8_t const * DspKeyCodeOnOff_val)
#elif EEP_CONTENT_DspKeyCodeOnOff_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_DspKeyCodeOnOff_Callback(EEP_HANDLE eep_handle,uint16_t const * DspKeyCodeOnOff_val)
#else
void DevEepTest_DspKeyCodeOnOff_Callback(EEP_HANDLE eep_handle,uint32_t const * DspKeyCodeOnOff_val)
#endif
{
	printf("dev eep test: DspKeyCodeOnOff, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_DspKeyCodeOnOff_ITEM_LEN; i ++)
	{
		printf("%d ", DspKeyCodeOnOff_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_DspKeyCodeOnOff_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_DspKeyCodeOnOff_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", DspKeyCodeOnOff_val[i]);
		#elif EEP_CONTENT_DspKeyCodeOnOff_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", DspKeyCodeOnOff_val[i]);
		#else
		printf("%08x ", DspKeyCodeOnOff_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_DspKeyCode(int argc, char **argv)
{
#if EEP_CONTENT_DspKeyCode_ITEM_LEN == 1
	#if EEP_CONTENT_DspKeyCode_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t DspKeyCode_val;
	#elif EEP_CONTENT_DspKeyCode_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t DspKeyCode_val;
	#else
	uint32_t DspKeyCode_val;
	#endif
	DspKeyCode_val = atoi(argv[2]);
#else // EEP_CONTENT_DspKeyCode_ITEM_LEN > 1
	#if EEP_CONTENT_DspKeyCode_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t DspKeyCode_val[EEP_CONTENT_DspKeyCode_ITEM_LEN];
	#elif EEP_CONTENT_DspKeyCode_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t DspKeyCode_val[EEP_CONTENT_DspKeyCode_ITEM_LEN];
	#else
	uint32_t DspKeyCode_val[EEP_CONTENT_DspKeyCode_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_DspKeyCode_ITEM_LEN; i ++ )
	{
		DspKeyCode_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_DspKeyCode_ITEM_LEN}	
	
	DevEep_Set_DspKeyCode(eep_handle,DspKeyCode_val);
}


#if EEP_CONTENT_DspKeyCode_ITEM_LEN == 1
#if EEP_CONTENT_DspKeyCode_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_DspKeyCode_Callback(EEP_HANDLE eep_handle,uint8_t const DspKeyCode_val)
#elif EEP_CONTENT_DspKeyCode_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_DspKeyCode_Callback(EEP_HANDLE eep_handle,uint16_t const DspKeyCode_val)
#else
void DevEepTest_DspKeyCode_Callback(EEP_HANDLE eep_handle,uint32_t const DspKeyCode_val)
#endif	
{
	
	printf("dev eep test: DspKeyCode, change to dec=%d,hex=0x%08x\r\n",DspKeyCode_val,DspKeyCode_val);
}
#else // EEP_CONTENT_DspKeyCode_ITEM_LEN > 1
#if EEP_CONTENT_DspKeyCode_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_DspKeyCode_Callback(EEP_HANDLE eep_handle,uint8_t const * DspKeyCode_val)
#elif EEP_CONTENT_DspKeyCode_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_DspKeyCode_Callback(EEP_HANDLE eep_handle,uint16_t const * DspKeyCode_val)
#else
void DevEepTest_DspKeyCode_Callback(EEP_HANDLE eep_handle,uint32_t const * DspKeyCode_val)
#endif
{
	printf("dev eep test: DspKeyCode, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_DspKeyCode_ITEM_LEN; i ++)
	{
		printf("%d ", DspKeyCode_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_DspKeyCode_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_DspKeyCode_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", DspKeyCode_val[i]);
		#elif EEP_CONTENT_DspKeyCode_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", DspKeyCode_val[i]);
		#else
		printf("%08x ", DspKeyCode_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_BatState_ChgDly(int argc, char **argv)
{
#if EEP_CONTENT_BatState_ChgDly_ITEM_LEN == 1
	#if EEP_CONTENT_BatState_ChgDly_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t BatState_ChgDly_val;
	#elif EEP_CONTENT_BatState_ChgDly_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t BatState_ChgDly_val;
	#else
	uint32_t BatState_ChgDly_val;
	#endif
	BatState_ChgDly_val = atoi(argv[2]);
#else // EEP_CONTENT_BatState_ChgDly_ITEM_LEN > 1
	#if EEP_CONTENT_BatState_ChgDly_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t BatState_ChgDly_val[EEP_CONTENT_BatState_ChgDly_ITEM_LEN];
	#elif EEP_CONTENT_BatState_ChgDly_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t BatState_ChgDly_val[EEP_CONTENT_BatState_ChgDly_ITEM_LEN];
	#else
	uint32_t BatState_ChgDly_val[EEP_CONTENT_BatState_ChgDly_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_BatState_ChgDly_ITEM_LEN; i ++ )
	{
		BatState_ChgDly_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_BatState_ChgDly_ITEM_LEN}	
	
	DevEep_Set_BatState_ChgDly(eep_handle,BatState_ChgDly_val);
}


#if EEP_CONTENT_BatState_ChgDly_ITEM_LEN == 1
#if EEP_CONTENT_BatState_ChgDly_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_BatState_ChgDly_Callback(EEP_HANDLE eep_handle,uint8_t const BatState_ChgDly_val)
#elif EEP_CONTENT_BatState_ChgDly_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_BatState_ChgDly_Callback(EEP_HANDLE eep_handle,uint16_t const BatState_ChgDly_val)
#else
void DevEepTest_BatState_ChgDly_Callback(EEP_HANDLE eep_handle,uint32_t const BatState_ChgDly_val)
#endif	
{
	
	printf("dev eep test: BatState_ChgDly, change to dec=%d,hex=0x%08x\r\n",BatState_ChgDly_val,BatState_ChgDly_val);
}
#else // EEP_CONTENT_BatState_ChgDly_ITEM_LEN > 1
#if EEP_CONTENT_BatState_ChgDly_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_BatState_ChgDly_Callback(EEP_HANDLE eep_handle,uint8_t const * BatState_ChgDly_val)
#elif EEP_CONTENT_BatState_ChgDly_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_BatState_ChgDly_Callback(EEP_HANDLE eep_handle,uint16_t const * BatState_ChgDly_val)
#else
void DevEepTest_BatState_ChgDly_Callback(EEP_HANDLE eep_handle,uint32_t const * BatState_ChgDly_val)
#endif
{
	printf("dev eep test: BatState_ChgDly, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_BatState_ChgDly_ITEM_LEN; i ++)
	{
		printf("%d ", BatState_ChgDly_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_BatState_ChgDly_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_BatState_ChgDly_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", BatState_ChgDly_val[i]);
		#elif EEP_CONTENT_BatState_ChgDly_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", BatState_ChgDly_val[i]);
		#else
		printf("%08x ", BatState_ChgDly_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_BatVolVeryHigh_Hysteresis_H(int argc, char **argv)
{
#if EEP_CONTENT_BatVolVeryHigh_Hysteresis_H_ITEM_LEN == 1
	#if EEP_CONTENT_BatVolVeryHigh_Hysteresis_H_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t BatVolVeryHigh_Hysteresis_H_val;
	#elif EEP_CONTENT_BatVolVeryHigh_Hysteresis_H_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t BatVolVeryHigh_Hysteresis_H_val;
	#else
	uint32_t BatVolVeryHigh_Hysteresis_H_val;
	#endif
	BatVolVeryHigh_Hysteresis_H_val = atoi(argv[2]);
#else // EEP_CONTENT_BatVolVeryHigh_Hysteresis_H_ITEM_LEN > 1
	#if EEP_CONTENT_BatVolVeryHigh_Hysteresis_H_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t BatVolVeryHigh_Hysteresis_H_val[EEP_CONTENT_BatVolVeryHigh_Hysteresis_H_ITEM_LEN];
	#elif EEP_CONTENT_BatVolVeryHigh_Hysteresis_H_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t BatVolVeryHigh_Hysteresis_H_val[EEP_CONTENT_BatVolVeryHigh_Hysteresis_H_ITEM_LEN];
	#else
	uint32_t BatVolVeryHigh_Hysteresis_H_val[EEP_CONTENT_BatVolVeryHigh_Hysteresis_H_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_BatVolVeryHigh_Hysteresis_H_ITEM_LEN; i ++ )
	{
		BatVolVeryHigh_Hysteresis_H_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_BatVolVeryHigh_Hysteresis_H_ITEM_LEN}	
	
	DevEep_Set_BatVolVeryHigh_Hysteresis_H(eep_handle,BatVolVeryHigh_Hysteresis_H_val);
}


#if EEP_CONTENT_BatVolVeryHigh_Hysteresis_H_ITEM_LEN == 1
#if EEP_CONTENT_BatVolVeryHigh_Hysteresis_H_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_BatVolVeryHigh_Hysteresis_H_Callback(EEP_HANDLE eep_handle,uint8_t const BatVolVeryHigh_Hysteresis_H_val)
#elif EEP_CONTENT_BatVolVeryHigh_Hysteresis_H_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_BatVolVeryHigh_Hysteresis_H_Callback(EEP_HANDLE eep_handle,uint16_t const BatVolVeryHigh_Hysteresis_H_val)
#else
void DevEepTest_BatVolVeryHigh_Hysteresis_H_Callback(EEP_HANDLE eep_handle,uint32_t const BatVolVeryHigh_Hysteresis_H_val)
#endif	
{
	
	printf("dev eep test: BatVolVeryHigh_Hysteresis_H, change to dec=%d,hex=0x%08x\r\n",BatVolVeryHigh_Hysteresis_H_val,BatVolVeryHigh_Hysteresis_H_val);
}
#else // EEP_CONTENT_BatVolVeryHigh_Hysteresis_H_ITEM_LEN > 1
#if EEP_CONTENT_BatVolVeryHigh_Hysteresis_H_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_BatVolVeryHigh_Hysteresis_H_Callback(EEP_HANDLE eep_handle,uint8_t const * BatVolVeryHigh_Hysteresis_H_val)
#elif EEP_CONTENT_BatVolVeryHigh_Hysteresis_H_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_BatVolVeryHigh_Hysteresis_H_Callback(EEP_HANDLE eep_handle,uint16_t const * BatVolVeryHigh_Hysteresis_H_val)
#else
void DevEepTest_BatVolVeryHigh_Hysteresis_H_Callback(EEP_HANDLE eep_handle,uint32_t const * BatVolVeryHigh_Hysteresis_H_val)
#endif
{
	printf("dev eep test: BatVolVeryHigh_Hysteresis_H, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_BatVolVeryHigh_Hysteresis_H_ITEM_LEN; i ++)
	{
		printf("%d ", BatVolVeryHigh_Hysteresis_H_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_BatVolVeryHigh_Hysteresis_H_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_BatVolVeryHigh_Hysteresis_H_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", BatVolVeryHigh_Hysteresis_H_val[i]);
		#elif EEP_CONTENT_BatVolVeryHigh_Hysteresis_H_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", BatVolVeryHigh_Hysteresis_H_val[i]);
		#else
		printf("%08x ", BatVolVeryHigh_Hysteresis_H_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_BatVolVeryHigh_Hysteresis_L(int argc, char **argv)
{
#if EEP_CONTENT_BatVolVeryHigh_Hysteresis_L_ITEM_LEN == 1
	#if EEP_CONTENT_BatVolVeryHigh_Hysteresis_L_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t BatVolVeryHigh_Hysteresis_L_val;
	#elif EEP_CONTENT_BatVolVeryHigh_Hysteresis_L_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t BatVolVeryHigh_Hysteresis_L_val;
	#else
	uint32_t BatVolVeryHigh_Hysteresis_L_val;
	#endif
	BatVolVeryHigh_Hysteresis_L_val = atoi(argv[2]);
#else // EEP_CONTENT_BatVolVeryHigh_Hysteresis_L_ITEM_LEN > 1
	#if EEP_CONTENT_BatVolVeryHigh_Hysteresis_L_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t BatVolVeryHigh_Hysteresis_L_val[EEP_CONTENT_BatVolVeryHigh_Hysteresis_L_ITEM_LEN];
	#elif EEP_CONTENT_BatVolVeryHigh_Hysteresis_L_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t BatVolVeryHigh_Hysteresis_L_val[EEP_CONTENT_BatVolVeryHigh_Hysteresis_L_ITEM_LEN];
	#else
	uint32_t BatVolVeryHigh_Hysteresis_L_val[EEP_CONTENT_BatVolVeryHigh_Hysteresis_L_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_BatVolVeryHigh_Hysteresis_L_ITEM_LEN; i ++ )
	{
		BatVolVeryHigh_Hysteresis_L_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_BatVolVeryHigh_Hysteresis_L_ITEM_LEN}	
	
	DevEep_Set_BatVolVeryHigh_Hysteresis_L(eep_handle,BatVolVeryHigh_Hysteresis_L_val);
}


#if EEP_CONTENT_BatVolVeryHigh_Hysteresis_L_ITEM_LEN == 1
#if EEP_CONTENT_BatVolVeryHigh_Hysteresis_L_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_BatVolVeryHigh_Hysteresis_L_Callback(EEP_HANDLE eep_handle,uint8_t const BatVolVeryHigh_Hysteresis_L_val)
#elif EEP_CONTENT_BatVolVeryHigh_Hysteresis_L_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_BatVolVeryHigh_Hysteresis_L_Callback(EEP_HANDLE eep_handle,uint16_t const BatVolVeryHigh_Hysteresis_L_val)
#else
void DevEepTest_BatVolVeryHigh_Hysteresis_L_Callback(EEP_HANDLE eep_handle,uint32_t const BatVolVeryHigh_Hysteresis_L_val)
#endif	
{
	
	printf("dev eep test: BatVolVeryHigh_Hysteresis_L, change to dec=%d,hex=0x%08x\r\n",BatVolVeryHigh_Hysteresis_L_val,BatVolVeryHigh_Hysteresis_L_val);
}
#else // EEP_CONTENT_BatVolVeryHigh_Hysteresis_L_ITEM_LEN > 1
#if EEP_CONTENT_BatVolVeryHigh_Hysteresis_L_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_BatVolVeryHigh_Hysteresis_L_Callback(EEP_HANDLE eep_handle,uint8_t const * BatVolVeryHigh_Hysteresis_L_val)
#elif EEP_CONTENT_BatVolVeryHigh_Hysteresis_L_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_BatVolVeryHigh_Hysteresis_L_Callback(EEP_HANDLE eep_handle,uint16_t const * BatVolVeryHigh_Hysteresis_L_val)
#else
void DevEepTest_BatVolVeryHigh_Hysteresis_L_Callback(EEP_HANDLE eep_handle,uint32_t const * BatVolVeryHigh_Hysteresis_L_val)
#endif
{
	printf("dev eep test: BatVolVeryHigh_Hysteresis_L, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_BatVolVeryHigh_Hysteresis_L_ITEM_LEN; i ++)
	{
		printf("%d ", BatVolVeryHigh_Hysteresis_L_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_BatVolVeryHigh_Hysteresis_L_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_BatVolVeryHigh_Hysteresis_L_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", BatVolVeryHigh_Hysteresis_L_val[i]);
		#elif EEP_CONTENT_BatVolVeryHigh_Hysteresis_L_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", BatVolVeryHigh_Hysteresis_L_val[i]);
		#else
		printf("%08x ", BatVolVeryHigh_Hysteresis_L_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_BatVolHigh_Hysteresis_H(int argc, char **argv)
{
#if EEP_CONTENT_BatVolHigh_Hysteresis_H_ITEM_LEN == 1
	#if EEP_CONTENT_BatVolHigh_Hysteresis_H_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t BatVolHigh_Hysteresis_H_val;
	#elif EEP_CONTENT_BatVolHigh_Hysteresis_H_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t BatVolHigh_Hysteresis_H_val;
	#else
	uint32_t BatVolHigh_Hysteresis_H_val;
	#endif
	BatVolHigh_Hysteresis_H_val = atoi(argv[2]);
#else // EEP_CONTENT_BatVolHigh_Hysteresis_H_ITEM_LEN > 1
	#if EEP_CONTENT_BatVolHigh_Hysteresis_H_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t BatVolHigh_Hysteresis_H_val[EEP_CONTENT_BatVolHigh_Hysteresis_H_ITEM_LEN];
	#elif EEP_CONTENT_BatVolHigh_Hysteresis_H_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t BatVolHigh_Hysteresis_H_val[EEP_CONTENT_BatVolHigh_Hysteresis_H_ITEM_LEN];
	#else
	uint32_t BatVolHigh_Hysteresis_H_val[EEP_CONTENT_BatVolHigh_Hysteresis_H_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_BatVolHigh_Hysteresis_H_ITEM_LEN; i ++ )
	{
		BatVolHigh_Hysteresis_H_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_BatVolHigh_Hysteresis_H_ITEM_LEN}	
	
	DevEep_Set_BatVolHigh_Hysteresis_H(eep_handle,BatVolHigh_Hysteresis_H_val);
}


#if EEP_CONTENT_BatVolHigh_Hysteresis_H_ITEM_LEN == 1
#if EEP_CONTENT_BatVolHigh_Hysteresis_H_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_BatVolHigh_Hysteresis_H_Callback(EEP_HANDLE eep_handle,uint8_t const BatVolHigh_Hysteresis_H_val)
#elif EEP_CONTENT_BatVolHigh_Hysteresis_H_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_BatVolHigh_Hysteresis_H_Callback(EEP_HANDLE eep_handle,uint16_t const BatVolHigh_Hysteresis_H_val)
#else
void DevEepTest_BatVolHigh_Hysteresis_H_Callback(EEP_HANDLE eep_handle,uint32_t const BatVolHigh_Hysteresis_H_val)
#endif	
{
	
	printf("dev eep test: BatVolHigh_Hysteresis_H, change to dec=%d,hex=0x%08x\r\n",BatVolHigh_Hysteresis_H_val,BatVolHigh_Hysteresis_H_val);
}
#else // EEP_CONTENT_BatVolHigh_Hysteresis_H_ITEM_LEN > 1
#if EEP_CONTENT_BatVolHigh_Hysteresis_H_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_BatVolHigh_Hysteresis_H_Callback(EEP_HANDLE eep_handle,uint8_t const * BatVolHigh_Hysteresis_H_val)
#elif EEP_CONTENT_BatVolHigh_Hysteresis_H_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_BatVolHigh_Hysteresis_H_Callback(EEP_HANDLE eep_handle,uint16_t const * BatVolHigh_Hysteresis_H_val)
#else
void DevEepTest_BatVolHigh_Hysteresis_H_Callback(EEP_HANDLE eep_handle,uint32_t const * BatVolHigh_Hysteresis_H_val)
#endif
{
	printf("dev eep test: BatVolHigh_Hysteresis_H, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_BatVolHigh_Hysteresis_H_ITEM_LEN; i ++)
	{
		printf("%d ", BatVolHigh_Hysteresis_H_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_BatVolHigh_Hysteresis_H_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_BatVolHigh_Hysteresis_H_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", BatVolHigh_Hysteresis_H_val[i]);
		#elif EEP_CONTENT_BatVolHigh_Hysteresis_H_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", BatVolHigh_Hysteresis_H_val[i]);
		#else
		printf("%08x ", BatVolHigh_Hysteresis_H_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_BatVolHigh_Hysteresis_L(int argc, char **argv)
{
#if EEP_CONTENT_BatVolHigh_Hysteresis_L_ITEM_LEN == 1
	#if EEP_CONTENT_BatVolHigh_Hysteresis_L_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t BatVolHigh_Hysteresis_L_val;
	#elif EEP_CONTENT_BatVolHigh_Hysteresis_L_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t BatVolHigh_Hysteresis_L_val;
	#else
	uint32_t BatVolHigh_Hysteresis_L_val;
	#endif
	BatVolHigh_Hysteresis_L_val = atoi(argv[2]);
#else // EEP_CONTENT_BatVolHigh_Hysteresis_L_ITEM_LEN > 1
	#if EEP_CONTENT_BatVolHigh_Hysteresis_L_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t BatVolHigh_Hysteresis_L_val[EEP_CONTENT_BatVolHigh_Hysteresis_L_ITEM_LEN];
	#elif EEP_CONTENT_BatVolHigh_Hysteresis_L_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t BatVolHigh_Hysteresis_L_val[EEP_CONTENT_BatVolHigh_Hysteresis_L_ITEM_LEN];
	#else
	uint32_t BatVolHigh_Hysteresis_L_val[EEP_CONTENT_BatVolHigh_Hysteresis_L_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_BatVolHigh_Hysteresis_L_ITEM_LEN; i ++ )
	{
		BatVolHigh_Hysteresis_L_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_BatVolHigh_Hysteresis_L_ITEM_LEN}	
	
	DevEep_Set_BatVolHigh_Hysteresis_L(eep_handle,BatVolHigh_Hysteresis_L_val);
}


#if EEP_CONTENT_BatVolHigh_Hysteresis_L_ITEM_LEN == 1
#if EEP_CONTENT_BatVolHigh_Hysteresis_L_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_BatVolHigh_Hysteresis_L_Callback(EEP_HANDLE eep_handle,uint8_t const BatVolHigh_Hysteresis_L_val)
#elif EEP_CONTENT_BatVolHigh_Hysteresis_L_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_BatVolHigh_Hysteresis_L_Callback(EEP_HANDLE eep_handle,uint16_t const BatVolHigh_Hysteresis_L_val)
#else
void DevEepTest_BatVolHigh_Hysteresis_L_Callback(EEP_HANDLE eep_handle,uint32_t const BatVolHigh_Hysteresis_L_val)
#endif	
{
	
	printf("dev eep test: BatVolHigh_Hysteresis_L, change to dec=%d,hex=0x%08x\r\n",BatVolHigh_Hysteresis_L_val,BatVolHigh_Hysteresis_L_val);
}
#else // EEP_CONTENT_BatVolHigh_Hysteresis_L_ITEM_LEN > 1
#if EEP_CONTENT_BatVolHigh_Hysteresis_L_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_BatVolHigh_Hysteresis_L_Callback(EEP_HANDLE eep_handle,uint8_t const * BatVolHigh_Hysteresis_L_val)
#elif EEP_CONTENT_BatVolHigh_Hysteresis_L_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_BatVolHigh_Hysteresis_L_Callback(EEP_HANDLE eep_handle,uint16_t const * BatVolHigh_Hysteresis_L_val)
#else
void DevEepTest_BatVolHigh_Hysteresis_L_Callback(EEP_HANDLE eep_handle,uint32_t const * BatVolHigh_Hysteresis_L_val)
#endif
{
	printf("dev eep test: BatVolHigh_Hysteresis_L, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_BatVolHigh_Hysteresis_L_ITEM_LEN; i ++)
	{
		printf("%d ", BatVolHigh_Hysteresis_L_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_BatVolHigh_Hysteresis_L_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_BatVolHigh_Hysteresis_L_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", BatVolHigh_Hysteresis_L_val[i]);
		#elif EEP_CONTENT_BatVolHigh_Hysteresis_L_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", BatVolHigh_Hysteresis_L_val[i]);
		#else
		printf("%08x ", BatVolHigh_Hysteresis_L_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_BatVolLow_Hysteresis_H(int argc, char **argv)
{
#if EEP_CONTENT_BatVolLow_Hysteresis_H_ITEM_LEN == 1
	#if EEP_CONTENT_BatVolLow_Hysteresis_H_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t BatVolLow_Hysteresis_H_val;
	#elif EEP_CONTENT_BatVolLow_Hysteresis_H_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t BatVolLow_Hysteresis_H_val;
	#else
	uint32_t BatVolLow_Hysteresis_H_val;
	#endif
	BatVolLow_Hysteresis_H_val = atoi(argv[2]);
#else // EEP_CONTENT_BatVolLow_Hysteresis_H_ITEM_LEN > 1
	#if EEP_CONTENT_BatVolLow_Hysteresis_H_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t BatVolLow_Hysteresis_H_val[EEP_CONTENT_BatVolLow_Hysteresis_H_ITEM_LEN];
	#elif EEP_CONTENT_BatVolLow_Hysteresis_H_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t BatVolLow_Hysteresis_H_val[EEP_CONTENT_BatVolLow_Hysteresis_H_ITEM_LEN];
	#else
	uint32_t BatVolLow_Hysteresis_H_val[EEP_CONTENT_BatVolLow_Hysteresis_H_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_BatVolLow_Hysteresis_H_ITEM_LEN; i ++ )
	{
		BatVolLow_Hysteresis_H_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_BatVolLow_Hysteresis_H_ITEM_LEN}	
	
	DevEep_Set_BatVolLow_Hysteresis_H(eep_handle,BatVolLow_Hysteresis_H_val);
}


#if EEP_CONTENT_BatVolLow_Hysteresis_H_ITEM_LEN == 1
#if EEP_CONTENT_BatVolLow_Hysteresis_H_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_BatVolLow_Hysteresis_H_Callback(EEP_HANDLE eep_handle,uint8_t const BatVolLow_Hysteresis_H_val)
#elif EEP_CONTENT_BatVolLow_Hysteresis_H_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_BatVolLow_Hysteresis_H_Callback(EEP_HANDLE eep_handle,uint16_t const BatVolLow_Hysteresis_H_val)
#else
void DevEepTest_BatVolLow_Hysteresis_H_Callback(EEP_HANDLE eep_handle,uint32_t const BatVolLow_Hysteresis_H_val)
#endif	
{
	
	printf("dev eep test: BatVolLow_Hysteresis_H, change to dec=%d,hex=0x%08x\r\n",BatVolLow_Hysteresis_H_val,BatVolLow_Hysteresis_H_val);
}
#else // EEP_CONTENT_BatVolLow_Hysteresis_H_ITEM_LEN > 1
#if EEP_CONTENT_BatVolLow_Hysteresis_H_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_BatVolLow_Hysteresis_H_Callback(EEP_HANDLE eep_handle,uint8_t const * BatVolLow_Hysteresis_H_val)
#elif EEP_CONTENT_BatVolLow_Hysteresis_H_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_BatVolLow_Hysteresis_H_Callback(EEP_HANDLE eep_handle,uint16_t const * BatVolLow_Hysteresis_H_val)
#else
void DevEepTest_BatVolLow_Hysteresis_H_Callback(EEP_HANDLE eep_handle,uint32_t const * BatVolLow_Hysteresis_H_val)
#endif
{
	printf("dev eep test: BatVolLow_Hysteresis_H, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_BatVolLow_Hysteresis_H_ITEM_LEN; i ++)
	{
		printf("%d ", BatVolLow_Hysteresis_H_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_BatVolLow_Hysteresis_H_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_BatVolLow_Hysteresis_H_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", BatVolLow_Hysteresis_H_val[i]);
		#elif EEP_CONTENT_BatVolLow_Hysteresis_H_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", BatVolLow_Hysteresis_H_val[i]);
		#else
		printf("%08x ", BatVolLow_Hysteresis_H_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_BatVolLow_Hysteresis_L(int argc, char **argv)
{
#if EEP_CONTENT_BatVolLow_Hysteresis_L_ITEM_LEN == 1
	#if EEP_CONTENT_BatVolLow_Hysteresis_L_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t BatVolLow_Hysteresis_L_val;
	#elif EEP_CONTENT_BatVolLow_Hysteresis_L_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t BatVolLow_Hysteresis_L_val;
	#else
	uint32_t BatVolLow_Hysteresis_L_val;
	#endif
	BatVolLow_Hysteresis_L_val = atoi(argv[2]);
#else // EEP_CONTENT_BatVolLow_Hysteresis_L_ITEM_LEN > 1
	#if EEP_CONTENT_BatVolLow_Hysteresis_L_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t BatVolLow_Hysteresis_L_val[EEP_CONTENT_BatVolLow_Hysteresis_L_ITEM_LEN];
	#elif EEP_CONTENT_BatVolLow_Hysteresis_L_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t BatVolLow_Hysteresis_L_val[EEP_CONTENT_BatVolLow_Hysteresis_L_ITEM_LEN];
	#else
	uint32_t BatVolLow_Hysteresis_L_val[EEP_CONTENT_BatVolLow_Hysteresis_L_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_BatVolLow_Hysteresis_L_ITEM_LEN; i ++ )
	{
		BatVolLow_Hysteresis_L_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_BatVolLow_Hysteresis_L_ITEM_LEN}	
	
	DevEep_Set_BatVolLow_Hysteresis_L(eep_handle,BatVolLow_Hysteresis_L_val);
}


#if EEP_CONTENT_BatVolLow_Hysteresis_L_ITEM_LEN == 1
#if EEP_CONTENT_BatVolLow_Hysteresis_L_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_BatVolLow_Hysteresis_L_Callback(EEP_HANDLE eep_handle,uint8_t const BatVolLow_Hysteresis_L_val)
#elif EEP_CONTENT_BatVolLow_Hysteresis_L_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_BatVolLow_Hysteresis_L_Callback(EEP_HANDLE eep_handle,uint16_t const BatVolLow_Hysteresis_L_val)
#else
void DevEepTest_BatVolLow_Hysteresis_L_Callback(EEP_HANDLE eep_handle,uint32_t const BatVolLow_Hysteresis_L_val)
#endif	
{
	
	printf("dev eep test: BatVolLow_Hysteresis_L, change to dec=%d,hex=0x%08x\r\n",BatVolLow_Hysteresis_L_val,BatVolLow_Hysteresis_L_val);
}
#else // EEP_CONTENT_BatVolLow_Hysteresis_L_ITEM_LEN > 1
#if EEP_CONTENT_BatVolLow_Hysteresis_L_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_BatVolLow_Hysteresis_L_Callback(EEP_HANDLE eep_handle,uint8_t const * BatVolLow_Hysteresis_L_val)
#elif EEP_CONTENT_BatVolLow_Hysteresis_L_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_BatVolLow_Hysteresis_L_Callback(EEP_HANDLE eep_handle,uint16_t const * BatVolLow_Hysteresis_L_val)
#else
void DevEepTest_BatVolLow_Hysteresis_L_Callback(EEP_HANDLE eep_handle,uint32_t const * BatVolLow_Hysteresis_L_val)
#endif
{
	printf("dev eep test: BatVolLow_Hysteresis_L, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_BatVolLow_Hysteresis_L_ITEM_LEN; i ++)
	{
		printf("%d ", BatVolLow_Hysteresis_L_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_BatVolLow_Hysteresis_L_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_BatVolLow_Hysteresis_L_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", BatVolLow_Hysteresis_L_val[i]);
		#elif EEP_CONTENT_BatVolLow_Hysteresis_L_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", BatVolLow_Hysteresis_L_val[i]);
		#else
		printf("%08x ", BatVolLow_Hysteresis_L_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_BatVolVeryLow_Hysteresis_H(int argc, char **argv)
{
#if EEP_CONTENT_BatVolVeryLow_Hysteresis_H_ITEM_LEN == 1
	#if EEP_CONTENT_BatVolVeryLow_Hysteresis_H_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t BatVolVeryLow_Hysteresis_H_val;
	#elif EEP_CONTENT_BatVolVeryLow_Hysteresis_H_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t BatVolVeryLow_Hysteresis_H_val;
	#else
	uint32_t BatVolVeryLow_Hysteresis_H_val;
	#endif
	BatVolVeryLow_Hysteresis_H_val = atoi(argv[2]);
#else // EEP_CONTENT_BatVolVeryLow_Hysteresis_H_ITEM_LEN > 1
	#if EEP_CONTENT_BatVolVeryLow_Hysteresis_H_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t BatVolVeryLow_Hysteresis_H_val[EEP_CONTENT_BatVolVeryLow_Hysteresis_H_ITEM_LEN];
	#elif EEP_CONTENT_BatVolVeryLow_Hysteresis_H_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t BatVolVeryLow_Hysteresis_H_val[EEP_CONTENT_BatVolVeryLow_Hysteresis_H_ITEM_LEN];
	#else
	uint32_t BatVolVeryLow_Hysteresis_H_val[EEP_CONTENT_BatVolVeryLow_Hysteresis_H_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_BatVolVeryLow_Hysteresis_H_ITEM_LEN; i ++ )
	{
		BatVolVeryLow_Hysteresis_H_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_BatVolVeryLow_Hysteresis_H_ITEM_LEN}	
	
	DevEep_Set_BatVolVeryLow_Hysteresis_H(eep_handle,BatVolVeryLow_Hysteresis_H_val);
}


#if EEP_CONTENT_BatVolVeryLow_Hysteresis_H_ITEM_LEN == 1
#if EEP_CONTENT_BatVolVeryLow_Hysteresis_H_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_BatVolVeryLow_Hysteresis_H_Callback(EEP_HANDLE eep_handle,uint8_t const BatVolVeryLow_Hysteresis_H_val)
#elif EEP_CONTENT_BatVolVeryLow_Hysteresis_H_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_BatVolVeryLow_Hysteresis_H_Callback(EEP_HANDLE eep_handle,uint16_t const BatVolVeryLow_Hysteresis_H_val)
#else
void DevEepTest_BatVolVeryLow_Hysteresis_H_Callback(EEP_HANDLE eep_handle,uint32_t const BatVolVeryLow_Hysteresis_H_val)
#endif	
{
	
	printf("dev eep test: BatVolVeryLow_Hysteresis_H, change to dec=%d,hex=0x%08x\r\n",BatVolVeryLow_Hysteresis_H_val,BatVolVeryLow_Hysteresis_H_val);
}
#else // EEP_CONTENT_BatVolVeryLow_Hysteresis_H_ITEM_LEN > 1
#if EEP_CONTENT_BatVolVeryLow_Hysteresis_H_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_BatVolVeryLow_Hysteresis_H_Callback(EEP_HANDLE eep_handle,uint8_t const * BatVolVeryLow_Hysteresis_H_val)
#elif EEP_CONTENT_BatVolVeryLow_Hysteresis_H_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_BatVolVeryLow_Hysteresis_H_Callback(EEP_HANDLE eep_handle,uint16_t const * BatVolVeryLow_Hysteresis_H_val)
#else
void DevEepTest_BatVolVeryLow_Hysteresis_H_Callback(EEP_HANDLE eep_handle,uint32_t const * BatVolVeryLow_Hysteresis_H_val)
#endif
{
	printf("dev eep test: BatVolVeryLow_Hysteresis_H, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_BatVolVeryLow_Hysteresis_H_ITEM_LEN; i ++)
	{
		printf("%d ", BatVolVeryLow_Hysteresis_H_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_BatVolVeryLow_Hysteresis_H_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_BatVolVeryLow_Hysteresis_H_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", BatVolVeryLow_Hysteresis_H_val[i]);
		#elif EEP_CONTENT_BatVolVeryLow_Hysteresis_H_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", BatVolVeryLow_Hysteresis_H_val[i]);
		#else
		printf("%08x ", BatVolVeryLow_Hysteresis_H_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_BatVolVeryLow_Hysteresis_L(int argc, char **argv)
{
#if EEP_CONTENT_BatVolVeryLow_Hysteresis_L_ITEM_LEN == 1
	#if EEP_CONTENT_BatVolVeryLow_Hysteresis_L_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t BatVolVeryLow_Hysteresis_L_val;
	#elif EEP_CONTENT_BatVolVeryLow_Hysteresis_L_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t BatVolVeryLow_Hysteresis_L_val;
	#else
	uint32_t BatVolVeryLow_Hysteresis_L_val;
	#endif
	BatVolVeryLow_Hysteresis_L_val = atoi(argv[2]);
#else // EEP_CONTENT_BatVolVeryLow_Hysteresis_L_ITEM_LEN > 1
	#if EEP_CONTENT_BatVolVeryLow_Hysteresis_L_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t BatVolVeryLow_Hysteresis_L_val[EEP_CONTENT_BatVolVeryLow_Hysteresis_L_ITEM_LEN];
	#elif EEP_CONTENT_BatVolVeryLow_Hysteresis_L_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t BatVolVeryLow_Hysteresis_L_val[EEP_CONTENT_BatVolVeryLow_Hysteresis_L_ITEM_LEN];
	#else
	uint32_t BatVolVeryLow_Hysteresis_L_val[EEP_CONTENT_BatVolVeryLow_Hysteresis_L_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_BatVolVeryLow_Hysteresis_L_ITEM_LEN; i ++ )
	{
		BatVolVeryLow_Hysteresis_L_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_BatVolVeryLow_Hysteresis_L_ITEM_LEN}	
	
	DevEep_Set_BatVolVeryLow_Hysteresis_L(eep_handle,BatVolVeryLow_Hysteresis_L_val);
}


#if EEP_CONTENT_BatVolVeryLow_Hysteresis_L_ITEM_LEN == 1
#if EEP_CONTENT_BatVolVeryLow_Hysteresis_L_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_BatVolVeryLow_Hysteresis_L_Callback(EEP_HANDLE eep_handle,uint8_t const BatVolVeryLow_Hysteresis_L_val)
#elif EEP_CONTENT_BatVolVeryLow_Hysteresis_L_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_BatVolVeryLow_Hysteresis_L_Callback(EEP_HANDLE eep_handle,uint16_t const BatVolVeryLow_Hysteresis_L_val)
#else
void DevEepTest_BatVolVeryLow_Hysteresis_L_Callback(EEP_HANDLE eep_handle,uint32_t const BatVolVeryLow_Hysteresis_L_val)
#endif	
{
	
	printf("dev eep test: BatVolVeryLow_Hysteresis_L, change to dec=%d,hex=0x%08x\r\n",BatVolVeryLow_Hysteresis_L_val,BatVolVeryLow_Hysteresis_L_val);
}
#else // EEP_CONTENT_BatVolVeryLow_Hysteresis_L_ITEM_LEN > 1
#if EEP_CONTENT_BatVolVeryLow_Hysteresis_L_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_BatVolVeryLow_Hysteresis_L_Callback(EEP_HANDLE eep_handle,uint8_t const * BatVolVeryLow_Hysteresis_L_val)
#elif EEP_CONTENT_BatVolVeryLow_Hysteresis_L_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_BatVolVeryLow_Hysteresis_L_Callback(EEP_HANDLE eep_handle,uint16_t const * BatVolVeryLow_Hysteresis_L_val)
#else
void DevEepTest_BatVolVeryLow_Hysteresis_L_Callback(EEP_HANDLE eep_handle,uint32_t const * BatVolVeryLow_Hysteresis_L_val)
#endif
{
	printf("dev eep test: BatVolVeryLow_Hysteresis_L, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_BatVolVeryLow_Hysteresis_L_ITEM_LEN; i ++)
	{
		printf("%d ", BatVolVeryLow_Hysteresis_L_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_BatVolVeryLow_Hysteresis_L_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_BatVolVeryLow_Hysteresis_L_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", BatVolVeryLow_Hysteresis_L_val[i]);
		#elif EEP_CONTENT_BatVolVeryLow_Hysteresis_L_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", BatVolVeryLow_Hysteresis_L_val[i]);
		#else
		printf("%08x ", BatVolVeryLow_Hysteresis_L_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_TempState_ChgDly(int argc, char **argv)
{
#if EEP_CONTENT_TempState_ChgDly_ITEM_LEN == 1
	#if EEP_CONTENT_TempState_ChgDly_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t TempState_ChgDly_val;
	#elif EEP_CONTENT_TempState_ChgDly_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t TempState_ChgDly_val;
	#else
	uint32_t TempState_ChgDly_val;
	#endif
	TempState_ChgDly_val = atoi(argv[2]);
#else // EEP_CONTENT_TempState_ChgDly_ITEM_LEN > 1
	#if EEP_CONTENT_TempState_ChgDly_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t TempState_ChgDly_val[EEP_CONTENT_TempState_ChgDly_ITEM_LEN];
	#elif EEP_CONTENT_TempState_ChgDly_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t TempState_ChgDly_val[EEP_CONTENT_TempState_ChgDly_ITEM_LEN];
	#else
	uint32_t TempState_ChgDly_val[EEP_CONTENT_TempState_ChgDly_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_TempState_ChgDly_ITEM_LEN; i ++ )
	{
		TempState_ChgDly_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_TempState_ChgDly_ITEM_LEN}	
	
	DevEep_Set_TempState_ChgDly(eep_handle,TempState_ChgDly_val);
}


#if EEP_CONTENT_TempState_ChgDly_ITEM_LEN == 1
#if EEP_CONTENT_TempState_ChgDly_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_TempState_ChgDly_Callback(EEP_HANDLE eep_handle,uint8_t const TempState_ChgDly_val)
#elif EEP_CONTENT_TempState_ChgDly_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_TempState_ChgDly_Callback(EEP_HANDLE eep_handle,uint16_t const TempState_ChgDly_val)
#else
void DevEepTest_TempState_ChgDly_Callback(EEP_HANDLE eep_handle,uint32_t const TempState_ChgDly_val)
#endif	
{
	
	printf("dev eep test: TempState_ChgDly, change to dec=%d,hex=0x%08x\r\n",TempState_ChgDly_val,TempState_ChgDly_val);
}
#else // EEP_CONTENT_TempState_ChgDly_ITEM_LEN > 1
#if EEP_CONTENT_TempState_ChgDly_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_TempState_ChgDly_Callback(EEP_HANDLE eep_handle,uint8_t const * TempState_ChgDly_val)
#elif EEP_CONTENT_TempState_ChgDly_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_TempState_ChgDly_Callback(EEP_HANDLE eep_handle,uint16_t const * TempState_ChgDly_val)
#else
void DevEepTest_TempState_ChgDly_Callback(EEP_HANDLE eep_handle,uint32_t const * TempState_ChgDly_val)
#endif
{
	printf("dev eep test: TempState_ChgDly, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_TempState_ChgDly_ITEM_LEN; i ++)
	{
		printf("%d ", TempState_ChgDly_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_TempState_ChgDly_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_TempState_ChgDly_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", TempState_ChgDly_val[i]);
		#elif EEP_CONTENT_TempState_ChgDly_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", TempState_ChgDly_val[i]);
		#else
		printf("%08x ", TempState_ChgDly_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_TempDegC_Low_Hysteresis_L(int argc, char **argv)
{
#if EEP_CONTENT_TempDegC_Low_Hysteresis_L_ITEM_LEN == 1
	#if EEP_CONTENT_TempDegC_Low_Hysteresis_L_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t TempDegC_Low_Hysteresis_L_val;
	#elif EEP_CONTENT_TempDegC_Low_Hysteresis_L_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t TempDegC_Low_Hysteresis_L_val;
	#else
	uint32_t TempDegC_Low_Hysteresis_L_val;
	#endif
	TempDegC_Low_Hysteresis_L_val = atoi(argv[2]);
#else // EEP_CONTENT_TempDegC_Low_Hysteresis_L_ITEM_LEN > 1
	#if EEP_CONTENT_TempDegC_Low_Hysteresis_L_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t TempDegC_Low_Hysteresis_L_val[EEP_CONTENT_TempDegC_Low_Hysteresis_L_ITEM_LEN];
	#elif EEP_CONTENT_TempDegC_Low_Hysteresis_L_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t TempDegC_Low_Hysteresis_L_val[EEP_CONTENT_TempDegC_Low_Hysteresis_L_ITEM_LEN];
	#else
	uint32_t TempDegC_Low_Hysteresis_L_val[EEP_CONTENT_TempDegC_Low_Hysteresis_L_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_TempDegC_Low_Hysteresis_L_ITEM_LEN; i ++ )
	{
		TempDegC_Low_Hysteresis_L_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_TempDegC_Low_Hysteresis_L_ITEM_LEN}	
	
	DevEep_Set_TempDegC_Low_Hysteresis_L(eep_handle,TempDegC_Low_Hysteresis_L_val);
}


#if EEP_CONTENT_TempDegC_Low_Hysteresis_L_ITEM_LEN == 1
#if EEP_CONTENT_TempDegC_Low_Hysteresis_L_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_TempDegC_Low_Hysteresis_L_Callback(EEP_HANDLE eep_handle,uint8_t const TempDegC_Low_Hysteresis_L_val)
#elif EEP_CONTENT_TempDegC_Low_Hysteresis_L_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_TempDegC_Low_Hysteresis_L_Callback(EEP_HANDLE eep_handle,uint16_t const TempDegC_Low_Hysteresis_L_val)
#else
void DevEepTest_TempDegC_Low_Hysteresis_L_Callback(EEP_HANDLE eep_handle,uint32_t const TempDegC_Low_Hysteresis_L_val)
#endif	
{
	
	printf("dev eep test: TempDegC_Low_Hysteresis_L, change to dec=%d,hex=0x%08x\r\n",TempDegC_Low_Hysteresis_L_val,TempDegC_Low_Hysteresis_L_val);
}
#else // EEP_CONTENT_TempDegC_Low_Hysteresis_L_ITEM_LEN > 1
#if EEP_CONTENT_TempDegC_Low_Hysteresis_L_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_TempDegC_Low_Hysteresis_L_Callback(EEP_HANDLE eep_handle,uint8_t const * TempDegC_Low_Hysteresis_L_val)
#elif EEP_CONTENT_TempDegC_Low_Hysteresis_L_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_TempDegC_Low_Hysteresis_L_Callback(EEP_HANDLE eep_handle,uint16_t const * TempDegC_Low_Hysteresis_L_val)
#else
void DevEepTest_TempDegC_Low_Hysteresis_L_Callback(EEP_HANDLE eep_handle,uint32_t const * TempDegC_Low_Hysteresis_L_val)
#endif
{
	printf("dev eep test: TempDegC_Low_Hysteresis_L, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_TempDegC_Low_Hysteresis_L_ITEM_LEN; i ++)
	{
		printf("%d ", TempDegC_Low_Hysteresis_L_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_TempDegC_Low_Hysteresis_L_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_TempDegC_Low_Hysteresis_L_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", TempDegC_Low_Hysteresis_L_val[i]);
		#elif EEP_CONTENT_TempDegC_Low_Hysteresis_L_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", TempDegC_Low_Hysteresis_L_val[i]);
		#else
		printf("%08x ", TempDegC_Low_Hysteresis_L_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_TempDegC_Low_Hysteresis_H(int argc, char **argv)
{
#if EEP_CONTENT_TempDegC_Low_Hysteresis_H_ITEM_LEN == 1
	#if EEP_CONTENT_TempDegC_Low_Hysteresis_H_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t TempDegC_Low_Hysteresis_H_val;
	#elif EEP_CONTENT_TempDegC_Low_Hysteresis_H_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t TempDegC_Low_Hysteresis_H_val;
	#else
	uint32_t TempDegC_Low_Hysteresis_H_val;
	#endif
	TempDegC_Low_Hysteresis_H_val = atoi(argv[2]);
#else // EEP_CONTENT_TempDegC_Low_Hysteresis_H_ITEM_LEN > 1
	#if EEP_CONTENT_TempDegC_Low_Hysteresis_H_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t TempDegC_Low_Hysteresis_H_val[EEP_CONTENT_TempDegC_Low_Hysteresis_H_ITEM_LEN];
	#elif EEP_CONTENT_TempDegC_Low_Hysteresis_H_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t TempDegC_Low_Hysteresis_H_val[EEP_CONTENT_TempDegC_Low_Hysteresis_H_ITEM_LEN];
	#else
	uint32_t TempDegC_Low_Hysteresis_H_val[EEP_CONTENT_TempDegC_Low_Hysteresis_H_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_TempDegC_Low_Hysteresis_H_ITEM_LEN; i ++ )
	{
		TempDegC_Low_Hysteresis_H_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_TempDegC_Low_Hysteresis_H_ITEM_LEN}	
	
	DevEep_Set_TempDegC_Low_Hysteresis_H(eep_handle,TempDegC_Low_Hysteresis_H_val);
}


#if EEP_CONTENT_TempDegC_Low_Hysteresis_H_ITEM_LEN == 1
#if EEP_CONTENT_TempDegC_Low_Hysteresis_H_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_TempDegC_Low_Hysteresis_H_Callback(EEP_HANDLE eep_handle,uint8_t const TempDegC_Low_Hysteresis_H_val)
#elif EEP_CONTENT_TempDegC_Low_Hysteresis_H_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_TempDegC_Low_Hysteresis_H_Callback(EEP_HANDLE eep_handle,uint16_t const TempDegC_Low_Hysteresis_H_val)
#else
void DevEepTest_TempDegC_Low_Hysteresis_H_Callback(EEP_HANDLE eep_handle,uint32_t const TempDegC_Low_Hysteresis_H_val)
#endif	
{
	
	printf("dev eep test: TempDegC_Low_Hysteresis_H, change to dec=%d,hex=0x%08x\r\n",TempDegC_Low_Hysteresis_H_val,TempDegC_Low_Hysteresis_H_val);
}
#else // EEP_CONTENT_TempDegC_Low_Hysteresis_H_ITEM_LEN > 1
#if EEP_CONTENT_TempDegC_Low_Hysteresis_H_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_TempDegC_Low_Hysteresis_H_Callback(EEP_HANDLE eep_handle,uint8_t const * TempDegC_Low_Hysteresis_H_val)
#elif EEP_CONTENT_TempDegC_Low_Hysteresis_H_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_TempDegC_Low_Hysteresis_H_Callback(EEP_HANDLE eep_handle,uint16_t const * TempDegC_Low_Hysteresis_H_val)
#else
void DevEepTest_TempDegC_Low_Hysteresis_H_Callback(EEP_HANDLE eep_handle,uint32_t const * TempDegC_Low_Hysteresis_H_val)
#endif
{
	printf("dev eep test: TempDegC_Low_Hysteresis_H, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_TempDegC_Low_Hysteresis_H_ITEM_LEN; i ++)
	{
		printf("%d ", TempDegC_Low_Hysteresis_H_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_TempDegC_Low_Hysteresis_H_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_TempDegC_Low_Hysteresis_H_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", TempDegC_Low_Hysteresis_H_val[i]);
		#elif EEP_CONTENT_TempDegC_Low_Hysteresis_H_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", TempDegC_Low_Hysteresis_H_val[i]);
		#else
		printf("%08x ", TempDegC_Low_Hysteresis_H_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_TempDegC_High_Hysteresis_L(int argc, char **argv)
{
#if EEP_CONTENT_TempDegC_High_Hysteresis_L_ITEM_LEN == 1
	#if EEP_CONTENT_TempDegC_High_Hysteresis_L_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t TempDegC_High_Hysteresis_L_val;
	#elif EEP_CONTENT_TempDegC_High_Hysteresis_L_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t TempDegC_High_Hysteresis_L_val;
	#else
	uint32_t TempDegC_High_Hysteresis_L_val;
	#endif
	TempDegC_High_Hysteresis_L_val = atoi(argv[2]);
#else // EEP_CONTENT_TempDegC_High_Hysteresis_L_ITEM_LEN > 1
	#if EEP_CONTENT_TempDegC_High_Hysteresis_L_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t TempDegC_High_Hysteresis_L_val[EEP_CONTENT_TempDegC_High_Hysteresis_L_ITEM_LEN];
	#elif EEP_CONTENT_TempDegC_High_Hysteresis_L_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t TempDegC_High_Hysteresis_L_val[EEP_CONTENT_TempDegC_High_Hysteresis_L_ITEM_LEN];
	#else
	uint32_t TempDegC_High_Hysteresis_L_val[EEP_CONTENT_TempDegC_High_Hysteresis_L_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_TempDegC_High_Hysteresis_L_ITEM_LEN; i ++ )
	{
		TempDegC_High_Hysteresis_L_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_TempDegC_High_Hysteresis_L_ITEM_LEN}	
	
	DevEep_Set_TempDegC_High_Hysteresis_L(eep_handle,TempDegC_High_Hysteresis_L_val);
}


#if EEP_CONTENT_TempDegC_High_Hysteresis_L_ITEM_LEN == 1
#if EEP_CONTENT_TempDegC_High_Hysteresis_L_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_TempDegC_High_Hysteresis_L_Callback(EEP_HANDLE eep_handle,uint8_t const TempDegC_High_Hysteresis_L_val)
#elif EEP_CONTENT_TempDegC_High_Hysteresis_L_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_TempDegC_High_Hysteresis_L_Callback(EEP_HANDLE eep_handle,uint16_t const TempDegC_High_Hysteresis_L_val)
#else
void DevEepTest_TempDegC_High_Hysteresis_L_Callback(EEP_HANDLE eep_handle,uint32_t const TempDegC_High_Hysteresis_L_val)
#endif	
{
	
	printf("dev eep test: TempDegC_High_Hysteresis_L, change to dec=%d,hex=0x%08x\r\n",TempDegC_High_Hysteresis_L_val,TempDegC_High_Hysteresis_L_val);
}
#else // EEP_CONTENT_TempDegC_High_Hysteresis_L_ITEM_LEN > 1
#if EEP_CONTENT_TempDegC_High_Hysteresis_L_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_TempDegC_High_Hysteresis_L_Callback(EEP_HANDLE eep_handle,uint8_t const * TempDegC_High_Hysteresis_L_val)
#elif EEP_CONTENT_TempDegC_High_Hysteresis_L_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_TempDegC_High_Hysteresis_L_Callback(EEP_HANDLE eep_handle,uint16_t const * TempDegC_High_Hysteresis_L_val)
#else
void DevEepTest_TempDegC_High_Hysteresis_L_Callback(EEP_HANDLE eep_handle,uint32_t const * TempDegC_High_Hysteresis_L_val)
#endif
{
	printf("dev eep test: TempDegC_High_Hysteresis_L, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_TempDegC_High_Hysteresis_L_ITEM_LEN; i ++)
	{
		printf("%d ", TempDegC_High_Hysteresis_L_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_TempDegC_High_Hysteresis_L_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_TempDegC_High_Hysteresis_L_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", TempDegC_High_Hysteresis_L_val[i]);
		#elif EEP_CONTENT_TempDegC_High_Hysteresis_L_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", TempDegC_High_Hysteresis_L_val[i]);
		#else
		printf("%08x ", TempDegC_High_Hysteresis_L_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_TempDegC_High_Hysteresis_H(int argc, char **argv)
{
#if EEP_CONTENT_TempDegC_High_Hysteresis_H_ITEM_LEN == 1
	#if EEP_CONTENT_TempDegC_High_Hysteresis_H_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t TempDegC_High_Hysteresis_H_val;
	#elif EEP_CONTENT_TempDegC_High_Hysteresis_H_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t TempDegC_High_Hysteresis_H_val;
	#else
	uint32_t TempDegC_High_Hysteresis_H_val;
	#endif
	TempDegC_High_Hysteresis_H_val = atoi(argv[2]);
#else // EEP_CONTENT_TempDegC_High_Hysteresis_H_ITEM_LEN > 1
	#if EEP_CONTENT_TempDegC_High_Hysteresis_H_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t TempDegC_High_Hysteresis_H_val[EEP_CONTENT_TempDegC_High_Hysteresis_H_ITEM_LEN];
	#elif EEP_CONTENT_TempDegC_High_Hysteresis_H_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t TempDegC_High_Hysteresis_H_val[EEP_CONTENT_TempDegC_High_Hysteresis_H_ITEM_LEN];
	#else
	uint32_t TempDegC_High_Hysteresis_H_val[EEP_CONTENT_TempDegC_High_Hysteresis_H_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_TempDegC_High_Hysteresis_H_ITEM_LEN; i ++ )
	{
		TempDegC_High_Hysteresis_H_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_TempDegC_High_Hysteresis_H_ITEM_LEN}	
	
	DevEep_Set_TempDegC_High_Hysteresis_H(eep_handle,TempDegC_High_Hysteresis_H_val);
}


#if EEP_CONTENT_TempDegC_High_Hysteresis_H_ITEM_LEN == 1
#if EEP_CONTENT_TempDegC_High_Hysteresis_H_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_TempDegC_High_Hysteresis_H_Callback(EEP_HANDLE eep_handle,uint8_t const TempDegC_High_Hysteresis_H_val)
#elif EEP_CONTENT_TempDegC_High_Hysteresis_H_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_TempDegC_High_Hysteresis_H_Callback(EEP_HANDLE eep_handle,uint16_t const TempDegC_High_Hysteresis_H_val)
#else
void DevEepTest_TempDegC_High_Hysteresis_H_Callback(EEP_HANDLE eep_handle,uint32_t const TempDegC_High_Hysteresis_H_val)
#endif	
{
	
	printf("dev eep test: TempDegC_High_Hysteresis_H, change to dec=%d,hex=0x%08x\r\n",TempDegC_High_Hysteresis_H_val,TempDegC_High_Hysteresis_H_val);
}
#else // EEP_CONTENT_TempDegC_High_Hysteresis_H_ITEM_LEN > 1
#if EEP_CONTENT_TempDegC_High_Hysteresis_H_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_TempDegC_High_Hysteresis_H_Callback(EEP_HANDLE eep_handle,uint8_t const * TempDegC_High_Hysteresis_H_val)
#elif EEP_CONTENT_TempDegC_High_Hysteresis_H_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_TempDegC_High_Hysteresis_H_Callback(EEP_HANDLE eep_handle,uint16_t const * TempDegC_High_Hysteresis_H_val)
#else
void DevEepTest_TempDegC_High_Hysteresis_H_Callback(EEP_HANDLE eep_handle,uint32_t const * TempDegC_High_Hysteresis_H_val)
#endif
{
	printf("dev eep test: TempDegC_High_Hysteresis_H, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_TempDegC_High_Hysteresis_H_ITEM_LEN; i ++)
	{
		printf("%d ", TempDegC_High_Hysteresis_H_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_TempDegC_High_Hysteresis_H_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_TempDegC_High_Hysteresis_H_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", TempDegC_High_Hysteresis_H_val[i]);
		#elif EEP_CONTENT_TempDegC_High_Hysteresis_H_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", TempDegC_High_Hysteresis_H_val[i]);
		#else
		printf("%08x ", TempDegC_High_Hysteresis_H_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_AmpHighTempProction(int argc, char **argv)
{
#if EEP_CONTENT_AmpHighTempProction_ITEM_LEN == 1
	#if EEP_CONTENT_AmpHighTempProction_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t AmpHighTempProction_val;
	#elif EEP_CONTENT_AmpHighTempProction_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t AmpHighTempProction_val;
	#else
	uint32_t AmpHighTempProction_val;
	#endif
	AmpHighTempProction_val = atoi(argv[2]);
#else // EEP_CONTENT_AmpHighTempProction_ITEM_LEN > 1
	#if EEP_CONTENT_AmpHighTempProction_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t AmpHighTempProction_val[EEP_CONTENT_AmpHighTempProction_ITEM_LEN];
	#elif EEP_CONTENT_AmpHighTempProction_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t AmpHighTempProction_val[EEP_CONTENT_AmpHighTempProction_ITEM_LEN];
	#else
	uint32_t AmpHighTempProction_val[EEP_CONTENT_AmpHighTempProction_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_AmpHighTempProction_ITEM_LEN; i ++ )
	{
		AmpHighTempProction_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_AmpHighTempProction_ITEM_LEN}	
	
	DevEep_Set_AmpHighTempProction(eep_handle,AmpHighTempProction_val);
}


#if EEP_CONTENT_AmpHighTempProction_ITEM_LEN == 1
#if EEP_CONTENT_AmpHighTempProction_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_AmpHighTempProction_Callback(EEP_HANDLE eep_handle,uint8_t const AmpHighTempProction_val)
#elif EEP_CONTENT_AmpHighTempProction_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_AmpHighTempProction_Callback(EEP_HANDLE eep_handle,uint16_t const AmpHighTempProction_val)
#else
void DevEepTest_AmpHighTempProction_Callback(EEP_HANDLE eep_handle,uint32_t const AmpHighTempProction_val)
#endif	
{
	
	printf("dev eep test: AmpHighTempProction, change to dec=%d,hex=0x%08x\r\n",AmpHighTempProction_val,AmpHighTempProction_val);
}
#else // EEP_CONTENT_AmpHighTempProction_ITEM_LEN > 1
#if EEP_CONTENT_AmpHighTempProction_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_AmpHighTempProction_Callback(EEP_HANDLE eep_handle,uint8_t const * AmpHighTempProction_val)
#elif EEP_CONTENT_AmpHighTempProction_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_AmpHighTempProction_Callback(EEP_HANDLE eep_handle,uint16_t const * AmpHighTempProction_val)
#else
void DevEepTest_AmpHighTempProction_Callback(EEP_HANDLE eep_handle,uint32_t const * AmpHighTempProction_val)
#endif
{
	printf("dev eep test: AmpHighTempProction, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_AmpHighTempProction_ITEM_LEN; i ++)
	{
		printf("%d ", AmpHighTempProction_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_AmpHighTempProction_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_AmpHighTempProction_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", AmpHighTempProction_val[i]);
		#elif EEP_CONTENT_AmpHighTempProction_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", AmpHighTempProction_val[i]);
		#else
		printf("%08x ", AmpHighTempProction_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_CanNm_DA_S1_Delay_ms(int argc, char **argv)
{
#if EEP_CONTENT_CanNm_DA_S1_Delay_ms_ITEM_LEN == 1
	#if EEP_CONTENT_CanNm_DA_S1_Delay_ms_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t CanNm_DA_S1_Delay_ms_val;
	#elif EEP_CONTENT_CanNm_DA_S1_Delay_ms_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t CanNm_DA_S1_Delay_ms_val;
	#else
	uint32_t CanNm_DA_S1_Delay_ms_val;
	#endif
	CanNm_DA_S1_Delay_ms_val = atoi(argv[2]);
#else // EEP_CONTENT_CanNm_DA_S1_Delay_ms_ITEM_LEN > 1
	#if EEP_CONTENT_CanNm_DA_S1_Delay_ms_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t CanNm_DA_S1_Delay_ms_val[EEP_CONTENT_CanNm_DA_S1_Delay_ms_ITEM_LEN];
	#elif EEP_CONTENT_CanNm_DA_S1_Delay_ms_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t CanNm_DA_S1_Delay_ms_val[EEP_CONTENT_CanNm_DA_S1_Delay_ms_ITEM_LEN];
	#else
	uint32_t CanNm_DA_S1_Delay_ms_val[EEP_CONTENT_CanNm_DA_S1_Delay_ms_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_CanNm_DA_S1_Delay_ms_ITEM_LEN; i ++ )
	{
		CanNm_DA_S1_Delay_ms_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_CanNm_DA_S1_Delay_ms_ITEM_LEN}	
	
	DevEep_Set_CanNm_DA_S1_Delay_ms(eep_handle,CanNm_DA_S1_Delay_ms_val);
}


#if EEP_CONTENT_CanNm_DA_S1_Delay_ms_ITEM_LEN == 1
#if EEP_CONTENT_CanNm_DA_S1_Delay_ms_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_CanNm_DA_S1_Delay_ms_Callback(EEP_HANDLE eep_handle,uint8_t const CanNm_DA_S1_Delay_ms_val)
#elif EEP_CONTENT_CanNm_DA_S1_Delay_ms_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_CanNm_DA_S1_Delay_ms_Callback(EEP_HANDLE eep_handle,uint16_t const CanNm_DA_S1_Delay_ms_val)
#else
void DevEepTest_CanNm_DA_S1_Delay_ms_Callback(EEP_HANDLE eep_handle,uint32_t const CanNm_DA_S1_Delay_ms_val)
#endif	
{
	
	printf("dev eep test: CanNm_DA_S1_Delay_ms, change to dec=%d,hex=0x%08x\r\n",CanNm_DA_S1_Delay_ms_val,CanNm_DA_S1_Delay_ms_val);
}
#else // EEP_CONTENT_CanNm_DA_S1_Delay_ms_ITEM_LEN > 1
#if EEP_CONTENT_CanNm_DA_S1_Delay_ms_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_CanNm_DA_S1_Delay_ms_Callback(EEP_HANDLE eep_handle,uint8_t const * CanNm_DA_S1_Delay_ms_val)
#elif EEP_CONTENT_CanNm_DA_S1_Delay_ms_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_CanNm_DA_S1_Delay_ms_Callback(EEP_HANDLE eep_handle,uint16_t const * CanNm_DA_S1_Delay_ms_val)
#else
void DevEepTest_CanNm_DA_S1_Delay_ms_Callback(EEP_HANDLE eep_handle,uint32_t const * CanNm_DA_S1_Delay_ms_val)
#endif
{
	printf("dev eep test: CanNm_DA_S1_Delay_ms, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_CanNm_DA_S1_Delay_ms_ITEM_LEN; i ++)
	{
		printf("%d ", CanNm_DA_S1_Delay_ms_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_CanNm_DA_S1_Delay_ms_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_CanNm_DA_S1_Delay_ms_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", CanNm_DA_S1_Delay_ms_val[i]);
		#elif EEP_CONTENT_CanNm_DA_S1_Delay_ms_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", CanNm_DA_S1_Delay_ms_val[i]);
		#else
		printf("%08x ", CanNm_DA_S1_Delay_ms_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_Vin(int argc, char **argv)
{
#if EEP_CONTENT_Vin_ITEM_LEN == 1
	#if EEP_CONTENT_Vin_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t Vin_val;
	#elif EEP_CONTENT_Vin_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t Vin_val;
	#else
	uint32_t Vin_val;
	#endif
	Vin_val = atoi(argv[2]);
#else // EEP_CONTENT_Vin_ITEM_LEN > 1
	#if EEP_CONTENT_Vin_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t Vin_val[EEP_CONTENT_Vin_ITEM_LEN];
	#elif EEP_CONTENT_Vin_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t Vin_val[EEP_CONTENT_Vin_ITEM_LEN];
	#else
	uint32_t Vin_val[EEP_CONTENT_Vin_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_Vin_ITEM_LEN; i ++ )
	{
		Vin_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_Vin_ITEM_LEN}	
	
	DevEep_Set_Vin(eep_handle,Vin_val);
}


#if EEP_CONTENT_Vin_ITEM_LEN == 1
#if EEP_CONTENT_Vin_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_Vin_Callback(EEP_HANDLE eep_handle,uint8_t const Vin_val)
#elif EEP_CONTENT_Vin_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_Vin_Callback(EEP_HANDLE eep_handle,uint16_t const Vin_val)
#else
void DevEepTest_Vin_Callback(EEP_HANDLE eep_handle,uint32_t const Vin_val)
#endif	
{
	
	printf("dev eep test: Vin, change to dec=%d,hex=0x%08x\r\n",Vin_val,Vin_val);
}
#else // EEP_CONTENT_Vin_ITEM_LEN > 1
#if EEP_CONTENT_Vin_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_Vin_Callback(EEP_HANDLE eep_handle,uint8_t const * Vin_val)
#elif EEP_CONTENT_Vin_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_Vin_Callback(EEP_HANDLE eep_handle,uint16_t const * Vin_val)
#else
void DevEepTest_Vin_Callback(EEP_HANDLE eep_handle,uint32_t const * Vin_val)
#endif
{
	printf("dev eep test: Vin, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_Vin_ITEM_LEN; i ++)
	{
		printf("%d ", Vin_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_Vin_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_Vin_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", Vin_val[i]);
		#elif EEP_CONTENT_Vin_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", Vin_val[i]);
		#else
		printf("%08x ", Vin_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_AutoSyncTimeWithGps(int argc, char **argv)
{
#if EEP_CONTENT_AutoSyncTimeWithGps_ITEM_LEN == 1
	#if EEP_CONTENT_AutoSyncTimeWithGps_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t AutoSyncTimeWithGps_val;
	#elif EEP_CONTENT_AutoSyncTimeWithGps_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t AutoSyncTimeWithGps_val;
	#else
	uint32_t AutoSyncTimeWithGps_val;
	#endif
	AutoSyncTimeWithGps_val = atoi(argv[2]);
#else // EEP_CONTENT_AutoSyncTimeWithGps_ITEM_LEN > 1
	#if EEP_CONTENT_AutoSyncTimeWithGps_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t AutoSyncTimeWithGps_val[EEP_CONTENT_AutoSyncTimeWithGps_ITEM_LEN];
	#elif EEP_CONTENT_AutoSyncTimeWithGps_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t AutoSyncTimeWithGps_val[EEP_CONTENT_AutoSyncTimeWithGps_ITEM_LEN];
	#else
	uint32_t AutoSyncTimeWithGps_val[EEP_CONTENT_AutoSyncTimeWithGps_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_AutoSyncTimeWithGps_ITEM_LEN; i ++ )
	{
		AutoSyncTimeWithGps_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_AutoSyncTimeWithGps_ITEM_LEN}	
	
	DevEep_Set_AutoSyncTimeWithGps(eep_handle,AutoSyncTimeWithGps_val);
}


#if EEP_CONTENT_AutoSyncTimeWithGps_ITEM_LEN == 1
#if EEP_CONTENT_AutoSyncTimeWithGps_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_AutoSyncTimeWithGps_Callback(EEP_HANDLE eep_handle,uint8_t const AutoSyncTimeWithGps_val)
#elif EEP_CONTENT_AutoSyncTimeWithGps_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_AutoSyncTimeWithGps_Callback(EEP_HANDLE eep_handle,uint16_t const AutoSyncTimeWithGps_val)
#else
void DevEepTest_AutoSyncTimeWithGps_Callback(EEP_HANDLE eep_handle,uint32_t const AutoSyncTimeWithGps_val)
#endif	
{
	
	printf("dev eep test: AutoSyncTimeWithGps, change to dec=%d,hex=0x%08x\r\n",AutoSyncTimeWithGps_val,AutoSyncTimeWithGps_val);
}
#else // EEP_CONTENT_AutoSyncTimeWithGps_ITEM_LEN > 1
#if EEP_CONTENT_AutoSyncTimeWithGps_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_AutoSyncTimeWithGps_Callback(EEP_HANDLE eep_handle,uint8_t const * AutoSyncTimeWithGps_val)
#elif EEP_CONTENT_AutoSyncTimeWithGps_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_AutoSyncTimeWithGps_Callback(EEP_HANDLE eep_handle,uint16_t const * AutoSyncTimeWithGps_val)
#else
void DevEepTest_AutoSyncTimeWithGps_Callback(EEP_HANDLE eep_handle,uint32_t const * AutoSyncTimeWithGps_val)
#endif
{
	printf("dev eep test: AutoSyncTimeWithGps, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_AutoSyncTimeWithGps_ITEM_LEN; i ++)
	{
		printf("%d ", AutoSyncTimeWithGps_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_AutoSyncTimeWithGps_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_AutoSyncTimeWithGps_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", AutoSyncTimeWithGps_val[i]);
		#elif EEP_CONTENT_AutoSyncTimeWithGps_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", AutoSyncTimeWithGps_val[i]);
		#else
		printf("%08x ", AutoSyncTimeWithGps_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_ScreenBackLightValOnDay(int argc, char **argv)
{
#if EEP_CONTENT_ScreenBackLightValOnDay_ITEM_LEN == 1
	#if EEP_CONTENT_ScreenBackLightValOnDay_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t ScreenBackLightValOnDay_val;
	#elif EEP_CONTENT_ScreenBackLightValOnDay_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t ScreenBackLightValOnDay_val;
	#else
	uint32_t ScreenBackLightValOnDay_val;
	#endif
	ScreenBackLightValOnDay_val = atoi(argv[2]);
#else // EEP_CONTENT_ScreenBackLightValOnDay_ITEM_LEN > 1
	#if EEP_CONTENT_ScreenBackLightValOnDay_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t ScreenBackLightValOnDay_val[EEP_CONTENT_ScreenBackLightValOnDay_ITEM_LEN];
	#elif EEP_CONTENT_ScreenBackLightValOnDay_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t ScreenBackLightValOnDay_val[EEP_CONTENT_ScreenBackLightValOnDay_ITEM_LEN];
	#else
	uint32_t ScreenBackLightValOnDay_val[EEP_CONTENT_ScreenBackLightValOnDay_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_ScreenBackLightValOnDay_ITEM_LEN; i ++ )
	{
		ScreenBackLightValOnDay_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_ScreenBackLightValOnDay_ITEM_LEN}	
	
	DevEep_Set_ScreenBackLightValOnDay(eep_handle,ScreenBackLightValOnDay_val);
}


#if EEP_CONTENT_ScreenBackLightValOnDay_ITEM_LEN == 1
#if EEP_CONTENT_ScreenBackLightValOnDay_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_ScreenBackLightValOnDay_Callback(EEP_HANDLE eep_handle,uint8_t const ScreenBackLightValOnDay_val)
#elif EEP_CONTENT_ScreenBackLightValOnDay_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_ScreenBackLightValOnDay_Callback(EEP_HANDLE eep_handle,uint16_t const ScreenBackLightValOnDay_val)
#else
void DevEepTest_ScreenBackLightValOnDay_Callback(EEP_HANDLE eep_handle,uint32_t const ScreenBackLightValOnDay_val)
#endif	
{
	
	printf("dev eep test: ScreenBackLightValOnDay, change to dec=%d,hex=0x%08x\r\n",ScreenBackLightValOnDay_val,ScreenBackLightValOnDay_val);
}
#else // EEP_CONTENT_ScreenBackLightValOnDay_ITEM_LEN > 1
#if EEP_CONTENT_ScreenBackLightValOnDay_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_ScreenBackLightValOnDay_Callback(EEP_HANDLE eep_handle,uint8_t const * ScreenBackLightValOnDay_val)
#elif EEP_CONTENT_ScreenBackLightValOnDay_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_ScreenBackLightValOnDay_Callback(EEP_HANDLE eep_handle,uint16_t const * ScreenBackLightValOnDay_val)
#else
void DevEepTest_ScreenBackLightValOnDay_Callback(EEP_HANDLE eep_handle,uint32_t const * ScreenBackLightValOnDay_val)
#endif
{
	printf("dev eep test: ScreenBackLightValOnDay, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_ScreenBackLightValOnDay_ITEM_LEN; i ++)
	{
		printf("%d ", ScreenBackLightValOnDay_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_ScreenBackLightValOnDay_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_ScreenBackLightValOnDay_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", ScreenBackLightValOnDay_val[i]);
		#elif EEP_CONTENT_ScreenBackLightValOnDay_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", ScreenBackLightValOnDay_val[i]);
		#else
		printf("%08x ", ScreenBackLightValOnDay_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_ScreenBackLightValOnNight(int argc, char **argv)
{
#if EEP_CONTENT_ScreenBackLightValOnNight_ITEM_LEN == 1
	#if EEP_CONTENT_ScreenBackLightValOnNight_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t ScreenBackLightValOnNight_val;
	#elif EEP_CONTENT_ScreenBackLightValOnNight_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t ScreenBackLightValOnNight_val;
	#else
	uint32_t ScreenBackLightValOnNight_val;
	#endif
	ScreenBackLightValOnNight_val = atoi(argv[2]);
#else // EEP_CONTENT_ScreenBackLightValOnNight_ITEM_LEN > 1
	#if EEP_CONTENT_ScreenBackLightValOnNight_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t ScreenBackLightValOnNight_val[EEP_CONTENT_ScreenBackLightValOnNight_ITEM_LEN];
	#elif EEP_CONTENT_ScreenBackLightValOnNight_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t ScreenBackLightValOnNight_val[EEP_CONTENT_ScreenBackLightValOnNight_ITEM_LEN];
	#else
	uint32_t ScreenBackLightValOnNight_val[EEP_CONTENT_ScreenBackLightValOnNight_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_ScreenBackLightValOnNight_ITEM_LEN; i ++ )
	{
		ScreenBackLightValOnNight_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_ScreenBackLightValOnNight_ITEM_LEN}	
	
	DevEep_Set_ScreenBackLightValOnNight(eep_handle,ScreenBackLightValOnNight_val);
}


#if EEP_CONTENT_ScreenBackLightValOnNight_ITEM_LEN == 1
#if EEP_CONTENT_ScreenBackLightValOnNight_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_ScreenBackLightValOnNight_Callback(EEP_HANDLE eep_handle,uint8_t const ScreenBackLightValOnNight_val)
#elif EEP_CONTENT_ScreenBackLightValOnNight_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_ScreenBackLightValOnNight_Callback(EEP_HANDLE eep_handle,uint16_t const ScreenBackLightValOnNight_val)
#else
void DevEepTest_ScreenBackLightValOnNight_Callback(EEP_HANDLE eep_handle,uint32_t const ScreenBackLightValOnNight_val)
#endif	
{
	
	printf("dev eep test: ScreenBackLightValOnNight, change to dec=%d,hex=0x%08x\r\n",ScreenBackLightValOnNight_val,ScreenBackLightValOnNight_val);
}
#else // EEP_CONTENT_ScreenBackLightValOnNight_ITEM_LEN > 1
#if EEP_CONTENT_ScreenBackLightValOnNight_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_ScreenBackLightValOnNight_Callback(EEP_HANDLE eep_handle,uint8_t const * ScreenBackLightValOnNight_val)
#elif EEP_CONTENT_ScreenBackLightValOnNight_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_ScreenBackLightValOnNight_Callback(EEP_HANDLE eep_handle,uint16_t const * ScreenBackLightValOnNight_val)
#else
void DevEepTest_ScreenBackLightValOnNight_Callback(EEP_HANDLE eep_handle,uint32_t const * ScreenBackLightValOnNight_val)
#endif
{
	printf("dev eep test: ScreenBackLightValOnNight, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_ScreenBackLightValOnNight_ITEM_LEN; i ++)
	{
		printf("%d ", ScreenBackLightValOnNight_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_ScreenBackLightValOnNight_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_ScreenBackLightValOnNight_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", ScreenBackLightValOnNight_val[i]);
		#elif EEP_CONTENT_ScreenBackLightValOnNight_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", ScreenBackLightValOnNight_val[i]);
		#else
		printf("%08x ", ScreenBackLightValOnNight_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_HistoryAverFuelCons(int argc, char **argv)
{
#if EEP_CONTENT_HistoryAverFuelCons_ITEM_LEN == 1
	#if EEP_CONTENT_HistoryAverFuelCons_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t HistoryAverFuelCons_val;
	#elif EEP_CONTENT_HistoryAverFuelCons_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t HistoryAverFuelCons_val;
	#else
	uint32_t HistoryAverFuelCons_val;
	#endif
	HistoryAverFuelCons_val = atoi(argv[2]);
#else // EEP_CONTENT_HistoryAverFuelCons_ITEM_LEN > 1
	#if EEP_CONTENT_HistoryAverFuelCons_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t HistoryAverFuelCons_val[EEP_CONTENT_HistoryAverFuelCons_ITEM_LEN];
	#elif EEP_CONTENT_HistoryAverFuelCons_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t HistoryAverFuelCons_val[EEP_CONTENT_HistoryAverFuelCons_ITEM_LEN];
	#else
	uint32_t HistoryAverFuelCons_val[EEP_CONTENT_HistoryAverFuelCons_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_HistoryAverFuelCons_ITEM_LEN; i ++ )
	{
		HistoryAverFuelCons_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_HistoryAverFuelCons_ITEM_LEN}	
	
	DevEep_Set_HistoryAverFuelCons(eep_handle,HistoryAverFuelCons_val);
}


#if EEP_CONTENT_HistoryAverFuelCons_ITEM_LEN == 1
#if EEP_CONTENT_HistoryAverFuelCons_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_HistoryAverFuelCons_Callback(EEP_HANDLE eep_handle,uint8_t const HistoryAverFuelCons_val)
#elif EEP_CONTENT_HistoryAverFuelCons_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_HistoryAverFuelCons_Callback(EEP_HANDLE eep_handle,uint16_t const HistoryAverFuelCons_val)
#else
void DevEepTest_HistoryAverFuelCons_Callback(EEP_HANDLE eep_handle,uint32_t const HistoryAverFuelCons_val)
#endif	
{
	
	printf("dev eep test: HistoryAverFuelCons, change to dec=%d,hex=0x%08x\r\n",HistoryAverFuelCons_val,HistoryAverFuelCons_val);
}
#else // EEP_CONTENT_HistoryAverFuelCons_ITEM_LEN > 1
#if EEP_CONTENT_HistoryAverFuelCons_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_HistoryAverFuelCons_Callback(EEP_HANDLE eep_handle,uint8_t const * HistoryAverFuelCons_val)
#elif EEP_CONTENT_HistoryAverFuelCons_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_HistoryAverFuelCons_Callback(EEP_HANDLE eep_handle,uint16_t const * HistoryAverFuelCons_val)
#else
void DevEepTest_HistoryAverFuelCons_Callback(EEP_HANDLE eep_handle,uint32_t const * HistoryAverFuelCons_val)
#endif
{
	printf("dev eep test: HistoryAverFuelCons, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_HistoryAverFuelCons_ITEM_LEN; i ++)
	{
		printf("%d ", HistoryAverFuelCons_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_HistoryAverFuelCons_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_HistoryAverFuelCons_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", HistoryAverFuelCons_val[i]);
		#elif EEP_CONTENT_HistoryAverFuelCons_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", HistoryAverFuelCons_val[i]);
		#else
		printf("%08x ", HistoryAverFuelCons_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_FuelResistance(int argc, char **argv)
{
#if EEP_CONTENT_FuelResistance_ITEM_LEN == 1
	#if EEP_CONTENT_FuelResistance_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t FuelResistance_val;
	#elif EEP_CONTENT_FuelResistance_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t FuelResistance_val;
	#else
	uint32_t FuelResistance_val;
	#endif
	FuelResistance_val = atoi(argv[2]);
#else // EEP_CONTENT_FuelResistance_ITEM_LEN > 1
	#if EEP_CONTENT_FuelResistance_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t FuelResistance_val[EEP_CONTENT_FuelResistance_ITEM_LEN];
	#elif EEP_CONTENT_FuelResistance_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t FuelResistance_val[EEP_CONTENT_FuelResistance_ITEM_LEN];
	#else
	uint32_t FuelResistance_val[EEP_CONTENT_FuelResistance_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_FuelResistance_ITEM_LEN; i ++ )
	{
		FuelResistance_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_FuelResistance_ITEM_LEN}	
	
	DevEep_Set_FuelResistance(eep_handle,FuelResistance_val);
}


#if EEP_CONTENT_FuelResistance_ITEM_LEN == 1
#if EEP_CONTENT_FuelResistance_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_FuelResistance_Callback(EEP_HANDLE eep_handle,uint8_t const FuelResistance_val)
#elif EEP_CONTENT_FuelResistance_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_FuelResistance_Callback(EEP_HANDLE eep_handle,uint16_t const FuelResistance_val)
#else
void DevEepTest_FuelResistance_Callback(EEP_HANDLE eep_handle,uint32_t const FuelResistance_val)
#endif	
{
	
	printf("dev eep test: FuelResistance, change to dec=%d,hex=0x%08x\r\n",FuelResistance_val,FuelResistance_val);
}
#else // EEP_CONTENT_FuelResistance_ITEM_LEN > 1
#if EEP_CONTENT_FuelResistance_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_FuelResistance_Callback(EEP_HANDLE eep_handle,uint8_t const * FuelResistance_val)
#elif EEP_CONTENT_FuelResistance_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_FuelResistance_Callback(EEP_HANDLE eep_handle,uint16_t const * FuelResistance_val)
#else
void DevEepTest_FuelResistance_Callback(EEP_HANDLE eep_handle,uint32_t const * FuelResistance_val)
#endif
{
	printf("dev eep test: FuelResistance, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_FuelResistance_ITEM_LEN; i ++)
	{
		printf("%d ", FuelResistance_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_FuelResistance_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_FuelResistance_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", FuelResistance_val[i]);
		#elif EEP_CONTENT_FuelResistance_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", FuelResistance_val[i]);
		#else
		printf("%08x ", FuelResistance_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_dtc_example(int argc, char **argv)
{
#if EEP_CONTENT_dtc_example_ITEM_LEN == 1
	#if EEP_CONTENT_dtc_example_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t dtc_example_val;
	#elif EEP_CONTENT_dtc_example_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t dtc_example_val;
	#else
	uint32_t dtc_example_val;
	#endif
	dtc_example_val = atoi(argv[2]);
#else // EEP_CONTENT_dtc_example_ITEM_LEN > 1
	#if EEP_CONTENT_dtc_example_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t dtc_example_val[EEP_CONTENT_dtc_example_ITEM_LEN];
	#elif EEP_CONTENT_dtc_example_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t dtc_example_val[EEP_CONTENT_dtc_example_ITEM_LEN];
	#else
	uint32_t dtc_example_val[EEP_CONTENT_dtc_example_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_dtc_example_ITEM_LEN; i ++ )
	{
		dtc_example_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_dtc_example_ITEM_LEN}	
	
	DevEep_Set_dtc_example(eep_handle,dtc_example_val);
}


#if EEP_CONTENT_dtc_example_ITEM_LEN == 1
#if EEP_CONTENT_dtc_example_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_dtc_example_Callback(EEP_HANDLE eep_handle,uint8_t const dtc_example_val)
#elif EEP_CONTENT_dtc_example_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_dtc_example_Callback(EEP_HANDLE eep_handle,uint16_t const dtc_example_val)
#else
void DevEepTest_dtc_example_Callback(EEP_HANDLE eep_handle,uint32_t const dtc_example_val)
#endif	
{
	
	printf("dev eep test: dtc_example, change to dec=%d,hex=0x%08x\r\n",dtc_example_val,dtc_example_val);
}
#else // EEP_CONTENT_dtc_example_ITEM_LEN > 1
#if EEP_CONTENT_dtc_example_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_dtc_example_Callback(EEP_HANDLE eep_handle,uint8_t const * dtc_example_val)
#elif EEP_CONTENT_dtc_example_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_dtc_example_Callback(EEP_HANDLE eep_handle,uint16_t const * dtc_example_val)
#else
void DevEepTest_dtc_example_Callback(EEP_HANDLE eep_handle,uint32_t const * dtc_example_val)
#endif
{
	printf("dev eep test: dtc_example, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_dtc_example_ITEM_LEN; i ++)
	{
		printf("%d ", dtc_example_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_dtc_example_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_dtc_example_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", dtc_example_val[i]);
		#elif EEP_CONTENT_dtc_example_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", dtc_example_val[i]);
		#else
		printf("%08x ", dtc_example_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_TotalOdo(int argc, char **argv)
{
#if EEP_CONTENT_TotalOdo_ITEM_LEN == 1
	#if EEP_CONTENT_TotalOdo_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t TotalOdo_val;
	#elif EEP_CONTENT_TotalOdo_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t TotalOdo_val;
	#else
	uint32_t TotalOdo_val;
	#endif
	TotalOdo_val = atoi(argv[2]);
#else // EEP_CONTENT_TotalOdo_ITEM_LEN > 1
	#if EEP_CONTENT_TotalOdo_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t TotalOdo_val[EEP_CONTENT_TotalOdo_ITEM_LEN];
	#elif EEP_CONTENT_TotalOdo_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t TotalOdo_val[EEP_CONTENT_TotalOdo_ITEM_LEN];
	#else
	uint32_t TotalOdo_val[EEP_CONTENT_TotalOdo_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_TotalOdo_ITEM_LEN; i ++ )
	{
		TotalOdo_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_TotalOdo_ITEM_LEN}	
	
	DevEep_Set_TotalOdo(eep_handle,TotalOdo_val);
}


#if EEP_CONTENT_TotalOdo_ITEM_LEN == 1
#if EEP_CONTENT_TotalOdo_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_TotalOdo_Callback(EEP_HANDLE eep_handle,uint8_t const TotalOdo_val)
#elif EEP_CONTENT_TotalOdo_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_TotalOdo_Callback(EEP_HANDLE eep_handle,uint16_t const TotalOdo_val)
#else
void DevEepTest_TotalOdo_Callback(EEP_HANDLE eep_handle,uint32_t const TotalOdo_val)
#endif	
{
	
	printf("dev eep test: TotalOdo, change to dec=%d,hex=0x%08x\r\n",TotalOdo_val,TotalOdo_val);
}
#else // EEP_CONTENT_TotalOdo_ITEM_LEN > 1
#if EEP_CONTENT_TotalOdo_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_TotalOdo_Callback(EEP_HANDLE eep_handle,uint8_t const * TotalOdo_val)
#elif EEP_CONTENT_TotalOdo_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_TotalOdo_Callback(EEP_HANDLE eep_handle,uint16_t const * TotalOdo_val)
#else
void DevEepTest_TotalOdo_Callback(EEP_HANDLE eep_handle,uint32_t const * TotalOdo_val)
#endif
{
	printf("dev eep test: TotalOdo, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_TotalOdo_ITEM_LEN; i ++)
	{
		printf("%d ", TotalOdo_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_TotalOdo_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_TotalOdo_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", TotalOdo_val[i]);
		#elif EEP_CONTENT_TotalOdo_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", TotalOdo_val[i]);
		#else
		printf("%08x ", TotalOdo_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_TotalTime(int argc, char **argv)
{
#if EEP_CONTENT_TotalTime_ITEM_LEN == 1
	#if EEP_CONTENT_TotalTime_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t TotalTime_val;
	#elif EEP_CONTENT_TotalTime_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t TotalTime_val;
	#else
	uint32_t TotalTime_val;
	#endif
	TotalTime_val = atoi(argv[2]);
#else // EEP_CONTENT_TotalTime_ITEM_LEN > 1
	#if EEP_CONTENT_TotalTime_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t TotalTime_val[EEP_CONTENT_TotalTime_ITEM_LEN];
	#elif EEP_CONTENT_TotalTime_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t TotalTime_val[EEP_CONTENT_TotalTime_ITEM_LEN];
	#else
	uint32_t TotalTime_val[EEP_CONTENT_TotalTime_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_TotalTime_ITEM_LEN; i ++ )
	{
		TotalTime_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_TotalTime_ITEM_LEN}	
	
	DevEep_Set_TotalTime(eep_handle,TotalTime_val);
}


#if EEP_CONTENT_TotalTime_ITEM_LEN == 1
#if EEP_CONTENT_TotalTime_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_TotalTime_Callback(EEP_HANDLE eep_handle,uint8_t const TotalTime_val)
#elif EEP_CONTENT_TotalTime_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_TotalTime_Callback(EEP_HANDLE eep_handle,uint16_t const TotalTime_val)
#else
void DevEepTest_TotalTime_Callback(EEP_HANDLE eep_handle,uint32_t const TotalTime_val)
#endif	
{
	
	printf("dev eep test: TotalTime, change to dec=%d,hex=0x%08x\r\n",TotalTime_val,TotalTime_val);
}
#else // EEP_CONTENT_TotalTime_ITEM_LEN > 1
#if EEP_CONTENT_TotalTime_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_TotalTime_Callback(EEP_HANDLE eep_handle,uint8_t const * TotalTime_val)
#elif EEP_CONTENT_TotalTime_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_TotalTime_Callback(EEP_HANDLE eep_handle,uint16_t const * TotalTime_val)
#else
void DevEepTest_TotalTime_Callback(EEP_HANDLE eep_handle,uint32_t const * TotalTime_val)
#endif
{
	printf("dev eep test: TotalTime, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_TotalTime_ITEM_LEN; i ++)
	{
		printf("%d ", TotalTime_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_TotalTime_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_TotalTime_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", TotalTime_val[i]);
		#elif EEP_CONTENT_TotalTime_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", TotalTime_val[i]);
		#else
		printf("%08x ", TotalTime_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_TravelTime(int argc, char **argv)
{
#if EEP_CONTENT_TravelTime_ITEM_LEN == 1
	#if EEP_CONTENT_TravelTime_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t TravelTime_val;
	#elif EEP_CONTENT_TravelTime_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t TravelTime_val;
	#else
	uint32_t TravelTime_val;
	#endif
	TravelTime_val = atoi(argv[2]);
#else // EEP_CONTENT_TravelTime_ITEM_LEN > 1
	#if EEP_CONTENT_TravelTime_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t TravelTime_val[EEP_CONTENT_TravelTime_ITEM_LEN];
	#elif EEP_CONTENT_TravelTime_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t TravelTime_val[EEP_CONTENT_TravelTime_ITEM_LEN];
	#else
	uint32_t TravelTime_val[EEP_CONTENT_TravelTime_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_TravelTime_ITEM_LEN; i ++ )
	{
		TravelTime_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_TravelTime_ITEM_LEN}	
	
	DevEep_Set_TravelTime(eep_handle,TravelTime_val);
}


#if EEP_CONTENT_TravelTime_ITEM_LEN == 1
#if EEP_CONTENT_TravelTime_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_TravelTime_Callback(EEP_HANDLE eep_handle,uint8_t const TravelTime_val)
#elif EEP_CONTENT_TravelTime_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_TravelTime_Callback(EEP_HANDLE eep_handle,uint16_t const TravelTime_val)
#else
void DevEepTest_TravelTime_Callback(EEP_HANDLE eep_handle,uint32_t const TravelTime_val)
#endif	
{
	
	printf("dev eep test: TravelTime, change to dec=%d,hex=0x%08x\r\n",TravelTime_val,TravelTime_val);
}
#else // EEP_CONTENT_TravelTime_ITEM_LEN > 1
#if EEP_CONTENT_TravelTime_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_TravelTime_Callback(EEP_HANDLE eep_handle,uint8_t const * TravelTime_val)
#elif EEP_CONTENT_TravelTime_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_TravelTime_Callback(EEP_HANDLE eep_handle,uint16_t const * TravelTime_val)
#else
void DevEepTest_TravelTime_Callback(EEP_HANDLE eep_handle,uint32_t const * TravelTime_val)
#endif
{
	printf("dev eep test: TravelTime, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_TravelTime_ITEM_LEN; i ++)
	{
		printf("%d ", TravelTime_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_TravelTime_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_TravelTime_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", TravelTime_val[i]);
		#elif EEP_CONTENT_TravelTime_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", TravelTime_val[i]);
		#else
		printf("%08x ", TravelTime_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_TravelOdo(int argc, char **argv)
{
#if EEP_CONTENT_TravelOdo_ITEM_LEN == 1
	#if EEP_CONTENT_TravelOdo_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t TravelOdo_val;
	#elif EEP_CONTENT_TravelOdo_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t TravelOdo_val;
	#else
	uint32_t TravelOdo_val;
	#endif
	TravelOdo_val = atoi(argv[2]);
#else // EEP_CONTENT_TravelOdo_ITEM_LEN > 1
	#if EEP_CONTENT_TravelOdo_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t TravelOdo_val[EEP_CONTENT_TravelOdo_ITEM_LEN];
	#elif EEP_CONTENT_TravelOdo_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t TravelOdo_val[EEP_CONTENT_TravelOdo_ITEM_LEN];
	#else
	uint32_t TravelOdo_val[EEP_CONTENT_TravelOdo_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_TravelOdo_ITEM_LEN; i ++ )
	{
		TravelOdo_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_TravelOdo_ITEM_LEN}	
	
	DevEep_Set_TravelOdo(eep_handle,TravelOdo_val);
}


#if EEP_CONTENT_TravelOdo_ITEM_LEN == 1
#if EEP_CONTENT_TravelOdo_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_TravelOdo_Callback(EEP_HANDLE eep_handle,uint8_t const TravelOdo_val)
#elif EEP_CONTENT_TravelOdo_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_TravelOdo_Callback(EEP_HANDLE eep_handle,uint16_t const TravelOdo_val)
#else
void DevEepTest_TravelOdo_Callback(EEP_HANDLE eep_handle,uint32_t const TravelOdo_val)
#endif	
{
	
	printf("dev eep test: TravelOdo, change to dec=%d,hex=0x%08x\r\n",TravelOdo_val,TravelOdo_val);
}
#else // EEP_CONTENT_TravelOdo_ITEM_LEN > 1
#if EEP_CONTENT_TravelOdo_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_TravelOdo_Callback(EEP_HANDLE eep_handle,uint8_t const * TravelOdo_val)
#elif EEP_CONTENT_TravelOdo_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_TravelOdo_Callback(EEP_HANDLE eep_handle,uint16_t const * TravelOdo_val)
#else
void DevEepTest_TravelOdo_Callback(EEP_HANDLE eep_handle,uint32_t const * TravelOdo_val)
#endif
{
	printf("dev eep test: TravelOdo, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_TravelOdo_ITEM_LEN; i ++)
	{
		printf("%d ", TravelOdo_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_TravelOdo_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_TravelOdo_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", TravelOdo_val[i]);
		#elif EEP_CONTENT_TravelOdo_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", TravelOdo_val[i]);
		#else
		printf("%08x ", TravelOdo_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_TripAMeter(int argc, char **argv)
{
#if EEP_CONTENT_TripAMeter_ITEM_LEN == 1
	#if EEP_CONTENT_TripAMeter_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t TripAMeter_val;
	#elif EEP_CONTENT_TripAMeter_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t TripAMeter_val;
	#else
	uint32_t TripAMeter_val;
	#endif
	TripAMeter_val = atoi(argv[2]);
#else // EEP_CONTENT_TripAMeter_ITEM_LEN > 1
	#if EEP_CONTENT_TripAMeter_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t TripAMeter_val[EEP_CONTENT_TripAMeter_ITEM_LEN];
	#elif EEP_CONTENT_TripAMeter_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t TripAMeter_val[EEP_CONTENT_TripAMeter_ITEM_LEN];
	#else
	uint32_t TripAMeter_val[EEP_CONTENT_TripAMeter_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_TripAMeter_ITEM_LEN; i ++ )
	{
		TripAMeter_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_TripAMeter_ITEM_LEN}	
	
	DevEep_Set_TripAMeter(eep_handle,TripAMeter_val);
}


#if EEP_CONTENT_TripAMeter_ITEM_LEN == 1
#if EEP_CONTENT_TripAMeter_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_TripAMeter_Callback(EEP_HANDLE eep_handle,uint8_t const TripAMeter_val)
#elif EEP_CONTENT_TripAMeter_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_TripAMeter_Callback(EEP_HANDLE eep_handle,uint16_t const TripAMeter_val)
#else
void DevEepTest_TripAMeter_Callback(EEP_HANDLE eep_handle,uint32_t const TripAMeter_val)
#endif	
{
	
	printf("dev eep test: TripAMeter, change to dec=%d,hex=0x%08x\r\n",TripAMeter_val,TripAMeter_val);
}
#else // EEP_CONTENT_TripAMeter_ITEM_LEN > 1
#if EEP_CONTENT_TripAMeter_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_TripAMeter_Callback(EEP_HANDLE eep_handle,uint8_t const * TripAMeter_val)
#elif EEP_CONTENT_TripAMeter_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_TripAMeter_Callback(EEP_HANDLE eep_handle,uint16_t const * TripAMeter_val)
#else
void DevEepTest_TripAMeter_Callback(EEP_HANDLE eep_handle,uint32_t const * TripAMeter_val)
#endif
{
	printf("dev eep test: TripAMeter, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_TripAMeter_ITEM_LEN; i ++)
	{
		printf("%d ", TripAMeter_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_TripAMeter_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_TripAMeter_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", TripAMeter_val[i]);
		#elif EEP_CONTENT_TripAMeter_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", TripAMeter_val[i]);
		#else
		printf("%08x ", TripAMeter_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_TripATime(int argc, char **argv)
{
#if EEP_CONTENT_TripATime_ITEM_LEN == 1
	#if EEP_CONTENT_TripATime_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t TripATime_val;
	#elif EEP_CONTENT_TripATime_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t TripATime_val;
	#else
	uint32_t TripATime_val;
	#endif
	TripATime_val = atoi(argv[2]);
#else // EEP_CONTENT_TripATime_ITEM_LEN > 1
	#if EEP_CONTENT_TripATime_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t TripATime_val[EEP_CONTENT_TripATime_ITEM_LEN];
	#elif EEP_CONTENT_TripATime_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t TripATime_val[EEP_CONTENT_TripATime_ITEM_LEN];
	#else
	uint32_t TripATime_val[EEP_CONTENT_TripATime_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_TripATime_ITEM_LEN; i ++ )
	{
		TripATime_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_TripATime_ITEM_LEN}	
	
	DevEep_Set_TripATime(eep_handle,TripATime_val);
}


#if EEP_CONTENT_TripATime_ITEM_LEN == 1
#if EEP_CONTENT_TripATime_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_TripATime_Callback(EEP_HANDLE eep_handle,uint8_t const TripATime_val)
#elif EEP_CONTENT_TripATime_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_TripATime_Callback(EEP_HANDLE eep_handle,uint16_t const TripATime_val)
#else
void DevEepTest_TripATime_Callback(EEP_HANDLE eep_handle,uint32_t const TripATime_val)
#endif	
{
	
	printf("dev eep test: TripATime, change to dec=%d,hex=0x%08x\r\n",TripATime_val,TripATime_val);
}
#else // EEP_CONTENT_TripATime_ITEM_LEN > 1
#if EEP_CONTENT_TripATime_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_TripATime_Callback(EEP_HANDLE eep_handle,uint8_t const * TripATime_val)
#elif EEP_CONTENT_TripATime_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_TripATime_Callback(EEP_HANDLE eep_handle,uint16_t const * TripATime_val)
#else
void DevEepTest_TripATime_Callback(EEP_HANDLE eep_handle,uint32_t const * TripATime_val)
#endif
{
	printf("dev eep test: TripATime, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_TripATime_ITEM_LEN; i ++)
	{
		printf("%d ", TripATime_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_TripATime_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_TripATime_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", TripATime_val[i]);
		#elif EEP_CONTENT_TripATime_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", TripATime_val[i]);
		#else
		printf("%08x ", TripATime_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_TripBMeter(int argc, char **argv)
{
#if EEP_CONTENT_TripBMeter_ITEM_LEN == 1
	#if EEP_CONTENT_TripBMeter_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t TripBMeter_val;
	#elif EEP_CONTENT_TripBMeter_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t TripBMeter_val;
	#else
	uint32_t TripBMeter_val;
	#endif
	TripBMeter_val = atoi(argv[2]);
#else // EEP_CONTENT_TripBMeter_ITEM_LEN > 1
	#if EEP_CONTENT_TripBMeter_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t TripBMeter_val[EEP_CONTENT_TripBMeter_ITEM_LEN];
	#elif EEP_CONTENT_TripBMeter_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t TripBMeter_val[EEP_CONTENT_TripBMeter_ITEM_LEN];
	#else
	uint32_t TripBMeter_val[EEP_CONTENT_TripBMeter_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_TripBMeter_ITEM_LEN; i ++ )
	{
		TripBMeter_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_TripBMeter_ITEM_LEN}	
	
	DevEep_Set_TripBMeter(eep_handle,TripBMeter_val);
}


#if EEP_CONTENT_TripBMeter_ITEM_LEN == 1
#if EEP_CONTENT_TripBMeter_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_TripBMeter_Callback(EEP_HANDLE eep_handle,uint8_t const TripBMeter_val)
#elif EEP_CONTENT_TripBMeter_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_TripBMeter_Callback(EEP_HANDLE eep_handle,uint16_t const TripBMeter_val)
#else
void DevEepTest_TripBMeter_Callback(EEP_HANDLE eep_handle,uint32_t const TripBMeter_val)
#endif	
{
	
	printf("dev eep test: TripBMeter, change to dec=%d,hex=0x%08x\r\n",TripBMeter_val,TripBMeter_val);
}
#else // EEP_CONTENT_TripBMeter_ITEM_LEN > 1
#if EEP_CONTENT_TripBMeter_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_TripBMeter_Callback(EEP_HANDLE eep_handle,uint8_t const * TripBMeter_val)
#elif EEP_CONTENT_TripBMeter_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_TripBMeter_Callback(EEP_HANDLE eep_handle,uint16_t const * TripBMeter_val)
#else
void DevEepTest_TripBMeter_Callback(EEP_HANDLE eep_handle,uint32_t const * TripBMeter_val)
#endif
{
	printf("dev eep test: TripBMeter, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_TripBMeter_ITEM_LEN; i ++)
	{
		printf("%d ", TripBMeter_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_TripBMeter_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_TripBMeter_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", TripBMeter_val[i]);
		#elif EEP_CONTENT_TripBMeter_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", TripBMeter_val[i]);
		#else
		printf("%08x ", TripBMeter_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_TripBTime(int argc, char **argv)
{
#if EEP_CONTENT_TripBTime_ITEM_LEN == 1
	#if EEP_CONTENT_TripBTime_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t TripBTime_val;
	#elif EEP_CONTENT_TripBTime_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t TripBTime_val;
	#else
	uint32_t TripBTime_val;
	#endif
	TripBTime_val = atoi(argv[2]);
#else // EEP_CONTENT_TripBTime_ITEM_LEN > 1
	#if EEP_CONTENT_TripBTime_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t TripBTime_val[EEP_CONTENT_TripBTime_ITEM_LEN];
	#elif EEP_CONTENT_TripBTime_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t TripBTime_val[EEP_CONTENT_TripBTime_ITEM_LEN];
	#else
	uint32_t TripBTime_val[EEP_CONTENT_TripBTime_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_TripBTime_ITEM_LEN; i ++ )
	{
		TripBTime_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_TripBTime_ITEM_LEN}	
	
	DevEep_Set_TripBTime(eep_handle,TripBTime_val);
}


#if EEP_CONTENT_TripBTime_ITEM_LEN == 1
#if EEP_CONTENT_TripBTime_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_TripBTime_Callback(EEP_HANDLE eep_handle,uint8_t const TripBTime_val)
#elif EEP_CONTENT_TripBTime_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_TripBTime_Callback(EEP_HANDLE eep_handle,uint16_t const TripBTime_val)
#else
void DevEepTest_TripBTime_Callback(EEP_HANDLE eep_handle,uint32_t const TripBTime_val)
#endif	
{
	
	printf("dev eep test: TripBTime, change to dec=%d,hex=0x%08x\r\n",TripBTime_val,TripBTime_val);
}
#else // EEP_CONTENT_TripBTime_ITEM_LEN > 1
#if EEP_CONTENT_TripBTime_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_TripBTime_Callback(EEP_HANDLE eep_handle,uint8_t const * TripBTime_val)
#elif EEP_CONTENT_TripBTime_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_TripBTime_Callback(EEP_HANDLE eep_handle,uint16_t const * TripBTime_val)
#else
void DevEepTest_TripBTime_Callback(EEP_HANDLE eep_handle,uint32_t const * TripBTime_val)
#endif
{
	printf("dev eep test: TripBTime, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_TripBTime_ITEM_LEN; i ++)
	{
		printf("%d ", TripBTime_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_TripBTime_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_TripBTime_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", TripBTime_val[i]);
		#elif EEP_CONTENT_TripBTime_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", TripBTime_val[i]);
		#else
		printf("%08x ", TripBTime_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_CruiseDistance(int argc, char **argv)
{
#if EEP_CONTENT_CruiseDistance_ITEM_LEN == 1
	#if EEP_CONTENT_CruiseDistance_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t CruiseDistance_val;
	#elif EEP_CONTENT_CruiseDistance_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t CruiseDistance_val;
	#else
	uint32_t CruiseDistance_val;
	#endif
	CruiseDistance_val = atoi(argv[2]);
#else // EEP_CONTENT_CruiseDistance_ITEM_LEN > 1
	#if EEP_CONTENT_CruiseDistance_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t CruiseDistance_val[EEP_CONTENT_CruiseDistance_ITEM_LEN];
	#elif EEP_CONTENT_CruiseDistance_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t CruiseDistance_val[EEP_CONTENT_CruiseDistance_ITEM_LEN];
	#else
	uint32_t CruiseDistance_val[EEP_CONTENT_CruiseDistance_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_CruiseDistance_ITEM_LEN; i ++ )
	{
		CruiseDistance_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_CruiseDistance_ITEM_LEN}	
	
	DevEep_Set_CruiseDistance(eep_handle,CruiseDistance_val);
}


#if EEP_CONTENT_CruiseDistance_ITEM_LEN == 1
#if EEP_CONTENT_CruiseDistance_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_CruiseDistance_Callback(EEP_HANDLE eep_handle,uint8_t const CruiseDistance_val)
#elif EEP_CONTENT_CruiseDistance_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_CruiseDistance_Callback(EEP_HANDLE eep_handle,uint16_t const CruiseDistance_val)
#else
void DevEepTest_CruiseDistance_Callback(EEP_HANDLE eep_handle,uint32_t const CruiseDistance_val)
#endif	
{
	
	printf("dev eep test: CruiseDistance, change to dec=%d,hex=0x%08x\r\n",CruiseDistance_val,CruiseDistance_val);
}
#else // EEP_CONTENT_CruiseDistance_ITEM_LEN > 1
#if EEP_CONTENT_CruiseDistance_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_CruiseDistance_Callback(EEP_HANDLE eep_handle,uint8_t const * CruiseDistance_val)
#elif EEP_CONTENT_CruiseDistance_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_CruiseDistance_Callback(EEP_HANDLE eep_handle,uint16_t const * CruiseDistance_val)
#else
void DevEepTest_CruiseDistance_Callback(EEP_HANDLE eep_handle,uint32_t const * CruiseDistance_val)
#endif
{
	printf("dev eep test: CruiseDistance, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_CruiseDistance_ITEM_LEN; i ++)
	{
		printf("%d ", CruiseDistance_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_CruiseDistance_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_CruiseDistance_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", CruiseDistance_val[i]);
		#elif EEP_CONTENT_CruiseDistance_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", CruiseDistance_val[i]);
		#else
		printf("%08x ", CruiseDistance_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_VipSwdlShareMem(int argc, char **argv)
{
#if EEP_CONTENT_VipSwdlShareMem_ITEM_LEN == 1
	#if EEP_CONTENT_VipSwdlShareMem_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t VipSwdlShareMem_val;
	#elif EEP_CONTENT_VipSwdlShareMem_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t VipSwdlShareMem_val;
	#else
	uint32_t VipSwdlShareMem_val;
	#endif
	VipSwdlShareMem_val = atoi(argv[2]);
#else // EEP_CONTENT_VipSwdlShareMem_ITEM_LEN > 1
	#if EEP_CONTENT_VipSwdlShareMem_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t VipSwdlShareMem_val[EEP_CONTENT_VipSwdlShareMem_ITEM_LEN];
	#elif EEP_CONTENT_VipSwdlShareMem_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t VipSwdlShareMem_val[EEP_CONTENT_VipSwdlShareMem_ITEM_LEN];
	#else
	uint32_t VipSwdlShareMem_val[EEP_CONTENT_VipSwdlShareMem_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_VipSwdlShareMem_ITEM_LEN; i ++ )
	{
		VipSwdlShareMem_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_VipSwdlShareMem_ITEM_LEN}	
	
	DevEep_Set_VipSwdlShareMem(eep_handle,VipSwdlShareMem_val);
}


#if EEP_CONTENT_VipSwdlShareMem_ITEM_LEN == 1
#if EEP_CONTENT_VipSwdlShareMem_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_VipSwdlShareMem_Callback(EEP_HANDLE eep_handle,uint8_t const VipSwdlShareMem_val)
#elif EEP_CONTENT_VipSwdlShareMem_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_VipSwdlShareMem_Callback(EEP_HANDLE eep_handle,uint16_t const VipSwdlShareMem_val)
#else
void DevEepTest_VipSwdlShareMem_Callback(EEP_HANDLE eep_handle,uint32_t const VipSwdlShareMem_val)
#endif	
{
	
	printf("dev eep test: VipSwdlShareMem, change to dec=%d,hex=0x%08x\r\n",VipSwdlShareMem_val,VipSwdlShareMem_val);
}
#else // EEP_CONTENT_VipSwdlShareMem_ITEM_LEN > 1
#if EEP_CONTENT_VipSwdlShareMem_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_VipSwdlShareMem_Callback(EEP_HANDLE eep_handle,uint8_t const * VipSwdlShareMem_val)
#elif EEP_CONTENT_VipSwdlShareMem_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_VipSwdlShareMem_Callback(EEP_HANDLE eep_handle,uint16_t const * VipSwdlShareMem_val)
#else
void DevEepTest_VipSwdlShareMem_Callback(EEP_HANDLE eep_handle,uint32_t const * VipSwdlShareMem_val)
#endif
{
	printf("dev eep test: VipSwdlShareMem, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_VipSwdlShareMem_ITEM_LEN; i ++)
	{
		printf("%d ", VipSwdlShareMem_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_VipSwdlShareMem_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_VipSwdlShareMem_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", VipSwdlShareMem_val[i]);
		#elif EEP_CONTENT_VipSwdlShareMem_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", VipSwdlShareMem_val[i]);
		#else
		printf("%08x ", VipSwdlShareMem_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_TotalImageSFailCnt(int argc, char **argv)
{
#if EEP_CONTENT_TotalImageSFailCnt_ITEM_LEN == 1
	#if EEP_CONTENT_TotalImageSFailCnt_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t TotalImageSFailCnt_val;
	#elif EEP_CONTENT_TotalImageSFailCnt_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t TotalImageSFailCnt_val;
	#else
	uint32_t TotalImageSFailCnt_val;
	#endif
	TotalImageSFailCnt_val = atoi(argv[2]);
#else // EEP_CONTENT_TotalImageSFailCnt_ITEM_LEN > 1
	#if EEP_CONTENT_TotalImageSFailCnt_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t TotalImageSFailCnt_val[EEP_CONTENT_TotalImageSFailCnt_ITEM_LEN];
	#elif EEP_CONTENT_TotalImageSFailCnt_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t TotalImageSFailCnt_val[EEP_CONTENT_TotalImageSFailCnt_ITEM_LEN];
	#else
	uint32_t TotalImageSFailCnt_val[EEP_CONTENT_TotalImageSFailCnt_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_TotalImageSFailCnt_ITEM_LEN; i ++ )
	{
		TotalImageSFailCnt_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_TotalImageSFailCnt_ITEM_LEN}	
	
	DevEep_Set_TotalImageSFailCnt(eep_handle,TotalImageSFailCnt_val);
}


#if EEP_CONTENT_TotalImageSFailCnt_ITEM_LEN == 1
#if EEP_CONTENT_TotalImageSFailCnt_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_TotalImageSFailCnt_Callback(EEP_HANDLE eep_handle,uint8_t const TotalImageSFailCnt_val)
#elif EEP_CONTENT_TotalImageSFailCnt_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_TotalImageSFailCnt_Callback(EEP_HANDLE eep_handle,uint16_t const TotalImageSFailCnt_val)
#else
void DevEepTest_TotalImageSFailCnt_Callback(EEP_HANDLE eep_handle,uint32_t const TotalImageSFailCnt_val)
#endif	
{
	
	printf("dev eep test: TotalImageSFailCnt, change to dec=%d,hex=0x%08x\r\n",TotalImageSFailCnt_val,TotalImageSFailCnt_val);
}
#else // EEP_CONTENT_TotalImageSFailCnt_ITEM_LEN > 1
#if EEP_CONTENT_TotalImageSFailCnt_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_TotalImageSFailCnt_Callback(EEP_HANDLE eep_handle,uint8_t const * TotalImageSFailCnt_val)
#elif EEP_CONTENT_TotalImageSFailCnt_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_TotalImageSFailCnt_Callback(EEP_HANDLE eep_handle,uint16_t const * TotalImageSFailCnt_val)
#else
void DevEepTest_TotalImageSFailCnt_Callback(EEP_HANDLE eep_handle,uint32_t const * TotalImageSFailCnt_val)
#endif
{
	printf("dev eep test: TotalImageSFailCnt, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_TotalImageSFailCnt_ITEM_LEN; i ++)
	{
		printf("%d ", TotalImageSFailCnt_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_TotalImageSFailCnt_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_TotalImageSFailCnt_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", TotalImageSFailCnt_val[i]);
		#elif EEP_CONTENT_TotalImageSFailCnt_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", TotalImageSFailCnt_val[i]);
		#else
		printf("%08x ", TotalImageSFailCnt_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_trace_phy_en(int argc, char **argv)
{
#if EEP_CONTENT_trace_phy_en_ITEM_LEN == 1
	#if EEP_CONTENT_trace_phy_en_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t trace_phy_en_val;
	#elif EEP_CONTENT_trace_phy_en_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t trace_phy_en_val;
	#else
	uint32_t trace_phy_en_val;
	#endif
	trace_phy_en_val = atoi(argv[2]);
#else // EEP_CONTENT_trace_phy_en_ITEM_LEN > 1
	#if EEP_CONTENT_trace_phy_en_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t trace_phy_en_val[EEP_CONTENT_trace_phy_en_ITEM_LEN];
	#elif EEP_CONTENT_trace_phy_en_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t trace_phy_en_val[EEP_CONTENT_trace_phy_en_ITEM_LEN];
	#else
	uint32_t trace_phy_en_val[EEP_CONTENT_trace_phy_en_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_trace_phy_en_ITEM_LEN; i ++ )
	{
		trace_phy_en_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_trace_phy_en_ITEM_LEN}	
	
	DevEep_Set_trace_phy_en(eep_handle,trace_phy_en_val);
}


#if EEP_CONTENT_trace_phy_en_ITEM_LEN == 1
#if EEP_CONTENT_trace_phy_en_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_trace_phy_en_Callback(EEP_HANDLE eep_handle,uint8_t const trace_phy_en_val)
#elif EEP_CONTENT_trace_phy_en_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_trace_phy_en_Callback(EEP_HANDLE eep_handle,uint16_t const trace_phy_en_val)
#else
void DevEepTest_trace_phy_en_Callback(EEP_HANDLE eep_handle,uint32_t const trace_phy_en_val)
#endif	
{
	
	printf("dev eep test: trace_phy_en, change to dec=%d,hex=0x%08x\r\n",trace_phy_en_val,trace_phy_en_val);
}
#else // EEP_CONTENT_trace_phy_en_ITEM_LEN > 1
#if EEP_CONTENT_trace_phy_en_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_trace_phy_en_Callback(EEP_HANDLE eep_handle,uint8_t const * trace_phy_en_val)
#elif EEP_CONTENT_trace_phy_en_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_trace_phy_en_Callback(EEP_HANDLE eep_handle,uint16_t const * trace_phy_en_val)
#else
void DevEepTest_trace_phy_en_Callback(EEP_HANDLE eep_handle,uint32_t const * trace_phy_en_val)
#endif
{
	printf("dev eep test: trace_phy_en, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_trace_phy_en_ITEM_LEN; i ++)
	{
		printf("%d ", trace_phy_en_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_trace_phy_en_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_trace_phy_en_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", trace_phy_en_val[i]);
		#elif EEP_CONTENT_trace_phy_en_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", trace_phy_en_val[i]);
		#else
		printf("%08x ", trace_phy_en_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_trace_phy(int argc, char **argv)
{
#if EEP_CONTENT_trace_phy_ITEM_LEN == 1
	#if EEP_CONTENT_trace_phy_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t trace_phy_val;
	#elif EEP_CONTENT_trace_phy_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t trace_phy_val;
	#else
	uint32_t trace_phy_val;
	#endif
	trace_phy_val = atoi(argv[2]);
#else // EEP_CONTENT_trace_phy_ITEM_LEN > 1
	#if EEP_CONTENT_trace_phy_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t trace_phy_val[EEP_CONTENT_trace_phy_ITEM_LEN];
	#elif EEP_CONTENT_trace_phy_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t trace_phy_val[EEP_CONTENT_trace_phy_ITEM_LEN];
	#else
	uint32_t trace_phy_val[EEP_CONTENT_trace_phy_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_trace_phy_ITEM_LEN; i ++ )
	{
		trace_phy_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_trace_phy_ITEM_LEN}	
	
	DevEep_Set_trace_phy(eep_handle,trace_phy_val);
}


#if EEP_CONTENT_trace_phy_ITEM_LEN == 1
#if EEP_CONTENT_trace_phy_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_trace_phy_Callback(EEP_HANDLE eep_handle,uint8_t const trace_phy_val)
#elif EEP_CONTENT_trace_phy_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_trace_phy_Callback(EEP_HANDLE eep_handle,uint16_t const trace_phy_val)
#else
void DevEepTest_trace_phy_Callback(EEP_HANDLE eep_handle,uint32_t const trace_phy_val)
#endif	
{
	
	printf("dev eep test: trace_phy, change to dec=%d,hex=0x%08x\r\n",trace_phy_val,trace_phy_val);
}
#else // EEP_CONTENT_trace_phy_ITEM_LEN > 1
#if EEP_CONTENT_trace_phy_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_trace_phy_Callback(EEP_HANDLE eep_handle,uint8_t const * trace_phy_val)
#elif EEP_CONTENT_trace_phy_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_trace_phy_Callback(EEP_HANDLE eep_handle,uint16_t const * trace_phy_val)
#else
void DevEepTest_trace_phy_Callback(EEP_HANDLE eep_handle,uint32_t const * trace_phy_val)
#endif
{
	printf("dev eep test: trace_phy, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_trace_phy_ITEM_LEN; i ++)
	{
		printf("%d ", trace_phy_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_trace_phy_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_trace_phy_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", trace_phy_val[i]);
		#elif EEP_CONTENT_trace_phy_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", trace_phy_val[i]);
		#else
		printf("%08x ", trace_phy_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_VipLogModLvl(int argc, char **argv)
{
#if EEP_CONTENT_VipLogModLvl_ITEM_LEN == 1
	#if EEP_CONTENT_VipLogModLvl_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t VipLogModLvl_val;
	#elif EEP_CONTENT_VipLogModLvl_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t VipLogModLvl_val;
	#else
	uint32_t VipLogModLvl_val;
	#endif
	VipLogModLvl_val = atoi(argv[2]);
#else // EEP_CONTENT_VipLogModLvl_ITEM_LEN > 1
	#if EEP_CONTENT_VipLogModLvl_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t VipLogModLvl_val[EEP_CONTENT_VipLogModLvl_ITEM_LEN];
	#elif EEP_CONTENT_VipLogModLvl_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t VipLogModLvl_val[EEP_CONTENT_VipLogModLvl_ITEM_LEN];
	#else
	uint32_t VipLogModLvl_val[EEP_CONTENT_VipLogModLvl_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_VipLogModLvl_ITEM_LEN; i ++ )
	{
		VipLogModLvl_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_VipLogModLvl_ITEM_LEN}	
	
	DevEep_Set_VipLogModLvl(eep_handle,VipLogModLvl_val);
}


#if EEP_CONTENT_VipLogModLvl_ITEM_LEN == 1
#if EEP_CONTENT_VipLogModLvl_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_VipLogModLvl_Callback(EEP_HANDLE eep_handle,uint8_t const VipLogModLvl_val)
#elif EEP_CONTENT_VipLogModLvl_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_VipLogModLvl_Callback(EEP_HANDLE eep_handle,uint16_t const VipLogModLvl_val)
#else
void DevEepTest_VipLogModLvl_Callback(EEP_HANDLE eep_handle,uint32_t const VipLogModLvl_val)
#endif	
{
	
	printf("dev eep test: VipLogModLvl, change to dec=%d,hex=0x%08x\r\n",VipLogModLvl_val,VipLogModLvl_val);
}
#else // EEP_CONTENT_VipLogModLvl_ITEM_LEN > 1
#if EEP_CONTENT_VipLogModLvl_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_VipLogModLvl_Callback(EEP_HANDLE eep_handle,uint8_t const * VipLogModLvl_val)
#elif EEP_CONTENT_VipLogModLvl_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_VipLogModLvl_Callback(EEP_HANDLE eep_handle,uint16_t const * VipLogModLvl_val)
#else
void DevEepTest_VipLogModLvl_Callback(EEP_HANDLE eep_handle,uint32_t const * VipLogModLvl_val)
#endif
{
	printf("dev eep test: VipLogModLvl, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_VipLogModLvl_ITEM_LEN; i ++)
	{
		printf("%d ", VipLogModLvl_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_VipLogModLvl_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_VipLogModLvl_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", VipLogModLvl_val[i]);
		#elif EEP_CONTENT_VipLogModLvl_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", VipLogModLvl_val[i]);
		#else
		printf("%08x ", VipLogModLvl_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_TraceModuleLvlCrc(int argc, char **argv)
{
#if EEP_CONTENT_TraceModuleLvlCrc_ITEM_LEN == 1
	#if EEP_CONTENT_TraceModuleLvlCrc_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t TraceModuleLvlCrc_val;
	#elif EEP_CONTENT_TraceModuleLvlCrc_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t TraceModuleLvlCrc_val;
	#else
	uint32_t TraceModuleLvlCrc_val;
	#endif
	TraceModuleLvlCrc_val = atoi(argv[2]);
#else // EEP_CONTENT_TraceModuleLvlCrc_ITEM_LEN > 1
	#if EEP_CONTENT_TraceModuleLvlCrc_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t TraceModuleLvlCrc_val[EEP_CONTENT_TraceModuleLvlCrc_ITEM_LEN];
	#elif EEP_CONTENT_TraceModuleLvlCrc_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t TraceModuleLvlCrc_val[EEP_CONTENT_TraceModuleLvlCrc_ITEM_LEN];
	#else
	uint32_t TraceModuleLvlCrc_val[EEP_CONTENT_TraceModuleLvlCrc_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_TraceModuleLvlCrc_ITEM_LEN; i ++ )
	{
		TraceModuleLvlCrc_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_TraceModuleLvlCrc_ITEM_LEN}	
	
	DevEep_Set_TraceModuleLvlCrc(eep_handle,TraceModuleLvlCrc_val);
}


#if EEP_CONTENT_TraceModuleLvlCrc_ITEM_LEN == 1
#if EEP_CONTENT_TraceModuleLvlCrc_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_TraceModuleLvlCrc_Callback(EEP_HANDLE eep_handle,uint8_t const TraceModuleLvlCrc_val)
#elif EEP_CONTENT_TraceModuleLvlCrc_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_TraceModuleLvlCrc_Callback(EEP_HANDLE eep_handle,uint16_t const TraceModuleLvlCrc_val)
#else
void DevEepTest_TraceModuleLvlCrc_Callback(EEP_HANDLE eep_handle,uint32_t const TraceModuleLvlCrc_val)
#endif	
{
	
	printf("dev eep test: TraceModuleLvlCrc, change to dec=%d,hex=0x%08x\r\n",TraceModuleLvlCrc_val,TraceModuleLvlCrc_val);
}
#else // EEP_CONTENT_TraceModuleLvlCrc_ITEM_LEN > 1
#if EEP_CONTENT_TraceModuleLvlCrc_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_TraceModuleLvlCrc_Callback(EEP_HANDLE eep_handle,uint8_t const * TraceModuleLvlCrc_val)
#elif EEP_CONTENT_TraceModuleLvlCrc_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_TraceModuleLvlCrc_Callback(EEP_HANDLE eep_handle,uint16_t const * TraceModuleLvlCrc_val)
#else
void DevEepTest_TraceModuleLvlCrc_Callback(EEP_HANDLE eep_handle,uint32_t const * TraceModuleLvlCrc_val)
#endif
{
	printf("dev eep test: TraceModuleLvlCrc, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_TraceModuleLvlCrc_ITEM_LEN; i ++)
	{
		printf("%d ", TraceModuleLvlCrc_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_TraceModuleLvlCrc_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_TraceModuleLvlCrc_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", TraceModuleLvlCrc_val[i]);
		#elif EEP_CONTENT_TraceModuleLvlCrc_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", TraceModuleLvlCrc_val[i]);
		#else
		printf("%08x ", TraceModuleLvlCrc_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_TotalReset_Cnt(int argc, char **argv)
{
#if EEP_CONTENT_TotalReset_Cnt_ITEM_LEN == 1
	#if EEP_CONTENT_TotalReset_Cnt_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t TotalReset_Cnt_val;
	#elif EEP_CONTENT_TotalReset_Cnt_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t TotalReset_Cnt_val;
	#else
	uint32_t TotalReset_Cnt_val;
	#endif
	TotalReset_Cnt_val = atoi(argv[2]);
#else // EEP_CONTENT_TotalReset_Cnt_ITEM_LEN > 1
	#if EEP_CONTENT_TotalReset_Cnt_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t TotalReset_Cnt_val[EEP_CONTENT_TotalReset_Cnt_ITEM_LEN];
	#elif EEP_CONTENT_TotalReset_Cnt_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t TotalReset_Cnt_val[EEP_CONTENT_TotalReset_Cnt_ITEM_LEN];
	#else
	uint32_t TotalReset_Cnt_val[EEP_CONTENT_TotalReset_Cnt_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_TotalReset_Cnt_ITEM_LEN; i ++ )
	{
		TotalReset_Cnt_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_TotalReset_Cnt_ITEM_LEN}	
	
	DevEep_Set_TotalReset_Cnt(eep_handle,TotalReset_Cnt_val);
}


#if EEP_CONTENT_TotalReset_Cnt_ITEM_LEN == 1
#if EEP_CONTENT_TotalReset_Cnt_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_TotalReset_Cnt_Callback(EEP_HANDLE eep_handle,uint8_t const TotalReset_Cnt_val)
#elif EEP_CONTENT_TotalReset_Cnt_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_TotalReset_Cnt_Callback(EEP_HANDLE eep_handle,uint16_t const TotalReset_Cnt_val)
#else
void DevEepTest_TotalReset_Cnt_Callback(EEP_HANDLE eep_handle,uint32_t const TotalReset_Cnt_val)
#endif	
{
	
	printf("dev eep test: TotalReset_Cnt, change to dec=%d,hex=0x%08x\r\n",TotalReset_Cnt_val,TotalReset_Cnt_val);
}
#else // EEP_CONTENT_TotalReset_Cnt_ITEM_LEN > 1
#if EEP_CONTENT_TotalReset_Cnt_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_TotalReset_Cnt_Callback(EEP_HANDLE eep_handle,uint8_t const * TotalReset_Cnt_val)
#elif EEP_CONTENT_TotalReset_Cnt_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_TotalReset_Cnt_Callback(EEP_HANDLE eep_handle,uint16_t const * TotalReset_Cnt_val)
#else
void DevEepTest_TotalReset_Cnt_Callback(EEP_HANDLE eep_handle,uint32_t const * TotalReset_Cnt_val)
#endif
{
	printf("dev eep test: TotalReset_Cnt, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_TotalReset_Cnt_ITEM_LEN; i ++)
	{
		printf("%d ", TotalReset_Cnt_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_TotalReset_Cnt_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_TotalReset_Cnt_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", TotalReset_Cnt_val[i]);
		#elif EEP_CONTENT_TotalReset_Cnt_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", TotalReset_Cnt_val[i]);
		#else
		printf("%08x ", TotalReset_Cnt_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_DspPOR_cnt(int argc, char **argv)
{
#if EEP_CONTENT_DspPOR_cnt_ITEM_LEN == 1
	#if EEP_CONTENT_DspPOR_cnt_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t DspPOR_cnt_val;
	#elif EEP_CONTENT_DspPOR_cnt_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t DspPOR_cnt_val;
	#else
	uint32_t DspPOR_cnt_val;
	#endif
	DspPOR_cnt_val = atoi(argv[2]);
#else // EEP_CONTENT_DspPOR_cnt_ITEM_LEN > 1
	#if EEP_CONTENT_DspPOR_cnt_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t DspPOR_cnt_val[EEP_CONTENT_DspPOR_cnt_ITEM_LEN];
	#elif EEP_CONTENT_DspPOR_cnt_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t DspPOR_cnt_val[EEP_CONTENT_DspPOR_cnt_ITEM_LEN];
	#else
	uint32_t DspPOR_cnt_val[EEP_CONTENT_DspPOR_cnt_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_DspPOR_cnt_ITEM_LEN; i ++ )
	{
		DspPOR_cnt_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_DspPOR_cnt_ITEM_LEN}	
	
	DevEep_Set_DspPOR_cnt(eep_handle,DspPOR_cnt_val);
}


#if EEP_CONTENT_DspPOR_cnt_ITEM_LEN == 1
#if EEP_CONTENT_DspPOR_cnt_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_DspPOR_cnt_Callback(EEP_HANDLE eep_handle,uint8_t const DspPOR_cnt_val)
#elif EEP_CONTENT_DspPOR_cnt_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_DspPOR_cnt_Callback(EEP_HANDLE eep_handle,uint16_t const DspPOR_cnt_val)
#else
void DevEepTest_DspPOR_cnt_Callback(EEP_HANDLE eep_handle,uint32_t const DspPOR_cnt_val)
#endif	
{
	
	printf("dev eep test: DspPOR_cnt, change to dec=%d,hex=0x%08x\r\n",DspPOR_cnt_val,DspPOR_cnt_val);
}
#else // EEP_CONTENT_DspPOR_cnt_ITEM_LEN > 1
#if EEP_CONTENT_DspPOR_cnt_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_DspPOR_cnt_Callback(EEP_HANDLE eep_handle,uint8_t const * DspPOR_cnt_val)
#elif EEP_CONTENT_DspPOR_cnt_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_DspPOR_cnt_Callback(EEP_HANDLE eep_handle,uint16_t const * DspPOR_cnt_val)
#else
void DevEepTest_DspPOR_cnt_Callback(EEP_HANDLE eep_handle,uint32_t const * DspPOR_cnt_val)
#endif
{
	printf("dev eep test: DspPOR_cnt, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_DspPOR_cnt_ITEM_LEN; i ++)
	{
		printf("%d ", DspPOR_cnt_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_DspPOR_cnt_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_DspPOR_cnt_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", DspPOR_cnt_val[i]);
		#elif EEP_CONTENT_DspPOR_cnt_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", DspPOR_cnt_val[i]);
		#else
		printf("%08x ", DspPOR_cnt_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_AudioCmdExecMaxTm(int argc, char **argv)
{
#if EEP_CONTENT_AudioCmdExecMaxTm_ITEM_LEN == 1
	#if EEP_CONTENT_AudioCmdExecMaxTm_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t AudioCmdExecMaxTm_val;
	#elif EEP_CONTENT_AudioCmdExecMaxTm_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t AudioCmdExecMaxTm_val;
	#else
	uint32_t AudioCmdExecMaxTm_val;
	#endif
	AudioCmdExecMaxTm_val = atoi(argv[2]);
#else // EEP_CONTENT_AudioCmdExecMaxTm_ITEM_LEN > 1
	#if EEP_CONTENT_AudioCmdExecMaxTm_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t AudioCmdExecMaxTm_val[EEP_CONTENT_AudioCmdExecMaxTm_ITEM_LEN];
	#elif EEP_CONTENT_AudioCmdExecMaxTm_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t AudioCmdExecMaxTm_val[EEP_CONTENT_AudioCmdExecMaxTm_ITEM_LEN];
	#else
	uint32_t AudioCmdExecMaxTm_val[EEP_CONTENT_AudioCmdExecMaxTm_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_AudioCmdExecMaxTm_ITEM_LEN; i ++ )
	{
		AudioCmdExecMaxTm_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_AudioCmdExecMaxTm_ITEM_LEN}	
	
	DevEep_Set_AudioCmdExecMaxTm(eep_handle,AudioCmdExecMaxTm_val);
}


#if EEP_CONTENT_AudioCmdExecMaxTm_ITEM_LEN == 1
#if EEP_CONTENT_AudioCmdExecMaxTm_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_AudioCmdExecMaxTm_Callback(EEP_HANDLE eep_handle,uint8_t const AudioCmdExecMaxTm_val)
#elif EEP_CONTENT_AudioCmdExecMaxTm_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_AudioCmdExecMaxTm_Callback(EEP_HANDLE eep_handle,uint16_t const AudioCmdExecMaxTm_val)
#else
void DevEepTest_AudioCmdExecMaxTm_Callback(EEP_HANDLE eep_handle,uint32_t const AudioCmdExecMaxTm_val)
#endif	
{
	
	printf("dev eep test: AudioCmdExecMaxTm, change to dec=%d,hex=0x%08x\r\n",AudioCmdExecMaxTm_val,AudioCmdExecMaxTm_val);
}
#else // EEP_CONTENT_AudioCmdExecMaxTm_ITEM_LEN > 1
#if EEP_CONTENT_AudioCmdExecMaxTm_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_AudioCmdExecMaxTm_Callback(EEP_HANDLE eep_handle,uint8_t const * AudioCmdExecMaxTm_val)
#elif EEP_CONTENT_AudioCmdExecMaxTm_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_AudioCmdExecMaxTm_Callback(EEP_HANDLE eep_handle,uint16_t const * AudioCmdExecMaxTm_val)
#else
void DevEepTest_AudioCmdExecMaxTm_Callback(EEP_HANDLE eep_handle,uint32_t const * AudioCmdExecMaxTm_val)
#endif
{
	printf("dev eep test: AudioCmdExecMaxTm, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_AudioCmdExecMaxTm_ITEM_LEN; i ++)
	{
		printf("%d ", AudioCmdExecMaxTm_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_AudioCmdExecMaxTm_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_AudioCmdExecMaxTm_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", AudioCmdExecMaxTm_val[i]);
		#elif EEP_CONTENT_AudioCmdExecMaxTm_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", AudioCmdExecMaxTm_val[i]);
		#else
		printf("%08x ", AudioCmdExecMaxTm_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_RadioCmdExecMaxTm(int argc, char **argv)
{
#if EEP_CONTENT_RadioCmdExecMaxTm_ITEM_LEN == 1
	#if EEP_CONTENT_RadioCmdExecMaxTm_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t RadioCmdExecMaxTm_val;
	#elif EEP_CONTENT_RadioCmdExecMaxTm_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t RadioCmdExecMaxTm_val;
	#else
	uint32_t RadioCmdExecMaxTm_val;
	#endif
	RadioCmdExecMaxTm_val = atoi(argv[2]);
#else // EEP_CONTENT_RadioCmdExecMaxTm_ITEM_LEN > 1
	#if EEP_CONTENT_RadioCmdExecMaxTm_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t RadioCmdExecMaxTm_val[EEP_CONTENT_RadioCmdExecMaxTm_ITEM_LEN];
	#elif EEP_CONTENT_RadioCmdExecMaxTm_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t RadioCmdExecMaxTm_val[EEP_CONTENT_RadioCmdExecMaxTm_ITEM_LEN];
	#else
	uint32_t RadioCmdExecMaxTm_val[EEP_CONTENT_RadioCmdExecMaxTm_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_RadioCmdExecMaxTm_ITEM_LEN; i ++ )
	{
		RadioCmdExecMaxTm_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_RadioCmdExecMaxTm_ITEM_LEN}	
	
	DevEep_Set_RadioCmdExecMaxTm(eep_handle,RadioCmdExecMaxTm_val);
}


#if EEP_CONTENT_RadioCmdExecMaxTm_ITEM_LEN == 1
#if EEP_CONTENT_RadioCmdExecMaxTm_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_RadioCmdExecMaxTm_Callback(EEP_HANDLE eep_handle,uint8_t const RadioCmdExecMaxTm_val)
#elif EEP_CONTENT_RadioCmdExecMaxTm_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_RadioCmdExecMaxTm_Callback(EEP_HANDLE eep_handle,uint16_t const RadioCmdExecMaxTm_val)
#else
void DevEepTest_RadioCmdExecMaxTm_Callback(EEP_HANDLE eep_handle,uint32_t const RadioCmdExecMaxTm_val)
#endif	
{
	
	printf("dev eep test: RadioCmdExecMaxTm, change to dec=%d,hex=0x%08x\r\n",RadioCmdExecMaxTm_val,RadioCmdExecMaxTm_val);
}
#else // EEP_CONTENT_RadioCmdExecMaxTm_ITEM_LEN > 1
#if EEP_CONTENT_RadioCmdExecMaxTm_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_RadioCmdExecMaxTm_Callback(EEP_HANDLE eep_handle,uint8_t const * RadioCmdExecMaxTm_val)
#elif EEP_CONTENT_RadioCmdExecMaxTm_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_RadioCmdExecMaxTm_Callback(EEP_HANDLE eep_handle,uint16_t const * RadioCmdExecMaxTm_val)
#else
void DevEepTest_RadioCmdExecMaxTm_Callback(EEP_HANDLE eep_handle,uint32_t const * RadioCmdExecMaxTm_val)
#endif
{
	printf("dev eep test: RadioCmdExecMaxTm, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_RadioCmdExecMaxTm_ITEM_LEN; i ++)
	{
		printf("%d ", RadioCmdExecMaxTm_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_RadioCmdExecMaxTm_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_RadioCmdExecMaxTm_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", RadioCmdExecMaxTm_val[i]);
		#elif EEP_CONTENT_RadioCmdExecMaxTm_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", RadioCmdExecMaxTm_val[i]);
		#else
		printf("%08x ", RadioCmdExecMaxTm_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_CpuDisIntMaxNest(int argc, char **argv)
{
#if EEP_CONTENT_CpuDisIntMaxNest_ITEM_LEN == 1
	#if EEP_CONTENT_CpuDisIntMaxNest_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t CpuDisIntMaxNest_val;
	#elif EEP_CONTENT_CpuDisIntMaxNest_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t CpuDisIntMaxNest_val;
	#else
	uint32_t CpuDisIntMaxNest_val;
	#endif
	CpuDisIntMaxNest_val = atoi(argv[2]);
#else // EEP_CONTENT_CpuDisIntMaxNest_ITEM_LEN > 1
	#if EEP_CONTENT_CpuDisIntMaxNest_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t CpuDisIntMaxNest_val[EEP_CONTENT_CpuDisIntMaxNest_ITEM_LEN];
	#elif EEP_CONTENT_CpuDisIntMaxNest_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t CpuDisIntMaxNest_val[EEP_CONTENT_CpuDisIntMaxNest_ITEM_LEN];
	#else
	uint32_t CpuDisIntMaxNest_val[EEP_CONTENT_CpuDisIntMaxNest_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_CpuDisIntMaxNest_ITEM_LEN; i ++ )
	{
		CpuDisIntMaxNest_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_CpuDisIntMaxNest_ITEM_LEN}	
	
	DevEep_Set_CpuDisIntMaxNest(eep_handle,CpuDisIntMaxNest_val);
}


#if EEP_CONTENT_CpuDisIntMaxNest_ITEM_LEN == 1
#if EEP_CONTENT_CpuDisIntMaxNest_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_CpuDisIntMaxNest_Callback(EEP_HANDLE eep_handle,uint8_t const CpuDisIntMaxNest_val)
#elif EEP_CONTENT_CpuDisIntMaxNest_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_CpuDisIntMaxNest_Callback(EEP_HANDLE eep_handle,uint16_t const CpuDisIntMaxNest_val)
#else
void DevEepTest_CpuDisIntMaxNest_Callback(EEP_HANDLE eep_handle,uint32_t const CpuDisIntMaxNest_val)
#endif	
{
	
	printf("dev eep test: CpuDisIntMaxNest, change to dec=%d,hex=0x%08x\r\n",CpuDisIntMaxNest_val,CpuDisIntMaxNest_val);
}
#else // EEP_CONTENT_CpuDisIntMaxNest_ITEM_LEN > 1
#if EEP_CONTENT_CpuDisIntMaxNest_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_CpuDisIntMaxNest_Callback(EEP_HANDLE eep_handle,uint8_t const * CpuDisIntMaxNest_val)
#elif EEP_CONTENT_CpuDisIntMaxNest_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_CpuDisIntMaxNest_Callback(EEP_HANDLE eep_handle,uint16_t const * CpuDisIntMaxNest_val)
#else
void DevEepTest_CpuDisIntMaxNest_Callback(EEP_HANDLE eep_handle,uint32_t const * CpuDisIntMaxNest_val)
#endif
{
	printf("dev eep test: CpuDisIntMaxNest, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_CpuDisIntMaxNest_ITEM_LEN; i ++)
	{
		printf("%d ", CpuDisIntMaxNest_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_CpuDisIntMaxNest_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_CpuDisIntMaxNest_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", CpuDisIntMaxNest_val[i]);
		#elif EEP_CONTENT_CpuDisIntMaxNest_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", CpuDisIntMaxNest_val[i]);
		#else
		printf("%08x ", CpuDisIntMaxNest_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_CpuDisIntMaxTm(int argc, char **argv)
{
#if EEP_CONTENT_CpuDisIntMaxTm_ITEM_LEN == 1
	#if EEP_CONTENT_CpuDisIntMaxTm_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t CpuDisIntMaxTm_val;
	#elif EEP_CONTENT_CpuDisIntMaxTm_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t CpuDisIntMaxTm_val;
	#else
	uint32_t CpuDisIntMaxTm_val;
	#endif
	CpuDisIntMaxTm_val = atoi(argv[2]);
#else // EEP_CONTENT_CpuDisIntMaxTm_ITEM_LEN > 1
	#if EEP_CONTENT_CpuDisIntMaxTm_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t CpuDisIntMaxTm_val[EEP_CONTENT_CpuDisIntMaxTm_ITEM_LEN];
	#elif EEP_CONTENT_CpuDisIntMaxTm_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t CpuDisIntMaxTm_val[EEP_CONTENT_CpuDisIntMaxTm_ITEM_LEN];
	#else
	uint32_t CpuDisIntMaxTm_val[EEP_CONTENT_CpuDisIntMaxTm_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_CpuDisIntMaxTm_ITEM_LEN; i ++ )
	{
		CpuDisIntMaxTm_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_CpuDisIntMaxTm_ITEM_LEN}	
	
	DevEep_Set_CpuDisIntMaxTm(eep_handle,CpuDisIntMaxTm_val);
}


#if EEP_CONTENT_CpuDisIntMaxTm_ITEM_LEN == 1
#if EEP_CONTENT_CpuDisIntMaxTm_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_CpuDisIntMaxTm_Callback(EEP_HANDLE eep_handle,uint8_t const CpuDisIntMaxTm_val)
#elif EEP_CONTENT_CpuDisIntMaxTm_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_CpuDisIntMaxTm_Callback(EEP_HANDLE eep_handle,uint16_t const CpuDisIntMaxTm_val)
#else
void DevEepTest_CpuDisIntMaxTm_Callback(EEP_HANDLE eep_handle,uint32_t const CpuDisIntMaxTm_val)
#endif	
{
	
	printf("dev eep test: CpuDisIntMaxTm, change to dec=%d,hex=0x%08x\r\n",CpuDisIntMaxTm_val,CpuDisIntMaxTm_val);
}
#else // EEP_CONTENT_CpuDisIntMaxTm_ITEM_LEN > 1
#if EEP_CONTENT_CpuDisIntMaxTm_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_CpuDisIntMaxTm_Callback(EEP_HANDLE eep_handle,uint8_t const * CpuDisIntMaxTm_val)
#elif EEP_CONTENT_CpuDisIntMaxTm_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_CpuDisIntMaxTm_Callback(EEP_HANDLE eep_handle,uint16_t const * CpuDisIntMaxTm_val)
#else
void DevEepTest_CpuDisIntMaxTm_Callback(EEP_HANDLE eep_handle,uint32_t const * CpuDisIntMaxTm_val)
#endif
{
	printf("dev eep test: CpuDisIntMaxTm, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_CpuDisIntMaxTm_ITEM_LEN; i ++)
	{
		printf("%d ", CpuDisIntMaxTm_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_CpuDisIntMaxTm_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_CpuDisIntMaxTm_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", CpuDisIntMaxTm_val[i]);
		#elif EEP_CONTENT_CpuDisIntMaxTm_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", CpuDisIntMaxTm_val[i]);
		#else
		printf("%08x ", CpuDisIntMaxTm_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_CpuDisIntTooLongCnt(int argc, char **argv)
{
#if EEP_CONTENT_CpuDisIntTooLongCnt_ITEM_LEN == 1
	#if EEP_CONTENT_CpuDisIntTooLongCnt_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t CpuDisIntTooLongCnt_val;
	#elif EEP_CONTENT_CpuDisIntTooLongCnt_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t CpuDisIntTooLongCnt_val;
	#else
	uint32_t CpuDisIntTooLongCnt_val;
	#endif
	CpuDisIntTooLongCnt_val = atoi(argv[2]);
#else // EEP_CONTENT_CpuDisIntTooLongCnt_ITEM_LEN > 1
	#if EEP_CONTENT_CpuDisIntTooLongCnt_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t CpuDisIntTooLongCnt_val[EEP_CONTENT_CpuDisIntTooLongCnt_ITEM_LEN];
	#elif EEP_CONTENT_CpuDisIntTooLongCnt_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t CpuDisIntTooLongCnt_val[EEP_CONTENT_CpuDisIntTooLongCnt_ITEM_LEN];
	#else
	uint32_t CpuDisIntTooLongCnt_val[EEP_CONTENT_CpuDisIntTooLongCnt_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_CpuDisIntTooLongCnt_ITEM_LEN; i ++ )
	{
		CpuDisIntTooLongCnt_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_CpuDisIntTooLongCnt_ITEM_LEN}	
	
	DevEep_Set_CpuDisIntTooLongCnt(eep_handle,CpuDisIntTooLongCnt_val);
}


#if EEP_CONTENT_CpuDisIntTooLongCnt_ITEM_LEN == 1
#if EEP_CONTENT_CpuDisIntTooLongCnt_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_CpuDisIntTooLongCnt_Callback(EEP_HANDLE eep_handle,uint8_t const CpuDisIntTooLongCnt_val)
#elif EEP_CONTENT_CpuDisIntTooLongCnt_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_CpuDisIntTooLongCnt_Callback(EEP_HANDLE eep_handle,uint16_t const CpuDisIntTooLongCnt_val)
#else
void DevEepTest_CpuDisIntTooLongCnt_Callback(EEP_HANDLE eep_handle,uint32_t const CpuDisIntTooLongCnt_val)
#endif	
{
	
	printf("dev eep test: CpuDisIntTooLongCnt, change to dec=%d,hex=0x%08x\r\n",CpuDisIntTooLongCnt_val,CpuDisIntTooLongCnt_val);
}
#else // EEP_CONTENT_CpuDisIntTooLongCnt_ITEM_LEN > 1
#if EEP_CONTENT_CpuDisIntTooLongCnt_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_CpuDisIntTooLongCnt_Callback(EEP_HANDLE eep_handle,uint8_t const * CpuDisIntTooLongCnt_val)
#elif EEP_CONTENT_CpuDisIntTooLongCnt_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_CpuDisIntTooLongCnt_Callback(EEP_HANDLE eep_handle,uint16_t const * CpuDisIntTooLongCnt_val)
#else
void DevEepTest_CpuDisIntTooLongCnt_Callback(EEP_HANDLE eep_handle,uint32_t const * CpuDisIntTooLongCnt_val)
#endif
{
	printf("dev eep test: CpuDisIntTooLongCnt, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_CpuDisIntTooLongCnt_ITEM_LEN; i ++)
	{
		printf("%d ", CpuDisIntTooLongCnt_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_CpuDisIntTooLongCnt_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_CpuDisIntTooLongCnt_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", CpuDisIntTooLongCnt_val[i]);
		#elif EEP_CONTENT_CpuDisIntTooLongCnt_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", CpuDisIntTooLongCnt_val[i]);
		#else
		printf("%08x ", CpuDisIntTooLongCnt_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

static void DevEepTest_SocResetReason(int argc, char **argv)
{
#if EEP_CONTENT_SocResetReason_ITEM_LEN == 1
	#if EEP_CONTENT_SocResetReason_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t SocResetReason_val;
	#elif EEP_CONTENT_SocResetReason_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t SocResetReason_val;
	#else
	uint32_t SocResetReason_val;
	#endif
	SocResetReason_val = atoi(argv[2]);
#else // EEP_CONTENT_SocResetReason_ITEM_LEN > 1
	#if EEP_CONTENT_SocResetReason_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t SocResetReason_val[EEP_CONTENT_SocResetReason_ITEM_LEN];
	#elif EEP_CONTENT_SocResetReason_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t SocResetReason_val[EEP_CONTENT_SocResetReason_ITEM_LEN];
	#else
	uint32_t SocResetReason_val[EEP_CONTENT_SocResetReason_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_SocResetReason_ITEM_LEN; i ++ )
	{
		SocResetReason_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_SocResetReason_ITEM_LEN}	
	
	DevEep_Set_SocResetReason(eep_handle,SocResetReason_val);
}


#if EEP_CONTENT_SocResetReason_ITEM_LEN == 1
#if EEP_CONTENT_SocResetReason_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_SocResetReason_Callback(EEP_HANDLE eep_handle,uint8_t const SocResetReason_val)
#elif EEP_CONTENT_SocResetReason_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_SocResetReason_Callback(EEP_HANDLE eep_handle,uint16_t const SocResetReason_val)
#else
void DevEepTest_SocResetReason_Callback(EEP_HANDLE eep_handle,uint32_t const SocResetReason_val)
#endif	
{
	
	printf("dev eep test: SocResetReason, change to dec=%d,hex=0x%08x\r\n",SocResetReason_val,SocResetReason_val);
}
#else // EEP_CONTENT_SocResetReason_ITEM_LEN > 1
#if EEP_CONTENT_SocResetReason_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_SocResetReason_Callback(EEP_HANDLE eep_handle,uint8_t const * SocResetReason_val)
#elif EEP_CONTENT_SocResetReason_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_SocResetReason_Callback(EEP_HANDLE eep_handle,uint16_t const * SocResetReason_val)
#else
void DevEepTest_SocResetReason_Callback(EEP_HANDLE eep_handle,uint32_t const * SocResetReason_val)
#endif
{
	printf("dev eep test: SocResetReason, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_SocResetReason_ITEM_LEN; i ++)
	{
		printf("%d ", SocResetReason_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_SocResetReason_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_SocResetReason_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", SocResetReason_val[i]);
		#elif EEP_CONTENT_SocResetReason_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", SocResetReason_val[i]);
		#else
		printf("%08x ", SocResetReason_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif



/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

typedef struct
{
	char *name;
	char *help;
	void (*set)(int argc, char **argv);
}EEP_SET_HANDLE;

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static EEP_SET_HANDLE const eep_set_handle[EEP_CONTENT_LIST_NUMBER_MAX]=
{
	{"-Set_Manuf_Visteon_Part_Number","-Set_Manuf_Visteon_Part_Number: set Manuf_Visteon_Part_Number value\n",DevEepTest_Manuf_Visteon_Part_Number},
	{"-Set_Manuf_EquippedPCB_VPN_Sub","-Set_Manuf_EquippedPCB_VPN_Sub: set Manuf_EquippedPCB_VPN_Sub value\n",DevEepTest_Manuf_EquippedPCB_VPN_Sub},
	{"-Set_Manuf_EquippedPCB_VPN_Main","-Set_Manuf_EquippedPCB_VPN_Main: set Manuf_EquippedPCB_VPN_Main value\n",DevEepTest_Manuf_EquippedPCB_VPN_Main},
	{"-Set_Manuf_Product_Serial_Number","-Set_Manuf_Product_Serial_Number: set Manuf_Product_Serial_Number value\n",DevEepTest_Manuf_Product_Serial_Number},
	{"-Set_Manuf_EquippedPCB_Serial_Number_Sub","-Set_Manuf_EquippedPCB_Serial_Number_Sub: set Manuf_EquippedPCB_Serial_Number_Sub value\n",DevEepTest_Manuf_EquippedPCB_Serial_Number_Sub},
	{"-Set_Manuf_EquippedPCB_Serial_Number_Main","-Set_Manuf_EquippedPCB_Serial_Number_Main: set Manuf_EquippedPCB_Serial_Number_Main value\n",DevEepTest_Manuf_EquippedPCB_Serial_Number_Main},
	{"-Set_NVM_Revision","-Set_NVM_Revision: set NVM_Revision value\n",DevEepTest_NVM_Revision},
	{"-Set_Manuf_SMD_Date1","-Set_Manuf_SMD_Date1: set Manuf_SMD_Date1 value\n",DevEepTest_Manuf_SMD_Date1},
	{"-Set_Manuf_SMD_Date2","-Set_Manuf_SMD_Date2: set Manuf_SMD_Date2 value\n",DevEepTest_Manuf_SMD_Date2},
	{"-Set_Manuf_Assembly_Date","-Set_Manuf_Assembly_Date: set Manuf_Assembly_Date value\n",DevEepTest_Manuf_Assembly_Date},
	{"-Set_Traceability_Data","-Set_Traceability_Data: set Traceability_Data value\n",DevEepTest_Traceability_Data},
	{"-Set_VehicleManufacturerSparePartNumber","-Set_VehicleManufacturerSparePartNumber: set VehicleManufacturerSparePartNumber value\n",DevEepTest_VehicleManufacturerSparePartNumber},
	{"-Set_VehicleManufacturerSparePartNumber_Assembly","-Set_VehicleManufacturerSparePartNumber_Assembly: set VehicleManufacturerSparePartNumber_Assembly value\n",DevEepTest_VehicleManufacturerSparePartNumber_Assembly},
	{"-Set_Operational_Reference","-Set_Operational_Reference: set Operational_Reference value\n",DevEepTest_Operational_Reference},
	{"-Set_Supplier_Number","-Set_Supplier_Number: set Supplier_Number value\n",DevEepTest_Supplier_Number},
	{"-Set_ECU_Serial_Number","-Set_ECU_Serial_Number: set ECU_Serial_Number value\n",DevEepTest_ECU_Serial_Number},
	{"-Set_Manufacturing_Identification_Code","-Set_Manufacturing_Identification_Code: set Manufacturing_Identification_Code value\n",DevEepTest_Manufacturing_Identification_Code},
	{"-Set_VDIAG","-Set_VDIAG: set VDIAG value\n",DevEepTest_VDIAG},
	{"-Set_Config_EQ1","-Set_Config_EQ1: set Config_EQ1 value\n",DevEepTest_Config_EQ1},
	{"-Set_Vehicle_Type","-Set_Vehicle_Type: set Vehicle_Type value\n",DevEepTest_Vehicle_Type},
	{"-Set_UUID","-Set_UUID: set UUID value\n",DevEepTest_UUID},
	{"-Set_NAVI_ID","-Set_NAVI_ID: set NAVI_ID value\n",DevEepTest_NAVI_ID},
	{"-Set_DA_ID","-Set_DA_ID: set DA_ID value\n",DevEepTest_DA_ID},
	{"-Set_DAMainBoardHwVer","-Set_DAMainBoardHwVer: set DAMainBoardHwVer value\n",DevEepTest_DAMainBoardHwVer},
	{"-Set_SW_Version_Date_Format","-Set_SW_Version_Date_Format: set SW_Version_Date_Format value\n",DevEepTest_SW_Version_Date_Format},
	{"-Set_SW_Edition_Version","-Set_SW_Edition_Version: set SW_Edition_Version value\n",DevEepTest_SW_Edition_Version},
	{"-Set_VehicleManufacturerSparePartNumber_Nissan","-Set_VehicleManufacturerSparePartNumber_Nissan: set VehicleManufacturerSparePartNumber_Nissan value\n",DevEepTest_VehicleManufacturerSparePartNumber_Nissan},
	{"-Set_VisteonProductPartNumber","-Set_VisteonProductPartNumber: set VisteonProductPartNumber value\n",DevEepTest_VisteonProductPartNumber},
	{"-Set_EquippedPCBVisteonPartNumMainBorad","-Set_EquippedPCBVisteonPartNumMainBorad: set EquippedPCBVisteonPartNumMainBorad value\n",DevEepTest_EquippedPCBVisteonPartNumMainBorad},
	{"-Set_EquippedPCBVisteonPartNumSubBorad","-Set_EquippedPCBVisteonPartNumSubBorad: set EquippedPCBVisteonPartNumSubBorad value\n",DevEepTest_EquippedPCBVisteonPartNumSubBorad},
	{"-Set_DAUniqueID","-Set_DAUniqueID: set DAUniqueID value\n",DevEepTest_DAUniqueID},
	{"-Set_ProductSerialNum","-Set_ProductSerialNum: set ProductSerialNum value\n",DevEepTest_ProductSerialNum},
	{"-Set_ProductSerialNumMainBoard","-Set_ProductSerialNumMainBoard: set ProductSerialNumMainBoard value\n",DevEepTest_ProductSerialNumMainBoard},
	{"-Set_ProductSerialNumSubBoard","-Set_ProductSerialNumSubBoard: set ProductSerialNumSubBoard value\n",DevEepTest_ProductSerialNumSubBoard},
	{"-Set_SMDManufacturingDate","-Set_SMDManufacturingDate: set SMDManufacturingDate value\n",DevEepTest_SMDManufacturingDate},
	{"-Set_AssemblyManufacturingDate","-Set_AssemblyManufacturingDate: set AssemblyManufacturingDate value\n",DevEepTest_AssemblyManufacturingDate},
	{"-Set_TraceabilityBytes","-Set_TraceabilityBytes: set TraceabilityBytes value\n",DevEepTest_TraceabilityBytes},
	{"-Set_SMDPlantNum","-Set_SMDPlantNum: set SMDPlantNum value\n",DevEepTest_SMDPlantNum},
	{"-Set_AssemblyPlantNum","-Set_AssemblyPlantNum: set AssemblyPlantNum value\n",DevEepTest_AssemblyPlantNum},
	{"-Set_DEM_EVENT_CAN_COM_DATA","-Set_DEM_EVENT_CAN_COM_DATA: set DEM_EVENT_CAN_COM_DATA value\n",DevEepTest_DEM_EVENT_CAN_COM_DATA},
	{"-Set_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA","-Set_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA: set DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA value\n",DevEepTest_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA},
	{"-Set_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA","-Set_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA: set DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA value\n",DevEepTest_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA},
	{"-Set_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA","-Set_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA: set DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA value\n",DevEepTest_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA},
	{"-Set_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA","-Set_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA: set DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA value\n",DevEepTest_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA},
	{"-Set_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA","-Set_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA: set DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA value\n",DevEepTest_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA},
	{"-Set_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA","-Set_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA: set DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA value\n",DevEepTest_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA},
	{"-Set_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA","-Set_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA: set DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA value\n",DevEepTest_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA},
	{"-Set_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA","-Set_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA: set DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA value\n",DevEepTest_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA},
	{"-Set_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA","-Set_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA: set DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA value\n",DevEepTest_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA},
	{"-Set_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA","-Set_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA: set DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA value\n",DevEepTest_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA},
	{"-Set_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA","-Set_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA: set DEM_EVENT_HVAC_PANEL_CONNECTION_DATA value\n",DevEepTest_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA},
	{"-Set_TachoUpValue","-Set_TachoUpValue: set TachoUpValue value\n",DevEepTest_TachoUpValue},
	{"-Set_TachoLowValue","-Set_TachoLowValue: set TachoLowValue value\n",DevEepTest_TachoLowValue},
	{"-Set_TachoLimitValue","-Set_TachoLimitValue: set TachoLimitValue value\n",DevEepTest_TachoLimitValue},
	{"-Set_TachoUDeltaLimit","-Set_TachoUDeltaLimit: set TachoUDeltaLimit value\n",DevEepTest_TachoUDeltaLimit},
	{"-Set_TachoLDeltaLimit","-Set_TachoLDeltaLimit: set TachoLDeltaLimit value\n",DevEepTest_TachoLDeltaLimit},
	{"-Set_TachoUpChangeValue","-Set_TachoUpChangeValue: set TachoUpChangeValue value\n",DevEepTest_TachoUpChangeValue},
	{"-Set_TachoLowChangeValue","-Set_TachoLowChangeValue: set TachoLowChangeValue value\n",DevEepTest_TachoLowChangeValue},
	{"-Set_TachoRun","-Set_TachoRun: set TachoRun value\n",DevEepTest_TachoRun},
	{"-Set_Speed_Rm_1","-Set_Speed_Rm_1: set Speed_Rm_1 value\n",DevEepTest_Speed_Rm_1},
	{"-Set_Speed_Rm_2","-Set_Speed_Rm_2: set Speed_Rm_2 value\n",DevEepTest_Speed_Rm_2},
	{"-Set_Speed_Rc","-Set_Speed_Rc: set Speed_Rc value\n",DevEepTest_Speed_Rc},
	{"-Set_FuelTelltaleOn_R","-Set_FuelTelltaleOn_R: set FuelTelltaleOn_R value\n",DevEepTest_FuelTelltaleOn_R},
	{"-Set_FuelTelltaleOff_R","-Set_FuelTelltaleOff_R: set FuelTelltaleOff_R value\n",DevEepTest_FuelTelltaleOff_R},
	{"-Set_FuelWarningOnPoint_R","-Set_FuelWarningOnPoint_R: set FuelWarningOnPoint_R value\n",DevEepTest_FuelWarningOnPoint_R},
	{"-Set_Fuel_R_Open","-Set_Fuel_R_Open: set Fuel_R_Open value\n",DevEepTest_Fuel_R_Open},
	{"-Set_Fuel_R_Short","-Set_Fuel_R_Short: set Fuel_R_Short value\n",DevEepTest_Fuel_R_Short},
	{"-Set_PkbWarningJudgeV1","-Set_PkbWarningJudgeV1: set PkbWarningJudgeV1 value\n",DevEepTest_PkbWarningJudgeV1},
	{"-Set_PkbWarningJudgeV2","-Set_PkbWarningJudgeV2: set PkbWarningJudgeV2 value\n",DevEepTest_PkbWarningJudgeV2},
	{"-Set_AudioEepTest","-Set_AudioEepTest: set AudioEepTest value\n",DevEepTest_AudioEepTest},
	{"-Set_DspKeyCodeOnOff","-Set_DspKeyCodeOnOff: set DspKeyCodeOnOff value\n",DevEepTest_DspKeyCodeOnOff},
	{"-Set_DspKeyCode","-Set_DspKeyCode: set DspKeyCode value\n",DevEepTest_DspKeyCode},
	{"-Set_BatState_ChgDly","-Set_BatState_ChgDly: set BatState_ChgDly value\n",DevEepTest_BatState_ChgDly},
	{"-Set_BatVolVeryHigh_Hysteresis_H","-Set_BatVolVeryHigh_Hysteresis_H: set BatVolVeryHigh_Hysteresis_H value\n",DevEepTest_BatVolVeryHigh_Hysteresis_H},
	{"-Set_BatVolVeryHigh_Hysteresis_L","-Set_BatVolVeryHigh_Hysteresis_L: set BatVolVeryHigh_Hysteresis_L value\n",DevEepTest_BatVolVeryHigh_Hysteresis_L},
	{"-Set_BatVolHigh_Hysteresis_H","-Set_BatVolHigh_Hysteresis_H: set BatVolHigh_Hysteresis_H value\n",DevEepTest_BatVolHigh_Hysteresis_H},
	{"-Set_BatVolHigh_Hysteresis_L","-Set_BatVolHigh_Hysteresis_L: set BatVolHigh_Hysteresis_L value\n",DevEepTest_BatVolHigh_Hysteresis_L},
	{"-Set_BatVolLow_Hysteresis_H","-Set_BatVolLow_Hysteresis_H: set BatVolLow_Hysteresis_H value\n",DevEepTest_BatVolLow_Hysteresis_H},
	{"-Set_BatVolLow_Hysteresis_L","-Set_BatVolLow_Hysteresis_L: set BatVolLow_Hysteresis_L value\n",DevEepTest_BatVolLow_Hysteresis_L},
	{"-Set_BatVolVeryLow_Hysteresis_H","-Set_BatVolVeryLow_Hysteresis_H: set BatVolVeryLow_Hysteresis_H value\n",DevEepTest_BatVolVeryLow_Hysteresis_H},
	{"-Set_BatVolVeryLow_Hysteresis_L","-Set_BatVolVeryLow_Hysteresis_L: set BatVolVeryLow_Hysteresis_L value\n",DevEepTest_BatVolVeryLow_Hysteresis_L},
	{"-Set_TempState_ChgDly","-Set_TempState_ChgDly: set TempState_ChgDly value\n",DevEepTest_TempState_ChgDly},
	{"-Set_TempDegC_Low_Hysteresis_L","-Set_TempDegC_Low_Hysteresis_L: set TempDegC_Low_Hysteresis_L value\n",DevEepTest_TempDegC_Low_Hysteresis_L},
	{"-Set_TempDegC_Low_Hysteresis_H","-Set_TempDegC_Low_Hysteresis_H: set TempDegC_Low_Hysteresis_H value\n",DevEepTest_TempDegC_Low_Hysteresis_H},
	{"-Set_TempDegC_High_Hysteresis_L","-Set_TempDegC_High_Hysteresis_L: set TempDegC_High_Hysteresis_L value\n",DevEepTest_TempDegC_High_Hysteresis_L},
	{"-Set_TempDegC_High_Hysteresis_H","-Set_TempDegC_High_Hysteresis_H: set TempDegC_High_Hysteresis_H value\n",DevEepTest_TempDegC_High_Hysteresis_H},
	{"-Set_AmpHighTempProction","-Set_AmpHighTempProction: set AmpHighTempProction value\n",DevEepTest_AmpHighTempProction},
	{"-Set_CanNm_DA_S1_Delay_ms","-Set_CanNm_DA_S1_Delay_ms: set CanNm_DA_S1_Delay_ms value\n",DevEepTest_CanNm_DA_S1_Delay_ms},
	{"-Set_Vin","-Set_Vin: set Vin value\n",DevEepTest_Vin},
	{"-Set_AutoSyncTimeWithGps","-Set_AutoSyncTimeWithGps: set AutoSyncTimeWithGps value\n",DevEepTest_AutoSyncTimeWithGps},
	{"-Set_ScreenBackLightValOnDay","-Set_ScreenBackLightValOnDay: set ScreenBackLightValOnDay value\n",DevEepTest_ScreenBackLightValOnDay},
	{"-Set_ScreenBackLightValOnNight","-Set_ScreenBackLightValOnNight: set ScreenBackLightValOnNight value\n",DevEepTest_ScreenBackLightValOnNight},
	{"-Set_HistoryAverFuelCons","-Set_HistoryAverFuelCons: set HistoryAverFuelCons value\n",DevEepTest_HistoryAverFuelCons},
	{"-Set_FuelResistance","-Set_FuelResistance: set FuelResistance value\n",DevEepTest_FuelResistance},
	{"-Set_dtc_example","-Set_dtc_example: set dtc_example value\n",DevEepTest_dtc_example},
	{"-Set_TotalOdo","-Set_TotalOdo: set TotalOdo value\n",DevEepTest_TotalOdo},
	{"-Set_TotalTime","-Set_TotalTime: set TotalTime value\n",DevEepTest_TotalTime},
	{"-Set_TravelTime","-Set_TravelTime: set TravelTime value\n",DevEepTest_TravelTime},
	{"-Set_TravelOdo","-Set_TravelOdo: set TravelOdo value\n",DevEepTest_TravelOdo},
	{"-Set_TripAMeter","-Set_TripAMeter: set TripAMeter value\n",DevEepTest_TripAMeter},
	{"-Set_TripATime","-Set_TripATime: set TripATime value\n",DevEepTest_TripATime},
	{"-Set_TripBMeter","-Set_TripBMeter: set TripBMeter value\n",DevEepTest_TripBMeter},
	{"-Set_TripBTime","-Set_TripBTime: set TripBTime value\n",DevEepTest_TripBTime},
	{"-Set_CruiseDistance","-Set_CruiseDistance: set CruiseDistance value\n",DevEepTest_CruiseDistance},
	{"-Set_VipSwdlShareMem","-Set_VipSwdlShareMem: set VipSwdlShareMem value\n",DevEepTest_VipSwdlShareMem},
	{"-Set_TotalImageSFailCnt","-Set_TotalImageSFailCnt: set TotalImageSFailCnt value\n",DevEepTest_TotalImageSFailCnt},
	{"-Set_trace_phy_en","-Set_trace_phy_en: set trace_phy_en value\n",DevEepTest_trace_phy_en},
	{"-Set_trace_phy","-Set_trace_phy: set trace_phy value\n",DevEepTest_trace_phy},
	{"-Set_VipLogModLvl","-Set_VipLogModLvl: set VipLogModLvl value\n",DevEepTest_VipLogModLvl},
	{"-Set_TraceModuleLvlCrc","-Set_TraceModuleLvlCrc: set TraceModuleLvlCrc value\n",DevEepTest_TraceModuleLvlCrc},
	{"-Set_TotalReset_Cnt","-Set_TotalReset_Cnt: set TotalReset_Cnt value\n",DevEepTest_TotalReset_Cnt},
	{"-Set_DspPOR_cnt","-Set_DspPOR_cnt: set DspPOR_cnt value\n",DevEepTest_DspPOR_cnt},
	{"-Set_AudioCmdExecMaxTm","-Set_AudioCmdExecMaxTm: set AudioCmdExecMaxTm value\n",DevEepTest_AudioCmdExecMaxTm},
	{"-Set_RadioCmdExecMaxTm","-Set_RadioCmdExecMaxTm: set RadioCmdExecMaxTm value\n",DevEepTest_RadioCmdExecMaxTm},
	{"-Set_CpuDisIntMaxNest","-Set_CpuDisIntMaxNest: set CpuDisIntMaxNest value\n",DevEepTest_CpuDisIntMaxNest},
	{"-Set_CpuDisIntMaxTm","-Set_CpuDisIntMaxTm: set CpuDisIntMaxTm value\n",DevEepTest_CpuDisIntMaxTm},
	{"-Set_CpuDisIntTooLongCnt","-Set_CpuDisIntTooLongCnt: set CpuDisIntTooLongCnt value\n",DevEepTest_CpuDisIntTooLongCnt},
	{"-Set_SocResetReason","-Set_SocResetReason: set SocResetReason value\n",DevEepTest_SocResetReason},
};
/*****************************************************************************************************************************/

/*****************************************************************************************************************************/
static void DevEepRx_TestAllItem(void)
{
	DevEep_Inject_ChgCalbk_Manuf_Visteon_Part_Number(eep_handle,DevEepTest_Manuf_Visteon_Part_Number_Callback);
	DevEep_Inject_ChgCalbk_Manuf_EquippedPCB_VPN_Sub(eep_handle,DevEepTest_Manuf_EquippedPCB_VPN_Sub_Callback);
	DevEep_Inject_ChgCalbk_Manuf_EquippedPCB_VPN_Main(eep_handle,DevEepTest_Manuf_EquippedPCB_VPN_Main_Callback);
	DevEep_Inject_ChgCalbk_Manuf_Product_Serial_Number(eep_handle,DevEepTest_Manuf_Product_Serial_Number_Callback);
	DevEep_Inject_ChgCalbk_Manuf_EquippedPCB_Serial_Number_Sub(eep_handle,DevEepTest_Manuf_EquippedPCB_Serial_Number_Sub_Callback);
	DevEep_Inject_ChgCalbk_Manuf_EquippedPCB_Serial_Number_Main(eep_handle,DevEepTest_Manuf_EquippedPCB_Serial_Number_Main_Callback);
	DevEep_Inject_ChgCalbk_NVM_Revision(eep_handle,DevEepTest_NVM_Revision_Callback);
	DevEep_Inject_ChgCalbk_Manuf_SMD_Date1(eep_handle,DevEepTest_Manuf_SMD_Date1_Callback);
	DevEep_Inject_ChgCalbk_Manuf_SMD_Date2(eep_handle,DevEepTest_Manuf_SMD_Date2_Callback);
	DevEep_Inject_ChgCalbk_Manuf_Assembly_Date(eep_handle,DevEepTest_Manuf_Assembly_Date_Callback);
	DevEep_Inject_ChgCalbk_Traceability_Data(eep_handle,DevEepTest_Traceability_Data_Callback);
	DevEep_Inject_ChgCalbk_VehicleManufacturerSparePartNumber(eep_handle,DevEepTest_VehicleManufacturerSparePartNumber_Callback);
	DevEep_Inject_ChgCalbk_VehicleManufacturerSparePartNumber_Assembly(eep_handle,DevEepTest_VehicleManufacturerSparePartNumber_Assembly_Callback);
	DevEep_Inject_ChgCalbk_Operational_Reference(eep_handle,DevEepTest_Operational_Reference_Callback);
	DevEep_Inject_ChgCalbk_Supplier_Number(eep_handle,DevEepTest_Supplier_Number_Callback);
	DevEep_Inject_ChgCalbk_ECU_Serial_Number(eep_handle,DevEepTest_ECU_Serial_Number_Callback);
	DevEep_Inject_ChgCalbk_Manufacturing_Identification_Code(eep_handle,DevEepTest_Manufacturing_Identification_Code_Callback);
	DevEep_Inject_ChgCalbk_VDIAG(eep_handle,DevEepTest_VDIAG_Callback);
	DevEep_Inject_ChgCalbk_Config_EQ1(eep_handle,DevEepTest_Config_EQ1_Callback);
	DevEep_Inject_ChgCalbk_Vehicle_Type(eep_handle,DevEepTest_Vehicle_Type_Callback);
	DevEep_Inject_ChgCalbk_UUID(eep_handle,DevEepTest_UUID_Callback);
	DevEep_Inject_ChgCalbk_NAVI_ID(eep_handle,DevEepTest_NAVI_ID_Callback);
	DevEep_Inject_ChgCalbk_DA_ID(eep_handle,DevEepTest_DA_ID_Callback);
	DevEep_Inject_ChgCalbk_DAMainBoardHwVer(eep_handle,DevEepTest_DAMainBoardHwVer_Callback);
	DevEep_Inject_ChgCalbk_SW_Version_Date_Format(eep_handle,DevEepTest_SW_Version_Date_Format_Callback);
	DevEep_Inject_ChgCalbk_SW_Edition_Version(eep_handle,DevEepTest_SW_Edition_Version_Callback);
	DevEep_Inject_ChgCalbk_VehicleManufacturerSparePartNumber_Nissan(eep_handle,DevEepTest_VehicleManufacturerSparePartNumber_Nissan_Callback);
	DevEep_Inject_ChgCalbk_VisteonProductPartNumber(eep_handle,DevEepTest_VisteonProductPartNumber_Callback);
	DevEep_Inject_ChgCalbk_EquippedPCBVisteonPartNumMainBorad(eep_handle,DevEepTest_EquippedPCBVisteonPartNumMainBorad_Callback);
	DevEep_Inject_ChgCalbk_EquippedPCBVisteonPartNumSubBorad(eep_handle,DevEepTest_EquippedPCBVisteonPartNumSubBorad_Callback);
	DevEep_Inject_ChgCalbk_DAUniqueID(eep_handle,DevEepTest_DAUniqueID_Callback);
	DevEep_Inject_ChgCalbk_ProductSerialNum(eep_handle,DevEepTest_ProductSerialNum_Callback);
	DevEep_Inject_ChgCalbk_ProductSerialNumMainBoard(eep_handle,DevEepTest_ProductSerialNumMainBoard_Callback);
	DevEep_Inject_ChgCalbk_ProductSerialNumSubBoard(eep_handle,DevEepTest_ProductSerialNumSubBoard_Callback);
	DevEep_Inject_ChgCalbk_SMDManufacturingDate(eep_handle,DevEepTest_SMDManufacturingDate_Callback);
	DevEep_Inject_ChgCalbk_AssemblyManufacturingDate(eep_handle,DevEepTest_AssemblyManufacturingDate_Callback);
	DevEep_Inject_ChgCalbk_TraceabilityBytes(eep_handle,DevEepTest_TraceabilityBytes_Callback);
	DevEep_Inject_ChgCalbk_SMDPlantNum(eep_handle,DevEepTest_SMDPlantNum_Callback);
	DevEep_Inject_ChgCalbk_AssemblyPlantNum(eep_handle,DevEepTest_AssemblyPlantNum_Callback);
	DevEep_Inject_ChgCalbk_DEM_EVENT_CAN_COM_DATA(eep_handle,DevEepTest_DEM_EVENT_CAN_COM_DATA_Callback);
	DevEep_Inject_ChgCalbk_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA(eep_handle,DevEepTest_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_Callback);
	DevEep_Inject_ChgCalbk_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA(eep_handle,DevEepTest_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_Callback);
	DevEep_Inject_ChgCalbk_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA(eep_handle,DevEepTest_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_Callback);
	DevEep_Inject_ChgCalbk_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA(eep_handle,DevEepTest_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_Callback);
	DevEep_Inject_ChgCalbk_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA(eep_handle,DevEepTest_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_Callback);
	DevEep_Inject_ChgCalbk_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA(eep_handle,DevEepTest_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_Callback);
	DevEep_Inject_ChgCalbk_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA(eep_handle,DevEepTest_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_Callback);
	DevEep_Inject_ChgCalbk_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA(eep_handle,DevEepTest_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_Callback);
	DevEep_Inject_ChgCalbk_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA(eep_handle,DevEepTest_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_Callback);
	DevEep_Inject_ChgCalbk_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA(eep_handle,DevEepTest_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_Callback);
	DevEep_Inject_ChgCalbk_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA(eep_handle,DevEepTest_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_Callback);
	DevEep_Inject_ChgCalbk_TachoUpValue(eep_handle,DevEepTest_TachoUpValue_Callback);
	DevEep_Inject_ChgCalbk_TachoLowValue(eep_handle,DevEepTest_TachoLowValue_Callback);
	DevEep_Inject_ChgCalbk_TachoLimitValue(eep_handle,DevEepTest_TachoLimitValue_Callback);
	DevEep_Inject_ChgCalbk_TachoUDeltaLimit(eep_handle,DevEepTest_TachoUDeltaLimit_Callback);
	DevEep_Inject_ChgCalbk_TachoLDeltaLimit(eep_handle,DevEepTest_TachoLDeltaLimit_Callback);
	DevEep_Inject_ChgCalbk_TachoUpChangeValue(eep_handle,DevEepTest_TachoUpChangeValue_Callback);
	DevEep_Inject_ChgCalbk_TachoLowChangeValue(eep_handle,DevEepTest_TachoLowChangeValue_Callback);
	DevEep_Inject_ChgCalbk_TachoRun(eep_handle,DevEepTest_TachoRun_Callback);
	DevEep_Inject_ChgCalbk_Speed_Rm_1(eep_handle,DevEepTest_Speed_Rm_1_Callback);
	DevEep_Inject_ChgCalbk_Speed_Rm_2(eep_handle,DevEepTest_Speed_Rm_2_Callback);
	DevEep_Inject_ChgCalbk_Speed_Rc(eep_handle,DevEepTest_Speed_Rc_Callback);
	DevEep_Inject_ChgCalbk_FuelTelltaleOn_R(eep_handle,DevEepTest_FuelTelltaleOn_R_Callback);
	DevEep_Inject_ChgCalbk_FuelTelltaleOff_R(eep_handle,DevEepTest_FuelTelltaleOff_R_Callback);
	DevEep_Inject_ChgCalbk_FuelWarningOnPoint_R(eep_handle,DevEepTest_FuelWarningOnPoint_R_Callback);
	DevEep_Inject_ChgCalbk_Fuel_R_Open(eep_handle,DevEepTest_Fuel_R_Open_Callback);
	DevEep_Inject_ChgCalbk_Fuel_R_Short(eep_handle,DevEepTest_Fuel_R_Short_Callback);
	DevEep_Inject_ChgCalbk_PkbWarningJudgeV1(eep_handle,DevEepTest_PkbWarningJudgeV1_Callback);
	DevEep_Inject_ChgCalbk_PkbWarningJudgeV2(eep_handle,DevEepTest_PkbWarningJudgeV2_Callback);
	DevEep_Inject_ChgCalbk_AudioEepTest(eep_handle,DevEepTest_AudioEepTest_Callback);
	DevEep_Inject_ChgCalbk_DspKeyCodeOnOff(eep_handle,DevEepTest_DspKeyCodeOnOff_Callback);
	DevEep_Inject_ChgCalbk_DspKeyCode(eep_handle,DevEepTest_DspKeyCode_Callback);
	DevEep_Inject_ChgCalbk_BatState_ChgDly(eep_handle,DevEepTest_BatState_ChgDly_Callback);
	DevEep_Inject_ChgCalbk_BatVolVeryHigh_Hysteresis_H(eep_handle,DevEepTest_BatVolVeryHigh_Hysteresis_H_Callback);
	DevEep_Inject_ChgCalbk_BatVolVeryHigh_Hysteresis_L(eep_handle,DevEepTest_BatVolVeryHigh_Hysteresis_L_Callback);
	DevEep_Inject_ChgCalbk_BatVolHigh_Hysteresis_H(eep_handle,DevEepTest_BatVolHigh_Hysteresis_H_Callback);
	DevEep_Inject_ChgCalbk_BatVolHigh_Hysteresis_L(eep_handle,DevEepTest_BatVolHigh_Hysteresis_L_Callback);
	DevEep_Inject_ChgCalbk_BatVolLow_Hysteresis_H(eep_handle,DevEepTest_BatVolLow_Hysteresis_H_Callback);
	DevEep_Inject_ChgCalbk_BatVolLow_Hysteresis_L(eep_handle,DevEepTest_BatVolLow_Hysteresis_L_Callback);
	DevEep_Inject_ChgCalbk_BatVolVeryLow_Hysteresis_H(eep_handle,DevEepTest_BatVolVeryLow_Hysteresis_H_Callback);
	DevEep_Inject_ChgCalbk_BatVolVeryLow_Hysteresis_L(eep_handle,DevEepTest_BatVolVeryLow_Hysteresis_L_Callback);
	DevEep_Inject_ChgCalbk_TempState_ChgDly(eep_handle,DevEepTest_TempState_ChgDly_Callback);
	DevEep_Inject_ChgCalbk_TempDegC_Low_Hysteresis_L(eep_handle,DevEepTest_TempDegC_Low_Hysteresis_L_Callback);
	DevEep_Inject_ChgCalbk_TempDegC_Low_Hysteresis_H(eep_handle,DevEepTest_TempDegC_Low_Hysteresis_H_Callback);
	DevEep_Inject_ChgCalbk_TempDegC_High_Hysteresis_L(eep_handle,DevEepTest_TempDegC_High_Hysteresis_L_Callback);
	DevEep_Inject_ChgCalbk_TempDegC_High_Hysteresis_H(eep_handle,DevEepTest_TempDegC_High_Hysteresis_H_Callback);
	DevEep_Inject_ChgCalbk_AmpHighTempProction(eep_handle,DevEepTest_AmpHighTempProction_Callback);
	DevEep_Inject_ChgCalbk_CanNm_DA_S1_Delay_ms(eep_handle,DevEepTest_CanNm_DA_S1_Delay_ms_Callback);
	DevEep_Inject_ChgCalbk_Vin(eep_handle,DevEepTest_Vin_Callback);
	DevEep_Inject_ChgCalbk_AutoSyncTimeWithGps(eep_handle,DevEepTest_AutoSyncTimeWithGps_Callback);
	DevEep_Inject_ChgCalbk_ScreenBackLightValOnDay(eep_handle,DevEepTest_ScreenBackLightValOnDay_Callback);
	DevEep_Inject_ChgCalbk_ScreenBackLightValOnNight(eep_handle,DevEepTest_ScreenBackLightValOnNight_Callback);
	DevEep_Inject_ChgCalbk_HistoryAverFuelCons(eep_handle,DevEepTest_HistoryAverFuelCons_Callback);
	DevEep_Inject_ChgCalbk_FuelResistance(eep_handle,DevEepTest_FuelResistance_Callback);
	DevEep_Inject_ChgCalbk_dtc_example(eep_handle,DevEepTest_dtc_example_Callback);
	DevEep_Inject_ChgCalbk_TotalOdo(eep_handle,DevEepTest_TotalOdo_Callback);
	DevEep_Inject_ChgCalbk_TotalTime(eep_handle,DevEepTest_TotalTime_Callback);
	DevEep_Inject_ChgCalbk_TravelTime(eep_handle,DevEepTest_TravelTime_Callback);
	DevEep_Inject_ChgCalbk_TravelOdo(eep_handle,DevEepTest_TravelOdo_Callback);
	DevEep_Inject_ChgCalbk_TripAMeter(eep_handle,DevEepTest_TripAMeter_Callback);
	DevEep_Inject_ChgCalbk_TripATime(eep_handle,DevEepTest_TripATime_Callback);
	DevEep_Inject_ChgCalbk_TripBMeter(eep_handle,DevEepTest_TripBMeter_Callback);
	DevEep_Inject_ChgCalbk_TripBTime(eep_handle,DevEepTest_TripBTime_Callback);
	DevEep_Inject_ChgCalbk_CruiseDistance(eep_handle,DevEepTest_CruiseDistance_Callback);
	DevEep_Inject_ChgCalbk_VipSwdlShareMem(eep_handle,DevEepTest_VipSwdlShareMem_Callback);
	DevEep_Inject_ChgCalbk_TotalImageSFailCnt(eep_handle,DevEepTest_TotalImageSFailCnt_Callback);
	DevEep_Inject_ChgCalbk_trace_phy_en(eep_handle,DevEepTest_trace_phy_en_Callback);
	DevEep_Inject_ChgCalbk_trace_phy(eep_handle,DevEepTest_trace_phy_Callback);
	DevEep_Inject_ChgCalbk_VipLogModLvl(eep_handle,DevEepTest_VipLogModLvl_Callback);
	DevEep_Inject_ChgCalbk_TraceModuleLvlCrc(eep_handle,DevEepTest_TraceModuleLvlCrc_Callback);
	DevEep_Inject_ChgCalbk_TotalReset_Cnt(eep_handle,DevEepTest_TotalReset_Cnt_Callback);
	DevEep_Inject_ChgCalbk_DspPOR_cnt(eep_handle,DevEepTest_DspPOR_cnt_Callback);
	DevEep_Inject_ChgCalbk_AudioCmdExecMaxTm(eep_handle,DevEepTest_AudioCmdExecMaxTm_Callback);
	DevEep_Inject_ChgCalbk_RadioCmdExecMaxTm(eep_handle,DevEepTest_RadioCmdExecMaxTm_Callback);
	DevEep_Inject_ChgCalbk_CpuDisIntMaxNest(eep_handle,DevEepTest_CpuDisIntMaxNest_Callback);
	DevEep_Inject_ChgCalbk_CpuDisIntMaxTm(eep_handle,DevEepTest_CpuDisIntMaxTm_Callback);
	DevEep_Inject_ChgCalbk_CpuDisIntTooLongCnt(eep_handle,DevEepTest_CpuDisIntTooLongCnt_Callback);
	DevEep_Inject_ChgCalbk_SocResetReason(eep_handle,DevEepTest_SocResetReason_Callback);

	while(1)
	{
		sleep(1);
	}		
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
int main(int argc, char **argv)
{
	if ( argc < 2 )
	{	// output help info
		printf("\n\nhelp:\r\n-Rx: received all signal\n");
		for(uint16_t i = 0; i < EEP_CONTENT_LIST_NUMBER_MAX; i ++ )
		{
			printf("%s",eep_set_handle[i].help);
		}
		return 0;
	}
	
	eep_handle = DevEep_Init();

	if(!strcmp(argv[1], "-Rx")) 
	{
		DevEepRx_TestAllItem();	// test all rx signal	
	}
	else
	{
		uint8_t name_ok = 0x00;
		for( uint16_t i = 0x00; i < EEP_CONTENT_LIST_NUMBER_MAX; i ++ )
		{
			if( !strcmp(argv[1], eep_set_handle[i].name) )
			{
				eep_set_handle[i].set(argc,argv);
				name_ok = 0x01;
				break;
			}
		}
		if ( name_ok == 0x00 )
		{
			printf("no %s eep item\r\n",argv[1]);
		}
	}
	

	return 0;
}
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

/** >>>>>>>> end of file  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


