#ifndef GEN_DEV_EEP_H__
#define GEN_DEV_EEP_H__
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

/*****this file is generate by perl**************************************************************************************** **/
#include"stdint.h"

#ifdef __cplusplus
extern "C" {
#endif

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
/*
	0x03									;			(imx->vip), read all eeprom command
 	0x83,xx,xx,xx,xx						;			(vip->imx), transfer all eeprom to imx
	
	
	(imx->vip), read eeprom by offset
	0x04,offset_h,offset_l, len_h, len_l,(offset_h,offset_l, len_h, len_l);	
	
	(vip->imx), transfer eeprom to imx by offset	
	0x84,offset_h,offset_l, len_h, len_l, xx xx xx xx,(offset_h,offset_l, len_h, len_l, xx xx xx xx);	

	
	(imx->vip), write eeprom by offset
	0x05,offset_h,offset_l, len_h, len_l, xx xx xx xx,(offset_h,offset_l, len_h, len_l, xx xx xx xx);
	
	(vip->imx), write eeprom by offset success 
	0x85,offset_h,offset_l, len_h, len_l, (offset_h,offset_l, len_h, len_l)			 ;	
	
	
	
	0x20,eep_list_id0_h eep_list_id0_l eep_list_id1_h eep_list_id1_l xx xx ;				(imx->vip), read eeprom by eeprom multi list id
	0xA0,eep_list_id0_h,eep_list_id0_l xx xx xx xx xx,eep_list_id1_h eep_list_id1_l xx xx ;	(vip->imx), transfer eeprom to imx by multi eeprom list id 
	
	
	0x21,eep_list_id0_h,eep_list_id0_l xx xx xx xx xx,eep_list_id1_h eep_list_id1_l xx xx;		(imx->vip), write eeprom by eeprom list id
	0xA1,eep_list_id0_h eep_list_id0_l eep_list_id1_h eep_list_id1_l xx xx ;					(vip->imx), write eeprom by list id success 
2.	
*/

//<<INCLUDE>>

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#define EEP_ITEM_TYPE_8			(0)
#define EEP_ITEM_TYPE_16		(1)
#define EEP_ITEM_TYPE_32		(2)

#define EEP_GEN_TIME	(1571293326)


#define EEP_NORMAL_ADDR	(0u)	/*lint -e534 -e835*/
#define EEP_BACKUK_ADDR	(1024u)
#define EEP_SECTOR_SIZE	(8u)
#define EEP_PROGRAM_SIZE	(4u)


#define DFLASH_START_ADDR	(0x10000000)
#define DFLASH_SECTOR_SIZE	(4096)
#define DFLASH_PROGRAM_SIZE	(8u)


#define EEP_BLOCK_manufacture_BLOCK_ID			(0)
#define	EEP_BLOCK_manufacture_LEN  (512u)
#define EEP_BLOCK_manufacture_IS_EEPROM (FALSE)
#define EEP_BLOCK_manufacture_DFLASH_START_ADDR	(0+DFLASH_START_ADDR)
#define EEP_BLOCK_manufacture_DFLASH_END_ADDR	(8191+DFLASH_START_ADDR)
#define EEP_BLOCK_manufacture_DFLASH_SECTOR_START (0)
#define EEP_BLOCK_manufacture_DFLASH_SECTOR_END	(1)
#define EEP_BLOCK_manufacture_DFLASH_SECTOR_NUM 	(2)
#define EEP_BLOCK_development_nvm_BLOCK_ID			(1)
#define	EEP_BLOCK_development_nvm_LEN  (1024u)
#define EEP_BLOCK_development_nvm_IS_EEPROM (FALSE)
#define EEP_BLOCK_development_nvm_DFLASH_START_ADDR	(8192+DFLASH_START_ADDR)
#define EEP_BLOCK_development_nvm_DFLASH_END_ADDR	(16383+DFLASH_START_ADDR)
#define EEP_BLOCK_development_nvm_DFLASH_SECTOR_START (2)
#define EEP_BLOCK_development_nvm_DFLASH_SECTOR_END	(3)
#define EEP_BLOCK_development_nvm_DFLASH_SECTOR_NUM 	(2)
#define EEP_BLOCK_OemSetting_BLOCK_ID			(2)
#define	EEP_BLOCK_OemSetting_LEN  (256u)
#define EEP_BLOCK_OemSetting_IS_EEPROM (FALSE)
#define EEP_BLOCK_OemSetting_DFLASH_START_ADDR	(16384+DFLASH_START_ADDR)
#define EEP_BLOCK_OemSetting_DFLASH_END_ADDR	(24575+DFLASH_START_ADDR)
#define EEP_BLOCK_OemSetting_DFLASH_SECTOR_START (4)
#define EEP_BLOCK_OemSetting_DFLASH_SECTOR_END	(5)
#define EEP_BLOCK_OemSetting_DFLASH_SECTOR_NUM 	(2)
#define EEP_BLOCK_user_setting_BLOCK_ID			(3)
#define	EEP_BLOCK_user_setting_LEN  (256u)
#define EEP_BLOCK_user_setting_IS_EEPROM (FALSE)
#define EEP_BLOCK_user_setting_DFLASH_START_ADDR	(24576+DFLASH_START_ADDR)
#define EEP_BLOCK_user_setting_DFLASH_END_ADDR	(32767+DFLASH_START_ADDR)
#define EEP_BLOCK_user_setting_DFLASH_SECTOR_START (6)
#define EEP_BLOCK_user_setting_DFLASH_SECTOR_END	(7)
#define EEP_BLOCK_user_setting_DFLASH_SECTOR_NUM 	(2)
#define EEP_BLOCK_DTC_INFO_BLOCK_ID			(4)
#define	EEP_BLOCK_DTC_INFO_LEN  (256u)
#define EEP_BLOCK_DTC_INFO_IS_EEPROM (FALSE)
#define EEP_BLOCK_DTC_INFO_DFLASH_START_ADDR	(32768+DFLASH_START_ADDR)
#define EEP_BLOCK_DTC_INFO_DFLASH_END_ADDR	(40959+DFLASH_START_ADDR)
#define EEP_BLOCK_DTC_INFO_DFLASH_SECTOR_START (8)
#define EEP_BLOCK_DTC_INFO_DFLASH_SECTOR_END	(9)
#define EEP_BLOCK_DTC_INFO_DFLASH_SECTOR_NUM 	(2)
#define EEP_BLOCK_OdoInfo_BLOCK_ID			(5)
#define	EEP_BLOCK_OdoInfo_LEN  (64u)
#define EEP_BLOCK_OdoInfo_IS_EEPROM (FALSE)
#define EEP_BLOCK_OdoInfo_DFLASH_START_ADDR	(40960+DFLASH_START_ADDR)
#define EEP_BLOCK_OdoInfo_DFLASH_END_ADDR	(73727+DFLASH_START_ADDR)
#define EEP_BLOCK_OdoInfo_DFLASH_SECTOR_START (10)
#define EEP_BLOCK_OdoInfo_DFLASH_SECTOR_END	(17)
#define EEP_BLOCK_OdoInfo_DFLASH_SECTOR_NUM 	(8)
#define EEP_BLOCK_share_eep_BLOCK_ID			(6)
#define	EEP_BLOCK_share_eep_LEN  (128u)
#define EEP_BLOCK_share_eep_IS_EEPROM (FALSE)
#define EEP_BLOCK_share_eep_DFLASH_START_ADDR	(114688+DFLASH_START_ADDR)
#define EEP_BLOCK_share_eep_DFLASH_END_ADDR	(122879+DFLASH_START_ADDR)
#define EEP_BLOCK_share_eep_DFLASH_SECTOR_START (28)
#define EEP_BLOCK_share_eep_DFLASH_SECTOR_END	(29)
#define EEP_BLOCK_share_eep_DFLASH_SECTOR_NUM 	(2)
#define EEP_BLOCK_ResetInfo_BLOCK_ID			(7)
#define	EEP_BLOCK_ResetInfo_LEN  (128u)
#define EEP_BLOCK_ResetInfo_IS_EEPROM (FALSE)
#define EEP_BLOCK_ResetInfo_DFLASH_START_ADDR	(122880+DFLASH_START_ADDR)
#define EEP_BLOCK_ResetInfo_DFLASH_END_ADDR	(131071+DFLASH_START_ADDR)
#define EEP_BLOCK_ResetInfo_DFLASH_SECTOR_START (30)
#define EEP_BLOCK_ResetInfo_DFLASH_SECTOR_END	(31)
#define EEP_BLOCK_ResetInfo_DFLASH_SECTOR_NUM 	(2)
#define	EEP_BLOCK_NUMBER_MAX	(8)


#define EEP_CONTENT_Manuf_Visteon_Part_Number_LIST_ID				(0)
#define EEP_CONTENT_Manuf_Visteon_Part_Number_ITEM_LEN		(17)
#define EEP_CONTENT_Manuf_Visteon_Part_Number_IN_BLOCK_NUM 	(0)
#define EEP_CONTENT_Manuf_Visteon_Part_Number_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_Manuf_Visteon_Part_Number_ITEM_BYTE_LEN		17
#define EEP_CONTENT_Manuf_Visteon_Part_Number_CONST_VARIBLE	(0u)
#define EEP_CONTENT_Manuf_EquippedPCB_VPN_Sub_LIST_ID				(1)
#define EEP_CONTENT_Manuf_EquippedPCB_VPN_Sub_ITEM_LEN		(17)
#define EEP_CONTENT_Manuf_EquippedPCB_VPN_Sub_IN_BLOCK_NUM 	(0)
#define EEP_CONTENT_Manuf_EquippedPCB_VPN_Sub_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_Manuf_EquippedPCB_VPN_Sub_ITEM_BYTE_LEN		17
#define EEP_CONTENT_Manuf_EquippedPCB_VPN_Sub_CONST_VARIBLE	(0u)
#define EEP_CONTENT_Manuf_EquippedPCB_VPN_Main_LIST_ID				(2)
#define EEP_CONTENT_Manuf_EquippedPCB_VPN_Main_ITEM_LEN		(17)
#define EEP_CONTENT_Manuf_EquippedPCB_VPN_Main_IN_BLOCK_NUM 	(0)
#define EEP_CONTENT_Manuf_EquippedPCB_VPN_Main_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_Manuf_EquippedPCB_VPN_Main_ITEM_BYTE_LEN		17
#define EEP_CONTENT_Manuf_EquippedPCB_VPN_Main_CONST_VARIBLE	(0u)
#define EEP_CONTENT_Manuf_Product_Serial_Number_LIST_ID				(3)
#define EEP_CONTENT_Manuf_Product_Serial_Number_ITEM_LEN		(5)
#define EEP_CONTENT_Manuf_Product_Serial_Number_IN_BLOCK_NUM 	(0)
#define EEP_CONTENT_Manuf_Product_Serial_Number_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_Manuf_Product_Serial_Number_ITEM_BYTE_LEN		5
#define EEP_CONTENT_Manuf_Product_Serial_Number_CONST_VARIBLE	(0u)
#define EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Sub_LIST_ID				(4)
#define EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Sub_ITEM_LEN		(5)
#define EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Sub_IN_BLOCK_NUM 	(0)
#define EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Sub_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Sub_ITEM_BYTE_LEN		5
#define EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Sub_CONST_VARIBLE	(0u)
#define EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Main_LIST_ID				(5)
#define EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Main_ITEM_LEN		(5)
#define EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Main_IN_BLOCK_NUM 	(0)
#define EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Main_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Main_ITEM_BYTE_LEN		5
#define EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Main_CONST_VARIBLE	(0u)
#define EEP_CONTENT_NVM_Revision_LIST_ID				(6)
#define EEP_CONTENT_NVM_Revision_ITEM_LEN		(1)
#define EEP_CONTENT_NVM_Revision_IN_BLOCK_NUM 	(0)
#define EEP_CONTENT_NVM_Revision_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_NVM_Revision_ITEM_BYTE_LEN		1
#define EEP_CONTENT_NVM_Revision_CONST_VARIBLE	(0u)
#define EEP_CONTENT_Manuf_SMD_Date1_LIST_ID				(7)
#define EEP_CONTENT_Manuf_SMD_Date1_ITEM_LEN		(3)
#define EEP_CONTENT_Manuf_SMD_Date1_IN_BLOCK_NUM 	(0)
#define EEP_CONTENT_Manuf_SMD_Date1_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_Manuf_SMD_Date1_ITEM_BYTE_LEN		3
#define EEP_CONTENT_Manuf_SMD_Date1_CONST_VARIBLE	(0u)
#define EEP_CONTENT_Manuf_SMD_Date2_LIST_ID				(8)
#define EEP_CONTENT_Manuf_SMD_Date2_ITEM_LEN		(3)
#define EEP_CONTENT_Manuf_SMD_Date2_IN_BLOCK_NUM 	(0)
#define EEP_CONTENT_Manuf_SMD_Date2_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_Manuf_SMD_Date2_ITEM_BYTE_LEN		3
#define EEP_CONTENT_Manuf_SMD_Date2_CONST_VARIBLE	(0u)
#define EEP_CONTENT_Manuf_Assembly_Date_LIST_ID				(9)
#define EEP_CONTENT_Manuf_Assembly_Date_ITEM_LEN		(3)
#define EEP_CONTENT_Manuf_Assembly_Date_IN_BLOCK_NUM 	(0)
#define EEP_CONTENT_Manuf_Assembly_Date_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_Manuf_Assembly_Date_ITEM_BYTE_LEN		3
#define EEP_CONTENT_Manuf_Assembly_Date_CONST_VARIBLE	(0u)
#define EEP_CONTENT_Traceability_Data_LIST_ID				(10)
#define EEP_CONTENT_Traceability_Data_ITEM_LEN		(9)
#define EEP_CONTENT_Traceability_Data_IN_BLOCK_NUM 	(0)
#define EEP_CONTENT_Traceability_Data_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_Traceability_Data_ITEM_BYTE_LEN		9
#define EEP_CONTENT_Traceability_Data_CONST_VARIBLE	(0u)
#define EEP_CONTENT_VehicleManufacturerSparePartNumber_LIST_ID				(11)
#define EEP_CONTENT_VehicleManufacturerSparePartNumber_ITEM_LEN		(10)
#define EEP_CONTENT_VehicleManufacturerSparePartNumber_IN_BLOCK_NUM 	(0)
#define EEP_CONTENT_VehicleManufacturerSparePartNumber_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_VehicleManufacturerSparePartNumber_ITEM_BYTE_LEN		10
#define EEP_CONTENT_VehicleManufacturerSparePartNumber_CONST_VARIBLE	(0u)
#define EEP_CONTENT_VehicleManufacturerSparePartNumber_Assembly_LIST_ID				(12)
#define EEP_CONTENT_VehicleManufacturerSparePartNumber_Assembly_ITEM_LEN		(10)
#define EEP_CONTENT_VehicleManufacturerSparePartNumber_Assembly_IN_BLOCK_NUM 	(0)
#define EEP_CONTENT_VehicleManufacturerSparePartNumber_Assembly_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_VehicleManufacturerSparePartNumber_Assembly_ITEM_BYTE_LEN		10
#define EEP_CONTENT_VehicleManufacturerSparePartNumber_Assembly_CONST_VARIBLE	(0u)
#define EEP_CONTENT_Operational_Reference_LIST_ID				(13)
#define EEP_CONTENT_Operational_Reference_ITEM_LEN		(10)
#define EEP_CONTENT_Operational_Reference_IN_BLOCK_NUM 	(0)
#define EEP_CONTENT_Operational_Reference_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_Operational_Reference_ITEM_BYTE_LEN		10
#define EEP_CONTENT_Operational_Reference_CONST_VARIBLE	(0u)
#define EEP_CONTENT_Supplier_Number_LIST_ID				(14)
#define EEP_CONTENT_Supplier_Number_ITEM_LEN		(4)
#define EEP_CONTENT_Supplier_Number_IN_BLOCK_NUM 	(0)
#define EEP_CONTENT_Supplier_Number_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_Supplier_Number_ITEM_BYTE_LEN		4
#define EEP_CONTENT_Supplier_Number_CONST_VARIBLE	(0u)
#define EEP_CONTENT_ECU_Serial_Number_LIST_ID				(15)
#define EEP_CONTENT_ECU_Serial_Number_ITEM_LEN		(20)
#define EEP_CONTENT_ECU_Serial_Number_IN_BLOCK_NUM 	(0)
#define EEP_CONTENT_ECU_Serial_Number_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_ECU_Serial_Number_ITEM_BYTE_LEN		20
#define EEP_CONTENT_ECU_Serial_Number_CONST_VARIBLE	(0u)
#define EEP_CONTENT_Manufacturing_Identification_Code_LIST_ID				(16)
#define EEP_CONTENT_Manufacturing_Identification_Code_ITEM_LEN		(1)
#define EEP_CONTENT_Manufacturing_Identification_Code_IN_BLOCK_NUM 	(0)
#define EEP_CONTENT_Manufacturing_Identification_Code_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_Manufacturing_Identification_Code_ITEM_BYTE_LEN		1
#define EEP_CONTENT_Manufacturing_Identification_Code_CONST_VARIBLE	(0u)
#define EEP_CONTENT_VDIAG_LIST_ID				(17)
#define EEP_CONTENT_VDIAG_ITEM_LEN		(1)
#define EEP_CONTENT_VDIAG_IN_BLOCK_NUM 	(0)
#define EEP_CONTENT_VDIAG_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_VDIAG_ITEM_BYTE_LEN		1
#define EEP_CONTENT_VDIAG_CONST_VARIBLE	(0u)
#define EEP_CONTENT_Config_EQ1_LIST_ID				(18)
#define EEP_CONTENT_Config_EQ1_ITEM_LEN		(1)
#define EEP_CONTENT_Config_EQ1_IN_BLOCK_NUM 	(0)
#define EEP_CONTENT_Config_EQ1_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_Config_EQ1_ITEM_BYTE_LEN		1
#define EEP_CONTENT_Config_EQ1_CONST_VARIBLE	(0u)
#define EEP_CONTENT_Vehicle_Type_LIST_ID				(19)
#define EEP_CONTENT_Vehicle_Type_ITEM_LEN		(1)
#define EEP_CONTENT_Vehicle_Type_IN_BLOCK_NUM 	(0)
#define EEP_CONTENT_Vehicle_Type_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_Vehicle_Type_ITEM_BYTE_LEN		1
#define EEP_CONTENT_Vehicle_Type_CONST_VARIBLE	(0u)
#define EEP_CONTENT_UUID_LIST_ID				(20)
#define EEP_CONTENT_UUID_ITEM_LEN		(16)
#define EEP_CONTENT_UUID_IN_BLOCK_NUM 	(0)
#define EEP_CONTENT_UUID_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_UUID_ITEM_BYTE_LEN		16
#define EEP_CONTENT_UUID_CONST_VARIBLE	(0u)
#define EEP_CONTENT_NAVI_ID_LIST_ID				(21)
#define EEP_CONTENT_NAVI_ID_ITEM_LEN		(16)
#define EEP_CONTENT_NAVI_ID_IN_BLOCK_NUM 	(0)
#define EEP_CONTENT_NAVI_ID_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_NAVI_ID_ITEM_BYTE_LEN		16
#define EEP_CONTENT_NAVI_ID_CONST_VARIBLE	(0u)
#define EEP_CONTENT_DA_ID_LIST_ID				(22)
#define EEP_CONTENT_DA_ID_ITEM_LEN		(16)
#define EEP_CONTENT_DA_ID_IN_BLOCK_NUM 	(0)
#define EEP_CONTENT_DA_ID_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_DA_ID_ITEM_BYTE_LEN		16
#define EEP_CONTENT_DA_ID_CONST_VARIBLE	(0u)
#define EEP_CONTENT_DAMainBoardHwVer_LIST_ID				(23)
#define EEP_CONTENT_DAMainBoardHwVer_ITEM_LEN		(16)
#define EEP_CONTENT_DAMainBoardHwVer_IN_BLOCK_NUM 	(0)
#define EEP_CONTENT_DAMainBoardHwVer_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_DAMainBoardHwVer_ITEM_BYTE_LEN		16
#define EEP_CONTENT_DAMainBoardHwVer_CONST_VARIBLE	(0u)
#define EEP_CONTENT_SW_Version_Date_Format_LIST_ID				(24)
#define EEP_CONTENT_SW_Version_Date_Format_ITEM_LEN		(18)
#define EEP_CONTENT_SW_Version_Date_Format_IN_BLOCK_NUM 	(0)
#define EEP_CONTENT_SW_Version_Date_Format_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_SW_Version_Date_Format_ITEM_BYTE_LEN		18
#define EEP_CONTENT_SW_Version_Date_Format_CONST_VARIBLE	(0u)
#define EEP_CONTENT_SW_Edition_Version_LIST_ID				(25)
#define EEP_CONTENT_SW_Edition_Version_ITEM_LEN		(18)
#define EEP_CONTENT_SW_Edition_Version_IN_BLOCK_NUM 	(0)
#define EEP_CONTENT_SW_Edition_Version_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_SW_Edition_Version_ITEM_BYTE_LEN		18
#define EEP_CONTENT_SW_Edition_Version_CONST_VARIBLE	(0u)
#define EEP_CONTENT_VehicleManufacturerSparePartNumber_Nissan_LIST_ID				(26)
#define EEP_CONTENT_VehicleManufacturerSparePartNumber_Nissan_ITEM_LEN		(10)
#define EEP_CONTENT_VehicleManufacturerSparePartNumber_Nissan_IN_BLOCK_NUM 	(0)
#define EEP_CONTENT_VehicleManufacturerSparePartNumber_Nissan_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_VehicleManufacturerSparePartNumber_Nissan_ITEM_BYTE_LEN		10
#define EEP_CONTENT_VehicleManufacturerSparePartNumber_Nissan_CONST_VARIBLE	(0u)
#define EEP_CONTENT_VisteonProductPartNumber_LIST_ID				(27)
#define EEP_CONTENT_VisteonProductPartNumber_ITEM_LEN		(17)
#define EEP_CONTENT_VisteonProductPartNumber_IN_BLOCK_NUM 	(0)
#define EEP_CONTENT_VisteonProductPartNumber_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_VisteonProductPartNumber_ITEM_BYTE_LEN		17
#define EEP_CONTENT_VisteonProductPartNumber_CONST_VARIBLE	(0u)
#define EEP_CONTENT_EquippedPCBVisteonPartNumMainBorad_LIST_ID				(28)
#define EEP_CONTENT_EquippedPCBVisteonPartNumMainBorad_ITEM_LEN		(17)
#define EEP_CONTENT_EquippedPCBVisteonPartNumMainBorad_IN_BLOCK_NUM 	(0)
#define EEP_CONTENT_EquippedPCBVisteonPartNumMainBorad_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_EquippedPCBVisteonPartNumMainBorad_ITEM_BYTE_LEN		17
#define EEP_CONTENT_EquippedPCBVisteonPartNumMainBorad_CONST_VARIBLE	(0u)
#define EEP_CONTENT_EquippedPCBVisteonPartNumSubBorad_LIST_ID				(29)
#define EEP_CONTENT_EquippedPCBVisteonPartNumSubBorad_ITEM_LEN		(17)
#define EEP_CONTENT_EquippedPCBVisteonPartNumSubBorad_IN_BLOCK_NUM 	(0)
#define EEP_CONTENT_EquippedPCBVisteonPartNumSubBorad_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_EquippedPCBVisteonPartNumSubBorad_ITEM_BYTE_LEN		17
#define EEP_CONTENT_EquippedPCBVisteonPartNumSubBorad_CONST_VARIBLE	(0u)
#define EEP_CONTENT_DAUniqueID_LIST_ID				(30)
#define EEP_CONTENT_DAUniqueID_ITEM_LEN		(16)
#define EEP_CONTENT_DAUniqueID_IN_BLOCK_NUM 	(0)
#define EEP_CONTENT_DAUniqueID_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_DAUniqueID_ITEM_BYTE_LEN		16
#define EEP_CONTENT_DAUniqueID_CONST_VARIBLE	(0u)
#define EEP_CONTENT_ProductSerialNum_LIST_ID				(31)
#define EEP_CONTENT_ProductSerialNum_ITEM_LEN		(5)
#define EEP_CONTENT_ProductSerialNum_IN_BLOCK_NUM 	(0)
#define EEP_CONTENT_ProductSerialNum_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_ProductSerialNum_ITEM_BYTE_LEN		5
#define EEP_CONTENT_ProductSerialNum_CONST_VARIBLE	(0u)
#define EEP_CONTENT_ProductSerialNumMainBoard_LIST_ID				(32)
#define EEP_CONTENT_ProductSerialNumMainBoard_ITEM_LEN		(5)
#define EEP_CONTENT_ProductSerialNumMainBoard_IN_BLOCK_NUM 	(0)
#define EEP_CONTENT_ProductSerialNumMainBoard_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_ProductSerialNumMainBoard_ITEM_BYTE_LEN		5
#define EEP_CONTENT_ProductSerialNumMainBoard_CONST_VARIBLE	(0u)
#define EEP_CONTENT_ProductSerialNumSubBoard_LIST_ID				(33)
#define EEP_CONTENT_ProductSerialNumSubBoard_ITEM_LEN		(5)
#define EEP_CONTENT_ProductSerialNumSubBoard_IN_BLOCK_NUM 	(0)
#define EEP_CONTENT_ProductSerialNumSubBoard_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_ProductSerialNumSubBoard_ITEM_BYTE_LEN		5
#define EEP_CONTENT_ProductSerialNumSubBoard_CONST_VARIBLE	(0u)
#define EEP_CONTENT_SMDManufacturingDate_LIST_ID				(34)
#define EEP_CONTENT_SMDManufacturingDate_ITEM_LEN		(3)
#define EEP_CONTENT_SMDManufacturingDate_IN_BLOCK_NUM 	(0)
#define EEP_CONTENT_SMDManufacturingDate_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_SMDManufacturingDate_ITEM_BYTE_LEN		3
#define EEP_CONTENT_SMDManufacturingDate_CONST_VARIBLE	(0u)
#define EEP_CONTENT_AssemblyManufacturingDate_LIST_ID				(35)
#define EEP_CONTENT_AssemblyManufacturingDate_ITEM_LEN		(3)
#define EEP_CONTENT_AssemblyManufacturingDate_IN_BLOCK_NUM 	(0)
#define EEP_CONTENT_AssemblyManufacturingDate_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_AssemblyManufacturingDate_ITEM_BYTE_LEN		3
#define EEP_CONTENT_AssemblyManufacturingDate_CONST_VARIBLE	(0u)
#define EEP_CONTENT_TraceabilityBytes_LIST_ID				(36)
#define EEP_CONTENT_TraceabilityBytes_ITEM_LEN		(3)
#define EEP_CONTENT_TraceabilityBytes_IN_BLOCK_NUM 	(0)
#define EEP_CONTENT_TraceabilityBytes_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_TraceabilityBytes_ITEM_BYTE_LEN		3
#define EEP_CONTENT_TraceabilityBytes_CONST_VARIBLE	(0u)
#define EEP_CONTENT_SMDPlantNum_LIST_ID				(37)
#define EEP_CONTENT_SMDPlantNum_ITEM_LEN		(3)
#define EEP_CONTENT_SMDPlantNum_IN_BLOCK_NUM 	(0)
#define EEP_CONTENT_SMDPlantNum_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_SMDPlantNum_ITEM_BYTE_LEN		3
#define EEP_CONTENT_SMDPlantNum_CONST_VARIBLE	(0u)
#define EEP_CONTENT_AssemblyPlantNum_LIST_ID				(38)
#define EEP_CONTENT_AssemblyPlantNum_ITEM_LEN		(3)
#define EEP_CONTENT_AssemblyPlantNum_IN_BLOCK_NUM 	(0)
#define EEP_CONTENT_AssemblyPlantNum_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_AssemblyPlantNum_ITEM_BYTE_LEN		3
#define EEP_CONTENT_AssemblyPlantNum_CONST_VARIBLE	(0u)
#define EEP_CONTENT_DEM_EVENT_CAN_COM_DATA_LIST_ID				(39)
#define EEP_CONTENT_DEM_EVENT_CAN_COM_DATA_ITEM_LEN		(10)
#define EEP_CONTENT_DEM_EVENT_CAN_COM_DATA_IN_BLOCK_NUM 	(0)
#define EEP_CONTENT_DEM_EVENT_CAN_COM_DATA_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_DEM_EVENT_CAN_COM_DATA_ITEM_BYTE_LEN		10
#define EEP_CONTENT_DEM_EVENT_CAN_COM_DATA_CONST_VARIBLE	(0u)
#define EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_LIST_ID				(40)
#define EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_LEN		(10)
#define EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_IN_BLOCK_NUM 	(0)
#define EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_BYTE_LEN		10
#define EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_CONST_VARIBLE	(0u)
#define EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_LIST_ID				(41)
#define EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_LEN		(10)
#define EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_IN_BLOCK_NUM 	(0)
#define EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_BYTE_LEN		10
#define EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_CONST_VARIBLE	(0u)
#define EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_LIST_ID				(42)
#define EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_LEN		(10)
#define EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_IN_BLOCK_NUM 	(0)
#define EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_BYTE_LEN		10
#define EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_CONST_VARIBLE	(0u)
#define EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_LIST_ID				(43)
#define EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_LEN		(10)
#define EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_IN_BLOCK_NUM 	(0)
#define EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_BYTE_LEN		10
#define EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_CONST_VARIBLE	(0u)
#define EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_LIST_ID				(44)
#define EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_LEN		(10)
#define EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_IN_BLOCK_NUM 	(0)
#define EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_BYTE_LEN		10
#define EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_CONST_VARIBLE	(0u)
#define EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_LIST_ID				(45)
#define EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_LEN		(10)
#define EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_IN_BLOCK_NUM 	(0)
#define EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_BYTE_LEN		10
#define EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_CONST_VARIBLE	(0u)
#define EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_LIST_ID				(46)
#define EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_LEN		(10)
#define EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_IN_BLOCK_NUM 	(0)
#define EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_BYTE_LEN		10
#define EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_CONST_VARIBLE	(0u)
#define EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_LIST_ID				(47)
#define EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_LEN		(10)
#define EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_IN_BLOCK_NUM 	(0)
#define EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_BYTE_LEN		10
#define EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_CONST_VARIBLE	(0u)
#define EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_LIST_ID				(48)
#define EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_ITEM_LEN		(10)
#define EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_IN_BLOCK_NUM 	(0)
#define EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_ITEM_BYTE_LEN		10
#define EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_CONST_VARIBLE	(0u)
#define EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_LIST_ID				(49)
#define EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_ITEM_LEN		(10)
#define EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_IN_BLOCK_NUM 	(0)
#define EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_ITEM_BYTE_LEN		10
#define EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_CONST_VARIBLE	(0u)
#define EEP_CONTENT_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_LIST_ID				(50)
#define EEP_CONTENT_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_ITEM_LEN		(10)
#define EEP_CONTENT_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_IN_BLOCK_NUM 	(0)
#define EEP_CONTENT_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_ITEM_BYTE_LEN		10
#define EEP_CONTENT_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_CONST_VARIBLE	(0u)
#define EEP_CONTENT_TachoUpValue_LIST_ID				(51)
#define EEP_CONTENT_TachoUpValue_ITEM_LEN		(2)
#define EEP_CONTENT_TachoUpValue_IN_BLOCK_NUM 	(1)
#define EEP_CONTENT_TachoUpValue_ITEM_TYPE		EEP_ITEM_TYPE_16
#define EEP_CONTENT_TachoUpValue_ITEM_BYTE_LEN		4
#define EEP_CONTENT_TachoUpValue_CONST_VARIBLE	(1u)
#define EEP_CONTENT_TachoLowValue_LIST_ID				(52)
#define EEP_CONTENT_TachoLowValue_ITEM_LEN		(2)
#define EEP_CONTENT_TachoLowValue_IN_BLOCK_NUM 	(1)
#define EEP_CONTENT_TachoLowValue_ITEM_TYPE		EEP_ITEM_TYPE_16
#define EEP_CONTENT_TachoLowValue_ITEM_BYTE_LEN		4
#define EEP_CONTENT_TachoLowValue_CONST_VARIBLE	(1u)
#define EEP_CONTENT_TachoLimitValue_LIST_ID				(53)
#define EEP_CONTENT_TachoLimitValue_ITEM_LEN		(2)
#define EEP_CONTENT_TachoLimitValue_IN_BLOCK_NUM 	(1)
#define EEP_CONTENT_TachoLimitValue_ITEM_TYPE		EEP_ITEM_TYPE_16
#define EEP_CONTENT_TachoLimitValue_ITEM_BYTE_LEN		4
#define EEP_CONTENT_TachoLimitValue_CONST_VARIBLE	(1u)
#define EEP_CONTENT_TachoUDeltaLimit_LIST_ID				(54)
#define EEP_CONTENT_TachoUDeltaLimit_ITEM_LEN		(2)
#define EEP_CONTENT_TachoUDeltaLimit_IN_BLOCK_NUM 	(1)
#define EEP_CONTENT_TachoUDeltaLimit_ITEM_TYPE		EEP_ITEM_TYPE_16
#define EEP_CONTENT_TachoUDeltaLimit_ITEM_BYTE_LEN		4
#define EEP_CONTENT_TachoUDeltaLimit_CONST_VARIBLE	(1u)
#define EEP_CONTENT_TachoLDeltaLimit_LIST_ID				(55)
#define EEP_CONTENT_TachoLDeltaLimit_ITEM_LEN		(2)
#define EEP_CONTENT_TachoLDeltaLimit_IN_BLOCK_NUM 	(1)
#define EEP_CONTENT_TachoLDeltaLimit_ITEM_TYPE		EEP_ITEM_TYPE_16
#define EEP_CONTENT_TachoLDeltaLimit_ITEM_BYTE_LEN		4
#define EEP_CONTENT_TachoLDeltaLimit_CONST_VARIBLE	(1u)
#define EEP_CONTENT_TachoUpChangeValue_LIST_ID				(56)
#define EEP_CONTENT_TachoUpChangeValue_ITEM_LEN		(2)
#define EEP_CONTENT_TachoUpChangeValue_IN_BLOCK_NUM 	(1)
#define EEP_CONTENT_TachoUpChangeValue_ITEM_TYPE		EEP_ITEM_TYPE_16
#define EEP_CONTENT_TachoUpChangeValue_ITEM_BYTE_LEN		4
#define EEP_CONTENT_TachoUpChangeValue_CONST_VARIBLE	(1u)
#define EEP_CONTENT_TachoLowChangeValue_LIST_ID				(57)
#define EEP_CONTENT_TachoLowChangeValue_ITEM_LEN		(2)
#define EEP_CONTENT_TachoLowChangeValue_IN_BLOCK_NUM 	(1)
#define EEP_CONTENT_TachoLowChangeValue_ITEM_TYPE		EEP_ITEM_TYPE_16
#define EEP_CONTENT_TachoLowChangeValue_ITEM_BYTE_LEN		4
#define EEP_CONTENT_TachoLowChangeValue_CONST_VARIBLE	(1u)
#define EEP_CONTENT_TachoRun_LIST_ID				(58)
#define EEP_CONTENT_TachoRun_ITEM_LEN		(2)
#define EEP_CONTENT_TachoRun_IN_BLOCK_NUM 	(1)
#define EEP_CONTENT_TachoRun_ITEM_TYPE		EEP_ITEM_TYPE_16
#define EEP_CONTENT_TachoRun_ITEM_BYTE_LEN		4
#define EEP_CONTENT_TachoRun_CONST_VARIBLE	(1u)
#define EEP_CONTENT_Speed_Rm_1_LIST_ID				(59)
#define EEP_CONTENT_Speed_Rm_1_ITEM_LEN		(1)
#define EEP_CONTENT_Speed_Rm_1_IN_BLOCK_NUM 	(1)
#define EEP_CONTENT_Speed_Rm_1_ITEM_TYPE		EEP_ITEM_TYPE_16
#define EEP_CONTENT_Speed_Rm_1_ITEM_BYTE_LEN		2
#define EEP_CONTENT_Speed_Rm_1_CONST_VARIBLE	(1u)
#define EEP_CONTENT_Speed_Rm_2_LIST_ID				(60)
#define EEP_CONTENT_Speed_Rm_2_ITEM_LEN		(1)
#define EEP_CONTENT_Speed_Rm_2_IN_BLOCK_NUM 	(1)
#define EEP_CONTENT_Speed_Rm_2_ITEM_TYPE		EEP_ITEM_TYPE_16
#define EEP_CONTENT_Speed_Rm_2_ITEM_BYTE_LEN		2
#define EEP_CONTENT_Speed_Rm_2_CONST_VARIBLE	(1u)
#define EEP_CONTENT_Speed_Rc_LIST_ID				(61)
#define EEP_CONTENT_Speed_Rc_ITEM_LEN		(1)
#define EEP_CONTENT_Speed_Rc_IN_BLOCK_NUM 	(1)
#define EEP_CONTENT_Speed_Rc_ITEM_TYPE		EEP_ITEM_TYPE_16
#define EEP_CONTENT_Speed_Rc_ITEM_BYTE_LEN		2
#define EEP_CONTENT_Speed_Rc_CONST_VARIBLE	(1u)
#define EEP_CONTENT_FuelTelltaleOn_R_LIST_ID				(62)
#define EEP_CONTENT_FuelTelltaleOn_R_ITEM_LEN		(1)
#define EEP_CONTENT_FuelTelltaleOn_R_IN_BLOCK_NUM 	(1)
#define EEP_CONTENT_FuelTelltaleOn_R_ITEM_TYPE		EEP_ITEM_TYPE_16
#define EEP_CONTENT_FuelTelltaleOn_R_ITEM_BYTE_LEN		2
#define EEP_CONTENT_FuelTelltaleOn_R_CONST_VARIBLE	(1u)
#define EEP_CONTENT_FuelTelltaleOff_R_LIST_ID				(63)
#define EEP_CONTENT_FuelTelltaleOff_R_ITEM_LEN		(1)
#define EEP_CONTENT_FuelTelltaleOff_R_IN_BLOCK_NUM 	(1)
#define EEP_CONTENT_FuelTelltaleOff_R_ITEM_TYPE		EEP_ITEM_TYPE_16
#define EEP_CONTENT_FuelTelltaleOff_R_ITEM_BYTE_LEN		2
#define EEP_CONTENT_FuelTelltaleOff_R_CONST_VARIBLE	(1u)
#define EEP_CONTENT_FuelWarningOnPoint_R_LIST_ID				(64)
#define EEP_CONTENT_FuelWarningOnPoint_R_ITEM_LEN		(1)
#define EEP_CONTENT_FuelWarningOnPoint_R_IN_BLOCK_NUM 	(1)
#define EEP_CONTENT_FuelWarningOnPoint_R_ITEM_TYPE		EEP_ITEM_TYPE_16
#define EEP_CONTENT_FuelWarningOnPoint_R_ITEM_BYTE_LEN		2
#define EEP_CONTENT_FuelWarningOnPoint_R_CONST_VARIBLE	(1u)
#define EEP_CONTENT_Fuel_R_Open_LIST_ID				(65)
#define EEP_CONTENT_Fuel_R_Open_ITEM_LEN		(1)
#define EEP_CONTENT_Fuel_R_Open_IN_BLOCK_NUM 	(1)
#define EEP_CONTENT_Fuel_R_Open_ITEM_TYPE		EEP_ITEM_TYPE_16
#define EEP_CONTENT_Fuel_R_Open_ITEM_BYTE_LEN		2
#define EEP_CONTENT_Fuel_R_Open_CONST_VARIBLE	(1u)
#define EEP_CONTENT_Fuel_R_Short_LIST_ID				(66)
#define EEP_CONTENT_Fuel_R_Short_ITEM_LEN		(1)
#define EEP_CONTENT_Fuel_R_Short_IN_BLOCK_NUM 	(1)
#define EEP_CONTENT_Fuel_R_Short_ITEM_TYPE		EEP_ITEM_TYPE_16
#define EEP_CONTENT_Fuel_R_Short_ITEM_BYTE_LEN		2
#define EEP_CONTENT_Fuel_R_Short_CONST_VARIBLE	(1u)
#define EEP_CONTENT_PkbWarningJudgeV1_LIST_ID				(67)
#define EEP_CONTENT_PkbWarningJudgeV1_ITEM_LEN		(1)
#define EEP_CONTENT_PkbWarningJudgeV1_IN_BLOCK_NUM 	(1)
#define EEP_CONTENT_PkbWarningJudgeV1_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_PkbWarningJudgeV1_ITEM_BYTE_LEN		1
#define EEP_CONTENT_PkbWarningJudgeV1_CONST_VARIBLE	(1u)
#define EEP_CONTENT_PkbWarningJudgeV2_LIST_ID				(68)
#define EEP_CONTENT_PkbWarningJudgeV2_ITEM_LEN		(1)
#define EEP_CONTENT_PkbWarningJudgeV2_IN_BLOCK_NUM 	(1)
#define EEP_CONTENT_PkbWarningJudgeV2_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_PkbWarningJudgeV2_ITEM_BYTE_LEN		1
#define EEP_CONTENT_PkbWarningJudgeV2_CONST_VARIBLE	(1u)
#define EEP_CONTENT_AudioEepTest_LIST_ID				(69)
#define EEP_CONTENT_AudioEepTest_ITEM_LEN		(1)
#define EEP_CONTENT_AudioEepTest_IN_BLOCK_NUM 	(1)
#define EEP_CONTENT_AudioEepTest_ITEM_TYPE		EEP_ITEM_TYPE_16
#define EEP_CONTENT_AudioEepTest_ITEM_BYTE_LEN		2
#define EEP_CONTENT_AudioEepTest_CONST_VARIBLE	(0u)
#define EEP_CONTENT_DspKeyCodeOnOff_LIST_ID				(70)
#define EEP_CONTENT_DspKeyCodeOnOff_ITEM_LEN		(1)
#define EEP_CONTENT_DspKeyCodeOnOff_IN_BLOCK_NUM 	(1)
#define EEP_CONTENT_DspKeyCodeOnOff_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_DspKeyCodeOnOff_ITEM_BYTE_LEN		1
#define EEP_CONTENT_DspKeyCodeOnOff_CONST_VARIBLE	(0u)
#define EEP_CONTENT_DspKeyCode_LIST_ID				(71)
#define EEP_CONTENT_DspKeyCode_ITEM_LEN		(1)
#define EEP_CONTENT_DspKeyCode_IN_BLOCK_NUM 	(1)
#define EEP_CONTENT_DspKeyCode_ITEM_TYPE		EEP_ITEM_TYPE_32
#define EEP_CONTENT_DspKeyCode_ITEM_BYTE_LEN		4
#define EEP_CONTENT_DspKeyCode_CONST_VARIBLE	(0u)
#define EEP_CONTENT_BatState_ChgDly_LIST_ID				(72)
#define EEP_CONTENT_BatState_ChgDly_ITEM_LEN		(1)
#define EEP_CONTENT_BatState_ChgDly_IN_BLOCK_NUM 	(1)
#define EEP_CONTENT_BatState_ChgDly_ITEM_TYPE		EEP_ITEM_TYPE_16
#define EEP_CONTENT_BatState_ChgDly_ITEM_BYTE_LEN		2
#define EEP_CONTENT_BatState_ChgDly_CONST_VARIBLE	(1u)
#define EEP_CONTENT_BatVolVeryHigh_Hysteresis_H_LIST_ID				(73)
#define EEP_CONTENT_BatVolVeryHigh_Hysteresis_H_ITEM_LEN		(1)
#define EEP_CONTENT_BatVolVeryHigh_Hysteresis_H_IN_BLOCK_NUM 	(1)
#define EEP_CONTENT_BatVolVeryHigh_Hysteresis_H_ITEM_TYPE		EEP_ITEM_TYPE_16
#define EEP_CONTENT_BatVolVeryHigh_Hysteresis_H_ITEM_BYTE_LEN		2
#define EEP_CONTENT_BatVolVeryHigh_Hysteresis_H_CONST_VARIBLE	(1u)
#define EEP_CONTENT_BatVolVeryHigh_Hysteresis_L_LIST_ID				(74)
#define EEP_CONTENT_BatVolVeryHigh_Hysteresis_L_ITEM_LEN		(1)
#define EEP_CONTENT_BatVolVeryHigh_Hysteresis_L_IN_BLOCK_NUM 	(1)
#define EEP_CONTENT_BatVolVeryHigh_Hysteresis_L_ITEM_TYPE		EEP_ITEM_TYPE_16
#define EEP_CONTENT_BatVolVeryHigh_Hysteresis_L_ITEM_BYTE_LEN		2
#define EEP_CONTENT_BatVolVeryHigh_Hysteresis_L_CONST_VARIBLE	(1u)
#define EEP_CONTENT_BatVolHigh_Hysteresis_H_LIST_ID				(75)
#define EEP_CONTENT_BatVolHigh_Hysteresis_H_ITEM_LEN		(1)
#define EEP_CONTENT_BatVolHigh_Hysteresis_H_IN_BLOCK_NUM 	(1)
#define EEP_CONTENT_BatVolHigh_Hysteresis_H_ITEM_TYPE		EEP_ITEM_TYPE_16
#define EEP_CONTENT_BatVolHigh_Hysteresis_H_ITEM_BYTE_LEN		2
#define EEP_CONTENT_BatVolHigh_Hysteresis_H_CONST_VARIBLE	(1u)
#define EEP_CONTENT_BatVolHigh_Hysteresis_L_LIST_ID				(76)
#define EEP_CONTENT_BatVolHigh_Hysteresis_L_ITEM_LEN		(1)
#define EEP_CONTENT_BatVolHigh_Hysteresis_L_IN_BLOCK_NUM 	(1)
#define EEP_CONTENT_BatVolHigh_Hysteresis_L_ITEM_TYPE		EEP_ITEM_TYPE_16
#define EEP_CONTENT_BatVolHigh_Hysteresis_L_ITEM_BYTE_LEN		2
#define EEP_CONTENT_BatVolHigh_Hysteresis_L_CONST_VARIBLE	(1u)
#define EEP_CONTENT_BatVolLow_Hysteresis_H_LIST_ID				(77)
#define EEP_CONTENT_BatVolLow_Hysteresis_H_ITEM_LEN		(1)
#define EEP_CONTENT_BatVolLow_Hysteresis_H_IN_BLOCK_NUM 	(1)
#define EEP_CONTENT_BatVolLow_Hysteresis_H_ITEM_TYPE		EEP_ITEM_TYPE_16
#define EEP_CONTENT_BatVolLow_Hysteresis_H_ITEM_BYTE_LEN		2
#define EEP_CONTENT_BatVolLow_Hysteresis_H_CONST_VARIBLE	(1u)
#define EEP_CONTENT_BatVolLow_Hysteresis_L_LIST_ID				(78)
#define EEP_CONTENT_BatVolLow_Hysteresis_L_ITEM_LEN		(1)
#define EEP_CONTENT_BatVolLow_Hysteresis_L_IN_BLOCK_NUM 	(1)
#define EEP_CONTENT_BatVolLow_Hysteresis_L_ITEM_TYPE		EEP_ITEM_TYPE_16
#define EEP_CONTENT_BatVolLow_Hysteresis_L_ITEM_BYTE_LEN		2
#define EEP_CONTENT_BatVolLow_Hysteresis_L_CONST_VARIBLE	(1u)
#define EEP_CONTENT_BatVolVeryLow_Hysteresis_H_LIST_ID				(79)
#define EEP_CONTENT_BatVolVeryLow_Hysteresis_H_ITEM_LEN		(1)
#define EEP_CONTENT_BatVolVeryLow_Hysteresis_H_IN_BLOCK_NUM 	(1)
#define EEP_CONTENT_BatVolVeryLow_Hysteresis_H_ITEM_TYPE		EEP_ITEM_TYPE_16
#define EEP_CONTENT_BatVolVeryLow_Hysteresis_H_ITEM_BYTE_LEN		2
#define EEP_CONTENT_BatVolVeryLow_Hysteresis_H_CONST_VARIBLE	(1u)
#define EEP_CONTENT_BatVolVeryLow_Hysteresis_L_LIST_ID				(80)
#define EEP_CONTENT_BatVolVeryLow_Hysteresis_L_ITEM_LEN		(1)
#define EEP_CONTENT_BatVolVeryLow_Hysteresis_L_IN_BLOCK_NUM 	(1)
#define EEP_CONTENT_BatVolVeryLow_Hysteresis_L_ITEM_TYPE		EEP_ITEM_TYPE_16
#define EEP_CONTENT_BatVolVeryLow_Hysteresis_L_ITEM_BYTE_LEN		2
#define EEP_CONTENT_BatVolVeryLow_Hysteresis_L_CONST_VARIBLE	(1u)
#define EEP_CONTENT_TempState_ChgDly_LIST_ID				(81)
#define EEP_CONTENT_TempState_ChgDly_ITEM_LEN		(1)
#define EEP_CONTENT_TempState_ChgDly_IN_BLOCK_NUM 	(1)
#define EEP_CONTENT_TempState_ChgDly_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_TempState_ChgDly_ITEM_BYTE_LEN		1
#define EEP_CONTENT_TempState_ChgDly_CONST_VARIBLE	(0u)
#define EEP_CONTENT_TempDegC_Low_Hysteresis_L_LIST_ID				(82)
#define EEP_CONTENT_TempDegC_Low_Hysteresis_L_ITEM_LEN		(1)
#define EEP_CONTENT_TempDegC_Low_Hysteresis_L_IN_BLOCK_NUM 	(1)
#define EEP_CONTENT_TempDegC_Low_Hysteresis_L_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_TempDegC_Low_Hysteresis_L_ITEM_BYTE_LEN		1
#define EEP_CONTENT_TempDegC_Low_Hysteresis_L_CONST_VARIBLE	(0u)
#define EEP_CONTENT_TempDegC_Low_Hysteresis_H_LIST_ID				(83)
#define EEP_CONTENT_TempDegC_Low_Hysteresis_H_ITEM_LEN		(1)
#define EEP_CONTENT_TempDegC_Low_Hysteresis_H_IN_BLOCK_NUM 	(1)
#define EEP_CONTENT_TempDegC_Low_Hysteresis_H_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_TempDegC_Low_Hysteresis_H_ITEM_BYTE_LEN		1
#define EEP_CONTENT_TempDegC_Low_Hysteresis_H_CONST_VARIBLE	(0u)
#define EEP_CONTENT_TempDegC_High_Hysteresis_L_LIST_ID				(84)
#define EEP_CONTENT_TempDegC_High_Hysteresis_L_ITEM_LEN		(1)
#define EEP_CONTENT_TempDegC_High_Hysteresis_L_IN_BLOCK_NUM 	(1)
#define EEP_CONTENT_TempDegC_High_Hysteresis_L_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_TempDegC_High_Hysteresis_L_ITEM_BYTE_LEN		1
#define EEP_CONTENT_TempDegC_High_Hysteresis_L_CONST_VARIBLE	(0u)
#define EEP_CONTENT_TempDegC_High_Hysteresis_H_LIST_ID				(85)
#define EEP_CONTENT_TempDegC_High_Hysteresis_H_ITEM_LEN		(1)
#define EEP_CONTENT_TempDegC_High_Hysteresis_H_IN_BLOCK_NUM 	(1)
#define EEP_CONTENT_TempDegC_High_Hysteresis_H_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_TempDegC_High_Hysteresis_H_ITEM_BYTE_LEN		1
#define EEP_CONTENT_TempDegC_High_Hysteresis_H_CONST_VARIBLE	(0u)
#define EEP_CONTENT_AmpHighTempProction_LIST_ID				(86)
#define EEP_CONTENT_AmpHighTempProction_ITEM_LEN		(5)
#define EEP_CONTENT_AmpHighTempProction_IN_BLOCK_NUM 	(1)
#define EEP_CONTENT_AmpHighTempProction_ITEM_TYPE		EEP_ITEM_TYPE_32
#define EEP_CONTENT_AmpHighTempProction_ITEM_BYTE_LEN		20
#define EEP_CONTENT_AmpHighTempProction_CONST_VARIBLE	(0u)
#define EEP_CONTENT_CanNm_DA_S1_Delay_ms_LIST_ID				(87)
#define EEP_CONTENT_CanNm_DA_S1_Delay_ms_ITEM_LEN		(1)
#define EEP_CONTENT_CanNm_DA_S1_Delay_ms_IN_BLOCK_NUM 	(1)
#define EEP_CONTENT_CanNm_DA_S1_Delay_ms_ITEM_TYPE		EEP_ITEM_TYPE_16
#define EEP_CONTENT_CanNm_DA_S1_Delay_ms_ITEM_BYTE_LEN		2
#define EEP_CONTENT_CanNm_DA_S1_Delay_ms_CONST_VARIBLE	(0u)
#define EEP_CONTENT_Vin_LIST_ID				(88)
#define EEP_CONTENT_Vin_ITEM_LEN		(17)
#define EEP_CONTENT_Vin_IN_BLOCK_NUM 	(2)
#define EEP_CONTENT_Vin_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_Vin_ITEM_BYTE_LEN		17
#define EEP_CONTENT_Vin_CONST_VARIBLE	(0u)
#define EEP_CONTENT_AutoSyncTimeWithGps_LIST_ID				(89)
#define EEP_CONTENT_AutoSyncTimeWithGps_ITEM_LEN		(1)
#define EEP_CONTENT_AutoSyncTimeWithGps_IN_BLOCK_NUM 	(3)
#define EEP_CONTENT_AutoSyncTimeWithGps_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_AutoSyncTimeWithGps_ITEM_BYTE_LEN		1
#define EEP_CONTENT_AutoSyncTimeWithGps_CONST_VARIBLE	(0u)
#define EEP_CONTENT_ScreenBackLightValOnDay_LIST_ID				(90)
#define EEP_CONTENT_ScreenBackLightValOnDay_ITEM_LEN		(1)
#define EEP_CONTENT_ScreenBackLightValOnDay_IN_BLOCK_NUM 	(3)
#define EEP_CONTENT_ScreenBackLightValOnDay_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_ScreenBackLightValOnDay_ITEM_BYTE_LEN		1
#define EEP_CONTENT_ScreenBackLightValOnDay_CONST_VARIBLE	(0u)
#define EEP_CONTENT_ScreenBackLightValOnNight_LIST_ID				(91)
#define EEP_CONTENT_ScreenBackLightValOnNight_ITEM_LEN		(1)
#define EEP_CONTENT_ScreenBackLightValOnNight_IN_BLOCK_NUM 	(3)
#define EEP_CONTENT_ScreenBackLightValOnNight_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_ScreenBackLightValOnNight_ITEM_BYTE_LEN		1
#define EEP_CONTENT_ScreenBackLightValOnNight_CONST_VARIBLE	(0u)
#define EEP_CONTENT_HistoryAverFuelCons_LIST_ID				(92)
#define EEP_CONTENT_HistoryAverFuelCons_ITEM_LEN		(6)
#define EEP_CONTENT_HistoryAverFuelCons_IN_BLOCK_NUM 	(3)
#define EEP_CONTENT_HistoryAverFuelCons_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_HistoryAverFuelCons_ITEM_BYTE_LEN		6
#define EEP_CONTENT_HistoryAverFuelCons_CONST_VARIBLE	(0u)
#define EEP_CONTENT_FuelResistance_LIST_ID				(93)
#define EEP_CONTENT_FuelResistance_ITEM_LEN		(1)
#define EEP_CONTENT_FuelResistance_IN_BLOCK_NUM 	(3)
#define EEP_CONTENT_FuelResistance_ITEM_TYPE		EEP_ITEM_TYPE_32
#define EEP_CONTENT_FuelResistance_ITEM_BYTE_LEN		4
#define EEP_CONTENT_FuelResistance_CONST_VARIBLE	(0u)
#define EEP_CONTENT_dtc_example_LIST_ID				(94)
#define EEP_CONTENT_dtc_example_ITEM_LEN		(10)
#define EEP_CONTENT_dtc_example_IN_BLOCK_NUM 	(4)
#define EEP_CONTENT_dtc_example_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_dtc_example_ITEM_BYTE_LEN		10
#define EEP_CONTENT_dtc_example_CONST_VARIBLE	(0u)
#define EEP_CONTENT_TotalOdo_LIST_ID				(95)
#define EEP_CONTENT_TotalOdo_ITEM_LEN		(1)
#define EEP_CONTENT_TotalOdo_IN_BLOCK_NUM 	(5)
#define EEP_CONTENT_TotalOdo_ITEM_TYPE		EEP_ITEM_TYPE_32
#define EEP_CONTENT_TotalOdo_ITEM_BYTE_LEN		4
#define EEP_CONTENT_TotalOdo_CONST_VARIBLE	(0u)
#define EEP_CONTENT_TotalTime_LIST_ID				(96)
#define EEP_CONTENT_TotalTime_ITEM_LEN		(1)
#define EEP_CONTENT_TotalTime_IN_BLOCK_NUM 	(5)
#define EEP_CONTENT_TotalTime_ITEM_TYPE		EEP_ITEM_TYPE_32
#define EEP_CONTENT_TotalTime_ITEM_BYTE_LEN		4
#define EEP_CONTENT_TotalTime_CONST_VARIBLE	(0u)
#define EEP_CONTENT_TravelTime_LIST_ID				(97)
#define EEP_CONTENT_TravelTime_ITEM_LEN		(1)
#define EEP_CONTENT_TravelTime_IN_BLOCK_NUM 	(5)
#define EEP_CONTENT_TravelTime_ITEM_TYPE		EEP_ITEM_TYPE_32
#define EEP_CONTENT_TravelTime_ITEM_BYTE_LEN		4
#define EEP_CONTENT_TravelTime_CONST_VARIBLE	(0u)
#define EEP_CONTENT_TravelOdo_LIST_ID				(98)
#define EEP_CONTENT_TravelOdo_ITEM_LEN		(1)
#define EEP_CONTENT_TravelOdo_IN_BLOCK_NUM 	(5)
#define EEP_CONTENT_TravelOdo_ITEM_TYPE		EEP_ITEM_TYPE_32
#define EEP_CONTENT_TravelOdo_ITEM_BYTE_LEN		4
#define EEP_CONTENT_TravelOdo_CONST_VARIBLE	(0u)
#define EEP_CONTENT_TripAMeter_LIST_ID				(99)
#define EEP_CONTENT_TripAMeter_ITEM_LEN		(1)
#define EEP_CONTENT_TripAMeter_IN_BLOCK_NUM 	(5)
#define EEP_CONTENT_TripAMeter_ITEM_TYPE		EEP_ITEM_TYPE_32
#define EEP_CONTENT_TripAMeter_ITEM_BYTE_LEN		4
#define EEP_CONTENT_TripAMeter_CONST_VARIBLE	(0u)
#define EEP_CONTENT_TripATime_LIST_ID				(100)
#define EEP_CONTENT_TripATime_ITEM_LEN		(1)
#define EEP_CONTENT_TripATime_IN_BLOCK_NUM 	(5)
#define EEP_CONTENT_TripATime_ITEM_TYPE		EEP_ITEM_TYPE_32
#define EEP_CONTENT_TripATime_ITEM_BYTE_LEN		4
#define EEP_CONTENT_TripATime_CONST_VARIBLE	(0u)
#define EEP_CONTENT_TripBMeter_LIST_ID				(101)
#define EEP_CONTENT_TripBMeter_ITEM_LEN		(1)
#define EEP_CONTENT_TripBMeter_IN_BLOCK_NUM 	(5)
#define EEP_CONTENT_TripBMeter_ITEM_TYPE		EEP_ITEM_TYPE_32
#define EEP_CONTENT_TripBMeter_ITEM_BYTE_LEN		4
#define EEP_CONTENT_TripBMeter_CONST_VARIBLE	(0u)
#define EEP_CONTENT_TripBTime_LIST_ID				(102)
#define EEP_CONTENT_TripBTime_ITEM_LEN		(1)
#define EEP_CONTENT_TripBTime_IN_BLOCK_NUM 	(5)
#define EEP_CONTENT_TripBTime_ITEM_TYPE		EEP_ITEM_TYPE_32
#define EEP_CONTENT_TripBTime_ITEM_BYTE_LEN		4
#define EEP_CONTENT_TripBTime_CONST_VARIBLE	(0u)
#define EEP_CONTENT_CruiseDistance_LIST_ID				(103)
#define EEP_CONTENT_CruiseDistance_ITEM_LEN		(1)
#define EEP_CONTENT_CruiseDistance_IN_BLOCK_NUM 	(5)
#define EEP_CONTENT_CruiseDistance_ITEM_TYPE		EEP_ITEM_TYPE_32
#define EEP_CONTENT_CruiseDistance_ITEM_BYTE_LEN		4
#define EEP_CONTENT_CruiseDistance_CONST_VARIBLE	(0u)
#define EEP_CONTENT_VipSwdlShareMem_LIST_ID				(104)
#define EEP_CONTENT_VipSwdlShareMem_ITEM_LEN		(36)
#define EEP_CONTENT_VipSwdlShareMem_IN_BLOCK_NUM 	(6)
#define EEP_CONTENT_VipSwdlShareMem_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_VipSwdlShareMem_ITEM_BYTE_LEN		36
#define EEP_CONTENT_VipSwdlShareMem_CONST_VARIBLE	(0u)
#define EEP_CONTENT_TotalImageSFailCnt_LIST_ID				(105)
#define EEP_CONTENT_TotalImageSFailCnt_ITEM_LEN		(1)
#define EEP_CONTENT_TotalImageSFailCnt_IN_BLOCK_NUM 	(6)
#define EEP_CONTENT_TotalImageSFailCnt_ITEM_TYPE		EEP_ITEM_TYPE_32
#define EEP_CONTENT_TotalImageSFailCnt_ITEM_BYTE_LEN		4
#define EEP_CONTENT_TotalImageSFailCnt_CONST_VARIBLE	(0u)
#define EEP_CONTENT_trace_phy_en_LIST_ID				(106)
#define EEP_CONTENT_trace_phy_en_ITEM_LEN		(1)
#define EEP_CONTENT_trace_phy_en_IN_BLOCK_NUM 	(6)
#define EEP_CONTENT_trace_phy_en_ITEM_TYPE		EEP_ITEM_TYPE_16
#define EEP_CONTENT_trace_phy_en_ITEM_BYTE_LEN		2
#define EEP_CONTENT_trace_phy_en_CONST_VARIBLE	(1u)
#define EEP_CONTENT_trace_phy_LIST_ID				(107)
#define EEP_CONTENT_trace_phy_ITEM_LEN		(1)
#define EEP_CONTENT_trace_phy_IN_BLOCK_NUM 	(6)
#define EEP_CONTENT_trace_phy_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_trace_phy_ITEM_BYTE_LEN		1
#define EEP_CONTENT_trace_phy_CONST_VARIBLE	(1u)
#define EEP_CONTENT_VipLogModLvl_LIST_ID				(108)
#define EEP_CONTENT_VipLogModLvl_ITEM_LEN		(32)
#define EEP_CONTENT_VipLogModLvl_IN_BLOCK_NUM 	(6)
#define EEP_CONTENT_VipLogModLvl_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_VipLogModLvl_ITEM_BYTE_LEN		32
#define EEP_CONTENT_VipLogModLvl_CONST_VARIBLE	(0u)
#define EEP_CONTENT_TraceModuleLvlCrc_LIST_ID				(109)
#define EEP_CONTENT_TraceModuleLvlCrc_ITEM_LEN		(1)
#define EEP_CONTENT_TraceModuleLvlCrc_IN_BLOCK_NUM 	(6)
#define EEP_CONTENT_TraceModuleLvlCrc_ITEM_TYPE		EEP_ITEM_TYPE_16
#define EEP_CONTENT_TraceModuleLvlCrc_ITEM_BYTE_LEN		2
#define EEP_CONTENT_TraceModuleLvlCrc_CONST_VARIBLE	(0u)
#define EEP_CONTENT_TotalReset_Cnt_LIST_ID				(110)
#define EEP_CONTENT_TotalReset_Cnt_ITEM_LEN		(1)
#define EEP_CONTENT_TotalReset_Cnt_IN_BLOCK_NUM 	(7)
#define EEP_CONTENT_TotalReset_Cnt_ITEM_TYPE		EEP_ITEM_TYPE_32
#define EEP_CONTENT_TotalReset_Cnt_ITEM_BYTE_LEN		4
#define EEP_CONTENT_TotalReset_Cnt_CONST_VARIBLE	(0u)
#define EEP_CONTENT_DspPOR_cnt_LIST_ID				(111)
#define EEP_CONTENT_DspPOR_cnt_ITEM_LEN		(1)
#define EEP_CONTENT_DspPOR_cnt_IN_BLOCK_NUM 	(7)
#define EEP_CONTENT_DspPOR_cnt_ITEM_TYPE		EEP_ITEM_TYPE_16
#define EEP_CONTENT_DspPOR_cnt_ITEM_BYTE_LEN		2
#define EEP_CONTENT_DspPOR_cnt_CONST_VARIBLE	(0u)
#define EEP_CONTENT_AudioCmdExecMaxTm_LIST_ID				(112)
#define EEP_CONTENT_AudioCmdExecMaxTm_ITEM_LEN		(1)
#define EEP_CONTENT_AudioCmdExecMaxTm_IN_BLOCK_NUM 	(7)
#define EEP_CONTENT_AudioCmdExecMaxTm_ITEM_TYPE		EEP_ITEM_TYPE_16
#define EEP_CONTENT_AudioCmdExecMaxTm_ITEM_BYTE_LEN		2
#define EEP_CONTENT_AudioCmdExecMaxTm_CONST_VARIBLE	(0u)
#define EEP_CONTENT_RadioCmdExecMaxTm_LIST_ID				(113)
#define EEP_CONTENT_RadioCmdExecMaxTm_ITEM_LEN		(1)
#define EEP_CONTENT_RadioCmdExecMaxTm_IN_BLOCK_NUM 	(7)
#define EEP_CONTENT_RadioCmdExecMaxTm_ITEM_TYPE		EEP_ITEM_TYPE_16
#define EEP_CONTENT_RadioCmdExecMaxTm_ITEM_BYTE_LEN		2
#define EEP_CONTENT_RadioCmdExecMaxTm_CONST_VARIBLE	(0u)
#define EEP_CONTENT_CpuDisIntMaxNest_LIST_ID				(114)
#define EEP_CONTENT_CpuDisIntMaxNest_ITEM_LEN		(1)
#define EEP_CONTENT_CpuDisIntMaxNest_IN_BLOCK_NUM 	(7)
#define EEP_CONTENT_CpuDisIntMaxNest_ITEM_TYPE		EEP_ITEM_TYPE_16
#define EEP_CONTENT_CpuDisIntMaxNest_ITEM_BYTE_LEN		2
#define EEP_CONTENT_CpuDisIntMaxNest_CONST_VARIBLE	(0u)
#define EEP_CONTENT_CpuDisIntMaxTm_LIST_ID				(115)
#define EEP_CONTENT_CpuDisIntMaxTm_ITEM_LEN		(1)
#define EEP_CONTENT_CpuDisIntMaxTm_IN_BLOCK_NUM 	(7)
#define EEP_CONTENT_CpuDisIntMaxTm_ITEM_TYPE		EEP_ITEM_TYPE_16
#define EEP_CONTENT_CpuDisIntMaxTm_ITEM_BYTE_LEN		2
#define EEP_CONTENT_CpuDisIntMaxTm_CONST_VARIBLE	(0u)
#define EEP_CONTENT_CpuDisIntTooLongCnt_LIST_ID				(116)
#define EEP_CONTENT_CpuDisIntTooLongCnt_ITEM_LEN		(1)
#define EEP_CONTENT_CpuDisIntTooLongCnt_IN_BLOCK_NUM 	(7)
#define EEP_CONTENT_CpuDisIntTooLongCnt_ITEM_TYPE		EEP_ITEM_TYPE_16
#define EEP_CONTENT_CpuDisIntTooLongCnt_ITEM_BYTE_LEN		2
#define EEP_CONTENT_CpuDisIntTooLongCnt_CONST_VARIBLE	(0u)
#define EEP_CONTENT_SocResetReason_LIST_ID				(117)
#define EEP_CONTENT_SocResetReason_ITEM_LEN		(32)
#define EEP_CONTENT_SocResetReason_IN_BLOCK_NUM 	(7)
#define EEP_CONTENT_SocResetReason_ITEM_TYPE		EEP_ITEM_TYPE_8
#define EEP_CONTENT_SocResetReason_ITEM_BYTE_LEN		32
#define EEP_CONTENT_SocResetReason_CONST_VARIBLE	(0u)
#define	EEP_CONTENT_LIST_NUMBER_MAX	(118)




#define EEP_CONT_LIST_FLAG_ARRAY_NUM ((EEP_CONTENT_LIST_NUMBER_MAX+31)/32)

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/



typedef struct
{
	uint16_t id;
	uint16_t len;
	uint8_t *eep;
}EEP_ITEM_LIST_INFO;

typedef struct
{
	/* >>>>>>>>>>>>>>>>>> manufacture start >>>>>>>>>>>>>>>>>>>>>>>>>>> */
	uint32_t manufacture_write_count;
	uint8_t   Manuf_Visteon_Part_Number[17];
	uint8_t   Manuf_EquippedPCB_VPN_Sub[17];
	uint8_t   Manuf_EquippedPCB_VPN_Main[17];
	uint8_t   Manuf_Product_Serial_Number[5];
	uint8_t   Manuf_EquippedPCB_Serial_Number_Sub[5];
	uint8_t   Manuf_EquippedPCB_Serial_Number_Main[5];
	uint8_t   NVM_Revision;
	uint8_t   Manuf_SMD_Date1[3];
	uint8_t   Manuf_SMD_Date2[3];
	uint8_t   Manuf_Assembly_Date[3];
	uint8_t   Traceability_Data[9];
	uint8_t   VehicleManufacturerSparePartNumber[10];
	uint8_t   VehicleManufacturerSparePartNumber_Assembly[10];
	uint8_t   Operational_Reference[10];
	uint8_t   Supplier_Number[4];
	uint8_t   ECU_Serial_Number[20];
	uint8_t   Manufacturing_Identification_Code;
	uint8_t   VDIAG;
	uint8_t   Config_EQ1;
	uint8_t   Vehicle_Type;
	uint8_t   UUID[16];
	uint8_t   NAVI_ID[16];
	uint8_t   DA_ID[16];
	uint8_t   DAMainBoardHwVer[16];
	uint8_t   SW_Version_Date_Format[18];
	uint8_t   SW_Edition_Version[18];
	uint8_t   VehicleManufacturerSparePartNumber_Nissan[10];
	uint8_t   VisteonProductPartNumber[17];
	uint8_t   EquippedPCBVisteonPartNumMainBorad[17];
	uint8_t   EquippedPCBVisteonPartNumSubBorad[17];
	uint8_t   DAUniqueID[16];
	uint8_t   ProductSerialNum[5];
	uint8_t   ProductSerialNumMainBoard[5];
	uint8_t   ProductSerialNumSubBoard[5];
	uint8_t   SMDManufacturingDate[3];
	uint8_t   AssemblyManufacturingDate[3];
	uint8_t   TraceabilityBytes[3];
	uint8_t   SMDPlantNum[3];
	uint8_t   AssemblyPlantNum[3];
	uint8_t   DEM_EVENT_CAN_COM_DATA[10];
	uint8_t   DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA[10];
	uint8_t   DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA[10];
	uint8_t   DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA[10];
	uint8_t   DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA[10];
	uint8_t   DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA[10];
	uint8_t   DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA[10];
	uint8_t   DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA[10];
	uint8_t   DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA[10];
	uint8_t   DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA[10];
	uint8_t   DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA[10];
	uint8_t   DEM_EVENT_HVAC_PANEL_CONNECTION_DATA[10];
	/* eeprom manufacture id size = 474 */ 
	uint8_t manufacture_reserved_474[34];
	uint32_t manufacture_crc;	/* manufacture crc32 checksum */
	/*	eeprom manufacture total size = 512 */ 
	/* >>>>>>>>>>>>>>>>>> manufacture end >>>>>>>>>>>>>>>>>>>>>>>>>>> */

	/* >>>>>>>>>>>>>>>>>> development_nvm start >>>>>>>>>>>>>>>>>>>>>>>>>>> */
	uint32_t development_nvm_write_count;
	uint16_t   TachoUpValue[2];
	uint16_t   TachoLowValue[2];
	uint16_t   TachoLimitValue[2];
	uint16_t   TachoUDeltaLimit[2];
	uint16_t   TachoLDeltaLimit[2];
	uint16_t   TachoUpChangeValue[2];
	uint16_t   TachoLowChangeValue[2];
	uint16_t   TachoRun[2];
	uint16_t   Speed_Rm_1;
	uint16_t   Speed_Rm_2;
	uint16_t   Speed_Rc;
	uint16_t   FuelTelltaleOn_R;
	uint16_t   FuelTelltaleOff_R;
	uint16_t   FuelWarningOnPoint_R;
	uint16_t   Fuel_R_Open;
	uint16_t   Fuel_R_Short;
	uint8_t   PkbWarningJudgeV1;
	uint8_t   PkbWarningJudgeV2;
	uint16_t   AudioEepTest;
	uint8_t   DspKeyCodeOnOff;
	uint8_t reserved_57_DspKeyCode[3];
	uint32_t   DspKeyCode;
	uint16_t   BatState_ChgDly;
	uint16_t   BatVolVeryHigh_Hysteresis_H;
	uint16_t   BatVolVeryHigh_Hysteresis_L;
	uint16_t   BatVolHigh_Hysteresis_H;
	uint16_t   BatVolHigh_Hysteresis_L;
	uint16_t   BatVolLow_Hysteresis_H;
	uint16_t   BatVolLow_Hysteresis_L;
	uint16_t   BatVolVeryLow_Hysteresis_H;
	uint16_t   BatVolVeryLow_Hysteresis_L;
	uint8_t   TempState_ChgDly;
	uint8_t   TempDegC_Low_Hysteresis_L;
	uint8_t   TempDegC_Low_Hysteresis_H;
	uint8_t   TempDegC_High_Hysteresis_L;
	uint8_t   TempDegC_High_Hysteresis_H;
	uint8_t reserved_87_AmpHighTempProction[1];
	uint32_t   AmpHighTempProction[5];
	uint16_t   CanNm_DA_S1_Delay_ms;
	/* eeprom development_nvm id size = 110 */ 
	uint8_t development_nvm_reserved_110[910];
	uint32_t development_nvm_crc;	/* development_nvm crc32 checksum */
	/*	eeprom development_nvm total size = 1024 */ 
	/* >>>>>>>>>>>>>>>>>> development_nvm end >>>>>>>>>>>>>>>>>>>>>>>>>>> */

	/* >>>>>>>>>>>>>>>>>> OemSetting start >>>>>>>>>>>>>>>>>>>>>>>>>>> */
	uint32_t OemSetting_write_count;
	uint8_t   Vin[17];
	/* eeprom OemSetting id size = 21 */ 
	uint8_t OemSetting_reserved_21[231];
	uint32_t OemSetting_crc;	/* OemSetting crc32 checksum */
	/*	eeprom OemSetting total size = 256 */ 
	/* >>>>>>>>>>>>>>>>>> OemSetting end >>>>>>>>>>>>>>>>>>>>>>>>>>> */

	/* >>>>>>>>>>>>>>>>>> user_setting start >>>>>>>>>>>>>>>>>>>>>>>>>>> */
	uint32_t user_setting_write_count;
	uint8_t   AutoSyncTimeWithGps;
	uint8_t   ScreenBackLightValOnDay;
	uint8_t   ScreenBackLightValOnNight;
	uint8_t   HistoryAverFuelCons[6];
	uint8_t reserved_13_FuelResistance[3];
	uint32_t   FuelResistance;
	/* eeprom user_setting id size = 20 */ 
	uint8_t user_setting_reserved_20[232];
	uint32_t user_setting_crc;	/* user_setting crc32 checksum */
	/*	eeprom user_setting total size = 256 */ 
	/* >>>>>>>>>>>>>>>>>> user_setting end >>>>>>>>>>>>>>>>>>>>>>>>>>> */

	/* >>>>>>>>>>>>>>>>>> DTC_INFO start >>>>>>>>>>>>>>>>>>>>>>>>>>> */
	uint32_t DTC_INFO_write_count;
	uint8_t   dtc_example[10];
	/* eeprom DTC_INFO id size = 14 */ 
	uint8_t DTC_INFO_reserved_14[238];
	uint32_t DTC_INFO_crc;	/* DTC_INFO crc32 checksum */
	/*	eeprom DTC_INFO total size = 256 */ 
	/* >>>>>>>>>>>>>>>>>> DTC_INFO end >>>>>>>>>>>>>>>>>>>>>>>>>>> */

	/* >>>>>>>>>>>>>>>>>> OdoInfo start >>>>>>>>>>>>>>>>>>>>>>>>>>> */
	uint32_t OdoInfo_write_count;
	uint32_t   TotalOdo;
	uint32_t   TotalTime;
	uint32_t   TravelTime;
	uint32_t   TravelOdo;
	uint32_t   TripAMeter;
	uint32_t   TripATime;
	uint32_t   TripBMeter;
	uint32_t   TripBTime;
	uint32_t   CruiseDistance;
	/* eeprom OdoInfo id size = 40 */ 
	uint8_t OdoInfo_reserved_40[20];
	uint32_t OdoInfo_crc;	/* OdoInfo crc32 checksum */
	/*	eeprom OdoInfo total size = 64 */ 
	/* >>>>>>>>>>>>>>>>>> OdoInfo end >>>>>>>>>>>>>>>>>>>>>>>>>>> */

	/* >>>>>>>>>>>>>>>>>> share_eep start >>>>>>>>>>>>>>>>>>>>>>>>>>> */
	uint32_t share_eep_write_count;
	uint8_t   VipSwdlShareMem[36];
	uint32_t   TotalImageSFailCnt;
	uint16_t   trace_phy_en;
	uint8_t   trace_phy;
	uint8_t   VipLogModLvl[32];
	uint8_t reserved_79_TraceModuleLvlCrc;
	uint16_t   TraceModuleLvlCrc;
	/* eeprom share_eep id size = 82 */ 
	uint8_t share_eep_reserved_82[42];
	uint32_t share_eep_crc;	/* share_eep crc32 checksum */
	/*	eeprom share_eep total size = 128 */ 
	/* >>>>>>>>>>>>>>>>>> share_eep end >>>>>>>>>>>>>>>>>>>>>>>>>>> */

	/* >>>>>>>>>>>>>>>>>> ResetInfo start >>>>>>>>>>>>>>>>>>>>>>>>>>> */
	uint32_t ResetInfo_write_count;
	uint32_t   TotalReset_Cnt;
	uint16_t   DspPOR_cnt;
	uint16_t   AudioCmdExecMaxTm;
	uint16_t   RadioCmdExecMaxTm;
	uint16_t   CpuDisIntMaxNest;
	uint16_t   CpuDisIntMaxTm;
	uint16_t   CpuDisIntTooLongCnt;
	uint8_t   SocResetReason[32];
	/* eeprom ResetInfo id size = 52 */ 
	uint8_t ResetInfo_reserved_52[72];
	uint32_t ResetInfo_crc;	/* ResetInfo crc32 checksum */
	/*	eeprom ResetInfo total size = 128 */ 
	/* >>>>>>>>>>>>>>>>>> ResetInfo end >>>>>>>>>>>>>>>>>>>>>>>>>>> */

}EEPROM_ST;

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
//extern uint16_t eep_write_block_req;
//extern uint32_t wr_eep_by_list_req[EEP_CONT_LIST_FLAG_ARRAY_NUM];
//extern uint32_t tx_eep_by_list_to_imx_req[EEP_CONT_LIST_FLAG_ARRAY_NUM];
//extern uint32_t tx_eep_by_list_to_imx_cur[EEP_CONT_LIST_FLAG_ARRAY_NUM];
// eeprom ram image 
//extern EEPROM_ST eep_ram_image;
// write eep_ram_image->eep_hw_image, than eep_hw_image->real eeprom 
//extern EEPROM_ST eep_hw_image; 

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if EEP_CONTENT_Manuf_Visteon_Part_Number_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_Manuf_EquippedPCB_VPN_Sub_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_Manuf_EquippedPCB_VPN_Main_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_Manuf_Product_Serial_Number_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Sub_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_Manuf_EquippedPCB_Serial_Number_Main_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_NVM_Revision_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_Manuf_SMD_Date1_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_Manuf_SMD_Date2_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_Manuf_Assembly_Date_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_Traceability_Data_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_VehicleManufacturerSparePartNumber_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_VehicleManufacturerSparePartNumber_Assembly_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_Operational_Reference_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_Supplier_Number_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_ECU_Serial_Number_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_Manufacturing_Identification_Code_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_VDIAG_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_Config_EQ1_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_Vehicle_Type_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_UUID_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_NAVI_ID_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_DA_ID_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_DAMainBoardHwVer_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_SW_Version_Date_Format_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_SW_Edition_Version_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_VehicleManufacturerSparePartNumber_Nissan_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_VisteonProductPartNumber_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_EquippedPCBVisteonPartNumMainBorad_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_EquippedPCBVisteonPartNumSubBorad_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_DAUniqueID_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_ProductSerialNum_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_ProductSerialNumMainBoard_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_ProductSerialNumSubBoard_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_SMDManufacturingDate_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_AssemblyManufacturingDate_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_TraceabilityBytes_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_SMDPlantNum_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_AssemblyPlantNum_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_DEM_EVENT_CAN_COM_DATA_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_DEM_EVENT_FL_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_DEM_EVENT_FR_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_DEM_EVENT_RL_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_GROUND_DATA_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_DEM_EVENT_RR_LOUDSPEAKER_SHORT_BAT_DATA_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_GROUND_DATA_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_DEM_EVENT_GPS_ANTENNA_SHORT_BAT_DATA_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_DEM_EVENT_HVAC_PANEL_CONNECTION_DATA_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_TachoUpValue_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_TachoLowValue_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_TachoLimitValue_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_TachoUDeltaLimit_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_TachoLDeltaLimit_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_TachoUpChangeValue_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_TachoLowChangeValue_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_TachoRun_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_Speed_Rm_1_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_Speed_Rm_2_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_Speed_Rc_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_FuelTelltaleOn_R_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_FuelTelltaleOff_R_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_FuelWarningOnPoint_R_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_Fuel_R_Open_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_Fuel_R_Short_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_PkbWarningJudgeV1_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_PkbWarningJudgeV2_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_AudioEepTest_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_DspKeyCodeOnOff_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_DspKeyCode_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_BatState_ChgDly_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_BatVolVeryHigh_Hysteresis_H_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_BatVolVeryHigh_Hysteresis_L_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_BatVolHigh_Hysteresis_H_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_BatVolHigh_Hysteresis_L_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_BatVolLow_Hysteresis_H_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_BatVolLow_Hysteresis_L_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_BatVolVeryLow_Hysteresis_H_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_BatVolVeryLow_Hysteresis_L_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_TempState_ChgDly_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_TempDegC_Low_Hysteresis_L_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_TempDegC_Low_Hysteresis_H_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_TempDegC_High_Hysteresis_L_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_TempDegC_High_Hysteresis_H_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_AmpHighTempProction_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_CanNm_DA_S1_Delay_ms_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_Vin_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_AutoSyncTimeWithGps_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_ScreenBackLightValOnDay_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_ScreenBackLightValOnNight_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_HistoryAverFuelCons_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_FuelResistance_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_dtc_example_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_TotalOdo_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_TotalTime_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_TravelTime_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_TravelOdo_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_TripAMeter_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_TripATime_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_TripBMeter_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_TripBTime_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_CruiseDistance_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_VipSwdlShareMem_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_TotalImageSFailCnt_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_trace_phy_en_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_trace_phy_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_VipLogModLvl_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_TraceModuleLvlCrc_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_TotalReset_Cnt_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_DspPOR_cnt_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_AudioCmdExecMaxTm_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_RadioCmdExecMaxTm_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_CpuDisIntMaxNest_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_CpuDisIntMaxTm_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_CpuDisIntTooLongCnt_ITEM_LEN > 1
#else 
#endif
#if EEP_CONTENT_SocResetReason_ITEM_LEN > 1
#else 
#endif





/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#ifdef __cplusplus
}
#endif

#endif
/** >>>>>>>> end of file  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/




