#ifndef LOG_CFG_H_H_
#define LOG_CFG_H_H_
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

/*****this file is generate by perl**************************************************************************************** **/
//#include"stdint.h"


#ifdef __cplusplus
extern "C" {
#endif

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#define MOD_ALWAYS_DefaultLevel			2 /*** This module is always enabled ***/
#define MOD_OS_DefaultLevel			0 /*** OS and Server ***/
#define MOD_HW_DefaultLevel			0 /*** Hw -- ***/
#define MOD_CAN_DefaultLevel			0 /*** CAN Module ***/
#define MOD_DIAG_DefaultLevel			0 /*** Diag Module ***/
#define MOD_EEPROM_DefaultLevel			0 /*** Eeprom Module ***/
#define MOD_DEV_IPC_SPI_0_DefaultLevel			1 /*** SPI IPC Kernel ***/
#define MOD_DEV_IPC_IIC_IC_DefaultLevel			1 /*** IIC IPC Kernel ***/
#define MOD_GPS_DefaultLevel			0 /*** GPS Module ***/
#define MOD_AUDIO_DefaultLevel			0 /*** Audio Module ***/
#define MOD_CF_KERNEL_DefaultLevel			0 /*** CF Module Kernel ***/
#define MOD_CF_STUB_DefaultLevel			0 /*** CF Module ***/
#define MOD_CF_PROXY_DefaultLevel			0 /*** CF Moduler ***/
#define MOD_SWDL_DefaultLevel			6 /*** SWDL Module ***/
#define MOD_METER_GAUGES_DefaultLevel			0 /*** Meter Module Gauges ***/
#define MOD_METER_OBC_DefaultLevel			0 /*** Meter Module OBC ***/
#define MOD_METER_WARNING_DefaultLevel			0 /*** Meter Module Warning ***/
#define MOD_METER_ILL_DefaultLevel			0 /*** Meter Module Illumination ***/
#define MOD_METER_TELLTALE_DefaultLevel			0 /*** Meter Module Telltales  ***/
#define MOD_METER_BUZZER_DefaultLevel			0 /*** Meter Module Buzzer ***/
#define MOD_METER_OUTPUT_DefaultLevel			0 /*** Meter Module Output ***/
#define MOD_IPC_DEV_SIG_DefaultLevel			0 /*** IPC Device Signal Module ***/
#define MOD_IC_DEV_SIG_DefaultLevel			0 /*** IC Device Signal Module ***/
#define MOD_METER_SWDL_DefaultLevel			6 /*** Meter SWDL Module ***/
#define MOD_HOST_I2C_DefaultLevel			6 /*** HOST I2C Module ***/
#define MOD_TFT_ADC_DefaultLevel			6 /*** TFT ADC ***/
#define MOD_TOUCH_IIC_DefaultLevel			6 /*** TOUCH IIC  ***/
#define LOG_ERR			0 /*** errors, display always ***/
#define LOG_SHOUT			0 /*** not an error but we should always see it ***/
#define LOG_WARN			1 /*** not errors but may indicate a problem ***/
#define LOG_INFO			2 /*** key informative messages ***/
#define LOG_DEBUG			3 /*** debug messages ***/
#define LOG_PROG			4 /*** progress messages ***/
#define LOG_IO			5 /*** IO to and from devices ***/
#define LOG_RAW			6 /*** raw low-level I/O ***/
#define LOG_LVL_7			7 /*** raw low-level I/O ***/
#define LOG_LVL_8			8 /*** raw low-level I/O ***/
#define LOG_LVL_9			9 /*** raw low-level I/O ***/
#define LOG_LVL_10			10 /*** raw low-level I/O ***/
#define MAIN_SWITCH			LOG_DEBUG /***  ***/
#define TRACE_SWITCH_ALWAYS			LOG_SHOUT /***  ***/
#define TRACE_SWITCH_DEBUG			LOG_DEBUG /***  ***/
#define TRACE_TEST			LOG_INFO /***  ***/
#define CPU_USAGE			LOG_IO /***  ***/
#define CF_PROXY_METHOD_CALl			LOG_DEBUG /***  ***/
#define CF_STUB_METHOD_IMP			LOG_DEBUG /***  ***/
#define CF_PROXY_METHOD_RET			LOG_DEBUG /***  ***/
#define CF_STUB_METHOD_RET			LOG_DEBUG /***  ***/
#define CF_PROXY_SIGNAL_HOOK			LOG_DEBUG /***  ***/
#define IPC_ALWAYS			LOG_SHOUT /***  ***/
#define EEP_ALWAYS			LOG_SHOUT /***  ***/
#define EEP_ERR			LOG_ERR /***  ***/
#define EEP_WRITE_BLOCK			LOG_DEBUG /***  ***/
#define EEP_WRITE_ITEM			LOG_DEBUG /***  ***/
#define EEP_SET_CONTENT			LOG_DEBUG /***  ***/
#define CF_KERNAL_RX_DEVIPC_RAW_DAT			LOG_RAW+2 /***  ***/
#define AUDIO_ALWAYS			0 /***  ***/
#define AUDIO_SHOUT			0 /***  ***/
#define AUDIO_RAW_CMD			LOG_RAW+2 /***  ***/
#define TRACE_COM_PHY_DEFAULT			TRACE_RS232 /*** trace com default is rs232 ***/
#define LOG_USE_EXT_VERSION			0u /*** trace version use extern function or not ***/
#define LOG_GET_EXT_VERSION			VerInfo_GetLogVersion /*** trace version use extern function name ***/
#define LOG_GET_EXT_VERSION_H			 "ver_info.h"  /*** VerInfo_GetLogVersion .h ***/



/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

typedef enum
{
	MOD_ALWAYS, /*** This module is always enabled ***/
	MOD_OS, /*** OS and Server ***/
	MOD_HW, /*** Hw -- ***/
	MOD_CAN, /*** CAN Module ***/
	MOD_DIAG, /*** Diag Module ***/
	MOD_EEPROM, /*** Eeprom Module ***/
	MOD_DEV_IPC_SPI_0, /*** SPI IPC Kernel ***/
	MOD_DEV_IPC_IIC_IC, /*** IIC IPC Kernel ***/
	MOD_GPS, /*** GPS Module ***/
	MOD_AUDIO, /*** Audio Module ***/
	MOD_CF_KERNEL, /*** CF Module Kernel ***/
	MOD_CF_STUB, /*** CF Module ***/
	MOD_CF_PROXY, /*** CF Moduler ***/
	MOD_SWDL, /*** SWDL Module ***/
	MOD_METER_GAUGES, /*** Meter Module Gauges ***/
	MOD_METER_OBC, /*** Meter Module OBC ***/
	MOD_METER_WARNING, /*** Meter Module Warning ***/
	MOD_METER_ILL, /*** Meter Module Illumination ***/
	MOD_METER_TELLTALE, /*** Meter Module Telltales  ***/
	MOD_METER_BUZZER, /*** Meter Module Buzzer ***/
	MOD_METER_OUTPUT, /*** Meter Module Output ***/
	MOD_IPC_DEV_SIG, /*** IPC Device Signal Module ***/
	MOD_IC_DEV_SIG, /*** IC Device Signal Module ***/
	MOD_METER_SWDL, /*** Meter SWDL Module ***/
	MOD_HOST_I2C, /*** HOST I2C Module ***/
	MOD_TFT_ADC, /*** TFT ADC ***/
	MOD_TOUCH_IIC, /*** TOUCH IIC  ***/

	LOG_MODULE_NAME_MAX
}LOG_MODULE_NAME;

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#ifdef __cplusplus
}
#endif

#endif
/** >>>>>>>> end of file  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/





