#ifndef _LOG_HANDLER_H_
#define _LOG_HANDLER_H_

#include"trace_cfg.h"
#include"Trace_Internal.h"

#include "log_cfg.h"
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
// extern module log level declear
extern uint8_t MOD_ALWAYS_Lvl;
extern uint8_t MOD_OS_Lvl;
extern uint8_t MOD_HW_Lvl;
extern uint8_t MOD_CAN_Lvl;
extern uint8_t MOD_DIAG_Lvl;
extern uint8_t MOD_EEPROM_Lvl;
extern uint8_t MOD_DEV_IPC_SPI_0_Lvl;
extern uint8_t MOD_DEV_IPC_IIC_IC_Lvl;
extern uint8_t MOD_GPS_Lvl;
extern uint8_t MOD_AUDIO_Lvl;
extern uint8_t MOD_CF_KERNEL_Lvl;
extern uint8_t MOD_CF_STUB_Lvl;
extern uint8_t MOD_CF_PROXY_Lvl;
extern uint8_t MOD_SWDL_Lvl;
extern uint8_t MOD_METER_GAUGES_Lvl;
extern uint8_t MOD_METER_OBC_Lvl;
extern uint8_t MOD_METER_WARNING_Lvl;
extern uint8_t MOD_METER_ILL_Lvl;
extern uint8_t MOD_METER_TELLTALE_Lvl;
extern uint8_t MOD_METER_BUZZER_Lvl;
extern uint8_t MOD_METER_OUTPUT_Lvl;
extern uint8_t MOD_IPC_DEV_SIG_Lvl;
extern uint8_t MOD_IC_DEV_SIG_Lvl;
extern uint8_t MOD_METER_SWDL_Lvl;
extern uint8_t MOD_HOST_I2C_Lvl;
extern uint8_t MOD_TFT_ADC_Lvl;
extern uint8_t MOD_TOUCH_IIC_Lvl;


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void LogHandle_Init(void);

void LogHandle_RestoreModuleLog(void);
void LogHandle_DisModuleLog(void);
void LogHandle_SetModuleMask(eTR_MODULE_SET_TYPE setType, uint32_t ModuleMask);
#if LOG_EEP_MOD_LVL_AVBL
void LogHandle_RestoreModuleLogLvlFromEep(void);
#else
#define LogHandle_RestoreModuleLogLvlFromEep()	do{}while(0)	
#endif

void LogHandle_SetModuleLogLvl(uint8_t module_id, uint8_t log_lvl);
void LogHandle_OutputAllModLvl(void);

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if LOG_ENABLE && LOG_ENABLE 
#define LogHandle_SetLogLvl_MOD_ALWAYS(lvl)	do{LogHandle_SetModuleLogLvl(MOD_ALWAYS,lvl);}while(0)
#else //!{LOG_ENABLE && LOG_ENABLE}
#define LogHandle_SetLogLvl_MOD_ALWAYS(lvl)	do{(void)(lvl);}while(0)
#endif //{LOG_ENABLE && LOG_ENABLE}

#if LOG_ENABLE && LOG_ENABLE 
#define LogHandle_SetLogLvl_MOD_OS(lvl)	do{LogHandle_SetModuleLogLvl(MOD_OS,lvl);}while(0)
#else //!{LOG_ENABLE && LOG_ENABLE}
#define LogHandle_SetLogLvl_MOD_OS(lvl)	do{(void)(lvl);}while(0)
#endif //{LOG_ENABLE && LOG_ENABLE}

#if LOG_ENABLE && LOG_ENABLE 
#define LogHandle_SetLogLvl_MOD_HW(lvl)	do{LogHandle_SetModuleLogLvl(MOD_HW,lvl);}while(0)
#else //!{LOG_ENABLE && LOG_ENABLE}
#define LogHandle_SetLogLvl_MOD_HW(lvl)	do{(void)(lvl);}while(0)
#endif //{LOG_ENABLE && LOG_ENABLE}

#if LOG_ENABLE && LOG_ENABLE 
#define LogHandle_SetLogLvl_MOD_CAN(lvl)	do{LogHandle_SetModuleLogLvl(MOD_CAN,lvl);}while(0)
#else //!{LOG_ENABLE && LOG_ENABLE}
#define LogHandle_SetLogLvl_MOD_CAN(lvl)	do{(void)(lvl);}while(0)
#endif //{LOG_ENABLE && LOG_ENABLE}

#if LOG_ENABLE && LOG_ENABLE 
#define LogHandle_SetLogLvl_MOD_DIAG(lvl)	do{LogHandle_SetModuleLogLvl(MOD_DIAG,lvl);}while(0)
#else //!{LOG_ENABLE && LOG_ENABLE}
#define LogHandle_SetLogLvl_MOD_DIAG(lvl)	do{(void)(lvl);}while(0)
#endif //{LOG_ENABLE && LOG_ENABLE}

#if LOG_ENABLE && LOG_ENABLE 
#define LogHandle_SetLogLvl_MOD_EEPROM(lvl)	do{LogHandle_SetModuleLogLvl(MOD_EEPROM,lvl);}while(0)
#else //!{LOG_ENABLE && LOG_ENABLE}
#define LogHandle_SetLogLvl_MOD_EEPROM(lvl)	do{(void)(lvl);}while(0)
#endif //{LOG_ENABLE && LOG_ENABLE}

#if LOG_ENABLE && LOG_ENABLE 
#define LogHandle_SetLogLvl_MOD_DEV_IPC_SPI_0(lvl)	do{LogHandle_SetModuleLogLvl(MOD_DEV_IPC_SPI_0,lvl);}while(0)
#else //!{LOG_ENABLE && LOG_ENABLE}
#define LogHandle_SetLogLvl_MOD_DEV_IPC_SPI_0(lvl)	do{(void)(lvl);}while(0)
#endif //{LOG_ENABLE && LOG_ENABLE}

#if LOG_ENABLE && LOG_ENABLE 
#define LogHandle_SetLogLvl_MOD_DEV_IPC_IIC_IC(lvl)	do{LogHandle_SetModuleLogLvl(MOD_DEV_IPC_IIC_IC,lvl);}while(0)
#else //!{LOG_ENABLE && LOG_ENABLE}
#define LogHandle_SetLogLvl_MOD_DEV_IPC_IIC_IC(lvl)	do{(void)(lvl);}while(0)
#endif //{LOG_ENABLE && LOG_ENABLE}

#if LOG_ENABLE && LOG_ENABLE 
#define LogHandle_SetLogLvl_MOD_GPS(lvl)	do{LogHandle_SetModuleLogLvl(MOD_GPS,lvl);}while(0)
#else //!{LOG_ENABLE && LOG_ENABLE}
#define LogHandle_SetLogLvl_MOD_GPS(lvl)	do{(void)(lvl);}while(0)
#endif //{LOG_ENABLE && LOG_ENABLE}

#if LOG_ENABLE && LOG_ENABLE 
#define LogHandle_SetLogLvl_MOD_AUDIO(lvl)	do{LogHandle_SetModuleLogLvl(MOD_AUDIO,lvl);}while(0)
#else //!{LOG_ENABLE && LOG_ENABLE}
#define LogHandle_SetLogLvl_MOD_AUDIO(lvl)	do{(void)(lvl);}while(0)
#endif //{LOG_ENABLE && LOG_ENABLE}

#if LOG_ENABLE && LOG_ENABLE 
#define LogHandle_SetLogLvl_MOD_CF_KERNEL(lvl)	do{LogHandle_SetModuleLogLvl(MOD_CF_KERNEL,lvl);}while(0)
#else //!{LOG_ENABLE && LOG_ENABLE}
#define LogHandle_SetLogLvl_MOD_CF_KERNEL(lvl)	do{(void)(lvl);}while(0)
#endif //{LOG_ENABLE && LOG_ENABLE}

#if LOG_ENABLE && LOG_ENABLE 
#define LogHandle_SetLogLvl_MOD_CF_STUB(lvl)	do{LogHandle_SetModuleLogLvl(MOD_CF_STUB,lvl);}while(0)
#else //!{LOG_ENABLE && LOG_ENABLE}
#define LogHandle_SetLogLvl_MOD_CF_STUB(lvl)	do{(void)(lvl);}while(0)
#endif //{LOG_ENABLE && LOG_ENABLE}

#if LOG_ENABLE && LOG_ENABLE 
#define LogHandle_SetLogLvl_MOD_CF_PROXY(lvl)	do{LogHandle_SetModuleLogLvl(MOD_CF_PROXY,lvl);}while(0)
#else //!{LOG_ENABLE && LOG_ENABLE}
#define LogHandle_SetLogLvl_MOD_CF_PROXY(lvl)	do{(void)(lvl);}while(0)
#endif //{LOG_ENABLE && LOG_ENABLE}

#if LOG_ENABLE && LOG_ENABLE 
#define LogHandle_SetLogLvl_MOD_SWDL(lvl)	do{LogHandle_SetModuleLogLvl(MOD_SWDL,lvl);}while(0)
#else //!{LOG_ENABLE && LOG_ENABLE}
#define LogHandle_SetLogLvl_MOD_SWDL(lvl)	do{(void)(lvl);}while(0)
#endif //{LOG_ENABLE && LOG_ENABLE}

#if LOG_ENABLE && LOG_ENABLE 
#define LogHandle_SetLogLvl_MOD_METER_GAUGES(lvl)	do{LogHandle_SetModuleLogLvl(MOD_METER_GAUGES,lvl);}while(0)
#else //!{LOG_ENABLE && LOG_ENABLE}
#define LogHandle_SetLogLvl_MOD_METER_GAUGES(lvl)	do{(void)(lvl);}while(0)
#endif //{LOG_ENABLE && LOG_ENABLE}

#if LOG_ENABLE && LOG_ENABLE 
#define LogHandle_SetLogLvl_MOD_METER_OBC(lvl)	do{LogHandle_SetModuleLogLvl(MOD_METER_OBC,lvl);}while(0)
#else //!{LOG_ENABLE && LOG_ENABLE}
#define LogHandle_SetLogLvl_MOD_METER_OBC(lvl)	do{(void)(lvl);}while(0)
#endif //{LOG_ENABLE && LOG_ENABLE}

#if LOG_ENABLE && LOG_ENABLE 
#define LogHandle_SetLogLvl_MOD_METER_WARNING(lvl)	do{LogHandle_SetModuleLogLvl(MOD_METER_WARNING,lvl);}while(0)
#else //!{LOG_ENABLE && LOG_ENABLE}
#define LogHandle_SetLogLvl_MOD_METER_WARNING(lvl)	do{(void)(lvl);}while(0)
#endif //{LOG_ENABLE && LOG_ENABLE}

#if LOG_ENABLE && LOG_ENABLE 
#define LogHandle_SetLogLvl_MOD_METER_ILL(lvl)	do{LogHandle_SetModuleLogLvl(MOD_METER_ILL,lvl);}while(0)
#else //!{LOG_ENABLE && LOG_ENABLE}
#define LogHandle_SetLogLvl_MOD_METER_ILL(lvl)	do{(void)(lvl);}while(0)
#endif //{LOG_ENABLE && LOG_ENABLE}

#if LOG_ENABLE && LOG_ENABLE 
#define LogHandle_SetLogLvl_MOD_METER_TELLTALE(lvl)	do{LogHandle_SetModuleLogLvl(MOD_METER_TELLTALE,lvl);}while(0)
#else //!{LOG_ENABLE && LOG_ENABLE}
#define LogHandle_SetLogLvl_MOD_METER_TELLTALE(lvl)	do{(void)(lvl);}while(0)
#endif //{LOG_ENABLE && LOG_ENABLE}

#if LOG_ENABLE && LOG_ENABLE 
#define LogHandle_SetLogLvl_MOD_METER_BUZZER(lvl)	do{LogHandle_SetModuleLogLvl(MOD_METER_BUZZER,lvl);}while(0)
#else //!{LOG_ENABLE && LOG_ENABLE}
#define LogHandle_SetLogLvl_MOD_METER_BUZZER(lvl)	do{(void)(lvl);}while(0)
#endif //{LOG_ENABLE && LOG_ENABLE}

#if LOG_ENABLE && LOG_ENABLE 
#define LogHandle_SetLogLvl_MOD_METER_OUTPUT(lvl)	do{LogHandle_SetModuleLogLvl(MOD_METER_OUTPUT,lvl);}while(0)
#else //!{LOG_ENABLE && LOG_ENABLE}
#define LogHandle_SetLogLvl_MOD_METER_OUTPUT(lvl)	do{(void)(lvl);}while(0)
#endif //{LOG_ENABLE && LOG_ENABLE}

#if LOG_ENABLE && LOG_ENABLE 
#define LogHandle_SetLogLvl_MOD_IPC_DEV_SIG(lvl)	do{LogHandle_SetModuleLogLvl(MOD_IPC_DEV_SIG,lvl);}while(0)
#else //!{LOG_ENABLE && LOG_ENABLE}
#define LogHandle_SetLogLvl_MOD_IPC_DEV_SIG(lvl)	do{(void)(lvl);}while(0)
#endif //{LOG_ENABLE && LOG_ENABLE}

#if LOG_ENABLE && LOG_ENABLE 
#define LogHandle_SetLogLvl_MOD_IC_DEV_SIG(lvl)	do{LogHandle_SetModuleLogLvl(MOD_IC_DEV_SIG,lvl);}while(0)
#else //!{LOG_ENABLE && LOG_ENABLE}
#define LogHandle_SetLogLvl_MOD_IC_DEV_SIG(lvl)	do{(void)(lvl);}while(0)
#endif //{LOG_ENABLE && LOG_ENABLE}

#if LOG_ENABLE && LOG_ENABLE 
#define LogHandle_SetLogLvl_MOD_METER_SWDL(lvl)	do{LogHandle_SetModuleLogLvl(MOD_METER_SWDL,lvl);}while(0)
#else //!{LOG_ENABLE && LOG_ENABLE}
#define LogHandle_SetLogLvl_MOD_METER_SWDL(lvl)	do{(void)(lvl);}while(0)
#endif //{LOG_ENABLE && LOG_ENABLE}

#if LOG_ENABLE && LOG_ENABLE 
#define LogHandle_SetLogLvl_MOD_HOST_I2C(lvl)	do{LogHandle_SetModuleLogLvl(MOD_HOST_I2C,lvl);}while(0)
#else //!{LOG_ENABLE && LOG_ENABLE}
#define LogHandle_SetLogLvl_MOD_HOST_I2C(lvl)	do{(void)(lvl);}while(0)
#endif //{LOG_ENABLE && LOG_ENABLE}

#if LOG_ENABLE && LOG_ENABLE 
#define LogHandle_SetLogLvl_MOD_TFT_ADC(lvl)	do{LogHandle_SetModuleLogLvl(MOD_TFT_ADC,lvl);}while(0)
#else //!{LOG_ENABLE && LOG_ENABLE}
#define LogHandle_SetLogLvl_MOD_TFT_ADC(lvl)	do{(void)(lvl);}while(0)
#endif //{LOG_ENABLE && LOG_ENABLE}

#if LOG_ENABLE && LOG_ENABLE 
#define LogHandle_SetLogLvl_MOD_TOUCH_IIC(lvl)	do{LogHandle_SetModuleLogLvl(MOD_TOUCH_IIC,lvl);}while(0)
#else //!{LOG_ENABLE && LOG_ENABLE}
#define LogHandle_SetLogLvl_MOD_TOUCH_IIC(lvl)	do{(void)(lvl);}while(0)
#endif //{LOG_ENABLE && LOG_ENABLE}


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

/***********************************************************************************************/
/* Function Name   :    SetModuleMask(eTR_MODULE_SET_TYPE setType, uint32_t ModuleMask)          */
/* Description     :    This function sets the Module-mask.                                    */
/***********************************************************************************************/
/**  @brief  This function sets the Module-mask.
 *   @param  setType The type of module-mask set-operation
 *   @param  ModuleMask The module-mask to set
 */
//void	SetModuleMask    (eTR_MODULE_SET_TYPE setType, uint32_t ModuleMask);

//void	SetModuleInitialMask(void);


/***********************************************************************************************/
/* Function Name   :    Trace_GetConfiguredModuleMask(void)                                    */
/* Description     :    This function returns the default Module-mask as configured in         */
/*                      Tr_Modules.cfg.                                                        */
/***********************************************************************************************/
/**  @brief  This function returns the default Module-mask as configured in Tr_Modules.cfg.
 */
//uint32_t Trace_GetConfiguredModuleMask(void);


void Trace_GetModuleLvl(uint8 channel,uint8 module_id);
/***********************************************************************************************/
/* Function Name   :    Trace_Text(uint16_t TraceID_u16)                                         */
/* Description     :    Call this function to log a simple text message.                       */
/***********************************************************************************************/
/**  @brief  Call this function to log a simple text message.
 *   @param  TraceID_u16 The Trace-ID for the text to trace. This ID is generated automatically by the compilation process.
 */
   void     Trace_Text        (uint16_t TraceID_u16);



/***********************************************************************************************/
/* Function Name   :  Trace_Value(uint16_t TraceID_u16, uint32_t Val_u32)                          */
/* Description     :  This function may be called when a text message ALONG WITH A 32 bit-VALUE*/
/*                    needs to be logged.                                                      */
/***********************************************************************************************/
/**  @brief  This function may be called when a text message ALONG WITH A 32 bit-VALUE needs to be logged.
 *   @param  TraceID_u16 The Trace-ID for the text to trace. This ID is generated automatically by the compilation process.
 *   @param  Val_u32 The 32-bit value to trace.
 */
   void     Trace_Value       (uint16_t TraceID_u16, uint32_t Val_u32);




/************************************************************************************************/
/* Function Name    :  Trace_Value2(uint16_t TraceID_u16,   uint16_t Val1_u16,   uint16_t Val2_u16)   */
/* Description      :  Call this function to log a text message ALONG WITH 2 16 bit-VALUES      */
/************************************************************************************************/
/**  @brief  Call this function to log a text message ALONG WITH 2 16 bit-VALUES
 *   @param  TraceID_u16 The Trace-ID for the text to trace. This ID is generated automatically by the compilation process.
 *   @param  Val1_u16 The first 16-bit value to trace.
 *   @param  Val2_u16 The second 16-bit value to trace.
 */
   void     Trace_Value2      (uint16_t TraceID_u16, uint16_t Val1_u16, uint16_t Val2_u16);





/***********************************************************************************************/
/* Function Name     :  Trace_Value4( uint16_t TraceID_u16, uint8_t Val1_u8,                       */
/*                                   uint8_t Val2_u8,       uint8_t Val3_u8,      uint8_t Val4_u8)   */
/* Description       :  Call this function to log a text message ALONG WITH 4 8 bit-VALUES     */
/***********************************************************************************************/
/**  @brief  Call this function to log a text message ALONG WITH 4 8 bit-VALUES
 *   @param  TraceID_u16 The Trace-ID for the text to trace. This ID is generated automatically by the compilation process.
 *   @param  Val1_u8 The first 8-bit value to trace.
 *   @param  Val2_u8 The second 8-bit value to trace.
 *   @param  Val3_u8 The third 8-bit value to trace.
 *   @param  Val4_u8 The fourth 8-bit value to trace.
 */
   void     Trace_Value4      (uint16_t TraceID_u16, uint8_t Val1_u8, uint8_t Val2_u8, uint8_t Val3_u8, uint8_t Val4_u8);

/************************************************************************************************/
/* Function Name     :  Trace_Mem( uint16_t TraceID_u16,   uint8_t Length,    char * ptr     )      */
/* Description       :  Call this function to log complete block of memory                      */
/************************************************************************************************/
/**  @brief   Call this function to log complete block of memory
 *   @param  TraceID_u16 The Trace-ID for the text to trace. This ID is generated automatically by the compilation process.
 *   @param  Length Number bytes to log
 *   @param  ptr Pointer to the start-address of of the memory-block to trace.
 */
   void      Trace_Mem         (uint16_t TraceID_u16, uint8_t Length, const uint8_t * dataPointer);



/************************************************************************************************/
/* Function Name   :  Trace_String(uint16_t  TraceID_u16, const char* str)                        */
/* Description     :  Call this function to log information that consists of a format string    */
/*                      and a null terminated string of "printable" characters.                 */
/************************************************************************************************/
/**  @brief  Call this function to log information that consists of a format string and a null terminated string of "printable" characters.
 *   @param  TraceID_u16 The Trace-ID for the text to trace. This ID is generated automatically by the compilation process.
 *   @param  str Pointer to the string to trace.
 */
   void      Trace_String      (uint16_t TraceID_u16, const char * str);



/***********************************************************************************************/
/* Function Name   :  printf_(const char* str)                                                 */
/* Description     :  Transmit a string of characters.                                         */
/***********************************************************************************************/
/**  @brief  Transmit a string of characters.
 *   @param  str Pointer to the string to trace.
 */

   void      printf_(const char* str);




/***********************************************************************************************/
/* Function Name   :  printf_1(const char* str, unsigned long p1)                              */
/* Description     :  Transmit a formatted string with a value.                                */
/***********************************************************************************************/
/**  @brief  Transmit a formatted string with a value.
 *   @param  str Pointer to the string to trace.
 *   @param  p1 Pointer to the value to trace.
 */
   void      printf_1(const char* str, unsigned long p1);


   void     Trace_SendUARTExtern(uint8_t * pDataToSend, uint8_t NoOfBytes);




/**
 * @}
 */


#endif /* _TRACE_HANDLER_H_ */


