/*******************_**********************************************************
 *
 * Copyright (c) 
 *
 *****************************************************************************/
 

 /*****************************************************************************
 *  Include Files
 *****************************************************************************/
#include "serv_evt.h"

#include "cpu.h"


/*****************************************************************************
 *  Local Macro Definitions
 *****************************************************************************/



 /*****************************************************************************
 *  Local Type Declarations
 *****************************************************************************/
 
/* event poll								*/
static	SERV_EVENT 	s_eventQueuePool[SERVER_EVENT_POOL_SIZE];

 
 /*****************************************************************************
 *  Global Variable Definitions (Global for the whole system)
 *****************************************************************************/
 
/*****************************************************************************
 *  Local Variable Definitions (Global within this file)
 *****************************************************************************/

 
 /*****************************************************************************
 *  Global Function Declarations
 *****************************************************************************/

/*****************************************************************************
 *  Local Function Declarations
*****************************************************************************/

/*****************************************************************************/
/*  Global Function Definitions                                              */
/*****************************************************************************/

/*****************************************************************************
*
* Function:  ServEvt_Create
*
* Description: create event queue
*              
*                                      
* Called from: 	
*
* Requrement:  
*
******************************************************************************/
SERV_EVENT	*ServEvt_Create ( unsigned char *ev_buf, unsigned short ev_length, const char * ev_name )
{
	SERV_EVENT *servEvt = NULL;
	if ( ev_buf != NULL && ev_length > 0 )
	{
		HAL_OS_SEM_ID os_ev	=	HalOS_SemaphoreCreate(0,ev_name);
		if( os_ev != NULL )
		{
			/* disable interrupt	*/
			CPU_SR_ALLOC();
			CPU_CRITICAL_ENTER();
			
			for ( unsigned char i = 0; i < sizeof(s_eventQueuePool)/sizeof(SERV_EVENT); i ++ )
			{
				
				if ( s_eventQueuePool[i].ev_buf == NULL )
				{	
					servEvt	=	&s_eventQueuePool[i];
					
					servEvt->ev_buf			=	ev_buf;
					servEvt->ev_size		=	ev_length;
					servEvt->ev_used		=	0;
#if SERVER_EVENT_NAME_EN
					servEvt->ev_name		=	ev_name;
#endif
					servEvt->os_event 	 	=	os_ev;
					
					break;
				}
			}
			
			/* restore interrupt	*/	
			CPU_CRITICAL_EXIT();
			
			if ( servEvt == NULL )
			{
				HalOS_SemaphoreDelete(os_ev);
			}
		}
	}
	
	return servEvt;
} 


/*****************************************************************************
*
* Function:  ServEvt_Del
*
* Description: del a event
*              
*                                      
* Called from: 	
*
* Requrement:  
*
******************************************************************************/
void	ServEvt_Del ( SERV_EVENT * servEvt )
{
	if ( servEvt != NULL )
	{	
		/* disable interrupt	*/
		CPU_SR_ALLOC();
		CPU_CRITICAL_ENTER();
		
		servEvt->ev_buf = NULL;
			
		HalOS_SemaphoreDelete(servEvt->os_event);
		
		/* restore interrupt	*/
		CPU_CRITICAL_EXIT();
	}
}

/*****************************************************************************
*
* Function:  ServEvt_Post
*
* Description: post a event
*              
*                                      
* Called from: 	
*
* Requrement:  
*
******************************************************************************/
boolean	ServEvt_Post ( SERV_EVENT * servEvt, unsigned char ev_code )
{
	boolean send_ok = FALSE;
	if ( servEvt != NULL && servEvt->ev_buf != NULL && ev_code != EVENT_NONE  )
	{
		/* disable interrupt	*/
		CPU_SR_ALLOC();
		CPU_CRITICAL_ENTER();
		
		if ( servEvt->ev_used < servEvt->ev_size )
		{
			servEvt->ev_used++;
			/* write event to queue		*/
            servEvt->ev_buf[servEvt->ev_write_index]	= ev_code;
			
			servEvt->ev_write_index ++;
			if ( servEvt->ev_write_index >= servEvt->ev_size )
			{	/* to arrayr end */
				servEvt->ev_write_index = 0;
			}

			send_ok = TRUE;
			
			HalOS_SemaphorePost(servEvt->os_event);
		}
		
		/* restore interrupt	*/
		CPU_CRITICAL_EXIT();
	}

	return send_ok;
}

/*****************************************************************************
*
* Function:  ServEvt_PostFront
*
* Description: post a event to queue front 
*              
*                                      
* Called from: 	
*
* Requrement:  
*
******************************************************************************/
void	ServEvt_PostFront ( SERV_EVENT * servEvt, unsigned char ev_code )
{
	if ( servEvt != NULL && servEvt->ev_buf != NULL && ev_code != EVENT_NONE  )
	{
		/* disable interrupt	*/
		CPU_SR_ALLOC();
		CPU_CRITICAL_ENTER();
		
		if ( servEvt->ev_used < servEvt->ev_size )
		{
			servEvt->ev_used++;
			/* write event to queue		*/
			if ( servEvt->ev_read_index > 0 ){
				servEvt->ev_read_index --;
			}else{
				servEvt->ev_read_index = servEvt->ev_size-1;
			}
            servEvt->ev_buf[servEvt->ev_read_index]	= ev_code;
			
			HalOS_SemaphorePost(servEvt->os_event);
		}
		/* restore interrupt	*/
		CPU_CRITICAL_EXIT();
	}
}

/*****************************************************************************
*
* Function:  ServEvt_Flush
*
* Description: flush event queue
*              
*                                      
* Called from: 	
*
* Requrement:  
*
******************************************************************************/
void	ServEvt_Flush ( SERV_EVENT * servEvt )
{
	if ( servEvt != NULL )
	{
		/* disable interrupt	*/
		CPU_SR_ALLOC();
		CPU_CRITICAL_ENTER();
		
		servEvt->ev_read_index	=	0;
		servEvt->ev_write_index	=	0;
		servEvt->ev_used		= 	0;

		do
		{
		}while( HalOS_SemaphoreWait(servEvt->os_event,0)==HAL_OS_SEM_ERR_NONE );

		/* restore interrupt	*/
		CPU_CRITICAL_EXIT();
	}
}


/*****************************************************************************
*
* Function:  ServEvt_Pend
*
* Description: This function waits for a event to be sent to a queue
*   timeout: 0, no wait, if event available return event else EVENT_NONE           
*                                      
* Called from: 	
*
* Requrement:  
*
******************************************************************************/
unsigned char ServEvt_Wait ( SERV_EVENT *servEvt, unsigned long timeout )
{
	unsigned char ev_code = SERV_EVENT_ERR;
	if ( servEvt != NULL && servEvt->ev_buf != NULL)
	{			
		switch ( HalOS_SemaphoreWait(servEvt->os_event,timeout) )
		{
		case HAL_OS_SEM_ERR_NONE:
			{
				/* disable interrupt	*/
				CPU_SR_ALLOC();
				CPU_CRITICAL_ENTER();
				
				if ( servEvt->ev_used > 0 )
				{	/* get event 			*/
					servEvt->ev_used --;
					ev_code	=	servEvt->ev_buf[servEvt->ev_read_index++];
					if ( servEvt->ev_read_index >=  servEvt->ev_size )
					{
						servEvt->ev_read_index = 0;
					}
				}
				/* restore interrupt	*/
				CPU_CRITICAL_EXIT();
			}
			break;
		
		case HAL_OS_SEM_TIMEOUT:
			/* time out 			*/
			ev_code	=	EVENT_TIMEOUT;
			break;
			
		default:
			/* no event in queue	*/
			ev_code	=	EVENT_NONE;
			break;
		}
	}

	return ev_code;
}


/*****************************************************************************
*
* Function:  ServEvt_Query
*
* Description: Query a event state, ocured or not
*             return event quantity
*                                      
* Called from: 	
*
* Requrement:  
*
******************************************************************************/
unsigned short  ServEvt_Query ( SERV_EVENT * servEvt )
{
	unsigned short retval = 0;
	
	(void)servEvt;
	
	if ( servEvt != NULL )
	{
		CPU_SR_ALLOC();
		CPU_CRITICAL_ENTER();

		retval	=	servEvt->ev_used;

		CPU_CRITICAL_EXIT();
	}
	return retval;
}

/*****************************************************************************
 *  Local Function Definitions
 *****************************************************************************/
 
/*****************************************************************************
*
* Function:  
*
* Description: 
*              
*                                      
* Called from: 	
*
* Requrement:  
*
******************************************************************************/

