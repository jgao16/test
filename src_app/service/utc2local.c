/************************Copyright (c)***************************
**
**                    http://www.bjfeic.com
**
**--------------File Info----------------------------------------
** File name:				utc2local.c
** Descriptions:		utc时间与RTC时间的转换实现文件,utc从1970-1-1 
**
**---------------------------------------------------------------
** Created by:			huangjianbiao
** Created date:		2015-4-30
** Version:					1.0
** Descriptions:	  
**---------------------------------------------------------------
** Modified by:
** Modified date:
** Version:
** Descriptions:
****************************************************************/

#include "utc2local.h"

//#define utc_8 8									//时间+8区

//LOCAL_TIME rtcTime;



//平年累积月分天数表 days2MonthNormal
static const uint16_t days2MonthNormal[12] = {
	31,//1
	31 + 28, //2
	31 + 28 + 31, //3
	31 + 28 + 31 + 30, //4
	31 + 28 + 31 + 30 + 31, //5
	31 + 28 + 31 + 30 + 31 + 30, //6
	31 + 28 + 31 + 30 + 31 + 30 + 31, //7
	31 + 28 + 31 + 30 + 31 + 30 + 31 + 31, //8
	31 + 28 + 31 + 30 + 31 + 30 + 31 + 31 + 30, //9
	31 + 28 + 31 + 30 + 31 + 30 + 31 + 31 + 30 + 31, //10
	31 + 28 + 31 + 30 + 31 + 30 + 31 + 31 + 30 + 31 + 30, //11
	31 + 28 + 31 + 30 + 31 + 30 + 31 + 31 + 30 + 31 + 30 + 31, //12
};

//闰年累积月分天数表 days2MonthRun
static const uint16_t days2MonthRun[12] = {
	31,//1
	31 + 29, //2
	31 + 29 + 31, //3
	31 + 29 + 31 + 30, //4
	31 + 29 + 31 + 30 + 31, //5
	31 + 29 + 31 + 30 + 31 + 30, //6
	31 + 29 + 31 + 30 + 31 + 30 + 31, //7
	31 + 29 + 31 + 30 + 31 + 30 + 31 + 31, //8
	31 + 29 + 31 + 30 + 31 + 30 + 31 + 31 + 30, //9
	31 + 29 + 31 + 30 + 31 + 30 + 31 + 31 + 30 + 31, //10
	31 + 29 + 31 + 30 + 31 + 30 + 31 + 31 + 30 + 31 + 30, //11
	31 + 29 + 31 + 30 + 31 + 30 + 31 + 31 + 30 + 31 + 30 + 31, //12
};




uint8_t IsRunNian(uint32_t year)
{
	//能被4整除,不能被百整除,能被400整除。

	if((year % 4 == 0) && ((year % 100 != 0) || (year % 400 == 0)))
	{
		return 1;
	}
	else
	{
		return 0;
	}
	/*
	//  初步判定为闰年.100不闰,400年又闰
	//	if (  year % 4 )			//不能被4除的是平年
	//		return 0;
	//
	//	if ( year % 100 )			//能被4除,但不能被百除的是闰年
	//		return 1;

	//	//100不闰,因此要进一步判定
	//	if ( year % 400 )
	//		return 0;
	//	else
	//		return 1;
	*/
}


void Utc2LocalTime(uint32 utc_tm, LOCAL_TIME *pTime, char const time_zone)
{
	uint32_t temp;
	uint32_t days = 0;
	uint32_t year = 1970;							//utc时间从1970开始


	//---------------得到时分秒和星期-----------------------
	pTime->Second = (utc_tm % 60);				//得到秒余数
	utc_tm = (utc_tm / 60);									//得到整数分钟数

	pTime->Minute = (utc_tm % 60);				//得到分钟余数
	utc_tm = (utc_tm / 60);									//得到整数小时数

	pTime->Hour =(utc_tm % 24 ) + time_zone;	//得到小时余数=====================
	if(pTime->Hour > 23)
	{
		pTime->Hour = pTime->Hour - 24;
		days = /*utc_tm =*/ ( utc_tm / 24 ) + 1;
		pTime->DayOfWeek = (4 + days) % 7;
		if(pTime->DayOfWeek == 0)
		{
			pTime->DayOfWeek = 7;
		}
	}
	else
	{
		pTime->Hour =(utc_tm % 24 ) + time_zone;	//得到小时余数=====================
		days = /*utc_tm =*/ ( utc_tm / 24 );				//转换为整数天数
		pTime->DayOfWeek = (4 + days) % 7;	                //1970-1-1 是星期4
		if(pTime->DayOfWeek == 0)
		{
			pTime->DayOfWeek = 7;
		}
	}
	//-------------------计算年分---------------------------
	//400年=146097天,100年=36524天,4年=1461天
	temp = (days / 146097) * 400;
	year += temp;

	days %= 146097;		//计算400年内的剩余天数
	temp = (days / 36524) * 100;
	year += temp;

	days %= 36524;
	temp = (days / 1461) * 4;
	year += temp;

	days %= 1461;			//计算4年内剩余天数,1970平1972闰年
	while( days > 365)
	{
		if(IsRunNian(year))
			days--;
		days -= 365;
		year++;
	}
	//now days <= 365
	if (!IsRunNian(year) && (days == 365) )
	{
		year++;
		pTime->Month = 1;
		pTime->Date = 1;
		pTime->Year = (uint16)year;
		return;
	}
	pTime->Year = (uint16)year;

	//---------------------------以下计算月、日----------------
	//here days <365
	if (IsRunNian(year))					//本年是闰年
	{
		for (uint8 i = 0; i < 12; i++)
		{
			if (days < days2MonthRun[i])
			{
				pTime->Month = i + 1;
				if (i == 0)
				{
					pTime->Date = (uint8)days;
				}
				else
				{
					pTime->Date = (uint8)(days - days2MonthRun[i - 1]);
				}
				pTime->Date++;
				return ;//<--------------------------------
			}
		}
	}
	else													//本年是平年
	{
		for (uint8 i = 0; i < 12; i++)
		{
			if (days < days2MonthNormal[i])
			{
				pTime->Month = i + 1;
				if (i == 0)
				{
					pTime->Date = (uint8)(days);
				}
				else
				{
					pTime->Date = (uint8)(days - days2MonthNormal[i - 1]);
				}
				pTime->Date++;
				return ;//<--------------------------------
			}
		}
	}
}



////////////////////
uint32  LocalTime2Utc(LOCAL_TIME const * ptTime, char const time_zone)
{
	//看一下有几个400年,几个100年,几个4年
	uint32_t y = ptTime->Year -1970;
	uint32_t dy = (y / 400);
	uint32_t days = dy *  (400 * 365 + 97);					//400年的天数

	dy = (y % 400) / 100;
	days += dy * (100 * 365 + 24);									//100年的天数

	dy = (y % 100) / 4;
	days += dy * (4 * 365 + 1);											//4年的天数

	dy = y % 4;																			//注意:这里1972是闰年,与1970只差2年
	days += dy * 365 ;
	//这个4年里,有没有闰年就差1天	
	if(dy == 3)
	{
		days++;	//只有这个是要手动加天数的,因为1973年计算时前面的天数按365天算,1972少算了一天
	}

	if (ptTime->Month != 1)
	{
		if(IsRunNian(ptTime->Year))										//看看今年是闰年还是平年
		{
			days += days2MonthRun[(ptTime->Month - 1) - 1];
		} 
		else 
		{
			days += days2MonthNormal[(ptTime->Month - 1) - 1]; //如果给定的月份数为x则,只有x-1个整数月
		}		
	}

	//月内的天数了
	days += ptTime->Date - 1;

	//注意了,这里的utc时间是8区的（对数据类型进行强制转换为32位）
	return (days * 24 * 3600 + ((uint32_t)ptTime->Hour - time_zone)* 3600 + (uint32_t)ptTime->Minute * 60 + (uint32_t)ptTime->Second);  

}

/****************************************************************
End Of File
****************************************************************/

