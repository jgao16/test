

#include "serv_wdt.h"
//#include "lib_mem.h"
#include "hal_wdt.h"
#include "serv_cfg.h"
#include "hal_os.h"
#include "cpu.h"

#include "trace_api.h"

#include "bsp_misc.h"
/********************************************************************************************************
watch dog server control block
********************************************************************************************************/


static SERV_WDT	s_st_wdtCB[WDT_SERV_NUM];


void 	ServWdt_init ( void )
{
	//HalWdt_Init();              	/* initial watch dog */
	
	//Mem_Clr ( s_st_wdtCB,sizeof(s_st_wdtCB) );	/* clear oswdt control block */
	
	//HalWdt_Feed();					/* feed watch dog */
}

void 	ServWdt_ForcePetWdt ( void )
{	/* force pet watch dog ,it is reflash flash */
	HalWdt_Feed();
}

void 	ServWdt_ForceRstCpu ( void )
{	/* force reset cpu */
	HalWdt_RstCpu();
}

SERV_WDT	*ServWdt_Create ( unsigned long	u4_wdtTimeout, char const *const name )
{
	/* disable interrupt */
	CPU_SR_ALLOC();
	
	SERV_WDT	*wdt_cb = NULL;
	
	CPU_CRITICAL_ENTER();
	
	for ( unsigned char	i = 0; i < WDT_SERV_NUM; i ++ )
	{
		if ( s_st_wdtCB[i].wdt_state == 0x00 )
		{
			wdt_cb = &s_st_wdtCB[i];
			
			wdt_cb->wdt_state 	= 	0x01;
			uint32 cnt_val      =  (u4_wdtTimeout+WDT_MONITOR_TIMER/2)/WDT_MONITOR_TIMER+1; //lint !e734
			if ( cnt_val > 0xFFFE )
			{
				cnt_val = 0xFFFE;
			}
			//wdt_cb->cntVal		=	(u4_wdtTimeout+WDT_MONITOR_TIMER/2)/WDT_MONITOR_TIMER+1; //lint !e734
			wdt_cb->cntVal = (uint16)cnt_val;
			if ( wdt_cb->cntVal < 2 )
			{
				wdt_cb->cntVal = 2;	/* min timeout */
			}
			wdt_cb->cnt		=	0;	/* stop watchdog */
#if SERV_WDT_NAME_EN > 0
			wdt_cb->name		=	name;
#endif
			
			break;
		}
	}
	
	CPU_CRITICAL_EXIT();
	
	return wdt_cb;
}

void 	ServWdt_start ( SERV_WDT *wdt_cb )
{
	if ( wdt_cb != NULL )
	{
		CPU_SR_ALLOC();
		CPU_CRITICAL_ENTER();
		
		wdt_cb->cnt = wdt_cb->cntVal;
		
		CPU_CRITICAL_EXIT();
	}
}

void 	ServWdt_stop ( SERV_WDT *wdt_cb )
{
	if ( wdt_cb != NULL )
	{
		CPU_SR_ALLOC();
		CPU_CRITICAL_ENTER();
		
		wdt_cb->cnt = 0;
		
		CPU_CRITICAL_EXIT();
	}
}

void 	ServWdt_reset ( SERV_WDT *wdt_cb )
{
	if ( wdt_cb != NULL )
	{
		CPU_SR_ALLOC();
		CPU_CRITICAL_ENTER();
		
		wdt_cb->cnt = wdt_cb->cntVal;
		
		CPU_CRITICAL_EXIT();
	}
}

void 	ServWdt_delete ( SERV_WDT *wdt_cb )
{
	if ( wdt_cb != NULL )
	{
		CPU_SR_ALLOC();
		CPU_CRITICAL_ENTER();
		
		wdt_cb->wdt_state = 0x00;
		
		CPU_CRITICAL_EXIT();
	}	
} 

void	ServWdt_monitor  ( void )
{
	CPU_SR_ALLOC();

	CPU_CRITICAL_ENTER();		/* disable interrutp */	
	
	for ( unsigned char	i = 0; i < WDT_SERV_NUM; i ++ )
	{
		if ( s_st_wdtCB[i].wdt_state == 0x01 )
		{
			if ( s_st_wdtCB[i].cnt > 0 )
			{
				s_st_wdtCB[i].cnt -- ;
				
				if ( s_st_wdtCB[i].cnt == 0 )
				{/* wdt dog timeout ,error */
					TRACE_VALUE(LOG_ERR, MOD_OS, "watchdog time out, id=%d", i);
#if SERV_WDT_NAME_EN					
					TRACE_STRING(LOG_ERR, MOD_OS, "Watchdog timeout: %s", s_st_wdtCB[i].name);
#endif
					HalOS_Delay(100);
					
					BspMisc_SetSwResetReason(SW_RESET_SERV_WDT);
					HalWdt_RstCpu();		/* force reset cpu */
				}				
			}
		}
	}
	
	CPU_CRITICAL_EXIT();
	HalWdt_Feed();
}
