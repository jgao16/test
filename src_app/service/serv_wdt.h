
#ifndef SERV_WDT_H__				
#define SERV_WDT_H__

#ifdef __cplusplus
extern "C" {
#endif 

#include "serv_cfg.h"


typedef struct
{
	unsigned char	wdt_state;
	unsigned short	cntVal;
	unsigned short	cnt;
#if SERV_WDT_NAME_EN > 0
	char const* name;
#endif	
}SERV_WDT;


void 		ServWdt_init ( void );
void 		ServWdt_ForcePetWdt ( void );
void 		ServWdt_ForceRstCpu ( void );
SERV_WDT *	ServWdt_Create ( unsigned long wdt_timeout_ms, char const *const name );
void 		ServWdt_start ( SERV_WDT *wdt_cb );
void 		ServWdt_stop ( SERV_WDT *wdt_cb );
void 		ServWdt_reset ( SERV_WDT *wdt_cb );
void 		ServWdt_delete ( SERV_WDT *wdt_cb );
void		ServWdt_monitor ( void );
void		ServWdt_monitor ( void );
void		ServWdt_monitor ( void );
#ifdef __cplusplus
}
#endif
#endif
