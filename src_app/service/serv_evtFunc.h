
#ifndef __SERV_EVT_FUNC_H__
#define __SERV_EVT_FUNC_H__
/*****************************************************************************
 *  Include Files
 *****************************************************************************/
//#include "std_type.h"
//#include "serv_evt.h"
#include "serv_cfg.h"

/*****************************************************************************
 *  Global Macro Definitions
 *****************************************************************************/
 




/*****************************************************************************
 *  Global Type Declarations
 *****************************************************************************/
/* %<Types> */


typedef struct
{
	unsigned char ev_code;
	
	void (*func)(void);
#if EVT_FUNC_NAME_EN
	unsigned char const *evtFunc_name;
#endif	
//	SERV_EVENT	*servEvt;
	
}SERV_EVT_FUNC;



#ifndef __SERV_EVT_FUNC_C__
/*****************************************************************************
 *  Global Data Declarations
 *****************************************************************************/
/* %<CalStructHeader> 	*/
/* %<CalStructContents>	*/
/* %<CalStructTrailer>	*/


/*****************************************************************************
 *  Global Function Declarations
 *****************************************************************************/
void			ServEvtFunc_Init	(void);
SERV_EVT_FUNC *	ServEvtFunc_Create 	(void(*func)(void),unsigned char const *const func_name );
void			ServEvtFunc_InvokeFunc (SERV_EVT_FUNC *servEvtFunc); 
unsigned char	ServEvtFunc_Start	(SERV_EVT_FUNC *servEvtFunc);
void			ServEvtFunc_Stop	(SERV_EVT_FUNC *servEvtFunc); 
void			ServEvtFunc_Del		(SERV_EVT_FUNC *servEvtFunc);
void 			ServEvtFunc_CallFunc(unsigned char ev_code);


#endif
#endif


