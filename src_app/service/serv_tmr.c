/*******************_**********************************************************
 *
 * Copyright (c) 
 *
 *****************************************************************************/
 
#define __SERV_TIMER_C__
 /*****************************************************************************
 *  Include Files
 *****************************************************************************/

#include "serv_cfg.h"
#include "serv_tmr.h"
#include "cpu.h"


/*****************************************************************************
 *  Local Macro Definitions
 *****************************************************************************/

 /*****************************************************************************
 *  Local Type Declarations
 *****************************************************************************/
 
/* event poll								*/
static	SERV_TIMER 	s_timerPool[TIMER_POOL_SIZE];

 
 /*****************************************************************************
 *  Global Variable Definitions (Global for the whole system)
 *****************************************************************************/
 
/*****************************************************************************
 *  Local Variable Definitions (Global within this file)
 *****************************************************************************/

 
 /*****************************************************************************
 *  Global Function Declarations
 *****************************************************************************/

/*****************************************************************************
 *  Local Function Declarations
*****************************************************************************/


/*****************************************************************************/
/*  Global Function Definitions                                              */
/*****************************************************************************/

/*****************************************************************************
*
* Function:  ServTmr_Create
*
* Description: create timer queue
*              
*                                      
* Called from: 	
*
* Requrement:  
*
******************************************************************************/
SERV_TIMER	*ServTmr_Create ( SERV_EVENT *servEvt, unsigned char tmr_code, unsigned char const *const tmr_name )
{
	SERV_TIMER	*servTmr = NULL;
	
	if ( servEvt != NULL )
	{
		/* disable interrupt	*/
		CPU_SR_ALLOC();
		CPU_CRITICAL_ENTER();
		
		for ( unsigned char i = 0; i < sizeof(s_timerPool)/sizeof(SERV_TIMER); i ++ )
		{
			if ( s_timerPool[i].servEvt == NULL )
			{
				servTmr = s_timerPool+i;
				
				servTmr->servEvt		=	servEvt;
				servTmr->tmr_code		=	tmr_code;
#if TIMER_QUEUE_NAME_EN
				servTmr->servTmrName	=	tmr_name;
#endif				
				break;
			}
		}
		
		/* restore interrupt	*/	
		CPU_CRITICAL_EXIT();

	}
	
	
	return servTmr;
} 
/*****************************************************************************
*
* Function:  ServTmr_Del
*
* Description: delete timer
*              
*                                      
* Called from: 	
*
* Requrement:  
*
******************************************************************************/
void	ServTmr_Del(SERV_TIMER *servTmr)
{	
	if ( servTmr != NULL )
	{
		/* disable interrupt	*/
		CPU_SR_ALLOC();
		CPU_CRITICAL_ENTER();
		
		servTmr->servEvt = NULL;

		/* restore interrupt	*/	
		CPU_CRITICAL_EXIT();
	}
}


/*****************************************************************************
*
* Function:  ServTmr_Stop
*
* Description: start timer for one short 
*              
*                                      
* Called from: 	
*
* Requrement:  
*
******************************************************************************/
void	ServTmr_Stop(SERV_TIMER *servTmr)
{	
	if ( servTmr != NULL )
	{
		/* disable interrupt	*/
		CPU_SR_ALLOC();
		CPU_CRITICAL_ENTER();
		
		//servTmr->cycTmCnt = 0;
		servTmr->Cnt	  = 0;
		
		/* restore interrupt	*/	
		CPU_CRITICAL_EXIT();
	}
}

/*****************************************************************************
*
* Function:  ServTmr_StartShorTmr
*
* Description: start timer for one short 
*              
*                                      
* Called from: 	
*
* Requrement:  
*
******************************************************************************/
void	ServTmr_StartShort(SERV_TIMER *servTmr, unsigned short timer)
{	
	if ( servTmr != NULL )
	{
		/* disable interrupt	*/
		CPU_SR_ALLOC();
		CPU_CRITICAL_ENTER();
		
		servTmr->cycTmCnt = 0;
		servTmr->Cnt	  = timer;
		
		/* restore interrupt	*/	
		CPU_CRITICAL_EXIT();		
		
		if ( timer == 0 )
		{	/* send event right */
			(void)ServEvt_Post ( servTmr->servEvt, servTmr->tmr_code );
		}

	}
}
/*****************************************************************************
*
* Function:  ServTmr_StartCyclicTmr
*
* Description: start timer cyclic, cyclic send event  
*              
*                                      
* Called from: 	
*
* Requrement:  
*
******************************************************************************/
void	ServTmr_StartCyclic(SERV_TIMER *servTmr, unsigned short timer_dly, unsigned short timer_cyc )
{
	if ( servTmr != NULL )
	{
		/* disable interrupt	*/
		CPU_SR_ALLOC();
		CPU_CRITICAL_ENTER();
		
		servTmr->cycTmCnt = timer_cyc;
		servTmr->Cnt 	  = timer_dly==0?timer_cyc:timer_dly;
		
		/* restore interrupt	*/	
		CPU_CRITICAL_EXIT();
		
		if ( timer_dly == 0 )
		{	/* send event right */
			(void)ServEvt_Post ( servTmr->servEvt, servTmr->tmr_code );
		}
	}
}

/*****************************************************************************
*
* Function:  ServTmr_CycCallTask
*
* Description: cyclic call ,10ms 
*              
*                                      
* Called from: 	
*
* Requrement:  
*
******************************************************************************/
void	ServTmr_CycCallTask ( void )
{
	SERV_TIMER	*serv;
	/* disable interrupt	*/
	CPU_SR_ALLOC();
	CPU_CRITICAL_ENTER();
		
	for ( unsigned char i = 0; i < sizeof(s_timerPool)/sizeof(SERV_TIMER); i ++ )
	{
		serv = s_timerPool+i;
		
		if ( serv->servEvt != NULL && serv->Cnt > 0 )
		{
			serv->Cnt --;
			if ( serv->Cnt == 0 )
			{
				serv->Cnt = serv->cycTmCnt;
				(void)ServEvt_Post ( serv->servEvt, serv->tmr_code );
			}
		}
	}
	
	/* restore interrupt	*/	
	CPU_CRITICAL_EXIT();
}


/*****************************************************************************
 *  Local Function Definitions
 *****************************************************************************/
 
/*****************************************************************************
*
* Function:  
*
* Description: 
*              
*                                      
* Called from: 	
*
* Requrement:  
*
******************************************************************************/

