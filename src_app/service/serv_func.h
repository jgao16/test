
#ifndef __SERV_FUNC_H__
#define __SERV_FUNC_H__
/*****************************************************************************
 *  Include Files
 *****************************************************************************/

/*****************************************************************************
 *  Global Macro Definitions
 *****************************************************************************/
 
//#define	SERV_FUNC_NAME_EN		1u
//#define	SERV_FUNC_POOL_SIZE		(10u)





/*****************************************************************************
 *  Global Type Declarations
 *****************************************************************************/
/* %<Types> */



typedef struct
{	
	unsigned short 		cycTmCnt;
	unsigned short 		Cnt;
	
	void (*func)(void);
	
#if TIMER_QUEUE_NAME_EN
	const char *func_name;
#endif	
	
}SERV_FUNC;


typedef void (*CYC_FUNC)(void const*arg);



#ifndef __SERV_FUNC_C__
/*****************************************************************************
 *  Global Data Declarations
 *****************************************************************************/
/* %<CalStructHeader> 	*/
/* %<CalStructContents>	*/
/* %<CalStructTrailer>	*/


/*****************************************************************************
 *  Global Function Declarations
 *****************************************************************************/
SERV_FUNC *	ServFunc_Create 	( void(*func)(void),char const *const func_name );

void * 		ServFunc_CreatMpCycFunc( CYC_FUNC func, void const * arg);
void * 		ServFunc_CreatLpCycFunc( CYC_FUNC func, void const * arg);
void * 		ServFunc_CreatRealTime500msCycFunc( CYC_FUNC func, void const *arg );
void  		ServFunc_DelMpLpRealtimeCycFunc( void const * serv_func);

void		ServFunc_Del		(SERV_FUNC *servFunc);
void		ServFunc_Stop		(SERV_FUNC *servFunc); 
void		ServFunc_StartShort	(SERV_FUNC *servFunc, unsigned short timer);
void		ServFunc_StartCyclic(SERV_FUNC *servFunc, unsigned short timer_dly, unsigned short timer_cyc );
void		ServFunc_CycCallTask( void );
void		ServFunc_MpCycCallTask ( void );
void		ServFunc_LpCycCallTask ( void );
void 		ServFunc_Realtime500msCycCallTask( void );
#endif
#endif


