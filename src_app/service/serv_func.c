/*******************_**********************************************************
 *
 * Copyright (c) 
 *
 *****************************************************************************/
 
#define __SERV_FUNC_C__
 /*****************************************************************************
 *  Include Files
 *****************************************************************************/
 
#include "serv_cfg.h" 
#include "cpu.h"
#include "serv_func.h"

#include "hal_os.h"

#include "trace_api.h"

/*****************************************************************************
 *  Local Macro Definitions
 *****************************************************************************/

#define		FUNC_ELEMENT_USED		(0x01)


 /*****************************************************************************
 *  Local Type Declarations
 *****************************************************************************/
 
/* event poll								*/
static	SERV_FUNC 	s_funcPool[SERV_FUNC_POOL_SIZE];

#if SERV_FUNC_MP_CYC_FUNC_50MS_SIZE
static CYC_FUNC		MpCycFunc50ms_tbl[SERV_FUNC_MP_CYC_FUNC_50MS_SIZE];
static void const   *MpCycFunc_Para[SERV_FUNC_MP_CYC_FUNC_50MS_SIZE];
#endif 

#if SERV_FUNC_LP_CYC_FUNC_500MS_SIZE
static CYC_FUNC		LpCycFunc500ms_tbl[SERV_FUNC_LP_CYC_FUNC_500MS_SIZE];
static void const   *LpCycFunc_Para[SERV_FUNC_LP_CYC_FUNC_500MS_SIZE];
#endif 

#if SERV_FUNC_REALTIME_CYC_FUNC_500MS_SIZE
static CYC_FUNC		RealTimeCycFunc500ms_tbl[SERV_FUNC_REALTIME_CYC_FUNC_500MS_SIZE];
static void const	*RealTimeCycFunc_Para[SERV_FUNC_REALTIME_CYC_FUNC_500MS_SIZE];
#endif

 /*****************************************************************************
 *  Global Variable Definitions (Global for the whole system)
 *****************************************************************************/
 
/*****************************************************************************
 *  Local Variable Definitions (Global within this file)
 *****************************************************************************/

 
 /*****************************************************************************
 *  Global Function Declarations
 *****************************************************************************/

/*****************************************************************************
 *  Local Function Declarations
*****************************************************************************/


/*****************************************************************************/
/*  Global Function Definitions                                              */
/*****************************************************************************/

/*****************************************************************************
*
* Function:  ServFunc_Create
*
* Description: create function service
*              
*                                      
* Called from: 	
*
* Requrement:  
*
******************************************************************************/
SERV_FUNC	*ServFunc_Create ( void(*func)(void), char const *const func_name )
{
	SERV_FUNC	*servFunc = NULL;
			
	/* disable interrupt	*/
	CPU_SR_ALLOC();
	CPU_CRITICAL_ENTER();
	
	for ( unsigned char i = 0; i < sizeof(s_funcPool)/sizeof(SERV_FUNC); i ++ )
	{
		if ( s_funcPool[i].func == NULL )
		{
			servFunc = s_funcPool+i;

			servFunc->func		= func;
			servFunc->cycTmCnt	= 0;
			servFunc->Cnt	  	= 0;
#if TIMER_QUEUE_NAME_EN
			servFunc->func_name	= func_name;
#endif				
			break;
		}
	}
	
	/* restore interrupt	*/	
	CPU_CRITICAL_EXIT();
	
	
	return		servFunc;
}


/*****************************************************************************
*
* Function:  ServFunc_CreatMpCycCal
*
* Description: 
*              
*                                      
* Called from: 	
*
* Requrement:  
*
******************************************************************************/
void * ServFunc_CreatMpCycFunc( CYC_FUNC func, void const*arg )
{
	void * serv_func = NULL;

#if SERV_FUNC_MP_CYC_FUNC_50MS_SIZE
	for ( uint16 i = 0; i < SERV_FUNC_MP_CYC_FUNC_50MS_SIZE; i ++ )
	{
		CPU_SR_ALLOC();
		CPU_CRITICAL_ENTER();

		if ( MpCycFunc50ms_tbl[i] == NULL )
		{
			MpCycFunc50ms_tbl[i] = func;
			MpCycFunc_Para[i]	 = arg;
			CPU_CRITICAL_EXIT();
			serv_func = &MpCycFunc50ms_tbl[i];
			break;
		}
		else
		{
			CPU_CRITICAL_EXIT();
		}
	}
#endif

	return serv_func;
}





/*****************************************************************************
*
* Function:  ServFunc_CreatLpCycFunc
*
* Description: 
*              
*                                      
* Called from: 	
*
* Requrement:  
*
******************************************************************************/
void * ServFunc_CreatLpCycFunc( CYC_FUNC func, void const*arg )
{
	void * serv_func = NULL;

#if SERV_FUNC_LP_CYC_FUNC_500MS_SIZE
	for ( uint16  i = 0; i < SERV_FUNC_LP_CYC_FUNC_500MS_SIZE; i ++ )
	{
		CPU_SR_ALLOC();
		CPU_CRITICAL_ENTER();

		if ( LpCycFunc500ms_tbl[i] == NULL )
		{
			LpCycFunc500ms_tbl[i] = func;
			LpCycFunc_Para[i]	 = arg;
			CPU_CRITICAL_EXIT();
			serv_func = &LpCycFunc500ms_tbl[i];
			break;
		}
		else
		{
			CPU_CRITICAL_EXIT();
		}
	}
#endif

	return serv_func;
}

void * ServFunc_CreatRealTime500msCycFunc( CYC_FUNC func, void const *arg )
{
	void * serv_func = NULL;

#if SERV_FUNC_REALTIME_CYC_FUNC_500MS_SIZE
	for ( uint16  i = 0; i < SERV_FUNC_REALTIME_CYC_FUNC_500MS_SIZE; i ++ )
	{
		CPU_SR_ALLOC();
		CPU_CRITICAL_ENTER();

		if ( RealTimeCycFunc500ms_tbl[i] == NULL )
		{
			RealTimeCycFunc500ms_tbl[i] = func;
			RealTimeCycFunc_Para[i]	 = arg;
			CPU_CRITICAL_EXIT();
			serv_func = &RealTimeCycFunc500ms_tbl[i];
			break;
		}
		else
		{
			CPU_CRITICAL_EXIT();
		}
	}
#endif

	return serv_func;
}

void  ServFunc_DelMpLpRealtimeCycFunc( void * serv_func)
{
	if ( serv_func != NULL )
	{
		CPU_SR_ALLOC();
		CPU_CRITICAL_ENTER();

		//CYC_FUNC *cyc_func = serv_func;
		//*cyc_func = NULL;
		*(CYC_FUNC *)serv_func = NULL;
		
		CPU_CRITICAL_EXIT();
	}
}


/*****************************************************************************
*
* Function:  ServFunc_Del
*
* Description: delete function service
*              
*                                      
* Called from: 	
*
* Requrement:  
*
******************************************************************************/
void	ServFunc_Del(SERV_FUNC *servFunc)
{	
	if ( servFunc != NULL )
	{
		/* disable interrupt	*/
		CPU_SR_ALLOC();
		CPU_CRITICAL_ENTER();
		
		servFunc->func = NULL;

		/* restore interrupt	*/	
		CPU_CRITICAL_EXIT();
	}
}


/*****************************************************************************
*
* Function:  ServFunc_Stop
*
* Description: stop function service 
*              
*                                      
* Called from: 	
*
* Requrement:  
*
******************************************************************************/
void	ServFunc_Stop(SERV_FUNC *servFunc)
{	
	if ( servFunc != NULL )
	{
		/* disable interrupt	*/
		CPU_SR_ALLOC();
		CPU_CRITICAL_ENTER();
		
		//servTmr->cycTmCnt = 0;
		servFunc->Cnt	  = 0;
		
		/* restore interrupt	*/	
		CPU_CRITICAL_EXIT();
	}
}

/*****************************************************************************
*
* Function:  ServTmr_StartShorTmr
*
* Description: start function service for one short 
*              
*                                      
* Called from: 	
*
* Requrement:  
*
******************************************************************************/
void	ServFunc_StartShort(SERV_FUNC *servFunc, unsigned short timer)
{	
	void (*func)(void);
	func = NULL;
	
	if ( servFunc != NULL )
	{
		/* disable interrupt	*/
		CPU_SR_ALLOC();
		CPU_CRITICAL_ENTER();
		
		servFunc->cycTmCnt = 0;
		servFunc->Cnt	  = timer;
		
		if ( timer == 0 )
		{
			func = servFunc->func;
		}
		
		/* restore interrupt	*/	
		CPU_CRITICAL_EXIT();
		
		if ( func != NULL )
		{
			func();
		}
	}
}
/*****************************************************************************
*
* Function:  ServFunc_StartCyclic
*
* Description: start cyclic func 
*              
*                                      
* Called from: 	
*
* Requrement:  
*
******************************************************************************/
void	ServFunc_StartCyclic(SERV_FUNC *servFunc, unsigned short timer_dly, unsigned short timer_cyc )
{
	void (*func)(void);
	
	func = NULL;
	
	if ( servFunc != NULL )
	{
		/* disable interrupt	*/
		CPU_SR_ALLOC();
		CPU_CRITICAL_ENTER();
		
		servFunc->cycTmCnt = timer_cyc;
		servFunc->Cnt 	  = timer_dly==0?timer_cyc:timer_dly;
		
		if ( timer_dly == 0 )
		{
			func = servFunc->func;
		}

		/* restore interrupt	*/	
		CPU_CRITICAL_EXIT();
		
		if ( func != NULL )
		{
			func();
		}
	}
}

/*****************************************************************************
*
* Function:  ServFunc_CycCallTask
*
* Description: cyclic call ,10ms 
*              
*                                      
* Called from: 	
*
* Requrement:  
*
******************************************************************************/
void	ServFunc_CycCallTask ( void )
{
	SERV_FUNC	*servFunc;
	void (*func)(void);
	
	/* alloc cpu SR */
	CPU_SR_ALLOC();
	
	for ( unsigned char i = 0; i < sizeof(s_funcPool)/sizeof(SERV_FUNC); i ++ )
	{
		servFunc = s_funcPool+i;
		
		func = NULL;
		
		/* disable interrupt	*/
		CPU_CRITICAL_ENTER();
		
		if ( servFunc->func != NULL && servFunc->Cnt > 0 )
		{
			servFunc->Cnt --;
			if ( servFunc->Cnt == 0 )
			{
				servFunc->Cnt = servFunc->cycTmCnt;
				func	  	  = servFunc->func;
			}
		}
		
		/* restore interrupt	*/	
		CPU_CRITICAL_EXIT();
		
		if ( func != NULL )
		{
			uint32 temp = HalOS_GetKernelTick();
			
			func();

			temp = HalOS_GetKernelTick() - temp;
			if ( temp > 10 )
			{
#if TIMER_QUEUE_NAME_EN
				TRACE_STRING(LOG_SHOUT, MOD_OS,"Serv Cyc Function execute > 10ms: %s", servFunc->func_name);
#endif	
				TRACE_VALUE2(LOG_SHOUT, MOD_OS, "Serv Cyc Function execute > 10ms, func_id=%d, tm=%dms", i, (uint16)temp);
			}
			else if ( temp > 5 )
			{
#if TIMER_QUEUE_NAME_EN
				TRACE_STRING(LOG_SHOUT, MOD_OS,"Serv Cyc Function execute > 5ms: %s", servFunc->func_name);
#endif			
				TRACE_VALUE2(LOG_SHOUT, MOD_OS, "Serv Cyc Function execute > 5ms, func_id=%d, exec tm=%dms", i, (uint16)temp);
			}
			else
			{}
		}	
	}
}



void	ServFunc_MpCycCallTask ( void )
{
#if SERV_FUNC_MP_CYC_FUNC_50MS_SIZE
	for ( uint16 i = 0; i < SERV_FUNC_MP_CYC_FUNC_50MS_SIZE; i ++ )
	{	
		CYC_FUNC func;
		
		CPU_SR_ALLOC();
		CPU_CRITICAL_ENTER();
		func = MpCycFunc50ms_tbl[i];
		CPU_CRITICAL_EXIT();

		if ( func != NULL )
		{
			func(MpCycFunc_Para[i]);
		}
	}
#endif	
}

void	ServFunc_LpCycCallTask ( void )
{
#if SERV_FUNC_LP_CYC_FUNC_500MS_SIZE
	for ( uint16 i = 0; i < SERV_FUNC_LP_CYC_FUNC_500MS_SIZE; i ++ )
	{	
		CYC_FUNC func;
		
		CPU_SR_ALLOC();
		CPU_CRITICAL_ENTER();
		func = LpCycFunc500ms_tbl[i];
		CPU_CRITICAL_EXIT();

		if ( func != NULL )
		{
			func(LpCycFunc_Para[i]);
		}
	}
#endif		
}


void ServFunc_Realtime500msCycCallTask( void )
{
#if SERV_FUNC_REALTIME_CYC_FUNC_500MS_SIZE
	for ( uint16 i = 0; i < SERV_FUNC_REALTIME_CYC_FUNC_500MS_SIZE; i ++ )
	{	
		CYC_FUNC func;
		
		CPU_SR_ALLOC();
		CPU_CRITICAL_ENTER();
		func = RealTimeCycFunc500ms_tbl[i];
		CPU_CRITICAL_EXIT();

		if ( func != NULL )
		{
			func(RealTimeCycFunc_Para[i]);
		}
	}
#endif
}
/*****************************************************************************
 *  Local Function Definitions
 *****************************************************************************/
 
/*****************************************************************************
*
* Function:  
*
* Description: 
*              
*                                      
* Called from: 	
*
* Requrement:  
*
******************************************************************************/

