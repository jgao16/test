#ifndef _AVG_FILTER_H_
#define _AVG_FILTER_H_

#include <stdbool.h>
#include <stdint.h>

typedef struct
{
    bool isBufFull;
    uint8_t wrIdx;
    uint16_t bufLen;
    uint16_t *buf;
    uint32_t sum;
} avg_filter_handle_t;

void AVG_FILTER_CreateHandle(avg_filter_handle_t *handle, uint16_t * buf, uint16_t bufLen);

uint16_t AVG_FILTER_Feed(avg_filter_handle_t *handle, uint16_t data);

#endif
