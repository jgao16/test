
#ifndef __SERV_MISC_H__
#define __SERV_MISC_H__
/*****************************************************************************
 *  Include Files
 *****************************************************************************/



/*****************************************************************************
 *  Global Macro Definitions
 *****************************************************************************/
 




/*****************************************************************************
 *  Global Type Declarations
 *****************************************************************************/
/* %<Types> */



/*****************************************************************************
 *  Global Data Declarations
 *****************************************************************************/
/* %<CalStructHeader> 	*/
/* %<CalStructContents>	*/
/* %<CalStructTrailer>	*/


/*****************************************************************************
 *  Global Function Declarations
 *****************************************************************************/
/* create event			*/
void 	ServMisc_CntSetUint8( uint8 *cnt, uint8 val );
uint8   ServMisc_CntGetUint8( uint8 *cnt );
void	ServMisc_CntIncUint8( uint8 *cnt );	// max cnt = 0xFFFE
void	ServMisc_CntDecUint8( uint8 *cnt );

void 	ServMisc_CntSetUint16( uint16 *cnt, uint16 val );
uint16  ServMisc_CntGetUint16( uint16 *cnt );
void	ServMisc_CntIncUint16( uint16 *cnt );	// max cnt = 0xFFFE
void	ServMisc_CntDecUint16( uint16 *cnt );


#endif




