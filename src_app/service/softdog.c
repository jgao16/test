

#include "std_type.h"

//#include "hal_os.h"

#include "serv.h"

#include "softdog.h"
#include "xxtea.h"

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/* 48h */
#define	TM_CNT_VALUE	(48u*60*60/10)	


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static	const	uint32	xxtea_key[4] = 
{
	0x12345678, 0x2345678, 0x00345678, 0x98765431
};

static uint32 xxtexa_seed[4];
static uint32 xxtea_cipher[4];

static	uint32	s_u4_tmCnt;			/* timer counter for 48h */
static	uint8	s_u1_lowTaskTm;

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static	void	tmr_10s_task ( void );

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void	SoftDog_Init ( void )
{
	/* read seed (unique code) */
	s_u4_tmCnt	=	TM_CNT_VALUE;

	/* create 10s timer server */
	ServFunc_StartCyclic(	ServFunc_Create (tmr_10s_task, "SoftDog 10s Func" ), 
							100, 10000u/10);
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void 	SoftDog_SetKey(uint32 const*seed, uint32 const*cipher)
{
	xxtexa_seed[0] = seed[0];
	xxtexa_seed[1] = seed[1];
	xxtexa_seed[2] = seed[2];
	
	xxtea_cipher[0] = cipher[0];
	xxtea_cipher[1] = cipher[1];
	xxtea_cipher[2] = cipher[2];
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void	SoftDog_Lp500msFunc ( void )
{
	if ( s_u1_lowTaskTm > 0 )
	{
		s_u1_lowTaskTm --;
	}

	if ( s_u1_lowTaskTm == 0 )
	{
		s_u1_lowTaskTm = 20;		// 10;
		if ( s_u4_tmCnt ){
			s_u4_tmCnt --;
		}
	}
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
SOFTDOG_CIPHER_STATE SoftDog_GetCipherState ( void )
{
	SOFTDOG_CIPHER_STATE state = SOFT_CIPHER_ERROR;
	
	uint32 seed[3];
	
	seed[0] = xxtexa_seed[0];
	seed[1] = xxtexa_seed[1];
	seed[2] = xxtexa_seed[2];
	
	xxtea_btea(seed, 3, xxtea_key);						/*	encrypt key				*/
	if ( xxtea_cipher[0] == seed[0] && xxtea_cipher[1] == seed[1] && xxtea_cipher[2] == seed[2] )
	{
		state       = SOFTDOG_CIPHER_OK;
		s_u4_tmCnt	=	TM_CNT_VALUE;					/* reload timer counter 	*/		
	}
	else if ( s_u4_tmCnt == 0 )
	{
		state = SOFTDOG_TM_EXPIRE;
	}
	else{}
	
	return state;
}


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static	void	tmr_10s_task ( void )
{
	if ( s_u4_tmCnt > 0 ){
		s_u4_tmCnt --;
	}
}


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/



