
#ifndef _CRC_32_H_H
#define _CRC_32_H_H

unsigned long Crc32_Calc(const unsigned char *buf, unsigned long size);

unsigned long Crc32_Init(void);
unsigned long Crc32_Update(unsigned char const*buf, unsigned long len, unsigned long crc);
unsigned long Crc32_Final ( unsigned long crc );



#endif

