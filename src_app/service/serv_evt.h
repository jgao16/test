
#ifndef __SERV_EVT_H__
#define __SERV_EVT_H__
/*****************************************************************************
 *  Include Files
 *****************************************************************************/

#include "hal_os.h"
#include "serv_cfg.h"

/*****************************************************************************
 *  Global Macro Definitions
 *****************************************************************************/
 




/*****************************************************************************
 *  Global Type Declarations
 *****************************************************************************/
/* %<Types> */
typedef enum
{
	EVENT_NONE	=	0,
	/* fill first event 					*/
	/* event 10ms timer 					*/
	EVENT_SYSTEM_2MS_TIMER,
	/* middle 50ms event					*/
	EVENT_SYSTEM_MIDDLE_POWER_50MS,
	/* event 100ms timer					*/
	EVENT_SYSTEM_100MS_TIMER,
	/* evernt power change 					*/
	EVENT_SYSTEM_POWER_CHG,
	/* for low power state, 500ms event		*/
	EVENT_SYSTEM_LOW_POWER_500MS,
	/* for system, enter to sleep			*/
	EVENT_SYSTEM_ENTER_TO_SLEEP,
	/* for realtime 500ms event				*/
	EVENT_SYSTEM_REALTIME_500MS,

	/* For Low Power Mode, urgent PVD, 		*/
	EVENT_PVD_FALLING_DETECTING,
	
	/* for flowmeasure initial complete		*/
	EVENT_SYSTEM_FLOWMEASURE_INITED,
	
	EVENT_SYSTEM_OUTPUT_CPU_LOAD,
	/* end event					*/
	
	/* start server event function	*/
	SERV_EVT_FUNC_START	=	0xC0,
	
	/* end server event function	*/
	SERV_EVT_FUNC_END	=	0xEF,


/******for special event value**************************/	

	/* for Mail box or queue event 	*/
	EVENT_MBOX		=	0xF0,
	/* for timeout event			*/
	EVENT_TIMEOUT	=	0xF1,
	/* no event quene				*/
	SERV_EVENT_ERR	=	0xF2,

}EVENT_TYPE;


typedef struct
{
	unsigned char		*ev_buf;
#if SERVER_EVENT_NAME_EN	
	const char			*ev_name;
#endif	

	HAL_OS_SEM_ID		os_event;

	unsigned short  	ev_size;
	unsigned short 		ev_used;
	unsigned short   	ev_read_index;
	unsigned short 		ev_write_index;	
}SERV_EVENT;


#ifndef __SERV_EVT_C__
/*****************************************************************************
 *  Global Data Declarations
 *****************************************************************************/
/* %<CalStructHeader> 	*/
/* %<CalStructContents>	*/
/* %<CalStructTrailer>	*/


/*****************************************************************************
 *  Global Function Declarations
 *****************************************************************************/
/* create event			*/
SERV_EVENT	*ServEvt_Create ( unsigned char *ev_buf, unsigned short ev_length, const char * ev_name );
/* wait a event			*/
unsigned char ServEvt_Wait ( SERV_EVENT *servEvt, unsigned long timeout );
/* delete server event	*/
void		ServEvt_Del ( SERV_EVENT * servEvt ); 
/* post event normal	*/
boolean		ServEvt_Post ( SERV_EVENT * servEvt, unsigned char ev_code );
/* post event to front	*/
void		ServEvt_PostFront ( SERV_EVENT * servEvt, unsigned char ev_code );

void		ServEvt_Flush ( SERV_EVENT * servEvt );

unsigned short  ServEvt_Query ( SERV_EVENT * servEvt );


#endif
#endif


