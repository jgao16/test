
#ifndef __SOFT_DOG_H_				
#define __SOFT_DOG_H_

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#include "std_type.h"

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#ifdef __cplusplus
extern "C" {
#endif 

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
typedef enum
{
	SOFTDOG_CIPHER_OK,
	SOFT_CIPHER_ERROR,
	SOFTDOG_TM_EXPIRE,
}SOFTDOG_CIPHER_STATE;

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void	SoftDog_Init ( void );
void 	SoftDog_SetKey(uint32 const *seed, uint32 const *cipher);
void	SoftDog_Lp500msFunc ( void );

SOFTDOG_CIPHER_STATE SoftDog_GetCipherState ( void );

#ifdef __cplusplus
}
#endif
#endif
