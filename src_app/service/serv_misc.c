

#include "std_type.h"

//#include "hal_os.h"
#include "cpu.h"

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void 	ServMisc_CntSetUint8( uint8 *cnt, uint8 val )
{
	CPU_SR_ALLOC();
	CPU_CRITICAL_ENTER();

	*cnt = val;
	
	CPU_CRITICAL_EXIT();
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
uint8  ServMisc_CntGetUint8( uint8 *cnt )
{
	uint8 val;

	(void)cnt;
	
	CPU_SR_ALLOC();
	CPU_CRITICAL_ENTER();

	val = *cnt;
	
	CPU_CRITICAL_EXIT();

	return val;
}
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void	ServMisc_CntIncUint8( uint8 *cnt )
{
	CPU_SR_ALLOC();
	CPU_CRITICAL_ENTER();
	if ( *cnt < 0xFF )
	{
		(*cnt) ++;
	}
	CPU_CRITICAL_EXIT();
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void	ServMisc_CntDecUint8( uint8 *cnt )
{
	CPU_SR_ALLOC();
	CPU_CRITICAL_ENTER();
	if ( *cnt > 0 )
	{
		(*cnt) --;
	}
	CPU_CRITICAL_EXIT();
}


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void 	ServMisc_CntSetUint16( uint16 *cnt, uint16 val )
{
	CPU_SR_ALLOC();
	CPU_CRITICAL_ENTER();

	*cnt = val;
	
	CPU_CRITICAL_EXIT();
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
uint16  ServMisc_CntGetUint16( uint16 *cnt )
{
	uint16 val;

	(void)cnt;
	
	CPU_SR_ALLOC();
	CPU_CRITICAL_ENTER();

	val = *cnt;
	
	CPU_CRITICAL_EXIT();

	return val;
}
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void	ServMisc_CntIncUint16( uint16 *cnt )
{
	CPU_SR_ALLOC();
	CPU_CRITICAL_ENTER();
	if ( *cnt < 0xFFFE)
	{
		(*cnt) ++;
	}
	CPU_CRITICAL_EXIT();
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void	ServMisc_CntDecUint16( uint16 *cnt )
{
	CPU_SR_ALLOC();
	CPU_CRITICAL_ENTER();
	if ( *cnt > 0 )
	{
		(*cnt) --;
	}
	CPU_CRITICAL_EXIT();
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/



/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/



