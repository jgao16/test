/*******************_**********************************************************
 *
 * Copyright (c) 
 *
 *****************************************************************************/
 
#define __SERV_EVT_FUNC_C__
 /*****************************************************************************
 *  Include Files
 *****************************************************************************/
#include "std_type.h" 
#include "serv_cfg.h"
#include "serv_evt.h"
#include "serv_evtFunc.h"

#include "cpu.h"

//lint -e818 -e830

/*****************************************************************************
 *  Local Macro Definitions
 *****************************************************************************/
#define	EVT_FUNC_NOT_USED	(0xffu)

 /*****************************************************************************
 *  Local Type Declarations
 *****************************************************************************/
 

 
 /*****************************************************************************
 *  Global Variable Definitions (Global for the whole system)
 *****************************************************************************/
 
/*****************************************************************************
 *  Local Variable Definitions (Global within this file)
 *****************************************************************************/
/* serv event function pool								*/
#if EVT_FUNC_POOL_SIZE > 0
static	SERV_EVT_FUNC 	s_eventFuncPool[EVT_FUNC_POOL_SIZE];
#endif 
 /*****************************************************************************
 *  Global Function Declarations
 *****************************************************************************/
extern SERV_EVENT * Sys_GetServEvt(void);	//lint -e526

/*****************************************************************************
 *  Local Function Declarations
*****************************************************************************/


/*****************************************************************************/
/*  Global Function Definitions                                              */
/*****************************************************************************/

/*****************************************************************************
*
* Function:  ServEvtFunc_Init
*
* Description: service event function init	
*              
*                                      
* Called from: 	
*
* Requrement:  
*
******************************************************************************/
void	ServEvtFunc_Init ( void )
{
#if EVT_FUNC_POOL_SIZE>0
	for ( uint8 i = 0; i < sizeof(s_eventFuncPool)/sizeof(SERV_EVT_FUNC); i ++ )
	{	/* ev_code indicate not used this pool	*/
		s_eventFuncPool[i].ev_code = EVT_FUNC_NOT_USED;	
	}
#endif
}

/*****************************************************************************
*
* Function:  ServEvt_Create
*
* Description: create event queue
*              
*                                      
* Called from: 	
*
* Requrement:  
*
******************************************************************************/
SERV_EVT_FUNC *	ServEvtFunc_Create ( void(*func)(void), unsigned char const *const func_name )
{
	SERV_EVT_FUNC *evtFunc = NULL;

#if EVT_FUNC_POOL_SIZE>0
	
	if ( func != NULL  )
	{
		/* disable interrupt	*/
		CPU_SR_ALLOC();
		CPU_CRITICAL_ENTER();
		
		for ( uint8 i = 0; i < sizeof(s_eventFuncPool)/sizeof(SERV_EVT_FUNC); i ++ )
		{	
			if ( s_eventFuncPool[i].ev_code == EVT_FUNC_NOT_USED )
			{
				evtFunc 			= 	s_eventFuncPool + i;
				evtFunc->ev_code 	= 	i;
				evtFunc->func       = 	func;
				//evtFunc->servEvt	=	servEvt;
#if EVT_FUNC_NAME_EN
				evtFunc->evtFunc_name	=	func_name;
#endif
				
				break;
			}
		}
		
		/* restore interrupt	*/	
		CPU_CRITICAL_EXIT();	
	}

#endif

	return evtFunc;
}

/*****************************************************************************
*
* Function:  ServEvtFunc_InvokeFunc
*
* Description: invoke event function 
*              
*                                      
* Called from: 	
*
* Requrement:  
*
******************************************************************************/
void ServEvtFunc_InvokeFunc (SERV_EVT_FUNC *servEvtFunc)
{
#if EVT_FUNC_POOL_SIZE>0

	if ( servEvtFunc != NULL )
	{
		uint8 	ev_code;
		
		CPU_SR_ALLOC();
		
		/* disable interrupt	*/
		CPU_CRITICAL_ENTER();

		ev_code	=	servEvtFunc->ev_code;
		//servEvt	=	servEvtFunc->servEvt;
		
		/* restore interrupt	*/	
		CPU_CRITICAL_EXIT();	
		
		if ( ev_code >= SERV_EVT_FUNC_START )
		{
			(void)ServEvt_Post ( Sys_GetServEvt(), ev_code );
		}
	}
#endif	
}

/*****************************************************************************
*
* Function:  ServEvtFunc_Start
*
* Description: start event function service 
*              
*                                      
* Called from: 	
*
* Requrement:  
*
******************************************************************************/
unsigned char ServEvtFunc_Start (SERV_EVT_FUNC *servEvtFunc)
{
	unsigned char ev_code	=0;

#if EVT_FUNC_POOL_SIZE>0

	if ( servEvtFunc != NULL )
	{
		CPU_SR_ALLOC();
		
		/* disable interrupt	*/
		CPU_CRITICAL_ENTER();
		if ( servEvtFunc->ev_code < SERV_EVT_FUNC_START )
		{
			servEvtFunc->ev_code += SERV_EVT_FUNC_START;
		}	

		ev_code	=	servEvtFunc->ev_code;
		
		/* restore interrupt	*/	
		CPU_CRITICAL_EXIT();		
	}
#endif
	return ev_code;
}

/*****************************************************************************
*
* Function:  ServEvtFunc_Stop
*
* Description: stop event function service 
*              
*                                      
* Called from: 	
*
* Requrement:  
*
******************************************************************************/
void ServEvtFunc_Stop	(SERV_EVT_FUNC *servEvtFunc)
{
#if EVT_FUNC_POOL_SIZE>0

	if ( servEvtFunc != NULL )
	{
		/* disable interrupt	*/
		CPU_SR_ALLOC();
		CPU_CRITICAL_ENTER();	
		
		if ( servEvtFunc->ev_code >= SERV_EVT_FUNC_START )
		{
			servEvtFunc->ev_code -= SERV_EVT_FUNC_START;
		}
		
		/* restore interrupt	*/	
		CPU_CRITICAL_EXIT();		
	}	
#endif
}

/*****************************************************************************
*
* Function:  ServEvtFunc_Del
*
* Description: delete event function service 
*              
*                                      
* Called from: 	
*
* Requrement:  
*
******************************************************************************/
void ServEvtFunc_Del (SERV_EVT_FUNC *servEvtFunc)
{
#if EVT_FUNC_POOL_SIZE>0

	if ( servEvtFunc != NULL  )
	{		
		/* disable interrupt	*/
		CPU_SR_ALLOC();
		CPU_CRITICAL_ENTER();
		
		servEvtFunc->ev_code = EVT_FUNC_NOT_USED;
		
		/* restore interrupt	*/	
		CPU_CRITICAL_EXIT();		
	}
#endif
}


/*****************************************************************************
*
* Function:  ServEvtFunc_CallFunc
*
* Description: call by service task 
*              
*                                      
* Called from: 	
*
* Requrement:  
*
******************************************************************************/
void ServEvtFunc_CallFunc (unsigned char ev_code)
{
#if EVT_FUNC_POOL_SIZE>0

	void (*func)(void);
	
	if ( ev_code >= SERV_EVT_FUNC_START  )
	{	
		CPU_SR_ALLOC();

		func = NULL;
		
		/* disable interrupt	*/
		CPU_CRITICAL_ENTER();
			
		for ( uint8 i = 0; i < sizeof(s_eventFuncPool)/sizeof(SERV_EVT_FUNC); i ++ )
		{	
			if ( s_eventFuncPool[i].ev_code == ev_code )
			{
				func =	s_eventFuncPool[i].func;
				break;
			}
		}		

		/* restore interrupt	*/	
		CPU_CRITICAL_EXIT();	

		if ( func != NULL )
		{
			func();
		}		
	}
#endif

}

/*****************************************************************************
 *  Local Function Definitions
 *****************************************************************************/
 
/*****************************************************************************
*
* Function:  
*
* Description: 
*              
*                                      
* Called from: 	
*
* Requrement:  
*
******************************************************************************/


