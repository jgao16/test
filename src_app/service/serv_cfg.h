
/*
    Service config:
    All rights reserved
*/

#ifndef __SERV_CFG_H__
#define __SERV_CFG_H__
/*****************************************************************************
 *  Include Files
 *****************************************************************************/
//#include "std_type.h"
/*****************************************************************************
 *  Global Macro Definitions
 *****************************************************************************/
#define SERV_TM_TICK_PER_SEC			(500u)
#define  SERV_500MS_TICK_PER_SEC        (2u)
#define	SERVER_EVENT_NAME_EN			(0u)
#define	SERVER_EVENT_POOL_SIZE			(8u)
 

#define	EVT_FUNC_NAME_EN				(0u)
#define	EVT_FUNC_POOL_SIZE				(4u)
 
 
#define	TIMER_QUEUE_NAME_EN				(1u)
#define	TIMER_POOL_SIZE					(8u)


#define	SERV_FUNC_NAME_EN						(0u)
#define	SERV_FUNC_POOL_SIZE						(32u)

#define	SERV_FUNC_MP_CYC_FUNC_50MS_SIZE			(0u)
#define	SERV_FUNC_LP_CYC_FUNC_500MS_SIZE		(0u)
#define	SERV_FUNC_REALTIME_CYC_FUNC_500MS_SIZE	(8u)
#define	SERV_FUNC_REALTIME_CYC_FUNC_MS			(500u)

#define WDT_MONITOR_TIMER	(100u)

#define	WDT_SERV_NUM		(12u)
#define	SERV_WDT_NAME_EN	(1u)


/*****************************************************************************
 *  Global Type Declarations
 *****************************************************************************/
/* %<Types> */






#ifndef __SERV_CFG_C__
/*****************************************************************************
 *  Global Data Declarations
 *****************************************************************************/
/* %<CalStructHeader> 	*/
/* %<CalStructContents>	*/
/* %<CalStructTrailer>	*/


/*****************************************************************************
 *  Global Function Declarations
 *****************************************************************************/

 
#endif
#endif


