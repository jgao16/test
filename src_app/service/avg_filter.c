#include "avg_filter.h"

void AVG_FILTER_CreateHandle(avg_filter_handle_t *handle, uint16_t * buf, uint16_t bufLen)
{
    handle->wrIdx = 0;
    handle->isBufFull = false;
    handle->sum = 0;
    handle->bufLen = bufLen;
    handle->buf = buf;
}

uint16_t AVG_FILTER_Feed(avg_filter_handle_t *handle, uint16_t data)
{
    uint16_t ret = 0;

    if (handle->isBufFull)
    {
        handle->sum -= handle->buf[handle->wrIdx];
        handle->sum += data;
        handle->buf[handle->wrIdx] = data;
        ret = (uint16_t)(handle->sum / handle->bufLen);
    }
    else
    {
        handle->sum += data;
        handle->buf[handle->wrIdx] = data;
        ret = (uint16_t)(handle->sum / (handle->wrIdx + 1));
    }

    handle->wrIdx++;

    if (handle->wrIdx >= handle->bufLen)
    {
        handle->wrIdx = 0;
        handle->isBufFull = true;
    }

    return ret;
}
