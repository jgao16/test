
#ifndef __SERV_TIMER_H__
#define __SERV_TIMER_H__
/*****************************************************************************
 *  Include Files
 *****************************************************************************/

#include "serv_evt.h"

/*****************************************************************************
 *  Global Macro Definitions
 *****************************************************************************/
 
//#define	TIMER_QUEUE_NAME_EN		1

//#define	TIMER_POOL_SIZE			(30u)





/*****************************************************************************
 *  Global Type Declarations
 *****************************************************************************/
/* %<Types> */



typedef struct
{
	unsigned char		tmr_code;
	
	unsigned short 		cycTmCnt;
	unsigned short 		Cnt;
	
#if TIMER_QUEUE_NAME_EN
	unsigned char const	*servTmrName;
#endif	
	
	SERV_EVENT 			*servEvt;
	
}SERV_TIMER;


#ifndef __SERV_TIMER_C__
/*****************************************************************************
 *  Global Data Declarations
 *****************************************************************************/
/* %<CalStructHeader> 	*/
/* %<CalStructContents>	*/
/* %<CalStructTrailer>	*/


/*****************************************************************************
 *  Global Function Declarations
 *****************************************************************************/
SERV_TIMER	*ServTmr_Create ( SERV_EVENT *servEvt, unsigned char tmr_code, unsigned char const *const tmr_name );
void		ServTmr_Del(SERV_TIMER *servTmr);
void		ServTmr_Stop(SERV_TIMER *servTmr);
void		ServTmr_StartShort(SERV_TIMER *servTmr, unsigned short timer);
void		ServTmr_StartCyclic(SERV_TIMER *servTmr, unsigned short timer_dly, unsigned short timer_cyc );
void		ServTmr_CycCallTask ( void );

 
#endif
#endif


