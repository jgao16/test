

#hal path: lib
SOURCE_DIRS  +=   hal
INCLUDE_DIRS += -Ihal

#config file path
INCLUDE_DIRS += -IHal/cfg 

SOURCE_DIRS  +=   hal/cpu_core
INCLUDE_DIRS += -Ihal/cpu_core
#cpu cortex_m4 lib
SOURCE_DIRS  +=   hal/cpu_core/ARM-Cortex-M4/IAR 
INCLUDE_DIRS += -IHal/cpu_core/ARM-Cortex-M4/IAR 
SOURCE_DIRS  +=   hal/cpu_core/CMSIS/Include
INCLUDE_DIRS += -Ihal/cpu_core/CMSIS/Include

SOURCE_DIRS  +=   hal/cpu_core/s32k14x
INCLUDE_DIRS += -Ihal/cpu_core/s32k14x

#extern device 
SOURCE_DIRS  +=   hal/ext_dev
INCLUDE_DIRS += -IHal/ext_dev

SOURCE_DIRS  +=   hal/hal_c
INCLUDE_DIRS += -IHal/hal_c
SOURCE_DIRS  +=   hal/os
INCLUDE_DIRS += -IHal/os
SOURCE_DIRS  +=   hal/hw
INCLUDE_DIRS += -IHal/hw

#hal path: devipc
#SOURCE_DIRS  +=   hal/devipc
# INCLUDE_DIRS += -Ihal/devipc
# SOURCE_DIRS  +=   hal/devipc/gen_code
# INCLUDE_DIRS += -Ihal/devipc/gen_code



