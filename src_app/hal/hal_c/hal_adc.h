/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
//hal_adc api
#ifndef HAL_ADC_H_H_
#define HAL_ADC_H_H_

#ifdef __cplusplus
extern "C" {
#endif 

#include "std_type.h"

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
typedef enum
{
#define ADC_DEV_DEF(dev_name, hw_name) dev_name##_ID,
    #include "dev_adc.cfg"
#undef ADC_DEV_DEF
    DEV_NAME_MAX
}ADC_DEV_NAME;



typedef struct
{
    void (*ch_init)         (uint8_t dev_name);                 //adc channel init
    void (*ch_deinit)       (uint8_t dev_name);                 //adc channel deint
    void (*cycle_convert)   (void);                             //cycle convert func, deault 10ms
    void (*get_ch_val)      (uint8_t dev_name, uint16_t *val);  //get channel result
}HAL_ADC_HW_API;

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void HalAdc_ChInit          ( uint8_t dev_name );

void HalAdc_ChDeInit        ( uint8_t dev_name );

void HalAdc_CycleConvert    (void);

void HalAdc_GetChVal        (uint8_t dev_name, uint16_t *val);


#ifdef __cplusplus
}
#endif


#endif
