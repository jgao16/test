
#ifndef HAL_RTC_H__
#define HAL_RTC_H__



/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
typedef struct
{
	unsigned char 	u1_year;		/* 2000 + year */
	unsigned char	u1_month;
	unsigned char	u1_date;
	unsigned char	u1_hour;
	unsigned char	u1_minute;
	unsigned char	u1_second;
}HAL_RTC_TM;

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
typedef struct
{
//	void (*mount)(void const *cfg);	
	boolean (*Init)(void);	// return true if initial success
	void (*GetTm)(HAL_RTC_TM *rtc_tm);
	void (*SetTm)(HAL_RTC_TM *rtc_tm);
	void (*SetIntCalbk)( void(*callbk_isr)(void) );
}HAL_RTC_HW_API;



/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#define RTC_CALLBACK_FUNCTION_NUM	(2u)

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void 			HalRtc_Init(void);
void 			HalRtc_GetTm(HAL_RTC_TM *rtc_tm);
void 			HalRtc_SetTm(HAL_RTC_TM *rtc_tm);
unsigned long 	HalRtc_GetMicrosoftTm(void);
void 			HalRtc_SetCycCallback( void(*callbk_isr)(void) );



#endif