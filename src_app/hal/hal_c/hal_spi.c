/*
********************************************************************************************************
*                                        Hal_SPI
*                           (c) Copyright 2011, kangqiaoren
*                                     All Rights Reserved
*                                         V0.01
*
*********************************************************************************************************
*/

/*
*********************************************************************************************************
*
*                                      Hal_SPI CONFIGURATION
*
* Filename      : Hal_SPI.c
* Version       : V0.01
* Programmer(s) : KangqiaoRen
* Email:		: kangqiaoren@gmail.com
*********************************************************************************************************
* Note(s)       : 
*********************************************************************************************************
*/

/*****************************************************************************
*  Include Files
*****************************************************************************/
#include "std_type.h"

#include "hal_os.h"
#include "hal_hw.h"
#include "hal_spi.h"


/*****************************************************************************
*  Local Macro Definitions
*****************************************************************************/
#define __SPI_VERSION         100      /**< 1.00 */



/*****************************************************************************
*  Local Type Declarations
*****************************************************************************/
/*typedef struct t_SPI_LogicalDeviceToHw_tag {
	uint8 hwChannel;
	uint8 hwDevice;
} SPI_LogicalDeviceToHw;

typedef struct SYS_HW_Channel_tag
{
	SPI_Device * lastHwChannelDevice;             // last used logical device of the HW channel
} HAL_HW_Channel;
*/

/*****************************************************************************
*  Global Variable Definitions (Global for the whole system)
*****************************************************************************/


/*****************************************************************************
*  Local Variable Definitions (Global within this file)
*****************************************************************************/
static HAL_OS_SEM_ID res_sem[HAL_SPI_HW_NAME_MAX];
static SPI_DEVICE  *last_dev[HAL_SPI_HW_NAME_MAX];
static SPI_DEVICE	 spi_dev[SPI_DEV_NAME_MAX];
static uint8 const	dev_map_to_hw[] = 
{
//#undef SPI_DEV_DEF
#define SPI_DEV_DEF(dev_name,spi_hw_name) spi_hw_name##_ID,
	#include "spi_dev.cfg"
#undef SPI_DEV_DEF	
};
static const SPI_TRANS_MOD spi_trans_mod[]=
{
#define CPU_SPI_HW_DEF(spi_hw_name,hw_obj,master,trans,calbk,sck_pt,sck_pin,mosi_pt,mosi_pin,miso_pt,miso_pin,nss_pt,nss_pin,nss_actv)	\
		SPI_##trans,
	#include "cpu_spi.cfg"
#undef CPU_SPI_HW_DEF	

};
static const HAL_SPI_HW_API	*hw_api;
/*****************************************************************************
*  Global Function Declarations
*****************************************************************************/

/*****************************************************************************
*  Local Function Declarations
*****************************************************************************/


/*****************************************************************************/
/*  Global Function Definitions                                              */
/*****************************************************************************/

/*****************************************************************************/
/*                        Initialisation                                     */
/*****************************************************************************/
void	HalSpi_Init(void)
{
	hw_api = HalHw_GetApi(HAL_SPI_HW_ID);

	for ( uint8 i = 0; i < HAL_SPI_HW_NAME_MAX; i ++ )
	{
		res_sem[i] 	= HalOS_SemaphoreCreate(1,"Hal Spi Res Sem");
		last_dev[i] = NULL;
	}
	for ( uint8 i = 0; i < SPI_DEV_NAME_MAX; i ++ )
	{
		spi_dev[i].spi_baud = HAL_SPI_1MBIT;
		spi_dev[i].spi_attr = 0;
	}
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
SPI_DEVICE*	HalSpi_GetDev(uint8 dev_name)
{
	SPI_DEVICE *dev = NULL;
	if ( dev_name < SPI_DEV_NAME_MAX )
	{
		dev = spi_dev+dev_name;
	}
	return dev;
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void 	HalSpi_Com   (uint8 dev_name, const uint16 len, const uint8 *tx_dat, uint8 *rx_dat)
{
	if ( dev_name < SPI_DEV_NAME_MAX && len > 0 )
	{
		SPI_DEVICE *dev = spi_dev+dev_name;
		uint8 	hw_ch 	= dev_map_to_hw[dev_name];
		if ( HalOS_SemaphoreWait(res_sem[hw_ch],HAL_OS_WaitForever) == HAL_OS_SEM_ERR_NONE )
		{	/* lock resource */
		
			if ( last_dev[hw_ch] != dev )
			{
				last_dev[hw_ch] = dev;
				if ( hw_api != NULL && hw_api->init != NULL )
				{	/* reinitial spi controller	*/
					hw_api->init(hw_ch,dev);
				}	
			}
			if ( dev->chip_en != NULL ){
				dev->chip_en();
			}

			if ( hw_api->com != NULL )
			{
				hw_api->com(hw_ch,len,tx_dat,rx_dat);
			}

			if ( spi_trans_mod[hw_ch] == SPI_SYNC )
			{
				if ( dev->chip_dis != NULL ){
					dev->chip_dis();
				}
				
				HalOS_SemaphorePost(res_sem[hw_ch]);
			}
		}
	}
}


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void		HalSpi_MuiltCom(uint8 dev_name, const uint16 msg_num, HAL_SPI_MSG *msg )
{
	(void)msg;
	if ( dev_name < SPI_DEV_NAME_MAX && msg_num > 0 && msg != NULL )
	{
		SPI_DEVICE *dev = spi_dev+dev_name;
		uint8 	hw_ch 	= dev_map_to_hw[dev_name];
		if ( HalOS_SemaphoreWait(res_sem[hw_ch],HAL_OS_WaitForever) == HAL_OS_SEM_ERR_NONE )
		{	/* lock resource */
		
			if ( last_dev[hw_ch] != dev )
			{
				last_dev[hw_ch] = dev;
				if ( hw_api != NULL && hw_api->init != NULL )
				{	/* reinitial spi controller	*/
					hw_api->init(hw_ch,dev);
				}	
			}
			if ( dev->chip_en != NULL ){
				dev->chip_en();
			}
			
			if ( hw_api->com != NULL )
			{
				for ( uint16 i = 0; i < msg_num; i ++ )
				{
					hw_api->com(hw_ch,(msg+i)->len,(msg+i)->tx_dat,(msg+i)->rx_dat);
				}			
			}

			if ( dev->chip_dis != NULL ){
				dev->chip_dis();
			}
			
			HalOS_SemaphorePost(res_sem[hw_ch]);
		}
	}
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void 	HalSpi_ComExt(SPI_DEVICE *dev,uint8 hw_ch, const uint16 len,const uint8 *tx_dat,uint8 *rx_dat)
{
	if ( spi_dev != NULL && hw_ch < HAL_SPI_HW_NAME_MAX && len > 0 )
	{
		if ( HalOS_SemaphoreWait(res_sem[hw_ch],HAL_OS_WaitForever) == HAL_OS_SEM_ERR_NONE )
		{	/* lock resource */
		
			if ( last_dev[hw_ch] != dev )
			{
				last_dev[hw_ch] = dev;
				if ( hw_api != NULL && hw_api->init != NULL )
				{	/* reinitial spi controller	*/
					hw_api->init(hw_ch,dev);
				}	
			}
			if ( dev->chip_en != NULL ){
				dev->chip_en();
			}

			if ( hw_api->com != NULL )
			{
				hw_api->com(hw_ch,len,tx_dat,rx_dat);
			}

			if ( dev->chip_dis != NULL ){
				dev->chip_dis();
			}
			
			HalOS_SemaphorePost(res_sem[hw_ch]);
		}
	}
}


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void 	HalSpi_HwComOk(uint8 hw_ch)
{
	SPI_DEVICE  *dev = last_dev[hw_ch];

	if ( spi_trans_mod[hw_ch] == SPI_ASYNC )
	{
		if ( dev->chip_dis != NULL )
		{
			dev->chip_dis();
		}
		HalOS_SemaphorePost(res_sem[hw_ch]);	
		
		if ( dev->trans_calbk != NULL )
		{
			dev->trans_calbk();
		}
	}
}

/*****************************************************************************/
/*                        Version                                            */
/*****************************************************************************/
/** \fn     uint32 SPI_GetVersion(void)
*  \brief  Versionsinfo
*
*  \return Versionsnummer
*/
uint32	HalSpi_GetVer(void)
{

	/* ReturnValue */
	return (__SPI_VERSION);
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
//void HalSpi_HwDeInit(uint8 dev_name)
//{
//	if ( dev_name < SPI_DEV_NAME_MAX )
//	{
//		SPI_DEVICE *dev = spi_dev+dev_name;
//		uint8 	hw_ch 	= dev_map_to_hw[dev_name];
//		if ( HalOS_SemaphoreWait(res_sem[hw_ch],HAL_OS_WaitForever) == HAL_OS_SEM_ERR_NONE )
//		{	/* lock resource */
//			if ( last_dev[hw_ch] == dev )
//			{
//				last_dev[hw_ch] = NULL;
//				if ( hw_api != NULL && hw_api->deinit != NULL )
//				{	/* reinitial spi controller	*/
//					hw_api->deinit(hw_ch);
//				}
//			}
//			HalOS_SemaphorePost(res_sem[hw_ch]);
//		}
//	}
//}

/*****************************************************************************
*  Local Function Definitions
*****************************************************************************/

