#ifndef __Hal_IIC_SLV_H_
#define __Hal_IIC_SLV_H_

#ifdef __cplusplus
extern "C" {
#endif

/*****************************************************************************
*  Include Files
*****************************************************************************/

#include "Std_type.h"
/*****************************************************************************
*  Global Macro Definitions
*****************************************************************************/

typedef enum
{
#undef  IIC_SLV_DEV_DEF
#define IIC_SLV_DEV_DEF(dev_name,iic_hw_name,addr) dev_name##_ID,
    #include "dev_i2c_slv.cfg"
    IIC_SLV_DEV_NAME_MAX,
#undef  IIC_SLV_DEV_DEF
} IIC_SLV_DEV_NAME;

/*****************************************************************************
*  Global Type Declarations
*****************************************************************************/

typedef enum
{
    IIC_SLV_IDLE,
    IIC_SLV_READ,
    IIC_SLV_WRITE,
    IIC_SLV_STATE_MAX
} IIC_SLV_STATE;

typedef enum
{
    IIC_SLV_READ_DONE,
    IIC_SLV_WRITE_DONE,
    IIC_SLV_EVENT_MAX
} IIC_SLV_EVENT;

/* Callback when all TX or RX done (stop received or repeat-start received) */
typedef void (*iic_slv_callback_t)(IIC_SLV_EVENT, const uint8_t *data, uint16_t rx_data_len);

typedef struct
{
    void          (*init)(uint8_t hw_name, uint8_t *rx_buff, uint16_t rx_buff_len, iic_slv_callback_t cb);
    void          (*set_tx_data)(uint8_t hw_name, uint8_t *data, uint16_t data_len);
    IIC_SLV_STATE (*get_state)(uint8_t hw_name);
} HAL_IIC_SLV_HW_API;

typedef struct
{
    uint8_t    map_to_hw_name;
} IIC_SLV_DEV_ATTR;

/*****************************************************************************
*  Global Data Declarations
*****************************************************************************/

/*****************************************************************************
*  Global Function Declarations
*****************************************************************************/

uint32_t HalIic_SLV_GetVer(void);
void * HalIic_SLV_GetDev(uint8_t const dev_name);

void  HalIic_SLV_Init(void *iic, uint8_t *rx_buff, uint16_t rx_buff_len, iic_slv_callback_t cb);
void HalIic_SLV_SetTxData(void const *iic, uint8_t *data, uint16_t data_len);
IIC_SLV_STATE HalIic_SLV_GetState(void const *iic);

#ifdef __cplusplus
}
#endif

#endif
