/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
//hal_adc
#include "std_type.h" 
#include "hal_hw.h"
#include "hal_adc.h"

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static const uint8_t adc_dev_map_to_hw[] = 
{
#define ADC_DEV_DEF(dev_name, hw_name) hw_name##_ID,
    #include "dev_adc.cfg"
#undef ADC_DEV_DEF
};

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static HAL_ADC_HW_API const *api;

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void HalAdc_ChInit ( uint8_t dev_name )
{
	api = HalHw_GetApi(HAL_ADC_HW_ID);
	if ( (NULL != api) && (NULL != api->ch_init) )
	{
		uint8_t hw_ch = adc_dev_map_to_hw[dev_name];
		api->ch_init(hw_ch);
	}
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void HalAdc_ChDeInit ( uint8_t dev_name )
{
	if ( (NULL != api) && (NULL != api->ch_deinit) )
	{
		uint8_t hw_ch = adc_dev_map_to_hw[dev_name];
		api->ch_deinit(hw_ch);
	}
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void HalAdc_CycleConvert(void)
{
	if ( (NULL != api) && (NULL != api->cycle_convert) )
	{
		api->cycle_convert();
	}
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void HalAdc_GetChVal(uint8_t dev_name, uint16_t *val)
{
	if ( (NULL != api) && (NULL != api->get_ch_val) )
	{
		uint8_t hw_ch = adc_dev_map_to_hw[dev_name];
		api->get_ch_val(hw_ch, val);
	}
}

