
#ifndef _HAL_CLK_H_
#define _HAL_CLK_H_
#include "stdint.h"

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void 	 HalClk_CpuClkInit(void);
uint32_t HalClk_GetCpuFreq(void);
//void			BSP_SetSysclkPLL ( void );



#endif

