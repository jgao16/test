
#include "std_type.h"

#include "hal_uart.h"
#include "hal_hw.h"

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

static HAL_UART_HW_API const * hw_api;

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void HalUart_Init ( void )
{
	hw_api = HalHw_GetApi(HAL_UART_HW_ID);
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void *	HalUart_GetDev(uint8 uart_name)
{
	if ( hw_api != NULL && hw_api->get_dev != NULL  )
	{
		return hw_api->get_dev(uart_name);
	}

	return NULL;
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void   HalUart_InitDev(void const * hdl)
{
	if ( hw_api != NULL && hw_api->init_dev != NULL  )
	{
		hw_api->init_dev(hdl);
	}
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void   HalUart_DeInitDev(void const * hdl)
{
	if ( hw_api != NULL && hw_api->deinit_dev != NULL  )
	{
		hw_api->deinit_dev(hdl);
	}	
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void   HalUart_Send(void const*hdl, unsigned char const* tx_dat,unsigned short const len)
{
	if ( hw_api != NULL && hw_api->send != NULL  )
	{
		hw_api->send(hdl,tx_dat,len);
	}		
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void HalUart_CycCall(void)
{
	if ( hw_api != NULL && hw_api->cyc_call != NULL  )
	{
		hw_api->cyc_call();
	}
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
boolean 	HalUart_IsRxProcess(void const*hdl)
{
	if ( hw_api != NULL && hw_api->is_rx_process != NULL  )
	{
		return hw_api->is_rx_process(hdl);
	}	
	return false;
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void 	HalUart_SetParameter (void *hdl,HAL_UART_BAUD baud,HAL_UART_PARITY_CHECK parityChk,HAL_UART_WORD_LENS wordLens,HAL_UART_STOP_BITS stopBits)
{
	if ( hw_api != NULL && hw_api->set_parameter != NULL  )
	{
		hw_api->set_parameter(hdl,baud,parityChk,wordLens,stopBits);
	}	
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void 	HalUart_SetPreDefBaud(void *hdl,HAL_UART_BAUD baud)
{
	if ( hw_api != NULL && hw_api->set_predef_baud != NULL  )
	{
		hw_api->set_predef_baud(hdl,baud);
	}	
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void 	HalUart_SetUserBaud (void *hdl,uint16 usr_baud)	//usr_baud = baud/100
{
	if ( hw_api != NULL && hw_api->set_user_baud != NULL  )
	{
		hw_api->set_user_baud(hdl,usr_baud);
	}		
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void 	HalUart_SetCalBk (	void *hdl,
							void * arg,
							void (*rx_clbak) (void*arg,unsigned char *rx_dat, unsigned short len),
							void (*tx_clbak) (void*arg),
							void (*err_clbak)(void*arg),
							void (*cyc_clbak)(void*arg),
							void (*wakeup_clbak)(void*arg) )
{
	if ( hw_api != NULL && hw_api->set_calbk != NULL  )
	{
		hw_api->set_calbk(hdl,arg,rx_clbak,tx_clbak,err_clbak,cyc_clbak,wakeup_clbak);
	}	
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
uint16 HalUart_GetBaud(void *hdl)	//return baud = baud/100
{
	if ( hw_api != NULL && hw_api->get_baud != NULL  )
	{
		return hw_api->get_baud(hdl);
	}	
	return 0;
}


