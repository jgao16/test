/*****************************************************************************
*  Include Files
*****************************************************************************/
#include "std_type.h"

#include "hal_os.h"

#include "hal_hw.h"
#include "hal_iic_slv.h"
#include "dev_i2c.cfg"
/*****************************************************************************
*  Local Macro Definitions
*****************************************************************************/
#define  __IIC_SLV_VERSION     (100u)      /* v1.00 */

/*****************************************************************************
*  Local Type Declarations
*****************************************************************************/

/*****************************************************************************
*  Global Variable Definitions (Global for the whole system)
*****************************************************************************/

/*****************************************************************************
*  Local Variable Definitions (Global within this file)
*****************************************************************************/
/* Must be defined in the Hal_SPI_cfg.h */
static const IIC_SLV_DEV_ATTR iic_slv_dev_attr[] =
{
#undef  IIC_SLV_DEV_DEF
#define IIC_SLV_DEV_DEF(dev_name,iic_hw_name,addr) {iic_hw_name##_ID},
    #include "dev_i2c_slv.cfg"
#undef  IIC_SLV_DEV_DEF
};

static const HAL_IIC_SLV_HW_API * hw_api;
/*****************************************************************************
*  Global Function Declarations
*****************************************************************************/

/*****************************************************************************
*  Local Function Declarations
*****************************************************************************/


/*****************************************************************************/
/*  Global Function Definitions                                              */
/*****************************************************************************/

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void  HalIic_SLV_Init(void *iic, uint8_t *rx_buff, uint16_t rx_buff_len, iic_slv_callback_t cb)
{
	hw_api = HalHw_GetApi(HAL_IIC_SLV_HW_ID);

    IIC_SLV_DEV_ATTR *dev = (IIC_SLV_DEV_ATTR*)iic;
    hw_api->init(dev->map_to_hw_name, rx_buff, rx_buff_len, cb);
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
IIC_SLV_STATE HalIic_SLV_GetState(void const *iic)
{
    IIC_SLV_DEV_ATTR *dev = (IIC_SLV_DEV_ATTR*)iic;
    return hw_api->get_state(dev->map_to_hw_name);
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void HalIic_SLV_SetTxData(void const *iic, uint8_t *data, uint16_t data_len)
{
    IIC_SLV_DEV_ATTR *dev = (IIC_SLV_DEV_ATTR*)iic;
    hw_api->set_tx_data(dev->map_to_hw_name, data, data_len);
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void * HalIic_SLV_GetDev(uint8_t const dev_name)
{
    if ( dev_name< IIC_SLV_DEV_NAME_MAX ){
        return (void*)(iic_slv_dev_attr + dev_name);
    }else{
        return NULL;
    }
}

/*****************************************************************************/
/*                        Version                                            */
/*****************************************************************************/
uint32_t HalIic_SLV_GetVer(void)
{
    return __IIC_SLV_VERSION;
}
