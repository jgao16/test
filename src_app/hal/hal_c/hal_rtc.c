
#include "std_type.h"

#include"hal_rtc.h"

#include"cpu.h"
#include "hal_hw.h"
#include "hal_os.h"

#include "utc2local.h"
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#define RTC_TM_ZONE		8u 

#define RTC_SYNC_HW_TM	120u	// read hw every 200s

#define	DIFF_ABS(x,y) 	((x)>=(y)?((x)-(y)):((y)-(x)))

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
typedef		void (*CALLBACK_FUNC)	(void);

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

static  uint8					rtc_int_cnt;
static  uint32 					utc_tm;
static  uint32					utc_old;

static 	HAL_RTC_HW_API const *	drv_rtc_api;
static	CALLBACK_FUNC			wakeup_callback[RTC_CALLBACK_FUNCTION_NUM]; 
static  HAL_OS_SEM_ID 			rtc_sem;

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void HalRtc_wakeup_isr(void);

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void HalRtc_Init(void)
{	
	drv_rtc_api = HalHw_GetApi(HAL_RTC_HW_ID);
	
	unsigned char init = FALSE;
	if ( drv_rtc_api != NULL )
	{
		init = drv_rtc_api->Init();
	}

	HAL_RTC_TM rtc_tm;
	if ( init )
	{
		/* get time			*/
		drv_rtc_api->GetTm(&rtc_tm);		
	}
	else
	{
		rtc_tm.u1_year	 	= 	16;
		rtc_tm.u1_month		=	1;
		rtc_tm.u1_date		=	1;
		rtc_tm.u1_hour		=	0;
		rtc_tm.u1_minute	=	0;
		rtc_tm.u1_second	=	0;
	}
	LOCAL_TIME tm;
	tm.Year 	= rtc_tm.u1_year + 2000;
	tm.Month 	= rtc_tm.u1_month;
	tm.Date 	= rtc_tm.u1_date;
	tm.Hour 	= rtc_tm.u1_hour;
	tm.Minute 	= rtc_tm.u1_minute;
	tm.Second 	= rtc_tm.u1_second;

	/* change to local utc 	*/
	utc_tm = LocalTime2Utc(&tm, RTC_TM_ZONE);

	/* set wakeup interrupt routine */
	if ( drv_rtc_api != NULL )
	{
		drv_rtc_api->SetIntCalbk(HalRtc_wakeup_isr);
	}

	rtc_sem    = HalOS_SemaphoreCreate(1, "rtc sem.");	
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void HalRtc_GetTm(HAL_RTC_TM *const rtc_tm)
{
	uint32 utc_tmp;

	CPU_SR_ALLOC();
	CPU_CRITICAL_ENTER();	/* disable interrupt	*/
	utc_tmp = utc_tm;
	CPU_CRITICAL_EXIT();

	if ( DIFF_ABS(utc_tmp,utc_old) > RTC_SYNC_HW_TM )
	{
		if ( drv_rtc_api != NULL 
		  && HalOS_SemaphoreWait(rtc_sem, HAL_OS_WaitForever) == HAL_OS_SEM_ERR_NONE )
		{
			/* get time			*/
			drv_rtc_api->GetTm(rtc_tm);	

			HalOS_SemaphorePost(rtc_sem);

			LOCAL_TIME tm;
			tm.Year 	= rtc_tm->u1_year + 2000;
			tm.Month 	= rtc_tm->u1_month;
			tm.Date 	= rtc_tm->u1_date;
			tm.Hour 	= rtc_tm->u1_hour;
			tm.Minute 	= rtc_tm->u1_minute;
			tm.Second 	= rtc_tm->u1_second;
			/* change to local utc 	*/
			utc_tmp = LocalTime2Utc(&tm, RTC_TM_ZONE);
			CPU_CRITICAL_ENTER();	/* disable interrupt	*/
			utc_tm = utc_tmp;
			CPU_CRITICAL_EXIT();
		}
		utc_old = utc_tmp;
	}
	else
	{
		LOCAL_TIME tm;
		
		Utc2LocalTime(utc_tmp, &tm, RTC_TM_ZONE);

		if ( tm.Year >= 2000 ){
			rtc_tm->u1_year = (uint8)(tm.Year-2000);
		}else{
			rtc_tm->u1_year = 0x00;
		}
		rtc_tm->u1_month	= tm.Month;
		rtc_tm->u1_date   	= tm.Date;
		rtc_tm->u1_hour		= tm.Hour;
		rtc_tm->u1_minute 	= tm.Minute;
		rtc_tm->u1_second 	= tm.Second;		
	}
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void HalRtc_SetTm(HAL_RTC_TM *rtc_tm)
{
	if ( drv_rtc_api != NULL 
	  && HalOS_SemaphoreWait(rtc_sem, HAL_OS_WaitForever) == HAL_OS_SEM_ERR_NONE )
	{
		drv_rtc_api->SetTm(rtc_tm);
		
		LOCAL_TIME tm;
		tm.Year 	= rtc_tm->u1_year + 2000;
		tm.Month 	= rtc_tm->u1_month;
		tm.Date 	= rtc_tm->u1_date;
		tm.Hour 	= rtc_tm->u1_hour;
		tm.Minute 	= rtc_tm->u1_minute;
		tm.Second 	= rtc_tm->u1_second;
		/* change to local utc 	*/
		uint32 utc_tmp = LocalTime2Utc(&tm, RTC_TM_ZONE);
	
		CPU_SR_ALLOC();
		/* disable interrupt	*/
		CPU_CRITICAL_ENTER();
		utc_tm = utc_tmp;
		/* restore interrupt	*/	
		CPU_CRITICAL_EXIT();
	
		HalOS_SemaphorePost(rtc_sem);
	}
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
uint32 HalRtc_GetLinuxTm(void)
{
	return utc_tm;
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
unsigned long HalRtc_GetMicrosoftTm(void)
{

	HAL_RTC_TM rtc_tm;
	HalRtc_GetTm(&rtc_tm);
	
	unsigned long ms_tm = 0;
	ms_tm +=	((uint32)(rtc_tm.u1_year+20)<<25);
	ms_tm += 	((uint32)(rtc_tm.u1_month)<<21);
	ms_tm += 	((uint32)(rtc_tm.u1_date)<<16);	
	ms_tm += 	((uint32)(rtc_tm.u1_hour)<<11);	
	ms_tm += 	((uint32)(rtc_tm.u1_minute)<<5);	
	ms_tm += 	((uint32)(rtc_tm.u1_second)/2);	
	
	return ms_tm;
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void HalRtc_SetCycCallback( void(*callbk_isr)(void) )
{
	CPU_SR_ALLOC();
	/* disable interrupt	*/
	CPU_CRITICAL_ENTER();
	for ( unsigned char i=0; i < RTC_CALLBACK_FUNCTION_NUM; i ++ )
	{
		if ( wakeup_callback[i] == NULL )
		{
			wakeup_callback[i] = callbk_isr;
			break;
		}
	}
	CPU_CRITICAL_EXIT();
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void HalRtc_wakeup_isr(void)
{
	CPU_SR_ALLOC();
	/* disable interrupt	*/
	CPU_CRITICAL_ENTER();
	
	rtc_int_cnt ++;
	if ( rtc_int_cnt & 0x01 )
	{
		utc_tm++;
	}
		
	/* restore interrupt	*/	
	CPU_CRITICAL_EXIT();
	
	for ( unsigned char i=0; i < RTC_CALLBACK_FUNCTION_NUM; i ++ )
	{
		CALLBACK_FUNC func;
		CPU_CRITICAL_ENTER();
		func = wakeup_callback[i];
		/* restore interrupt	*/	
		CPU_CRITICAL_EXIT();
	
		if ( func != NULL )
		{
			func();
		}
	}
}


