/******************************************************************************
 * Project          s32k14x platform
 * (c) copyright    2015
 * Company          Visteon
 *                  All rights reserved
 * creation Date    2015-03 
 ******************************************************************************/


#ifndef HAL_EEP_H_H_
#define HAL_EEP_H_H_

#ifdef __cplusplus
extern "C" {
#endif 

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
typedef struct
{
	void 	(*init)	(void);
	uint32 	(*read)	(uint32 const offset, uint8 *const buf, uint32 const num);
	uint32 	(*write)(uint32 const offset, uint8 const* buf, uint32 const num);
	boolean	(*is_busy)(void);
}HAL_EEP_HW_API;


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void 	HalEep_Init	(void);
uint32 	HalEep_Read	(uint32 const offset, uint8 * const buf, uint32 const num);
uint32 	HalEep_Write(uint32 const offset, uint8 const * buf, uint32 const num);
boolean HalEep_IsBusy(void);



#ifdef __cplusplus
}
#endif


#endif
