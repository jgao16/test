

#ifndef HAL_INT_H__
#define	HAL_INT_H__


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

#include "hal_cpu_int.h"

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

void  	HalInt_Init (void);

void  	HalInt_DisAll (void);

void  	HalInt_En ( uint8_t int_id);
void 	HalInt_Dis (uint8_t  int_id);
/* instal interrupt function to hal 	*/
void  	HalInt_VectSet (uint8_t int_id,void(*isr)(void) );
/* interrupt func for cpu interrupt call*/
void	HalInt_Handler (uint8_t  int_id);
/* set interrupt prio					*/
void  	HalInt_PrioSet (int16_t int_id, uint32_t prio);

void	PendSVHandler_Occure(void);
void	SysTickHandler_Occure(void);


#endif

