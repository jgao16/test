
/*
*********************************************************************************************************
*                                             INCLUDE FILES
*********************************************************************************************************
*/
#include "std_type.h"
#include "std_lib.h"
#include "cpu.h"
#include "hal_int.h"
#include "trace_api.h"
//lint -e734
/*
*********************************************************************************************************
*                                            LOCAL DEFINES
*********************************************************************************************************
*/
#define		INT_MEASURE_OCCURED_EN	(TRUE) 

#if LOG_ENABLE == 0
#undef 	INT_MEASURE_OCCURED_EN
#define INT_MEASURE_OCCURED_EN FALSE
#endif

/*
*********************************************************************************************************
*                                           LOCAL CONSTANTS
*********************************************************************************************************
*/


/*
*********************************************************************************************************
*                                          LOCAL DATA TYPES
*********************************************************************************************************
*/


/*
*********************************************************************************************************
*                                            LOCAL TABLES
*********************************************************************************************************
*/

static  CPU_FNCT_VOID  Hal_IntVectTbl[HAL_INT_ID_MAX];


/*
*********************************************************************************************************
*                                       LOCAL GLOBAL VARIABLES
*********************************************************************************************************
*/
#if INT_MEASURE_OCCURED_EN
static uint8_t  tick_cnt;
static uint16_t tick_1s;
static uint32_t cpu_int_cnt[10];
#endif
//static uint32_t	cpu_int_cnt_per_1s;
/*
*********************************************************************************************************
*                                      LOCAL FUNCTION PROTOTYPES
*********************************************************************************************************
*/

static  void  HalInt_HandlerDummy(void);


/*
*********************************************************************************************************
*                                     LOCAL CONFIGURATION ERRORS
*********************************************************************************************************
*/


/*
*********************************************************************************************************
*                                              BSP_IntClr()
*
* Description : Clear interrupt.
*
* Argument(s) : int_id      Interrupt to clear.
*
* Return(s)   : none.
*
* Caller(s)   : Application.
*
* Note(s)     : (1) An interrupt does not need to be cleared within the interrupt controller.
*********************************************************************************************************
*/

void  HalInt_Clr ( uint8  int_id)
{
	if ( int_id < HAL_INT_ID_MAX )
	{
        HalInt_VectSet(int_id, HalInt_HandlerDummy);
	}
}


/*
*********************************************************************************************************
*                                              BSP_IntDis()
*
* Description : Disable interrupt.
*
* Argument(s) : int_id      Interrupt to disable.
*
* Return(s)   : none.
*
* Caller(s)   : Application.
*
* Note(s)     : none.
*********************************************************************************************************
*/

void HalInt_Dis ( uint8  int_id)
{
    if ( int_id < HAL_INT_ID_MAX ) {
    	NVIC_DisableIRQ((IRQn_Type)int_id);
        //CPU_IntSrcDis(int_id);
    }
}


/*
*********************************************************************************************************
*                                           BSP_IntDisAll()
*
* Description : Disable ALL interrupts.
*
* Argument(s) : none.
*
* Return(s)   : none.
*
* Caller(s)   : Application.
*
* Note(s)     : none.
*********************************************************************************************************
*/

void  HalInt_DisAll (void)
{
    CPU_IntDis();
}


/*
*********************************************************************************************************
*                                               BSP_IntEn()
*
* Description : Enable interrupt.
*
* Argument(s) : int_id      Interrupt to enable.
*
* Return(s)   : none.
*
* Caller(s)   : Application.
*
* Note(s)     : none.
*********************************************************************************************************
*/

void  HalInt_En ( uint8  int_id)
{
    if ( int_id < HAL_INT_ID_MAX ) {
    	NVIC_EnableIRQ((IRQn_Type)int_id);
        //CPU_IntSrcEn(int_id + 16);
    }
}


/*
*********************************************************************************************************
*                                            Hal_IntVectSet()
*
* Description : Assign ISR handler.
*
* Argument(s) : int_id      Interrupt for which vector will be set.
*
*               isr         Handler to assign
*
* Return(s)   : none.
*
* Caller(s)   : Application.
*
* Note(s)     : none.
*********************************************************************************************************
*/

void  HalInt_VectSet (uint8       int_id,
                      CPU_FNCT_VOID  isr)
{
    if (int_id < HAL_INT_ID_MAX) {
        Hal_IntVectTbl[int_id] = isr;
    }
}


/*
*********************************************************************************************************
*                                            BSP_IntPrioSet()
*
* Description : Assign ISR priority.
*
* Argument(s) : int_id      Interrupt for which vector will be set.
*
*               prio        Priority to assign
*
* Return(s)   : none.
*
* Caller(s)   : Application.
*
* Note(s)     : none.
*********************************************************************************************************
*/

void  HalInt_PrioSet ( int16_t int_id, uint32_t  prio )
{
    if ( int_id < HAL_INT_ID_MAX && int_id>=-16 ) {
        NVIC_SetPriority((IRQn_Type)int_id,prio);
    }
}


/*
*********************************************************************************************************
*********************************************************************************************************
*                                           INTERNAL FUNCTIONS
*********************************************************************************************************
*********************************************************************************************************
*/

/*
*********************************************************************************************************
*                                              BSP_IntInit()
*
* Description : Initialize interrupts:
*
* Argument(s) : none.
*
* Return(s)   : none.
*
* Caller(s)   : BSP_InitHp().
*
* Note(s)     : none.
*********************************************************************************************************
*/

void  HalInt_Init (void)
{
    uint8_t  int_id;

    for (int_id = 0; int_id < HAL_INT_ID_MAX; int_id++) {
        HalInt_VectSet(int_id, HalInt_HandlerDummy);
        HalInt_PrioSet(int_id,15);
    }

	HalInt_PrioSet(PendSV_IRQn,15);				/**< Cortex-M4 Pend SV Interrupt */
	HalInt_PrioSet(SysTick_IRQn,15);			/**< Cortex-M4 System Tick Interrupt */
}


/*
*********************************************************************************************************
*                                          HalInt_Handler()
*
* Description : Central interrupt handler.
*
* Argument(s) : int_id          Interrupt that will be handled.
*
* Return(s)   : none.
*
* Caller(s)   : ISR handlers.
*
* Note(s)     : none.
*********************************************************************************************************
*/

void  HalInt_Handler (uint8_t  int_id)
{
    CPU_FNCT_VOID  isr;
    
#if INT_MEASURE_OCCURED_EN
	cpu_int_cnt[9]++;
#endif

    if (int_id < HAL_INT_ID_MAX) {
        isr = Hal_IntVectTbl[int_id];
        if (isr != (CPU_FNCT_VOID)0) {
            isr();
        }
    }
}


void	PendSVHandler_Occure(void)
{
#if INT_MEASURE_OCCURED_EN
	cpu_int_cnt[9]++;
#endif	
}

void	SysTickHandler_Occure(void)
{
#if INT_MEASURE_OCCURED_EN

	if ( MOD_HW_Lvl >= LOG_IO )
	{
		cpu_int_cnt[9] ++;

		tick_1s ++;
		tick_cnt ++;
		if ( tick_cnt > 99 )
		{
			tick_cnt 		= 0x00;

			if ( tick_1s > 999 )
			{
				tick_1s -= 1000;
				uint32_t int_cnt = cpu_int_cnt[0] + cpu_int_cnt[1] + cpu_int_cnt[2] + cpu_int_cnt[3] 
								+  cpu_int_cnt[4] + cpu_int_cnt[5] + cpu_int_cnt[6] + cpu_int_cnt[7] 
								+  cpu_int_cnt[8] + cpu_int_cnt[9];
				TRACE_VALUE(LOG_IO, MOD_HW, "interrupt occured %d times every 1 second", int_cnt);
			}
			//TRACE_VALUE(LOG_RAW, MOD_HW,"interrupt occured %d times at last 100ms", cpu_int_cnt[9]);
			
			cpu_int_cnt[0]	=	cpu_int_cnt[1];
			cpu_int_cnt[1]	=	cpu_int_cnt[2];
			cpu_int_cnt[2]	=	cpu_int_cnt[3];
			cpu_int_cnt[3]	=	cpu_int_cnt[4];
			cpu_int_cnt[4]	=	cpu_int_cnt[5];
			cpu_int_cnt[5]	=	cpu_int_cnt[6];
			cpu_int_cnt[6]	=	cpu_int_cnt[7];
			cpu_int_cnt[7]	=	cpu_int_cnt[8];
			cpu_int_cnt[8]	=	cpu_int_cnt[9];
			cpu_int_cnt[9] 	= 	0x00;	
		}	
	}
#endif	
}
/*
*********************************************************************************************************
*********************************************************************************************************
*                                           LOCAL FUNCTIONS
*********************************************************************************************************
*********************************************************************************************************
*/




/*
*********************************************************************************************************
*                                        HalInt_HandlerDummy()
*
* Description : Dummy interrupt handler.
*
* Argument(s) : none.
*
* Return(s)   : none.
*
* Caller(s)   : HalInt_Handler().
*
* Note(s)     : none.
*********************************************************************************************************
*/

static  void  HalInt_HandlerDummy (void)
{

}

