

#ifndef HAL_UART_H__
#define HAL_UART_H__

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#define RS232 0

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
typedef enum
{
	HAL_UART_BAUD_600 = 0,
	HAL_UART_BAUD_1200,
	HAL_UART_BAUD_2400,
	HAL_UART_BAUD_4800,
	HAL_UART_BAUD_9600,
	HAL_UART_BAUD_19200,
	HAL_UART_BAUD_38400,
	HAL_UART_BAUD_57600,
	HAL_UART_BAUD_115200,
	HAL_UART_BAUD_230400,
	HAL_UART_BAUD_1000000,
	HAL_UART_BAUD_USER_DEF,
	HAL_UART_BAUD_MAX,
	
}HAL_UART_BAUD;

typedef enum
{
	HAL_UART_PARITY_NONE = 0,
	HAL_UART_PARITY_ODD,
	HAL_UART_PARITY_EVEN,
}HAL_UART_PARITY_CHECK;

typedef enum
{
	HAL_UART_WORD_8_BITS = 0,
	HAL_UART_WORD_7_BITS,
	HAL_UART_WORD_9_BITS,
}HAL_UART_WORD_LENS;
typedef enum
{
	HAL_UART_STOP_1_BIT = 0,
	HAL_UART_STOP_2_BITS,
}HAL_UART_STOP_BITS;


  
typedef struct
{	
//	void 	(*mount)            (void const*cfg);	// must be invoke before initial
	void 	(*cyc_call)			(void);
	
	void *	(*get_dev)			(uint8 uart_name);
	void 	(*init_dev)			(void const * hdl);
	void   	(*deinit_dev)		(void const * hdl);
	void   	(*send)				(void const*hdl, uint8 const* tx_dat,uint16 const len);
	boolean (*is_rx_process)	(void const*hdl);
	void 	(*set_parameter) 	(void *hdl,HAL_UART_BAUD baud,HAL_UART_PARITY_CHECK parityChk,HAL_UART_WORD_LENS wordLens,HAL_UART_STOP_BITS stopBits);
	void 	(*set_predef_baud)	(void *hdl,HAL_UART_BAUD baud);
	void 	(*set_user_baud) 	(void *hdl,uint16 usr_baud);	//usr_baud = baud/100
	void 	(*set_calbk) 		(void *hdl,
								void * arg,
								void (*rx_clbak) (void*arg,unsigned char *rx_dat, unsigned short len),
								void (*tx_clbak) (void*arg),
								void (*err_clbak)(void*arg),
								void (*cyc_clbak)(void*arg),
								void (*wakeup_clbak)(void*arg) );
	uint16 (*get_baud)(void *hdl);	//return baud = baud/100
}HAL_UART_HW_API;
 

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void 	HalUart_Init ( void );
void *  HalUart_GetDev(uint8 uart_name);
void    HalUart_InitDev(void const * hdl);
void    HalUart_DeInitDev(void const * hdl);
void    HalUart_Send(void const*hdl, unsigned char const* tx_dat,unsigned short const len);
void 	HalUart_CycCall(void);
boolean HalUart_IsRxProcess(void const*hdl);
void 	HalUart_SetParameter (void *hdl,HAL_UART_BAUD baud,HAL_UART_PARITY_CHECK parityChk,HAL_UART_WORD_LENS wordLens,HAL_UART_STOP_BITS stopBits);
void 	HalUart_SetPreDefBaud(void *hdl,HAL_UART_BAUD baud);
void 	HalUart_SetUserBaud (void *hdl,uint16 usr_baud);	//usr_baud = baud/100
void 	HalUart_SetCalBk (	void *hdl,
							void * arg,
							void (*rx_clbak) (void*arg,unsigned char *rx_dat, unsigned short len),
							void (*tx_clbak) (void*arg),
							void (*err_clbak)(void*arg),
							void (*cyc_clbak)(void*arg),
							void (*wakeup_clbak)(void*arg) );
uint16 HalUart_GetBaud(void *hdl);	//return baud = baud/100

#endif


