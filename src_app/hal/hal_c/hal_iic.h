/*
********************************************************************************************************
*                                        Hal_SPI
*                           (c) Copyright 2011, kangqiaoren
*                                     All Rights Reserved
*                                         V0.01
*
*********************************************************************************************************
*/

/*
*********************************************************************************************************
*
*                                      Hal SPI CONFIGURATION
*
* Filename      : Hal_SPI.h
* Version       : V0.01
* Programmer(s) : KangqiaoRen
* Email:		: kangqiaoren@gmail.com
*********************************************************************************************************
* Note(s)       : 
*********************************************************************************************************
*/

#ifndef __Hal_IIC_H_
#define __Hal_IIC_H_

#ifdef __cplusplus
extern "C" {
#endif

/*****************************************************************************
*  Include Files
*****************************************************************************/

//#include "hal_i2c.cfg"

/*****************************************************************************
*  Global Macro Definitions
*****************************************************************************/

#define		I2C_MASTER	0
#define		I2C_SLAVE	1

typedef enum
{
#undef  IIC_DEV_DEF
#define IIC_DEV_DEF(dev_name,iic_hw_name,addr) dev_name##_ID,
    #include "dev_i2c.cfg"
    IIC_DEV_NAME_MAX,
#undef  IIC_DEV_DEF
}IIC_DEV_NAME;



/*****************************************************************************
*  Global Type Declarations
*****************************************************************************/

typedef struct
{
	uint16	snd_len;
	uint16  rcv_len;
	uint8 *snd_dat;
	uint8 *rcv_dat;
}IIC_MSG;

typedef struct
{
	void	(*init)(uint8 hw_name);
//	void	(*deinit)(uint8 hw_name);
	uint16 	(*wr)  (uint8 hw_name, uint8 slave_addr, uint8 *dat, uint16 len);
	uint16	(*rd)  (uint8 hw_name, uint8 slave_addr, uint8 *dat, uint16 len);
	
}HAL_IIC_HW_API;


typedef struct
{
	uint8 	map_to_hw_name;		// which hw is used 
	uint8	addr;
}IIC_DEV_ATTR;

/*****************************************************************************
*  Global Data Declarations
*****************************************************************************/
/* %<CalStructHeader> 	*/
/* %<CalStructContents>	*/
/* %<CalStructTrailer>	*/


/*****************************************************************************
*  Global Function Declarations
*****************************************************************************/

/* Routinen */
void	HalIic_Init(void);
void *	HalIic_GetDev(uint8 const dev_name);
void	HalIic_SndRcv(void *iic,IIC_MSG *iic_msg);

void	HalIic_SndRcvExt(uint8 const iic_hw_name,uint8 slave_addr,IIC_MSG *msg);
void	HalI2c_SndRcvExtAddr(void *iic,IIC_MSG *msg,uint8 addr);

//uint32 	HalIic_GetVer(void);

void  	HalIic_Disable(void const *iic);



#ifdef __cplusplus
}
#endif

#endif


