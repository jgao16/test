/******************************************************************************
 * Project          s32k14x platform
 * (c) copyright    2015
 * Company          Visteon
 *                  All rights reserved
 * creation Date    2015-03 
 ******************************************************************************/


#ifndef HAL_DFLASH_H_H_
#define HAL_DFLASH_H_H_

#ifdef __cplusplus
extern "C" {
#endif 

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

typedef enum
{
	HAL_DFLASH_IDLE,
	HAL_DFLASH_PROGRAM,
	HAL_DFLASH_READ,
	HAL_DFLASH_ERASE
}HAL_DFLASH_STATUS;

typedef struct
{
	/* init driver */
	void 	(*init)(void);
	/* get entire flash size 	*/
	uint32 	(*get_flash_size)(void);
	/* get flash sector size	*/
	uint32	(*get_sector_size)(void);
	/* get page size 			*/
	uint32  (*get_page_size)(void);
	/* get segment size : Minimum number of bytes that has to be programmed at a time  */
	uint32  (*get_segment_size)(void);
	/* erase entire flash		*/
	void 	(*erase_entire_flash)(void);
	/* erase flash sector 		*/
	void    (*erase_flash_sector)( uint32 addr );
	/* program data 				*/
	uint32	(*program_data)(uint32 addr, const uint8 *dat, uint32 len);
	/* read data 				*/
	uint32  (*read_data)(uint32 addr,uint8 *dat, uint32 len);
	
	boolean	(*is_busy)(void);	
}HAL_DFLASH_HW_API;


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
/* hal flash init	*/
void 	HalDFlash_Init ( void );
/* flash erase entire flash	*/
void 	HalDFlash_EraseEntire(void);
/* flash get sector size 	*/
uint32 	HalDFlash_GetSectorSize(void);

/* flash erase 				*/
uint32 	HalDFlash_Erase( uint32 start_addr, uint32 len );
/* flash program data		*/
uint32 	HalDFlash_ProgramData(uint32 addr, const uint8 *dat, uint32 len);
/* flash read data 			*/
uint32 	HalDFlash_ReadData(uint32 addr, uint8 *dat, uint32 len);	



HAL_DFLASH_STATUS HalDFlash_GetStatus(void);

boolean HalDFlash_IsBusy(void); //TRUE:busy now


#ifdef __cplusplus
}
#endif


#endif
