
#include "std_type.h"

#include "hal_gpio.h"
#include "hal_hw.h"

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

static HAL_GPIO_HW_API const * hw_api;

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void	HalGpio_Init(void)
{
	hw_api = HalHw_GetApi(HAL_GPIO_HW_ID);
	if ( hw_api != NULL && hw_api->init_dev != NULL )
	{	
		hw_api->init_dev();
	}
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void	HalGpio_InitPinGpio( HAL_GPIO_INIT const*const init)
{
	if ( hw_api != NULL )
	{
		hw_api->init_pin_gpio(init);
	}
}
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void			HalGpio_SetPinVal(HAL_PORT_NAME pt,unsigned char pin,unsigned char val)
{
	if ( hw_api != NULL )
	{
		hw_api->set_pin_val(pt,pin,val);
	}
}

void			HalGpio_TogglePinval(HAL_PORT_NAME pt,unsigned char pin)
{
	if ( hw_api != NULL )
	{
		hw_api->toggle_pin_val(pt,pin);
	}
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void			HalGpio_SetPortVal(HAL_PORT_NAME pt,unsigned long const val)
{
	if ( hw_api != NULL )
	{
		hw_api->set_port_val(pt,val);
	}
}
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
boolean 		HalGpio_GetPinVal(HAL_PORT_NAME pt,unsigned char pin)
{
	if ( hw_api != NULL )
	{
		return hw_api->get_pin_val(pt,pin);
	}
	
	return 0;
}
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
uint32 			HalGpio_GetPortVal(HAL_PORT_NAME pt)
{
	if ( hw_api != NULL )
	{
		return hw_api->get_port_val(pt);
	}
	
	return 0;
}
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void 			HalGpio_PinAFConfig(HAL_PORT_NAME pt,unsigned char pin,unsigned char af)
{
	if ( hw_api != NULL )
	{
		hw_api->pin_af_config(pt,pin,af);
	}
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void			HalGpio_InitPinInt(HAL_GPIO_INT const* cfg)
{
	if ( hw_api != NULL )
	{
		hw_api->init_pin_int(cfg);
	}
}
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void 			HalGpio_EnPinInt(HAL_PORT_NAME pt,unsigned char pin)
{
	if ( hw_api != NULL )
	{
		hw_api->en_pin_int(pt,pin);
	}
}
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void 			HalGpio_DisPinInt(HAL_PORT_NAME pt,unsigned char pin)
{
	if ( hw_api != NULL )
	{
		hw_api->dis_pin_int(pt,pin);
	}
}

