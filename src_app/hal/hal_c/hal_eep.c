/******************************************************************************
 * Project          s32k14x platform
 * (c) copyright    2015
 * Company          Visteon
 *                  All rights reserved
 * creation Date    
 ******************************************************************************/
#include "std_type.h"
#include "hal_hw.h"
#include "hal_eep.h"


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static HAL_EEP_HW_API const *api;

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void HalEep_Init(void)
{
	api = HalHw_GetApi(HAL_EEP_HW_ID);
	if ( api != NULL && api->init )
	{
		api->init();
	}
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
uint32 	HalEep_Read	(uint32 const offset, uint8 *const buf, uint32 const num)
{
	uint32 ret_val = 0;
	if ( api != NULL && api->read )
	{
		ret_val = api->read(offset,buf,num);
	}
	return ret_val;
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
uint32 	HalEep_Write(uint32 const offset, uint8 const* buf, uint32 const num)
{
	uint32 ret_val = 0;
	if ( api != NULL && api->write )
	{
		ret_val = api->write(offset,buf,num);
	}
	return ret_val;
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
boolean HalEep_IsBusy(void)
{
	boolean ret_val = FALSE;

	if ( api != NULL && api->is_busy )
	{
		ret_val = api->is_busy();
	}	
	return ret_val;
}




