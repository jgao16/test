/******************************************************************************
 * Project          s32k14x platform
 * (c) copyright    2015
 * Company          Visteon
 *                  All rights reserved
 * creation Date    2015-03 
 ******************************************************************************/


#ifndef HAL_FLASH_H_H_
#define HAL_FLASH_H_H_

#ifdef __cplusplus
extern "C" {
#endif 

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

typedef enum
{
	HAL_FLASH_IDLE,
	HAL_FLASH_PROGRAM,
	HAL_FLASH_READ,
	HAL_FLASH_ERASE
}HAL_FLASH_STATUS;

typedef struct
{
	/* init driver */
	void 	(*init)(void);
	/* get entire flash size 	*/
	uint32 	(*get_flash_size)(void);
	/* get flash sector size	*/
	uint32	(*get_sector_size)(void);
	/* get page size 			*/
	uint32  (*get_page_size)(void);
	/* get segment size : Minimum number of bytes that has to be programmed at a time  */
	uint32  (*get_segment_size)(void);
	/* erase entire flash		*/
	void 	(*erase_entire_flash)(void);
	/* erase flash sector 		*/
	void    (*erase_flash_sector)( uint32 addr );
	/* program data 				*/
	uint32	(*program_data)(uint32 addr, const uint8 *dat, uint32 len);
	/* read data 				*/
	uint32  (*read_data)(uint32 addr,uint8 *dat, uint32 len);

	void 	(*enter_power_down)(void);
	void 	(*exit_power_down)(void);
	
	boolean	(*is_busy)(void);
	void (*sync)(void);	
}HAL_FLASH_HW_API;


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
/* hal flash init	*/
void 	HalFlash_Init ( void );
/* flash erase entire flash	*/
void 	HalFlash_EraseEntire(void);
/* flash get sector size 	*/
uint32 	HalFlash_GetSectorSize(void);
/* flash erase 				*/
uint32 	HalFlash_Erase( uint32 start_addr, uint32 len );
/* flash program data		*/
uint32 	HalFlash_ProgramData(uint32 addr, const uint8 *dat, uint32 len);
/* force flash sync program */
void   	HalFlash_ForceSync(void);

/* flash read data 			*/
uint32 	HalFlash_ReadData(uint32 addr, uint8 *dat, uint32 len);	

void 	HalFlash_EnterPowerDown(void);
void	HalFlash_ExitPowerDown(void);

HAL_FLASH_STATUS HalFlash_GetStatus(void);

void	HalFlash_Sync(void);

#ifdef __cplusplus
}
#endif


#endif
