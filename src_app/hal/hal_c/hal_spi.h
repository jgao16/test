/*
********************************************************************************************************
*                                        Hal_SPI
*                           (c) Copyright 2011, kangqiaoren
*                                     All Rights Reserved
*                                         V0.01
*
*********************************************************************************************************
*/

/*
*********************************************************************************************************
*
*                                      Hal SPI CONFIGURATION
*
* Filename      : Hal_SPI.h
* Version       : V0.01
* Programmer(s) : KangqiaoRen
* Email:		: kangqiaoren@gmail.com
*********************************************************************************************************
* Note(s)       : 
*********************************************************************************************************
*/

#ifndef __HAL_SPI_H_
#define __HAL_SPI_H_

#ifdef __cplusplus
extern "C" {
#endif

/*****************************************************************************
*  Include Files
*****************************************************************************/
//#include "Hal_SPI_cfg.h"

/*****************************************************************************
*  Global Macro Definitions
*****************************************************************************/
//enum
//{
//	SPI_CPOL_BIT	=	0x01,
//	SPI_CPHA_BIT	=	0x02,
//	SPI_NSS_BIT		=	0x04,
//	SPI_FIRST_BIT	= 	0x08,
//	SPI_DATALENGTH_BIT	=	0x10
//};

/* Clock Polarity, IDLE state of SCK, high 	*/
#define SPI_CPOL_LOW		0x00
#define	SPI_CPOL_HIGH		0x01

#define SPI_CPHA_SECONDE_EDGE	0x00
/* first edge capture						*/
#define	SPI_CPHA_FIRST_EDGE	0x02

#define SPI_NNS_SOFTWARE	0x00
/* hardware selected						*/
#define	SPI_NSS_HARD		0x04
/* hardware select high, if SPI_NSS_HARD is set */
#define	SPI_HW_NSS_HIGH		0x08
/* hw select num: bit 4~6, if SPI_NSS_HARD is set*/
#define	SPI_HW_NSS(x)		(((x)&0x03)<<4)	//lint -e845 -e835
/* MSB fisrt, bit 7							*/
#define	SPI_MSB_FIRST		0x80
#define SPI_LSG_FIRST		0x00

/* Data Length, default is 8 bits: bit 8~9 	*/
/* Data length 16 bitset					*/
#define SPI_DATA_LENGTH_8   0x000
#define	SPI_DATA_LENGTH_16	0x100
#define	SPI_DATA_LENGTH_24	0x200	// not support for bit num 24	
#define	SPI_DATA_LENGTH_32	0x300


/*****************************************************************************
*  Global Type Declarations
*****************************************************************************/
/* %<Types> */

typedef enum
{
#undef SPI_DEV_DEF
#define SPI_DEV_DEF(dev_name,spi_hw_name) dev_name##_ID,
	#include "spi_dev.cfg"
#undef SPI_DEV_DEF
	SPI_DEV_NAME_MAX
}SPI_DEV_NAME;


typedef enum
{
	SPI_MASTER,
	SPI_SLAVE,

	SPI_CLOCK_MAX
}SPI_CLOCK_DIR;

typedef enum
{
	SPI_SYNC,
	SPI_ASYNC,

	SPI_TRANS_MAX
}SPI_TRANS_MOD;

/// SPI transfer rate.
typedef enum 
{
	HAL_SPI_100KBIT = 0,
	HAL_SPI_125KBIT,
	HAL_SPI_250KBIT,
	HAL_SPI_500KBIT,
	HAL_SPI_1MBIT,
	HAL_SPI_2MBIT,
	HAL_SPI_4MBIT,
	HAL_SPI_8MBIT,
	HAL_SPI_9MBIT,
	HAL_SPI_BAUD_MAX
}HAL_SPI_BAUD;


typedef struct
{
	uint16 len;
	uint8 *tx_dat;
	uint8 *rx_dat;
}HAL_SPI_MSG;

typedef struct
{	
	/* Clock Polarity, IDLE state of SCK, LOW 	*/
	/* second edge capture						*/
	/* hardware selected						*/
	/* LSB fisrt								*/
	/* Data length 8 bitset						*/
	uint16			spi_attr;
		
	HAL_SPI_BAUD	spi_baud;

	void (*chip_en)(void);						/**< Chip Enable for external Hardware */
	void (*chip_dis)(void);						/**< Chip Disable for external Hardware */
	void (*trans_calbk)(void);					/* async transfer complete callback		*/
}SPI_DEVICE;


typedef struct
{
	void (*init)(uint8 hw_ch, const SPI_DEVICE * spi_dev);
//	void (*deinit)(uint8 hw_ch);
	void (*com) (uint8 hw_ch, const uint16 len, const uint8 *tx_dat,uint8 *rx_dat);
	
//	void (*lock_hw)	(uint8 hw_ch);
//	void (*unlock_hw)(uint8 hw_ch);
}HAL_SPI_HW_API;


/*****************************************************************************
*  Global Data Declarations
*****************************************************************************/
/* %<CalStructHeader> 	*/
/* %<CalStructContents>	*/
/* %<CalStructTrailer>	*/


/*****************************************************************************
*  Global Function Declarations
*****************************************************************************/
void		HalSpi_Init(void);
SPI_DEVICE*	HalSpi_GetDev(uint8 dev_name);
void 		HalSpi_Com   (uint8 dev_name, const uint16 len, const uint8 *tx_dat,uint8 *rx_dat);

void		HalSpi_MuiltCom(uint8 dev_name, const uint16 msg_num, HAL_SPI_MSG *msg );

void 		HalSpi_ComExt(SPI_DEVICE *spi_dev,uint8 hw_ch, const uint16 len,const uint8 *tx_dat,uint8 *rx_dat);

uint32		HalSpi_GetVer(void);

void 		HalSpi_HwComOk(uint8 hw_ch);

#ifdef __cplusplus
}
#endif

#endif


