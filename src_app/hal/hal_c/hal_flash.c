/******************************************************************************
 * Project          s32k14x platform
 * (c) copyright    2015
 * Company          Visteon
 *                  All rights reserved
 * creation Date    2015-03 
 ******************************************************************************/

#include "std_type.h" 
#include "hal_hw.h"
#include "hal_flash.h"


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

static HAL_FLASH_HW_API const *api;


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
/* hal flash init	*/
void HalFlash_Init ( void )
{
	api = HalHw_GetApi(HAL_FLASH_HW_ID);
	if ( api != NULL && api->init )
	{
		api->init();
	}
}


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
/* flash erase entire flash	*/
void 	HalFlash_EraseEntire(void)
{
	if ( api != NULL && api->erase_entire_flash )
	{
		api->erase_entire_flash();
	}
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
/* flash get sector size 	*/
uint32 	HalFlash_GetSectorSize(void)
{
	if ( api != NULL && api->get_sector_size )
	{
		return api->get_sector_size();
	}
	return 0;
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
/* flash erase 				*/
uint32 	HalFlash_Erase( uint32 start_addr, uint32 len )
{
	if ( api != NULL && api->erase_flash_sector )
	{
		api->erase_flash_sector(start_addr);
	}
	
	return len;
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
/* flash program data		*/
uint32 	HalFlash_ProgramData(uint32 addr, const uint8 *dat, uint32 len)
{
	if ( api != NULL && api->program_data )
	{
		len = api->program_data(addr,dat,len);
	}
	else
	{
		len = 0;
	}
	
	return len;
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
/* force flash sync program */
void   	HalFlash_ForceSync(void)
{
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
/* flash read data 			*/
uint32 	HalFlash_ReadData(uint32 addr, uint8 *dat, uint32 len)
{
	if ( api != NULL && api->read_data )
	{
		len = api->read_data(addr,dat,len);
	}
	else
	{
		len = 0;
	}
	return len;
}

void 	HalFlash_EnterPowerDown(void)
{
	if ( api != NULL && api->enter_power_down )
	{
		api->enter_power_down();
	}
}
void	HalFlash_ExitPowerDown(void)
{
	if ( api != NULL && api->exit_power_down )
	{
		api->exit_power_down();
	}
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
/* get flash status */
HAL_FLASH_STATUS HalFlash_GetStatus(void)
{
	HAL_FLASH_STATUS status = HAL_FLASH_IDLE;

	if ( api != NULL && api->is_busy )
	{
		if ( api->is_busy() )
		{
			status = HAL_FLASH_PROGRAM;
		}
	}

	return status;
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void	HalFlash_Sync(void)
{
	if ( api != NULL && api->sync )
	{
		api->sync();
	}	
}


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

