/*
********************************************************************************************************
*                                        hal_iic
*                           (c) Copyright 2011, kangqiaoren
*                                     All Rights Reserved
*                                         V0.01
*
*********************************************************************************************************
*/

/*
*********************************************************************************************************
*
*                                      hal_iic CONFIGURATION
*
* Filename      : hal_iic.c
* Version       : V0.01
* Programmer(s) : KangqiaoRen
* Email:		: kangqiaoren@gmail.com
*********************************************************************************************************
* Note(s)       : 
*********************************************************************************************************
*/

/*****************************************************************************
*  Include Files
*****************************************************************************/
#include "std_type.h"

#include "hal_os.h"

#include "hal_hw.h"
#include "hal_iic.h"
#include "dev_i2c.cfg"
/*****************************************************************************
*  Local Macro Definitions
*****************************************************************************/
#define		__IIC_VERSION 	(100u)      /* v1.00 */
#define		IIC_MASTER		(0u)
#define		IIC_SLAVE		(1u)

/*****************************************************************************
*  Local Type Declarations
*****************************************************************************/

/*****************************************************************************
*  Global Variable Definitions (Global for the whole system)
*****************************************************************************/


/*****************************************************************************
*  Local Variable Definitions (Global within this file)
*****************************************************************************/
/* Must be defined in the Hal_SPI_cfg.h */
static const IIC_DEV_ATTR iic_dev_attr[] =
{
#undef  IIC_DEV_DEF
#define IIC_DEV_DEF(dev_name,iic_hw_name,addr) {iic_hw_name##_ID,addr},
    #include "dev_i2c.cfg"
#undef  IIC_DEV_DEF
};

static HAL_OS_SEM_ID		 iic_hw_res[HAL_IIC_HW_NAME_MAX];
static const HAL_IIC_HW_API * hw_api;
//static const HAL_IIC_HW_API * flexio_i2c_api;


static boolean	iic_hw_inited[HAL_IIC_HW_NAME_MAX];
/*****************************************************************************
*  Global Function Declarations
*****************************************************************************/

/*****************************************************************************
*  Local Function Declarations
*****************************************************************************/


/*****************************************************************************/
/*  Global Function Definitions                                              */
/*****************************************************************************/

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void	HalIic_Init(void)
{
	hw_api 			= 	HalHw_GetApi(HAL_IIC_HW_ID);
	//flexio_i2c_api	=	HalHw_GetApi(HAL_FLEXIO_IIC_ID);
	for (uint8 i = 0; i < HAL_IIC_HW_NAME_MAX; i ++)
	{
		iic_hw_res[i] 	 = HalOS_SemaphoreCreate(1,"Hal IIC Res Sem");	
		iic_hw_inited[i] = FALSE;
	}
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void *	HalIic_GetDev(uint8 const dev_name)
{					
	if ( dev_name< IIC_DEV_NAME_MAX ){
		return (void*)(iic_dev_attr + dev_name);
	}else{
		return NULL;
	}
}


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void	HalIic_SndRcv(void *iic,IIC_MSG *msg)
{	
	IIC_DEV_ATTR *dev = (IIC_DEV_ATTR*)iic;

	if ( iic != NULL && msg != NULL )
	{
		if ( HalOS_SemaphoreWait(iic_hw_res[dev->map_to_hw_name],HAL_OS_WaitForever) == HAL_OS_SEM_ERR_NONE )
		{
			HAL_IIC_HW_API const*api;
			//if ( dev->map_to_hw_name == IIC_A2_ID ){
			//	api = flexio_i2c_api;
			//}else{
				api = hw_api;
			//}
			
			if ( !iic_hw_inited[dev->map_to_hw_name] )
			{	/* need to initial iic hw */
				iic_hw_inited[dev->map_to_hw_name] = TRUE;
				if ( api != NULL && api->init != NULL ){
					api->init(dev->map_to_hw_name);
				}
			}
			/* send dat to iic device */
			if ( msg->snd_len )
			{
				if ( api != NULL && api->wr != NULL )
				{
					msg->snd_len = api->wr(dev->map_to_hw_name,dev->addr,msg->snd_dat,msg->snd_len);			
				}
			}

			if ( msg->rcv_len )
			{
				if ( api != NULL && api->rd != NULL )
				{
					msg->rcv_len = api->rd(dev->map_to_hw_name,dev->addr,msg->rcv_dat,msg->rcv_len);	
				}
			}

			HalOS_SemaphorePost(iic_hw_res[dev->map_to_hw_name]);
		}		
	}
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void	HalIic_SndRcvExt(uint8 const iic_hw_name,uint8 slave_addr,IIC_MSG *msg)
{
	if ( iic_hw_name < HAL_IIC_HW_NAME_MAX && msg != NULL )
	{
		if ( HalOS_SemaphoreWait(iic_hw_res[iic_hw_name],HAL_OS_WaitForever) == HAL_OS_SEM_ERR_NONE )
		{
			HAL_IIC_HW_API const*api;
			//if ( iic_hw_name == IIC_A2_ID ){
			//	api = flexio_i2c_api;
			//}else{
				api = hw_api;
			//}
			
			if ( !iic_hw_inited[iic_hw_name] )
			{	/* need to initial iic hw */
				iic_hw_inited[iic_hw_name] = TRUE;
				if ( api != NULL && api->init != NULL ){
					api->init(iic_hw_name);
				}
			}
			/* send dat to iic device */
			if ( msg->snd_len )
			{
				if ( api != NULL && api->wr != NULL )
				{
					msg->snd_len = api->wr(iic_hw_name,slave_addr,msg->snd_dat,msg->snd_len);			
				}
			}

			if ( msg->rcv_len )
			{
				if ( api != NULL && api->rd != NULL )
				{
					msg->rcv_len = api->rd(iic_hw_name,slave_addr,msg->rcv_dat,msg->rcv_len);	
				}
			}

			HalOS_SemaphorePost(iic_hw_res[iic_hw_name]);
		}		
	}
}
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void	HalI2c_SndRcvExtAddr(void *iic,IIC_MSG *msg,uint8 addr)
{	
	IIC_DEV_ATTR *dev = (IIC_DEV_ATTR*)iic;

	if ( iic != NULL && msg != NULL )
	{
		if ( HalOS_SemaphoreWait(iic_hw_res[dev->map_to_hw_name],HAL_OS_WaitForever) == HAL_OS_SEM_ERR_NONE )
		{
			HAL_IIC_HW_API const*api;
			//if ( dev->map_to_hw_name == IIC_A2_ID ){
			//	api = flexio_i2c_api;
			//}else{
				api = hw_api;
			//}
			
			if ( !iic_hw_inited[dev->map_to_hw_name] )
			{	/* need to initial iic hw */
				iic_hw_inited[dev->map_to_hw_name] = TRUE;
				if ( api != NULL && api->init != NULL ){
					api->init(dev->map_to_hw_name);
				}
			}
			/* send dat to iic device */
			if ( msg->snd_len )
			{
				if ( api != NULL && api->wr != NULL )
				{
					msg->snd_len = api->wr(dev->map_to_hw_name,addr,msg->snd_dat,msg->snd_len);			
				}
			}

			if ( msg->rcv_len )
			{
				if ( api != NULL && api->rd != NULL )
				{
					msg->rcv_len = api->rd(dev->map_to_hw_name,addr,msg->rcv_dat,msg->rcv_len);	
				}
			}

			HalOS_SemaphorePost(iic_hw_res[dev->map_to_hw_name]);
		}		
	}
}
/*****************************************************************************/
/*                        Version                                            */
/*****************************************************************************/
uint32		HalIic_GetVer(void)
{
	return __IIC_VERSION;
}
/*****************************************************************************/
/*                        Disable                                            */
/*****************************************************************************/
void	HalIic_Disable(void const *iic)
{
//	if ( iic != NULL)
//	{
//		IIC_DEV_ATTR *dev = (IIC_DEV_ATTR*)iic;
//
//		if ( HalOS_SemaphoreWait(iic_hw_res[dev->map_to_hw_name],HAL_OS_WaitForever) == HAL_OS_SEM_ERR_NONE )
//		{
//			HAL_IIC_HW_API const *api = hw_api;
//
//			if (iic_hw_inited[dev->map_to_hw_name] )
//			{	/* need to initial iic hw */
//				iic_hw_inited[dev->map_to_hw_name] = FALSE;
//				if ( api != NULL && api->deinit != NULL ){
//					api->deinit(dev->map_to_hw_name);
//				}
//			}
//
//			HalOS_SemaphorePost(iic_hw_res[dev->map_to_hw_name]);
//		}
//	}
}



/*****************************************************************************
*  Local Function Definitions
*****************************************************************************/

