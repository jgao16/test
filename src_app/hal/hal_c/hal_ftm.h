/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
//hal_ftm api
#ifndef HAL_FTM_H_H_
#define HAL_FTM_H_H_

#ifdef __cplusplus
extern "C" {
#endif 

#include "std_type.h"


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
typedef enum
{
#define PWM_FTM_DEV_DEF(dev_name, hw_name) dev_name##_ID,
    #include "dev_ftm.cfg"
#undef PWM_FTM_DEV_DEF
    PWM_DEV_NAME_MAX
}PWM_DEV_NAME;

typedef struct
{
    void (*ch_init)         (uint8 hw_ch);
    void (*ch_deinit)       (uint8 hw_ch);
    void (*ch_set_pwm_val)  (uint8 hw_ch, uint8 const val);
    void (*ch_get_pwm_val)  (uint8 hw_ch, uint8 *val);
}HAL_FTM_HW_API;

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void HalFtm_ChInit ( uint8 dev_name );
void HalFtm_ChDeInit ( uint8 dev_name );
void HalFtm_ChSetPwmVal(uint8 dev_name, uint8 const val);
void HalFtm_ChGetPwmVal(uint8 dev_name, uint8 *val);


#ifdef __cplusplus
}
#endif


#endif

