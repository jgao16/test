/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
//hal_adc
#include "std_type.h" 
#include "hal_hw.h"
#include "hal_ftm.h"

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static const uint8 ftm_dev_map_to_hw[] = 
{
#define PWM_FTM_DEV_DEF(dev_name, hw_name) hw_name##_ID,
    #include "dev_ftm.cfg"
#undef PWM_FTM_DEV_DEF

};

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static HAL_FTM_HW_API const *api;

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void HalFtm_ChInit ( uint8 dev_name )
{
	api = HalHw_GetApi(HAL_FTM_HW_ID);
	if ( (NULL != api) && (NULL != api->ch_init) )
	{
		uint8 hw_ch = ftm_dev_map_to_hw[dev_name];
		api->ch_init(hw_ch);
	}
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void HalFtm_ChDeInit ( uint8 dev_name )
{
	if ( (NULL != api) && (NULL != api->ch_deinit) )
	{
		uint8 hw_ch = ftm_dev_map_to_hw[dev_name];
		api->ch_deinit(hw_ch);
	}
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void HalFtm_ChSetPwmVal(uint8 dev_name, uint8 const val)
{
	if ( (NULL != api) && (NULL != api->ch_set_pwm_val) )
	{
		uint8 hw_ch = ftm_dev_map_to_hw[dev_name];
		api->ch_set_pwm_val(hw_ch, val);
	}
    if ( val != 0x00 )
    {
        __asm("nop");
    }
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void HalFtm_ChGetPwmVal(uint8 dev_name, uint8 *val)
{
	if ( (NULL != api) && (NULL != api->ch_set_pwm_val) )
	{
		uint8 hw_ch = ftm_dev_map_to_hw[dev_name];
		api->ch_get_pwm_val(hw_ch, val);
	}
}


