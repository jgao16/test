
#ifndef __HAL_GPIO_H_
#define	__HAL_GPIO_H_


#ifdef __cplusplus
extern "C" {
#endif

/*****************************************************************************
*  Global Type Declarations
*****************************************************************************/
typedef enum
{
	HAL_GPIO_PT_A	=	0x00,	
	HAL_GPIO_PT_B,
	HAL_GPIO_PT_C,
	HAL_GPIO_PT_D,
	HAL_GPIO_PT_E,
	HAL_GPIO_PT_F,
	HAL_GPIO_PT_G,
	HAL_GPIO_PT_H,
	HAL_GPIO_PT_I,
	HAL_GPIO_PT_J,
	HAL_GPIO_PT_K,
	HAL_GPIO_PT_L,

	HAL_GPIO_PT_MAX,
	
}HAL_PORT_NAME;

typedef enum
{
	PORT_INPUT = 0, 
	PORT_OUTPUT,
	PORT_AF,
	PORT_AN,
	
	PORT_MODE_MAX,
}PORT_MODE;
typedef enum
{
	OUTPUT_PUSH_PULL	=	0x00,
	OUTPUT_OPEN_DRAIN,
}OUTPUT_TYPE;
typedef enum
{
	INPUT_NO_PULL		=	0x00,
	INPUT_PULL_UP,
	INPUT_PULL_DOWN,
}INPUT_TYPE;

/*
pt:port
pin: pin num, 0~31
mode:input,output,Alternate function and other
otype,output type:Push pull,open drain
pupd:pull up, pull down, no pull
*/
typedef struct
{
	HAL_PORT_NAME 	pt;
	unsigned char 	pin;
	PORT_MODE		mode;
	OUTPUT_TYPE		otype;
	INPUT_TYPE		itype;
}HAL_GPIO_INIT;

typedef enum
{
	GPIO_RISING_EDGE = 0,
	GPIO_FALLING_EDGE,
	GPIO_BOTH_EDGE,
}INT_EDGE;
typedef struct 
{
	HAL_PORT_NAME 	pt;
	unsigned char 	pin;
	INT_EDGE		edge;
	void (*isr)(void);		// interrupt callback
}HAL_GPIO_INT;


typedef struct
{
	void	(*init_dev)			( void );
	void 	(*init_pin_gpio)	( HAL_GPIO_INIT const*const init);
	void 	(*set_pin_val)		(HAL_PORT_NAME pt,unsigned char pin,unsigned char val);
	void 	(*toggle_pin_val)		(HAL_PORT_NAME pt,unsigned char pin);
	void 	(*set_port_val)		(HAL_PORT_NAME pt,unsigned long const val);
	boolean (*get_pin_val)		(HAL_PORT_NAME pt,unsigned char pin);
	uint32 	(*get_port_val)		(HAL_PORT_NAME pt);
	void 	(*pin_af_config)	(HAL_PORT_NAME pt,unsigned char pin,unsigned char af);

	void	(*init_pin_int)		(HAL_GPIO_INT const* cfg);
	void 	(*en_pin_int)		(HAL_PORT_NAME pt,unsigned char pin);
	void 	(*dis_pin_int)		(HAL_PORT_NAME pt,unsigned char pin);	
}HAL_GPIO_HW_API;

#define HalGpio_InitPinGpio_Def(pt_x,pin_x,mode_x,otype_x,itype_x) 	\
	do{																\
		HAL_GPIO_INIT init;											\
		init.pt		=	pt_x;										\
		init.pin	=	pin_x;										\
		init.mode	=	mode_x;										\
		init.otype	=	otype_x;									\
		init.itype	=	itype_x;									\
		HalGpio_InitPinGpio(&init);									\
	}while(0)

#define HalGpio_InitPinInt_Def(pt_x,pin_x,edge_x,isr_x) 	\
	do{										\
		HAL_GPIO_INT cfg;					\
		cfg.pt   = pt_x;					\
		cfg.pin  = pin_x;					\
		cfg.edge = edge_x;					\
		cfg.isr  = isr_x;					\
		HalGpio_InitPinInt(&cfg);			\
	}while(0)

/*****************************************************************************
*  Global Function Declarations
*****************************************************************************/
void			HalGpio_Init(void);
void			HalGpio_InitPinGpio( HAL_GPIO_INIT const*const init);
void			HalGpio_SetPinVal(HAL_PORT_NAME pt,unsigned char pin,unsigned char val);
void			HalGpio_TogglePinval(HAL_PORT_NAME pt,unsigned char pin);
void			HalGpio_SetPortVal(HAL_PORT_NAME pt,unsigned long const val);
boolean 		HalGpio_GetPinVal(HAL_PORT_NAME pt,unsigned char pin);
uint32 			HalGpio_GetPortVal(HAL_PORT_NAME pt);
void 			HalGpio_PinAFConfig(HAL_PORT_NAME pt,unsigned char pin,unsigned char af);

void			HalGpio_InitPinInt(HAL_GPIO_INT const* cfg);
void 			HalGpio_EnPinInt(HAL_PORT_NAME pt,unsigned char pin);
void 			HalGpio_DisPinInt(HAL_PORT_NAME pt,unsigned char pin);

#ifdef __cplusplus
}
#endif

#endif

