
#ifndef __STD_TYPE_H
#define __STD_TYPE_H
/*****************************************************************************
 *  Include Files
 *****************************************************************************/
#include "stdint.h"
#include "stdio.h"
#include "stdbool.h"

#include "stddef.h"

/*****************************************************************************
 *  Global Macro Definitions
 *****************************************************************************/

#ifndef TRUE
#define	TRUE	(1)
#endif

#ifndef FALSE
#define FALSE	(0)
#endif

#ifndef HIGH
#define HIGH	(1)
#endif

#ifndef LOW
#define LOW		(0)
#endif


#ifndef ACTIVATE_HIGH
#define	ACTIVATE_HIGH	(1u)
#endif

#ifndef ACTIVATE_LOW
#define	ACTIVATE_LOW	(0u)
#endif

#ifndef ACTIVATE
#define	ACTIVATE		(1u)
#endif

#ifndef INACTIVATE
#define	INACTIVATE		(0u)
#endif

#ifndef DEACTIVATE
#define	DEACTIVATE		(0u)
#endif

//#ifndef ENABLE
//#define ENABLE (1)
//#endif

//#ifndef DISABLE
//#define DISABLE (0)
//#endif

//#ifndef NULL
//#define NULL	((void *)0)
//#endif

#define RemoteIC_ID		0

#define BIT0				(0x0001)
#define BIT1				(0x0002)
#define BIT2				(0x0004)
#define BIT3				(0x0008)
#define BIT4				(0x0010)
#define BIT5				(0x0020)
#define BIT6				(0x0040)
#define BIT7				(0x0080)
#define BIT8				(0x0100)
#define BIT9				(0x0200)
#define BIT10				(0x0400)
#define BIT11				(0x0800)
#define BIT12				(0x1000)
#define BIT13				(0x2000)
#define BIT14				(0x4000)
#define BIT15				(0x8000)
#define BIT16				(0x10000)
#define BIT17				(0x20000)
#define BIT18				(0x40000)
#define BIT19				(0x80000)
#define BIT20				(0x100000)
#define BIT21				(0x200000)
#define BIT22				(0x00400000)
#define BIT23				(0x00800000)
#define BIT24				(0x01000000)
#define BIT25				(0x02000000)
#define BIT26				(0x04000000)
#define BIT27				(0x08000000)
#define BIT28				(0x10000000)
#define BIT29				(0x20000000)
#define BIT30				(0x40000000)
#define BIT31				(0x80000000)

/*****************************************************************************
 *  Global Type Declarations
 *****************************************************************************/
/* %<Types> */

typedef 		 unsigned char	U1;
typedef 		 unsigned char	uint8;
typedef volatile unsigned char	VU1;
typedef volatile unsigned char	vuint8;
typedef 		 unsigned short	U2;
typedef 		 unsigned short	uint16;
typedef volatile unsigned short	VU2;
typedef volatile unsigned short	vuint16;
typedef 		 unsigned long	U4;
typedef 		 unsigned long	uint32;
typedef volatile unsigned long 	VU4;
typedef volatile unsigned long 	vuint32;

typedef 		unsigned char	UC;

typedef 		unsigned long long U64;
typedef 		unsigned long long uint64;

typedef 		 signed char	S1;
typedef 		 signed char	sint8;
typedef volatile signed char	VS1;
typedef volatile signed char	vsint8;
typedef 		 signed short	S2;
typedef 		 signed short	sint16;
typedef volatile signed short	VS2;
typedef volatile signed short	vsint16;
typedef 		 signed long	S4;
typedef 		 signed long	sint32;
typedef volatile signed long	VS4;
typedef volatile signed long	vsint32;

typedef 		 signed long long S64;
typedef 		 signed long long sint64;

//#ifndef boolean
typedef unsigned char	boolean;
//#endif


/*****************************************************************************
 *  Global Data Declarations
 *****************************************************************************/
/* %<CalStructHeader> 	*/
/* %<CalStructContents>	*/
/* %<CalStructTrailer>	*/


/*****************************************************************************
 *  Global Function Declarations
 *****************************************************************************/

#endif


