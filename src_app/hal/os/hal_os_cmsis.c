
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#include"hal_os.h"

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
//#if ( configQUEUE_REGISTRY_SIZE > 0 )
//HAL_OS_SEM_ID 		HalOS_SemaphoreCreate(uint16 cnt, const char * sem_name)
//{
//	osSemaphoreDef_t sem =  {0};
	
//	HAL_OS_SEM_ID id = osSemaphoreCreate((osSemaphoreDef_t*){0},cnt);
//	vQueueAddToRegistry(id,sem_name);
//	return id;
//}
//#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
//#ifdef POWER_MODE_CTRL_EN
//HAL_OS_SEM_STATE  	HalOS_SemaphoreWait  (HAL_OS_SEM_ID id, uint32 millisec)
//{	
//	HAL_OS_SEM_STATE state;

//	if ( millisec != HAL_OS_WaitForever && millisec != 0x00 ){
//		PwrCtrl_DisMpSleepGlobal();
//	}
	
//	state = (osSemaphoreWait(id,millisec)==0) ? HAL_OS_SEM_ERR_NONE : HAL_OS_SEM_TIMEOUT;

//	if ( millisec != HAL_OS_WaitForever && millisec != 0x00 ){
//		PwrCtrl_EnMpSleepGlobal();
//	}

//	return state;
//}
//#endif

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
/* return 0: false, other: success	*/
//void  HalOS_SemaphoreDelete(HAL_OS_SEM_ID id)
//{
//	(void)osSemaphoreDelete(id);
//#if ( configQUEUE_REGISTRY_SIZE > 0 )
//	vQueueUnregisterQueue(id);
//#endif	
//}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
//void  HalOS_SemaphorePost  (HAL_OS_SEM_ID id)
//{
//	(void)osSemaphoreRelease(id);
//}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
//uint32 HalOS_GetKernelTick  (void)
//{
//	return osKernelSysTick();
//}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
//void   HalOS_Delay	( uint32 millisec )
//{
//	(void)osDelay(millisec);
//}
