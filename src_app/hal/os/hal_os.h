
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    Service config:
    All rights reserved
>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

#ifndef __HAL_OS_H__
#define __HAL_OS_H__

#ifdef  __cplusplus
extern "C"
{
#endif

#include "std_type.h"
#include "cmsis_os.h"
#include "pwrctrl.h"
/*****************************************************************************
 *  Include Files
 *****************************************************************************/

/*****************************************************************************
 *  Global Macro Definitions
 *****************************************************************************/
#define HAL_OS_WaitForever	(osWaitForever)

#define	HAL_OS_TICK_PER_SEC	(configTICK_RATE_HZ)

typedef enum
{
	HAL_OS_SEM_ERR_NONE,
	HAL_OS_SEM_TIMEOUT,
	
	HAL_OS_SEM_ERR_AVBL
}HAL_OS_SEM_STATE; 
 
typedef osSemaphoreId	HAL_OS_SEM_ID;




/*****************************************************************************
 *  Global Type Declarations
 *****************************************************************************/
/* %<Types> */





/*****************************************************************************
 *  Global Data Declarations
 *****************************************************************************/
/* %<CalStructHeader> 	*/
/* %<CalStructContents>	*/
/* %<CalStructTrailer>	*/


/*****************************************************************************
 *  Global Function Declarations
 *****************************************************************************/
 /** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
/* hal os semphore create */
//#define HalOS_SemaphoreCreate(cnt,name) osSemaphoreCreate((osSemaphoreDef_t*){0},cnt)
static inline HAL_OS_SEM_ID HalOS_SemaphoreCreate(uint16 cnt, const char * sem_name)
{
	HAL_OS_SEM_ID id = osSemaphoreCreate((osSemaphoreDef_t*){0},cnt);
	vQueueAddToRegistry(id,sem_name);
	return id;
}

/* hal os semphore create with max count  */
//#define HalOS_SemaphoreCreate(cnt,name) osSemaphoreCreate((osSemaphoreDef_t*){0},cnt)
static inline HAL_OS_SEM_ID HalOS_SemaphoreCreateWithMaxCnt(uint16 cnt, uint16 max_cnt, const char * sem_name)
{
	//HAL_OS_SEM_ID id = osSemaphoreCreate((osSemaphoreDef_t*){0},cnt);
	HAL_OS_SEM_ID id = xSemaphoreCreateCounting(max_cnt, cnt);
	vQueueAddToRegistry(id,sem_name);
	return id;
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
/* hal os semphore wait */
static inline HAL_OS_SEM_STATE  HalOS_SemaphoreWait  (HAL_OS_SEM_ID id, uint32 millisec)
{	
	HAL_OS_SEM_STATE state;

	if ( millisec != HAL_OS_WaitForever && millisec != 0x00 ){
		PwrCtrl_DisMpSleepGlobal();
	}
	
	state = (osSemaphoreWait(id,millisec)==0) ? HAL_OS_SEM_ERR_NONE : HAL_OS_SEM_TIMEOUT;

	if ( millisec != HAL_OS_WaitForever && millisec != 0x00 ){
		PwrCtrl_EnMpSleepGlobal();
	}

	return state;
}


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
//void	HalOS_Delay	( uint32 delay_ms );
#define	HalOS_Delay(ms)				do{ if((ms)!=HAL_OS_WaitForever&&ms!=0x00) PwrCtrl_DisMpSleepGlobal();(void)osDelay((ms)); if((ms)!=HAL_OS_WaitForever&&ms!=0x00) PwrCtrl_EnMpSleepGlobal();}while(0)

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
//void  HalOS_SemaphoreDelete(HAL_OS_SEM_ID id);
#define HalOS_SemaphoreDelete(id)	do{	(void)osSemaphoreDelete(id); }while(0)
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/		
//void  HalOS_SemaphorePost(HAL_OS_SEM_ID id); 							
#define HalOS_SemaphorePost(id)		do{(void)osSemaphoreRelease(id);}while(0)
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/		
//static inline void  HalOS_SemaphoreSet(HAL_OS_SEM_ID id,uint32 cnt)
//{
//	while( HalOS_SemaphoreWait(id,0)==HAL_OS_SEM_ERR_NONE );
//	while(cnt)
//	{
//		cnt --;
//		HalOS_SemaphorePost(id);
//	}			
//}					
#define HalOS_SemaphoreSet(id,cnt)		do{ while( HalOS_SemaphoreWait(id,0)==HAL_OS_SEM_ERR_NONE ); uint32 sem_cnt = cnt;while(sem_cnt){sem_cnt--;HalOS_SemaphorePost(id);}}while(0) /*lint !e722 !e681*/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
//uint32 HalOS_GetKernelTick  (void);
#define HalOS_GetKernelTick()		osKernelSysTick()






#ifdef  __cplusplus
}
#endif
 
#endif


