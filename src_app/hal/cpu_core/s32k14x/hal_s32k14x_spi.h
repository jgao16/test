/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    s32k SPI driver api
    All rights reserved
>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#ifndef __HAL_S32K14X_SPI_H__
#define __HAL_S32K14X_SPI_H__

#ifdef  __cplusplus
extern "C"
{
#endif

/*****************************************************************************
 *  Include Files
 *****************************************************************************/
#include "hal_spi.h"


/*****************************************************************************
 *  define
 *****************************************************************************/
#ifndef HW_SPI_THROUGHPUT_MEASURE_EN
#define HW_SPI_THROUGHPUT_MEASURE_EN FALSE
#endif

/*****************************************************************************
 *  Global Function Declarations
 *****************************************************************************/
void 				  HalS32K14x_SpiInit(void);
HAL_SPI_HW_API const *S32K14x_SPI_GetSPIHwApi(void);

#if HW_SPI_THROUGHPUT_MEASURE_EN
void 				  S32K14x_SpIMeasureTickCall(void);
#endif

#ifdef  __cplusplus
}
#endif

#endif
