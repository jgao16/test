/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
s32k DFlash driver api
All rights reserved
>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#ifndef __HAL_S32K14X_DFLASH_H__
#define __HAL_S32K14X_DFLASH_H__

#ifdef  __cplusplus
extern "C"
{
#endif

/*****************************************************************************
 *  Include Files
 *****************************************************************************/
#include "hal_dflash.h"

#define	DFLASH_SIZE	(0x20000)
/*****************************************************************************
 *  Global Function Declarations
 *****************************************************************************/
//void                   HalS32K14x_DFlashInit(void);
HAL_DFLASH_HW_API const *S32K14x_DFlash_GetDFlashHwApi(void);

void 	S32K14x_DflashReadErr(void);
#ifdef  __cplusplus
}
#endif

#endif

