/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
s32k PFlash driver api
All rights reserved
>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#ifndef __HAL_S32K14X_PFLASH_H__
#define __HAL_S32K14X_PFLASH_H__

#ifdef  __cplusplus
extern "C"
{
#endif

/*****************************************************************************
 *  Include Files
 *****************************************************************************/
#include "hal_flash.h"

/*****************************************************************************
 *  Global Function Declarations
 *****************************************************************************/
HAL_FLASH_HW_API const *S32K14x_PFlash_GetPFlashHwApi(void);

#ifdef  __cplusplus
}
#endif

#endif
