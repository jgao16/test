/*****************************************************************************
 *
 * Copyright (c) 
 *
 *****************************************************************************/
 

 /*****************************************************************************
 *  Include Files
 *****************************************************************************/
#include "std_type.h" 

//#include "hal_gpio.h"
#include "hal_wdt.h"

#include "cpu.h"

#include "std_lib.h"

/*****************************************************************************
 *  Local Macro Definitions
 *****************************************************************************/
#define WDOG_UNLOCK_KEY     (0xD928C520UL)
#define WDOG_CLK_FROM_BUS   (0x0UL)
#define WDOG_CLK_FROM_LPO   (0x1UL)
#define WDOG_CLK_FROM_SOSC  (0x2UL)
#define WDOG_CLK_FROM_SIRC  (0x3UL)

 /*****************************************************************************
 *  Local Type Declarations
 *****************************************************************************/



 
 /*****************************************************************************
 *  Global Variable Definitions (Global for the whole system)
 *****************************************************************************/

 
/*****************************************************************************
 *  Local Variable Definitions (Global within this file)
 *****************************************************************************/
static boolean mounted;

static void	S32K14x_Wdt_Init(void);
static void	S32K14x_Wdt_Feed(void);
static void	S32K14x_Wdt_RstCpu(void);
static HAL_WDT_HW_API const api = 
{
 	.init 	=	S32K14x_Wdt_Init,
 	.feed	=	S32K14x_Wdt_Feed,
 	.rst_cpu=	S32K14x_Wdt_RstCpu,
};
 
 /*****************************************************************************
 *  Global Function Declarations
 *****************************************************************************/


/*****************************************************************************
 *  Local Function Declarations
*****************************************************************************/

/*****************************************************************************/
/*  Global Function Definitions                                              */
/*****************************************************************************/

void	HalS32K14x_Wdt_Mount(void)
{
	mounted = TRUE;
}

HAL_WDT_HW_API const * HalS32K14x_Wdt_GetApi(void)
{
	return &api;
}


/*****************************************************************************
 *  Local Function Definitions
 *****************************************************************************/

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void    S32K14x_Wdt_Init ( void )
{
	if ( mounted )
	{
		CPU_SR_ALLOC();
		CPU_CRITICAL_ENTER();/* disable global interrupt */
				
		/*inital write of wdog config register; clock select from lpo / 256, update disable, watchdog enable */
		/*in the cs reg, bit 13 is reserved but if it is not written the wdog can't be disabled*/
		WDOG->CNT = (uint32_t ) WDOG_UNLOCK_KEY;
		while ( !(WDOG->CS & WDOG_CS_ULK_MASK) )
		{}
		WDOG->CS =  (uint32_t ) ((1 << 13) | (WDOG_CLK_FROM_LPO << WDOG_CS_CLK_SHIFT) | (1 << WDOG_CS_EN_SHIFT)| 
								 /*(0 << WDOG_CS_UPDATE_SHIFT)|*/ (1<<WDOG_CS_WAIT_SHIFT) | (1<<WDOG_CS_PRES_SHIFT) );
		/*write of the wdog unlock key to cnt register, must be done in order to allow any modifications*/
		WDOG->TOVAL = 1000;	// about 2s

		while ( !(WDOG->CS & WDOG_CS_RCS_MASK) )
        {}
		/* restore interrupt	*/	
		CPU_CRITICAL_EXIT();	
	}
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void    S32K14x_Wdt_Feed ( void )
{
	if ( mounted )
	{
		CPU_SR_ALLOC();
		CPU_CRITICAL_ENTER();/* disable global interrupt */

		WDOG->CNT = 0xB480A602;
	
		/* restore interrupt	*/	
		CPU_CRITICAL_EXIT();		
	}
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void	S32K14x_Wdt_RstCpu ( void )
{
    /* pet extern dog than set a system reset */
extern  void  __iar_program_start(void);	//lint -e830 -e526

	S32K14x_Wdt_Feed();	/* pet watch dog */
	
  	NVIC_SystemReset();
	__iar_program_start();
    for ( ;; );	//lint !e722
}




