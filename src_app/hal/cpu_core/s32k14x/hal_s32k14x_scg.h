/******************************************************************************
 * Project          s32k14x platform
 * (c) copyright    2015
 * Company          Visteon
 *                  All rights reserved
 * creation Date    2015-03 
 ******************************************************************************/


#ifndef HAL_S32K14x_SCG_H_
#define HAL_S32K14x_SCG_H_

#ifdef __cplusplus
extern "C" {
#endif 

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
typedef enum
{
	CORE_CLK,
	SYS_CLK,
	BUS_CLK,
	FLASH_CLK,
	SPLLDIV1_CLK,
	SPLLDIV2_CLK,
	FIRCDIV1_CLK,
	FIRCDIV2_CLK,
	SIRCDIV1_CLK,
	SIRCDIV2_CLK,	
	SOSCDIV1_CLK,
	SOSCDIV2_CLK
		
}CLOCK_SOURCE;
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
typedef struct
{
	uint32_t core_clk;
	uint32_t sys_clk;
	uint32_t flash_clk;
	uint32_t bus_clk;
	uint32_t splldiv1_clk;
	uint32_t splldiv2_clk;
	uint32_t fircdiv1_clk;
	uint32_t fircdiv2_clk;
	uint32_t sircdiv1_clk;
	uint32_t sircdiv2_clk;
	uint32_t soscdiv1_clk;
	uint32_t soscdiv2_clk;
}S32K_SCG_CLK;
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


void 		HalS32K14x_ScgInit(void);
uint32_t 	HalS32K14x_ScgGetFreq(CLOCK_SOURCE clk_src);
void 		HalS43K14x_ScgGetAllFreq(S32K_SCG_CLK *clk);

#ifdef __cplusplus
}
#endif


#endif
