/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    s32k eeprom - dflash driver api
    All rights reserved
>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#ifndef __HAL_S32K14X_EEPROM_H__
#define __HAL_S32K14X_EEPROM_H__

#include "hal_eep.h"



/*****************************************************************************
 *  Global Function Declarations
 *****************************************************************************/

HAL_EEP_HW_API const *S32K14x_Eep_GetApi(void);


#if defined(__cplusplus)
}
#endif

/*@}*/ /* End of C90TFS Flash driver APIs*/
/*! @}*/ /* End of addtogroup c90tfs_flash_driver */

#endif  /* __HAL_S32K14X_EEPROM_H__ */
