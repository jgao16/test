#ifndef __HAL_S32K14X_IIC_SLV_H__
#define __HAL_S32K14X_IIC_SLV_H__

#ifdef  __cplusplus
extern "C"
{
#endif

/*****************************************************************************
 *  Include Files
 *****************************************************************************/
#include "hal_iic_slv.h"

/*****************************************************************************
 *  Global Function Declarations
 *****************************************************************************/
HAL_IIC_SLV_HW_API const *S32K14x_IIC_SLV_GetHwApi(void);

#ifdef  __cplusplus
}
#endif

#endif
