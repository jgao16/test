/*****************************************************************************
 *
 * Copyright (c) 
 *
 *****************************************************************************/
 

 /*****************************************************************************
 *  Include Files
 *****************************************************************************/
#include "std_type.h" 
#include "std_lib.h"
//#include "hal_eep.h"
#include "std_lib.h"

#include "hal_s32k14x_scg.h"
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

#define SMC_PMSTAT_RUN_MODE_OK                     (0x00000001)
#define SMC_PMSTAT_HSRUN_MODE_OK                   (0x00000080)
#define SMC_PMCTRL_STOPM_VLP_STOP_MASK             (0x00000002)

#define SCG_SOSCCFG_RANGE_HIGH_FREQ_MASK           (0x00000030)

#define SCG_SOSCDIV_SOSCDIV2_DIV_1_MASK            (0x00000100)
#define SCG_SOSCDIV_SOSCDIV1_DIV_1_MASK            (0x00000001)

#define SCG_PARAM_CLKPRES_SOSC_MASK                (0x00000002)
#define SCG_PARAM_CLKPRES_SPLL_MASK                (0x00000040)

#define SCG_SPLLDIV_DIV2_1_MASK                    (0x00000100)
#define SCG_SPLLDIV_DIV2_2_MASK                    (0x00000200)
#define SCG_SPLLDIV_DIV2_4_MASK                    (0x00000300)
#define SCG_SPLLDIV_DIV1_1_MASK                    (0x00000001)

#define SCG_SPLLCFG_MULT_20_MASK                   (0x00040000)
#define SCG_SPLLCFG_MULT_24_MASK                   (0x00080000)
#define SCG_SPLLCFG_MULT_30_MASK                   (0x000E0000)
#define SCG_SPLLCFG_MULT_36_MASK                   (0x00140000)
#define SCG_SPLLCFG_PREDIV_2_MASK                  (0x00000100)
#define SCG_SPLLCFG_PREDIV_4_MASK                  (0x00000300)
#define SCG_SPLLCFG_PREDIV_8_MASK                  (0x00000700)

#define SCG_RCCR_SCS_SPLL_MASK                     (0x06000000)
#define SCG_RCCR_SCS_SOSC_MASK                     (0x01000000)
#define SCG_RCCR_SCS_FIRC_MASK                     (0x03000000)
#define SCG_RCCR_SCS_RTCO_MASK                     (0x04000000)
#define SCG_RCCR_DIVCORE_2_MASK                    (0x00010000)
#define SCG_RCCR_DIVCORE_4_MASK                    (0x00030000)
#define SCG_RCCR_DIVBUS_2_MASK                     (0x00000010)
#define SCG_RCCR_DIVBUS_4_MASK                     (0x00000030)
#define SCG_RCCR_DIVSLOW_2_MASK                    (0x00000001)
#define SCG_RCCR_DIVSLOW_3_MASK                    (0x00000002)
#define SCG_RCCR_DIVSLOW_4_MASK                    (0x00000003)
#define SCG_RCCR_DIVSLOW_6_MASK                    (0x00000005)

#define SCG_HCCR_SCS_SPLL_MASK                     (0x06000000)
#define SCG_HCCR_SCS_SOSC_MASK                     (0x01000000)
#define SCG_HCCR_SCS_FIRC_MASK                     (0x03000000)
#define SCG_HCCR_SCS_RTCO_MASK                     (0x04000000)
#define SCG_HCCR_DIVCORE_2_MASK                    (0x00010000)
#define SCG_HCCR_DIVCORE_4_MASK                    (0x00030000)
#define SCG_HCCR_DIVBUS_2_MASK                     (0x00000010)
#define SCG_HCCR_DIVBUS_4_MASK                     (0x00000030)
#define SCG_HCCR_DIVSLOW_2_MASK                    (0x00000001)
#define SCG_HCCR_DIVSLOW_4_MASK                    (0x00000003)


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void 		HalS32K14x_ScgInit(void)
{
	/* SMC_PMPROT default,HSRUN mode not allowed, VLPS,VLPR and VLPW mode not allowed */
	/* SMC_PMCTRL default, use Normal RUN mode */
	SMC->PMPROT   = SMC_PMPROT_AVLP_MASK;                                     //allow VLPS mode
	SMC->STOPCTRL = SMC_STOPCTRL_STOPO(1);                                    //1:BOTH system and bus clock disable;2:system clock disable, bus clock enabled
	SMC->PMCTRL  |= SMC_PMCTRL_STOPM_VLP_STOP_MASK;//+ SMC_PMCTRL_RUNM_MASK;  //select very-low-power stop
	//while (!(SMC->PMSTAT & SMC_PMSTAT_HSRUN_MODE_OK));                        //wait HSRUN ok.
	while (!(SMC->PMSTAT & SMC_PMSTAT_RUN_MODE_OK)){ ; }                      //wait RUN ok.

	SCG->SIRCCSR &= ~(SCG_SIRCCSR_SIRCLPEN_MASK + SCG_SIRCCSR_SIRCEN_MASK);   //disable SIRC

	/* set up SOSC */
	while (!((SCG->PARAM & SCG_PARAM_CLKPRES_MASK) & 0x02)){ ; }			  //wait SOSC present
	SCG->SOSCCSR  = SCG_SOSCCSR_SOSCERR_MASK;								  //clear System OSC Clock Error
	SCG->SOSCDIV |= SCG_SOSCDIV_SOSCDIV2_DIV_1_MASK + SCG_SOSCDIV_SOSCDIV1_DIV_1_MASK;
	SCG->RCCR	  = SCG_RCCR_SCS_SOSC_MASK;									  //select sosc mode
	//SCG->HCCR     = SCG_HCCR_SCS_SOSC_MASK;									  //select sosc mode

	SCG->SOSCCFG &= ~(SCG_SOSCCFG_RANGE_MASK);
	SCG->SOSCCFG |= SCG_SOSCCFG_RANGE_HIGH_FREQ_MASK | SCG_SOSCCFG_HGO_MASK;  //high frequency range selected, high-power operation
	SCG->SOSCCFG |= SCG_SOSCCFG_EREFS_MASK; 								  //use external OSC
	SCG->SOSCCSR |= SCG_SOSCCSR_SOSCEN_MASK;								  //system osc enabled
	while (!(SCG->SOSCCSR & SCG_SOSCCSR_SOSCVLD_MASK)){ ; }					  //wait SOSC enable

	/* set up SPLL */
	SCG->SPLLCFG |= SCG_SPLLCFG_MULT_36_MASK + SCG_SPLLCFG_PREDIV_2_MASK;	  //clock source is System OSC,mul=36,prediv=2, so sys PLL=36/2/2*16=144MHz
	SCG->SPLLDIV |= SCG_SPLLDIV_DIV2_4_MASK + SCG_SPLLDIV_DIV1_1_MASK;        //spll div2=36MHz
	SCG->SPLLCSR |= SCG_SPLLCSR_SPLLERR_MASK;								  //clear spll err flag
	SCG->SPLLCSR |= SCG_SPLLCSR_SPLLEN_MASK;								  //SPLL enable
	while (!(SCG->SPLLCSR & SCG_SPLLCSR_SPLLVLD_MASK)){ ; }                   //wait system clock being system clock source.

	/* Set up clock sources */
	SCG->RCCR	  = SCG_RCCR_SCS_SPLL_MASK    //select System PLL source as MCU clock
				  + SCG_RCCR_DIVCORE_2_MASK   //core clock divide by SPLL_CLK=2,72MHz
				  + SCG_RCCR_DIVBUS_2_MASK    //bus clock divide by CORE_CLK=2,36MHz
				  + SCG_RCCR_DIVSLOW_3_MASK;  //slow clock divide by CORE_CLK=3,24MHz,like Flash clock
	//SCG->HCCR	  = SCG_HCCR_SCS_SPLL_MASK + SCG_HCCR_DIVSLOW_2_MASK;         //select System PLL source as MCU clock

	/* For s32k14x cache enable */	
	//LMEM->PCCCR = 0x05000000;
	/* set ccr[go] bit to initiate command to invalidate cache */
	//LMEM->PCCCR |= LMEM_PCCCR_GO(1);	//lint -e835
	/* wait until the ccr[go] bit clears to indicate command complete */
	//while((LMEM->PCCCR & LMEM_PCCCR_GO_MASK) == LMEM_PCCCR_GO_MASK){};
	/* enable write buffer */
	//LMEM->PCCCR |= 1;
	/* enable cache */
	//LMEM->PCCCR |= LMEM_PCCCR_ENCACHE(1);
}


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
uint32_t 	HalS32K14x_ScgGetFreq(CLOCK_SOURCE clk_src)
{	
	uint32_t clk = 0;
	switch(clk_src)
	{
	case CORE_CLK:
		clk = 72000000;
		break;
	case SPLLDIV2_CLK:
		clk = 36000000;
		break;
	default:
		break;
	}
	return clk;
}


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void 		HalS43K14x_ScgGetAllFreq(S32K_SCG_CLK *clk)
{
	clk = clk;
	if ( clk != NULL )
	{

	}
}




