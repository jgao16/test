/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    s32k SPI driver api
    All rights reserved
>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#ifndef __HAL_S32K14X_IIC_H__
#define __HAL_S32K14X_IIC_H__

#ifdef  __cplusplus
extern "C"
{
#endif

/*****************************************************************************
 *  Include Files
 *****************************************************************************/
#include "hal_iic.h"


/*****************************************************************************
 *  Global Function Declarations
 *****************************************************************************/
void 				  HalS32K14x_IICInit(void);
HAL_IIC_HW_API const *S32K14x_IIC_GetHwApi(void);

#ifdef  __cplusplus
}
#endif

#endif
