/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
//eDMA moudle
#include "std_lib.h"
#include "std_type.h"

#include "hal_int.h"
#include "hal_s32k14x_edma.h"
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#define DMA_MAX_CHANNEL_COUNT                               (16)                        //DMA channel max counter

#define EDMA_TCD_CH_INTER                                   (0x20)                      //offset between two adjcent TCD channels for eDMA
#define EDMA_TCD_START_ADDR                                 (DMA_BASE + 0x1000)         //TCD[0] start address for eDMA

#define EDMA_TCD_CSR_INTMAJOR_MASK                          (0x0002)                    //bit 1
#define EDMA_TCD_CSR_DREQ_MASK                              (0x0008)                    //bit 7
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
//TCD definition
typedef struct
{
    __IO uint32_t SADDR;
    __IO uint16_t SOFF;
    __IO uint16_t ATTR;
    union {
    __IO uint32_t MLNO;
    __IO uint32_t MLOFFNO;
    __IO uint32_t MLOFFYES;
    }NBYTES;
    __IO uint32_t SLAST;
    __IO uint32_t DADDR;
    __IO uint16_t DOFF;
    union {
    __IO uint16_t ELINKNO;
    __IO uint16_t ELINKYES;
    }CITER;
    __IO uint32_t DLASTSGA;
    __IO uint16_t CSR;
    union {
    __IO uint16_t ELINKNO;
    __IO uint16_t ELINKYES;
    }BITER;
}TCD_DEF;

typedef void (*DMA_CH_ISR)(void);  //DMA channel isr entry.

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void HalS32k14x_eDMAErrIsr(void);

static void HalS32k14x_eDMACh0Isr(void);
static void HalS32k14x_eDMACh1Isr(void);
static void HalS32k14x_eDMACh2Isr(void);
static void HalS32k14x_eDMACh3Isr(void);
static void HalS32k14x_eDMACh4Isr(void);
static void HalS32k14x_eDMACh5Isr(void);
static void HalS32k14x_eDMACh6Isr(void);
static void HalS32k14x_eDMACh7Isr(void);
static void HalS32k14x_eDMACh8Isr(void);
static void HalS32k14x_eDMACh9Isr(void);
static void HalS32k14x_eDMACh10Isr(void);
static void HalS32k14x_eDMACh11Isr(void);
static void HalS32k14x_eDMACh12Isr(void);
static void HalS32k14x_eDMACh13Isr(void);
static void HalS32k14x_eDMACh14Isr(void);
static void HalS32k14x_eDMACh15Isr(void);
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static EDMA_USER_ISR edma_user_isr[DMA_MAX_CHANNEL_COUNT]; //user callback for DMA, max 16

static DMA_CH_ISR const dma_ch_isr[] = 
{
    HalS32k14x_eDMACh0Isr,
    HalS32k14x_eDMACh1Isr,
    HalS32k14x_eDMACh2Isr,
    HalS32k14x_eDMACh3Isr,
    HalS32k14x_eDMACh4Isr,
    HalS32k14x_eDMACh5Isr,
    HalS32k14x_eDMACh6Isr,
    HalS32k14x_eDMACh7Isr,
    HalS32k14x_eDMACh8Isr,
    HalS32k14x_eDMACh9Isr,
    HalS32k14x_eDMACh10Isr,
    HalS32k14x_eDMACh11Isr,
    HalS32k14x_eDMACh12Isr,
    HalS32k14x_eDMACh13Isr,
    HalS32k14x_eDMACh14Isr,
    HalS32k14x_eDMACh15Isr,
};
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void HalS32k14x_eDMAInit(void) //init+channel
{
    //DMAMUX clock init
    __IO uint32_t *pcc = PCC->PCCn + PCC_DMAMUX0_INDEX;
	if ((NULL != pcc) && ((*pcc) & PCC_PCCn_PR_MASK))
	{
		*pcc = PCC_PCCn_CGC_MASK; //open DMAMUX pcc clock
	}

	DMA->CR |= DMA_CR_ERCA_MASK; //Round robin arbitration
	DMA->EEI = 0;
	//TCD init
	for (uint8 i = 0; i < DMA_MAX_CHANNEL_COUNT; i++)
	{
		TCD_DEF *tcd = (TCD_DEF *)(EDMA_TCD_START_ADDR + EDMA_TCD_CH_INTER * i);
		tcd->SADDR = 0u;
		tcd->SOFF = 0u;
		tcd->ATTR = 0u;
		tcd->NBYTES.MLNO = 0u;
		tcd->SLAST = 0u;
		tcd->DADDR = 0u;
		tcd->DOFF = 0u;
		tcd->CITER.ELINKNO = 0u;
		tcd->DLASTSGA = 0u;
		tcd->CSR = 0u;
		tcd->BITER.ELINKNO = 0u;
	}

	for (uint8 i = 0; i < DMA_MAX_CHANNEL_COUNT; i++)
	{
		edma_user_isr[i] = NULL;
	}
}
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void HalS32K14x_eDMAChInit(uint8 channel)
{
    if (channel < DMA_MAX_CHANNEL_COUNT)
    {
        TCD_DEF *tcd = (TCD_DEF *)(EDMA_TCD_START_ADDR + EDMA_TCD_CH_INTER * channel);
        tcd->SADDR = 0u;
        tcd->SOFF = 0u;
        tcd->ATTR = 0u;
        tcd->NBYTES.MLNO = 0u;
        tcd->SLAST = 0u;
        tcd->DADDR = 0u;
        tcd->DOFF = 0u;
        tcd->CITER.ELINKNO = 0u;
        tcd->DLASTSGA = 0u;
        tcd->CSR = 0u;
        tcd->BITER.ELINKNO = 0u;

        DMA->ERR = 1u << channel; //clear error flags
    }
}
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void HalS32K14x_eDMAConfigSourceChannel(uint8 channel, DMA_REQ_SOURCE source)
{
	if ((source < EDMA_REQ_MAX) && (channel < DMA_MAX_CHANNEL_COUNT))
	{
        DMAMUX->CHCFG[channel] = 0x00;
		DMAMUX->CHCFG[channel] = DMAMUX_CHCFG_ENBL_MASK | (uint8)source;
	}
}
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void HalS32K14x_eDMAEnableErrIsr(void)
{
    HalInt_VectSet(DMA_Error_IRQn, HalS32k14x_eDMAErrIsr);
    HalInt_En(DMA_Error_IRQn);
}
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void HalS32K14x_eDMADisableErrIsr(void)
{
    HalInt_Dis(DMA_Error_IRQn);
}
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void HalS32K14x_eDMAEnableChErrInt(uint8 channel)
{
    DMA->EEI = 1u << channel;
}
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void HalS32K14x_eDMASetChannelIsr(uint8 channel, EDMA_USER_ISR user_isr)
{
    if (channel < DMA_MAX_CHANNEL_COUNT)
    {
        uint8 chanl_id = channel + (uint8)DMA0_IRQn;
        DMA_CH_ISR isr =  dma_ch_isr[channel];
        HalInt_VectSet(chanl_id, isr);
        HalInt_En(chanl_id);

        edma_user_isr[channel] = user_isr;
    }
}
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void HalS32K14x_eDMADisChannelIsr(uint8 channel)
{
    if (channel < DMA_MAX_CHANNEL_COUNT)
    {
        uint8 chanl_id = channel + (uint8)DMA0_IRQn;
        HalInt_Dis(chanl_id);
    }
}
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void HalS32K14x_eDMASetTCDSrcDestAddr(uint8 channel, uint32 src_addr, uint32 dest_addr)
{
    if (channel < DMA_MAX_CHANNEL_COUNT)
    {
        TCD_DEF *tcd =  (TCD_DEF *)(EDMA_TCD_START_ADDR + EDMA_TCD_CH_INTER * channel);
        if (NULL != tcd)
        {
            tcd->SADDR = src_addr;
            tcd->DADDR = dest_addr;
        }
    }
}
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
//dat_len:total bytes count, dat_size: per data size(8,16,32-bit...)
void HalS32K14x_eDMASetTCDFrameAttr(uint8 channel, const uint16 dat_len, uint8 dat_size)
{
    if (channel < DMA_MAX_CHANNEL_COUNT)
    {
        TCD_DEF *tcd =  (TCD_DEF *)(EDMA_TCD_START_ADDR + EDMA_TCD_CH_INTER * channel);
        if (NULL != tcd)
        {
            if ((1 == dat_size) || (2 == dat_size) || (4 == dat_size))
            {
                if (dat_len >= dat_size)
                {
                    tcd->ATTR          = ((uint32)(dat_size / 2)) << 8 | (dat_size / 2);    //8/16/32bit
                    tcd->NBYTES.MLNO   = dat_size;                                          //minor loop count(EMLM = 0)
                    tcd->BITER.ELINKNO = dat_len / dat_size;                                //beginning major loop count
                    tcd->CITER.ELINKNO = dat_len / dat_size;                                //current major loop count
                }
                else
                {
                    tcd->ATTR          = 0x0000;                                            //8bit
                    tcd->NBYTES.MLNO   = dat_len;                                           //minor loop count(EMLM = 0)
                    tcd->BITER.ELINKNO = 1;                                                 //beginning major loop count
                    tcd->CITER.ELINKNO = 1;                                                 //current major loop count
                }
            }
            else //invalid framesize
            {
                tcd->ATTR          = 0u;
                tcd->NBYTES.MLNO   = 0u;
                tcd->BITER.ELINKNO = 0u;
                tcd->CITER.ELINKNO = 0u;
            }
        }
    }
}
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void HalS32K14x_eDMASetTCDSOFF(uint8 channel, uint8 dat_size)
{
    if ((channel < DMA_MAX_CHANNEL_COUNT) && (dat_size < 8))
    {
        TCD_DEF *tcd =  (TCD_DEF *)(EDMA_TCD_START_ADDR + EDMA_TCD_CH_INTER * channel);
        if (NULL != tcd)
        {
            if ((1 == dat_size) || (2 == dat_size) || (4 == dat_size))
            {
                tcd->SOFF = dat_size;
            }
            else
            {
                tcd->SOFF = 0u;
            }
        }
    }
}
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void HalS32K14x_eDMASetTCDDOFF(uint8 channel, uint8 dat_size)
{
    if ((channel < DMA_MAX_CHANNEL_COUNT) && (dat_size < 8))
    {
        TCD_DEF *tcd =  (TCD_DEF *)(EDMA_TCD_START_ADDR + EDMA_TCD_CH_INTER * channel);
        if (NULL != tcd)
        {
            if ((1 == dat_size) || (2 == dat_size) || (4 == dat_size))
            {
                tcd->DOFF = dat_size;
            }
            else
            {
                tcd->DOFF = 0u;
            }
        }
    }
}
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void HalS32K14x_eDMASetLastSignedAddr   (uint8 channel, uint32 src_addr, uint32 dest_addr)
{
    if (channel < DMA_MAX_CHANNEL_COUNT)
    {
        TCD_DEF *tcd =  (TCD_DEF *)(EDMA_TCD_START_ADDR + EDMA_TCD_CH_INTER * channel);
        if (NULL != tcd)
        {
            tcd->SLAST      = src_addr;
            tcd->DLASTSGA   = dest_addr;
        }
    }
}
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void HalS32k14x_eDMAReqTrigTypeSet(uint8 channel, boolean inter_trig) //TRUE:interrupt
{
    if (channel < DMA_MAX_CHANNEL_COUNT)
    {
        TCD_DEF *tcd =  (TCD_DEF *)(EDMA_TCD_START_ADDR + EDMA_TCD_CH_INTER * channel);
        if (NULL != tcd)
        {
            if (inter_trig) //use DMA interrupt 
            {
                tcd->CSR |= EDMA_TCD_CSR_DREQ_MASK;        //DREQ = 1
                tcd->CSR |= EDMA_TCD_CSR_INTMAJOR_MASK;    //INTMAJOR = 1
            }
            else
            {
                tcd->CSR |= EDMA_TCD_CSR_DREQ_MASK;
                tcd->CSR &= ~EDMA_TCD_CSR_INTMAJOR_MASK;
            }
        }
    }
}
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void HalS32k14x_eDMAERQSet(uint8 channel, boolean onoff)
{
    if (channel < DMA_MAX_CHANNEL_COUNT)
    {
        if (onoff)
        {
            DMA->ERQ |= 1u << channel;
        }
        else
        {
            DMA->ERQ &= ~(1u << channel);
        }
    }
}
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
uint32 HalS32k14x_eDMAGetTransferedLen(uint8 channel)
{
    uint32 len = 0u;
    if (channel < DMA_MAX_CHANNEL_COUNT)
    {
        TCD_DEF *tcd =  (TCD_DEF *)(EDMA_TCD_START_ADDR + EDMA_TCD_CH_INTER * channel);
        if (NULL != tcd)
        {
            uint16_t begin_cnt = tcd->BITER.ELINKNO;
            uint16_t cur_cnt   = tcd->CITER.ELINKNO;
            uint32_t nbytes    = tcd->NBYTES.MLNO;
            len                = (uint32)((begin_cnt - cur_cnt) * nbytes); //data numbers has been transfered.
        }
    }

    return len;
}
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
boolean HalS32k14x_eDMAIsERQValid(uint8 channel)
{
    boolean is_vaild = FALSE;
    if (channel < DMA_MAX_CHANNEL_COUNT)
    {
        is_vaild = !!(DMA->ERQ & (1u << channel));    
    }
    return is_vaild;
}
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void HalS32k14x_eDMAErrIsr(void)
{
    for (uint8 i = 0; i < DMA_MAX_CHANNEL_COUNT; i++)
    {
        if ((DMA->ERR) & (1u << i))
        {
            DMA->EEI &= ~(1u << i); //disable channel err interrupt
            DMA->ERR = 1u << i;     //clear error flags
        }
    }
}
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void HalS32k14x_eDMACh0Isr(void)
{
    TCD_DEF *tcd =  (TCD_DEF *)(EDMA_TCD_START_ADDR);
    tcd->CSR &= ~(EDMA_TCD_CSR_INTMAJOR_MASK | EDMA_TCD_CSR_DREQ_MASK);    //INTMAJOR = 0, DREQ = 0

    DMA->INT = 1u << EDMA_CH_0;

    if (NULL != edma_user_isr[EDMA_CH_0])
    {
        edma_user_isr[EDMA_CH_0]();
    }
}
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void HalS32k14x_eDMACh1Isr(void)
{
    TCD_DEF *tcd =  (TCD_DEF *)(EDMA_TCD_START_ADDR + EDMA_TCD_CH_INTER * EDMA_CH_1);
    tcd->CSR &= ~(EDMA_TCD_CSR_INTMAJOR_MASK | EDMA_TCD_CSR_DREQ_MASK);    //INTMAJOR = 0, DREQ = 0

    DMA->INT = 1u << EDMA_CH_1;

    if (NULL != edma_user_isr[EDMA_CH_1])
    {
        edma_user_isr[EDMA_CH_1]();
    }
}
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void HalS32k14x_eDMACh2Isr(void)
{
    TCD_DEF *tcd =  (TCD_DEF *)(EDMA_TCD_START_ADDR + EDMA_TCD_CH_INTER * EDMA_CH_2);
    tcd->CSR &= ~(EDMA_TCD_CSR_INTMAJOR_MASK | EDMA_TCD_CSR_DREQ_MASK);    //INTMAJOR = 0, DREQ = 0

    DMA->INT = 1u << EDMA_CH_2;

    if (NULL != edma_user_isr[EDMA_CH_2])
    {
        edma_user_isr[EDMA_CH_2]();
    }
}
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void HalS32k14x_eDMACh3Isr(void)
{
    TCD_DEF *tcd =  (TCD_DEF *)(EDMA_TCD_START_ADDR + EDMA_TCD_CH_INTER * EDMA_CH_3);
    tcd->CSR &= ~(EDMA_TCD_CSR_INTMAJOR_MASK | EDMA_TCD_CSR_DREQ_MASK);    //INTMAJOR = 0, DREQ = 0

    DMA->INT = 1u << EDMA_CH_3;

    if (NULL != edma_user_isr[EDMA_CH_3])
    {
        edma_user_isr[EDMA_CH_3]();
    }
}
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void HalS32k14x_eDMACh4Isr(void)
{
    TCD_DEF *tcd =  (TCD_DEF *)(EDMA_TCD_START_ADDR + EDMA_TCD_CH_INTER * EDMA_CH_4);
    tcd->CSR &= ~(EDMA_TCD_CSR_INTMAJOR_MASK | EDMA_TCD_CSR_DREQ_MASK);    //INTMAJOR = 0, DREQ = 0

	DMA->INT = 1u << EDMA_CH_4;

    if (NULL != edma_user_isr[EDMA_CH_4])
    {
        edma_user_isr[EDMA_CH_4]();
    }
}
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void HalS32k14x_eDMACh5Isr(void)
{
    TCD_DEF *tcd =  (TCD_DEF *)(EDMA_TCD_START_ADDR + EDMA_TCD_CH_INTER * EDMA_CH_5);
    tcd->CSR &= ~(EDMA_TCD_CSR_INTMAJOR_MASK | EDMA_TCD_CSR_DREQ_MASK);    //INTMAJOR = 0, DREQ = 0

	DMA->INT = 1u << EDMA_CH_5;

	if (NULL != edma_user_isr[EDMA_CH_5])
    {
		edma_user_isr[EDMA_CH_5]();
    }
}
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void HalS32k14x_eDMACh6Isr(void)
{
    TCD_DEF *tcd =  (TCD_DEF *)(EDMA_TCD_START_ADDR + EDMA_TCD_CH_INTER * EDMA_CH_6);
    tcd->CSR &= ~(EDMA_TCD_CSR_INTMAJOR_MASK | EDMA_TCD_CSR_DREQ_MASK);    //INTMAJOR = 0, DREQ = 0

	DMA->INT = 1u << EDMA_CH_6;

	if (NULL != edma_user_isr[EDMA_CH_6])
    {
		edma_user_isr[EDMA_CH_6]();
    }
}
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void HalS32k14x_eDMACh7Isr(void)
{
    TCD_DEF *tcd =  (TCD_DEF *)(EDMA_TCD_START_ADDR + EDMA_TCD_CH_INTER * EDMA_CH_7);
    tcd->CSR &= ~(EDMA_TCD_CSR_INTMAJOR_MASK | EDMA_TCD_CSR_DREQ_MASK);    //INTMAJOR = 0, DREQ = 0

	DMA->INT = 1u << EDMA_CH_7;

	if (NULL != edma_user_isr[EDMA_CH_7])
    {
		edma_user_isr[EDMA_CH_7]();
    }
}
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void HalS32k14x_eDMACh8Isr(void)
{
    TCD_DEF *tcd =  (TCD_DEF *)(EDMA_TCD_START_ADDR + EDMA_TCD_CH_INTER * EDMA_CH_8);
    tcd->CSR &= ~(EDMA_TCD_CSR_INTMAJOR_MASK | EDMA_TCD_CSR_DREQ_MASK);    //INTMAJOR = 0, DREQ = 0

	DMA->INT = 1u << EDMA_CH_8;

	if (NULL != edma_user_isr[EDMA_CH_8])
    {
		edma_user_isr[EDMA_CH_8]();
    }
}
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void HalS32k14x_eDMACh9Isr(void)
{
    TCD_DEF *tcd =  (TCD_DEF *)(EDMA_TCD_START_ADDR + EDMA_TCD_CH_INTER * EDMA_CH_9);
    tcd->CSR &= ~(EDMA_TCD_CSR_INTMAJOR_MASK | EDMA_TCD_CSR_DREQ_MASK);    //INTMAJOR = 0, DREQ = 0

	DMA->INT = 1u << EDMA_CH_9;

	if (NULL != edma_user_isr[EDMA_CH_9])
    {
		edma_user_isr[EDMA_CH_9]();
    }
}
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void HalS32k14x_eDMACh10Isr(void)
{
    TCD_DEF *tcd =  (TCD_DEF *)(EDMA_TCD_START_ADDR + EDMA_TCD_CH_INTER * EDMA_CH_10);
    tcd->CSR &= ~(EDMA_TCD_CSR_INTMAJOR_MASK | EDMA_TCD_CSR_DREQ_MASK);    //INTMAJOR = 0, DREQ = 0

	DMA->INT = 1u << EDMA_CH_10;

	if (NULL != edma_user_isr[EDMA_CH_10])
    {
		edma_user_isr[EDMA_CH_10]();
    }
}
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void HalS32k14x_eDMACh11Isr(void)
{
    TCD_DEF *tcd =  (TCD_DEF *)(EDMA_TCD_START_ADDR + EDMA_TCD_CH_INTER * EDMA_CH_11);
    tcd->CSR &= ~(EDMA_TCD_CSR_INTMAJOR_MASK | EDMA_TCD_CSR_DREQ_MASK);    //INTMAJOR = 0, DREQ = 0

	DMA->INT = 1u << EDMA_CH_11;

	if (NULL != edma_user_isr[EDMA_CH_11])
    {
		edma_user_isr[EDMA_CH_11]();
    }
}
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void HalS32k14x_eDMACh12Isr(void)
{
    TCD_DEF *tcd =  (TCD_DEF *)(EDMA_TCD_START_ADDR + EDMA_TCD_CH_INTER * EDMA_CH_12);
    tcd->CSR &= ~(EDMA_TCD_CSR_INTMAJOR_MASK | EDMA_TCD_CSR_DREQ_MASK);    //INTMAJOR = 0, DREQ = 0

	DMA->INT = 1u << EDMA_CH_12;

	if (NULL != edma_user_isr[EDMA_CH_12])
    {
		edma_user_isr[EDMA_CH_12]();
    }
}
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void HalS32k14x_eDMACh13Isr(void)
{
    TCD_DEF *tcd =  (TCD_DEF *)(EDMA_TCD_START_ADDR + EDMA_TCD_CH_INTER * EDMA_CH_13);
    tcd->CSR &= ~(EDMA_TCD_CSR_INTMAJOR_MASK | EDMA_TCD_CSR_DREQ_MASK);    //INTMAJOR = 0, DREQ = 0

	DMA->INT = 1u << EDMA_CH_13;

	if (NULL != edma_user_isr[EDMA_CH_13])
    {
		edma_user_isr[EDMA_CH_13]();
    }
}
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void HalS32k14x_eDMACh14Isr(void)
{
    TCD_DEF *tcd =  (TCD_DEF *)(EDMA_TCD_START_ADDR + EDMA_TCD_CH_INTER * EDMA_CH_14);
    tcd->CSR &= ~(EDMA_TCD_CSR_INTMAJOR_MASK | EDMA_TCD_CSR_DREQ_MASK);    //INTMAJOR = 0, DREQ = 0

	DMA->INT = 1u << EDMA_CH_14;

	if (NULL != edma_user_isr[EDMA_CH_14])
    {
		edma_user_isr[EDMA_CH_14]();
    }
}
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void HalS32k14x_eDMACh15Isr(void)
{
    TCD_DEF *tcd =  (TCD_DEF *)(EDMA_TCD_START_ADDR + EDMA_TCD_CH_INTER * EDMA_CH_15);
    tcd->CSR &= ~(EDMA_TCD_CSR_INTMAJOR_MASK | EDMA_TCD_CSR_DREQ_MASK);    //INTMAJOR = 0, DREQ = 0

	DMA->INT = 1u << EDMA_CH_15;

	if (NULL != edma_user_isr[EDMA_CH_15])
    {
		edma_user_isr[EDMA_CH_15]();
    }
}

