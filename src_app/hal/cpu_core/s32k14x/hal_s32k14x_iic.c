/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#include "std_type.h"
#include "std_lib.h"

#include "hal_iic.h"
#include "hal_gpio.h"
#include "hal_s32k14x_scg.h"
#include "hal_os.h"
#include "hal_hw.h"
#include "hal_int.h"
#include "hal_s32k14x_edma.h"
#include "trace_api.h"
#include "cpu.h"
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#define HW_IIC_NUM                                                 (sizeof(i2c_hw_attr) / sizeof(IIC_HW_ATTR))

#define LPI2C_MSR_CLEAR_ALL_FLAGS                                  (0x00007F00u)
//#define LPI2C_MCFGR3_PINLOWTIME_VAL                                (176u)	// about 3ms
//#define LPI2C_MCFGR3_PINLOWTIME_VAL                                (400u)	// about 5ms
//#define LPI2C_MCFGR3_PINLOWTIME_VAL                                (800u)	// about 10ms
#define LPI2C_MCFGR3_PINLOWTIME_VAL                                (1600u)	// about 20ms

#define LPI2C_RW_BIT                                               (0x01)

#define IIC_SetMTDR(obj,cmd,data)	do{(obj)->MTDR = ((uint32_t)(cmd) << 8u) | (uint32)(data);}while(0)	/*lint -e835*/
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
typedef void(*IIC_INT_ISR)(void);  //for LPI2C hw ISR entry.

typedef enum
{
	MTDRCMD_TRANS_DATA                        = 0,
	MTDRCMD_RECV_BYTES                           ,
	MTDRCMD_GEN_STOP_CONDI                       ,
	MTDRCMD_RECV_AND_DISCARD_BYTES               ,
	MTDRCMD_GEN_START_AND_TRANS_ADDR             ,
	MTDRCMD_GEN_START_AND_TRANS_ADDR_NACK        ,
	MTDRCMD_GEN_START_AND_TRANS_ADDR_HS_MODE     ,
	MTDRCMD_GEN_START_AND_TRANS_ADDR_HS_MODE_NACK
}MTDR_CMD; //MTDR cmd, see datasheet page 1142

typedef enum
{
	IIC_MASTER_IDLE,
	IIC_MSATER_READ,
	IIC_MSATER_WRITE,
	
	IIC_MASTER_STATE_MAX
}IIC_MSATER_STATE;

typedef struct 
{
	LPI2C_Type   * 		obj;
	uint32_t			baud;
	uint8_t				iic_mode;                 /* IIC_MASTER or IIC_SLAVE,only support master now	*/
	
	HAL_PORT_NAME    	scl_port;
	unsigned char      	scl_pin;
	HAL_PORT_NAME    	sda_port;
	unsigned char     	sda_pin;
	uint8_t       		int_num;
	
	boolean             dma_used;                /* use DMA or not,TRUE:use it */
	uint8_t 			dma_ch;
	uint8_t				slave_addr;				// if iic_mode == I2C_SLAVE
}IIC_HW_ATTR;

typedef struct
{
	uint8_t 		 slave_addr;	/* 8 bit */
	
	IIC_MSATER_STATE state;

	boolean			 unlock_req;
	
	uint16_t		 dat_len;
	uint16_t		 dat_cnt;
	
	uint8_t 	*dat;
}DEV_IIC_MSG;
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static const IIC_HW_ATTR i2c_hw_attr[] = 
{
#define CPU_IIC_HW_DEF(iic_name,hw_dev,iic_mode,iic_baud,scl_port,scl_pin,sda_port,sda_pin,int_num,dma_used,dma_ch,slave_address) \
				  {hw_dev,iic_baud,iic_mode,scl_port,scl_pin,sda_port,sda_pin,int_num,dma_used,dma_ch,slave_address},
    #include "cpu_iic.cfg"
#undef  CPU_IIC_HW_DEF
};

static HAL_OS_SEM_ID		 		iic_hw_int[HW_IIC_NUM];
static DEV_IIC_MSG				 	dev_iic_msg[HW_IIC_NUM];

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void 	IIC_HwInit(uint8_t hw_ch);
//static void 	IIC_HwDeInit(uint8_t hw_ch);
static uint16_t	IIC_HwWrite(uint8_t hw_ch, uint8_t slave_addr, uint8_t *dat, uint16_t len);
static uint16_t	IIC_HwRead(uint8_t hw_ch, uint8_t slave_addr, uint8_t *dat, uint16_t len);

static void 	IIC_UnlockBus(IIC_HW_ATTR const * hw_attr);
static void		IIC_InitDrv(IIC_HW_ATTR const * hw_attr);
static void		IIC_IntMasterHandle(uint8_t hw_ch);
static void		IIC_IntSlaveHandle(uint8_t hw_ch);
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#define CPU_IIC_HW_DEF(iic_name,hw_dev,iic_mode,iic_baud,scl_port,scl_pin,sda_port,sda_pin,int_num,dma_used,dma_ch,slave_address) \
	static void IIC_Int_##iic_name(void)	{iic_mode==I2C_MASTER?IIC_IntMasterHandle(iic_name##_ID):IIC_IntSlaveHandle(iic_name##_ID);}
	#include "cpu_iic.cfg"
#undef  CPU_IIC_HW_DEF
static IIC_INT_ISR const iic_int_isr[] = 
{
#define CPU_IIC_HW_DEF(iic_name,hw_dev,iic_mode,iic_baud,scl_port,scl_pin,sda_port,sda_pin,int_num,dma_used,dma_ch,slave_address) \
	IIC_Int_##iic_name,	
	#include "cpu_iic.cfg"
#undef  CPU_IIC_HW_DEF
	
};

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static HAL_IIC_HW_API const hw_api =
{
	.init   = IIC_HwInit,
//	.deinit = IIC_HwDeInit,
	.wr     = IIC_HwWrite,
	.rd     = IIC_HwRead,
};
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
HAL_IIC_HW_API const *S32K14x_IIC_GetHwApi(void)
{
	return &hw_api;
}


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void HalS32K14x_IICInit(void)
{	
	//for( uint8_t i = 0; i < HW_IIC_NUM; i ++ )
	//{
		/* create semphore for interrupt	*/
		//iic_hw_int[i] = HalOS_SemaphoreCreate(0,(const char*)"IIC Int Sem");	
	//}
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void 	IIC_HwInit(uint8_t hw_ch)
{
	if ( hw_ch < HW_IIC_NUM )
	{
		/* create semphore for interrupt	*/
		if ( iic_hw_int[hw_ch] == NULL )
		{
			IIC_HW_ATTR const * hw_attr = i2c_hw_attr+hw_ch;
			__IO uint32_t     * pcc     = NULL;
			
			if ( hw_attr->obj == LPI2C0 )	{
				pcc = PCC->PCCn + PCC_LPI2C0_INDEX;
			}else if( hw_attr->obj == LPI2C1 ){
				pcc = PCC->PCCn + PCC_LPI2C1_INDEX;	
			}else{}

			if ( pcc != NULL && (*pcc&PCC_PCCn_PR_MASK) )
			{	// enable gpio 
				*pcc &= ~PCC_PCCn_CGC_MASK;
				*pcc &= ~PCC_PCCn_PCS_MASK;                    	//clear PCS
				*pcc |= PCC_PCCn_PCS(6);						//select SPLL
				*pcc |= PCC_PCCn_CGC_MASK;						//enable iic clock

				//iic_hw_int[hw_ch] = HalOS_SemaphoreCreate(0,(const char*)"IIC Int Sem");
				iic_hw_int[hw_ch] = HalOS_SemaphoreCreateWithMaxCnt(0,1,(const char*)"IIC Int Sem");
				HalInt_VectSet(hw_attr->int_num,iic_int_isr[hw_ch]);

				IIC_UnlockBus(hw_attr);
				IIC_InitDrv(hw_attr); /* initial i2c device now */

				HalInt_En(hw_attr->int_num);

				dev_iic_msg[hw_ch].slave_addr = 0u;
				dev_iic_msg[hw_ch].dat_len    = 0u;
				dev_iic_msg[hw_ch].dat_cnt    = 0u;
				dev_iic_msg[hw_ch].dat        = NULL;
			}
		}
	}
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
//static void 	IIC_HwDeInit(uint8_t hw_ch)
//{
//	if ( hw_ch < HW_IIC_NUM )
//	{
//		IIC_HW_ATTR const * hw_attr = i2c_hw_attr + hw_ch;
//
//		hw_attr->obj->MSR  = LPI2C_MSR_CLEAR_ALL_FLAGS;     //clear all interrupt flags
//		hw_attr->obj->MIER = 0u;                            //disable all interrupt source
//		HalInt_Dis(hw_attr->int_num);                       //disable iic NVIC ISR
//
//		HalGpio_InitPinGpio_Def(hw_attr->scl_port,  hw_attr->scl_pin,   PORT_OUTPUT,OUTPUT_OPEN_DRAIN,INPUT_NO_PULL);
//		HalGpio_InitPinGpio_Def(hw_attr->sda_port,  hw_attr->sda_pin,   PORT_OUTPUT,OUTPUT_OPEN_DRAIN,INPUT_NO_PULL);
//
//		HalGpio_SetPinVal(hw_attr->sda_port,  hw_attr->sda_pin, LOW);
//		HalGpio_SetPinVal(hw_attr->scl_port,  hw_attr->scl_pin, LOW);
//	}
//}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static uint16_t	IIC_HwWrite(uint8_t hw_name, uint8_t slave_addr, uint8_t *dat, uint16_t len)
{
	uint16_t            wr_len  = 0;
	IIC_HW_ATTR const * hw_attr = i2c_hw_attr + hw_name;
	DEV_IIC_MSG       * msg     = dev_iic_msg + hw_name;

	//TRACE_VALUE2(LOG_DEBUG, MOD_HW, "IIC_HwWrite, hw_name=%d,len=%d", hw_name,hw_name);
	
	if ((NULL != hw_attr) && (NULL != hw_attr->obj) && (NULL != dat) && (len > 0u) /*&& IIC_WaitUntilIdle(i2c_hw_attr+hw_name)*/ )
	{
		msg->slave_addr = slave_addr & (~((uint8_t)LPI2C_RW_BIT)); //write
		msg->dat_len    = len;
		msg->dat_cnt    = 0;
		msg->dat        = dat;
		
		(void)HalOS_SemaphoreWait(iic_hw_int[hw_name],0); // clear semaphore

		if ( msg->unlock_req || !HalGpio_GetPinVal(hw_attr->sda_port,hw_attr->sda_pin) || !HalGpio_GetPinVal(hw_attr->scl_port,hw_attr->scl_pin) ) 
		{
			msg->unlock_req = FALSE;
			
			hw_attr->obj->MCR &= ~LPI2C_MCR_MEN_MASK;
			IIC_UnlockBus(hw_attr);
			hw_attr->obj->MCR |= LPI2C_MCR_MEN_MASK;
			hw_attr->obj->MSR  = LPI2C_MSR_CLEAR_ALL_FLAGS;                //clear all flags
			
			
			HalOS_Delay(2);

			if ( !HalGpio_GetPinVal(hw_attr->sda_port,hw_attr->sda_pin) || !HalGpio_GetPinVal(hw_attr->scl_port,hw_attr->scl_pin) )
			{
				TRACE_VALUE2(LOG_ERR, MOD_HW, "S32K14x iic bus =%d unlock error,MSR=0x%04x,in WRITE", hw_name,(uint16_t)(hw_attr->obj->MSR));
				return 0;
			}
			else
			{
				TRACE_VALUE(LOG_SHOUT, MOD_HW, "S32K14x iic bus =%d unlock succeed in  WRITE", hw_name);
			}
		}
		
		hw_attr->obj->MCR  |= LPI2C_MCR_RRF_MASK | LPI2C_MCR_RTF_MASK;  //receive&transmit fifo is reset
		hw_attr->obj->MSR   = LPI2C_MSR_CLEAR_ALL_FLAGS;                //clear all flags
		
		if (hw_attr->dma_used) //using DMA
		{
			uint8_t tx_channel = EDMA_CH_MAX; //used channel,0~15
			if (hw_attr->obj == LPI2C0)
			{
				if (0x18270000 == SIM->SDID) //0N77P
				{
					HalS32K14x_eDMAConfigSourceChannel(EDMA_CH_6, EDMA_REQ_LPSPI2_RX); //18
				}
				else
				{
					HalS32K14x_eDMAConfigSourceChannel(EDMA_CH_6, EDMA_REQ_LPI2C0_TX); //I2C0_TX-->DMA channel 6. Note: in 0N77P, source id=18
				}
				
				tx_channel = EDMA_CH_6;
			}
			else if (hw_attr->obj == LPI2C1) //only support in S32K148
			{
				HalS32K14x_eDMAConfigSourceChannel(EDMA_CH_8, EDMA_REQ_LPI2C1_TX); //I2C1_TX-->DMA channel 8
				tx_channel = EDMA_CH_8;
			}
			else
			{
			}

			if (tx_channel != EDMA_CH_MAX)
			{
				//edma_tx_ch_used[hw_name] = tx_channel;
				hw_attr->obj->MIER |= LPI2C_MIER_NDIE_MASK   //NACK
									| LPI2C_MIER_PLTIE_MASK  //Pin low timeout
									| LPI2C_MIER_SDIE_MASK   //Stop detection
									| LPI2C_MIER_FEIE_MASK   //FIFO error
									| LPI2C_MIER_ALIE_MASK;  //Abstraction lost

				hw_attr->obj->MDER |= LPI2C_MDER_TDDE_MASK;  //Enable Master Tranmit Data DMA

				HalS32K14x_eDMAChInit(tx_channel);                                                                                     //init TCD
				HalS32K14x_eDMASetTCDSrcDestAddr(tx_channel, (uint32)(msg->dat), (uint32)(&(hw_attr->obj->MTDR)));                     //data-->MTDR
				HalS32K14x_eDMASetTCDSOFF(tx_channel, 1);                                                                              //source offset 1(data)
				HalS32K14x_eDMASetTCDFrameAttr(tx_channel, msg->dat_len, 1);                                                           //DMA NBYTES=1,CITER=BITER=data length
				HalS32K14x_eDMASetTCDDOFF(tx_channel, 0u);                                                                             //destination offset 0(MTDR)
				HalS32k14x_eDMAReqTrigTypeSet(tx_channel, FALSE);                                                                      //tx use auto disable ERQ
				HalS32K14x_eDMAEnableChErrInt(tx_channel);                                                                             //enable DMA channel error interrupt for LPI2C

				IIC_SetMTDR(hw_attr->obj, MTDRCMD_GEN_START_AND_TRANS_ADDR, msg->slave_addr);                                          //transfer slave addr

				HalS32k14x_eDMAERQSet(tx_channel, TRUE);                                                                               //enable DMA channel request for LPI2C
			}
		}
		else
		{
			CPU_SR_ALLOC();
			CPU_CRITICAL_ENTER();
			//hw_attr->obj->MCR 	|= 	LPI2C_MCR_RTF_MASK;     //reset Transmit FIFO.
			//hw_attr->obj->MSR  	=  LPI2C_MSR_CLEAR_ALL_FLAGS;  //clear all interrupt flags
			
			IIC_SetMTDR(hw_attr->obj, MTDRCMD_GEN_START_AND_TRANS_ADDR, msg->slave_addr);   //transfer slave addr
			//do
			//{	// write to data 
			//	IIC_SetMTDR(hw_attr->obj, MTDRCMD_TRANS_DATA, msg->dat[msg->dat_cnt++]); 		//TRANSFER data
			//}while( (uint8_t)(hw_attr->obj->MFSR) < 4 && msg->dat_cnt < msg->dat_len );
			msg->state 		   = IIC_MSATER_WRITE;
			hw_attr->obj->MIER = LPI2C_MIER_TDIE_MASK        //Transmit
								| LPI2C_MIER_NDIE_MASK        //NACK detect
								| LPI2C_MIER_PLTIE_MASK       //PIN low timeout
								| LPI2C_MIER_FEIE_MASK        //FIFO error
								| LPI2C_MIER_ALIE_MASK;       //Abstraction lost
								
			CPU_CRITICAL_EXIT();
			
			HalInt_En(hw_attr->int_num);
		}

		if ( HalOS_SemaphoreWait(iic_hw_int[hw_name],500) == HAL_OS_SEM_ERR_NONE ) 
		{	//500ms at most
			wr_len = msg->dat_len>0?msg->dat_cnt:0;
		}
		else
		{
			TRACE_VALUE2(LOG_ERR, MOD_HW, "iic %d driver WRITE error, 500ms timeout.,MSR=0x%04x", hw_name,(uint16)(hw_attr->obj->MSR));
			wr_len = 0;
		}
		HalInt_Dis(hw_attr->int_num);
		hw_attr->obj->MIER 	= 0x00;	//disable all iic interrupt 
		msg->state 			= IIC_MASTER_IDLE;

		msg->dat_len = 0u;
	}
	
	return wr_len;
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static uint16_t	IIC_HwRead(uint8_t hw_name, uint8_t slave_addr, uint8_t *dat, uint16_t len)
{
	uint16_t              rd_len  = 0;
	IIC_HW_ATTR const * hw_attr = i2c_hw_attr + hw_name;
	DEV_IIC_MSG       * msg     = dev_iic_msg + hw_name;

	//TRACE_VALUE2(LOG_DEBUG, MOD_HW, "IIC_HwRead, hw_name=%d,len=%d", hw_name,hw_name);
	
	if ((NULL != hw_attr) && (NULL != hw_attr->obj) && (NULL != dat) && (len > 0u) /*&& IIC_WaitUntilIdle(i2c_hw_attr+hw_name)*/ )
	{
		msg->slave_addr = slave_addr | ((uint8_t)LPI2C_RW_BIT); //Read
		msg->dat_len    = len;
		msg->dat_cnt    = 0;
		msg->dat        = dat;
		
		(void)HalOS_SemaphoreWait(iic_hw_int[hw_name],0); // clear semaphore

		if ( msg->unlock_req || !HalGpio_GetPinVal(hw_attr->sda_port,hw_attr->sda_pin) || !HalGpio_GetPinVal(hw_attr->scl_port,hw_attr->scl_pin) ) 
		{
			msg->unlock_req = FALSE;
			
			hw_attr->obj->MCR &= ~LPI2C_MCR_MEN_MASK;
			IIC_UnlockBus(hw_attr);
			hw_attr->obj->MCR |= LPI2C_MCR_MEN_MASK;
			hw_attr->obj->MSR  = LPI2C_MSR_CLEAR_ALL_FLAGS;                //clear all flags

			//TRACE_VALUE(LOG_ERR, MOD_HW, "S32K14x iic bus =%d need unlock.. in READ", hw_name);
			HalOS_Delay(2);

			if ( !HalGpio_GetPinVal(hw_attr->sda_port,hw_attr->sda_pin) || !HalGpio_GetPinVal(hw_attr->scl_port,hw_attr->scl_pin) )
			{
				TRACE_VALUE2(LOG_ERR, MOD_HW, "S32K14x iic bus =%d unlock error,MSR=0x%04x,in READ", hw_name,(uint16_t)(hw_attr->obj->MSR));
				return 0;
			}
			else
			{
				TRACE_VALUE(LOG_SHOUT, MOD_HW, "S32K14x iic bus =%d unlock succeed in  READ", hw_name);
			}
		}

		hw_attr->obj->MCR  |= LPI2C_MCR_RRF_MASK | LPI2C_MCR_RTF_MASK;  //receive&transmit fifo is reset
		hw_attr->obj->MSR   = LPI2C_MSR_CLEAR_ALL_FLAGS;                //clear all flags
		//hw_attr->obj->MFCR = 0x00020002;                                //RX, TX watermark=2

		if (hw_attr->dma_used) //using DMA
		{
			uint8_t rx_channel = EDMA_CH_MAX; //used channel,0~15
			if (hw_attr->obj == LPI2C0)
			{
				if (0x18270000 == SIM->SDID) //0N77P
				{
					HalS32K14x_eDMAConfigSourceChannel(EDMA_CH_7, EDMA_REQ_LPSPI2_RX); //18
				}
				else
				{
					HalS32K14x_eDMAConfigSourceChannel(EDMA_CH_7, EDMA_REQ_LPI2C0_RX); //I2C0_TX-->DMA channel 7. Note: in 0N77P, source id=18
				}

				rx_channel = EDMA_CH_7;
			}
			else if (hw_attr->obj == LPI2C1) //only support in S32K148
			{
				HalS32K14x_eDMAConfigSourceChannel(EDMA_CH_9, EDMA_REQ_LPI2C1_RX); //I2C1_TX-->DMA channel 9
				rx_channel = EDMA_CH_9;
			}
			else
			{
			}

			if (rx_channel != EDMA_CH_MAX)
			{
				//edma_rx_ch_used[hw_name] = rx_channel;
				hw_attr->obj->MIER |= LPI2C_MIER_NDIE_MASK   //NACK
									| LPI2C_MIER_PLTIE_MASK  //Pin low timeout
									| LPI2C_MIER_SDIE_MASK   //Stop detection
									| LPI2C_MIER_FEIE_MASK   //FIFO error
									| LPI2C_MIER_ALIE_MASK;  //Abstraction lost

				hw_attr->obj->MDER |= LPI2C_MDER_RDDE_MASK;  //Enable Master Receive Data DMA

				HalS32K14x_eDMAChInit(rx_channel);                                                                                     //init TCD
				HalS32K14x_eDMASetTCDSrcDestAddr(rx_channel, (uint32)(&(hw_attr->obj->MRDR)), (uint32)(msg->dat));                     //MRDR-->data
				HalS32K14x_eDMASetTCDSOFF(rx_channel, 0u);                                                                             //source offset 0(MRDR)
				HalS32K14x_eDMASetTCDFrameAttr(rx_channel, msg->dat_len, 1);                                                           //DMA NBYTES=1,CITER=BITER=data length
				HalS32K14x_eDMASetTCDDOFF(rx_channel, 1);                                                                              //destination offset 1(data)
				HalS32k14x_eDMAReqTrigTypeSet(rx_channel, FALSE);                                                                      //rx use auto disable ERQ
				HalS32K14x_eDMAEnableChErrInt(rx_channel);                                                                             //enable DMA channel error interrupt for LPI2C
				
				IIC_SetMTDR(hw_attr->obj, MTDRCMD_GEN_START_AND_TRANS_ADDR, msg->slave_addr);                                          //transfer slave addr
				IIC_SetMTDR(hw_attr->obj, MTDRCMD_RECV_BYTES, (uint8_t)msg->dat_len - 1);                                                //ready to receive bytes

				HalS32k14x_eDMAERQSet(rx_channel, TRUE);                                                                               //enable DMA channel request for LPI2C
			}
		}
		else
		{
			CPU_SR_ALLOC();
			CPU_CRITICAL_ENTER();
			//hw_attr->obj->MCR 	|= 	LPI2C_MCR_RRF_MASK;     //reset Receive FIFO.
			//hw_attr->obj->MSR  	=  	LPI2C_MSR_CLEAR_ALL_FLAGS;  //clear all interrupt flags

			IIC_SetMTDR(hw_attr->obj, MTDRCMD_GEN_START_AND_TRANS_ADDR, msg->slave_addr);       //transfer slave addr
			IIC_SetMTDR(hw_attr->obj, MTDRCMD_RECV_BYTES, (uint8_t)msg->dat_len - 1);             //ready to receive bytes
			msg->state 		   = IIC_MSATER_READ;
			
			hw_attr->obj->MIER  = LPI2C_MIER_RDIE_MASK        //received
								| LPI2C_MIER_NDIE_MASK        //NACK detect
								| LPI2C_MIER_PLTIE_MASK       //PIN low timeout
								| LPI2C_MIER_FEIE_MASK        //FIFO error
								| LPI2C_MIER_ALIE_MASK;       //Abstraction lost

			CPU_CRITICAL_EXIT();
			HalInt_En(hw_attr->int_num);
		}

		if ( HalOS_SemaphoreWait(iic_hw_int[hw_name],500) == HAL_OS_SEM_ERR_NONE ) 
		{	//500ms at most
			rd_len = msg->dat_len>0?msg->dat_cnt:0;
		}
		else
		{
			TRACE_VALUE2(LOG_ERR, MOD_HW, "iic %d driver READ error, 500ms timeout.,MSR=0x%04x", hw_name,(uint16)(hw_attr->obj->MSR));
			rd_len = 0;
		}
		HalInt_Dis(hw_attr->int_num);
		hw_attr->obj->MIER 	= 0x00;	//disable all iic interrupt 
		msg->state 			= IIC_MASTER_IDLE;

		msg->dat_len = 0u;
	}

	return rd_len;
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void IIC_UnlockBus(IIC_HW_ATTR const * hw_attr)
{
	//if (NULL != hw_attr)
	{
		/* deinit I2c	*/
		HalGpio_InitPinGpio_Def(hw_attr->sda_port,	hw_attr->sda_pin,	PORT_OUTPUT,OUTPUT_OPEN_DRAIN,INPUT_NO_PULL);
		HalGpio_SetPinVal(hw_attr->sda_port, hw_attr->sda_pin,LOW);
		HalGpio_InitPinGpio_Def(hw_attr->scl_port,	hw_attr->scl_pin,	PORT_OUTPUT,OUTPUT_OPEN_DRAIN,INPUT_NO_PULL);

		/* Reset I2C bus 				*/
		for ( uint8_t j = 0; j < 9; j ++ )
		{
			HalGpio_SetPinVal(hw_attr->scl_port, hw_attr->scl_pin,LOW);
			HalGpio_SetPinVal(hw_attr->scl_port, hw_attr->scl_pin,HIGH);
		}
		/* Set Stop Signal to I2C bus 	*/
		HalGpio_SetPinVal(hw_attr->scl_port, hw_attr->scl_pin,LOW);
		HalGpio_SetPinVal(hw_attr->sda_port, hw_attr->sda_pin,LOW);
		HalGpio_SetPinVal(hw_attr->scl_port, hw_attr->scl_pin,HIGH);
		HalGpio_SetPinVal(hw_attr->sda_port, hw_attr->sda_pin,HIGH);

		HalGpio_InitPinGpio_Def(hw_attr->scl_port,	hw_attr->scl_pin,	PORT_AF,OUTPUT_OPEN_DRAIN,INPUT_NO_PULL);
		HalGpio_InitPinGpio_Def(hw_attr->sda_port,	hw_attr->sda_pin,	PORT_AF,OUTPUT_OPEN_DRAIN,INPUT_NO_PULL);

		if (HAL_GPIO_PT_A == hw_attr->scl_port)
		{
			HalGpio_PinAFConfig(hw_attr->scl_port, hw_attr->scl_pin, 3);
		}
		else if ((HAL_GPIO_PT_B == hw_attr->scl_port) || (HAL_GPIO_PT_D == hw_attr->scl_port))
		{
			HalGpio_PinAFConfig(hw_attr->scl_port, hw_attr->scl_pin, 2);
		}
		else if (HAL_GPIO_PT_E == hw_attr->scl_port)
		{
			HalGpio_PinAFConfig(hw_attr->scl_port, hw_attr->scl_pin, 4);
		}
		else
		{
		}

		if (HAL_GPIO_PT_A == hw_attr->sda_port)
		{
			HalGpio_PinAFConfig(hw_attr->sda_port, hw_attr->sda_pin, 3);
		}
		else if ((HAL_GPIO_PT_B == hw_attr->sda_port) || (HAL_GPIO_PT_D == hw_attr->sda_port))
		{
			HalGpio_PinAFConfig(hw_attr->sda_port, hw_attr->sda_pin, 2);
		}
		else if (HAL_GPIO_PT_E == hw_attr->sda_port)
		{
			HalGpio_PinAFConfig(hw_attr->sda_port, hw_attr->sda_pin, 4);
		}
		else
		{
		}
	}	
}

/* >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> */
static void		IIC_InitDrv(IIC_HW_ATTR const * hw_attr)
{
	if ((NULL != hw_attr) && (NULL != hw_attr->obj))
	{
		hw_attr->obj->MCR |= LPI2C_MCR_RST_MASK;                                          //reset master logic
		hw_attr->obj->MCR |= LPI2C_MCR_RRF_MASK + LPI2C_MCR_RTF_MASK;                     //receive&transmit fifo is reset
		hw_attr->obj->MCR &= ~LPI2C_MCR_RST_MASK;                                         //reset master logic

		hw_attr->obj->SCR |= LPI2C_SCR_RST_MASK;                                          //reset slave logic
		hw_attr->obj->SCR &= ~LPI2C_SCR_RST_MASK;                                         //reset slave logic

		if (I2C_MASTER == hw_attr->iic_mode)
		{
			hw_attr->obj->MSR  = LPI2C_MSR_CLEAR_ALL_FLAGS;  //clear all interrupt flags

			uint8_t  pre_div  = 0u;
			uint32 clk_hi   = 0u;
			uint32 clk_lo   = 0u;
			uint32 data_vd  = 0u;
			uint32 set_hold = 0u;
			uint32 clk_all  = (uint32)(HalS32K14x_ScgGetFreq(SPLLDIV2_CLK) / (hw_attr->baud)); //gen i2c clock freq

			if (clk_all < 0x5f) //95
			{
				clk_lo  = 2 * clk_all / 3;        //2/3 clk cycle
				clk_hi  = clk_all - clk_lo;       //1/3 clk cycle
			}
			else if (clk_all < 0xbe) //190
			{
				pre_div = 1;
				clk_lo  = 2 * clk_all / 3 / 2;    //2/3 clk cycle
				clk_hi  = clk_all / 2 - clk_lo;   //1/3 clk cycle
			}
			else if (clk_all < 0x17c) //380
			{
				pre_div = 2;
				clk_lo  = 2 * clk_all / 3 / 4;    //2/3 clk cycle
				clk_hi  = clk_all / 4 - clk_lo;   //1/3 clk cycle
			}
			else
			{
				pre_div = 3;
				clk_lo  = 2 * clk_all / 3 / 8;    //2/3 clk cycle
				clk_hi  = clk_all / 8 - clk_lo;   //1/3 clk cycle
			}

			if (clk_hi > 2)
			{
				clk_hi -= 3;
			}

			if (clk_lo > 0u)
			{
				clk_lo -= 1;
			}

			if (clk_lo > 0u)
			{
				if (1 == clk_lo)
				{
					data_vd  = 1;
					set_hold = 1;
				}
				else
				{
					data_vd  = clk_lo / 4;
					set_hold = clk_lo / 3;
				}
			}

			hw_attr->obj->MCFGR1 &= ~(LPI2C_MCFGR1_PRESCALE_MASK);                                     //clear prescale
			hw_attr->obj->MCFGR1 |= pre_div | LPI2C_MCFGR1_TIMECFG_MASK;                               // auto stop, SCL/SDA pin low timeout
			hw_attr->obj->MCCR0  |= (data_vd  << LPI2C_MCCR0_DATAVD_SHIFT)
								  | (set_hold << LPI2C_MCCR0_SETHOLD_SHIFT)
								  | (clk_hi   << LPI2C_MCCR0_CLKHI_SHIFT)
								  | clk_lo;
			hw_attr->obj->MCFGR2 |= (uint16_t)(3 * clk_all + 4);                                       //SDA & SCL both high time > BUSIDLE cycles, assumed to be idle.
			hw_attr->obj->MCFGR3 |= LPI2C_MCFGR3_PINLOWTIME_VAL << LPI2C_MCFGR3_PINLOW_SHIFT;          //SDA/SCL is low for(PINLOW * 256)cycles, PLTF is set
			//hw_attr->obj->MFCR    = 0x00000003;

			hw_attr->obj->MCR    |= LPI2C_MCR_MEN_MASK;                                                //master logic enable
		}
	}
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void IIC_IntMasterHandle(uint8_t hw_ch)
{
	LPI2C_Type  volatile * iic_hw    = (LPI2C_Type  volatile * )((i2c_hw_attr + hw_ch)->obj);
	DEV_IIC_MSG  * iic_msg   = dev_iic_msg + hw_ch;
	
	uint16_t msr 	= (uint16_t)(iic_hw->MSR);
	iic_hw->MSR  	= LPI2C_MSR_CLEAR_ALL_FLAGS;  //clear all interrupt flags
	
	if ( iic_msg->state == IIC_MSATER_WRITE )
	{
		if ( msr & (LPI2C_MSR_SDF_MASK|LPI2C_MSR_FEF_MASK|LPI2C_MSR_ALF_MASK|LPI2C_MSR_NDF_MASK|LPI2C_MSR_PLTF_MASK) )     //detect stop condition, or error
		{
			if ( msr & LPI2C_MSR_SDF_MASK )
			{	/* STOP Detect Flag */
				HalInt_Dis((i2c_hw_attr + hw_ch)->int_num);
				iic_hw->MIER 	= 	0x00;            		//disable all interrupt
				iic_msg->state 	= 	IIC_MASTER_IDLE;
				HalOS_SemaphorePost(iic_hw_int[hw_ch]);
				
				if ( msr & (LPI2C_MSR_FEF_MASK|LPI2C_MSR_ALF_MASK|LPI2C_MSR_NDF_MASK) )
				{	/* FIFO Error Flag, Arbitration Lost Flag, NACK Detect Flag */
					uint8_t tx_remainder = (uint8_t)(iic_hw->MFSR);
					TRACE_VALUE2(LOG_ERR, MOD_HW,"IIC bus = %d Master Write Error(FEF,NDF,ALF) At Stop, Proc MSR=0x%04x",hw_ch,msr);
					TRACE_VALUE(LOG_ERR, MOD_HW,"IICMaster Write Error((FEF,NDF,ALF) At Stop, Cur MSR=0x%08x",iic_hw->MSR);
					if ( tx_remainder &&  iic_msg->dat_cnt >= tx_remainder)
					{				
						iic_msg->dat_cnt -=  tx_remainder;
						TRACE_VALUE2(LOG_ERR, MOD_HW, "IIC Master write With Stop reminder(ERROR)=%d, dat_cnt=%d", tx_remainder, iic_msg->dat_cnt);
					}
					iic_hw->MCR |= LPI2C_MCR_RTF_MASK;      //reset Transmit FIFO.
				}
				else if ( msr & LPI2C_MSR_PLTF_MASK )
				{	/* Pin Low Timeout Flag */
					iic_hw->MCR 		|= 	LPI2C_MCR_RTF_MASK;     //reset Transmit FIFO.
					//iic_msg->unlock_req = 	TRUE;
					TRACE_VALUE2(LOG_ERR, MOD_HW, "IIC bus =%d Master Write Stop With Error(PLTF), Proc MSR=0x%04x", hw_ch, msr);
				}
				else{}
			}
			else if ( msr & LPI2C_MSR_PLTF_MASK )
			{	/* Pin Low Timeout Flag */
				HalInt_Dis((i2c_hw_attr + hw_ch)->int_num);
				iic_hw->MIER 	= 	0x00;            		//disable all interrupt
				iic_hw->MCR 	|= 	LPI2C_MCR_RTF_MASK;     //reset Transmit FIFO.
				iic_msg->state 	= 	IIC_MASTER_IDLE;
				HalOS_SemaphorePost(iic_hw_int[hw_ch]);
				iic_msg->unlock_req = TRUE;
				TRACE_VALUE2(LOG_ERR, MOD_HW, "iic master write Error(PLTF), Proc MSR=0x%04x, Cur MSR=0x%04x", msr,(uint16_t)(iic_hw->MSR));
			}
			else if ( msr & (LPI2C_MSR_FEF_MASK|LPI2C_MSR_ALF_MASK|LPI2C_MSR_NDF_MASK) )
			{	/* FIFO Error Flag, Arbitration Lost Flag, NACK Detect Flag */
				uint8_t tx_remainder = (uint8_t)(iic_hw->MFSR);
				if ( tx_remainder &&  iic_msg->dat_cnt >= tx_remainder)
				{				
					iic_msg->dat_cnt -=  tx_remainder;
					TRACE_VALUE2(LOG_ERR, MOD_HW, "iic master write reminder(ERROR)=%d, Proc MSR=0x%04x", tx_remainder, msr);
				}
				TRACE_VALUE2(LOG_ERR, MOD_HW, "iic master write Error(FEF,NDF,ALF), Proc MSR=0x%04x, Cur MSR=0x%04x", msr,(uint16_t)(iic_hw->MSR));
				
				iic_hw->MCR |= LPI2C_MCR_RTF_MASK;      //reset Transmit FIFO.
				iic_hw->MIER = (LPI2C_MIER_SDIE_MASK+LPI2C_MIER_PLTIE_MASK);   	//only enable stop and PIN low timeout interrupt
				IIC_SetMTDR(iic_hw, MTDRCMD_GEN_STOP_CONDI, 0u);        		//end transfer Error
			}
			else
			{
				TRACE_VALUE2(LOG_ERR, MOD_HW, "iic bus=%d master Enter error State,Proc MSR=0x%04x", hw_ch, msr);
				TRACE_VALUE(LOG_ERR, MOD_HW, "iic master Enter error State,Cur MSR=0x%08x", iic_hw->MSR);
			}
		}
		else if ( msr &  0x01 ) // TDF
		{
			if (iic_msg->dat_cnt < iic_msg->dat_len)
			{
				iic_hw->MIER &= ~LPI2C_MIER_TDIE_MASK; //disable transimit interrput to ensure current operation done.
				do
				{	// write to data 
					IIC_SetMTDR(iic_hw, MTDRCMD_TRANS_DATA, iic_msg->dat[iic_msg->dat_cnt++]); 		//TRANSFER data
				}while( (uint8_t)(iic_hw->MFSR) < 4 && iic_msg->dat_cnt < iic_msg->dat_len );
				iic_hw->MIER |= LPI2C_MIER_TDIE_MASK;  //enable transimit interrput
			}
			else
			{
				IIC_SetMTDR(iic_hw, MTDRCMD_GEN_STOP_CONDI, 0u);         //end transfer
				/* enable stop generate */
				iic_hw->MIER = 	LPI2C_MIER_SDIE_MASK        //STOP
								| LPI2C_MIER_NDIE_MASK        //NACK detect
								| LPI2C_MIER_PLTIE_MASK       //PIN low timeout
								| LPI2C_MIER_FEIE_MASK        //FIFO error
								| LPI2C_MIER_ALIE_MASK;       //Abstraction lost
			}
		}
		else
		{// state error , disable all interrupt
			//HalInt_Dis((i2c_hw_attr + hw_ch)->int_num);
			//iic_hw->MIER = 0x00;            		//disable all interrupt 
			TRACE_VALUE2(LOG_ERR, MOD_HW, "iic bus =%d, Master write State Error, Proc MSR=0x%04x",hw_ch, msr);
			TRACE_VALUE(LOG_ERR, MOD_HW,"Current MSR=0x%08x", iic_hw->MSR);
		}
	}
	else if ( iic_msg->state == IIC_MSATER_READ )
	{
		if ( msr & (LPI2C_MSR_SDF_MASK|LPI2C_MSR_FEF_MASK|LPI2C_MSR_ALF_MASK|LPI2C_MSR_NDF_MASK/*|LPI2C_MSR_PLTF_MASK*/) )     //detect stop condition, or error
		{	// error or stop
			while( (iic_hw->MSR&0x02) && iic_msg->dat_cnt < iic_msg->dat_len )	// check rx fifo 
			{	/* rx data if data is avaible */
				iic_msg->dat[iic_msg->dat_cnt++] = (uint8_t)(iic_hw->MRDR); //restore recv bytes
			}
			//if ( msr & LPI2C_MSR_PLTF_MASK )
			//{	/* Pin Low Timeout Flag */
			//	HalInt_Dis((i2c_hw_attr + hw_ch)->int_num);
			//	iic_hw->MIER 	= 	0x00;            		//disable all interrupt
			//	iic_hw->MCR 	|= 	LPI2C_MCR_RRF_MASK;     //reset Receive FIFO.
			//	iic_msg->state 	= 	IIC_MASTER_IDLE;
			//	HalOS_SemaphorePost(iic_hw_int[hw_ch]);
			//	TRACE_VALUE2(LOG_ERR, MOD_HW, "iic master Read Error(PLTF), Proc MSR=0x%04x, Cur MSR=0x%04x", msr,(uint16_t)(iic_hw->MSR));
			//}
			/*else*/ if ( msr & (LPI2C_MSR_FEF_MASK|LPI2C_MSR_ALF_MASK|LPI2C_MSR_NDF_MASK) )
			{	/* FIFO Error Flag, Arbitration Lost Flag, NACK Detect Flag */
				iic_hw->MCR |= LPI2C_MCR_RRF_MASK;      //reset Receive FIFO.
				iic_hw->MIER = (LPI2C_MIER_SDIE_MASK+LPI2C_MIER_PLTIE_MASK);   	//only enable stop and PIN low timeout interrupt
				IIC_SetMTDR(iic_hw, MTDRCMD_GEN_STOP_CONDI, 0u);        		// set stop
				
				TRACE_VALUE2(LOG_ERR, MOD_HW, "iic master Read Error(FEF,NDF,ALF), Proc MSR=0x%04x, Cur MSR=0x%04x", msr,(uint16_t)(iic_hw->MSR));
			}
			else if ( msr & LPI2C_MSR_SDF_MASK )
			{	/* STOP Detect Flag */
				HalInt_Dis((i2c_hw_attr + hw_ch)->int_num);
				iic_hw->MIER 	= 	0x00;            		//disable all interrupt
				iic_msg->state 	= 	IIC_MASTER_IDLE;
				HalOS_SemaphorePost(iic_hw_int[hw_ch]);
			}	
			else
			{
				TRACE_VALUE2(LOG_ERR, MOD_HW, "iic bus Master Read Enter error State..., Proc MSR=0x%04x, Cur MSR=0x%04x", msr,(uint16_t)(iic_hw->MSR));
			}
		}
		else if ( msr &  0x02 ) // RDF
		{
			iic_hw->MIER &= ~LPI2C_MIER_RDIE_MASK; //disable receive interrput to ensure current operation done.
			while( (iic_hw->MSR&0x02) && (iic_msg->dat_cnt < iic_msg->dat_len) )	// check rx fifo
			{
				iic_msg->dat[iic_msg->dat_cnt++] = (uint8_t)(iic_hw->MRDR); //restore recv bytes
			}
			while ( (iic_hw->MSR&0x02) && iic_msg->dat_cnt == iic_msg->dat_len )
			{
				TRACE_VALUE2(LOG_ERR,MOD_HW,"IIC Master Read too Many data,Data=0x%04x,CUR MSR=0x%04x",(uint16_t)(iic_hw->MRDR),(uint16_t)(iic_hw->MSR));
			}
			iic_hw->MIER |= LPI2C_MIER_RDIE_MASK;  //enable receive interrput

			if ( msr & LPI2C_MSR_PLTF_MASK )
			{
				TRACE_VALUE2(LOG_ERR, MOD_HW, "iic master Read Ok, But (PLTF) occured, Proc MSR=0x%04x, Cur MSR=0x%04x", msr,(uint16_t)(iic_hw->MSR));
			}
			if ( iic_msg->dat_cnt == iic_msg->dat_len )
			{
				IIC_SetMTDR(iic_hw, MTDRCMD_GEN_STOP_CONDI, 0u);         //end transfer
				/* enable stop generate */
				iic_hw->MIER = 	LPI2C_MIER_SDIE_MASK        //STOP
								| LPI2C_MIER_NDIE_MASK        //NACK detect
								| LPI2C_MIER_PLTIE_MASK       //PIN low timeout
								| LPI2C_MIER_FEIE_MASK        //FIFO error
								| LPI2C_MIER_ALIE_MASK;       //Abstraction lost			
			}
		}
		else if ( msr & LPI2C_MSR_PLTF_MASK )
		{	/* Pin Low Timeout Flag */
			HalInt_Dis((i2c_hw_attr + hw_ch)->int_num);
			iic_hw->MIER 		= 	0x00;            		//disable all interrupt
			iic_hw->MCR 		|= 	LPI2C_MCR_RRF_MASK;     //reset Receive FIFO.
			iic_msg->state 		= 	IIC_MASTER_IDLE;
			iic_msg->unlock_req = 	TRUE;
			
			HalOS_SemaphorePost(iic_hw_int[hw_ch]);
			TRACE_VALUE2(LOG_ERR, MOD_HW, "iic master Read Error(PLTF), Proc MSR=0x%04x, Cur MSR=0x%04x", msr,(uint16_t)(iic_hw->MSR));
		}
		else 
		{// state error , disable all interrupt 
			TRACE_VALUE2(LOG_ERR, MOD_HW, "IIC bus =%d Master Read state Error1, MIER=0x%04x..", hw_ch,(uint16_t)(iic_hw->MIER));
			TRACE_VALUE(LOG_ERR, MOD_HW, "iic bus Master Read Enter error State..., Proc MSR=0x%04x", msr);
			TRACE_VALUE(LOG_ERR, MOD_HW, "iic bus Master Read Enter error State..., Cur MSR=0x%08x", iic_hw->MSR);
			//iic_hw->MIER = 0x00;            		//disable all interrupt 
		}		
	}
	else
	{	// state error, disable all interrupt 
		TRACE_VALUE2(LOG_ERR, MOD_HW, "IIC Error2,IIC bus=%d, MIER=%x", hw_ch,(uint16_t)(iic_hw->MIER));
		TRACE_VALUE2(LOG_ERR, MOD_HW, "IIC Error2, Proc MSR=0x%04x, Cur MSR=0x%04x", msr,(uint16_t)(iic_hw->MSR));
	
		HalInt_Dis((i2c_hw_attr + hw_ch)->int_num);
		iic_hw->MIER = 0x00;
	}
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void	IIC_IntSlaveHandle(uint8_t hw_ch)
{}

