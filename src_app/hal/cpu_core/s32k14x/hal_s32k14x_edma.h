/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    s32k EDMA driver api
    All rights reserved
>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#ifndef __HAL_S32K14X_EDMA_H__
#define __HAL_S32K14X_EDMA_H__

#ifdef  __cplusplus
extern "C"
{
#endif

#include "std_type.h"

//DMA source selection
typedef enum {
    EDMA_REQ_LPUART0_RX             = 2U,
    EDMA_REQ_LPUART0_TX             = 3U,
    EDMA_REQ_LPUART1_RX             = 4U,
    EDMA_REQ_LPUART1_TX             = 5U,
    EDMA_REQ_LPUART2_RX             = 6U,
    EDMA_REQ_LPUART2_TX             = 7U,
    EDMA_REQ_LPI2C1_RX              = 8U,    //only for 148
    EDMA_REQ_LPI2C1_TX              = 9U,    //only for 148
    EDMA_REQ_FLEXIO_SHIFTER0        = 10U,
    EDMA_REQ_FLEXIO_SHIFTER1        = 11U,
    EDMA_REQ_FLEXIO_SHIFTER2        = 12U,
    EDMA_REQ_FLEXIO_SHIFTER3        = 13U,
    EDMA_REQ_LPSPI0_RX              = 14U,
    EDMA_REQ_LPSPI0_TX              = 15U,
    EDMA_REQ_LPSPI1_RX              = 16U,
    EDMA_REQ_LPSPI1_TX              = 17U,
    EDMA_REQ_LPSPI2_RX              = 18U,
    EDMA_REQ_LPSPI2_TX              = 19U,
    EDMA_REQ_FTM1_CHANNEL_0         = 20U,
    EDMA_REQ_FTM1_CHANNEL_1         = 21U,
    EDMA_REQ_FTM1_CHANNEL_2         = 22U,
    EDMA_REQ_FTM1_CHANNEL_3         = 23U,
    EDMA_REQ_FTM1_CHANNEL_4         = 24U,
    EDMA_REQ_FTM1_CHANNEL_5         = 25U,
    EDMA_REQ_FTM1_CHANNEL_6         = 26U,
    EDMA_REQ_FTM1_CHANNEL_7         = 27U,
    EDMA_REQ_FTM2_CHANNEL_0         = 28U,
    EDMA_REQ_FTM2_CHANNEL_1         = 29U,
    EDMA_REQ_FTM2_CHANNEL_2         = 30U,
    EDMA_REQ_FTM2_CHANNEL_3         = 31U,
    EDMA_REQ_FTM2_CHANNEL_4         = 32U,
    EDMA_REQ_FTM2_CHANNEL_5         = 33U,
    EDMA_REQ_FTM2_CHANNEL_6         = 34U,
    EDMA_REQ_FTM2_CHANNEL_7         = 35U,
    EDMA_REQ_FTM0_OR_CH0_CH7        = 36U,
    EDMA_REQ_FTM3_OR_CH0_CH7        = 37U,
    EDMA_REQ_FTM4_OR_CH0_CH7        = 38U,    //only for 148
    EDMA_REQ_FTM5_OR_CH0_CH7        = 39U,    //only for 148
    EDMA_REQ_FTM6_OR_CH0_CH7        = 40U,    //only for 148
    EDMA_REQ_FTM7_OR_CH0_CH7        = 41U,    //only for 148
    EDMA_REQ_ADC0                   = 42U,
    EDMA_REQ_ADC1                   = 43U,
    EDMA_REQ_LPI2C0_RX              = 44U,
    EDMA_REQ_LPI2C0_TX              = 45U,
    EDMA_REQ_PDB0                   = 46U,
    EDMA_REQ_PDB1                   = 47U,
    EDMA_REQ_CMP0                   = 48U,
    EDMA_REQ_PORTA                  = 49U,
    EDMA_REQ_PORTB                  = 50U,
    EDMA_REQ_PORTC                  = 51U,
    EDMA_REQ_PORTD                  = 52U,
    EDMA_REQ_PORTE                  = 53U,
    EDMA_REQ_FLEXCAN0               = 54U,
    EDMA_REQ_FLEXCAN1               = 55U,
    EDMA_REQ_FLEXCAN2               = 56U,
    EDMA_REQ_SAI0_RX                = 57U,    //only for 148
    EDMA_REQ_SAI0_TX                = 58U,    //only for 148
    EDMA_REQ_LPTMR0                 = 59U,
    EDMA_REQ_QSPI_RX                = 60U,    //only for 148
    EDMA_REQ_QSPI_TX                = 61U,    //only for 148
    EDMA_REQ_DMAMUX_ALWAYS_ENABLED0 = 62U,
    EDMA_REQ_DMAMUX_ALWAYS_ENABLED1 = 63U,

	EDMA_REQ_MAX
} DMA_REQ_SOURCE;

typedef enum
{
	EDMA_CH_0  = 0u,
	EDMA_CH_1  = 1u,
	EDMA_CH_2  = 2u,
	EDMA_CH_3  = 3u,
	EDMA_CH_4  = 4u,
	EDMA_CH_5  = 5u,
	EDMA_CH_6  = 6u,
	EDMA_CH_7  = 7u,
	EDMA_CH_8  = 8u,
	EDMA_CH_9  = 9u,
	EDMA_CH_10 = 10u,
	EDMA_CH_11 = 11u,
	EDMA_CH_12 = 12u,
	EDMA_CH_13 = 13u,
	EDMA_CH_14 = 14u,
	EDMA_CH_15 = 15u,

    EDMA_CH_MAX
}EDMA_CH_DEF;

typedef void (*EDMA_USER_ISR)(void);    //user callback type

void    HalS32k14x_eDMAInit                (void);
void    HalS32K14x_eDMAChInit              (uint8 channel);
void    HalS32K14x_eDMAConfigSourceChannel (uint8 channel, DMA_REQ_SOURCE source);
void    HalS32K14x_eDMAEnableErrIsr        (void);
void    HalS32K14x_eDMADisableErrIsr       (void);
void    HalS32K14x_eDMAEnableChErrInt      (uint8 channel);
void    HalS32K14x_eDMASetChannelIsr       (uint8 channel, EDMA_USER_ISR user_isr);
void    HalS32K14x_eDMADisChannelIsr       (uint8 channel);
void    HalS32K14x_eDMASetTCDSrcDestAddr   (uint8 channel, uint32 src_addr, uint32 dest_addr);
void    HalS32K14x_eDMASetTCDFrameAttr     (uint8 channel, const uint16 dat_len, uint8 dat_size);
void    HalS32K14x_eDMASetTCDSOFF          (uint8 channel, uint8 dat_size);
void    HalS32K14x_eDMASetTCDDOFF          (uint8 channel, uint8 dat_size);
void    HalS32K14x_eDMASetLastSignedAddr   (uint8 channel, uint32 src_addr, uint32 dest_addr);
void    HalS32k14x_eDMAReqTrigTypeSet      (uint8 channel, boolean inter_trig);
void    HalS32k14x_eDMAERQSet              (uint8 channel, boolean onoff);
uint32  HalS32k14x_eDMAGetTransferedLen    (uint8 channel);
boolean HalS32k14x_eDMAIsERQValid          (uint8 channel);


#ifdef  __cplusplus
}
#endif

#endif

