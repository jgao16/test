/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
//Dflash moudle

#include "std_type.h"
#include "std_lib.h"

#include "hal_dflash.h"
#include "cpu.h"
#include "hal_int.h"
#include "hal_s32k14x_dflash.h"
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

//Flash command list
#define FTFx_VERIFY_BLOCK               0x00U
#define FTFx_VERIFY_SECTION             0x01U
#define FTFx_PROGRAM_CHECK              0x02U
#define FTFx_READ_RESOURCE              0x03U
#define FTFx_PROGRAM_LONGWORD           0x06U
#define FTFx_PROGRAM_PHRASE             0x07U
#define FTFx_ERASE_BLOCK                0x08U
#define FTFx_ERASE_SECTOR               0x09U
#define FTFx_PROGRAM_SECTION            0x0BU 
#define FTFx_VERIFY_ALL_BLOCK           0x40U
#define FTFx_READ_ONCE                  0x41U
#define FTFx_PROGRAM_ONCE               0x43U
#define FTFx_ERASE_ALL_BLOCK            0x44U
#define FTFx_SECURITY_BY_PASS           0x45U
#define FTFx_PFLASH_SWAP                0x46U
#define FTFx_PROGRAM_PARTITION          0x80U
#define FTFx_SET_EERAM                  0x81U
#define FTFx_ERASE_ALL_BLOCK_UNSECURE   0x49U

//FCCOB list
#define FCCOB0                          (FTFC->FCCOB[3])
#define FCCOB1                          (FTFC->FCCOB[2])
#define FCCOB2                          (FTFC->FCCOB[1])
#define FCCOB3                          (FTFC->FCCOB[0])
#define FCCOB4                          (FTFC->FCCOB[7])
#define FCCOB5                          (FTFC->FCCOB[6])
#define FCCOB6                          (FTFC->FCCOB[5])
#define FCCOB7                          (FTFC->FCCOB[4])
#define FCCOB8                          (FTFC->FCCOB[11])
#define FCCOB9                          (FTFC->FCCOB[10])
#define FCCOBA                          (FTFC->FCCOB[9])
#define FCCOBB                          (FTFC->FCCOB[8])

//flexnvm start address
#define FLEXNVM_START_ADDR              (0x10000000)
#define DFLASH_PROG_UNIT_SIZE           (8)
#define DFLASH_PROG_BASE_ADDR           (0x800000)

//byte operation
#define GET_BIT_0_7(value)              ((uint8)( ((uint32)(value))        & 0xff))
#define GET_BIT_8_15(value)             ((uint8)((((uint32)(value)) >> 8 ) & 0xff))
#define GET_BIT_16_23(value)            ((uint8)((((uint32)(value)) >> 16) & 0xff))
#define GET_BIT_24_31(value)            ((uint8)((((uint32)(value)) >> 24) & 0xff))
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

static uint32 current_dflash_read_addr;
static volatile uint32 dflash_err_flag[DFLASH_SIZE/32/64];	// 64 byte as a block

static volatile boolean	 dflash_read_process;
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void    S32K14xDFlash_Init             (void);
static uint32  S32K14xDFlash_GetFlashSize     (void);
static uint32  S32K14xDFlash_GetSectorSize    (void);
static uint32  S32K14xDFlash_GetPageSize      (void);
static uint32  S32K14xDFlash_GetSegmentSize   (void);
static void    S32K14xDFlash_EraseEntireFlash (void);
static void    S32K14xDFlash_EraseFlashSector (uint32 addr);
static uint32  S32K14xDFlash_ProgramData      (uint32 addr, const uint8 *dat, uint32 len);
static uint32  S32K14xDFlash_ReadData         (uint32 addr, uint8 *dat, uint32 len);
static boolean S32K14xDFlash_IsBusy           (void);
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static HAL_DFLASH_HW_API const dflash_hw_api = 
{
	.init               = S32K14xDFlash_Init,
	.get_flash_size     = S32K14xDFlash_GetFlashSize,
	.get_sector_size    = S32K14xDFlash_GetSectorSize,
	.get_page_size      = S32K14xDFlash_GetPageSize,
	.get_segment_size   = S32K14xDFlash_GetSegmentSize,
	.erase_entire_flash = S32K14xDFlash_EraseEntireFlash,
	.erase_flash_sector = S32K14xDFlash_EraseFlashSector,
	.program_data       = S32K14xDFlash_ProgramData,
	.read_data          = S32K14xDFlash_ReadData,
	.is_busy            = S32K14xDFlash_IsBusy,

};
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void FtfcReadCollision(void);
static void FtfcFault(void);
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
HAL_DFLASH_HW_API const *S32K14x_DFlash_GetDFlashHwApi(void)
{
	return &dflash_hw_api;
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void 	S32K14x_DflashReadErr(void)
{
	uint32 offset =  	current_dflash_read_addr%DFLASH_SIZE;
	uint32 bit_   = 	offset / 64;
	
	dflash_err_flag[bit_/32] |= (1u<<(bit_&0x1F));
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
boolean S32K14x_DflashReadProcess(void)
{
	return dflash_read_process;
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void    S32K14xDFlash_Init(void)
{
	// Config DFlash (EEPROM)
	CPU_SR_ALLOC();
	CPU_CRITICAL_ENTER();
#if 0
	if (0x10000 == S32K14xDFlash_GetFlashSize()) //DFlash has not been partitioned(default 64KB).
	{
		FTFC->FSTAT = FTFC_FSTAT_FPVIOL_MASK | FTFC_FSTAT_ACCERR_MASK | FTFC_FSTAT_RDCOLERR_MASK; //clr error flags

		FCCOB0      = FTFx_PROGRAM_PARTITION; //PGMPART command
		FCCOB3      = 0x00;                   //FlexRAM loaded with valid EEPROM data during reset sequence
		FCCOB4      = 0x33;                   //EEPROM set size:2k bytes
		FCCOB5      = 0x03;                   //DFlash set size:32k bytes,EEPROM backup set size:32k bytes

		FTFC->FSTAT = FTFC_FSTAT_CCIF_MASK;   //clr CCIF to launch cmd
		while (0x0u == (FTFC->FSTAT & FTFC_FSTAT_CCIF_MASK)){ ; } //wait until CCIF bit set
	}
#endif
	MSCM->OCMDR[1] |= 0x00000030; 	// disbale data flash speculation

	HalInt_VectSet(HAL_INT_ID_Read_Collision_IRQn, FtfcReadCollision);
	HalInt_VectSet(HAL_INT_ID_FTFC_Fault_IRQn, FtfcFault);

	//FTFC->FCNFG |= FTFC_FCNFG_RDCOLLIE_MASK;
	//FTFC->FERSTAT |= 0X02;

	//HalInt_En(HAL_INT_ID_Read_Collision_IRQn);
	//HalInt_En(HAL_INT_ID_FTFC_Fault_IRQn);
	
	CPU_CRITICAL_EXIT();	
#if 0
	for(uint8 i = 0; i < 30; i ++ )
	{
		uint8 test_data[8];
		uint32 addr;
		addr = FLEXNVM_START_ADDR;
		test_data[4] = 0xAA;
		for(;addr <  FLEXNVM_START_ADDR + DFLASH_SIZE; addr += 64 )
		{
			(void)S32K14xDFlash_ProgramData(addr,test_data,8);
			while(S32K14xDFlash_IsBusy()){}
		}
		addr = FLEXNVM_START_ADDR;
		test_data[4] = 0xA0;
		for(;addr <  FLEXNVM_START_ADDR + DFLASH_SIZE; addr += 64 )
		{
			(void)S32K14xDFlash_ProgramData(addr,test_data,8);
			while(S32K14xDFlash_IsBusy()){}
		}
		ServWdt_ForcePetWdt();
	}
#endif
	dflash_read_process = TRUE; 	// set read flag
	/* read all dflash check if error */	
	for(uint32 i = 0; i < DFLASH_SIZE/64; i++ )
	{
		current_dflash_read_addr     = 0x10000000 + (i *64);
		uint32 volatile * start_addr = (uint32 volatile *)current_dflash_read_addr;
		
		for ( uint8 j = 0; j < 16 && (!(dflash_err_flag[i/32] & (1u<<(i&0x1F)))); j ++ )
		{
			(void)start_addr[j];
		}
	}

	dflash_read_process = FALSE;
}
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static uint32  S32K14xDFlash_GetFlashSize(void)
{
	return DFLASH_SIZE;
#if 0	
	uint32 size   = 0u; //return flash size
	uint8  depart = (uint8)((SIM->FCFG1 & 0x0000f000) >> 12);

	if ((0x00 == depart) || (0x0c == depart) || (0x0f == depart)) //64k
	{
		size = 0x10000;
	}
	else if ((0x03 == depart) || (0x0b == depart)) //32k
	{
		size = 0x8000;
	}
	else if (0x0a == depart) //16k
	{
		size = 0x4000;
	}
	else
	{
	}

	return size;
#endif
}
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static uint32  S32K14xDFlash_GetSectorSize(void)
{
    uint32 size = 0u;
    if ((0x4000000 == (SIM->SDID & SIM_SDID_SUBSERIES_MASK)) && (0x800000 == (SIM->SDID & SIM_SDID_DERIVATE_MASK))) //s32k148
    {
        size = 0x1000; //4k
    }
    else
    {
        size = 0x800; //2k
    }
	return size;
}
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static uint32  S32K14xDFlash_GetPageSize(void)
{
	return 0x800; //2k
}
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static uint32  S32K14xDFlash_GetSegmentSize(void)
{
	return 0x800; //2k
}
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void    S32K14xDFlash_EraseEntireFlash(void)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();

    if (!S32K14xDFlash_IsBusy())
    {
        FTFC->FSTAT  = FTFC_FSTAT_FPVIOL_MASK | FTFC_FSTAT_ACCERR_MASK | FTFC_FSTAT_RDCOLERR_MASK; //clr error flags

        FCCOB0       = FTFx_ERASE_BLOCK;
        FCCOB1       = 0x80;
        FCCOB2       = 0x00;
        FCCOB3       = 0x00;

        FTFC->FSTAT  = FTFC_FSTAT_CCIF_MASK;        //clr CCIF to launch cmd

        for (uint16_t i = 0u; i < sizeof(dflash_err_flag)/4; i++)
        {
            dflash_err_flag[i] = 0u; //clear all error flags
        }
    }

    CPU_CRITICAL_EXIT();
}
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void    S32K14xDFlash_EraseFlashSector(uint32 addr)
{
	CPU_SR_ALLOC();
	CPU_CRITICAL_ENTER();

	if ((!S32K14xDFlash_IsBusy()) && (addr >= FLEXNVM_START_ADDR) && (addr < FLEXNVM_START_ADDR + S32K14xDFlash_GetFlashSize()))
	{
		uint32 sector_size  = S32K14xDFlash_GetSectorSize();                      //sector size
		uint8  sector_index = (uint8)((addr - FLEXNVM_START_ADDR) / sector_size); //in which sector
		uint32 erase_addr   = sector_index * sector_size + DFLASH_PROG_BASE_ADDR; //start address writen to FCCOB

		FTFC->FSTAT  = FTFC_FSTAT_FPVIOL_MASK | FTFC_FSTAT_ACCERR_MASK | FTFC_FSTAT_RDCOLERR_MASK; //clr error flags

		FCCOB0       = FTFx_ERASE_SECTOR;
		FCCOB1       = GET_BIT_16_23(erase_addr);
		FCCOB2       = GET_BIT_8_15 (erase_addr);
		FCCOB3       = GET_BIT_0_7  (erase_addr);

		FTFC->FSTAT  = FTFC_FSTAT_CCIF_MASK;        //clr CCIF to launch cmd

		/* clear dflash_err_flag bit */
		uint32 sector_start_addr = (addr/sector_size)*sector_size - FLEXNVM_START_ADDR;
		//uint32 sector_end_addr   = sector_start_addr + sector_size;
		uint32 start_bit_ 		 = sector_start_addr/64;
		uint32 end_bit_  		 = start_bit_ + sector_size/64;
		for ( uint32 i = start_bit_; i < end_bit_; i ++ )
		{
			dflash_err_flag[i/32] &= ~(1u<<(i&0x1F));
		}
		//uint8 sector_num 			= (addr - FLEXNVM_START_ADDR)/S32K14xDFlash_GetSectorSize();
		//uint8 sector_num 			= (uint8)((addr&0xFFFF)/2048);
		//dflash_err_flag[sector_num] = 0x00;
	}

	CPU_CRITICAL_EXIT();
}
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static uint32  S32K14xDFlash_ProgramData(uint32 addr, const uint8 *dat, uint32 len)
{
	uint32 p_len = 0u; //length for programmed

	(void)dat;
	
	CPU_SR_ALLOC();
	CPU_CRITICAL_ENTER();

	if ((!S32K14xDFlash_IsBusy())
	 && (NULL != dat) && (len >= DFLASH_PROG_UNIT_SIZE)                       //at least 8 bytes
	 && (!(addr % DFLASH_PROG_UNIT_SIZE)) && (addr >= FLEXNVM_START_ADDR))    //64-bit aligned
	{
		/* check dflash address content is ok*/
		uint32 start_bit_ = (addr%DFLASH_SIZE)/64;
		uint32 end_bit_   = ((addr+len+63)%DFLASH_SIZE)/64;

		boolean flash_err = FALSE;
		
		for(uint32 i = start_bit_; i < end_bit_ && !flash_err; i ++ )
		{
			if ( dflash_err_flag[i/32] & (1u<<(i&0x1F)) )
			{	// dflash error , 
				flash_err = TRUE;
			}
		}
		
		if ( !flash_err )
		{
			uint32 dflash_prog_addr  = addr - FLEXNVM_START_ADDR + DFLASH_PROG_BASE_ADDR;

			FTFC->FSTAT = FTFC_FSTAT_FPVIOL_MASK | FTFC_FSTAT_ACCERR_MASK | FTFC_FSTAT_RDCOLERR_MASK; //clr error flags

			FCCOB0      = FTFx_PROGRAM_PHRASE; //program 8 bytes per operation
			FCCOB1      = GET_BIT_16_23(dflash_prog_addr);
			FCCOB2      = GET_BIT_8_15 (dflash_prog_addr);
			FCCOB3      = GET_BIT_0_7  (dflash_prog_addr);

			for (uint8 i = 0u; i < DFLASH_PROG_UNIT_SIZE; i++)
			{
				FTFC->FCCOB[i + 4] = dat[i];
			}

			FTFC->FSTAT  = FTFC_FSTAT_CCIF_MASK;         //clr CCIF to launch cmd

			p_len = DFLASH_PROG_UNIT_SIZE;			
		}
	}

	CPU_CRITICAL_EXIT();

	return p_len;
}
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static uint32  S32K14xDFlash_ReadData(uint32 addr, uint8 *dat, uint32 len)
{
	uint32 r_len = 0u; //length for read

	
	/* check dflash address content is ok*/
	uint32 start_bit_ = (addr%DFLASH_SIZE)/64;
	uint32 end_bit_   = ((addr+len+63)%DFLASH_SIZE)/64;

	boolean flash_err = FALSE;
	
	for(uint32 i = start_bit_; i < end_bit_ && !flash_err; i ++ )
	{
		if ( dflash_err_flag[i/32] & (1u<<(i&0x1F)) )
		{	// dflash error , 
			flash_err = TRUE;
		}		
	}

	if ( flash_err )
	{
		for ( uint32 i = 0; i < len; i ++ )
		{
			dat[i] = 0x00;
		}
		r_len = len;
	}
	else
	{
		dflash_read_process = TRUE;	// set read flag
		
		CPU_SR_ALLOC();
		CPU_CRITICAL_ENTER();

		if ((!S32K14xDFlash_IsBusy()) && (NULL != dat) && (len > 0u))
		{
			uint32 max_size = S32K14xDFlash_GetFlashSize();
			uint32 rd_addr  = 0u;

			if (addr < FLEXNVM_START_ADDR)
			{
				if (((addr + len) >= FLEXNVM_START_ADDR) && ((addr + len) < (FLEXNVM_START_ADDR + max_size)))
				{
					r_len   = addr + len - FLEXNVM_START_ADDR;     //actual valid numbers
					rd_addr = FLEXNVM_START_ADDR;                  //start read address
				}
				else if ((addr + len) >= (FLEXNVM_START_ADDR + max_size))
				{
					r_len   = max_size;
					rd_addr = FLEXNVM_START_ADDR;
				}
				else
				{
				}
			}
			else if (addr <= (FLEXNVM_START_ADDR + max_size))
			{
				if ((addr + len) <= (FLEXNVM_START_ADDR + max_size))
				{
					r_len   = len;
					rd_addr = addr;
				}
				else
				{
					r_len   = FLEXNVM_START_ADDR + max_size - addr;
					rd_addr = addr;
				}
			}
			else
			{
			}

			if ((0u != rd_addr) && (0u != r_len))
			{
				uint8_t volatile* temp = (uint8 volatile*)rd_addr;
				for (uint32 i = 0; i < r_len; i++)
				{
					current_dflash_read_addr = (uint32)temp + i;
					dat[i] = temp[i];
				}
			}	
		}

		CPU_CRITICAL_EXIT();	

		dflash_read_process = FALSE;	// clear read flag		
	}
	
	return r_len;
}
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static boolean S32K14xDFlash_IsBusy(void)
{
	//boolean is_busy = FALSE; //indicator DFlash current busy or not,TRUE is busy now.

	//if ( 0x00 == (FTFC->FSTAT & FTFC_FSTAT_CCIF_MASK) || (0 == (FTFC->FCNFG & FTFC_FCNFG_EEERDY_MASK)) )
	//{
		//CCIF=0 or dflash_flash_cmd < FLASH_CMD_MAX,busy now
	//	is_busy = TRUE;
	//}

	//return is_busy;
	return (FTFC->FSTAT & FTFC_FSTAT_CCIF_MASK)==0x00 ;
}
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void FtfcReadCollision(void)
{
	// clear 
	FTFC->FSTAT = FTFC_FSTAT_FPVIOL_MASK | FTFC_FSTAT_ACCERR_MASK | FTFC_FSTAT_RDCOLERR_MASK; //clr error flags
}
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void FtfcFault(void)
{
	FTFC->FERSTAT = 0x02;
}



/** >>>>>>>>>>>>>>>>>>>>>>>>>>>> end of file >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


