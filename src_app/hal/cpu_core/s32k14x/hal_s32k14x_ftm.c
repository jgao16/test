/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
//ftm module

#include "std_lib.h"

#include "hal_gpio.h"
#include "hal_ftm.h"
#include "cpu.h"

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#define PWM_HW_ATTR_SIZE                                        (sizeof(pwm_hw_attr) / sizeof(FTM_PWM_HW_CFG))

#define FTM_MOD_VAL                                             (16000 / 160)  //cycle counter
#define FTM_CNSC_EDGE_ALIGNED_PWM                               (0x00000028)

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
typedef struct
{
    FTM_Type    * obj;                 //FTM moudle
    unsigned char pwm_ch;              //pwm channel number
    HAL_PORT_NAME port_name;           //FTM port name
    unsigned char pin_num;             //FTM pin number
    unsigned char af_num;              //FTM pin af number
}FTM_PWM_HW_CFG;

static FTM_PWM_HW_CFG const pwm_hw_attr[] = 
{
#undef  CPU_FTM_HW_DEF
#define CPU_FTM_HW_DEF(hw_name, hw_obj, pwm_ch, port_name, pin_num, af_num) \
	{hw_obj, pwm_ch, port_name, pin_num, af_num},
    #include "cpu_ftm.cfg"
#undef  CPU_FTM_HW_DEF
};

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void HalS32K14x_FtmPwmHwInit     (uint8 hw_ch);
static void HalS32K14x_FtmPwmHwDeInit   (uint8 hw_ch);
static void HalS32K14x_PwmChSetVal      (uint8 hw_ch, const uint8 val);
static void HalS32K14x_PwmChGetVal      (uint8 hw_ch, uint8 *val);

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
HAL_FTM_HW_API const ftm_hw_api =
{
    .ch_init        = HalS32K14x_FtmPwmHwInit,
    .ch_deinit      = HalS32K14x_FtmPwmHwDeInit,
    .ch_set_pwm_val = HalS32K14x_PwmChSetVal,
    .ch_get_pwm_val = HalS32K14x_PwmChGetVal,
};

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
HAL_FTM_HW_API const *S32K14x_Ftm_GetApi(void)
{
	return &ftm_hw_api;
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static boolean ftm_ch_avbl[PWM_HW_ATTR_SIZE] = {FALSE};

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void HalS32K14x_FtmPwmHwInit(uint8 hw_ch)
{
    if ((hw_ch < PWM_HW_ATTR_SIZE) && !ftm_ch_avbl[hw_ch])
    {
        FTM_PWM_HW_CFG const *cfg = pwm_hw_attr + hw_ch;
        if ((NULL != cfg) && (NULL != cfg->obj))
        {
            uint8           pcc_index   = 0u;
            __IO uint32_t * pcc         = NULL;

            if (FTM0 == cfg->obj)
            {
                pcc_index = PCC_FTM0_INDEX;
            }
            else if (FTM1 == cfg->obj)
            {
                pcc_index = PCC_FTM1_INDEX;
            }
            else if (FTM2 == cfg->obj)
            {
                pcc_index = PCC_FTM2_INDEX;
            }
            // else if (FTM3 == cfg->obj) //FTM3 for cpu_ts
            // {
            //     pcc_index = PCC_FLEXTMR3_INDEX;
            // }
            else
            {
            }

            if (pcc_index > 0u)
            {
                pcc = PCC->PCCn + pcc_index;
            }

            if ((NULL != pcc) && ((*pcc) & PCC_PCCn_PR_MASK))   //PCC CLOCK OFF
            {
                *pcc &= ~PCC_PCCn_CGC_MASK;
                *pcc &= ~PCC_PCCn_PCS_MASK;                    	//clear PCS
                *pcc |= PCC_PCCn_PCS(1);                        //select SOSC_DIV1
                *pcc |= PCC_PCCn_CGC_MASK;                      //enable spi clock

                HalGpio_InitPinGpio_Def(cfg->port_name, cfg->pin_num, PORT_AF, OUTPUT_PUSH_PULL, INPUT_NO_PULL);
                HalGpio_PinAFConfig(cfg->port_name, cfg->pin_num, cfg->af_num);

                cfg->obj->MODE     |= FTM_MODE_WPDIS_MASK;                          /* Write protect to registers disabled (default) */
                cfg->obj->SC       |= 1u << (16 + cfg->pwm_ch);                     /* choose ps, enable channel */
                cfg->obj->COMBINE   = 0u;
                cfg->obj->POL       = 0u;
                cfg->obj->MOD       = FTM_MOD_VAL;                                  /* FTM1 counter final value (used for PWM mode) */
                                                                                    /* FTM1 Period = MOD-CNTIN+0x0001 ~= 100 ctr clks  */
                                                                                    /* 16MHz -> ticks -> about 19KHz */
                cfg->obj->SC       |= FTM_SC_CLKS(3);                               /* Start FTMn counter with SOSC_DIV1 CLOCK */

                ftm_ch_avbl[hw_ch]  = TRUE;
            }
        }
    }
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void HalS32K14x_FtmPwmHwDeInit(uint8 hw_ch)
{
    if ((hw_ch < PWM_HW_ATTR_SIZE) && ftm_ch_avbl[hw_ch])
    {
        CPU_SR_ALLOC();
        CPU_CRITICAL_ENTER();

        FTM_PWM_HW_CFG const *cfg = pwm_hw_attr + hw_ch;
        if ((NULL != cfg) && (NULL != cfg->obj))
        {
            cfg->obj->SC        &= ~(1u << (16 + cfg->pwm_ch)); //disable channel output.
            ftm_ch_avbl[hw_ch]   = FALSE;
//            HalGpio_InitPinGpio_Def(cfg->port_name, cfg->pin_num, PORT_OUTPUT, OUTPUT_OPEN_DRAIN, INPUT_NO_PULL);
//            HalGpio_SetPinVal(cfg->port_name, cfg->pin_num, LOW);
        }

        CPU_CRITICAL_EXIT();
    }
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void HalS32K14x_PwmChSetVal(uint8 hw_ch, const uint8 val)
{
    if ((hw_ch < PWM_HW_ATTR_SIZE) && ftm_ch_avbl[hw_ch])
    {
        CPU_SR_ALLOC();
        CPU_CRITICAL_ENTER();

        FTM_PWM_HW_CFG const *cfg = pwm_hw_attr + hw_ch;
        if ((NULL != cfg) && (NULL != cfg->obj))
        {
            cfg->obj->CONTROLS[cfg->pwm_ch].CnSC = FTM_CNSC_EDGE_ALIGNED_PWM;               // edge-aligned PWM, high true pulses */
            uint8 tmp_val                        = (val >= 100) ? 101 : val;                // to get 100% duty cycle, CnV must > MOD
            cfg->obj->CONTROLS[cfg->pwm_ch].CnV  = (uint32)(tmp_val * FTM_MOD_VAL / 100);   // set pwm duty cycle
        }

        CPU_CRITICAL_EXIT();
    }
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void HalS32K14x_PwmChGetVal(uint8 hw_ch, uint8 *val)
{
    if ((hw_ch < PWM_HW_ATTR_SIZE) && (NULL != val) && ftm_ch_avbl[hw_ch])
    {
        CPU_SR_ALLOC();
        CPU_CRITICAL_ENTER();

        FTM_PWM_HW_CFG const *cfg = pwm_hw_attr + hw_ch;
        if ((NULL != cfg) && (NULL != cfg->obj))
        {
            *val = (cfg->obj->CONTROLS[cfg->pwm_ch].CnV > FTM_MOD_VAL) ? 100 : ((uint8)(cfg->obj->CONTROLS[cfg->pwm_ch].CnV * 100 / FTM_MOD_VAL + 1));
        }

        CPU_CRITICAL_EXIT();
    }
}

