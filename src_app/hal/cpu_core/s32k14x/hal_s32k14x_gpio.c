/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#include "std_type.h"
#include "std_lib.h"

#include "hal_gpio.h"
#include "hal_int.h"
#include "gpio_interrupt.cfg"
#include "cpu.h"
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#define GPIO_PCC_PORT_BASE                                                                      ((uint32 *)(0x40065124))
#define GPIO_PCC_OPEN_PH_CLOCK                                                                  (0x40000000) 

#define GPIO_PCR_MUX_CLR_MASK                                                                   (0xFFFFF8FF)

#define GPIO_SET_AS_GPIO_MASK                                                                   (0x00000100)

#define GPIO_PCR_PE_CLR_MASK                                                                    (0xFFFFFFFD)
#define GPIO_PCR_PE_SET_MASK                                                                    (0x00000002)
#define GPIO_PCR_PS_CLR_MASK                                                                    (0xFFFFFFFE)
#define GPIO_PCR_PS_SET_MASK                                                                    (0x00000001)

#define GPIO_PCR_LK_CLR_MASK                                                                    (0xFFFF7FFF)
#define GPIO_PCR_LK_SET_MASK                                                                    (0x00008000)

#define GPIO_PCR_IRQC_CLR_MASK                                                                  (0xFFF0FFFF)
#define GPIO_SET_FALLING_EDGE_MASK                                                              (0x000A0000)
#define GPIO_SET_RISING_EDGE_MASK                                                               (0x00090000)
#define GPIO_SET_EITHER_EDGE_MASK                                                               (0x000B0000)

#define PIN_MAX_NUM                                                                             (32)
#define PORT_MAX_NUM                                                                            (5)
#define PORT_MUX_MAX_NUM                                                                        (7)
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
typedef void (*PIN_USER_ISR)(void);        //user isr pointer
typedef void (*GPIO_ISR_ENTRY)(void);      //GPIO isr entry type
typedef struct
{
	GPIO_ISR_ENTRY pin_isr[32];
}PORT_ISR;


typedef struct
{
	PORT_ISR port_isr[5];
}GPIO_ISR;
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void    S32K14x_GPIO_InitDevice	(void                                                                      );
static void    S32K14x_GPIO_InitPinGPIO (HAL_GPIO_INIT const*const init                                            );
static void    S32K14x_GPIO_SetPinVal   (HAL_PORT_NAME             pt,   unsigned char       pin, unsigned char val);
static void    S32K14x_GPIO_TogglePinVal(HAL_PORT_NAME pt, unsigned char pin                                       );
static void    S32K14x_GPIO_SetPortVal  (HAL_PORT_NAME             pt,   unsigned long const val                   );
static boolean S32K14x_GPIO_GetPinVal   (HAL_PORT_NAME             pt,   unsigned char       pin                   );
static uint32  S32K14x_GPIO_GetPortVal  (HAL_PORT_NAME             pt                                              );
static void    S32K14x_GPIO_PinAfConfig (HAL_PORT_NAME             pt,   unsigned char       pin, unsigned char af );
static void    S32K14x_GPIO_InitPinInt  (HAL_GPIO_INT const*       cfg                                             );
static void    S32K14x_GPIO_EnPinInt    (HAL_PORT_NAME             pt,   unsigned char       pin                   );
static void    S32K14x_GPIO_DisPinInt   (HAL_PORT_NAME             pt,   unsigned char       pin                   );

static void    S32K14x_GPIO_PortAISR    (void                                                                      );
static void    S32K14x_GPIO_PortBISR    (void                                                                      );
static void    S32K14x_GPIO_PortCISR    (void                                                                      );
static void    S32K14x_GPIO_PortDISR    (void                                                                      );
static void    S32K14x_GPIO_PortEISR    (void                                                                      );
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static HAL_GPIO_HW_API const gpio_hw_api = 
{
	.init_dev 	   = S32K14x_GPIO_InitDevice  ,
	.init_pin_gpio = S32K14x_GPIO_InitPinGPIO ,
	.set_pin_val   = S32K14x_GPIO_SetPinVal   ,
	.toggle_pin_val= S32K14x_GPIO_TogglePinVal,
	.set_port_val  = S32K14x_GPIO_SetPortVal  ,
	.get_pin_val   = S32K14x_GPIO_GetPinVal   ,
	.get_port_val  = S32K14x_GPIO_GetPortVal  ,
	.pin_af_config = S32K14x_GPIO_PinAfConfig ,
	.init_pin_int  = S32K14x_GPIO_InitPinInt  ,
	.en_pin_int    = S32K14x_GPIO_EnPinInt    ,
	.dis_pin_int   = S32K14x_GPIO_DisPinInt
};
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static PORT_Type       *const port_type_grp[]                           =   PORT_BASE_PTRS       ;  //PORT type group
static GPIO_Type       *const gpio_type_grp[]                           =   GPIO_BASE_PTRS       ;  //GPIO type group
static uint8            pin_int_edge[PORT_MAX_NUM][PIN_MAX_NUM]                                  ;  //GPIO isr attr group


/*gpio interrupt routine */
//lint -e10 -e65 -e133
static 	GPIO_ISR const gpio_isr = 
{
#define GPIO_INTERRUPT_CFG(port_name,pin_num,isr_routine) .port_isr[port_name].pin_isr[pin_num]= isr_routine,
	#include "gpio_interrupt.cfg"
#undef 	GPIO_INTERRUPT_CFG
};			
//lint +e10 +e65 +e133
														   
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
HAL_GPIO_HW_API const *S32K14x_GPIO_GetGPIOHwApi(void)
{
	return &gpio_hw_api;
}
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void S32K14x_GPIO_InitDevice(void)
{
	/* open all gpio clock */
	for ( uint8_t i = 0; i < PIN_MAX_NUM; i ++ )
	{
		uint32 *port_pcc_ptr	= GPIO_PCC_PORT_BASE + i;
		if ( *port_pcc_ptr & PCC_PCCn_PR_MASK )
		{
			*port_pcc_ptr		|= GPIO_PCC_OPEN_PH_CLOCK;      //open peripheral clock
		}
	}
	/* install all interrupt */
	HalInt_VectSet(PORTA_IRQn, S32K14x_GPIO_PortAISR); 
	HalInt_VectSet(PORTB_IRQn, S32K14x_GPIO_PortBISR); 
	HalInt_VectSet(PORTC_IRQn, S32K14x_GPIO_PortCISR); 
	HalInt_VectSet(PORTD_IRQn, S32K14x_GPIO_PortDISR); 
	HalInt_VectSet(PORTE_IRQn, S32K14x_GPIO_PortEISR); 
	HalInt_En(PORTA_IRQn); 
	HalInt_En(PORTB_IRQn); 	
	HalInt_En(PORTC_IRQn); 	
	HalInt_En(PORTD_IRQn); 	
	HalInt_En(PORTE_IRQn); 	
}
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void S32K14x_GPIO_InitPinGPIO(HAL_GPIO_INIT const*const init)
{
	if ((NULL != init) && (init->pt < PORT_MAX_NUM) && (init->pin < PIN_MAX_NUM))
	{
		PORT_Type *port_ptr                  = port_type_grp[(uint8)(init->pt)];
		GPIO_Type *gpio_ptr	                 = gpio_type_grp[(uint8)(init->pt)];

		CPU_SR_ALLOC();
		CPU_CRITICAL_ENTER();
		
		switch (init->mode)
		{
		case PORT_INPUT:
			port_ptr->PCR[init->pin]        &= GPIO_PCR_MUX_CLR_MASK;
			port_ptr->PCR[init->pin]        |= GPIO_SET_AS_GPIO_MASK;       //set as GPIO

			gpio_ptr->PDDR &= ~(1u << (init->pin));                         //set as digital input

			if (INPUT_NO_PULL == init->itype)                               //no pull
			{
				port_ptr->PCR[init->pin]    &= GPIO_PCR_PE_CLR_MASK;        //PE = 0
			}
			else if (INPUT_PULL_UP == init->itype)                          //pull-up
			{
				port_ptr->PCR[init->pin]    |= GPIO_PCR_PE_SET_MASK;        //PE = 1
				port_ptr->PCR[init->pin]    |= GPIO_PCR_PS_SET_MASK;        //PS = 1
			}
			else                                                            //pull-down
			{
				port_ptr->PCR[init->pin]    |= GPIO_PCR_PE_SET_MASK;        //PE = 1
				port_ptr->PCR[init->pin]    &= GPIO_PCR_PS_CLR_MASK;        //PS = 0
			}
			break;

		case PORT_OUTPUT:
			port_ptr->PCR[init->pin]        &= GPIO_PCR_MUX_CLR_MASK;
			port_ptr->PCR[init->pin]        |= GPIO_SET_AS_GPIO_MASK;       //set as GPIO

			gpio_ptr->PDDR |= (1u << (init->pin));                          //set as digital output

			if (OUTPUT_PUSH_PULL == init->otype)
			{
				port_ptr->PCR[init->pin]    |= PORT_PCR_DSE_MASK;           //high drive strength
			}
			else
			{
				port_ptr->PCR[init->pin]    &= ~PORT_PCR_DSE_MASK;          //low drive strength
			}
			break;

		case PORT_AF:
			//choose alternative type(2~7).
			if (INPUT_NO_PULL == init->itype)                               //no pull
			{
				port_ptr->PCR[init->pin]    &= GPIO_PCR_PE_CLR_MASK;        //PE = 0
			}
			else if (INPUT_PULL_UP == init->itype)                          //pull-up
			{
				port_ptr->PCR[init->pin]    |= GPIO_PCR_PE_SET_MASK;        //PE = 1
				port_ptr->PCR[init->pin]    |= GPIO_PCR_PS_SET_MASK;        //PS = 1
			}
			else                                                            //pull-down
			{
				port_ptr->PCR[init->pin]    |= GPIO_PCR_PE_SET_MASK;        //PE = 1
				port_ptr->PCR[init->pin]    &= GPIO_PCR_PS_CLR_MASK;        //PS = 0
			}

			if (OUTPUT_PUSH_PULL == init->otype)
			{
				port_ptr->PCR[init->pin]    |= PORT_PCR_DSE_MASK;           //high drive strength
			}
			else
			{
				port_ptr->PCR[init->pin]    &= ~PORT_PCR_DSE_MASK;          //low drive strength
			}
			break;

		case PORT_AN:
			port_ptr->PCR[init->pin]        &= GPIO_PCR_PE_CLR_MASK;        //PE & PS should set 0 before entering AN mode.
			port_ptr->PCR[init->pin]        &= GPIO_PCR_MUX_CLR_MASK;       //set as analog port, MUX = 000
			break;

		default:
			break;
		}

		port_ptr->PCR[init->pin]            &= GPIO_PCR_LK_CLR_MASK;        //PCR fields[15:0] not locked.

		CPU_CRITICAL_EXIT();
	}
}
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void S32K14x_GPIO_SetPinVal(HAL_PORT_NAME pt, unsigned char pin, unsigned char val)
{
	if ((pt < PORT_MAX_NUM) && (pin < PIN_MAX_NUM))
	{
		GPIO_Type  *gpio_ptr = gpio_type_grp[(uint8)pt];

		if (val > 0)
		{
			gpio_ptr->PSOR  = (1u << (pin));
		}
		else
		{
			gpio_ptr->PCOR  = (1u << (pin));
		}
	}
}
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void S32K14x_GPIO_TogglePinVal(HAL_PORT_NAME pt, unsigned char pin)
{
	if ((pt < PORT_MAX_NUM) && (pin < PIN_MAX_NUM))
	{
		GPIO_Type  *gpio_ptr = gpio_type_grp[(uint8)pt];
		gpio_ptr->PTOR       = (1u << (pin));
	}
}
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void S32K14x_GPIO_SetPortVal(HAL_PORT_NAME pt, unsigned long const val)
{
	if (pt < PORT_MAX_NUM)
	{
		GPIO_Type  *gpio_ptr    = gpio_type_grp[(uint8)pt];

		gpio_ptr->PDOR = val;
	}
}
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static boolean S32K14x_GPIO_GetPinVal(HAL_PORT_NAME pt, unsigned char pin)
{
	boolean ret             = FALSE;

	if ((pt < PORT_MAX_NUM) && (pin < PIN_MAX_NUM))
	{
		GPIO_Type *gpio_ptr = gpio_type_grp[(uint8)pt];

		if ((gpio_ptr->PDIR) & (1u << (pin)))
		{
				ret    = TRUE;
		}
	}

	return ret;
}
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static uint32 S32K14x_GPIO_GetPortVal(HAL_PORT_NAME pt)
{
	uint32 ret              = 0u;
        
	if (pt < PORT_MAX_NUM)
	{
		GPIO_Type *gpio_ptr = gpio_type_grp[(uint8)pt];

		ret = gpio_ptr->PDIR;
	}

	return ret;
}
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void S32K14x_GPIO_PinAfConfig(HAL_PORT_NAME pt, unsigned char pin, unsigned char af)
{
	if ((pt < PORT_MAX_NUM) && (pin < PIN_MAX_NUM) && (af <= PORT_MUX_MAX_NUM))
	{
		PORT_Type *port_ptr    = port_type_grp[(uint8)pt];

		CPU_SR_ALLOC();
		CPU_CRITICAL_ENTER();
		
		if (7 != af)
		{
			port_ptr->PCR[pin] &= (~PORT_PCR_PFE_MASK); //except ALT7, PFE should kept 0.
		}

		port_ptr->PCR[pin]    &= GPIO_PCR_MUX_CLR_MASK;
		port_ptr->PCR[pin]    |= ((uint32)af) << 8u;    //set mux

		CPU_CRITICAL_EXIT();
	}
}
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void S32K14x_GPIO_InitPinInt(HAL_GPIO_INT const* cfg)
{
	if ((NULL != cfg) && (cfg->pin < PIN_MAX_NUM) && (cfg->pt < PORT_MAX_NUM))
	{
		pin_int_edge[(uint8)cfg->pt][cfg->pin] 	= 	(uint8)(cfg->edge);
		PORT_Type *port_ptr         			= 	port_type_grp[(uint8)(cfg->pt)];

		CPU_SR_ALLOC();
		CPU_CRITICAL_ENTER();
		
		port_ptr->PCR[cfg->pin]               	&= 	GPIO_PCR_IRQC_CLR_MASK;             //clear IRQC[19:16],disable interrupt 

		CPU_CRITICAL_EXIT();
	}
}
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void S32K14x_GPIO_EnPinInt(HAL_PORT_NAME pt, unsigned char pin)
{
	if ((pt < PORT_MAX_NUM) && (pin < PIN_MAX_NUM))
	{
		PORT_Type  *port_ptr    = port_type_grp[(uint8)pt];

		CPU_SR_ALLOC();
		CPU_CRITICAL_ENTER();
	
		port_ptr->ISFR          = 1u << (pin);                        //clear interrupt flag

		switch (pin_int_edge[(uint8)pt][pin])
		{
		case GPIO_FALLING_EDGE:
			port_ptr->PCR[pin] |= GPIO_SET_FALLING_EDGE_MASK;         //set falling-edge
			break;

		case GPIO_RISING_EDGE:
			port_ptr->PCR[pin] |= GPIO_SET_RISING_EDGE_MASK;          //set rising-edge
			break;

		case GPIO_BOTH_EDGE:
			port_ptr->PCR[pin] |= GPIO_SET_EITHER_EDGE_MASK;          //set either-edge
			break;

		default:
			break;
		}

		CPU_CRITICAL_EXIT();
	}
}
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void S32K14x_GPIO_DisPinInt(HAL_PORT_NAME pt, unsigned char pin)
{
	if ((pt < PORT_MAX_NUM) && (pin < PIN_MAX_NUM))
	{
		PORT_Type *port_ptr = port_type_grp[(uint8)pt];

		CPU_SR_ALLOC();
		CPU_CRITICAL_ENTER();	
		
		port_ptr->PCR[pin] &= GPIO_PCR_IRQC_CLR_MASK;                      //clear IRQC[19:16], disable ISF

		CPU_CRITICAL_EXIT();

	}
}
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void S32K14x_GPIO_PortAISR(void)
{
	PORT_ISR const*port_isr = gpio_isr.port_isr + HAL_GPIO_PT_A;
	for (uint8 i = 0; i < PIN_MAX_NUM; i++)
	{
		if (PORTA->ISFR & (1u << i))
		{
			PORTA->ISFR = 1u << i;                                  //clear interrupt flag

			if (NULL != port_isr->pin_isr[i])
			{
			    port_isr->pin_isr[i]();                     //callback user isr
			}
		}
	}
}
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void S32K14x_GPIO_PortBISR(void)
{
	PORT_ISR const*port_isr = gpio_isr.port_isr + HAL_GPIO_PT_B;
	for (uint8 i = 0; i < PIN_MAX_NUM; i++)
	{
		if (PORTB->ISFR & (1u << i))
		{
			PORTB->ISFR = 1u << i;                                  //clear interrupt flag

			if (NULL != port_isr->pin_isr[i])
			{
			    port_isr->pin_isr[i]();                     //callback user isr
			}
		}
	}
}
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void S32K14x_GPIO_PortCISR(void)
{
	PORT_ISR const*port_isr = gpio_isr.port_isr + HAL_GPIO_PT_C;
	for (uint8 i = 0; i < PIN_MAX_NUM; i++)
	{
		if (PORTC->ISFR & (1u << i))
		{
			PORTC->ISFR = 1u << i;                                  //clear interrupt flag

			if (NULL != port_isr->pin_isr[i])
			{
			    port_isr->pin_isr[i]();                     //callback user isr
			}
		}
	}
}
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void S32K14x_GPIO_PortDISR(void)
{
	PORT_ISR const*port_isr = gpio_isr.port_isr + HAL_GPIO_PT_D;
	for (uint8 i = 0; i < PIN_MAX_NUM; i++)
	{
		if (PORTD->ISFR & (1u << i))
		{
			PORTD->ISFR = 1u << i;                                  //clear interrupt flag

			if (NULL != port_isr->pin_isr[i])
			{
			    port_isr->pin_isr[i]();                     //callback user isr
			}
		}
	}
}
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void S32K14x_GPIO_PortEISR(void)
{
	PORT_ISR const*port_isr = gpio_isr.port_isr + HAL_GPIO_PT_E;
	for (uint8 i = 0; i < PIN_MAX_NUM; i++)
	{
		if (PORTE->ISFR & (1u << i))
		{
			PORTE->ISFR = 1u << i;                                  //clear interrupt flag

			if (NULL != port_isr->pin_isr[i])
			{
			    port_isr->pin_isr[i]();                     //callback user isr
			}
		}
	}
}



