/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#include "std_type.h"
#include "std_lib.h"

#include "hal_spi.h"
#include "hal_gpio.h"
#include "hal_s32k14x_scg.h"
#include "hal_s32k14x_edma.h"
#include "hal_hw.h"

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#define SPI_PCC_BASE                                                 ((uint32 *)0x400650B0u)
#define SPI_PCC_PCS_SOSC_MASK                                        (0x01000000)
#define SPI_PCC_PCS_SIRC_MASK                                        (0x02000000)
#define SPI_PCC_PCS_FIRC_MASK                                        (0x03000000)
#define SPI_PCC_PCS_SPLL_MASK                                        (0x06000000)

#define SPI_FCR_RXWATER_2_WORDS_MASK                                 (0x00020000)
#define SPI_FCR_TXWATER_2_WORDS_MASK                                 (0x00000002)

#define SPI_CCR_SCKPCS_10_CYCLES_MASK                                (0x09000000)
#define SPI_CCR_PCSSCK_10_CYCLES_MASK                                (0x00090000)
#define SPI_CCR_DBT_10_CYCLES_MASK                                   (0x00000800)

#define SPI_TCR_FRAMESZ_8_BIT_MASK                                   (7)
#define SPI_TCR_FRAMESZ_16_BIT_MASK                                  (15)
#define SPI_TCR_FRAMESZ_24_BIT_MASK                                  (23)
#define SPI_TCR_FRAMESZ_32_BIT_MASK                                  (31)

#define SPI_SR_CLR_ALL_W1C_FLAGS_MASK                                (0x00003F00)

#define SPI_BUS_CLOCK_FREQ                                           (48 / 2 * 1000 * 1000)                 //24MHz


#define HW_SPI_NUM													 sizeof(spi_hw_attr)/sizeof(SPI_HW_ATTR)

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
//SPI HW attr tyoe
typedef struct
{
	LPSPI_Type 			*obj;

	SPI_CLOCK_DIR		spi_clock;
	SPI_TRANS_MOD		spi_trans;
	void				(*calbk)(uint8 hw_ch);
	
	HAL_PORT_NAME    	spi_sck_port;
	unsigned char      	spi_sck_pin;
	HAL_PORT_NAME    	spi_mosi_port;
	unsigned char     	spi_mosi_pin;
	HAL_PORT_NAME    	spi_miso_port;
	unsigned char     	spi_miso_pin;
	HAL_PORT_NAME    	spi_nss_port;
	unsigned char     	spi_nss_pin;
	//unsigned char 	spi_nss_num;
	boolean				spi_nss_actv;
}SPI_HW_ATTR;

typedef void (*SPI_TRANS_DMA_FUNC)(void);  //callback type for DMA engine
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
//spi attribute
#define CPU_SPI_HW_DEF(spi_hw_name,hw_obj,master,trans,calbk,sck_pt,sck_pin,mosi_pt,mosi_pin,miso_pt,miso_pin,nss_pt,nss_pin,nss_actv)	\
		{hw_obj,SPI_##master,SPI_##trans,calbk,sck_pt,sck_pin,mosi_pt,mosi_pin,miso_pt,miso_pin,nss_pt,nss_pin,nss_actv},
static const SPI_HW_ATTR spi_hw_attr[] = 
{
	#include "cpu_spi.cfg"
};
#undef CPU_SPI_HW_DEF
// for hw dma callback
#define CPU_SPI_HW_DEF(spi_hw_name,hw_obj,master,trans,s_calbk,sck_pt,sck_pin,mosi_pt,mosi_pin,miso_pt,miso_pin,nss_pt,nss_pin,nss_actv)   \
	static void HalS32k_DmaTransCalBk_##spi_hw_name(void) {if(spi_hw_attr[spi_hw_name##_ID].calbk!=NULL){spi_hw_attr[spi_hw_name##_ID].calbk(spi_hw_name##_ID);}}
	#include "cpu_spi.cfg"
#undef CPU_SPI_HW_DEF

static const SPI_TRANS_DMA_FUNC spi_dma_calbk[HW_SPI_NUM] =
{
  	#define CPU_SPI_HW_DEF(spi_hw_name,hw_obj,master,trans,calbk,sck_pt,sck_pin,mosi_pt,mosi_pin,miso_pt,miso_pin,nss_pt,nss_pin,nss_actv) \
		HalS32k_DmaTransCalBk_##spi_hw_name,
  		#include "cpu_spi.cfg"
  	#undef CPU_SPI_HW_DEF
};

static uint8_t spi_nss_af_num[HW_SPI_NUM];

static uint32_t const spi_baud[HAL_SPI_BAUD_MAX]=
{
	100000,125000,250000,500000,1000000,2000000,4000000,8000000,9000000
};

//static const uint32_t baudratePrescaler[] = { 1, 2, 4, 8, 16, 32, 64, 128 };
static uint32 const					tx_tmp = 0x82828282u; //for DMA transmit when SPI tx buffer NULL
static uint32 						rx_tmp = 0u; //for DMA receive when SPI rx buffer NULL
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void S32K14x_SPI_Init(uint8 hw_ch, const SPI_DEVICE * spi_dev);
//static void S32K14x_SPI_DeInit(uint8 hw_ch);
static void S32K14x_SPI_Com (uint8 hw_ch, const uint16       len, const uint8 *tx_dat, uint8 *rx_dat);
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

HAL_SPI_HW_API const spi_hw_api =
{
	.init   = S32K14x_SPI_Init,
//	.deinit = S32K14x_SPI_DeInit,
	.com    = S32K14x_SPI_Com
};
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
HAL_SPI_HW_API const *S32K14x_SPI_GetSPIHwApi(void)
{
	return &spi_hw_api;
}


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void HalS32K14x_SpiInit(void)
{
	for (uint8_t i = 0; i < HW_SPI_NUM; i ++ )
	{	
		SPI_HW_ATTR const *hw  = spi_hw_attr + i;
		__IO uint32_t     *pcc = NULL;
		/* enable clock and gpio */
		if ( hw->obj == LPSPI0 )
		{
			pcc = PCC->PCCn + PCC_LPSPI0_INDEX;
		}
		else if ( hw->obj == LPSPI1 )
		{
			pcc = PCC->PCCn + PCC_LPSPI1_INDEX;
		}
		else if ( hw->obj == LPSPI2 )
		{
			pcc = PCC->PCCn + PCC_LPSPI2_INDEX;
		}
		else
		{}
		
		if ( (pcc != NULL) && ((*pcc) & PCC_PCCn_PR_MASK) )
		{	// enable gpio 
			*pcc &= ~PCC_PCCn_CGC_MASK;
			*pcc &= ~PCC_PCCn_PCS_MASK;                    	//clear PCS
			*pcc |= PCC_PCCn_PCS(6);						//select SPLL
			*pcc |= PCC_PCCn_CGC_MASK;						//enable spi clock

			HalGpio_InitPinGpio_Def(hw->spi_sck_port,	hw->spi_sck_pin,	PORT_AF,OUTPUT_PUSH_PULL,INPUT_NO_PULL);
			HalGpio_InitPinGpio_Def(hw->spi_mosi_port,	hw->spi_mosi_pin,	PORT_AF,OUTPUT_PUSH_PULL,INPUT_NO_PULL);
			HalGpio_InitPinGpio_Def(hw->spi_miso_port,	hw->spi_miso_pin,	PORT_AF,OUTPUT_PUSH_PULL,INPUT_NO_PULL);
			HalGpio_InitPinGpio_Def(hw->spi_nss_port,	hw->spi_nss_pin,	PORT_AF,OUTPUT_PUSH_PULL,INPUT_NO_PULL);

			if ( hw->obj == LPSPI0 )
			{
				if ( hw->spi_sck_port == HAL_GPIO_PT_D ){	// PTD15
					HalGpio_PinAFConfig(hw->spi_sck_port,	hw->spi_sck_pin,	4);
				}else{
					HalGpio_PinAFConfig(hw->spi_sck_port,	hw->spi_sck_pin,	3);
				}
				if ( hw->spi_mosi_port == HAL_GPIO_PT_E ){	// PTE2 
					HalGpio_PinAFConfig(hw->spi_mosi_port,	hw->spi_mosi_pin,	2);
				}else{
					HalGpio_PinAFConfig(hw->spi_mosi_port,	hw->spi_mosi_pin,	3);
				}
				if ( hw->spi_miso_port == HAL_GPIO_PT_E ){ // PTE1
					HalGpio_PinAFConfig(hw->spi_miso_port,	hw->spi_miso_pin,	2);
				}else if ( hw->spi_miso_port == HAL_GPIO_PT_D ){ // PTD16
					HalGpio_PinAFConfig(hw->spi_miso_port,	hw->spi_miso_pin,	4);
				}else{
					HalGpio_PinAFConfig(hw->spi_miso_port,	hw->spi_miso_pin,	3);
				}

				if (hw->spi_nss_port==HAL_GPIO_PT_E ) { // PTE6, PCS2
					spi_nss_af_num[i] = 2;
				}else{
					spi_nss_af_num[i] = 3;
				}
				
			}
			else if ( hw->obj == LPSPI1 )
			{
				
				HalGpio_PinAFConfig(hw->spi_sck_port,	hw->spi_sck_pin,	3);
				if ( hw->spi_mosi_port == HAL_GPIO_PT_E ){	// PTE0 
					HalGpio_PinAFConfig(hw->spi_mosi_port,	hw->spi_mosi_pin,	5);
				}else{
					HalGpio_PinAFConfig(hw->spi_mosi_port,	hw->spi_mosi_pin,	3);
				}
				HalGpio_PinAFConfig(hw->spi_miso_port,	hw->spi_miso_pin,	3);
				if (hw->spi_nss_port==HAL_GPIO_PT_E ) { // PTE1, PCS0
					spi_nss_af_num[i] = 5;
				}else{
					spi_nss_af_num[i] = 3;
				}
				
				/* MOSI <--> MISO */
			    hw->obj->CFGR1 |= LPSPI_CFGR1_PINCFG(3);
			}
			else if ( hw->obj == LPSPI2 )
			{
				HalGpio_PinAFConfig(hw->spi_sck_port,	hw->spi_sck_pin,	3);
				HalGpio_PinAFConfig(hw->spi_mosi_port,	hw->spi_mosi_pin,	3);
				HalGpio_PinAFConfig(hw->spi_miso_port,	hw->spi_miso_pin,	3);
				
				if (hw->spi_nss_port==HAL_GPIO_PT_E ) { // PTE11, PCS0
					spi_nss_af_num[i] = 2;
				}else if ( hw->spi_nss_port==HAL_GPIO_PT_A ) { // PTA15,PCS3,
					spi_nss_af_num[i] = 4;
				}else{
					spi_nss_af_num[i] = 3;
				}
			}
			else
			{}

			HalGpio_PinAFConfig(hw->spi_nss_port,	hw->spi_nss_pin,	spi_nss_af_num[i]);
			if ( hw->spi_nss_actv ){	// activate high 
				hw->obj->CFGR1     |= LPSPI_CFGR1_PCSPOL(0x0f);
			}else{
				hw->obj->CFGR1     &= ~LPSPI_CFGR1_PCSPOL(0x0f);
			}
		}
	}
}
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void S32K14x_SPI_Init(uint8 hw_ch, const SPI_DEVICE * spi_dev)
{
	if ( hw_ch < HW_SPI_NUM && spi_dev != NULL )
	{
		SPI_HW_ATTR const *hw  = spi_hw_attr + hw_ch;
		LPSPI_Type *spi_obj    = hw->obj;

		/* reset all fifo and internal logic and module enable */
		spi_obj->CR        |= (LPSPI_CR_RRF_MASK + LPSPI_CR_RTF_MASK + LPSPI_CR_MEN_MASK);
		/* Now bring the LPSPI module out of reset */
		spi_obj->CR        &= ~(LPSPI_CR_RST_MASK);                    //close reset

		uint32_t tcr = 0;

		if (hw->spi_clock == SPI_MASTER)
		{
			spi_obj->CFGR1 |= LPSPI_CFGR1_MASTER_MASK;                 //master mode enable

			uint32_t baud = spi_baud[HAL_SPI_BAUD_MAX - 1];
			if (spi_dev->spi_baud < HAL_SPI_BAUD_MAX)
			{
				baud = spi_baud[spi_dev->spi_baud];
			}
			uint32_t spi_clk = HalS32K14x_ScgGetFreq(SPLLDIV2_CLK);
			uint16_t sckdiv = (uint16_t)(spi_clk / baud);
			
			if (sckdiv > 257)
			{	// set PRESCALE = 011, divide by 8
				sckdiv = sckdiv / 8;
				tcr |= LPSPI_TCR_PRESCALE(3);
			}
			if ((spi_clk % baud) >= (baud / 2))
			{
				sckdiv += 1; //fix div
			}
			if (sckdiv > 2)
			{
				sckdiv = sckdiv - 2;
			}
			else
			{
				sckdiv = 0u;
			}
			spi_obj->CCR = LPSPI_CCR_SCKDIV(sckdiv);	/*lint !e835*/
		}
		else if (hw->spi_clock == SPI_SLAVE)
		{
			spi_obj->CFGR1 &= ~LPSPI_CFGR1_MASTER_MASK;                 //slave mode enable
		}
		else
		{
		}

		if (spi_dev->spi_attr & SPI_CPOL_HIGH){
		    tcr        |= LPSPI_TCR_CPOL_MASK;
		}
		if (!(spi_dev->spi_attr & SPI_CPHA_FIRST_EDGE)){
		    tcr        |= LPSPI_TCR_CPHA_MASK;
		}
		if (spi_dev->spi_attr & SPI_NSS_HARD)
		{
			HalGpio_PinAFConfig(hw->spi_nss_port,hw->spi_nss_pin,spi_nss_af_num[hw_ch]);
		    uint8 pcs  =  (spi_dev->spi_attr & 0x30) >> 4u;        //flag bit 4,5 for pcs, only when nss hard valid
		    tcr        |= LPSPI_TCR_PCS(pcs);
		    if ( hw->spi_nss_actv )
		    {	// activate high 
		    	spi_obj->CFGR1     |= LPSPI_CFGR1_PCSPOL(1u<<pcs);
		    }
		    else
		    {
		    	spi_obj->CFGR1     &= ~LPSPI_CFGR1_PCSPOL(1u<<pcs);
		    }
		}
		else
		{
			HalGpio_InitPinGpio_Def(hw->spi_nss_port,hw->spi_nss_pin,	PORT_OUTPUT,OUTPUT_PUSH_PULL,INPUT_NO_PULL);
			//HalGpio_PinAFConfig(hw->spi_nss_port,	hw->spi_nss_pin,	1);
			HalGpio_SetPinVal(hw->spi_nss_port, hw->spi_nss_pin, (uint8)(!hw->spi_nss_actv));
		}

		if (!(spi_dev->spi_attr & SPI_MSB_FIRST)){
		    tcr        |= LPSPI_TCR_LSBF_MASK;
		}

		if ( (spi_dev->spi_attr & SPI_DATA_LENGTH_32) == SPI_DATA_LENGTH_32 ){
			tcr        |= 31;
		}else if ((spi_dev->spi_attr & SPI_DATA_LENGTH_24) == SPI_DATA_LENGTH_24){
			tcr        |= 23;
		}
		else if ((spi_dev->spi_attr & SPI_DATA_LENGTH_16) == SPI_DATA_LENGTH_16){
		    tcr        |= 15;
		}else{
		    tcr        |= 7;
		}
		spi_obj->TCR    = tcr;
	}
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
//static void S32K14x_SPI_DeInit(uint8 hw_ch)
//{
//	if ( hw_ch < HW_SPI_NUM)
//	{
//		SPI_HW_ATTR const *hw  = spi_hw_attr + hw_ch;
//		HalGpio_InitPinGpio_Def(hw->spi_sck_port,   hw->spi_sck_pin,    PORT_OUTPUT,OUTPUT_OPEN_DRAIN,INPUT_NO_PULL);
//		if ( hw->obj == LPSPI1 )
//		{
//			HalGpio_InitPinGpio_Def(hw->spi_mosi_port,  hw->spi_mosi_pin,   PORT_INPUT,OUTPUT_OPEN_DRAIN,INPUT_NO_PULL);
//			HalGpio_InitPinGpio_Def(hw->spi_miso_port,  hw->spi_miso_pin,   PORT_OUTPUT,OUTPUT_OPEN_DRAIN,INPUT_NO_PULL);
//			HalGpio_SetPinVal(hw->spi_miso_port,  hw->spi_miso_pin, LOW);
//
//		}
//		else
//		{
//			HalGpio_InitPinGpio_Def(hw->spi_mosi_port,  hw->spi_mosi_pin,   PORT_OUTPUT,OUTPUT_OPEN_DRAIN,INPUT_NO_PULL);
//			HalGpio_InitPinGpio_Def(hw->spi_miso_port,  hw->spi_miso_pin,   PORT_INPUT,OUTPUT_OPEN_DRAIN,INPUT_NO_PULL);
//			HalGpio_SetPinVal(hw->spi_mosi_port,  hw->spi_mosi_pin, LOW);
//		}
//		HalGpio_InitPinGpio_Def(hw->spi_nss_port,   hw->spi_nss_pin,    PORT_OUTPUT,OUTPUT_OPEN_DRAIN,INPUT_NO_PULL);
//
//		HalGpio_SetPinVal(hw->spi_sck_port,   hw->spi_sck_pin, LOW);
//		HalGpio_SetPinVal(hw->spi_nss_port,   hw->spi_nss_pin, LOW);
//	}
//}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void S32K14x_SPISyncOperation(LPSPI_Type *spi_obj, const uint16 len, const uint8 *tx_dat, uint8 *rx_dat)
{
	if ((NULL != spi_obj) && (len > 0u))
	{
		spi_obj->CR |= LPSPI_CR_RTF_MASK + LPSPI_CR_RRF_MASK;           //clear FIFO
		spi_obj->TCR &= ~(LPSPI_TCR_RXMSK_MASK + LPSPI_TCR_TXMSK_MASK);  //clear tx and rx mask

		if (tx_dat != NULL)
		{
			if (rx_dat != NULL)
			{
				if (SPI_TCR_FRAMESZ_8_BIT_MASK == (spi_obj->TCR & LPSPI_TCR_FRAMESZ_MASK))
				{
					for (uint16 i = 0u; i < len; i++)    //one byte transfer
					{
						spi_obj->TDR = tx_dat[i];
						while (!(spi_obj->SR & LPSPI_SR_RDF_MASK)){ ; }                                   //wait RDF=1,wait rx fifo not empty

						rx_dat[i] = (uint8)(spi_obj->RDR);
					}
				}
				else if (SPI_TCR_FRAMESZ_16_BIT_MASK == (spi_obj->TCR & LPSPI_TCR_FRAMESZ_MASK))
				{
					uint16_t *tx_ = (void*)tx_dat;
					uint16_t *rx_ = (void*)rx_dat;

					for (uint16 i = 0u; i < len / 2; i++) //two bytes transfer
					{
						spi_obj->TDR = tx_[i];
						while (!(spi_obj->SR & LPSPI_SR_RDF_MASK)){ ; }                                   //wait RDF=1
						rx_[i] = (uint16)(spi_obj->RDR);
					}
				}
				else if (SPI_TCR_FRAMESZ_32_BIT_MASK == (spi_obj->TCR & LPSPI_TCR_FRAMESZ_MASK))
				{
					uint32_t *tx_ = (void*)tx_dat;
					uint32_t *rx_ = (void*)rx_dat;
					uint16_t rx_num = 0u;
					for (uint16 i = 0u; i < len / 4; i++) 			//four bytes transfer
					{
						while ((spi_obj->FSR & 0xFF) > 2){ ; }		// at most 3 byte in tx fifo
						spi_obj->TDR = tx_[i];
						while (spi_obj->SR & LPSPI_SR_RDF_MASK)
						{
							rx_[rx_num++] = spi_obj->RDR;			// read spi received
						}
					}
					while (rx_num < len / 4)
					{
						if (spi_obj->SR & LPSPI_SR_RDF_MASK)
						{
							rx_[rx_num++] = spi_obj->RDR;	// read spi received
						}
					}
				}
				else
				{
					//other framesize type
				}
			}
			else
			{
				if (SPI_TCR_FRAMESZ_8_BIT_MASK == (spi_obj->TCR & LPSPI_TCR_FRAMESZ_MASK))
				{
					for (uint16 i = 0u; i < len; i++)    //one byte transfer
					{
						while ((spi_obj->FSR & 0xFF) >= 4){ ; }
						spi_obj->TDR = tx_dat[i];
						(void)spi_obj->RDR;				// read spi received data  
					}
				}
				else if (SPI_TCR_FRAMESZ_16_BIT_MASK == (spi_obj->TCR & LPSPI_TCR_FRAMESZ_MASK))
				{
					uint16_t *tx_ = (void*)tx_dat;
					for (uint16 i = 0u; i < len / 2; i++) 	//two bytes transfer
					{
						while ((spi_obj->FSR & 0xFF) >= 4){ ; }
						spi_obj->TDR = tx_[i];
						(void)spi_obj->RDR;					// read spi received data
					}
				}
				else if (SPI_TCR_FRAMESZ_32_BIT_MASK == (spi_obj->TCR & LPSPI_TCR_FRAMESZ_MASK))
				{
					uint32_t *tx_ = (void*)tx_dat;
					for (uint16 i = 0u; i < len / 4; i++) 	//four bytes transfer
					{
						while ((spi_obj->FSR & 0xFF) >= 4){ ; }
						spi_obj->TDR = tx_[i];
						(void)spi_obj->RDR;	// read spi received data
					}
				}
				else
				{
				}
			}
		}
		else
		{
			if (rx_dat != NULL)
			{
				if (SPI_TCR_FRAMESZ_8_BIT_MASK == (spi_obj->TCR & LPSPI_TCR_FRAMESZ_MASK))
				{
					for (uint16 i = 0; i < len; i++)    //one byte transfer
					{
						spi_obj->TDR = 0x00;
						while (!(spi_obj->SR & LPSPI_SR_RDF_MASK)){ ; }                                   //wait RDF=1,rx fifo not empty
						rx_dat[i] = (uint8)(spi_obj->RDR);
					}
				}
				else if (SPI_TCR_FRAMESZ_16_BIT_MASK == (spi_obj->TCR & LPSPI_TCR_FRAMESZ_MASK))
				{
					uint16_t *rx_ = (void*)rx_dat;
					for (uint16 i = 0u; i < len / 2; i++) //two bytes transfer
					{
						spi_obj->TDR = 0x00;
						while (!(spi_obj->SR & LPSPI_SR_RDF_MASK)){ ; }                                   //wait RDF=1
						rx_[i] = (uint16)(spi_obj->RDR);
					}
				}
				else if (SPI_TCR_FRAMESZ_32_BIT_MASK == (spi_obj->TCR & LPSPI_TCR_FRAMESZ_MASK))
				{
					uint32_t *rx_ = (void*)rx_dat;
					uint16_t rx_num = 0u;
					for (uint16 i = 0u; i < len / 4; i++) //four bytes transfer
					{
						while ((spi_obj->FSR & 0xFF) > 2){ ; }		// at most 3 byte in tx fifo
						spi_obj->TDR = 0x00;
						while (spi_obj->SR & LPSPI_SR_RDF_MASK)
						{
							rx_[rx_num++] = spi_obj->RDR;			// read spi received
						}
					}
					while (rx_num < len / 4)
					{
						if (spi_obj->SR & LPSPI_SR_RDF_MASK)
						{
							rx_[rx_num++] = spi_obj->RDR;	// read spi received
						}
					}

				}
				else{}//other framesize type
			}
			else
			{	//rx_dat = NULL
				uint16_t len_x = 0u;
				if (SPI_TCR_FRAMESZ_8_BIT_MASK == (spi_obj->TCR & LPSPI_TCR_FRAMESZ_MASK)){
					len_x = len;
				}
				else if (SPI_TCR_FRAMESZ_16_BIT_MASK == (spi_obj->TCR & LPSPI_TCR_FRAMESZ_MASK)){
					len_x = len / 2;
				}
				else if (SPI_TCR_FRAMESZ_32_BIT_MASK == (spi_obj->TCR & LPSPI_TCR_FRAMESZ_MASK)){
					len_x = len / 4;
				}
				else{}
				for (uint16 i = 0u; i < len_x; i++)    //one byte transfer
				{
					while ((spi_obj->FSR & 0xFF) >= 4){ ; }
					spi_obj->TDR = 0x00;
					(void)spi_obj->RDR;	// read spi received data 
				}
			}
		}

		while (spi_obj->SR&LPSPI_SR_MBF_MASK){ ; }

		spi_obj->SR = SPI_SR_CLR_ALL_W1C_FLAGS_MASK; // Clear all flag
		while ((spi_obj->SR & LPSPI_SR_RDF_MASK))	//read all data, wait RDF=1
		{
			(void)spi_obj->RDR;
		}
	}
}
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void S32K14x_SPIAsyncOperation(LPSPI_Type *spi_obj, uint8 hw_ch, const uint16 len, const uint8 *tx_dat, uint8 *rx_dat)
{
	if ((NULL != spi_obj) && (len > 0u) && (hw_ch < HW_SPI_NUM))
	{
		SPI_HW_ATTR const 	*hw  		= spi_hw_attr + hw_ch;
		uint8				tx_channel 	= EDMA_CH_MAX;  //used channel,0~15
		uint8				rx_channel 	= EDMA_CH_MAX;  //used channel,0~15

		spi_obj->CR |= LPSPI_CR_RRF_MASK | LPSPI_CR_RTF_MASK; 		//CLEAR SPI FIFO
		spi_obj->TCR &= ~(LPSPI_TCR_RXMSK_MASK | LPSPI_TCR_TXMSK_MASK);  //clear tx and rx mask
		spi_obj->DER = LPSPI_DER_TDDE_MASK | LPSPI_DER_RDDE_MASK; 	//Tx DMA request enabled.
		// spi_obj->FCR |= SPI_FCR_RXWATER_2_WORDS_MASK | SPI_FCR_TXWATER_2_WORDS_MASK;

		//config source and channel
		if (hw->obj == LPSPI0)
		{
			HalS32K14x_eDMASetChannelIsr(EDMA_CH_1, spi_dma_calbk[hw_ch]);

			HalS32K14x_eDMAConfigSourceChannel(EDMA_CH_0, EDMA_REQ_LPSPI0_RX); //SPI0_RX-->DMA channel 0
			HalS32K14x_eDMAConfigSourceChannel(EDMA_CH_1, EDMA_REQ_LPSPI0_TX); //SPI0_TX-->DMA channel 1

			rx_channel = EDMA_CH_0;
			tx_channel = EDMA_CH_1;
		}
		else if (hw->obj == LPSPI1)
		{
			HalS32K14x_eDMASetChannelIsr(EDMA_CH_3, spi_dma_calbk[hw_ch]);

			HalS32K14x_eDMAConfigSourceChannel(EDMA_CH_2, EDMA_REQ_LPSPI1_RX); //SPI1_RX-->DMA channel 2
			HalS32K14x_eDMAConfigSourceChannel(EDMA_CH_3, EDMA_REQ_LPSPI1_TX); //SPI1_TX-->DMA channel 3

			rx_channel = EDMA_CH_2;
			tx_channel = EDMA_CH_3;
		}
		else if (hw->obj == LPSPI2)
		{
			HalS32K14x_eDMASetChannelIsr(EDMA_CH_5, spi_dma_calbk[hw_ch]);

			HalS32K14x_eDMAConfigSourceChannel(EDMA_CH_4, EDMA_REQ_LPSPI2_RX); //SPI2_RX-->DMA channel 4
			HalS32K14x_eDMAConfigSourceChannel(EDMA_CH_5, EDMA_REQ_LPSPI2_TX); //SPI2_TX-->DMA channel 5

			rx_channel = EDMA_CH_4;
			tx_channel = EDMA_CH_5;
		}
		else
		{
		}

		if ((tx_channel != EDMA_CH_MAX) && (rx_channel != EDMA_CH_MAX))
		{
			uint8 frame_size = (uint8)(((spi_obj->TCR) & LPSPI_TCR_FRAMESZ_MASK) + 1); //frame size

			HalS32K14x_eDMAChInit(tx_channel); //init TCD
			HalS32K14x_eDMAChInit(rx_channel); 

			if (tx_dat != NULL)
			{
				HalS32K14x_eDMASetTCDSrcDestAddr(tx_channel, (uint32)tx_dat, (uint32)(&(spi_obj->TDR)));
				HalS32K14x_eDMASetTCDSOFF(tx_channel, frame_size / 8);
			}
			else
			{
				HalS32K14x_eDMASetTCDSrcDestAddr(tx_channel, (uint32)(&tx_tmp), (uint32)(&(spi_obj->TDR))); //
				HalS32K14x_eDMASetTCDSOFF(tx_channel, 0u); //tx offset 0
			}

			HalS32K14x_eDMASetTCDFrameAttr(tx_channel, len, frame_size / 8);
			HalS32K14x_eDMASetTCDDOFF(tx_channel, 0u);
			HalS32k14x_eDMAReqTrigTypeSet(tx_channel, TRUE); //tx use inetrrupt

			if (rx_dat != NULL)
			{
				if (!(spi_obj->RSR & LPSPI_RSR_SOF_MASK))
				{
					HalS32K14x_eDMASetTCDSrcDestAddr(rx_channel, (uint32)(&(spi_obj->RDR)), (uint32)rx_dat);
					HalS32K14x_eDMASetTCDDOFF(rx_channel, frame_size / 8);
				}
				else
				{
					HalS32K14x_eDMASetTCDSrcDestAddr(rx_channel, (uint32)(&(spi_obj->RDR)), (uint32)(&rx_tmp));
					HalS32K14x_eDMASetTCDDOFF(rx_channel, 0u);
				}
			}
			else
			{
				HalS32K14x_eDMASetTCDSrcDestAddr(rx_channel, (uint32)(&(spi_obj->RDR)), (uint32)(&rx_tmp));
				HalS32K14x_eDMASetTCDDOFF(rx_channel, 0u);
			}

			HalS32K14x_eDMASetTCDFrameAttr(rx_channel, len, frame_size / 8);
			HalS32K14x_eDMASetTCDSOFF(rx_channel, 0u);
			HalS32k14x_eDMAReqTrigTypeSet(rx_channel, FALSE); //rx use auto dis

			HalS32K14x_eDMAEnableChErrInt(tx_channel); //enable channel error interrupt 
			HalS32k14x_eDMAERQSet(rx_channel, TRUE);   //open RX ERQ,automatically cleared by hw after recv done.
			HalS32k14x_eDMAERQSet(tx_channel, TRUE);   //start DMA transfer
		}
	}
}
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void S32K14x_SPI_Com(uint8 hw_ch, const uint16 len, const uint8 *tx_dat, uint8 *rx_dat)
{
	if ((hw_ch < HW_SPI_NUM) && (len > 0u))
	{
		LPSPI_Type *spi_obj = (spi_hw_attr + hw_ch)->obj;

		if ( (spi_hw_attr + hw_ch)->spi_trans == SPI_SYNC )
		{
			if ((spi_hw_attr + hw_ch)->spi_clock == SPI_MASTER)
			{
				S32K14x_SPISyncOperation(spi_obj, len, tx_dat, rx_dat);
			}
		}
		else
		{
			S32K14x_SPIAsyncOperation(spi_obj, hw_ch,len, tx_dat, rx_dat);
		}
	}
}

