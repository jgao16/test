/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    s32k eeprom - dflash driver api
    All rights reserved
>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

/* include the header files */
#include "std_type.h"
#include "hal_eep.h"
#include "std_lib.h"
#include "hal_int.h"
#include "cpu.h"

/*------------- Flash hardware algorithm operation commands -------------*/
#define FLASH_CMD_VERIFY_BLOCK               0x00U
#define FLASH_CMD_VERIFY_SECTION             0x01U
#define FLASH_CMD_PROGRAM_CHECK              0x02U
#define FLASH_CMD_READ_RESOURCE              0x03U
#define FLASH_CMD_PROGRAM_LONGWORD           0x06U
#define FLASH_CMD_PROGRAM_PHRASE             0x07U
#define FLASH_CMD_ERASE_BLOCK                0x08U
#define FLASH_CMD_ERASE_SECTOR               0x09U
#define FLASH_CMD_PROGRAM_SECTION            0x0BU
#define FLASH_CMD_VERIFY_ALL_BLOCK           0x40U
#define FLASH_CMD_READ_ONCE                  0x41U
#define FLASH_CMD_PROGRAM_ONCE               0x43U
#define FLASH_CMD_ERASE_ALL_BLOCK            0x44U
#define FLASH_CMD_SECURITY_BY_PASS           0x45U
#define FLASH_CMD_PFLASH_SWAP                0x46U
#define FLASH_CMD_PROGRAM_PARTITION          0x80U
#define FLASH_CMD_SET_EERAM                  0x81U
#define FLASH_CMD_ERASE_ALL_BLOCK_UNSECURE   0x49U
/*------------------eeprom size parameter--------------------*/
#define EEPROM_0      0xF      
#define EEPROM_32     0x9    
#define EEPROM_64     0x8    
#define EEPROM_128    0x7    
#define EEPROM_256    0x6    
#define EEPROM_512    0x5    
#define EEPROM_1024   0x4    
#define EEPROM_2048   0x3
#define EEPROM_4096   0x2  
/* Defines for the dflash_size parameter */
#define EFLASH_SIZE_0K     0x00
#define EFLASH_SIZE_32K    0x03
#define EFLASH_SIZE_64K    0x04
/* FTFC -FCCOB Register instance definitions */
#define FTFC_FCCOB3			FTFC->FCCOB[0] 
#define FTFC_FCCOB2			FTFC->FCCOB[1]
#define FTFC_FCCOB1			FTFC->FCCOB[2]
#define FTFC_FCCOB0			FTFC->FCCOB[3]
#define FTFC_FCCOB7			FTFC->FCCOB[4]
#define FTFC_FCCOB6			FTFC->FCCOB[5]
#define FTFC_FCCOB5			FTFC->FCCOB[6]
#define FTFC_FCCOB4			FTFC->FCCOB[7]
#define FTFC_FCCOBB			FTFC->FCCOB[8]
#define FTFC_FCCOBA			FTFC->FCCOB[9]
#define FTFC_FCCOB9			FTFC->FCCOB[10]
#define FTFC_FCCOB8			FTFC->FCCOB[11]

#define SET_FLEXRAM_RAM		0xFF
#define SET_FLEXRAM_EEP		0x00


#define EEP_DATA_SZIE		2048	//2048 byte
#define EEP_BCKUP_SIZE		32		//32k

#define EEP_START_ADDR		0x14000000
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void		S32K14x_Eep_Init(void);
static uint32	S32K14x_Eep_Read(uint32 const offset, uint8 *const buf, uint32 const num);
static uint32	S32K14x_Eep_Write(uint32 const offset, uint8 const* buf, uint32 const num);
static boolean	S32K14x_Eep_ISBusy(void);
static void		DEFlashPartition(void);
static void		EEPRom_EN(void);

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

HAL_EEP_HW_API const eep_hw_api =
{
	.init 		= S32K14x_Eep_Init,
	.read  		= S32K14x_Eep_Read,
	.write  	= S32K14x_Eep_Write,
	.is_busy	= S32K14x_Eep_ISBusy,
};


/*****************************************************************************
 *  Global Function Declarations
 *****************************************************************************/
HAL_EEP_HW_API const *S32K14x_Eep_GetApi(void)
{
	return &eep_hw_api;
}



static void S32K14x_Eep_Init(void)
{
	uint8  depart = (uint8)((SIM->FCFG1 & SIM_FCFG1_DEPART_MASK) >> SIM_FCFG1_DEPART_SHIFT);
	
	if((depart != 0x03) && (depart != 0x0b))//already partitioned to 32k dflash and eep backup
	{
		CPU_SR_ALLOC();
		CPU_CRITICAL_ENTER();
		
		DEFlashPartition();
		EEPRom_EN();

		CPU_CRITICAL_EXIT();
	}
	else
	{
	}
}

static uint32 S32K14x_Eep_Read(uint32 const offset, uint8 *const buf, uint32 const num)
{
	uint32 read_cnt = 0;

	if((NULL != buf)&&(offset < EEP_DATA_SZIE)&&(num > 0))
	{
		CPU_SR_ALLOC();
		CPU_CRITICAL_ENTER();

		if ( FTFC->FCNFG & FTFC_FCNFG_EEERDY_MASK )
		{
			uint32 read_addr = EEP_START_ADDR + offset;
			
			read_cnt = num;
			if((read_addr + read_cnt) > (EEP_START_ADDR + EEP_DATA_SZIE))
			{
				read_cnt = (EEP_START_ADDR + EEP_DATA_SZIE) - read_addr;
			}

			for(uint32 i = 0;i < read_cnt;i++)
			{
				buf[i] = ((uint8 *)(EEP_START_ADDR + offset))[i];
			}			
		}
		
		CPU_CRITICAL_EXIT();
	}
	return read_cnt;
}

static uint32 S32K14x_Eep_Write(uint32 const offset, uint8 const* buf, uint32 const num)
{
	uint32 write_cnt = 0;

	if( NULL != buf && offset < EEP_DATA_SZIE && num > 0 )
	{
		CPU_SR_ALLOC();
		CPU_CRITICAL_ENTER();
		if( FTFC->FCNFG & FTFC_FCNFG_EEERDY_MASK )
		{
			uint32 wirte_addr = EEP_START_ADDR + offset;
			
			write_cnt  = num;
			if((wirte_addr + write_cnt) > (EEP_START_ADDR + EEP_DATA_SZIE))
			{
				write_cnt = (EEP_START_ADDR + EEP_DATA_SZIE) - wirte_addr;
			}
			if ( write_cnt > 3 )
			{	// at least 4 byte 
				if ( wirte_addr & 0x01 )
				{
					*(uint8 *)wirte_addr = buf[0];
					write_cnt = 1;
				}
				else if ( wirte_addr & 0x02 )
				{
					*(uint16 *)wirte_addr = ((uint16_t*)((void*)buf))[0];
					write_cnt = 2;
				}
				else
				{
					*(uint32_t *)wirte_addr = ((uint32_t*)((void*)buf))[0];
					write_cnt = 4;
				}			
			}
			else if ( write_cnt > 1 )
			{	// 2 or 3 byte 
				if ( wirte_addr & 0x01 )
				{
					*(uint8 *)wirte_addr = buf[0];
					write_cnt = 1;
				}
				else// if ( wirte_addr & 0x02 )
				{
					*(uint16 *)wirte_addr = ((uint16_t*)((void*)buf))[0];
					write_cnt = 2;
				}
			}
			else if ( write_cnt > 0 )
			{	// one byte 
				*(uint8 *)wirte_addr = buf[0];
				//write_cnt = 1;
			}
			else
			{}
		}
		
		CPU_CRITICAL_EXIT();
	}
	return write_cnt;
}

static boolean	S32K14x_Eep_ISBusy(void)
{
	boolean ret = FALSE;

	if((0 == (FTFC->FSTAT & FTFC_FSTAT_CCIF_MASK)) ||	//FTFC command or emulated EEPROM file system operation in progress
		(0 == (FTFC->FCNFG & FTFC_FCNFG_EEERDY_MASK)))
	{
		ret = TRUE;
	}
	return ret;
}

static void DEFlashPartition(void)
{
	
	//---------------Program Partition CMD set----------------------
	// clear RDCOLERR & ACCERR & FPVIOL flag in flash status register. Write 1 to clear//
	FTFC->FSTAT = FTFC_FSTAT_FPVIOL_MASK | FTFC_FSTAT_ACCERR_MASK | FTFC_FSTAT_RDCOLERR_MASK;
	
	FTFC_FCCOB0 = FLASH_CMD_PROGRAM_PARTITION;	//Program Partition command
	FTFC_FCCOB3 = 0x0;				//FlexRAM loaded with valid EEPROM data during reset sequence
	
	if(0 == EEP_DATA_SZIE)
	{
		FTFC_FCCOB4 = 0x30|EEPROM_0;
	}
	else if(32 == EEP_DATA_SZIE)
	{
		FTFC_FCCOB4 = 0x30|EEPROM_32;
	}
	else if(64 == EEP_DATA_SZIE)
	{
		FTFC_FCCOB4 = 0x30|EEPROM_64;
	}
	else if(128 == EEP_DATA_SZIE)
	{
		FTFC_FCCOB4 = 0x30|EEPROM_128;
	}
	else if(256 == EEP_DATA_SZIE)
	{
		FTFC_FCCOB4 = 0x30|EEPROM_256;
	}
	else if(512 == EEP_DATA_SZIE)
	{
		FTFC_FCCOB4 = 0x30|EEPROM_512;
	}
	else if(1024 == EEP_DATA_SZIE)
	{
		FTFC_FCCOB4 = 0x30|EEPROM_1024;
	}
	else if(2048 == EEP_DATA_SZIE)
	{
		FTFC_FCCOB4 = 0x30|EEPROM_2048;
	}
	else if(4096 == EEP_DATA_SZIE)
	{
		FTFC_FCCOB4 = 0x30|EEPROM_4096;
	}
	else
	{
		FTFC_FCCOB4 = 0x30|EEPROM_0;
	}
	
	if(0 == EEP_BCKUP_SIZE)
	{
		FTFC_FCCOB5 = EFLASH_SIZE_0K;
	}
	else if(32 == EEP_BCKUP_SIZE)
	{
		FTFC_FCCOB5 = EFLASH_SIZE_32K;
	}
	else if(64 == EEP_BCKUP_SIZE)
	{
		FTFC_FCCOB5 = EFLASH_SIZE_64K;
	}
	else
	{
		FTFC_FCCOB5 = EFLASH_SIZE_0K;
	}
	//--------------------------launch command---------------------------------
	FTFC->FSTAT |= FTFC_FSTAT_CCIF_MASK;	//clear CCIF to launch command
	while(0 == (FTFC->FSTAT & FTFC_FSTAT_CCIF_MASK)){;}
}

static void		EEPRom_EN(void)
{
	//------------------------set eeprom enable---------------------------
	// clear RDCOLERR & ACCERR & FPVIOL flag in flash status register. Write 1 to clear//
	FTFC->FSTAT = FTFC_FSTAT_FPVIOL_MASK | FTFC_FSTAT_ACCERR_MASK | FTFC_FSTAT_RDCOLERR_MASK;
	FTFC_FCCOB0 = FLASH_CMD_SET_EERAM;
	FTFC_FCCOB1 = SET_FLEXRAM_EEP;
	FTFC_FCCOB4 = 0x0;
	FTFC_FCCOB5 = 0x0;
	FTFC_FCCOB6 = 0x0;
	FTFC_FCCOB7 = 0x0;
	FTFC_FCCOB8 = 0x0;
	FTFC_FCCOB9 = 0x0;
	//--------------------------launch command---------------------------------
	FTFC->FSTAT |= FTFC_FSTAT_CCIF_MASK;	//clear CCIF to launch command
	while(0 == (FTFC->FSTAT & FTFC_FSTAT_CCIF_MASK)){;}
}

