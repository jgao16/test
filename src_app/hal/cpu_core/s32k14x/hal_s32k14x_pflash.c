/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
//	PFlash module

#include "std_type.h"
#include "std_lib.h"

#include "hal_flash.h"
#include "cpu.h"
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
//Flash command list
#define LK_FTFx_VERIFY_BLOCK               (0x00U)
#define LK_FTFx_VERIFY_SECTION             (0x01U)
#define LK_FTFx_PROGRAM_CHECK              (0x02U)
#define LK_FTFx_READ_RESOURCE              (0x03U)
#define LK_FTFx_PROGRAM_LONGWORD           (0x06U)
#define LK_FTFx_PROGRAM_PHRASE             (0x07U)
#define LK_FTFx_ERASE_BLOCK                (0x08U)
#define LK_FTFx_ERASE_SECTOR               (0x09U)
#define LK_FTFx_PROGRAM_SECTION            (0x0BU)
#define LK_FTFx_VERIFY_ALL_BLOCK           (0x40U)
#define LK_FTFx_READ_ONCE                  (0x41U)
#define LK_FTFx_PROGRAM_ONCE               (0x43U)
#define LK_FTFx_ERASE_ALL_BLOCK            (0x44U)
#define LK_FTFx_SECURITY_BY_PASS           (0x45U)
#define LK_FTFx_PFLASH_SWAP                (0x46U)
#define LK_FTFx_PROGRAM_PARTITION          (0x80U)
#define LK_FTFx_SET_EERAM                  (0x81U)
#define LK_FTFx_ERASE_ALL_BLOCK_UNSECURE   (0x49U)

//FCCOB list
#define LK_FCCOB0          			(FTFC->FCCOB[3])
#define LK_FCCOB1             		(FTFC->FCCOB[2])
#define LK_FCCOB2             		(FTFC->FCCOB[1])
#define LK_FCCOB3             		(FTFC->FCCOB[0])
#define LK_FCCOB4              		(FTFC->FCCOB[7])
#define LK_FCCOB5               	(FTFC->FCCOB[6])
#define LK_FCCOB6               	(FTFC->FCCOB[5])
#define LK_FCCOB7               	(FTFC->FCCOB[4])
#define LK_FCCOB8              		(FTFC->FCCOB[11])
#define LK_FCCOB9             		(FTFC->FCCOB[10])
#define LK_FCCOBA              		(FTFC->FCCOB[9])
#define LK_FCCOBB              		(FTFC->FCCOB[8])

//	PFlash start address
#define LK_PFLASH_START_ADDR		(0x00U)
#define LK_PFLASH_PROG_UNIT_SIZE	(8U)
#define LK_PFLASH_PROG_BASE_ADDR	(0x00U)

//	byte operation
#define LK_GET_BIT_0_7(value)		((uint8)( ((uint32)(value))        & 0xff))
#define LK_GET_BIT_8_15(value)		((uint8)((((uint32)(value)) >> 8 ) & 0xff))
#define LK_GET_BIT_16_23(value)		((uint8)((((uint32)(value)) >> 16) & 0xff))
#define LK_GET_BIT_24_31(value)		((uint8)((((uint32)(value)) >> 24) & 0xff))
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void		S32K14xPFlash_Init             	(void);
static uint32	S32K14xPFlash_GetFlashSize     	(void);
static uint32  	S32K14xPFlash_GetSectorSize    	(void);
static uint32  	S32K14xPFlash_GetPageSize      	(void);
static uint32  	S32K14xPFlash_GetSegmentSize   	(void);
static void    	S32K14xPFlash_EraseEntireFlash 	(void);
static void    	S32K14xPFlash_EraseFlashSector 	(uint32 addr);
static uint32  	S32K14xPFlash_ProgramData      	(uint32 addr, const uint8 *dat, uint32 len);
static uint32  	S32K14xPFlash_ReadData         	(uint32 addr, uint8 *dat, uint32 len);
static void		S32K14xPFlash_EnterPowerDown	(void);
static void		S32K14xPFlash_ExitPowerDown		(void);
static boolean 	S32K14xPFlash_IsBusy           	(void);
static void 	S32K14xPFlash_Sync           	(void);

__ramfunc	static	void	S32K14PFlash_LaunchCMD	(uint8_t bWaitComplete);
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static HAL_FLASH_HW_API const flash_hw_api =
{
	.init               =	S32K14xPFlash_Init,
	.get_flash_size     =	S32K14xPFlash_GetFlashSize,
	.get_sector_size    =	S32K14xPFlash_GetSectorSize,
	.get_page_size      =	S32K14xPFlash_GetPageSize,
	.get_segment_size   =	S32K14xPFlash_GetSegmentSize,
	.erase_entire_flash =	S32K14xPFlash_EraseEntireFlash,
	.erase_flash_sector =	S32K14xPFlash_EraseFlashSector,
	.program_data       =	S32K14xPFlash_ProgramData,
	.read_data          =	S32K14xPFlash_ReadData,
	.enter_power_down	=	S32K14xPFlash_EnterPowerDown,
	.exit_power_down	=	S32K14xPFlash_ExitPowerDown,
	.is_busy            =	S32K14xPFlash_IsBusy,
	.sync				=	S32K14xPFlash_Sync,
};

static	boolean	su1PFChkFlag	=	FALSE;

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
HAL_FLASH_HW_API const *S32K14x_PFlash_GetPFlashHwApi(void)
{
	return &flash_hw_api;
}
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void    S32K14xPFlash_Init(void)
{
	CPU_SR_ALLOC();						//	configure PFlash
	CPU_CRITICAL_ENTER();

	uint32	u4_tmp	=	(MSCM->OCMDR[0] & MSCM_OCMDR_OCMT_MASK) >> MSCM_OCMDR_OCMT_SHIFT;
	uint32	u4_size	=	S32K14xPFlash_GetFlashSize();
	if(((0x80000	==	u4_size)	||
		(0x100000	==	u4_size)	||
		(0x200000	==	u4_size)	||
		(0x40000	==	u4_size))
	&&	(0x04	==	u4_tmp))
	{
		su1PFChkFlag	=	TRUE;		//	PFlash size check pass
	}
	else{
		su1PFChkFlag	=	FALSE;		//	PFlash size check failure
	}
	
	/*
	The flash controller needs to be idle when writing to an OCMDRn register associated with Flash memory.
	Changing controller configuration while active can cause undesired results
	*/
	//MSCM->OCMDR[0]	&=	0xFFFEFFFF;		//	RO 			= 0, 	enable write to OCMDR0[11:0]
	//MSCM->OCMDR[0]	|=	0x00000030;		//	OCMDR0[5:4] = 3,	disable program flash speculation

	CPU_CRITICAL_EXIT();
}
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static uint32  S32K14xPFlash_GetFlashSize(void)
{
	uint32	size = 0U;

	uint32	ocmsz	=	(MSCM->OCMDR[0] & MSCM_OCMDR_OCMSZ_MASK) >> MSCM_OCMDR_OCMSZ_SHIFT;
	if(0x0A == ocmsz){
		size	=	0x80000;		//	512K
	}
	else if(0x0B == ocmsz){
		size	=	0x100000;		//	1M
	}
	else if(0x0C == ocmsz){
		size	=	0x200000;		//	2M
	}
	else if(0x09 == ocmsz){
		size	=	0x40000;		//	256K
	}
	else if(0x08 == ocmsz){
		size	=	0x20000;		//	128K
	}
	else if(0x07 == ocmsz){
		size	=	0x10000;		//	64K
	}
	else if(0x06 == ocmsz){
		size	=	0x8000;			//	32K
	}
	else if(0x05 == ocmsz){
		size	=	0x4000;			//	16K
	}
	else if(0x04 == ocmsz){
		size	=	0x2000;			//	8K
	}
	else if(0x03 == ocmsz){
		size	=	0x1000;			//	4K
	}
	else if(0x02 == ocmsz){
		size	=	0x800;			//	2K
	}
	else if(0x01 == ocmsz){
		size	=	0x400;			//	1K
	}
	else if(0x0D == ocmsz){
		size	=	0x400000;		//	4M
	}
	else if(0x0E == ocmsz){
		size	=	0x800000;		//	8M
	}
	else if(0x0F == ocmsz){
		size	=	0x1000000;		//	16M
	}
	else{}

	return(size);
}
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static uint32  S32K14xPFlash_GetSectorSize(void)
{
	return(0x1000);	//	4K
}
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static uint32  S32K14xPFlash_GetPageSize(void)
{
	return(0x1000);	//	4K
}
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static uint32  S32K14xPFlash_GetSegmentSize(void)
{
	return(0x1000);	//	4K
}
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void    S32K14xPFlash_EraseEntireFlash(void)
{
	//	not supported
}
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void    S32K14xPFlash_EraseFlashSector(uint32 addr)
{
	CPU_SR_ALLOC();
	CPU_CRITICAL_ENTER();

	uint32	max_size	=	S32K14xPFlash_GetFlashSize();

	while( S32K14xPFlash_IsBusy() )
	{}
#if	LK_PFLASH_START_ADDR == 0U
	if(	(!S32K14xPFlash_IsBusy()) && (su1PFChkFlag == TRUE) && (addr < max_size))
#else
	if(	(!S32K14xPFlash_IsBusy())	&&	(su1PFChkFlag == TRUE)
	&&	(addr	>=	LK_PFLASH_START_ADDR)
	&&	(addr	<	(LK_PFLASH_START_ADDR + max_size)))
#endif
	{
		//	sector size
		uint32	sector_size		=	S32K14xPFlash_GetSectorSize();

		//	in which sector
		uint32	sector_index;
#if	LK_PFLASH_START_ADDR == 0U
		sector_index	=	addr / sector_size;
#else
		sector_index	=	(addr - LK_PFLASH_START_ADDR) / sector_size;
#endif
		//	start address write to FCCOB
		uint32	erase_addr;
#if	LK_PFLASH_PROG_BASE_ADDR == 0U
		erase_addr	=	sector_index * sector_size;
#else
		erase_addr	=	sector_index * sector_size + LK_PFLASH_PROG_BASE_ADDR;
#endif
		//	clear error flags
		FTFC->FSTAT	= (	FTFC_FSTAT_FPVIOL_MASK
					|	FTFC_FSTAT_ACCERR_MASK
					|	FTFC_FSTAT_RDCOLERR_MASK);

		LK_FCCOB0	=	LK_FTFx_ERASE_SECTOR;
		LK_FCCOB1	=	LK_GET_BIT_16_23(erase_addr);
		LK_FCCOB2	=	LK_GET_BIT_8_15 (erase_addr);
		LK_FCCOB3	=	LK_GET_BIT_0_7  (erase_addr);

		//	clear CCIF to launch command
		S32K14PFlash_LaunchCMD(1);
	}

	CPU_CRITICAL_EXIT();
}
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static uint32  S32K14xPFlash_ProgramData(uint32 addr, const uint8 *dat, uint32 len)
{
	uint32	wr_len = 0U;	//	length for programmed

	(void)	dat;

	CPU_SR_ALLOC();
	CPU_CRITICAL_ENTER();

	while( S32K14xPFlash_IsBusy() )
	{}
	
	uint32	max_size	=	S32K14xPFlash_GetFlashSize();
#if	LK_PFLASH_START_ADDR == 0U
	if(	(!S32K14xPFlash_IsBusy())	&&	(NULL	!=	dat)	&&	(su1PFChkFlag == TRUE)
	&&	(addr	<	max_size)
	&&	(!(addr	%	LK_PFLASH_PROG_UNIT_SIZE))	//	64-bit aligned
	&&	(len	>=	LK_PFLASH_PROG_UNIT_SIZE))	//	at least 8 bytes
#else
	if(	(!S32K14xPFlash_IsBusy())	&&	(NULL	!=	dat)	&&	(su1PFChkFlag == TRUE)
	&&	(addr	>=	LK_PFLASH_START_ADDR)
	&&	(addr	<	(LK_PFLASH_START_ADDR + max_size))
	&&	(!(addr	%	LK_PFLASH_PROG_UNIT_SIZE))	//	64-bit aligned
	&&	(len	>=	LK_PFLASH_PROG_UNIT_SIZE))	//	at least 8 bytes
#endif
	{
		uint32	pflash_prog_addr;

#if	(LK_PFLASH_START_ADDR == 0U) && (LK_PFLASH_PROG_BASE_ADDR == 0U)
		pflash_prog_addr	=	addr;
#elif	LK_PFLASH_START_ADDR == 0U
		pflash_prog_addr	=	addr + LK_PFLASH_PROG_BASE_ADDR;
#elif	LK_PFLASH_PROG_BASE_ADDR == 0U
		pflash_prog_addr	=	addr - LK_PFLASH_START_ADDR;
#else
		pflash_prog_addr	=	addr - LK_PFLASH_START_ADDR + LK_PFLASH_PROG_BASE_ADDR;
#endif
		//	clear error flags
		FTFC->FSTAT = (	FTFC_FSTAT_FPVIOL_MASK
					|	FTFC_FSTAT_ACCERR_MASK
					|	FTFC_FSTAT_RDCOLERR_MASK);

		LK_FCCOB0	=	LK_FTFx_PROGRAM_PHRASE; //	program 8 bytes per operation
		LK_FCCOB1	=	LK_GET_BIT_16_23(pflash_prog_addr);
		LK_FCCOB2	=	LK_GET_BIT_8_15	(pflash_prog_addr);
		LK_FCCOB3	=	LK_GET_BIT_0_7	(pflash_prog_addr);

		for(uint32 i = 0U; i < LK_PFLASH_PROG_UNIT_SIZE; i += 1U)
		{
			FTFC->FCCOB[i + 4] = dat[i];
		}

		//	clear CCIF to launch command
		S32K14PFlash_LaunchCMD(1);

		wr_len	=	LK_PFLASH_PROG_UNIT_SIZE;
	}

	CPU_CRITICAL_EXIT();

	return(wr_len);
}
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static uint32  S32K14xPFlash_ReadData(uint32 addr, uint8 *dat, uint32 len)
{
	uint32	rd_len = 0U;	//	length for read

	CPU_SR_ALLOC();
	CPU_CRITICAL_ENTER();
	
	__DSB();

	uint32	max_size	=	S32K14xPFlash_GetFlashSize();
#if	LK_PFLASH_START_ADDR == 0U
	if(	(!S32K14xPFlash_IsBusy()) && (NULL != dat)	&&	(su1PFChkFlag == TRUE)
	&&	(addr	<	max_size)
	&&	(len	>	0U)	&&	(len	<=	max_size))
#else
	if(	(!S32K14xPFlash_IsBusy()) && (NULL != dat)	&&	(su1PFChkFlag == TRUE)
	&&	(addr	>=	LK_PFLASH_START_ADDR)
	&&	(addr	<	(LK_PFLASH_START_ADDR + max_size))
	&&	(len > 0U)	&&	(len <= max_size))
#endif
	{
#if	LK_PFLASH_START_ADDR == 0U
		if((addr + len) <= max_size){
			rd_len	=	len;
		}
		else{
			rd_len	=	max_size - addr;
		}
#else
		if((addr + len) <= (LK_PFLASH_START_ADDR + max_size)){
			rd_len	=	len;
		}
		else{
			rd_len	=	LK_PFLASH_START_ADDR + max_size - addr;
		}
#endif
		uint8_t volatile * pu1 = (uint8 volatile *) addr;
		for(uint32 i = 0U; i < rd_len; i += 1U)
		{
			dat[i] = pu1[i];
		}
	}

	CPU_CRITICAL_EXIT();

	return(rd_len);
}
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static boolean S32K14xPFlash_IsBusy(void)
{
	boolean is_busy = FALSE;	//	indicator PFlash current busy or not,TRUE is busy now.

	if(0x00 == (FTFC->FSTAT & FTFC_FSTAT_CCIF_MASK))
	{
		is_busy = TRUE;
	}

	return is_busy;
}
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void S32K14xPFlash_EnterPowerDown(void)
{
	//	not supported
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void S32K14xPFlash_ExitPowerDown(void)
{
	//	not supported
}

__ramfunc	static	void S32K14PFlash_LaunchCMD(uint8_t bWaitComplete)
{
	FTFC->FSTAT	=	FTFC_FSTAT_CCIF_MASK;	//	clear CCIF to launch command
	if(bWaitComplete)
	{
		//	wait till command is completed
		while(!(FTFC->FSTAT & FTFC_FSTAT_CCIF_MASK)){;}
	}
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void S32K14xPFlash_Sync (void)
{
	__DSB();
}


/** >>>>>>>>>>end of file>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


