/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
//adc moudle
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#include "std_lib.h"

#include "hal_adc.h"
#include "hal_gpio.h"
#include "cpu.h"
#include "hal_os.h"

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#define ADC_HW_ATTR_SIZE                                        (sizeof(adc_hw_attr) / sizeof(ADC_HW_CFG))

#define ADC_CFG1_ADIV_SEL                                       (2u)        //div=4,4MHz
#define ADC_CFG1_MODE_SEL                                       (1u)        //12-bit conversion
#define ADC_CFG2_SMPLTS_SEL                                     (0x0f)      //sample time=16 ADCK=4us
#define ADC_SC3_AVGS_SEL                                        (0x3)       //32 times hw samples

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
typedef struct
{
    ADC_Type    * obj;                 //ADC moudle
    unsigned char adc_ch;              //ADC channel number
    HAL_PORT_NAME port_name;           //ADC port name
    unsigned char pin_num;             //ADC pin number
}ADC_HW_CFG;

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static ADC_HW_CFG const adc_hw_attr[] = 
{
#undef  CPU_ADC_HW_DEF
#define CPU_ADC_HW_DEF(hw_name, hw_obj, adc_ch, port_name, pin_num) \
	                           {hw_obj, adc_ch, port_name, pin_num},
    #include "cpu_adc.cfg"
#undef  CPU_ADC_HW_DEF
};

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static uint16_t adc_ch_val[ADC_HW_ATTR_SIZE]     = {0u};    //adc conversion value
static boolean  adc_ch_avbl[ADC_HW_ATTR_SIZE]    = {FALSE}; //adc channel enable
static boolean  adc_ch_in_conv[ADC_HW_ATTR_SIZE] = {FALSE}; //adc channel in conversion or not.
static uint8    adc_ch_index                     = 0u;      //current adc channel               

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void HalS32K14x_AdcChInit    (uint8 hw_ch);
static void HalS32K14x_AdcChDeInit  (uint8 hw_ch);
static void HalS32K14x_AdcFunc1ms   (void);
static void HalS32K14x_AdcGetChVal  (uint8 hw_ch, uint16_t *val);

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
HAL_ADC_HW_API const adc_hw_api =
{
    .ch_init        = HalS32K14x_AdcChInit,
    .ch_deinit      = HalS32K14x_AdcChDeInit,
    .cycle_convert  = HalS32K14x_AdcFunc1ms,
    .get_ch_val     = HalS32K14x_AdcGetChVal,
};

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
HAL_ADC_HW_API const *S32K14x_Adc_GetApi(void)
{
	return &adc_hw_api;
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void HalS32K14x_AdcHwInit(void)
{
    //Init ADC0
    __IO uint32_t * pcc = PCC->PCCn + PCC_ADC0_INDEX;

    if ((NULL != pcc) && ((*pcc) & PCC_PCCn_PR_MASK))   //PCC CLOCK OFF
    {
        *pcc &= ~PCC_PCCn_CGC_MASK;
        *pcc &= ~PCC_PCCn_PCS_MASK;                    	//clear PCS
        *pcc |= PCC_PCCn_PCS(1);                        //select SOSCDIV2, 16MHz
        *pcc |= PCC_PCCn_CGC_MASK;                      //enable ADC0 clock

        /*auto-calibration start*/
        ADC0->CLPS = 0u;
        ADC0->CLP3 = 0u;
        ADC0->CLP2 = 0u;
        ADC0->CLP1 = 0u;
        ADC0->CLP0 = 0u;
        ADC0->CLPX = 0u;
        ADC0->CLP9 = 0u;
        ADC0->SC3  = ADC_SC3_CAL_MASK | ADC_SC3_AVGE_MASK | ADC_SC3_AVGS_SEL;
        uint8_t cnt = 0u;
        while((ADC0->SC3 & ADC_SC3_CAL_MASK) && (cnt++ < 2))  /* Wait until CAL flag cleared at end of calibration, max wait 2ms */
        {
            HalOS_Delay(1);
        }
        ADC0->SC3  &= ~ADC_SC3_CAL_MASK;
        /*auto-calibration stop*/

        ADC0->CFG1 = (ADC_CFG1_MODE_SEL << ADC_CFG1_MODE_SHIFT) | (ADC_CFG1_ADIV_SEL << ADC_CFG1_ADIV_SHIFT); //DIV=4 for ADCK,mode=12-bit,ADCLK=ADC_ALTCLK1
        ADC0->CFG2 = ADC_CFG2_SMPLTS_SEL;                                                                     //sample times=16 ADCK=4 us
        ADC0->SC2 &= ~ADC_SC2_ADTRG_MASK;                                                                     //SOFTWARE trigger,voltage ref is VREFH an VREFL
        ADC0->SC3 |= ADC_SC3_ADCO_MASK;                                                                       //Continuous conversion,hw average enable,32 samples averaged
    }

    //Init ADC1
    pcc = PCC->PCCn + PCC_ADC1_INDEX;

    if ((NULL != pcc) && ((*pcc) & PCC_PCCn_PR_MASK))   //PCC CLOCK OFF
    {
        *pcc &= ~PCC_PCCn_CGC_MASK;
        *pcc &= ~PCC_PCCn_PCS_MASK;                    	//clear PCS
        *pcc |= PCC_PCCn_PCS(1);                        //select SOSCDIV2, 16MHz
        *pcc |= PCC_PCCn_CGC_MASK;                      //enable ADC1 clock

        /*auto-calibration start*/
        ADC1->CLPS = 0u;
        ADC1->CLP3 = 0u;
        ADC1->CLP2 = 0u;
        ADC1->CLP1 = 0u;
        ADC1->CLP0 = 0u;
        ADC1->CLPX = 0u;
        ADC1->CLP9 = 0u;
        ADC1->SC3  = ADC_SC3_CAL_MASK | ADC_SC3_AVGE_MASK | ADC_SC3_AVGS_SEL;
        uint8_t cnt = 0u;
        while((ADC1->SC3 & ADC_SC3_CAL_MASK) && (cnt++ < 2))  /* Wait until CAL flag cleared at end of calibration, max wait 2ms */
        {
            HalOS_Delay(1);
        }
        ADC1->SC3  &= ~ADC_SC3_CAL_MASK;
        /*auto-calibration stop*/

        ADC1->CFG1 = (ADC_CFG1_MODE_SEL << ADC_CFG1_MODE_SHIFT) | (ADC_CFG1_ADIV_SEL << ADC_CFG1_ADIV_SHIFT); //DIV=4 for ADCK,mode=12-bit,ADCLK=ADC_ALTCLK1
        ADC1->CFG2 = ADC_CFG2_SMPLTS_SEL;                                                                     //sample times=16 ADCK=4 us
        ADC1->SC2 &= ~ADC_SC2_ADTRG_MASK;                                                                     //SOFTWARE trigger,voltage ref is VREFH an VREFL
        ADC1->SC3 |= ADC_SC3_ADCO_MASK;                                                                       //Continuous conversion,hw average enable,32 samples averaged
    }
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void HalS32K14x_AdcChInit(uint8 hw_ch)
{
    if (hw_ch < ADC_HW_ATTR_SIZE)
    {
        CPU_SR_ALLOC();
        CPU_CRITICAL_ENTER();

        ADC_HW_CFG const *cfg = adc_hw_attr + hw_ch;
        if ((NULL != cfg) && (NULL != cfg->obj) && (!adc_ch_avbl[hw_ch]))
        {
            //port init for used adc channel
            HalGpio_InitPinGpio_Def(cfg->port_name, cfg->pin_num, PORT_AN, OUTPUT_OPEN_DRAIN, INPUT_NO_PULL);

            adc_ch_avbl[hw_ch] = TRUE;
        }

        CPU_CRITICAL_EXIT();
    }
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void HalS32K14x_AdcChDeInit(uint8 hw_ch)
{
    if (hw_ch < ADC_HW_ATTR_SIZE)
    {
        CPU_SR_ALLOC();
        CPU_CRITICAL_ENTER();

        ADC_HW_CFG const *cfg = adc_hw_attr + hw_ch;
        if ((NULL != cfg) && (NULL != cfg->obj) && adc_ch_avbl[hw_ch])
        {
            adc_ch_avbl[hw_ch]    = FALSE;
            adc_ch_val[hw_ch]     = 0u;
            adc_ch_in_conv[hw_ch] = FALSE;
            cfg->obj->SC1[0]      = ADC_SC1_ADCH_MASK;  //disable current channel
        }

        CPU_CRITICAL_EXIT();
    }
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void HalS32K14x_AdcFunc1ms(void)
{
    //adc conversion time=(((SMPLTS+1)+1+28+5)ADC clocks + 5 bus clocks) * AVGS=about 413us
    boolean next_ch_avbl = FALSE;

    if (adc_ch_avbl[adc_ch_index])
    {
        ADC_HW_CFG const *cfg  = adc_hw_attr + adc_ch_index;
        if ((NULL != cfg) && (NULL != cfg->obj))
        {
            if (adc_ch_in_conv[adc_ch_index])
            {
                adc_ch_val[adc_ch_index]     = (uint16_t)(cfg->obj->R[0]); //get result
                adc_ch_in_conv[adc_ch_index] = FALSE;
                next_ch_avbl                 = TRUE;
            }
            else
            {
                cfg->obj->SC1[0]             = cfg->adc_ch;              //trigger ADC conversion for current channel
                adc_ch_in_conv[adc_ch_index] = TRUE;
            }
        }
    }
    else
    {
        next_ch_avbl = TRUE;
    }

    if (next_ch_avbl)
    {
        adc_ch_index++;
        if (adc_ch_index >= ADC_HW_ATTR_SIZE)
        {
            adc_ch_index = 0u;
        }
    }
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void  HalS32K14x_AdcGetChVal(uint8 hw_ch, uint16_t *val)
{
    if ((hw_ch < ADC_HW_ATTR_SIZE) && (NULL != val))
    {
        CPU_SR_ALLOC();
        CPU_CRITICAL_ENTER();
        *val = adc_ch_val[hw_ch];
        CPU_CRITICAL_EXIT();
    }
}

