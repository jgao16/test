/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    s32k ADC driver api
    All rights reserved
>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#ifndef __HAL_S32K14X_ADC_H__
#define __HAL_S32K14X_ADC_H__

#ifdef  __cplusplus
extern "C"
{
#endif

/*****************************************************************************
 *  Include Files
 *****************************************************************************/
#include "std_type.h"


/*****************************************************************************
 *  Global Function Declarations
 *****************************************************************************/
void                    HalS32K14x_AdcHwInit    (void);
HAL_ADC_HW_API const *  S32K14x_Adc_GetApi      (void);

#ifdef  __cplusplus
}
#endif

#endif
