/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#include "std_type.h"
#include "std_lib.h"

#include "hal_uart.h"
#include "hal_gpio.h"
#include "hal_s32k14x_scg.h"
#include "hal_hw.h"
#include "hal_int.h"
#include "cpu.h"



/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#define HW_UART_NUM                             sizeof(uart_hw_attr)/sizeof(UART_HW_ATTR)

#define LPUART_CLR_ALL_INTERRUPT_FLAGS          (0xc01fc000)
#define LPUART_DIS_ALL_INTERRUPT                (0x00000040)

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
typedef void(*UART_INT_ISR)(void); 


typedef struct
{
	uint8				uart_name;
	LPUART_Type *obj;

	HAL_PORT_NAME    	rx_port;
	unsigned char      	rx_pin;
	HAL_PORT_NAME    	tx_port;
	unsigned char     	tx_pin;

	
	uint8				rx_evt_num;
	uint8			    err_int_num;
}UART_HW_ATTR;

typedef struct
{
	uint16 			baud:		4;		//HAL_UART_BAUD
	uint16 			parityChk:	2;		//HAL_UART_PARITY_CHECK
	uint16 			wordLens:	2;		//HAL_UART_WORD_LENS
	uint16 			stopBits:	2;		//HAL_UART_STOP_BITS

	uint16 						user_baud;			//if baud = HAL_UART_BAUD_USER_DEF
	
	void * arg;
	void (*rx_clbak) (void*arg,uint8 *rx_dat, uint16 len);
	void (*tx_clbak) (void*arg);
	void (*err_clbak)(void*arg);
	void (*cyc_clbak)(void*arg);
	void (*wakeup_clbak)(void*arg);
	
}UART_HW_DEV;

typedef struct
{
	uint8  const * 	dat;
	uint16 			len;
	uint16 			tx_cnt;
}UART_TX_DAT;

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#define CPU_UART_HW_DEF(name,com_attr,hw_dev,rx_port,rx_pin,tx_port,tx_pin, rx_int_num, tx_int_num, err_int_num, dma_dev, dam_int_num) \
					  {name##_ID,hw_dev,rx_port,rx_pin,tx_port,tx_pin, rx_int_num,err_int_num},

static const UART_HW_ATTR uart_hw_attr[] = 
{
    #include "cpu_uart.cfg"
};
#undef  CPU_UART_HW_DEF

static const uint32 uart_baud_tbl[HAL_UART_BAUD_MAX] = 
	{600,1200,2400,4800,9600,19200,38400,57600,115200,230400,1000000,0};




static UART_TX_DAT				uart_tx_data[HW_UART_NUM];
static UART_HW_DEV				uart_dev[HW_UART_NUM];

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void *	S32K14x_UART_GetDev(uint8 uart_name);

static void		S32K14x_UART_Init(void const * hdl);
static void		S32K14x_UART_Para_Set(void *hdl,HAL_UART_BAUD baud,HAL_UART_PARITY_CHECK parityChk,HAL_UART_WORD_LENS wordLens,HAL_UART_STOP_BITS stopBits);
static void		S32K14x_UART_DeInit(void const * hdl);
static void		S32K14x_UART_Handle(uint8 uart_name);
static boolean	S32K14x_UART_RxProc(void const * hdl);
static void		S32K14x_UART_Send(void const*hdl, uint8 const* tx_dat,uint16 const len);
static uint16	S32K14x_UART_GetBand(void * hdl);
static void		S32K14x_UART_SetPreDefBand(void *hdl,HAL_UART_BAUD baud);
static void		S32K14x_UART_SetUserBand(void *hdl,uint16 baud);
static void 	S32K14x_Uart_SetCalBk (	void *hdl,
											void * arg,
											void (*rx_clbak) (void*arg,unsigned char *rx_dat, unsigned short len),
											void (*tx_clbak) (void*arg),
											void (*err_clbak)(void*arg),
											void (*cyc_clbak)(void*arg),
											void (*wakeup_clbak)(void*arg) 
											);


#define CPU_UART_HW_DEF(name,com_attr,hw_dev,rx_port,rx_pin,tx_port,tx_pin, rx_int_num, tx_int_num, err_int_num, dma_dev, dam_int_num) \
	static void UART_Int_##name(void)	{S32K14x_UART_Handle(name##_ID);}
	#include "cpu_uart.cfg"
#undef  CPU_UART_HW_DEF





static UART_INT_ISR const uart_int_isr[] = 
{
#define CPU_UART_HW_DEF(name,com_attr,hw_dev,rx_port,rx_pin,tx_port,tx_pin, rx_int_num, tx_int_num, err_int_num, dma_dev, dam_int_num) \
	UART_Int_##name,	
	#include "cpu_uart.cfg"
#undef  CPU_UART_HW_DEF
};

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
HAL_UART_HW_API  const uart_hw_api =
{
	.cyc_call		=	NULL,
	.get_dev 		= 	S32K14x_UART_GetDev,
	.init_dev 		= 	S32K14x_UART_Init,
	.deinit_dev 	= 	S32K14x_UART_DeInit,
	.send 			= 	S32K14x_UART_Send,
	.is_rx_process 	= 	S32K14x_UART_RxProc,
	.set_parameter 	= 	S32K14x_UART_Para_Set,
	.set_predef_baud=	S32K14x_UART_SetPreDefBand,
	.set_user_baud	=	S32K14x_UART_SetUserBand,
	.set_calbk		=	S32K14x_Uart_SetCalBk,
	.get_baud		=	S32K14x_UART_GetBand,
};

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
HAL_UART_HW_API const *S32K14x_UART_GetUARTHwApi(void)
{
	return &uart_hw_api;
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void *	S32K14x_UART_GetDev(uint8 uart_name)
{
	UART_HW_ATTR const *dev = NULL;
	if ( uart_name < HAL_UART_HW_NAME_MAX )
	{
		dev = uart_hw_attr + uart_name;
	}
	return (void *)dev;
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

static void S32K14x_UART_Init(void const * hdl)
{
	if ( hdl != NULL )
	{	
		UART_HW_ATTR const *hw = (UART_HW_ATTR*)hdl;
		__IO uint32_t     *pcc = NULL;
		//enable uart clock
		if(LPUART0 == hw->obj)
		{
			pcc = PCC->PCCn + PCC_LPUART0_INDEX;
		}
		else if(LPUART1 == hw->obj)
		{
			pcc = PCC->PCCn + PCC_LPUART1_INDEX;
		}
		else if(LPUART2 == hw->obj)
		{
			pcc = PCC->PCCn + PCC_LPUART2_INDEX;
		}
		else
		{}

		if ( pcc != NULL && (*pcc&PCC_PCCn_PR_MASK) )
		{	
			*pcc &= ~PCC_PCCn_CGC_MASK;
			*pcc &= ~PCC_PCCn_PCS_MASK; 					//clear PCS
			*pcc |= PCC_PCCn_PCS(6);						//select SPLL
			*pcc |= PCC_PCCn_CGC_MASK;						//enable uart clock


			HalGpio_InitPinGpio_Def(hw->rx_port,hw->rx_pin,	PORT_AF,OUTPUT_PUSH_PULL,INPUT_PULL_UP);
			HalGpio_InitPinGpio_Def(hw->tx_port,hw->tx_pin,	PORT_AF,OUTPUT_PUSH_PULL,INPUT_NO_PULL);

			if(LPUART0 == hw->obj)
			{
				if(HAL_GPIO_PT_B == hw->tx_port)//b0,b1
				{
					HalGpio_PinAFConfig(hw->rx_port,hw->rx_pin,	2);
					HalGpio_PinAFConfig(hw->tx_port,hw->tx_pin,	2);
				}
				else if(HAL_GPIO_PT_C== hw->tx_port)//c2,c3
				{
					HalGpio_PinAFConfig(hw->tx_port,hw->rx_pin,	4);
					HalGpio_PinAFConfig(hw->tx_port,hw->tx_pin,	4);
				}
				else if(HAL_GPIO_PT_A== hw->rx_port)
				{
					if(2 == hw->rx_pin)//a2,a3
					{
						HalGpio_PinAFConfig(hw->tx_port,hw->rx_pin,6);
						HalGpio_PinAFConfig(hw->tx_port,hw->tx_pin,6);
					}
					else if(28 == hw->rx_pin)//a27,a28
					{
						HalGpio_PinAFConfig(hw->tx_port,hw->rx_pin,4);
						HalGpio_PinAFConfig(hw->tx_port,hw->tx_pin,4);
					}
				}
					
			}
			else if(LPUART1 == hw->obj)
			{
				if(HAL_GPIO_PT_D== hw->tx_port)//d13,d14
				{
					HalGpio_PinAFConfig(hw->rx_port,hw->rx_pin,	3);
					HalGpio_PinAFConfig(hw->tx_port,hw->tx_pin,	3);
				}
				else if(HAL_GPIO_PT_C== hw->tx_port)//c8,c9;c6,c7
				{
					HalGpio_PinAFConfig(hw->tx_port,hw->rx_pin,	2);
					HalGpio_PinAFConfig(hw->tx_port,hw->tx_pin,	2);
				}

				hw->obj->CTRL |= LPUART_CTRL_TXINV_MASK; //ONLY FOR this device.
			}
			else if(LPUART2 == hw->obj)
			{
				if((HAL_GPIO_PT_A== hw->tx_port) ||//a8,a9
					(HAL_GPIO_PT_D== hw->tx_port))//d6,d7
				{
					HalGpio_PinAFConfig(hw->rx_port,hw->rx_pin,	2);
					HalGpio_PinAFConfig(hw->tx_port,hw->tx_pin,	2);
				}
				else if(HAL_GPIO_PT_E== hw->tx_port)//d17,e12
				{
					HalGpio_PinAFConfig(hw->tx_port,hw->rx_pin,	3);
					HalGpio_PinAFConfig(hw->tx_port,hw->tx_pin,	3);
				}
			}
			else
			{}

			//disable rx & tx before parameter set
			hw->obj->CTRL &= ~(LPUART_CTRL_RE_MASK+LPUART_CTRL_TE_MASK);
			//hw->obj->CTRL &= ~LPUART_CTRL_TE_MASK;
			//parity set
			if(HAL_UART_PARITY_NONE < uart_dev[hw->uart_name].parityChk)
			{
				hw->obj->CTRL |= LPUART_CTRL_PE_MASK;	//parit enable
				if(HAL_UART_PARITY_ODD == uart_dev[hw->uart_name].parityChk)
				{
					hw->obj->CTRL |= LPUART_CTRL_PT_MASK;	//odd parit
				}
				else// if(HAL_UART_PARITY_EVEN == uart_dev[hw->uart_name].parityChk)
				{
					hw->obj->CTRL &= ~LPUART_CTRL_PT_MASK;	//even parit
				}
			}
			else
			{
				hw->obj->CTRL &= ~LPUART_CTRL_PE_MASK;	//parit disable
			}
			
			//stop bit set
			if(HAL_UART_STOP_1_BIT == uart_dev[hw->uart_name].stopBits)
			{
				hw->obj->BAUD &= ~LPUART_BAUD_SBNS_MASK;
			}
			else //if(HAL_UART_STOP_2_BITS == uart_dev[hw->uart_name].stopBits)
			{
				hw->obj->BAUD |= LPUART_BAUD_SBNS_MASK;
			}
			//else
			//{}
			
			
			//word length set
			if(HAL_UART_WORD_7_BITS == uart_dev[hw->uart_name].wordLens)
			{
				hw->obj->CTRL |= LPUART_CTRL_M7_MASK;	//bit M7 set
			}
			else
			{
				hw->obj->CTRL &= ~LPUART_CTRL_M7_MASK;
				if(HAL_UART_WORD_8_BITS == uart_dev[hw->uart_name].wordLens)
				{
					hw->obj->CTRL &= ~LPUART_CTRL_M_MASK;
				}
				else if(HAL_UART_WORD_9_BITS == uart_dev[hw->uart_name].wordLens)
				{
					hw->obj->CTRL |= LPUART_CTRL_M_MASK;
				}
			}
			
			//baud rate set
			uint16 sbr, sbrTemp, i;
			uint32 osr, tempDiff, calculatedBaud, baudDiff,sourceClockInHz,desiredBaudRate;
			
			sourceClockInHz = HalS32K14x_ScgGetFreq(SPLLDIV2_CLK);

			if(uart_dev[hw->uart_name].baud < HAL_UART_BAUD_MAX)
			{
				desiredBaudRate = uart_baud_tbl[uart_dev[hw->uart_name].baud];
			}
			else
			{
				desiredBaudRate = uart_baud_tbl[HAL_UART_BAUD_115200];
			}

			if(0 == desiredBaudRate)
			{
				if(0 != uart_dev[hw->uart_name].user_baud)
				{
					desiredBaudRate = uart_baud_tbl[uart_dev[hw->uart_name].user_baud] * 100;
				}
				else
				{
					desiredBaudRate = uart_baud_tbl[HAL_UART_BAUD_115200];
				}
			}
			/* This lpuart instantiation uses a slightly different baud rate calculation
			 * The idea is to use the best OSR (over-sampling rate) possible
			 * Note, osr is typically hard-set to 16 in other lpuart instantiations
			 * First calculate the baud rate using the minimum OSR possible (4) */
			osr = 4;	//4x oversampling ratio
			sbr = (uint16)(sourceClockInHz/(desiredBaudRate * osr));
			calculatedBaud = (sourceClockInHz / (osr * sbr));
			
			if (calculatedBaud > desiredBaudRate)
			{
				baudDiff = calculatedBaud - desiredBaudRate;
			}
			else
			{
				baudDiff = desiredBaudRate - calculatedBaud;
			}
			
			/* loop to find the best osr value possible, one that generates minimum baudDiff
			 * iterate through the rest of the supported values of osr */
			for (i = 5; i <= 32; i++)
			{
				/* calculate the temporary sbr value   */
				sbrTemp = (uint16)(sourceClockInHz/(desiredBaudRate * i));
				/* calculate the baud rate based on the temporary osr and sbr values */
				calculatedBaud = (sourceClockInHz / (i * sbrTemp));
			
				if (calculatedBaud > desiredBaudRate)
				{
					tempDiff = calculatedBaud - desiredBaudRate;
				}
				else
				{
					tempDiff = desiredBaudRate - calculatedBaud;
				}
			
				if (tempDiff <= baudDiff)
				{
					baudDiff = tempDiff;
					osr = i;  /* update and store the best osr value calculated */
					sbr = sbrTemp;	/* update store the best sbr value calculated */
				}
			}
			
			/* Check to see if actual baud rate is within 3% of desired baud rate
			 * based on the best calculate osr value */
			if (baudDiff < ((desiredBaudRate / 100) * 3))
			{
				/* Acceptable baud rate, check if osr is between 4x and 7x oversampling.
				 * If so, then "BOTHEDGE" sampling must be turned on */
				if ((osr > 3) && (osr < 8))
				{
					//LPUART_BWR_BAUD_BOTHEDGE(base, 1);
					 hw->obj->BAUD |=1 <<LPUART_BAUD_BOTHEDGE_SHIFT;
				}
			
				/* program the osr value (bit value is one less than actual value) */
				//LPUART_BAUD_OSR(hw->obj, (osr-1));
				uint32 tmp;
				hw->obj->BAUD |= 0x1F << LPUART_BAUD_OSR_SHIFT;;//set all"1" 0b11111
				tmp = (~LPUART_BAUD_OSR_MASK) |((osr-1) << LPUART_BAUD_OSR_SHIFT);
				hw->obj->BAUD &= tmp;
				/* write the sbr value to the BAUD registers */
				//LPUART_BWR_BAUD_SBR(base, sbr);
				hw->obj->BAUD &= ~LPUART_BAUD_SBR_MASK;
				hw->obj->BAUD |= sbr;
			}
			else
			{
				/* Unacceptable baud rate difference of more than 3% */
			}
			// set rx/tx fifo enable
			hw->obj->FIFO |= (LPUART_FIFO_RXFE_MASK+LPUART_FIFO_TXFE_MASK+LPUART_FIFO_TXFIFOSIZE(1)+LPUART_FIFO_RXFIFOSIZE(1));	/*lint !e835*/
			//hw->obj->WATER|= LPUART_WATER_TXWATER(3);	/*lint !e835*/
			// reflush rx and tx fifo
			hw->obj->FIFO |= (LPUART_FIFO_TXFLUSH_MASK+LPUART_FIFO_RXFLUSH_MASK);
			//enable rx & tx ctrl reg
			hw->obj->CTRL |= (LPUART_CTRL_RE_MASK+LPUART_CTRL_TE_MASK);
			//hw->obj->CTRL |= LPUART_CTRL_TE_MASK;
			
			HalInt_VectSet(hw->rx_evt_num,uart_int_isr[hw->uart_name]);
			HalInt_En(hw->rx_evt_num);
			
			//enable uart rx interrupt, reset tx and rx fifo
			//enable rx interrupt
			hw->obj->CTRL |= LPUART_CTRL_RIE_MASK;
		}
	}
}
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void	S32K14x_UART_DeInit(void const * hdl)
{
	if ( hdl != NULL )
	{
		UART_HW_ATTR const *hw = (UART_HW_ATTR*)hdl;
		//disable uart interrupt
		hw->obj->STAT = LPUART_CLR_ALL_INTERRUPT_FLAGS; //clear all interrupt flags
        hw->obj->CTRL = LPUART_DIS_ALL_INTERRUPT;       //disable all interrupt sources.
        HalInt_Dis(hw->rx_evt_num);                     //disable UART NVIC ISR

//        HalGpio_InitPinGpio_Def(hw->rx_port,hw->rx_pin,	PORT_INPUT,OUTPUT_OPEN_DRAIN,INPUT_NO_PULL);
//        HalGpio_InitPinGpio_Def(hw->tx_port,hw->tx_pin,	PORT_OUTPUT,OUTPUT_OPEN_DRAIN,INPUT_NO_PULL);
//        HalGpio_SetPinVal(hw->tx_port,hw->tx_pin, LOW);

		//clear the para information in memory
		uart_tx_data[hw->uart_name].len    = 0;
		uart_tx_data[hw->uart_name].tx_cnt = 0;
		uart_tx_data[hw->uart_name].dat    = NULL;
	}
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void S32K14x_UART_Para_Set(void *hdl,HAL_UART_BAUD baud,HAL_UART_PARITY_CHECK parityChk,HAL_UART_WORD_LENS wordLens,HAL_UART_STOP_BITS stopBits)
{
	(void)hdl;
	if( hdl != NULL )
	{
		UART_HW_ATTR const *hw = (UART_HW_ATTR*)hdl;
		
		//save the information of uart
		uart_dev[hw->uart_name].baud		= baud;
		uart_dev[hw->uart_name].parityChk	= parityChk;
		uart_dev[hw->uart_name].stopBits	= stopBits;
		uart_dev[hw->uart_name].wordLens	= wordLens;
	}
}
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

static boolean S32K14x_UART_RxProc(void const * hdl)
{
	return FALSE;
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
//__root uint32 uart_water;
//__root uint32 uart_fifo;
static void	S32K14x_UART_Handle(uint8 uart_name)//(void const * hdl)
{
	UART_HW_ATTR const *hw = uart_hw_attr + uart_name;

	UART_TX_DAT			*tx_data = uart_tx_data + uart_name;
	UART_HW_DEV			*_dev	 = uart_dev + uart_name;


	/* process rx uart data */
	//while( !(hw->obj->FIFO & LPUART_FIFO_RXEMPT_MASK) )
	//{	
	//	uint8 rx_data = (uint8)(hw->obj->DATA);
		
	//	if(_dev->rx_clbak != NULL)
	//	{
	//		_dev->rx_clbak(_dev->arg,&rx_data,1);
	//	}
	//}
	if(hw->obj->STAT & LPUART_STAT_OR_MASK)
	{
		hw->obj->STAT |= LPUART_STAT_OR_MASK;
	}
	uint16 rx_dat;
	while( !((rx_dat = (uint16)(hw->obj->DATA))&LPUART_DATA_RXEMPT_MASK) )
	{
		if(_dev->rx_clbak != NULL)
		{
			uint8 temp =(uint8)rx_dat;
			_dev->rx_clbak(_dev->arg,&temp,1);
		}
	}
	
	if ( tx_data->len )
	{
		if ( tx_data->tx_cnt < tx_data->len )
		{
			while ( tx_data->tx_cnt < tx_data->len )
			{
				if ( ((hw->obj->WATER>>8)&0x07) < 3)
				{
					hw->obj->DATA = tx_data->dat[tx_data->tx_cnt++];
				}
				else
				{
					break;
				}
			}
			if ( tx_data->tx_cnt == tx_data->len )
			{	// disable tdre interrupt
				hw->obj->CTRL &= ~LPUART_CTRL_TIE_MASK;			/* disable TDRE interrupts 	*/
				// enable TC interruupt
				hw->obj->CTRL |= LPUART_CTRL_TCIE_MASK;			/* enable TC interrupts 	*/
			}
		}
		else
		{
			if ( hw->obj->STAT & LPUART_STAT_TC_MASK )
			{
				hw->obj->CTRL &= ~LPUART_CTRL_TCIE_MASK;		/* disable TC interrupts 	*/
				tx_data->len  = 0x00;
				if(_dev->tx_clbak != NULL)
				{
					_dev->tx_clbak(_dev->arg);
				}
			}
		}
	}
	else
	{
		hw->obj->CTRL &= ~(LPUART_CTRL_TCIE_MASK+LPUART_CTRL_TIE_MASK);		/* disable TC interrupts 	*/
	}
}
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void	S32K14x_UART_Send(void const*hdl, uint8 const* tx_dat,uint16 const len)
{
	UART_HW_ATTR const *hw = (UART_HW_ATTR*)hdl;
	if(len > 0)
	{
		UART_TX_DAT *tx = uart_tx_data + hw->uart_name;

		CPU_SR_ALLOC();
		CPU_CRITICAL_ENTER();
		
		tx->dat 	= tx_dat;
		tx->len		= len;
		tx->tx_cnt	= 0;
		hw->obj->CTRL |= LPUART_CTRL_TIE_MASK;	//enable TDRE interrupts

		CPU_CRITICAL_EXIT();
	}
}
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

static uint16	S32K14x_UART_GetBand(void * hdl)
{
	(void)hdl;
	uint16 ret = 0;
	
	if ( hdl != NULL )
	{
		UART_HW_ATTR const *hw = (UART_HW_ATTR*)hdl;
		
		if(hw->uart_name < HAL_UART_HW_NAME_MAX)
		{
			if(uart_dev[hw->uart_name].baud < HAL_UART_BAUD_MAX)
			{
				ret = (uint16)(uart_baud_tbl[uart_dev[hw->uart_name].baud] / 100);
			}
			else
			{
				ret = (uint16)(uart_baud_tbl[HAL_UART_BAUD_115200] / 100);
			}

			if(0 == ret)
			{
				if(0 != uart_dev[hw->uart_name].user_baud)
				{
					ret = (uint16)(uart_baud_tbl[uart_dev[hw->uart_name].user_baud]);
				}
				else
				{
					ret = (uint16)(uart_baud_tbl[HAL_UART_BAUD_115200] / 100);
				}
			}
		}
	}
	return ret;
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void	S32K14x_UART_SetPreDefBand(void *hdl,HAL_UART_BAUD baud)
{
	(void)hdl;
	if ( hdl != NULL && baud < HAL_UART_BAUD_MAX )
	{
		
		UART_HW_ATTR const *hw = (UART_HW_ATTR*)hdl;
		uart_dev[hw->uart_name].baud = baud;
	}
}


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

static void		S32K14x_UART_SetUserBand(void *hdl,uint16 baud)
{
	(void)hdl;
	if ( hdl != NULL && baud < HAL_UART_BAUD_MAX )
	{
		
		UART_HW_ATTR const *hw = (UART_HW_ATTR*)hdl;
		uart_dev[hw->uart_name].baud 		= HAL_UART_BAUD_USER_DEF;
		uart_dev[hw->uart_name].user_baud 	= baud;
	}
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void 	S32K14x_Uart_SetCalBk (	void *hdl,
											void * arg,
											void (*rx_clbak) (void*arg,unsigned char *rx_dat, unsigned short len),
											void (*tx_clbak) (void*arg),
											void (*err_clbak)(void*arg),
											void (*cyc_clbak)(void*arg),
											void (*wakeup_clbak)(void*arg) 
											)
{
	(void)hdl;
	if(hdl != NULL)
	{
		
		UART_HW_ATTR const *hw = (UART_HW_ATTR*)hdl;
		
		
		uart_dev[hw->uart_name].arg 			= arg;
		uart_dev[hw->uart_name].rx_clbak 		= rx_clbak;
		uart_dev[hw->uart_name].tx_clbak  		= tx_clbak;
		uart_dev[hw->uart_name].err_clbak  		= err_clbak;
		uart_dev[hw->uart_name].cyc_clbak  		= cyc_clbak;
		uart_dev[hw->uart_name].wakeup_clbak 	= wakeup_clbak;
	}
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


