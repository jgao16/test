/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#include "std_type.h"
#include "std_lib.h"

#include "hal_iic.h"
#include "hal_gpio.h"
#include "Bsp_gpio.h"
#include "gpio.cfg"
#include "hal_s32k14x_scg.h"
#include "hal_os.h"
#include "hal_hw.h"
#include "hal_int.h"
#include "trace_api.h"
#include "cpu.h"
#include "hal_s32k14x_iic_slv.h"
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#define HW_IIC_NUM                                                 (sizeof(i2c_hw_attr) / sizeof(IIC_SLV_HW_ATTR))

#define LPI2C_SSR_CLEAR_ALL_FLAGS                                  (0x00000F00u)

#define LPI2C_SLV_GLITCH_FILTER_WIDTH_NS 50
#define LPI2C_SLV_GLITCH_FILTER_MAX_CYCLE 15
#define LPI2C_SLV_DATA_VALID_DELAY_CYCLE 1
#define LPI2C_SLV_CLOCK_HOLD_CYCLE 2

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
typedef void(*IIC_INT_ISR)(void);  //for LPI2C hw ISR entry.

typedef struct
{
    LPI2C_Type   *      obj;
    uint32_t            baud;
    HAL_PORT_NAME       scl_port;
    unsigned char       scl_pin;
    HAL_PORT_NAME       sda_port;
    unsigned char       sda_pin;
    uint8_t             int_num;
    boolean             dma_used;                /* use DMA or not,TRUE:use it */
    uint8_t             dma_ch;
    uint8_t             addr;
} IIC_SLV_HW_ATTR;

typedef struct
{
    uint8_t addr;    /* 8 bit */
    IIC_SLV_STATE state;
    uint8_t * rx_buff;
    uint16_t rx_buff_wr_idx; /* Current write index. */
    uint16_t rx_buff_len; /* Total len. */
    uint8_t * volatile tx_buff;
    volatile uint16_t tx_buff_len;
    iic_slv_callback_t cb;
} DEV_IIC_SLV_HANDLE;

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static const IIC_SLV_HW_ATTR i2c_hw_attr[] =
{
#define CPU_IIC_SLV_HW_DEF(iic_name,hw_dev,iic_baud,scls_port,scls_pin,sdas_port,sdas_pin,int_num,dma_used,dma_ch,slave_address) \
                  {hw_dev,iic_baud,scls_port,scls_pin,sdas_port,sdas_pin,int_num,dma_used,dma_ch,slave_address},
    #include "cpu_iic_slv.cfg"
#undef  CPU_IIC_SLV_HW_DEF
};

static DEV_IIC_SLV_HANDLE   dev_iic_slv_handle[HW_IIC_NUM];

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void IIC_SLV_HwInit(uint8_t hw_ch, uint8_t *rx_buff, uint16_t rx_buff_len, iic_slv_callback_t cb);
static IIC_SLV_STATE IIC_SLV_Get_State(uint8_t hw_ch);
static void IIC_SLV_SetTxData(uint8_t hw_ch, uint8_t *data, uint16_t data_len);

static void        IIC_SLV_InitDrv(IIC_SLV_HW_ATTR const * hw_attr);
static void        IIC_SLV_IntSlaveHandle(uint8_t hw_ch);
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#define CPU_IIC_SLV_HW_DEF(iic_name,hw_dev,iic_baud,scls_port,scls_pin,sdas_port,sdas_pin,slv_int_num,dma_used,dma_ch,slave_address) \
    static void IIC_SLV_Int_##iic_name(void)    {IIC_SLV_IntSlaveHandle(iic_name##_ID);}
    #include "cpu_iic_slv.cfg"
#undef  CPU_IIC_SLV_HW_DEF

static IIC_INT_ISR const iic_slv_int_isr[] =
{
#define CPU_IIC_SLV_HW_DEF(iic_name,hw_dev,iic_baud,scls_port,scls_pin,sdas_port,sdas_pin,slv_int_num,dma_used,dma_ch,slave_address) \
    IIC_SLV_Int_##iic_name,
    #include "cpu_iic_slv.cfg"
#undef  CPU_IIC_SLV_HW_DEF
};

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static HAL_IIC_SLV_HW_API const hw_api =
{
    .init = IIC_SLV_HwInit,
    .set_tx_data = IIC_SLV_SetTxData,
    .get_state  = IIC_SLV_Get_State,
};

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
HAL_IIC_SLV_HW_API const *S32K14x_IIC_SLV_GetHwApi(void)
{
    return &hw_api;
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

static void IIC_SLV_HwInit(uint8_t hw_ch, uint8_t *rx_buff, uint16_t rx_buff_len, iic_slv_callback_t cb)
{
    if ( hw_ch < HW_IIC_NUM )
    {
        IIC_SLV_HW_ATTR const * hw_attr = i2c_hw_attr+hw_ch;
        __IO uint32_t     * pcc     = NULL;

        if ( hw_attr->obj == LPI2C0 )    {
            pcc = PCC->PCCn + PCC_LPI2C0_INDEX;
        }else if( hw_attr->obj == LPI2C1 ){
            pcc = PCC->PCCn + PCC_LPI2C1_INDEX;
        }else{}

        if ( pcc != NULL && (*pcc&PCC_PCCn_PR_MASK) )
        {    // enable gpio
            *pcc &= ~PCC_PCCn_CGC_MASK;
            *pcc &= ~PCC_PCCn_PCS_MASK;                        //clear PCS
            *pcc |= PCC_PCCn_PCS(6);                        //select SPLL
            *pcc |= PCC_PCCn_CGC_MASK;                        //enable iic clock

            HalInt_VectSet(hw_attr->int_num,iic_slv_int_isr[hw_ch]);

            if (HAL_GPIO_PT_A == hw_attr->scl_port)
            {
                HalGpio_PinAFConfig(hw_attr->scl_port, hw_attr->scl_pin, 3);
            }
            else if ((HAL_GPIO_PT_B == hw_attr->scl_port) || (HAL_GPIO_PT_D == hw_attr->scl_port))
            {
                HalGpio_PinAFConfig(hw_attr->scl_port, hw_attr->scl_pin, 2);
            }
            else if (HAL_GPIO_PT_E == hw_attr->scl_port)
            {
                HalGpio_PinAFConfig(hw_attr->scl_port, hw_attr->scl_pin, 4);
            }
            else
            {
            }

            if (HAL_GPIO_PT_A == hw_attr->sda_port)
            {
                HalGpio_PinAFConfig(hw_attr->sda_port, hw_attr->sda_pin, 3);
            }
            else if ((HAL_GPIO_PT_B == hw_attr->sda_port) || (HAL_GPIO_PT_D == hw_attr->sda_port))
            {
                HalGpio_PinAFConfig(hw_attr->sda_port, hw_attr->sda_pin, 2);
            }
            else if (HAL_GPIO_PT_E == hw_attr->sda_port)
            {
                HalGpio_PinAFConfig(hw_attr->sda_port, hw_attr->sda_pin, 4);
            }
            else
            {
            }

            dev_iic_slv_handle[hw_ch].state          = IIC_SLV_IDLE;
            dev_iic_slv_handle[hw_ch].rx_buff        = rx_buff;
            dev_iic_slv_handle[hw_ch].rx_buff_wr_idx = 0;
            dev_iic_slv_handle[hw_ch].rx_buff_len    = rx_buff_len;
            dev_iic_slv_handle[hw_ch].tx_buff        = NULL;
            dev_iic_slv_handle[hw_ch].tx_buff_len    = 0;
            dev_iic_slv_handle[hw_ch].cb          = cb;

            IIC_SLV_InitDrv(hw_attr); /* initial i2c device now */

            HalInt_En(hw_attr->int_num);
        }
    }
}

/* >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> */
static IIC_SLV_STATE IIC_SLV_Get_State(uint8_t hw_ch)
{
    return dev_iic_slv_handle[hw_ch].state;
}

/* >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> */
static void IIC_SLV_SetTxData(uint8_t hw_ch, uint8_t *data, uint16_t data_len)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    dev_iic_slv_handle[hw_ch].tx_buff        = data;
    dev_iic_slv_handle[hw_ch].tx_buff_len    = data_len;
    CPU_CRITICAL_EXIT();
}

/* >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> */
static uint8_t IIC_SLV_GetGlitchFilterCycle(uint32_t lpi2c_clk_hz, uint32_t glitchWidth_ns)
{
    uint32_t busCycle_ns = 1000000 / (lpi2c_clk_hz / 1000);

    uint32_t cycles = 0;

    while (((cycles + 1) * busCycle_ns) < glitchWidth_ns)
    {
        ++cycles;
    }

    // If we end up with zero cycles, then set the filter to a single cycle unless the
    // bus clock is greater than 10x the desired glitch width.
    if ((cycles == 0) && (busCycle_ns <= (glitchWidth_ns * 10)))
    {
        cycles = 1;
    }
    // If the cycles is greater the max cycles supported to set glitch filter,
    // then cycles should be equal to max cycles
    else if (cycles > LPI2C_SLV_GLITCH_FILTER_MAX_CYCLE)
    {
        cycles = LPI2C_SLV_GLITCH_FILTER_MAX_CYCLE;
    }

    return (uint8_t)cycles;
}

/* >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> */
static void IIC_SLV_InitDrv(IIC_SLV_HW_ATTR const * hw_attr)
{
    uint8_t glitchFilterCycle = 0;
    uint32_t lpi2c_clk_hz = 0;

    if ((NULL != hw_attr) && (NULL != hw_attr->obj))
    {
        lpi2c_clk_hz = HalS32K14x_ScgGetFreq(SPLLDIV2_CLK);

        glitchFilterCycle = IIC_SLV_GetGlitchFilterCycle(lpi2c_clk_hz, LPI2C_SLV_GLITCH_FILTER_WIDTH_NS);

        hw_attr->obj->SCR |= LPI2C_SCR_RST_MASK;                                          //reset slave logic
        hw_attr->obj->SCR &= ~LPI2C_SCR_RST_MASK;                                         //reset slave logic

        hw_attr->obj->SSR  = LPI2C_SSR_CLEAR_ALL_FLAGS;          //clear all interrupt flags

        hw_attr->obj->SCFGR1 = LPI2C_SCFGR1_ADRSTALL_MASK
                                | LPI2C_SCFGR1_RXSTALL_MASK
                                | LPI2C_SCFGR1_TXDSTALL_MASK;

        hw_attr->obj->SCFGR2 = LPI2C_SCFGR2_CLKHOLD(LPI2C_SLV_CLOCK_HOLD_CYCLE)
                                | LPI2C_SCFGR2_DATAVD(LPI2C_SLV_DATA_VALID_DELAY_CYCLE)
                                | LPI2C_SCFGR2_FILTSCL(glitchFilterCycle)
                                | LPI2C_SCFGR2_FILTSDA(glitchFilterCycle);

        hw_attr->obj->SAMR = LPI2C_SAMR_ADDR0(hw_attr->addr);

        hw_attr->obj->SIER = LPI2C_SIER_RDIE_MASK
                              | LPI2C_SIER_RSIE_MASK
                              | LPI2C_SIER_SDIE_MASK
                              | LPI2C_SIER_BEIE_MASK
                              | LPI2C_SIER_FEIE_MASK
                              | LPI2C_SIER_AM0IE_MASK;

        hw_attr->obj->SCR  |= LPI2C_SCR_SEN_MASK;             //SLV logic enable
    }
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void    IIC_SLV_IntSlaveHandle(uint8_t hw_ch)
{
    LPI2C_Type  volatile * iic_hw    = (LPI2C_Type  volatile * )((i2c_hw_attr + hw_ch)->obj);
    DEV_IIC_SLV_HANDLE  * iic_handle   = dev_iic_slv_handle + hw_ch;
    uint32_t ssr     = iic_hw->SSR;
    ssr &=  iic_hw->SIER;
    iic_hw->SSR      = ssr;  //Clear enabled interrupt flags.

    /*
     * Before checking the TX flag, call the callback so that application could
     * set TX data based on RX data.
     */
    /* Stop or repeat start, notify user about the RX data. */
    if(ssr & (LPI2C_SSR_RSF_MASK | LPI2C_SSR_SDF_MASK))
    {
        iic_hw->SIER &= ~LPI2C_SIER_TDIE_MASK;

        if (iic_handle->state == IIC_SLV_WRITE)
        {
            if ((iic_handle->cb != NULL) && (iic_handle->rx_buff_wr_idx > 0))
            {
                iic_handle->cb(IIC_SLV_WRITE_DONE, iic_handle->rx_buff, iic_handle->rx_buff_wr_idx);
            }
        }
        else if (iic_handle->state == IIC_SLV_READ)
        {
            if (iic_handle->cb != NULL)
            {
                iic_handle->cb(IIC_SLV_READ_DONE, NULL, 0);
            }
        }
    }

    if(ssr & LPI2C_SSR_AM0F_MASK)
    {
        iic_handle->addr = (uint8_t)(iic_hw->SASR);

        if(iic_handle->addr & 0x01) {
            iic_handle->state = IIC_SLV_READ;
            iic_hw->SIER |= LPI2C_SIER_TDIE_MASK;
        } else {
            iic_handle->rx_buff_wr_idx = 0;
            iic_handle->state = IIC_SLV_WRITE;
        }
    }

    if(ssr & LPI2C_SSR_RDF_MASK)
    {
        if (iic_handle->rx_buff_wr_idx < iic_handle->rx_buff_len)
        {
            iic_handle->rx_buff[iic_handle->rx_buff_wr_idx] = iic_hw->SRDR & 0xFF;
            iic_handle->rx_buff_wr_idx++;
        }
        else
        {
            /* No room for RX data. */
        }
    }

    if(ssr & LPI2C_SSR_TDF_MASK)
    {
        if (iic_handle->tx_buff_len > 0)
        {
            iic_hw->STDR = *(iic_handle->tx_buff);
            iic_handle->tx_buff++;
            iic_handle->tx_buff_len--;
        }
        else
        {
            /* No data to send, send 0. */
            iic_hw->STDR = 0;
        }
    }

    if(ssr & LPI2C_SSR_SDF_MASK)
    {
        iic_hw->SIER &= ~LPI2C_SIER_TDIE_MASK;
        iic_handle->state = IIC_SLV_IDLE;
        iic_handle->addr = 0;
    }

    if(ssr & (LPI2C_SSR_BEF_MASK | LPI2C_SSR_FEF_MASK))
    {
        iic_hw->SIER &= ~LPI2C_SIER_TDIE_MASK;

        /* Error. */
        iic_handle->state = IIC_SLV_IDLE;
    }

/*
    if(ssr == LPI2C_SSR_BEF_MASK|LPI2C_SSR_FEF_MASK){
        //iic SLV detected error.
        //TRACE_VALUE2(LOG_ERR, MOD_HW, "iic bus SLV Read Enter error State..., Proc SSR=0x%04x, Cur MSR=0x%04x", ssr,(uint16_t)(iic_hw->SSR));
    }else if(ssr & (LPI2C_SSR_AM0F_MASK | LPI2C_SSR_AVF_MASK)){
        iic_handle->slave_addr = iic_hw->SASR;
        //Bsp_SOC_INT_Dis();
        if(iic_handle->slave_addr & 0x01){
            iic_handle->state = IIC_SLV_WRITE;
        }else{
            iic_handle->state = IIC_SLV_READ;
        }
    }else if(ssr & LPI2C_SSR_TDF_MASK){
        iic_hw->STDR = iic_handle->dat[iic_handle->dat_cnt ++];
    }else if(ssr & LPI2C_SSR_RDF_MASK){
        if(iic_handle->dat_cnt == 0){
            reg_p = iic_hw->SRDR & 0xFF;
            iic_handle->reg_pointer = reg_p << 8;
            iic_handle->dat_cnt ++;
        }else if(iic_handle->dat_cnt == 1){
            reg_p = iic_hw->SRDR & 0xFF;
            iic_handle->reg_pointer |= reg_p;
            iic_handle->dat_cnt ++;
        }else{
            iic_handle->dat[iic_handle->reg_pointer + iic_handle->dat_cnt - 2] = iic_hw->SRDR & 0xff;
        }
    }else if(ssr & LPI2C_SSR_TAF_MASK){
        //Do nothing.
    }else if(ssr & LPI2C_SSR_RSF_MASK){

    }else if(ssr & LPI2C_SSR_SDF_MASK){
        iic_handle->state = IIC_SLV_IDLE;
        iic_handle->dat = NULL;
        iic_handle->dat_cnt = 0;
        iic_handle->reg_pointer = 0;
        iic_handle->slave_addr = 0;
        iic_handle->call_back();
    }else{
        //TRACE_VALUE2(LOG_ERR, MOD_HW, "iic bus SLV Read Enter error State..., Proc SSR=0x%04x, Cur MSR=0x%04x", ssr,(uint16_t)(iic_hw->SSR));
    }
*/
}
