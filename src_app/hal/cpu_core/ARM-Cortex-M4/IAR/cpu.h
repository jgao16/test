

#ifndef  CPU_H_____                                     
#define  CPU_H_____


#include  <intrinsics.h>
#include "stdint.h"
#include "FreeRTOS.h"

#ifdef __cplusplus
extern  "C" {
#endif



typedef            void      (*CPU_FNCT_VOID)(void);            /* See Note #2a.                                        */
typedef            void      (*CPU_FNCT_PTR )(void *p_obj);     /* See Note #2b.                                        */



/*
*********************************************************************************************************
*                                 CONFIGURE CPU ADDRESS & DATA TYPES
*********************************************************************************************************
*/




typedef  unsigned long                 CPU_SR;
      
	  
#define  CPU_SR_ALLOC()             	CPU_SR  cpu_sr = (CPU_SR)0
#define  CPU_CRITICAL_ENTER()         	do { cpu_sr = CPU_SR_Save(); CPU_TS_INT_DIS_MEAS_START();} while (0)
#define  CPU_CRITICAL_EXIT()          	do { CPU_TS_INT_DIS_MEAS_STOP(); CPU_SR_Restore(cpu_sr); } while (0)  /* Restore CPU status word.                     */


/*
*********************************************************************************************************
*                                    MEMORY BARRIERS CONFIGURATION
*
* Note(s) : (1) (a) Configure memory barriers if required by the architecture.
*
*                   CPU_MB      Full memory barrier.
*                   CPU_RMB     Read (Loads) memory barrier.
*                   CPU_WMB     Write (Stores) memory barrier.
*
*********************************************************************************************************
*/

#define  CPU_MB()       __DSB()
#define  CPU_RMB()      __DSB()
#define  CPU_WMB()      __DSB()

/*
*********************************************************************************************************
*                                         FUNCTION PROTOTYPES
*********************************************************************************************************
*/

void        CPU_IntDis       (void);
void        CPU_IntEn        (void);



CPU_SR      CPU_SR_Save      (void);
void        CPU_SR_Restore   (CPU_SR      cpu_sr);


void        CPU_WaitForInt   (void);
void        CPU_WaitForExcept(void);


//CPU_DATA    CPU_RevBits      (CPU_DATA    val);

uint32_t  CPU_CntLeadZeros(uint32_t  val);

#ifdef __cplusplus
}
#endif

#endif                                                      


