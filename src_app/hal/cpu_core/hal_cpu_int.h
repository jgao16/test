
#ifndef HAL_CPU_INT_H_
#define HAL_CPU_INT_H_

typedef enum
{
#ifdef CPU_STM32L1xx
	#define STM32L1XX_INT_DEF(x)	HAL_INT_ID_##x,
	#include"cpu_interrupt.cfg"
	#undef STM32L1XX_INT_DEF
	
#elif defined(CPU_STM32F10x)
	#define STM32F10X_INT_DEF(x)	HAL_INT_ID_##x,
	#include"cpu_interrupt.cfg"
	#undef STM32F10X_INT_DEF

#elif defined(CPU_S32K14x)
	#define S32K14x_INT_DEF(x)	HAL_INT_ID_##x,
	#include"cpu_interrupt.cfg"
	#undef S32K14x_INT_DEF
	
#else
	#error "build not define cpu..."
#endif	
	HAL_INT_ID_MAX
}HAL_INT_ID_NAME;


#endif