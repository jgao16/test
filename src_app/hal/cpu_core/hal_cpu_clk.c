/******************************************************************************
 * Project          s32k14x platform
 * (c) copyright    2015
 * Company          Visteon
 *                  All rights reserved
 * creation Date    
 ******************************************************************************/
#include "std_type.h"
#include "std_lib.h"
//#include "hal_eep.h"
#include "hal_s32k14x_scg.h"


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#define SMC_PMCTRL_STOPM_NORMAL_STOP_MASK          (0x00000000)
#define SMC_PMCTRL_STOPM_VLP_STOP_MASK             (0x00000002)

#define SCG_SOSCCFG_RANGE_HIGH_FREQ_MASK           (0x00000030)

#define SCG_SOSCDIV_SOSCDIV3_DIV_1_MASK            (0x00010000)
#define SCG_SOSCDIV_SOSCDIV2_DIV_1_MASK            (0x00000100)
#define SCG_SOSCDIV_SOSCDIV1_DIV_1_MASK            (0x00000001)

#define SCG_PARAM_CLKPRES_SOSC_MASK                (0x00000002)
#define SCG_PARAM_CLKPRES_SPLL_MASK                (0x00000040)

#define SCG_SPLLDIV_DIV3_1_MASK                    (0x00010000)
#define SCG_SPLLDIV_DIV2_1_MASK                    (0x00000100)
#define SCG_SPLLDIV_DIV1_1_MASK                    (0x00000001)

#define SCG_SPLLCFG_MULT_20_MASK                   (0x00040000)
#define SCG_SPLLCFG_MULT_24_MASK                   (0x00080000)
#define SCG_SPLLCFG_PREDIV_2_MASK                  (0x00000100)
#define SCG_SPLLCFG_PREDIV_4_MASK                  (0x00000300)
#define SCG_SPLLCFG_PREDIV_8_MASK                  (0x00000700)

#define SCG_RCCR_SCS_SPLL_MASK                     (0x06000000)
#define SCG_RCCR_SCS_SOSC_MASK                     (0x01000000)
#define SCG_RCCR_SCS_FIRC_MASK                     (0x03000000)
#define SCG_RCCR_SCS_RTCO_MASK                     (0x04000000)
#define SCG_RCCR_DIVBUS_2_MASK                     (0x00000010)
#define SCG_RCCR_DIVSLOW_1_MASK                    (0x00000001)
#define SCG_RCCR_DIVSLOW_4_MASK                    (0x00000003)
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void HalClk_CpuClkInit(void)
{
	HalS32K14x_ScgInit();
}


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
unsigned long 	HalClk_GetCpuFreq(void)
{
	return HalS32K14x_ScgGetFreq(CORE_CLK);
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
//void			BSP_SetSysclkPLL ( void )
//{
//	
//}
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


