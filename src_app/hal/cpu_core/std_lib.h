/*
********************************************************************************************************
*                                        std_lib.h 
*                           (c) Copyright 2011, Chao Wang
*                                     All Rights Reserved
*                                         V0.01
*
*********************************************************************************************************
*/
#ifndef STD_LIB_H_H_
#define STD_LIB_H_H_

//#pragma once

#define HSE_VALUE    ((uint32_t)8000000) /*!< Value of the External oscillator in Hz */

#if defined(CPU_S32K14x)
	#include "std_type.h"
	#include "S32K14x.h"
	#include "core_cm4.h"
#else
	#error "build not define cpu..."
#endif	


#endif


