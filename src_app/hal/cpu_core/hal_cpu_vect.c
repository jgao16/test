

/* >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> */

#include  "std_type.h"

#include  "hal_int.h"
//#include  "hal_clk.h"
#include "std_lib.h"

//#include "bsp_gpio.h"

//lint -e10

/*
*********************************************************************************************************
*                                          LOCAL DATA TYPES
*********************************************************************************************************
*/
typedef  void     (*CPU_FNCT_VOID)(void); 
typedef  union {
    CPU_FNCT_VOID   Fnct;
    void           *Ptr;
} APP_INTVECT_ELEM;

/*
*********************************************************************************************************
*                                            LOCAL TABLES
*********************************************************************************************************
*/


/*
*********************************************************************************************************
*                                       LOCAL GLOBAL VARIABLES
*********************************************************************************************************
*/


/*
*********************************************************************************************************
*                                      LOCAL FUNCTION PROTOTYPES
*********************************************************************************************************
*/

//lint -e716 -e708 -e651 -e526

#pragma language=extended
#pragma segment="CSTACK"


extern  void  __iar_program_start(void);
/*
 * Exception handlers.
 */
extern void xPortSysTickHandler( void );
extern void xPortPendSVHandler( void );
extern void vPortSVCHandler( void );

static  void  App_NMI_ISR        (void);
static  void  App_Fault_ISR      (void);
static  void  App_BusFault_ISR   (void);
static  void  App_UsageFault_ISR (void);
static  void  App_MemFault_ISR   (void);
static  void  App_Spurious_ISR   (void);
static 	void  Program_start 	 (void);



#ifdef CPU_STM32L1xx
	#define STM32L1XX_INT_DEF(x)	void cpu_interrupt_##x(void) { HalInt_Handler(HAL_INT_ID_##x); }
	#include"cpu_interrupt.cfg"
	#undef STM32L1XX_INT_DEF
	
#elif defined(CPU_STM32F10x)
	#define STM32F10X_INT_DEF(x)	void cpu_interrupt_##x(void) { HalInt_Handler(HAL_INT_ID_##x); }
	#include"cpu_interrupt.cfg"
	#undef STM32F10X_INT_DEF

#elif defined(CPU_S32K14x)
	#define S32K14x_INT_DEF(x)	void cpu_interrupt_##x(void) { HalInt_Handler(HAL_INT_ID_##x); }
	#include"cpu_interrupt.cfg"
	#undef S32K14x_INT_DEF
	
#else
	#error "build not define cpu..."
#endif	



/*
*********************************************************************************************************
*                                     LOCAL CONFIGURATION ERRORS
*********************************************************************************************************
*/

/*
*********************************************************************************************************
*                                  EXCEPTION / INTERRUPT VECTOR TABLE
*
* Note(s) : (1) The Cortex-M3 may have up to 256 external interrupts, which are the final entries in the
*               vector table.  The STM32 has 48 external interrupt vectors.
*********************************************************************************************************
*/
__root  const  APP_INTVECT_ELEM  __vector_table[] @ ".intvec" = {
    { .Ptr = (void *)__sfe( "CSTACK" )},                        /*  0, SP start value.                                  */
    //__iar_program_start,                                        /*  1, PC start value.                                  */
	Program_start,
    App_NMI_ISR,                                                /*  2, NMI.                                             */
    App_Fault_ISR,                                              /*  3, Hard Fault.                                      */
    App_MemFault_ISR,                                           /*  4, Memory Management.                               */
    App_BusFault_ISR,                                           /*  5, Bus Fault.                                       */
    App_UsageFault_ISR,                                         /*  6, Usage Fault.                                     */
    App_Spurious_ISR,                                           /*  7, Reserved.                                        */
    App_Spurious_ISR,                                           /*  8, Reserved.                                        */
    App_Spurious_ISR,                                           /*  9, Reserved.                                        */
    App_Spurious_ISR,                                           /* 10, Reserved.                                        */
    vPortSVCHandler,                                           	/* 11, SVCall.                                          */
    App_Spurious_ISR,                                           /* 12, Debug Monitor.                                   */
    App_Spurious_ISR,                                           /* 13, Reserved.                                        */
    xPortPendSVHandler,                                      	/* 14, PendSV Handler.                                  */
    xPortSysTickHandler,                                      	/* 15, Freertos Tick ISR Handler.                       */
#ifdef CPU_STM32L1xx
	#define STM32L1XX_INT_DEF(x)	cpu_interrupt_##x,
	#include"cpu_interrupt.cfg"
	#undef STM32L1XX_INT_DEF
#elif defined(CPU_STM32F10x)
	#define STM32F10X_INT_DEF(x)	cpu_interrupt_##x,
	#include"cpu_interrupt.cfg"
	#undef STM32F10X_INT_DEF
	
#elif defined(CPU_S32K14x)
	#define S32K14x_INT_DEF(x)	cpu_interrupt_##x,
	#include"cpu_interrupt.cfg"
	#undef S32K14x_INT_DEF
	
#else
	#error "build not define cpu..."
#endif	
};

static unsigned long const vector_table_addr = (unsigned long)__vector_table;

static 	void  Program_start (void)
{
#define WDOG_UNLOCK_KEY     (0xD928C520UL)
#define WDOG_CLK_FROM_BUS   (0x0UL)
#define WDOG_CLK_FROM_LPO   (0x1UL)
#define WDOG_CLK_FROM_SOSC  (0x2UL)
#define WDOG_CLK_FROM_SIRC  (0x3UL)
	/*inital write of wdog config register; clock select from lpo, update enable, watchdog disabled*/
    PMC->REGSC = 0x00;  //LPO enabled, bias disabled.
	WDOG->CNT = (uint32_t ) WDOG_UNLOCK_KEY;
	WDOG->CS =  (uint32_t ) ((1 << 13) | (WDOG_CLK_FROM_LPO << WDOG_CS_CLK_SHIFT) /*| (0 << WDOG_CS_EN_SHIFT)*/
	          | (1 << WDOG_CS_UPDATE_SHIFT));
	/*write of the wdog unlock key to cnt register, must be done in order to allow any modifications*/
	WDOG->TOVAL = (uint32_t )0xFFFF;

	__set_MSP(*(uint32*)vector_table_addr);
	NVIC_SetVectorTable(vector_table_addr,0);
	//NVIC_SetPriorityGrouping(0x5);	// must be done 

	__iar_program_start();
}

/*
*********************************************************************************************************
*                                            App_NMI_ISR()
*
* Description : Handle Non-Maskable Interrupt (NMI).
*
* Argument(s) : none.
*
* Return(s)   : none.
*
* Caller(s)   : This is an ISR.
*
* Note(s)     : (1) Since the NMI is not being used, this serves merely as a catch for a spurious
*                   exception.
*********************************************************************************************************
*/

static  void  App_NMI_ISR (void)
{
	NVIC_SystemReset();
    //while (1) {
    //    ;
    //}
}

/*
*********************************************************************************************************
*                                             App_Fault_ISR()
*
* Description : Handle hard fault.
*
* Argument(s) : none.
*
* Return(s)   : none.
*
* Caller(s)   : This is an ISR.
*
* Note(s)     : none.
*********************************************************************************************************
*/
void 	S32K14x_DflashReadErr(void);
boolean S32K14x_DflashReadProcess(void);

static  void  App_Fault_ISR (void)
{
	//if ( FTFC->FERSTAT & 0x02 )
	if ( S32K14x_DflashReadProcess() )
	{
		// dflash read error
		FTFC->FERSTAT = 0x02;
		S32K14x_DflashReadErr();

		__asm("mrs r1,psp");
		__asm("mov r2, #0x18");
		__asm("add r3,r1,r2");
		__asm("ldr r0,[r3]");
		__asm("mov r2, #0x4");
		__asm("add r0,r0,r2");
		__asm("str r0,[r3]");

		//NVIC_SystemReset();
		//__iar_program_start();
	}
	else
	{
		NVIC_SystemReset();
		//while(1)
		//{}
	}
}


/*
*********************************************************************************************************
*                                           App_BusFault_ISR()
*
* Description : Handle bus fault.
*
* Argument(s) : none.
*
* Return(s)   : none.
*
* Caller(s)   : This is an ISR.
*
* Note(s)     : none.
*********************************************************************************************************
*/

static  void  App_BusFault_ISR (void)
{
	NVIC_SystemReset();
    //while (1) {
    //    ;
    //}
}


/*
*********************************************************************************************************
*                                          App_UsageFault_ISR()
*
* Description : Handle usage fault.
*
* Argument(s) : none.
*
* Return(s)   : none.
*
* Caller(s)   : This is an ISR.
*
* Note(s)     : none.
*********************************************************************************************************
*/

static  void  App_UsageFault_ISR (void)
{
	NVIC_SystemReset();
    //while (1) {
    //    ;
    //}
}


/*
*********************************************************************************************************
*                                           App_MemFault_ISR()
*
* Description : Handle memory fault.
*
* Argument(s) : none.
*
* Return(s)   : none.
*
* Caller(s)   : This is an ISR.
*
* Note(s)     : none.
*********************************************************************************************************
*/

static  void  App_MemFault_ISR (void)
{
	NVIC_SystemReset();
    //while (1) {
    //    ;
    //}
}


/*
*********************************************************************************************************
*                                           App_Spurious_ISR()
*
* Description : Handle spurious interrupt.
*
* Argument(s) : none.
*
* Return(s)   : none.
*
* Caller(s)   : This is an ISR.
*
* Note(s)     : none.
*********************************************************************************************************
*/

static  void  App_Spurious_ISR (void)
{
	NVIC_SystemReset();
    //while (1) {
    //    ;
    //}
}
