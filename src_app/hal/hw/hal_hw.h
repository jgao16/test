
#ifndef __HAL_HW_H_H__
#define __HAL_HW_H_H__
/*****************************************************************************
 *  Include Files
 *****************************************************************************/

//#include "hal_stm32l1xx_iic.h"

/*****************************************************************************
 *  Global Macro Definitions
 *****************************************************************************/

typedef enum
{
#define CPU_UART_HW_DEF(name,uart_attr,hw_dev,rx_port,rx_pin,tx_port,tx_pin,rx_int_num,tx_int_num,err_int_num,dma_dev,dam_int_num) \
                name##_ID,
    #include "cpu_uart.cfg"
#undef  CPU_UART_HW_DEF    
    HAL_UART_HW_NAME_MAX
}HAL_UART_HW_NAME;


typedef enum
{
#define CPU_IIC_HW_DEF(iic_name,hw_dev,iic_mode,iic_baud,scl_port,scl_pin,sda_port,sda_pin,int_num,dma_used,dma_ch,slave_address) iic_name##_ID,
    #include "cpu_iic.cfg"
#undef  CPU_IIC_HW_DEF

#define FLEX_IO_IIC(iic_name,hw_dev,iic_mode,iic_baud,scl_port,scl_pin,sda_port,sda_pin,int_num,int_err_num)  iic_name##_ID,
    #include "flexio.cfg"
#undef  FLEX_IO_IIC

    HAL_IIC_HW_NAME_MAX
}HAL_IIC_HW_NAME;

typedef enum
{
#define CPU_IIC_SLV_HW_DEF(iic_name,hw_dev,iic_baud,scl_port,scl_pin,sda_port,sda_pin,int_num,dma_used,dma_ch,slave_address) iic_name##_ID,
    #include "cpu_iic_slv.cfg"
#undef  CPU_IIC_SLV_HW_DEF

    HAL_IIC_SLV_HW_NAME_MAX
}HAL_IIC_SLV_HW_NAME;

typedef enum
{
#define CPU_SPI_HW_DEF(spi_hw_name,hw_obj,master,trans,calbk,sck_pt,sck_pin,mosi_pt,mosi_pin,miso_pt,miso_pin,nss_pt,nss_pin,nss_actv) spi_hw_name##_ID,
	#include "cpu_spi.cfg"
#undef CPU_SPI_HW_DEF
	HAL_SPI_HW_NAME_MAX
}HAL_SPI_HW_NAME;

typedef enum
{
#define CPU_FTM_HW_DEF(hw_name, hw_obj, pwm_ch, port_name, pin_num, af_num) hw_name##_ID,
    #include "cpu_ftm.cfg"
#undef CPU_FTM_HW_DEF
    HAL_FTM_HW_NAME_MAX
}HAL_FTM_HW_NAME;

typedef enum
{
#define CPU_ADC_HW_DEF(hw_name, hw_obj, adc_ch, port_name, pin_num) hw_name##_ID,
    #include "cpu_adc.cfg"
#undef CPU_ADC_HW_DEF
    HAL_ADC_HW_NAME_MAX
}HAL_ADC_HW_NAME;

typedef enum
{
#define HAL_HW_API_DEF(hw, api) hw##_ID,
	#include "hal_hw_api.cfg"
#undef HAL_HW_API_DEF
	HAL_HW_ID_MAX	
}HAL_HW_ID_NAME;
/*****************************************************************************
 *  Global Type Declarations
 *****************************************************************************/
/* %<Types> */


/*****************************************************************************
 *  Global Data Declarations
 *****************************************************************************/
/* %<CalStructHeader> 	*/
/* %<CalStructContents>	*/
/* %<CalStructTrailer>	*/


/*****************************************************************************
 *  Global Function Declarations
 *****************************************************************************/

void HalHw_Init(void); 


//void const * HalHw_GetCfg(uint8 hw_name);
void const * HalHw_GetApi(uint8 hw_name);

#endif





