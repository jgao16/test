/*******************_**********************************************************
 *
 * Copyright (c) 
 *
 *****************************************************************************/
 

 /*****************************************************************************
 *  Include Files
 *****************************************************************************/
#include "std_type.h"

#define INCLUDE_HAL_HEAD_FILE
	#include "hal_hw_api.cfg"
#undef INCLUDE_HAL_HEAD_FILE


#include "hal_hw.h"

#include "hal_os.h"

#include "hal_s32k14x_edma.h"

/*****************************************************************************
 *  Local Macro Definitions
 *****************************************************************************/

 /*****************************************************************************
 *  Local Type Declarations
 *****************************************************************************/
typedef const void *(*GET_API_FUNC)(void);


 /*****************************************************************************
 *  Global Variable Definitions (Global for the whole system)
 *****************************************************************************/

 
/*****************************************************************************
 *  Local Variable Definitions (Global within this file)
 *****************************************************************************/
/* event poll								*/
/*#define CPU_UART_DEF(name,com_attr,hw_dev,rx_port,rx_pin,tx_port,tx_pin,rx_int_num,tx_int_num,err_int_num,dma_dev,dam_int_num) \
		   {name##_ID,com_attr,hw_dev,rx_port,rx_pin,tx_port,tx_pin,rx_int_num,tx_int_num,err_int_num,dma_dev,dam_int_num},
static const STM32F10x_UART_HW_ATTR	stm32f10x_uart_hw_attr[]=
{
	#include "cpu_uart.cfg"		
};
#undef  CPU_UART_DEF
*/

/*
#undef STM32L1xx_SPI_HW_DEF
#define STM32L1xx_SPI_HW_DEF(spi_hw_name,hw_obj,sck_pt,sck_pin,mosi_pt,mosi_pin,miso_pt,miso_pin,nss_pt,nss_pin	)	\
		{hw_obj,sck_pt,sck_pin,mosi_pt,mosi_pin,miso_pt,miso_pin,nss_pt,nss_pin},
static const STM32L1xx_SPI_HW_ATTR spi_attr[] = 
{
	#include "stm32l1xx_spi.cfg"
};
#undef STM32L1xx_SPI_HW_DEF

static const STM32L1xx_IIC_HW_ATTR stm32l1xx_iic_attr[] = 
{
#undef  STM32L1xx_IIC_HW_DEF
#define STM32L1xx_IIC_HW_DEF(name,hw_dev,iic_mode,iic_baud,scl_port,scl_pin,sda_port,sda_pin,int_evt_num,int_err_num) \
				  {hw_dev,iic_baud,iic_mode,scl_port,scl_pin,sda_port,sda_pin,int_evt_num,int_err_num},
    #include "stm32l1xx_iic.cfg"
#undef  STM32L1xx_IIC_HW_DEF
};
*/

/* hal hw api arrary*/
static const GET_API_FUNC hal_hw_get_api[] = 
{
#undef HAL_HW_API_DEF
#define HAL_HW_API_DEF(hw, api) (GET_API_FUNC)(api),
	#include "hal_hw_api.cfg"
#undef HAL_HW_API_DEF
};






 /*****************************************************************************
 *  Global Function Declarations
 *****************************************************************************/

/*****************************************************************************
 *  Local Function Declarations
*****************************************************************************/

/*****************************************************************************/
/*  Global Function Definitions                                              */
/*****************************************************************************/

/*****************************************************************************
*
* Function:  ServEvt_Create
*
* Description: create event queue
*              
*                                      
* Called from: 	
*
* Requrement:  
*
******************************************************************************/


void HalHw_Init ( void )
{

	/*hal gpio initial				*/
	//HalGpio_Init();


/** >>>>>>>>>>>>>>>>>>>> mount cpu device >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/		
	HalS32K14x_Wdt_Mount();
	/* initial cpu spi */
	HalS32K14x_SpiInit();

	HalS32k14x_eDMAInit();

	HalS32K14x_AdcHwInit();
	/* mount stm32l1xx uart	*/
	//STM32F10x_Uart_MountHw(stm32f10x_uart_hw_attr,sizeof(stm32f10x_uart_hw_attr)/sizeof(STM32F10x_UART_HW_ATTR));
	/* mount stm32l1xx_iic	*/
	//STM32L1xx_Iic_Mount(stm32l1xx_iic_attr,sizeof(stm32l1xx_iic_attr)/sizeof(STM32L1xx_IIC_HW_ATTR));
	/* mount stm32l1xx_spi	*/
	//STM32L1xx_Spi_Mount(spi_attr,sizeof(spi_attr)/sizeof(STM32L1xx_SPI_HW_ATTR));
	/* mount eeprom driver 	*/
	//STM32L1xx_Eep_Mount(0x08080000, 12288);
	//STM32F10x_Rtc_Mount(USE_EXTERNAL_CRYSTAL);
/** >>>>>>>>>>>>>>>>>>>> mount extern device >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/			
	/* mount pcf8583 hw 	*/
	//PCF8583_MountAttr(HAL_GPIO_PT_C,14,RTC_PCF8583_ID);
	//S35392_MountAttr(HAL_GPIO_PT_C,14,RTC_S35392_ID);
	/* mount max6864		*/
	//Max6864_Wdt_Mount(HAL_GPIO_PT_C,13);//pc13
	/* AD5422 mount */
	//Ad5422_AoOut_Mount(HAL_GPIO_PT_B, 13, SPI_AD5422_ID);

	/* m25pxx mount */
	//M25Pxx_Mount(HAL_GPIO_PT_B,13,SPI_M25Pxx_ID);
	

	//TdcGp22_Mount(0, HAL_GPIO_PT_A, 4, SPI_TDC_GP22_0_ID);
	//Ad8324_Mount(HAL_GPIO_PT_B, 12, SPI_AD8324_ID);
	//Ad5623_Mount(HAL_GPIO_PT_B, 11, SPI_AD5623_ID);

	//Hc595Display_Mount(&hc595_display_cfg);
	//St7920Display_Mount(&st7920_display_cfg);
	//MatricKey_Mount(&matric_key_cfg);
/** >>>>>>>>>>>>>>>>>>>> cpu device hal initial start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/	
	/** >>>>hal wdt initial>>>>> **/
	HalWdt_Init();
	/* pet watchdog		*/
	HalWdt_Feed();

	/* hal i2c initial 	*/
	HalIic_Init();    
	/* hal uart init	*/
	HalUart_Init();
	/* hal spi initial	*/
	HalSpi_Init();
	/* hal eeprom */
	//HalEep_Init();

	/* hal flash 		*/
	HalFlash_Init();
	HalDFlash_Init();
	
/** >>>>>>>>>>>>>>>>>>>> extern device initial start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/	
	/* hal rtc initial	*/
	HalRtc_Init();

	/* ao out initial	*/
    //HalAoOut_Init();
    
	/* pet watchdog		*/
	HalWdt_Feed();


/** >>>>>>>>>>>>>>>>>>>> extern bsp initial start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/	
	/* initial tdc_gp22 reset pin */
	//HalGpio_InitPinGpio_Def(HAL_GPIO_PT_D,2,PORT_OUTPUT,OUTPUT_PUSH_PULL,INPUT_NO_PULL);
	/* set cs to chip dis state */
	//HalGpio_SetPinVal(HAL_GPIO_PT_D, 2,HIGH);

	/* initial tdc gp22 */
	//(void)TdcGp22_Init(0);
	/* ad5623 initial 	*/
	//Ad5623_Init();
	/* ad5623 initial	*/
	//Ad5623_Init();
	//HalLcm16032_Init();
	//HalLcm16032_SetBackLight(LCM16032_BL_ON);
	//HalLcm16032_WrScreenString("   tiger platform    Hal Lcm16032 test  ");

	/* hal key 	*/
	//HalKey_Init();

	//uint8 wr_dat[7]={0x11,0x22,0x33,0x44,0x55,0x66,0x77};
	//(void)HalEep_Write(0, wr_dat, sizeof(wr_dat));
	//(void)HalEep_Write(1, wr_dat, sizeof(wr_dat));
	//(void)HalEep_Write(2, wr_dat, sizeof(wr_dat));
	//(void)HalEep_Write(3, wr_dat, sizeof(wr_dat));
	//(void)HalEep_Write(4, wr_dat, sizeof(wr_dat));
	//(void)HalEep_Write(5, wr_dat, sizeof(wr_dat));
	//(void)HalEep_Write(6, wr_dat, sizeof(wr_dat));
	//(void)HalEep_Write(7, wr_dat, sizeof(wr_dat));
}

/*****************************************************************************
*
* Function:  HalHw_GetCfg
*
* Description: del a event
*              
*                                      
* Called from: 	
*
* Requrement:  
*
******************************************************************************/

void const * HalHw_GetApi(uint8 hw_name)
{
	void const * api = NULL;
	if (hw_name < HAL_HW_ID_MAX)
	{
		if ( hal_hw_get_api[hw_name] != NULL )
		{
			api = hal_hw_get_api[hw_name]();
		}
	}

	//if ( hw_name == HAL_LCM16032_HW_ID )
	//{
	//	api = (void const *)St7920Display_Lcm16032_GetApi();
	//}
	
	return api;
}
/*****************************************************************************
*
* Function:  ServEvt_Post
*
* Description: post a event
*              
*                                      
* Called from: 	
*
* Requrement:  
*
******************************************************************************/



/*****************************************************************************
*
* Function:  ServEvt_PostFront
*
* Description: post a event to queue front 
*              
*                                      
* Called from: 	
*
* Requrement:  
*
******************************************************************************/






/*****************************************************************************
 *  Local Function Definitions
 *****************************************************************************/
 
/*****************************************************************************
*
* Function:  
*
* Description: 
*              
*                                      
* Called from: 	
*
* Requrement:  
*
******************************************************************************/

