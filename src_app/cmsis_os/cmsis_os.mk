

SOURCE_DIRS  +=    cmsis_os
INCLUDE_DIRS +=  -Icmsis_os
SOURCE_DIRS  +=    cmsis_os/Freertos
INCLUDE_DIRS +=  -Icmsis_os/Freertos
INCLUDE_DIRS +=  -Icmsis_os/Freertos/include
SOURCE_DIRS  +=    cmsis_os/Freertos/portable/IAR/ARM_CM4F
INCLUDE_DIRS +=  -Icmsis_os/Freertos/portable/IAR/ARM_CM4F
SOURCE_DIRS  +=    cmsis_os/Freertos/portable/MemMang
INCLUDE_DIRS +=  -Icmsis_os/Freertos/portable/MemMang