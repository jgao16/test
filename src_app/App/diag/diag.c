
#include "stdlib.h"
#include "string.h"
#include "serv.h"
#include "pwrctrl.h"
#include "diag_internal.h"
#include "diag.h"
#include "diag_phy.h"

/********************************* Structure and Enumeration */

typedef struct
{
    DIAG_FRAME          tx_frame;
    DIAG_FRAME          rx_frame;
    uint8_t             tx_frame_size;
    uint8_t             rx_frame_size;
    ePHY_SEND_STATE     tx_frame_state;
    boolean             EnabledState;
    boolean             clientConnected;
    boolean             clientConnected_ToClosed;
}DIAG_DISPATCH_State;

/********************************** Global Variable */

static DIAG_DISPATCH_State     s_dispatcher;
/* For the SEMA */
static  HAL_OS_SEM_ID actv_diag_task;

/********************************** Functions */
void Diag_DispatcherInit()
{
    /* Init the Dispatcher State */
    s_dispatcher.tx_frame_state = SEND_OK;
    s_dispatcher.tx_frame_size = 0;
    for (uint8_t ii=0; ii < 32; ii++)
    {
        s_dispatcher.tx_frame.diagRawData[ii] = 0;
    }

    s_dispatcher.rx_frame_size = 0;
    for (uint8_t ii=0; ii < 32; ii++)
    {
        s_dispatcher.rx_frame.diagRawData[ii] = 0;
    }
    s_dispatcher.EnabledState = TRUE;

    /* Init the PHY for the diagnostic communication */
    DiagPhy_Init();
}

void Diag_Dispatcher_SendNextFrameFromBuffer(void)
{
    /* Previous tx Frame is sent out and there is frame to be sent */
    if ((s_dispatcher.tx_frame_state == SEND_OK) && (s_dispatcher.tx_frame_size != 0))
    {
        // Retrieve the status from the PHY
        s_dispatcher.tx_frame_state = DiagPhy_SendPacket(&(s_dispatcher.tx_frame.diagRawData[0]), s_dispatcher.tx_frame_size);
        // Code can buffer more frame
        s_dispatcher.tx_frame_size = 0;
    }
}

/* Callback Function for the PHY Code */
void Diag_Dispatcher_TransmitComplete(void)
{
    /* Update Dispatcher State*/
    s_dispatcher.tx_frame_state = SEND_OK;

    if (s_dispatcher.clientConnected_ToClosed == TRUE)
    {
        s_dispatcher.clientConnected_ToClosed = FALSE;
        s_dispatcher.clientConnected = FALSE;
    }
    
    /* Trigger the Task to send next frame is available */
    HalOS_SemaphorePost(actv_diag_task);
}

/* Server provide the frame and send to Client */
void Diag_Dispatcher_InsertFrame(const DIAG_FRAME *pFrameToSend)
{
    CPU_SR_ALLOC();
    /* disable interrupt */
    CPU_CRITICAL_ENTER();

    if (s_dispatcher.tx_frame_size == 0)
    {
        s_dispatcher.tx_frame = *pFrameToSend;
        s_dispatcher.tx_frame_size = pFrameToSend->diagServerFrame.len + 1;
    }
    else
    {
        // assert(0);
    }

    /* restore interrupt */
    CPU_CRITICAL_EXIT();
}

/* Callback Function for the PHY code*/
// void Diag_Dispatcher_ReceiveFrame(const DIAG_FRAME * pFrameToAdd)
void Diag_Dispatcher_ReceiveFrame(uint8_t *ptr)
{
    /* if Trace is being disabled, discard all received Diag-Frames */
    if(s_dispatcher.EnabledState)
    {
        CPU_SR_ALLOC();
        /* disable interrupt */
        CPU_CRITICAL_ENTER();

        if (s_dispatcher.rx_frame_size == 0)
        {
            // s_dispatcher.rx_frame.diagRawData[0] = *ptr;
            // s_dispatcher.rx_frame_size++;
            for (uint8_t ii=0; ii < ((*ptr) + 1); ii++) 
            {
                s_dispatcher.rx_frame.diagRawData[ii] = *(ptr + ii);
                s_dispatcher.rx_frame_size++;
            }
            // for (uint8_t ii=0; ii < pFrameToAdd->diagClientFrame.len; ii++)
            // {
            //     s_dispatcher.rx_frame.diagRawData[ii] = pFrameToAdd->diagRawData[ii];
            //     s_dispatcher.rx_frame_size++;
            // }
        }

        /* restore interrupt */
        CPU_CRITICAL_EXIT();

        /* Trigger the Task to do with received frame is available */
        HalOS_SemaphorePost(actv_diag_task);
    }
}

boolean Diag_IsEnabledState()
{
    return(s_dispatcher.EnabledState);
}

void Diag_SetEnable(boolean isEnable)
{
    s_dispatcher.EnabledState = isEnable;

    if (s_dispatcher.EnabledState)
    {
        // TRACE_TEXT(TRACE_SWITCH_ALWAYS, MOD_ALWAYS, "=============== Switching ON DIAG!!!!================= ");
    }
    else
    {
        // TRACE_TEXT(TRACE_SWITCH_ALWAYS, MOD_ALWAYS, "++++++++++++++++ Switching OFF DIAG!!!! +++++++++++++++");
    }
}

boolean Diag_IsClientConnected()
{
    return s_dispatcher.clientConnected;
}

void Diag_SetClientConnected(boolean isConnected)
{
    // s_dispatcher.clientConnected = isConnected;

    if (isConnected)
    {
        s_dispatcher.clientConnected = TRUE; 
        // TRACE_TEXT(TRACE_SWITCH_ALWAYS, MOD_ALWAYS, "=============== DIAG Start !!!!================= ");
    }
    else
    {
        s_dispatcher.clientConnected_ToClosed = TRUE;
        // TRACE_TEXT(TRACE_SWITCH_ALWAYS, MOD_ALWAYS, "++++++++++++++++ DIAG Stop!!!! +++++++++++++++");
    }
}

void Diag_Message_Received(void)
{
	if ( s_dispatcher.rx_frame_size != 0)
	{
		// TRACE_VALUE(TRACE_SWITCH_DEBUG, MOD_ALWAYS, "???????????????????????? number of rx-commands in Q: %x ", FifoRx.used);

        // * Get the data out
        DIAG_FRAME msg = s_dispatcher.rx_frame;

		CPU_SR_ALLOC();
		CPU_CRITICAL_ENTER();
        // Release the buffer
        s_dispatcher.rx_frame_size = 0;
		CPU_CRITICAL_EXIT();

        /* Handle trace-command */
        Diag_ProcessClientCommands(&msg);

	}
}

void Diag_Message_Transmitted(void)
{
	if ( s_dispatcher.clientConnected )
	{
		Diag_Dispatcher_SendNextFrameFromBuffer();
	}
}

void Task_Diag( void const*p_arg )
{
    (void)p_arg;

    if ( actv_diag_task == NULL ){
        actv_diag_task = HalOS_SemaphoreCreateWithMaxCnt(1,1,"Actv Diagnostic Task...");
    }

    if ( actv_diag_task == NULL ){
        (void)osThreadTerminate(osThreadGetId());
    }

    /* Init Dispatcher */
    // TRACE_TEXT(TRACE_TEXT, MOD_DIAG, "############# Diag_Channels_Init() called! ");
    Diag_DispatcherInit();

    /* Enable tracing */
    s_dispatcher.clientConnected  = TRUE;


    for( ;; )
    {
        /* Wait for Execution of the Diagnostic Task */
        (void)HalOS_SemaphoreWait(actv_diag_task, 100);
        
        PwrCtrl_DisLpSleepGlobal(); // disable sleep

        Diag_Message_Transmitted(); // Active the Transfer BUFFER

        Diag_Message_Received();    // check if there is received message and Do with the Diagnostic Request

        PwrCtrl_EnLpSleepGlobal();  // enable sleep
        
    } // task infinite for loop
}

/**
 * @}
 */

