#ifndef _DIAG_INTERNAL_H_
#define _DIAG_INTERNAL_H_

#include "std_type.h"
#include "cpu.h"

/** Diag-Frame Union, unioning all Trace-message types
 */
typedef struct 
{
    uint8_t len;
    uint8_t cmd;
    uint8_t data[28];
}DIAG_CLIENT_FRAME;

typedef struct
{
    uint8_t len;
    uint8_t cmd;
    uint8_t flag;
    uint8_t data[27];
}DIAG_SERVER_FRAME;

typedef struct
{
    uint8_t len;
    uint8_t cmd;
}DIAG_FRAME_REQ;

typedef struct
{
    uint8_t len;
    uint8_t cmd;
    uint8_t flag;
    uint8_t data[20];
}DIAG_FRAME_RSP;

typedef struct
{
    uint8_t len;
    uint8_t cmd;
    uint8_t pwm;
}DIAG_FRAME_REQ_BACKLIGHT;

typedef struct 
{
    uint8_t     len;
    uint8_t     cmd;
    uint8_t     flag;
    uint8_t     version_4_msb;
    uint8_t     version_3;
    uint8_t     version_2;
    uint8_t     version_1_lsb;
}DIAG_FRAME_RSP_SW_VERSION;

typedef struct 
{
    uint8_t     len;
    uint8_t     cmd;
    uint8_t     flag;
    uint8_t     version_2_msb;
    uint8_t     version_1_lsb;
}DIAG_FRAME_RSP_HW_VERSION;

typedef union {
    uint8_t                         diagRawData[32];      /** A Diag-Frame always consists of 8 bytes */
    DIAG_CLIENT_FRAME               diagClientFrame;
    DIAG_SERVER_FRAME               diagServerFrame;
    DIAG_FRAME_REQ                  diagClientReqFrame;
    DIAG_FRAME_RSP                  diagServerResponseFrame;
    DIAG_FRAME_REQ_BACKLIGHT        diagClientReqBacklightFrame;
    DIAG_FRAME_RSP_SW_VERSION       diagServerResponseSWVersionFrame;
    DIAG_FRAME_RSP_HW_VERSION       diagServerResponseHWVersionFrame;
}DIAG_FRAME;

#define NO_INIT_DATA __no_init

void Diag_Dispatcher_TransmitComplete(void);
void Diag_Dispatcher_ReceiveFrame(uint8_t *ptr);
void Diag_Dispatcher_InsertFrame(const DIAG_FRAME *pFrameToSend);
void Diag_ProcessClientCommands(const DIAG_FRAME *msgFrame);

#endif