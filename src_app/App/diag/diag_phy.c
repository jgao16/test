

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#include <string.h>
#include "diag_phy.h"
#include "diag_internal.h"

#include "hal_hw.h"
#include "hal_uart.h"

#include "crc16.h"

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
/* DEFINE */

#define DIAG_UART_ID                                DIAG_ID

// PHY PACKET DEFINITION
#define PHY_PACKET_MAX_SIZE                         (32)

#define PHY_PACKET_START_BYTE                       (0xF7)
#define PHY_PACKET_START_BYTE_LEN                   (1)
#define PHY_PACKET_START_BYTE_OFFSET                (0)

#define PHY_PACKET_LEN_LEN                          (1)
#define PHY_PACKET_LEN_OFFSET                       (1)

#define PHY_PACKET_DATA_LEN(data_length)            (data_length)               
#define PHY_PACKET_DATA_OFFSET                      (2)

#define PHY_PACKET_CRC_LEN                          (2)
#define PHY_PACKET_CRC_OFFSET(data_length)          (2 + (data_length))

#define PHY_PACKET_STOP_BYTE                        (0xFE)
#define PHY_PACKET_STOP_BYTE_LEN                    (1)
#define PHY_PACKET_STOP_BYTE_OFFSET(data_length)    ((data_length) + 4)

// PHY PACKET BUFFER
#define PHY_PACKET_BUFF_SIZE                        (32)
#define PHY_PACKET_BUFF_GET_IDX(x)                  ((x) % PHY_PACKET_BUFF_SIZE) 


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
/* Structure and Enumberation*/
typedef struct
{
    uint8_t     wr_idx;
    uint8_t     buff[PHY_PACKET_BUFF_SIZE];
}phy_buff;

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
/* Global Variables */
static void         *phy_hdl;
phy_buff            phy_rx_buff = {0};
uint8_t             phy_tx_buff[PHY_PACKET_MAX_SIZE] = {0};

static void DiagPhy_TxCallback(void*arg);
static void DiagPhy_RxCallback (void*arg,unsigned char *rx_dat, unsigned short len);
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
/* Adaptoer Layer Function*/
void PHY_HAL_Init(void)
{

    phy_hdl = HalUart_GetDev(DIAG_UART_ID);
    if ( phy_hdl != NULL )
    {
        HalUart_SetParameter(phy_hdl,HAL_UART_BAUD_115200,HAL_UART_PARITY_NONE,HAL_UART_WORD_8_BITS, HAL_UART_STOP_1_BIT);
        HalUart_SetCalBk(phy_hdl,NULL, DiagPhy_RxCallback, DiagPhy_TxCallback, NULL, NULL, NULL);
        HalUart_InitDev(phy_hdl);
    }
    else {}
}

ePHY_SEND_STATE PHY_HAL_Send(const uint8_t *ptr, uint8_t len)
{
    HalUart_Send(phy_hdl, ptr, len);

    return SEND_PENDING;
}

void PHY_INVOKER_TxCallback()
{
    Diag_Dispatcher_TransmitComplete();
}

void PHY_INVOKER_RxCallback(uint8_t *ptr)
{
    // DIAG_FRAME *receivedFrame = (DIAG_FRAME *)ptr;

    Diag_Dispatcher_ReceiveFrame(ptr);              // Add the received frame to the RX-buffer
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
/* DiagPhy Functiones */
void DiagPhy_Init(void)
{
    /* Init the Packet Buffer */
    memset(&phy_rx_buff, 0, sizeof(phy_rx_buff));

    /* Customization Part */
    PHY_HAL_Init();

}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void DiagPhy_ResetConnection(void)
{
    /* Don't Do Stuff*/
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void DiagPhy_PollingFunc(void)
{
    /* Don't Do Stuff*/
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
ePHY_SEND_STATE DiagPhy_SendPacket(const uint8_t *ptr, uint8_t length)
{
    uint16_t crc16;

    /* Prepare the Packet */
    // Fill the strat byte
    phy_tx_buff[PHY_PACKET_START_BYTE_OFFSET] = PHY_PACKET_START_BYTE;

    // FIll the content
    for (uint8_t ii=0; ii < length; ii++)
    {
        phy_tx_buff[PHY_PACKET_LEN_OFFSET + ii] = ptr[ii];
    }

    // Fill the CRC with MSB filled first and LSB filled later
    crc16 = Crc16_Calc(&phy_tx_buff[PHY_PACKET_LEN_OFFSET], PHY_PACKET_LEN_LEN + length - 1);
    phy_tx_buff[PHY_PACKET_CRC_OFFSET(length - 1)] = (crc16 >> 8) & 0xFF;
    phy_tx_buff[PHY_PACKET_CRC_OFFSET(length)] = crc16 & 0xFF;

    // Fill the stop byte
    phy_tx_buff[PHY_PACKET_STOP_BYTE_OFFSET(length-1)] = PHY_PACKET_STOP_BYTE;

    /* Send the Packet */
    return PHY_HAL_Send(&phy_tx_buff[0], PHY_PACKET_STOP_BYTE_OFFSET(length - 1) + PHY_PACKET_STOP_BYTE_LEN);
}

/* Upper layer customization */
static void DiagPhy_TxCallback(void*arg)
{
    (void)arg;
    PHY_INVOKER_TxCallback();
}

static bool DiagPhy_SearchRxPacket()
{
    uint8_t tmp_rx_packet_buff[PHY_PACKET_MAX_SIZE - 1] = {0};
    bool    ret = false;

    // Sort the packet
    for (uint8_t ii=0; ii < PHY_PACKET_MAX_SIZE; ii++)
    {
        tmp_rx_packet_buff[ii] = phy_rx_buff.buff[PHY_PACKET_BUFF_GET_IDX(phy_rx_buff.wr_idx + 1 + ii)];
    }

    // Match the packet
    for (uint8_t ii=0; ii < (PHY_PACKET_MAX_SIZE - 1); ii++)
    {
        // Run further check starting from matched start byte
        if (tmp_rx_packet_buff[ii] != PHY_PACKET_START_BYTE)
        {
            continue;
        }
        else
        {
            uint8_t length = tmp_rx_packet_buff[ii + PHY_PACKET_LEN_OFFSET]; 

            // Check position
            if ((ii + PHY_PACKET_STOP_BYTE_OFFSET(length)) != (PHY_PACKET_MAX_SIZE - 1))
            {
                continue;
            }
            else
            {
                /* Check CRC */
                uint16_t crc16 = Crc16_Calc(&(tmp_rx_packet_buff[ii + PHY_PACKET_LEN_OFFSET]), PHY_PACKET_LEN_LEN + length); 

                if ((tmp_rx_packet_buff[ii + PHY_PACKET_CRC_OFFSET(length)] == ((crc16 >> 8) & 0xFF)) && \
                            (tmp_rx_packet_buff[ii + PHY_PACKET_CRC_OFFSET(length) + 1] == (crc16 & 0xFF)))
                {
                    PHY_INVOKER_RxCallback(&tmp_rx_packet_buff[ii + PHY_PACKET_LEN_OFFSET]);
                    ret = true;
                    break;
                }
                break;
            }
        }
    }

    return ret;

}

static void DiagPhy_RxCallback (void*arg,unsigned char *rx_dat, unsigned short len)
{

    /* Receive for each byte, and decide whether it is complete package */
    (void)arg;
    (void)rx_dat;

    if ( rx_dat != NULL /* && len > 0*/ )
    {
        for ( uint8 ii = 0; ii < len; ii ++ )
        {
            phy_rx_buff.buff[phy_rx_buff.wr_idx] = rx_dat[ii];

            /* Decide whether a full packet in the buffer */
            if (rx_dat[ii] == PHY_PACKET_STOP_BYTE)
            {
                (void)DiagPhy_SearchRxPacket();
            }
            else {}

            phy_rx_buff.wr_idx = PHY_PACKET_BUFF_GET_IDX(phy_rx_buff.wr_idx + 1);
        }
    }
}
/** >>>>>>>>>>>>>>>>>>>>>> end of file >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


