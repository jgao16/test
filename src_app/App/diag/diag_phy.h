/**********************************************************************************/
/* trace_phy.h                                                                    */
/* trace server  			                                                       */
/**********************************************************************************/
#ifndef TRACE_PHY_H_H__
#define TRACE_PHY_H_H__
//#pragma once

#include "stdint.h"
#include "diag_internal.h"

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
/** A structure describing the state of a Diag-Frame transmission */
typedef enum {
    SEND_OK = 0,     /** Transmission has successfully completed */
    SEND_FAILED,     /** Transmission failed */
    SEND_PENDING     /** Transmission is pending */
}ePHY_SEND_STATE;

#ifdef __cplusplus
extern "C" {
#endif

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
/* Function */
void                DiagPhy_Init(void);
void                DiagPhy_ResetConnection(void);
ePHY_SEND_STATE     DiagPhy_SendPacket(const uint8_t *ptr, uint8_t length);
void 			    DiagPhy_PollingFunc(void);

#ifdef __cplusplus
}
#endif

#endif // __gen_type_cf_type_h_h

