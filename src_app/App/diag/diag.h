#ifndef _DIAG_H_H_
#define _DIAG_H_H_

#include "std_type.h"

#define DIAG_ENABLE             (1)

void        Diag_SetEnable(boolean isEnable);
boolean     Diag_IsEnabledState(void);
void        Diag_SetClientConnected(boolean isConnected);
boolean     Diag_IsClientConnected(void);
void        Task_Diag( void const*p_arg );

#endif