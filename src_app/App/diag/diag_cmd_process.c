#include "diag_internal.h"
#include "diag.h"

#include "ver_info.h"


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
/* Sturcture and Enums */

#define DIAG_CMD_REQ_START                  (0x10U)       
#define DIAG_CMD_RSP_START                  (0x30U)       
#define DIAG_FRAME_RSP_START_LEN            (0x02U)

#define DIAG_CMD_REQ_STOP                   (0x11U)       
#define DIAG_CMD_RSP_STOP                   (0x31U)       
#define DIAG_FRAME_RSP_STOP_LEN             (0x02U)

#define DIAG_CMD_REQ_PWM_BACKLIGHT          (0x12U)       
#define DIAG_CMD_RSP_PWM_BACKLIGHT          (0x32U)       
#define DIAG_FRAME_RSP_PWM_BACKLIGHT_LEN    (0x02U)       


#define DIAG_CMD_REQ_SW_VERSION             (0x13U)       
#define DIAG_CMD_RSP_SW_VERSION             (0x33U)       
#define DIAG_FRAME_RSP_SW_VERSION_LEN       (0x06U)       

#define DIAG_CMD_REQ_HW_VERSION             (0x14U)       
#define DIAG_CMD_RSP_HW_VERSION             (0x34U)       
#define DIAG_FRAME_RSP_HW_VERSION_LEN       (0x04U)       

#define DIAG_CMD_REQ_TOUCH                  (0x15U)       
#define DIAG_CMD_RSP_TOUCH                  (0x35U)       
#define DIAG_FRAME_RSP_TOUCH_LEN            (0x16U)       

#define DIAG_CMD_REQ_SCREEN_ON              (0x16U)       
#define DIAG_CMD_RSP_SCREEN_ON              (0x36U)       
#define DIAG_FRAME_RSP_SCREEN_ON_LEN        (0x02U)       

#define DIAG_CMD_REQ_SCREEN_OFF             (0x17U)       
#define DIAG_CMD_RSP_SCREEN_OFF             (0x37U)       
#define DIAG_FRAME_RSP_SCREEN_OFF_LEN       (0x02U)       

#define DIAG_RSP_FLAG_SUCCESS               (0x01)
#define DIAG_RSP_FLAG_FAILURE               (0x00)
#define DIAG_FRAME_RSP_FAILURE_LEN          (0x02)

#define DIAG_CMD_PROCESS_MAX_TIMEOUT_TICK   (10000)


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
/* Global Variables */


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
/* Functions */

static void DiagProc_Start(const const DIAG_FRAME *msgFrame)
{
    DIAG_FRAME rspFrame;

    rspFrame.diagServerResponseFrame.len    = DIAG_FRAME_RSP_START_LEN;
    rspFrame.diagServerResponseFrame.cmd    = DIAG_CMD_RSP_START;
    rspFrame.diagServerResponseFrame.flag   = DIAG_RSP_FLAG_SUCCESS;

    Diag_Dispatcher_InsertFrame(&rspFrame);
    Diag_SetClientConnected(TRUE);

}

static void DIagProc_Stop(const DIAG_FRAME *msgFrame)
{
    DIAG_FRAME rspFrame;

    rspFrame.diagServerResponseFrame.len    = DIAG_FRAME_RSP_STOP_LEN;
    rspFrame.diagServerResponseFrame.cmd    = DIAG_CMD_RSP_STOP;
    rspFrame.diagServerResponseFrame.flag   = DIAG_RSP_FLAG_SUCCESS;

    Diag_Dispatcher_InsertFrame(&rspFrame);
    Diag_SetClientConnected(FALSE);

}

static void DIagProc_PWM_Backlight(const DIAG_FRAME *msgFrame)
{
    DIAG_FRAME rspFrame;
    boolean     status = FALSE;

    rspFrame.diagServerResponseFrame.len    = DIAG_FRAME_RSP_PWM_BACKLIGHT_LEN;
    rspFrame.diagServerResponseFrame.cmd    = DIAG_CMD_RSP_PWM_BACKLIGHT;

    if (msgFrame->diagClientReqBacklightFrame.pwm <= 100)
    {
        // TODO: Set backlight and check the result
    }
    else
    {
        status = FALSE;

    }

    if (status == FALSE)
    {
        rspFrame.diagServerResponseFrame.flag   = DIAG_RSP_FLAG_FAILURE;
    }
    else
    {
        rspFrame.diagServerResponseFrame.flag   = DIAG_RSP_FLAG_FAILURE;
    }

    Diag_Dispatcher_InsertFrame(&rspFrame);
}

static void DIagProc_SW_Version(const DIAG_FRAME *msgFrame)
{
    DIAG_FRAME rspFrame;
    uint32_t     version = VerInfo_GetSWVersion();

    rspFrame.diagServerResponseSWVersionFrame.len    = DIAG_FRAME_RSP_SW_VERSION_LEN;
    rspFrame.diagServerResponseSWVersionFrame.cmd    = DIAG_CMD_RSP_SW_VERSION;
    rspFrame.diagServerResponseSWVersionFrame.flag   = DIAG_RSP_FLAG_SUCCESS;

    rspFrame.diagServerResponseSWVersionFrame.version_4_msb = (version >> 24) & 0xFF;
    rspFrame.diagServerResponseSWVersionFrame.version_3     = (version >> 16) & 0xFF;
    rspFrame.diagServerResponseSWVersionFrame.version_2     = (version >> 8) & 0xFF;
    rspFrame.diagServerResponseSWVersionFrame.version_1_lsb = (version) & 0xFF;

    // TODO: Fill the software version
    Diag_Dispatcher_InsertFrame(&rspFrame);
}

static void DIagProc_HW_Version(const DIAG_FRAME *msgFrame)
{
    DIAG_FRAME rspFrame;
    uint16_t     version = VerInfo_GetHWVersion();

    rspFrame.diagServerResponseHWVersionFrame.len    = DIAG_FRAME_RSP_HW_VERSION_LEN;
    rspFrame.diagServerResponseHWVersionFrame.cmd    = DIAG_CMD_RSP_HW_VERSION;
    rspFrame.diagServerResponseHWVersionFrame.flag   = DIAG_RSP_FLAG_SUCCESS;

    rspFrame.diagServerResponseHWVersionFrame.version_2_msb = (version >> 8) & 0xFF;
    rspFrame.diagServerResponseHWVersionFrame.version_1_lsb = (version) & 0xFF;

    // TODO: Fill the hardware version
    Diag_Dispatcher_InsertFrame(&rspFrame);
    
}

static void DIagProc_Touch(const DIAG_FRAME *msgFrame)
{
    DIAG_FRAME rspFrame;
    boolean     status = FALSE;
    // uint32_t FrozenSystemTime_u32 = HalOS_GetKernelTick();      // Get the current time to decide whether there is touch inforamtion got

    rspFrame.diagServerResponseFrame.len    = DIAG_FRAME_RSP_TOUCH_LEN;
    rspFrame.diagServerResponseFrame.cmd    = DIAG_CMD_RSP_TOUCH;
    rspFrame.diagServerResponseFrame.flag   = DIAG_RSP_FLAG_SUCCESS;

    // TODO: Add touch information. Need consider to check the timeout. If the timeout exceed the expected timeout, return false;
    // while ((HalOS_GetKernelTick() - FrozenSystemTime_u32) < DIAG_CMD_PROCESS_MAX_TIMEOUT_TICK)
    // {
    // }
    
    if (status == FALSE)
    {
        rspFrame.diagServerResponseFrame.flag   = DIAG_RSP_FLAG_FAILURE;
    }
    else
    {
        rspFrame.diagServerResponseFrame.flag   = DIAG_RSP_FLAG_FAILURE;
    }
    Diag_Dispatcher_InsertFrame(&rspFrame);
}

static void DIagProc_ScreenOn(const DIAG_FRAME *msgFrame)
{
    DIAG_FRAME rspFrame;
    boolean     status = FALSE;

    rspFrame.diagServerResponseFrame.len    = DIAG_FRAME_RSP_SCREEN_ON_LEN;
    rspFrame.diagServerResponseFrame.cmd    = DIAG_CMD_RSP_SCREEN_ON;

    // TODO On the scren

    if (status == FALSE)
    {
        rspFrame.diagServerResponseFrame.flag   = DIAG_RSP_FLAG_FAILURE;
    }
    else
    {
        rspFrame.diagServerResponseFrame.flag   = DIAG_RSP_FLAG_FAILURE;
    }
    Diag_Dispatcher_InsertFrame(&rspFrame);
    
}

static void DIagProc_ScreenOff(const DIAG_FRAME *msgFrame)
{
    DIAG_FRAME rspFrame;
    boolean     status = FALSE;

    rspFrame.diagServerResponseFrame.len    = DIAG_FRAME_RSP_SCREEN_OFF_LEN;
    rspFrame.diagServerResponseFrame.cmd    = DIAG_CMD_RSP_SCREEN_OFF;

    // TODO On the scren

    if (status == FALSE)
    {
        rspFrame.diagServerResponseFrame.flag   = DIAG_RSP_FLAG_FAILURE;
    }
    else
    {
        rspFrame.diagServerResponseFrame.flag   = DIAG_RSP_FLAG_FAILURE;
    }
    Diag_Dispatcher_InsertFrame(&rspFrame);
    
}

void Diag_ProcessClientCommands(const DIAG_FRAME *msgFrame)
{
    DIAG_FRAME_REQ *prtClientFrame = (DIAG_FRAME_REQ *)msgFrame;

    switch (prtClientFrame->cmd)
    {
        case DIAG_CMD_REQ_START:
            DiagProc_Start(msgFrame);
            break;

        case DIAG_CMD_REQ_STOP:
            DIagProc_Stop(msgFrame);
            break;

        case DIAG_CMD_REQ_PWM_BACKLIGHT:
            DIagProc_PWM_Backlight(msgFrame);
            break;

        case DIAG_CMD_REQ_SW_VERSION:
            DIagProc_SW_Version(msgFrame);
            break;

        case DIAG_CMD_REQ_HW_VERSION:
            DIagProc_HW_Version(msgFrame);
            break;

        case DIAG_CMD_REQ_TOUCH:
            DIagProc_Touch(msgFrame);
            break;

        case DIAG_CMD_REQ_SCREEN_ON:
            DIagProc_ScreenOn(msgFrame);
            break;

        case DIAG_CMD_REQ_SCREEN_OFF:
            DIagProc_ScreenOff(msgFrame);
            break;

        default:
            // TODO: UNKONWN Command
            // assert(0);
            break;
    }

}

