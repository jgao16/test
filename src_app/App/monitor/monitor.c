#include "monitor.h"
#include "std_type.h"
#include "serv.h"
#include "hal_os.h"

#include "backlight.h"
#include "panel.h"
#include "temperate.h"
#include "bat_mana.h"

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

static void Monitor_Task(void);

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void Monitor_Init(void)
{
    ServFunc_StartCyclic(ServFunc_Create(Monitor_Task, "Monitor Task"), 5u, 10);
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static  void Monitor_Task(void)
{
    uint32 cpu_sr;
    uint8_t allowed_pwm;
    lcd_power_state_t lcd_power_state;

    cpu_sr = osEnterCritical();

    lcd_power_state = LCD_GetPowerState();

    if (BAT_NORMAL != BatMana_GetBatState())
    {
        if (LCD_POWER_ON == lcd_power_state)
        {
            LCD_PowerOn(FALSE);
        }

        allowed_pwm = 0;
    }
    else
    {
        if (LCD_POWER_OFF == lcd_power_state)
        {
            LCD_PowerOn(TRUE);
        }

        allowed_pwm = Temperate_GetAllowedPwm();
    }

    BackLight_SetAllowedPwm(allowed_pwm);

    osExitCritical(cpu_sr);
}
