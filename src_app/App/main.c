
/* Standard includes. */
#include "std_type.h"
#include "cmsis_os.h"
#include "serv.h"

#include "hal_hw.h"

#include "hal_clk.h"
#include "hal_rtc.h"

#include "hal_uart.h"
#include "hal_gpio.h"

//#include "hal_key.h"
//#include "matric_key.h"
//#include "eeprom.h"

#include "board_hw.h"

#include "std_lib.h"

#include "hal_int.h"
#include "hal_adc.h"
#include "bsp_gpio.h"
//#include "bsp_audio.h"
//#include "bsp_serdes.h"
#include "map_mana.h"
#include "bat_mana.h"
#include "temperate.h"
//#include "panel.h"
//#include "swc.h"
//#include "dvr.h"
#include "dev_pwr.h"
// #include "dev_ipc.h"
//#include "cf_kernel.h"
#include "trace_api.h"
#include "diag.h"
//#include "Trace_Handler.h"

#include "gen_eep_mana.h"
#include "bsp_misc.h"
#include "log_cmd.h"
#include "backlight.h"
#include "ver_info.h"
//#include "ap_state.h"
// MDL
//#include "MDL_OBC_Sys.h"
//#include "MDL_ODO_Sys.h"
//#include "MDL_Speed_Sys.h"
//#include "MDL_Fuel_Sys.h"
//#include "MDL_Tacho_Sys.h"
//#include "MDL_Temperature_Sys.h"
//#include "MDL_GearBox_Sys.h"
//#include "MDL_TestTask_Sys.h"
//#include "MDL_Drive_Sys.h"
//#include "MDL_IdleStop_Sys.h"
//#include "MDL_ECO_Sys.h"
//#include "MDL_ANA_Sys.h"
//#include "MDL_FuelCan_Sys.h"
//#include "Buzzer.h"
//#include "MeterOutput.h"
//#include "MeterWarning.h"
//#include "MeterHMI.h"
//
//#include "CanTask.h"
//
//#include "meter_speed.h"
//#include "meter_odo.h"
//#include "meter_temperature.h"
//#include "meter_at_position.h"
//#include "meter_obc.h"
//#include "meter_tacho.h"
//#include "meter_fuel.h"
//#include "meter_dte.h"
//#include "GSI_model.h"
//
//#include "ComRTE.h"
#include "power_mode.h"
#include "ver_info.h"
//#include "power.h"
#include "monitor.h"
#include "panel.h"
#include "tp_iic.h"
#include "host_iic.h"
#include "bootloader.h"
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
/* for system event service 	*/
//static  uint8	mp_50ms_cnt;
static	uint8	tick_cnt_2ms;
static	uint16	tick_cnt_500ms;

static	uint16	s_u2_cpuTickCnt;
static	uint16	s_u2_cpuLoadCnt;
static	uint16	s_u2_cpuLoadPerSession;

//static 	SERV_WDT *	app_wdt;

static	SERV_EVENT	*sys_evt;
static	uint8		sys_evt_cod_e_buf[64];

static 	boolean	eep_initial_compl;

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static	void	CalcCpuLoad ( void );
//static  void 	Sys500ms_Callback(void);

static  void 	AppTaskStart(void const *argument);
static osThreadDef(AppStart, 	AppTaskStart, osPriorityHigh, 0, 192);
static void		AppWriteResetInfo(void);

#if LOG_ENABLE    
/* for trace task	*/
static osThreadDef(TraceServ, 	Task_Trace,   osPriorityNormal, 0, 192);
#endif
#if DIAG_ENABLE    
/* for diagnostic task	*/
static osThreadDef(DiagServ, 	Task_Diag,   osPriorityNormal, 0, 192);
#endif
//osThreadDef(TraceServ, 	Task_Trace,   osPriorityAboveNormal, 0, 192);
//static void	OsStatusMonitor(void);

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/



/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void vApplicationTickHook( void )
{	
	/* system 1ms tick */
	MapMana_1msFunc();

	
	POWER_MODE power_mode = PwrCtrl_GetPwrMode();
	if ( power_mode == POWER_MODE_HIGH )
	{
		tick_cnt_2ms++;
		if (tick_cnt_2ms >= (configTICK_RATE_HZ / SERV_TM_TICK_PER_SEC)) 
		{
			/* send system 10ms event */
			if ( ServEvt_Post(sys_evt, EVENT_SYSTEM_2MS_TIMER) )
			{
				tick_cnt_2ms -= (configTICK_RATE_HZ / SERV_TM_TICK_PER_SEC);
			}
		}

		tick_cnt_500ms++;
		if (tick_cnt_500ms >= (configTICK_RATE_HZ / SERV_500MS_TICK_PER_SEC))
		{
			if(ServEvt_Post(sys_evt, EVENT_SYSTEM_REALTIME_500MS))
			{
				tick_cnt_500ms -= (configTICK_RATE_HZ / SERV_500MS_TICK_PER_SEC);
			}
		}
		/* calc cpu load 		*/
		CalcCpuLoad();
		/* power control hp function */
		PwrCtrl_1msHpFunc();		
	}

	/* adc 1ms conversion function */
	HalAdc_CycleConvert();

#if 0
	CanTask_1msTick();
#endif
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void vApplicationStackOverflowHook( TaskHandle_t pxTask, char *pcTaskName )
{
	( void ) pcTaskName;
	( void ) pxTask;

	/* Run time stack overflow checking is performed if
	configconfigCHECK_FOR_STACK_OVERFLOW is defined to 1 or 2.  This hook
	function is called if a stack overflow is detected. */
	for( ;; );	//lint !e722
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void vApplicationMallocFailedHook( void )
{
	/* Called if a call to pvPortMalloc() fails because there is insufficient
	free memory available in the FreeRTOS heap.  pvPortMalloc() is called
	internally by FreeRTOS API functions that create tasks, queues or
	semaphores. */
	for( ;; );	//lint !e722
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void vApplicationIdleHook( void )
{
	/* Called on each iteration of the idle task.  In this case the idle task
	just enters a low(ish) power mode. */
	//PWR_EnterSleepMode( PWR_Regulator_ON, PWR_SLEEPEntry_WFI );
	if( eep_initial_compl ){
		EepMana_IdleTask();
	}
	// VerInfo_MainFunc();
	PwrCtrl_IdleFunc();
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
SERV_EVENT * Sys_GetServEvt(void)
{
	return sys_evt;
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
uint8_t 	AppGet_CpuLoad(void)
{
	return (uint8_t)((s_u2_cpuLoadPerSession+20)/40);
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void main( void )
{ 
	/* misc initial */
	BspMisc_Init();

	/* CPU time stamp init					*/
	CPU_TS_INIT();	
	
	/*Hal interrupt initial :this need to be done firstly */
	HalInt_Init();
	/* initial gpio */
	HalGpio_Init();
	/* initial gpio first */
	BspGpio_Init();

	//SystemInit();
	/* initial cpu clock */
	HalClk_CpuClkInit();

	/* map manager initial */
	MapMana_Init();

	(void)osKernelStart(&os_thread_def_AppStart,NULL);
	/* If all is well then this line will never be reached.  If it is reached
	then it is likely that there was insufficient (FreeRTOS) heap memory space
	to create the idle task.  This may have been trapped by the malloc() failed
	hook function, if one is configured. */
	for( ;; );	//lint !e722
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
//static void * rs232_hdl;

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void AppTaskStart(void const *argument)
{	
	/* hal initial, api and hw 	*/
	HalHw_Init();
	/* service event,time event	*/
	Serv_Init();
	/* service wdt 				*/
	ServWdt_init();


	/* trace service initial				*/
	TRACE_API_INIT();
	TRACE_VALUE(LOG_SHOUT, MOD_ALWAYS, "Tis Log is N331 DA Master Log., wakeup reason=%d",BspMisc_GetWakeUpReason());
	WakeUpHwReg_T reg;
	BspMisc_GetWakeUpHwReg(&reg);
	TRACE_VALUE2(LOG_SHOUT, MOD_ALWAYS, "Wake up Hw reg: SRS=0x%04x,wdt_id=%d", reg.RCM_SRS, reg.wdt_id);
	/* Ap size state pin monitor */
	//ApState_Init();
	/* eeprom initial						*/
	EepMana_Init();
	MapMana_RecordResetInfo(SOC_POR_RESET);	// record soc reset info
	
	eep_initial_compl = TRUE;

    /* reset trace module mask after eeprom is initial  */
	TRACE_RESTORE_MODULE_LVL_FROM_EEP();
#if LOG_ENABLE    
	/* create trace task for trace server	*/
	(void)osThreadCreate(&os_thread_def_TraceServ,NULL);	// if need high power mode, create this thread
#endif

#if DIAG_ENABLE
	/* create trace task for trace server	*/
	(void)osThreadCreate(&os_thread_def_DiagServ,NULL);	// if need high power mode, create this thread
#endif
	
	BoardHw_Init();
	PwrCtrl_Init(BoardHw_GetPwrCtrlApi());

	
	/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> application initial start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
	/* init can task, however not start in this function */
	//CanTask_Init();
	/* disable middle power gage			*/
	PwrCtrl_DisMpSleepGate(MP_PWR_GATE_APP_MODE);
	/* dev ipc initial */
	// DevIpc_Init();
	/* c-framework initial	*/
	//CfKernel_Init();
	/* screen backlight 	*/
	BackLight_Init();

	/* Serializer and deserializer management init */
	//BspSerdes_Init();
	/* battery management init */
	BatMana_Init();

	/* bsp audio initial */
	//BspAudio_Init();

	/* Panel initial */
	//Panel_Init();

	/* Swc initial */
	//SwcKey_Init();

	/* DVR initial */
	//Dvr_Init();

	/* device power initial */
	DevPower_Init();

	/* log received command initial */
#if LOG_ENABLE
	LogCmd_Init();
#endif

	/* Temperate management init */
	Temperate_Init();
	/* version info check initial */
	VerInfo_Init();

	// added by xmu2
	//MDL_FuelCan_Init();
//    MDL_Drive_Init();
	//MDL_Test_Init();
//	MDL_GearBox_Init();
//	MDL_Tacho_Init();
	//MDL_Temperature_Init();

	//MDL_Fuel_Init();
	//MDL_Speed_Init();
	//MDL_Odo_Init();
	//MDL_OBC_Init();
	//MDL_IdleStop_Init();
	//MDL_ANA_Init();
	

#if 0
	MeterWarning_Init();
	MeterHMI_Init();
	MeterOBC_Init();
	MeterSpeed_Init();/*speed */
	MeterOdo_Init();
	MeterTemperature_Init();
	MeterAtPos_Init();
	//Engine round speed init.
	MeterTacho_Init();
	MeterOutput_Init();
	//Fuel module init.
	MeterFuel_Init();
	MeterDTE_Init();
	GSI_model_initialize();
#endif
    Monitor_Init();

    PANEL_Init();

    TP_Init();

    BOOTLOADER_Init();

    /* HOST I2C. */
    HOST_I2C_Init();

	/* set 500ms callback					*/
	//HalRtc_SetCycCallback(Sys500ms_Callback);
	//ServFunc_StartCyclic (	ServFunc_Create (Sys500ms_Callback, "500ms system callback" ), 
							//1, 500/10);

	/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> application initial end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
	/* force pet watchdog */
	ServWdt_ForcePetWdt();
	
	/* create timer for wdt */
	ServFunc_StartCyclic (	ServFunc_Create (ServWdt_monitor, "Wdt Manager Func" ), 
							1, WDT_MONITOR_TIMER/2);
	//app_wdt	=	ServWdt_Create(1000,"App Watch Dog");	/* create task watch dog 									*/
	//if ( app_wdt == NULL ){
	//	(void)osThreadTerminate(osThreadGetId());
	//}
	//ServWdt_start(app_wdt);
	
	/* create system event service	*/
	sys_evt	=	ServEvt_Create(sys_evt_cod_e_buf,sizeof(sys_evt_cod_e_buf), "System Event" );

	/* force pet watchdog 			*/
	ServWdt_ForcePetWdt();	
	/*change system task to low 	*/
	(void)osThreadSetPriority(osThreadGetId(),osPriorityNormal);

	//(void)osDelay(1000);
	/* flush system */
	ServEvt_Flush(sys_evt);
	
	/* enable middle sleep*/
	PwrCtrl_EnMpSleepGate(MP_PWR_GATE_APP_MODE);

	AppWriteResetInfo();
	
	/* open IC device signal for meter device signal  */
	//DevIpc_Open(IC_DEV_SIG);

	/*ComRTE init*/
//	ComRTE_Init();

	/*power mode init*/
	PowerMode_Init();

	TRACE_TEXT(LOG_SHOUT, MOD_ALWAYS, "Tis Log is N331 DA Master Log,App initial complete");

	while(1)	//lint !e716
	{             
		uint8 ev_code;
		/* wait a event	*/
		ev_code	=	ServEvt_Wait(sys_evt, HAL_OS_WaitForever);
		uint32 exec_start = HalOS_GetKernelTick();
		switch ( ev_code )
		{
		case EVENT_SYSTEM_2MS_TIMER:
			/* reset watch dog 					*/
			//ServWdt_reset(app_wdt); 			
			/* service fuction, call task 		*/
			ServFunc_CycCallTask();
			/* service timer call task			*/
			//ServTmr_CycCallTask();
			/* hal gpio 10ms function			*/
			BspGpio_MainFunc();
			break;

		case EVENT_SYSTEM_REALTIME_500MS:
			//tp_iic_CHG_test();
			//ServFunc_Realtime500msCycCallTask();
			//PwrCtrl_EnLpSleepGlobal();
			//OsStatusMonitor();	// monitor os status
			//TRACE_TEXT(TRACE_TEST, MOD_ALWAYS, "Time 500ms occured");
			break;
			
		case EVENT_SYSTEM_OUTPUT_CPU_LOAD:
			TRACE_VALUE(CPU_USAGE, MOD_OS, "############ Cpu Load: %u%%", (s_u2_cpuLoadPerSession+20)/40);			
			break;

		/* for event 50ms in low power uart enable	*/
		case EVENT_SYSTEM_MIDDLE_POWER_50MS:
			/* reset watch dog 					*/
			//ServWdt_reset(app_wdt);
			//mp_50ms_cnt++;
			//if ( mp_50ms_cnt & 0x01 ){
			//	ServWdt_monitor();
			//}
			//ServFunc_MpCycCallTask();
			break;

		case EVENT_SYSTEM_LOW_POWER_500MS:
			//mp_50ms_cnt = 0x01;
			/* reset watch dog 					*/
			//ServWdt_reset(app_wdt);
			//ServWdt_monitor();
			//ServFunc_LpCycCallTask();
			//PwrCtrl_EnLpSleepGlobal();
			break;
			
		default:
			/* may be a event function			*/
			ServEvtFunc_CallFunc(ev_code);
			break;
		}
		uint32 exec_intv_tm = HalOS_GetKernelTick() - exec_start;
		if ( exec_intv_tm >= 10 ){
			TRACE_VALUE2(LOG_SHOUT,MOD_OS,"Serv Func execute too long, Tm = %dms,ev_code=%d", (uint16)exec_intv_tm,ev_code);
		}
	}
}


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static	void	CalcCpuLoad ( void )
{
	if ( osThreadGetId() != xTaskGetIdleTaskHandle() ){
		s_u2_cpuLoadCnt	++;
	}
	if ( s_u2_cpuTickCnt > 0 ){
		s_u2_cpuTickCnt --;
	}
	if ( s_u2_cpuTickCnt == 0 ){
		/* calc cpu load per 10s	*/
		s_u2_cpuTickCnt			=	4000;
		s_u2_cpuLoadPerSession	=	s_u2_cpuLoadCnt;	
		s_u2_cpuLoadCnt			=	0;
		/* send system 10ms event 	*/
		(void)ServEvt_Post(sys_evt, EVENT_SYSTEM_OUTPUT_CPU_LOAD);
	}
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if 0 
static void Sys500ms_Callback(void)
{
	if ( ServEvt_Post(sys_evt, EVENT_SYSTEM_REALTIME_500MS) )
	{	/* disable sleep */
		PwrCtrl_DisLpSleepGlobal();
	}
	
	if ( POWER_MODE_LOW == PwrCtrl_GetPwrMode() )
	{
		if ( ServEvt_Post(sys_evt, EVENT_SYSTEM_LOW_POWER_500MS) )
		{
			/* disable sleep */
			PwrCtrl_DisLpSleepGlobal();
		}
	}
}
#endif
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void		AppWriteResetInfo(void)
{
	uint32_t total_reset = GenEep_Get_TotalReset_Cnt();
	total_reset++;
	GenEep_Set_TotalReset_Cnt(total_reset);
	/* inital all cpu ts count */
	CpuTs_ReinitCnt();
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
/*static void	OsStatusMonitor(void)
{
	uint16 temp;
	
	temp = CpuTs_GetDisIntMaxTm();
	if ( temp > GenEep_Get_CpuDisIntMaxTm() )
	{
		GenEep_Set_CpuDisIntMaxTm(temp);

		TRACE_VALUE(LOG_SHOUT, MOD_OS, "OS disable interrupt tm max change to %dus", temp);
	}

	temp = CpuTs_GetDisIntNestMax();
	if ( temp > GenEep_Get_CpuDisIntMaxNest() )
	{
		GenEep_Set_CpuDisIntMaxNest(temp);

		TRACE_VALUE(LOG_SHOUT, MOD_OS, "OS disable interrupt nest max change to %d", temp);
	}

	temp = CpuTs_GetDisIntLongCnt();
	if( temp < GenEep_Get_CpuDisIntTooLongCnt() )
	{
		GenEep_Set_CpuDisIntTooLongCnt(temp);
		TRACE_VALUE(LOG_SHOUT, MOD_OS, "OS disable interrupt too long count = %d", temp);
	}
}*/



