#include "tp_iic.h"
#include "bsp_gpio.h"
#include "hal_gpio.h"
#include "serv.h"
#include "gpio.cfg"
#include "Hal_os.h"
#include "crc8.h"
#include "trace_api.h"
#include "hal_iic.h"
#include "hal_iic_slv.h"
#include "host_iic.h"
#include "panel.h"
#include "ver_info.h"

#define TP_IIC_DRV_BUFFER_MAX_BYTE 2
#define TP_IIC_ADDRESS_LSB         0x00
#define TP_IIC_ADDRESS_MSB         0x00
#define TP_IIC_T5_BUFFER_SIZE      200
#define TOUCH_MSG_QUENE_LEN 10

static uint8 tp_iic_drv_buffer_u8[TP_IIC_DRV_BUFFER_MAX_BYTE];
static uint8 tp_iic_T5_buffer_u8[TP_IIC_T5_BUFFER_SIZE];
static uint8 tp_iic_T44_address_buffer_u8[TP_IIC_DRV_BUFFER_MAX_BYTE];

static IIC_MSG tp_iic_handler_iic_struct;

static uint8 T44_LSB = 0;
static uint8 T44_MSB = 0;
static uint8 T5_size = 0;

static boolean isTpInited = FALSE;
static boolean iicXferOngoing = FALSE;
static uint8 T100_object_id_end = 0;
static uint8 T100_object_id_start = 0;
static uint16 TP_IIC_INFO_Timer = 0;
static	HAL_OS_SEM_ID 	CHG_isr_sem;
static	HAL_OS_SEM_ID 	s_semDataSentToHost;
static 	osMessageQId    QTouchId;
static  osMessageQDef_t TouchDef = { (uint32_t)10, (uint32_t)sizeof(touch_msgType)};
uint8_t tp_info_blk[TP_INFO_BLK_SIZE];

touch_msgType msgGetFromQ, msgPutToQ, msgForHost;

//static uint8 test_buf[100];

void Touch_Thread(void const *arg);
void SendSOC_Thread(void const *arg);
static osThreadDef(TouchEvent, Touch_Thread, osPriorityNormal, 0, 256);
static osThreadDef(SendSOCEvent, SendSOC_Thread, osPriorityNormal, 0, 256);
void Get_TP_Info_2ms(void);

static uint32_t crc24(uint32_t crc, uint8_t firstbyte, uint8_t secondbyte)
{
    static const uint32_t crcpoly = 0x80001B;
    uint32_t result;
    uint32_t data_word;

    data_word = (uint16_t)firstbyte | (uint16_t)((uint16_t)secondbyte << 8U);
    result = (uint32_t)data_word ^ (uint32_t)(crc << 1U);

    if (result & 0x1000000)
    {
        result ^= crcpoly;
    }

    return result;
}

static boolean tp_verify_info_blk(void)
{
    /* Checksum verify. */
    uint32_t crc = 0;
    uint32_t data_size = TP_INFO_BLK_SIZE - TP_CRC_SIZE;
    uint32_t desiredCRC = (tp_info_blk[TP_INFO_BLK_SIZE - 1] << 16) |
                          (tp_info_blk[TP_INFO_BLK_SIZE - 2] << 8)  |
                          (tp_info_blk[TP_INFO_BLK_SIZE - 3] << 0);

    uint8_t *data = tp_info_blk;

    while (data_size > 1)
    {
        crc = crc24(crc, data[0], data[1]);
        data += 2;
        data_size -= 2;
    }

    if (data_size == 1)
    {
        crc = crc24(crc, *data, 0);
    }

    crc = crc & 0xFFFFFF;

    if (crc == desiredCRC)
    {
        return TRUE;
    }
    else
    {
        return FALSE;
    }
}

static void tp_iic_handler_receive_data(uint8* p_data_ptr_U8P, uint8* p_rec_ptr_U8P, uint16 transmit_length, uint16 receive_length)
{
    tp_iic_handler_iic_struct.rcv_dat = p_rec_ptr_U8P;
    tp_iic_handler_iic_struct.rcv_len = receive_length;
    tp_iic_handler_iic_struct.snd_dat = p_data_ptr_U8P;
    tp_iic_handler_iic_struct.snd_len = transmit_length;
    HalIic_SndRcv(HalIic_GetDev(TFT_ID), &tp_iic_handler_iic_struct);
}

static void handle_read_init_info(void)
{
    uint8 object_id = 0;
    tp_id_info_t *id_info;
    object_info_t *obj_info;

    if (tp_verify_info_blk() == TRUE)
    {
        id_info = (tp_id_info_t *)(&tp_info_blk[0]);

        VerInfo_UpdateTpFirmwareVersion(id_info->version, id_info->build);

        obj_info = (object_info_t *)(&tp_info_blk[TP_ID_INFO_SIZE]);

        for(uint8 fl_index_U8 = 0; fl_index_U8 < TP_OBJ_NUM; ++fl_index_U8)
        {
            object_id += (uint8_t)((obj_info[fl_index_U8].instance + 1) * obj_info[fl_index_U8].numberReportId);
            if(obj_info[fl_index_U8].type == 44)
            {
                T44_LSB = obj_info[fl_index_U8].startLSByte;
                T44_MSB = obj_info[fl_index_U8].startMSByte;
                tp_iic_T44_address_buffer_u8[0] = T44_LSB;
                tp_iic_T44_address_buffer_u8[1] = T44_MSB;
            }
            else if(obj_info[fl_index_U8].type == 5)
            {
                T5_size = obj_info[fl_index_U8].size;
            }
            else if(obj_info[fl_index_U8].type == 100)
            {
                T100_object_id_end = object_id;
                T100_object_id_start = object_id - (uint8_t)((obj_info[fl_index_U8].instance + 1) * obj_info[fl_index_U8].numberReportId) + 1;
            }
        }

        isTpInited = TRUE;

        TRACE_TEXT(LOG_INFO, MOD_TOUCH_IIC, "Touch IC info read finished");
    }
    else
    {
        TRACE_TEXT(LOG_ERR, MOD_TOUCH_IIC, "Touch IC info block read error");
    }
}

static void handle_read_T5_info(void)
{
    //is_up_down up_down_flag = {FALSE, FALSE, FALSE, FALSE, FALSE};
    //memset(&msgPutToQ, 0, sizeof(touch_msgType));
    msgPutToQ.touchStatus1 = 0;

    for(uint8 fl_index_U8 = 1; fl_index_U8 < (T5_size * tp_iic_T5_buffer_u8[0]); )
    {
        if((tp_iic_T5_buffer_u8[fl_index_U8] >= (T100_object_id_start + 2)) && (tp_iic_T5_buffer_u8[fl_index_U8] <= T100_object_id_end))
        {
            if(tp_iic_T5_buffer_u8[fl_index_U8] == (T100_object_id_start + 2))
            {
                msgPutToQ.touch_points[0].XLSB = tp_iic_T5_buffer_u8[fl_index_U8+2];
                msgPutToQ.touch_points[0].XMSB = tp_iic_T5_buffer_u8[fl_index_U8+3];
                msgPutToQ.touch_points[0].YLSB = tp_iic_T5_buffer_u8[fl_index_U8+4];
                msgPutToQ.touch_points[0].YMSB = tp_iic_T5_buffer_u8[fl_index_U8+5];
                msgPutToQ.touch_points[0].CRC8 = crc8_Calc(&tp_iic_T5_buffer_u8[fl_index_U8+2], 4);
                if((tp_iic_T5_buffer_u8[fl_index_U8 + 1] & 0xF) == 4)
                {
                    msgPutToQ.touchStatus1 |= BIT0;
                }
                else if((tp_iic_T5_buffer_u8[fl_index_U8 + 1] & 0xF) == 5)
                {
                    msgPutToQ.touchStatus1 &= ~BIT0;
                }
                else if((tp_iic_T5_buffer_u8[fl_index_U8 + 1] & 0xF) == 9)
                {
                    msgPutToQ.touchStatus1 &= ~BIT0;
                    //up_down_flag.touch0 = true;
                }
                else if((tp_iic_T5_buffer_u8[fl_index_U8 + 1] & 0xF) == 1)
                {
                    msgPutToQ.touchStatus1 |= BIT0;
                }
            }
            else if(tp_iic_T5_buffer_u8[fl_index_U8] == (T100_object_id_start + 3))
            {
                msgPutToQ.touch_points[1].XLSB = tp_iic_T5_buffer_u8[fl_index_U8+2];
                msgPutToQ.touch_points[1].XMSB = tp_iic_T5_buffer_u8[fl_index_U8+3];
                msgPutToQ.touch_points[1].YLSB = tp_iic_T5_buffer_u8[fl_index_U8+4];
                msgPutToQ.touch_points[1].YMSB = tp_iic_T5_buffer_u8[fl_index_U8+5];
                if((tp_iic_T5_buffer_u8[fl_index_U8 + 1] & 0xF) == 4)
                {
                    msgPutToQ.touchStatus1 |= BIT1;
                }
                else if((tp_iic_T5_buffer_u8[fl_index_U8 + 1] & 0xF) == 5)
                {
                    msgPutToQ.touchStatus1 &= ~BIT1;
                }
                else if((tp_iic_T5_buffer_u8[fl_index_U8 + 1] & 0xF) == 9)
                {
                    msgPutToQ.touchStatus1 &= ~BIT1;
                    //up_down_flag.touch1 = true;
                }
                else if((tp_iic_T5_buffer_u8[fl_index_U8 + 1] & 0xF) == 1)
                {
                    msgPutToQ.touchStatus1 |= BIT1;
                }
            }
            else if(tp_iic_T5_buffer_u8[fl_index_U8] == (T100_object_id_start + 4))
            {
                msgPutToQ.touch_points[2].XLSB = tp_iic_T5_buffer_u8[fl_index_U8+2];
                msgPutToQ.touch_points[2].XMSB = tp_iic_T5_buffer_u8[fl_index_U8+3];
                msgPutToQ.touch_points[2].YLSB = tp_iic_T5_buffer_u8[fl_index_U8+4];
                msgPutToQ.touch_points[2].YMSB = tp_iic_T5_buffer_u8[fl_index_U8+5];
                msgPutToQ.touch_points[2].CRC8 = crc8_Calc(&tp_iic_T5_buffer_u8[fl_index_U8+2], 4);
                if((tp_iic_T5_buffer_u8[fl_index_U8 + 1] & 0xF) == 4)
                {
                    msgPutToQ.touchStatus1 |= BIT2;
                }
                else if((tp_iic_T5_buffer_u8[fl_index_U8 + 1] & 0xF) == 5)
                {
                    msgPutToQ.touchStatus1 &= ~BIT2;
                }
                else if((tp_iic_T5_buffer_u8[fl_index_U8 + 1] & 0xF) == 9)
                {
                    msgPutToQ.touchStatus1 &= ~BIT2;
                    //up_down_flag.touch2 = true;
                }
                else if((tp_iic_T5_buffer_u8[fl_index_U8 + 1] & 0xF) == 1)
                {
                    msgPutToQ.touchStatus1 |= BIT2;
                }
            }
            else if(tp_iic_T5_buffer_u8[fl_index_U8] == (T100_object_id_start + 5))
            {
                msgPutToQ.touch_points[3].XLSB = tp_iic_T5_buffer_u8[fl_index_U8+2];
                msgPutToQ.touch_points[3].XMSB = tp_iic_T5_buffer_u8[fl_index_U8+3];
                msgPutToQ.touch_points[3].YLSB = tp_iic_T5_buffer_u8[fl_index_U8+4];
                msgPutToQ.touch_points[3].YMSB = tp_iic_T5_buffer_u8[fl_index_U8+5];
                msgPutToQ.touch_points[3].CRC8 = crc8_Calc(&tp_iic_T5_buffer_u8[fl_index_U8+2], 4);
                if((tp_iic_T5_buffer_u8[fl_index_U8 + 1] & 0xF) == 4)
                {
                    msgPutToQ.touchStatus1 |= BIT3;
                }
                else if((tp_iic_T5_buffer_u8[fl_index_U8 + 1] & 0xF) == 5)
                {
                    msgPutToQ.touchStatus1 &= ~BIT3;
                }
                else if((tp_iic_T5_buffer_u8[fl_index_U8 + 1] & 0xF) == 9)
                {
                    msgPutToQ.touchStatus1 &= ~BIT3;
                    //up_down_flag.touch3 = true;
                }
                else if((tp_iic_T5_buffer_u8[fl_index_U8 + 1] & 0xF) == 1)
                {
                    msgPutToQ.touchStatus1 |= BIT3;
                }
            }
            else if(tp_iic_T5_buffer_u8[fl_index_U8] == (T100_object_id_start + 6))
            {
                msgPutToQ.touch_points[4].XLSB = tp_iic_T5_buffer_u8[fl_index_U8+2];
                msgPutToQ.touch_points[4].XMSB = tp_iic_T5_buffer_u8[fl_index_U8+3];
                msgPutToQ.touch_points[4].YLSB = tp_iic_T5_buffer_u8[fl_index_U8+4];
                msgPutToQ.touch_points[4].YMSB = tp_iic_T5_buffer_u8[fl_index_U8+5];
                msgPutToQ.touch_points[4].CRC8 = crc8_Calc(&tp_iic_T5_buffer_u8[fl_index_U8+2], 4);
                if((tp_iic_T5_buffer_u8[fl_index_U8 + 1] & 0xF) == 4)
                {
                    msgPutToQ.touchStatus1 |= BIT4;
                }
                else if((tp_iic_T5_buffer_u8[fl_index_U8 + 1] & 0xF) == 5)
                {
                    msgPutToQ.touchStatus1 &= ~BIT4;
                }
                else if((tp_iic_T5_buffer_u8[fl_index_U8 + 1] & 0xF) == 9)
                {
                    msgPutToQ.touchStatus1 &= ~BIT4;
                    //up_down_flag.touch4 = true;
                }
                else if((tp_iic_T5_buffer_u8[fl_index_U8 + 1] & 0xF) == 1)
                {
                    msgPutToQ.touchStatus1 |= BIT4;
                }
            }
            else
            {
                //error
            }
        }

        fl_index_U8 += T5_size;
    }

    if(osErrorOS == osMessagePut1 (QTouchId, (uint8_t*)(&msgPutToQ), osWaitForever))
	{
        TRACE_TEXT(LOG_ERR, MOD_TOUCH_IIC, "Not able to put touch data to msg Q");
	}
    else
    {
        TRACE_TEXT(LOG_SHOUT, MOD_TOUCH_IIC, "Put new touch data to msg Q");
    }
}


void TP_Init(void)
{
	//uint8 i;
    for (uint8_t i=0; i<sizeof(tp_info_blk)/sizeof(tp_info_blk[0]); i++)
    {
        tp_info_blk[i] = 0;
    }

    tp_iic_drv_buffer_u8[0] = TP_IIC_ADDRESS_LSB;
    tp_iic_drv_buffer_u8[1] = TP_IIC_ADDRESS_MSB;
	//TouchDef.item_sz = (uint32_t)sizeof(_u_touchinfo_Receive_buf);
	//TouchDef.queue_sz = (uint32_t)10;
	/*
	for(i=0; i<100; i++)
	{
		//test_buf[i] = i;
	}
	*/

    TP_IIC_INFO_Timer = 0;
    isTpInited = FALSE;

	BspGpio_SetGpioInt(LCD_IIC_CHG_ID, GPIO_FALLING_EDGE); //IGN wakeup, rising edge.
    BspGpio_EnGpioInt(LCD_IIC_CHG_ID);
	CHG_isr_sem = HalOS_SemaphoreCreateWithMaxCnt(0, 10, "Touch iic rdy");
	s_semDataSentToHost = HalOS_SemaphoreCreateWithMaxCnt(1, 1, "Touch Data Tx Done");
	QTouchId = osMessageCreate (&(TouchDef), NULL);
	(void)osThreadCreate(&os_thread_def_TouchEvent, NULL);
	(void)osThreadCreate(&os_thread_def_SendSOCEvent, NULL);

    ServFunc_StartCyclic(ServFunc_Create(Get_TP_Info_2ms, "iic INIT INFO Task"), 1, 2);
}

void Touch_Thread(void const *arg)
{
    for (;;)
	{
		if(HAL_OS_SEM_ERR_NONE == HalOS_SemaphoreWait(CHG_isr_sem, HAL_OS_WaitForever))
	    {
            if ((isTpInited == TRUE) && (TP_Power_State_On == LCD_TP_GetPowerState()))
            {
                if((T44_LSB != 0) || (T44_MSB != 0))
                {
                    memset(&tp_iic_T5_buffer_u8[0], 0, TP_IIC_T5_BUFFER_SIZE);
                    tp_iic_handler_receive_data(tp_iic_T44_address_buffer_u8, tp_iic_T5_buffer_u8, TP_IIC_DRV_BUFFER_MAX_BYTE, TP_IIC_T5_BUFFER_SIZE);
                    handle_read_T5_info();
                }
            }
	    }
	}
}

void SendSOC_Thread(void const *arg)
{
    HAL_OS_SEM_STATE semState;
    uint32 cpu_sr;

	for (;;)
	{
        if(osOK == osMessageGet1 (QTouchId, &msgGetFromQ,  osWaitForever))
        {
            semState = HalOS_SemaphoreWait(s_semDataSentToHost, 50);

            if (HAL_OS_SEM_TIMEOUT == semState)
            {
                TRACE_TEXT(LOG_INFO, MOD_TOUCH_IIC, "Host does not read touch data in time");
                Bsp_SOC_INT_Dis();
            }

            cpu_sr = osEnterCritical();
            if (!iicXferOngoing)
            {
                msgForHost = msgGetFromQ;

                //Pull down the soc pin.
                Bsp_SOC_INT_En();
            }
            osExitCritical(cpu_sr);
        }
        else
        {
            TRACE_TEXT(LOG_ERR, MOD_TOUCH_IIC, "Get touch data from msg Q error.");
        }
	}
}

void tp_iic_CHG_ISR(void)
{
	HalOS_SemaphorePost(CHG_isr_sem);
}

#if 0
void tp_iic_CHG_test(void)
{
	static uint8 flag = 0;
	static uint8 cnt = 0;
	uint8 i;
	//HalOS_SemaphorePost(CHG_isr_sem);
	if(flag == 0)
	{
		//Bsp_SOC_INT_En();
		//Bsp_TFTPowerBLEn();
		flag = 1;
	}
	else
	{
		//Bsp_SOC_INT_Dis();
		//Bsp_TFTPowerBLDis();
		flag = 0;
	}
	if(cnt < 2)
	{
		cnt ++;
	}
	else
	{
		cnt = 0;
		for(i=0; i<TOUCHINFO_DATA_LENGTH; i++)
		{
			touchinfo_tp_Receive_buf.byte[i] = i;
		}
		if(osErrorOS == osMessagePut1 (QTouchId, &(touchinfo_tp_Receive_buf.byte[0]), 0))
		{
			//error handler
		}
	}
}
#endif

void Get_TP_Info_2ms(void)
{
    if(TP_IIC_INFO_Timer < 250)
    {
        ++TP_IIC_INFO_Timer;
    }
    else if (TP_IIC_INFO_Timer == 250)
    {
        if ((isTpInited == FALSE) && (TP_Power_State_On == LCD_TP_GetPowerState()))
        {
            tp_iic_handler_receive_data(tp_iic_drv_buffer_u8, tp_info_blk, TP_IIC_DRV_BUFFER_MAX_BYTE, TP_INFO_BLK_SIZE);
            handle_read_init_info();
            ++TP_IIC_INFO_Timer;
        }
    }
}

void TOUCH_I2C_RxMsgHandler(uint16_t regOffset, uint16_t len, const uint8_t* data)
{
    iicXferOngoing = TRUE;

    Bsp_SOC_INT_Dis();

    /* Set the data address for read. */
    HalIic_SLV_SetTxData(host_i2c, ((uint8_t*)(&msgForHost)) + regOffset, sizeof(touch_msgType) - regOffset);
}

void TOUCH_I2C_TxDoneHandler(uint16_t regOffset)
{
	HalOS_SemaphorePost(s_semDataSentToHost);

    iicXferOngoing = FALSE;
}
