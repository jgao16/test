#ifndef TP_IIC_H
#define TP_IIC_H

#include "std_type.h"
#include <stdint.h>

#define TOUCH_POINT_NUM 5

typedef struct
{
    uint8 XLSB;
    uint8 XMSB;
    uint8 YLSB;
    uint8 YMSB;
    uint8 CRC8;
} touch_point_t;

typedef struct
{
    uint8 touchStatus1;
    uint8 touchstatus2;
    touch_point_t touch_points[TOUCH_POINT_NUM];
} touch_msgType;

#define TP_ID_INFO_SIZE 7
#define TP_OBJ_SIZE 6
#define TP_OBJ_NUM 34
#define TP_CRC_SIZE 3
#define TP_INFO_BLK_SIZE (TP_ID_INFO_SIZE + (TP_OBJ_SIZE * TP_OBJ_NUM) + TP_CRC_SIZE)

typedef struct
{
    uint8_t familyID;
    uint8_t variantID;
    uint8_t version;
    uint8_t build;
    uint8_t maxX;
    uint8_t maxY;
    uint8_t objNum;
} tp_id_info_t;

typedef struct
{
    uint8 type;
    uint8 startLSByte;
    uint8 startMSByte;
    uint8 size;
    uint8 instance;
    uint8 numberReportId;
} object_info_t;

extern void TP_Init(void);

extern void tp_iic_CHG_Detect_2ms(void);
extern void tp_iic_CHG_ISR(void);
extern void tp_iic_CHG_test(void);

void TOUCH_I2C_RxMsgHandler(uint16_t regOffset, uint16_t len, const uint8_t* data);
void TOUCH_I2C_TxDoneHandler(uint16_t regOffset);

#endif
