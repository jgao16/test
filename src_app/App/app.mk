

SOURCE_DIRS  +=   App
INCLUDE_DIRS += -IApp


SOURCE_DIRS  +=   App/eep
INCLUDE_DIRS += -IApp/eep

SOURCE_DIRS  +=   App/flash
INCLUDE_DIRS += -IApp/flash

SOURCE_DIRS  +=   App/pwrctrl
INCLUDE_DIRS += -IApp/pwrctrl

SOURCE_DIRS  +=   App/version
INCLUDE_DIRS += -IApp/version

SOURCE_DIRS  +=   App/diag
INCLUDE_DIRS += -IApp/diag

# SOURCE_DIRS  +=   App/app_ipc
# INCLUDE_DIRS += -IApp/app_ipc

INCLUDE_DIRS += -IApp/cfg

SOURCE_DIRS  +=   App/cfg/gen_code
INCLUDE_DIRS += -IApp/cfg/gen_code

SOURCE_DIRS  +=   App/monitor
INCLUDE_DIRS += -IApp/monitor

SOURCE_DIRS  +=   App/tp_iic
INCLUDE_DIRS += -IApp/tp_iic

SOURCE_DIRS  +=   App/host_iic
INCLUDE_DIRS += -IApp/host_iic

SOURCE_DIRS  +=   App/cf_test
INCLUDE_DIRS += -IApp/cf_test

#SOURCE_DIRS  +=   App/CAN
#INCLUDE_DIRS += -IApp/CAN
#
#SOURCE_DIRS  +=   App/CAN/ComRTE
#INCLUDE_DIRS += -IApp/CAN/ComRTE
#
#SOURCE_DIRS  +=   App/CAN/CanNm
#INCLUDE_DIRS += -IApp/CAN/CanNm
#
#SOURCE_DIRS  +=   App/CAN/CanDrv
#INCLUDE_DIRS += -IApp/CAN/CanDrv
#
#SOURCE_DIRS  +=   App/CAN/CanIf
#INCLUDE_DIRS += -IApp/CAN/CanIf
#
#SOURCE_DIRS  +=   App/CAN/Com
#INCLUDE_DIRS += -IApp/CAN/Com
#
#SOURCE_DIRS  +=   App/CAN/CanTp
#INCLUDE_DIRS += -IApp/CAN/CanTp
#
#SOURCE_DIRS  +=   App/CAN/Diag/Dem
#INCLUDE_DIRS += -IApp/CAN/Diag/Dem
#
#SOURCE_DIRS  +=   App/CAN/Diag/Dcm/Cfg
#INCLUDE_DIRS += -IApp/CAN/Diag/Dcm/Cfg
#
#SOURCE_DIRS  +=   App/CAN/Diag/Dcm/DcmDsd
#INCLUDE_DIRS += -IApp/CAN/Diag/Dcm/DcmDsd
#
#SOURCE_DIRS  +=   App/CAN/Diag/Dcm/DcmDsl
#INCLUDE_DIRS += -IApp/CAN/Diag/Dcm/DcmDsl
#
#SOURCE_DIRS  +=   App/CAN/Diag/Dcm/DcmDsp
#INCLUDE_DIRS += -IApp/CAN/Diag/Dcm/DcmDsp
#
#SOURCE_DIRS  +=   App/CAN/Diag/Dcm/DcmDsp/UDS
#INCLUDE_DIRS += -IApp/CAN/Diag/Dcm/DcmDsp/UDS
#
#SOURCE_DIRS  +=   App/CAN/Diag/Dcm/include
#INCLUDE_DIRS += -IApp/CAN/Diag/Dcm/include
#
##SOURCE_DIRS  +=   App/CAN/Diag/Dcm/Service_DF_Add
##INCLUDE_DIRS += -IApp/CAN/Diag/Dcm/Service_DF_Add
#
#SOURCE_DIRS  +=   App/CAN/Diag/DiagImx
#INCLUDE_DIRS += -IApp/CAN/Diag/DiagImx
#
#SOURCE_DIRS  +=   App/CAN/Diag/DiagVip
#INCLUDE_DIRS += -IApp/CAN/Diag/DiagVip
#
#SOURCE_DIRS  +=   App/CAN/Diag/DiagTest
#INCLUDE_DIRS += -IApp/CAN/Diag/DiagTest
#
#SOURCE_DIRS  +=   App/CAN/Diag/DiagVip/Gen_Code
#INCLUDE_DIRS += -IApp/CAN/Diag/DiagVip/Gen_Code
#
#SOURCE_DIRS  +=   App/CAN/Gen_Code
#INCLUDE_DIRS += -IApp/CAN/Gen_Code
#
#SOURCE_DIRS  +=   App/CAN/Test
#INCLUDE_DIRS += -IApp/CAN/Test

SOURCE_DIRS  +=   App/eeprom/gen_code
INCLUDE_DIRS += -IApp/eeprom/gen_code

# SOURCE_DIRS  +=   App/dev_sig/gen_code
# INCLUDE_DIRS += -IApp/dev_sig/gen_code
# SOURCE_DIRS  +=   App/dev_sig/dev_sig_test
# INCLUDE_DIRS += -IApp/dev_sig/dev_sig_test

#SOURCE_DIRS  +=   App/Meter
#INCLUDE_DIRS += -IApp/Meter
#
#SOURCE_DIRS  +=   App/Buzzer
#INCLUDE_DIRS += -IApp/Buzzer
#
#SOURCE_DIRS  +=   App/MeterOutput
#INCLUDE_DIRS += -IApp/MeterOutput
#
#SOURCE_DIRS  +=   App/MeterWarning
#INCLUDE_DIRS += -IApp/MeterWarning
#
#SOURCE_DIRS  +=   App/MeterHMI
#INCLUDE_DIRS += -IApp/MeterHMI
#
#SOURCE_DIRS  +=   App/Meter_GSI
#INCLUDE_DIRS += -IApp/Meter_GSI

SOURCE_DIRS  +=   App/boot
INCLUDE_DIRS += -IApp/boot

########### Added by xmu2
##INCLUDE_DIRS += -IApp/MDL/MDL_Include
#
##SOURCE_DIRS  +=   App/MDL/RTE
##INCLUDE_DIRS += -IApp/MDL/RTE
#
#SOURCE_DIRS  +=   App/MDL/MDL_ODO
#INCLUDE_DIRS += -IApp/MDL/MDL_ODO
#
##SOURCE_DIRS  +=   App/MDL/MDL_OBC
##INCLUDE_DIRS += -IApp/MDL/MDL_OBC
#
###SOURCE_DIRS  +=   App/MDL/MDL_GearBox
##INCLUDE_DIRS += -IApp/MDL/MDL_GearBox
#
#SOURCE_DIRS  +=   App/MDL/MDL_Fuel
#INCLUDE_DIRS += -IApp/MDL/MDL_Fuel
#
#SOURCE_DIRS  +=   App/MDL/MDL_Speed
#INCLUDE_DIRS += -IApp/MDL/MDL_Speed
#
#SOURCE_DIRS  +=   App/MDL/MDL_Tacho
#INCLUDE_DIRS += -IApp/MDL/MDL_Tacho
#
#SOURCE_DIRS  +=   App/MDL/MDL_Temperature
#INCLUDE_DIRS += -IApp/MDL/MDL_Temperature
#
##SOURCE_DIRS  +=   App/MDL/MDL_TestTask
##INCLUDE_DIRS += -IApp/MDL/MDL_TestTask
#
###SOURCE_DIRS  +=   App/MDL/MDL_Drive
##INCLUDE_DIRS += -IApp/MDL/MDL_Drive
#
###SOURCE_DIRS  +=   App/MDL/MDL_IdleStop
##INCLUDE_DIRS += -IApp/MDL/MDL_IdleStop
#
###SOURCE_DIRS  +=   App/MDL/MDL_ECO
##INCLUDE_DIRS += -IApp/MDL/MDL_ECO
#
##SOURCE_DIRS  +=   App/MDL/MDL_ANA
##INCLUDE_DIRS += -IApp/MDL/MDL_ANA
#
##SOURCE_DIRS  +=   App/MDL/LIB_MEM
##INCLUDE_DIRS += -IApp/MDL/LIB_MEM
#
##SOURCE_DIRS  +=   App/MDL/LIB_Math
##INCLUDE_DIRS += -IApp/MDL/LIB_Math
#
#
##SOURCE_DIRS  +=   App/MDL/MDL_FuelCan
##INCLUDE_DIRS += -IApp/MDL/MDL_FuelCan
#
#SOURCE_DIRS  +=   App/MDL
#INCLUDE_DIRS += -IApp/MDL

SOURCE_DIRS  +=   App/power_mana
INCLUDE_DIRS += -IApp/power_mana
