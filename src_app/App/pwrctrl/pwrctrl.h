
#ifndef __PWR_CTRL_H___				
#define __PWR_CTRL_H___

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#ifdef __cplusplus
extern "C" {
#endif 

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
//#define POWER_MODE_CTRL_EN

#define		SYS_POWER_RESET		(0x01u)
#define		SYS_EXTERN_RESET	(0x02u)
#define		SYS_WDT_RESET		(0x04u)
#define		SYS_SOFT_RESET		(0x08u)

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
/* low power gate 											*/
/* if the relative flag is set, cpu can't enter to sleep 	*/
typedef enum
{	
	LP_PWR_GATE_APP = 	0x00,
	LP_PWR_GATE_FLOW,
	LP_PWR_GATE_500ms,
	LP_PWR_GATE_GPRS,

	LP_PWR_ADC_CONV,
	
	LP_PWR_GATE_MAX			// LP_PWR_GATE_MAX must <= 32 
}LOW_POWER_GATE;

/* middle power gate		*/
typedef enum
{	// max 32
	MP_PWR_GATE_APP_MODE,
	MP_PWR_GATE_KEY_WAKEUP,	// need 50ms, middle function call 
	MP_PWR_GATE_GPRS,
	MP_PWR_GATE_RS485,		// other function need 50ms call, need 1ms call, for uart_cyclic(1ms)
	MP_PWR_GATE_SYS_DLY,	// used as ostime delay
	MP_PWR_GATE_SYS_WAIT,	// used as halsem_wait time 
	
	MP_PWR_GATE_MAX			// MP_PWR_GATE_MAX must <= 32 
}MID_POWER_GATE;

/* mode mode				*/
typedef enum
{
	POWER_MODE_NOT_SET,
	POWER_MODE_LOW,			// use default osc, no 1ms tick
	POWER_MODE_MIDDLE,		// use extern osc, with 1ms tick 
	
	POWER_MODE_HIGH			// normal 
}POWER_MODE;


typedef struct
{	
	//uint8 high power mode is need function , return TRUE if need high power 
	boolean (*hp_required)(void);
	/* urgent shutdown, hp->sleep		*/
	boolean  (*urgent_shutdown)(void);
	/* enter urgent shutdowm function	*/
	void (*enter_urgent_shutdown)(void);
	
	//enter to high power 
	void (*enter_hp)(void);
	//exit from high power 
	void (*exit_hp)(void);
	//request enter midlle power,
	void (*enter_mp)(void);
	//request exit middle power
	void (*exit_mp)(void);
	//request enter low power,
	void (*enter_lp)(void);
	//request exit low power
	void (*exit_lp)(void);
	
	//enter to sleep function
	void (*enter_sleep)(void);
	//exit from sleep function
	void (*exit_sleep)(void);
	
	// signal reset cpu as power chage: hp->lp or lp->hp
	void (*request_reset_cpu)(void);
}POWER_API;

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#ifdef POWER_MODE_CTRL_EN
void PwrCtrl_Init				(POWER_API const * api);
void PwrCtrl_IdleFunc			(void);		// cpu idle call
void PwrCtrl_1msHpFunc			(void);		// 1ms tick call, 
//void PwrCtrl_10msHpFunc			(void);		// 10ms task call 

void PwrCtrl_En1msMonitor   	(void);		// enable 1ms tick monitor
void PwrCtrl_Dis1msMonitor		(void);		// disable 1ms tick monitor

void PwrCtrl_EnLpSleepGlobal	(void);
void PwrCtrl_DisLpSleepGlobal	(void);
void PwrCtrl_EnLpSleepGate		(uint8 gate);
void PwrCtrl_DisLpSleepGate		(uint8 gate);
void PwrCtrl_EnMpSleepGlobal	(void);
void PwrCtrl_DisMpSleepGlobal	(void);
void PwrCtrl_EnMpSleepGate		(uint8 gate);
void PwrCtrl_DisMpSleepGate		(uint8 gate);

void PwrCtrl_ForceAllSleep		(void);
/* Get Power Mode */
POWER_MODE	PwrCtrl_GetPwrMode	(void);

#else 	// not define POWER_MODE_CTRL

#define 	PwrCtrl_Init(x)				do{}while(0)
#define 	PwrCtrl_IdleFunc()			do{}while(0)		// cpu idle call
#define 	PwrCtrl_1msHpFunc()			do{}while(0)		// 1ms tick call, 
//#define 	PwrCtrl_10msHpFunc()		do{}while(0)		// 10ms task call 

#define 	PwrCtrl_En1msMonitor()		do{}while(0)		// enable 1ms tick monitor
#define	 	PwrCtrl_Dis1msMonitor()		do{}while(0)		// disable 1ms tick monitor

#define 	PwrCtrl_EnLpSleepGlobal()	do{}while(0)
#define 	PwrCtrl_DisLpSleepGlobal()	do{}while(0)
#define 	PwrCtrl_EnLpSleepGate(x)	do{}while(0)
#define 	PwrCtrl_DisLpSleepGate(x)	do{}while(0)
#define 	PwrCtrl_EnMpSleepGlobal()	do{}while(0)
#define 	PwrCtrl_DisMpSleepGlobal()	do{}while(0)
#define 	PwrCtrl_EnMpSleepGate(x)	do{}while(0)
#define 	PwrCtrl_DisMpSleepGate(x)	do{}while(0)

#define 	PwrCtrl_ForceAllSleep()		do{}while(0)
/* Get Power Mode */
#define		PwrCtrl_GetPwrMode()		(POWER_MODE_HIGH)

#endif 


extern uint8 g_u1_sysPwrRstFlag;



//boolean PwrCtrl_AllowSleep	(void);

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#ifdef __cplusplus
}
#endif
#endif
