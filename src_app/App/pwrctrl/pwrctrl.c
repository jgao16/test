

#include "std_type.h"

#include "cpu.h"
#include "hal_os.h"
//#include "pwrctrl.h"
#include "serv.h"
//#include "il.h"
//#include "adc.h"

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  **/
#ifdef POWER_MODE_CTRL_EN

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  **/
typedef enum
{
	PWR_CTRL_EVT_NONE = 0x00,
	PWR_CTRL_EVT_10ms,
	PWR_CTRL_EVT_50ms,
	PWR_CTRL_EVT_500ms,
	PWR_CTRL_MODE_CHANGE
	
}PWR_CTRL_EVENT;

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  **/

uint8 g_u1_sysPwrRstFlag;

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  **/
static POWER_MODE			power_mode;
static boolean				pwr_en1ms_monitor;
static uint8				cnt_1ms;
static POWER_API const * 	api;

static uint32 				middle_power_gate;
static uint32 				middle_power_global;
static uint32   			system_sleep_gate;
static uint32				system_sleep_global;


static HAL_OS_SEM_ID 		pwrctrl_sem;

static void PwrCtrl_Task(void const*arg);
osThreadDef(PwrCtrl, PwrCtrl_Task, osPriorityNormal, 0, 160);

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
/* Set Power Mode */
static void PwrCtrl_StateCtrl ( void );
static void Task_PwrStateCtrl ( void );
//static void PwrCtrl_50msMpFunc(void*arg);
//static void PwrCtrl_500msLpFunc	(void*arg);
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  **/
/* power control init */
void PwrCtrl_Init(POWER_API const * pwr_api)
{
    //if ( SET == RCC_GetFlagStatus(RCC_FLAG_PORRST) ){
	//	g_u1_sysPwrRstFlag	|= SYS_POWER_RESET;
    //}
	//if ( SET == RCC_GetFlagStatus(RCC_FLAG_SFTRST) ){
	//	g_u1_sysPwrRstFlag	|= SYS_SOFT_RESET;
	//}
	//if ( SET == RCC_GetFlagStatus(RCC_FLAG_PINRST) ){
	//	g_u1_sysPwrRstFlag	|= SYS_EXTERN_RESET;	/* extern reset wat */
	//}
	//RCC_ClearFlag();
	
	api = pwr_api;

	pwr_en1ms_monitor 	= 	FALSE;
	power_mode			=	POWER_MODE_HIGH;

//	(void)ServFunc_CreatMpCycFunc(PwrCtrl_50msMpFunc, NULL);
//	(void)ServFunc_CreatLpCycFunc(PwrCtrl_500msLpFunc,NULL);
	
	/* create semaphore and task */
	pwrctrl_sem = HalOS_SemaphoreCreate(1, "PwrCtrl Sem..");
	/* create power control task */
	//(void)osThreadCreate(&os_thread_def_PwrCtrl,NULL);
	//if ( api != NULL ) 
	//{
	//	if ( api->hp_required != NULL )
	//	{
	//		if ( !api->hp_required() )
	//		{
	//			/* change power mode to middle powwer */
	//			HalOS_SemaphorePost(pwrctrl_sem);
	//		}		
	//	}
	//	else
	//	{	/* change power mode to middle powwer */
	//		HalOS_SemaphorePost(pwrctrl_sem);
	//	}
	//}
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  **/
/* Get Power Mode */
POWER_MODE	PwrCtrl_GetPwrMode(void)
{
	POWER_MODE pwr;
	
	CPU_SR_ALLOC();
	CPU_CRITICAL_ENTER();
	
	pwr =  power_mode;
	
	CPU_CRITICAL_EXIT();
	
	return pwr;
}
// cpu idle call
void PwrCtrl_IdleFunc(void)		
{	/* check power is change */
	PwrCtrl_StateCtrl();
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void PwrCtrl_1msHpFunc		(void)		// 1ms tick call, 
{
	if ( api != NULL )
	{
		boolean snd_sem = FALSE;
		
		if ( pwr_en1ms_monitor )
		{
			/* ADC cyclic call */
			//Adc_CycCall();
			
			CPU_SR_ALLOC();
			CPU_CRITICAL_ENTER();

			if ( POWER_MODE_HIGH == power_mode )
			{
				CPU_CRITICAL_EXIT();
				
				if ( api->urgent_shutdown != NULL && api->urgent_shutdown())
				{
					snd_sem = TRUE;
				}			
			}
			else
			{
				CPU_CRITICAL_EXIT();
			}
		}	
		
		cnt_1ms ++;
		if ( cnt_1ms > 9 )
		{	/* 10ms */
			cnt_1ms = 0x00;
			snd_sem = TRUE;
		}
		if ( snd_sem )
		{
			HalOS_SemaphorePost(pwrctrl_sem);
		}
	}
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
//void PwrCtrl_10msHpFunc		(void)		// 10ms task call 
//{
//	/* send event code to power controll */
//	if ( api != NULL )
//	{
//		HalOS_SemaphorePost(pwrctrl_sem);			// post ,power mode check 	
//	}
//}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
//static void PwrCtrl_50msMpFunc	(void * arg)		// for middle power consumer
//{
//	(void)arg;
//	if ( api != NULL )
//	{
//		HalOS_SemaphorePost(pwrctrl_sem);			// post ,power mode check 
//	}	
//}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
//static void PwrCtrl_500msLpFunc	(void*arg)		// for low power 500ms task
//{
//	(void)arg;
//	if ( api != NULL )
//	{
//		HalOS_SemaphorePost(pwrctrl_sem);			// post ,power mode check 
//	}	
//}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void PwrCtrl_En1msMonitor   (void)		// enable 1ms tick monitor
{
	pwr_en1ms_monitor = TRUE;
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void PwrCtrl_Dis1msMonitor	(void)		// disable 1ms tick monitor
{
	pwr_en1ms_monitor = FALSE;
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void PwrCtrl_EnLpSleepGlobal (void)
{
	CPU_SR_ALLOC();
	CPU_CRITICAL_ENTER();
	
	if ( system_sleep_global ){
		system_sleep_global --;
	}
	
	CPU_CRITICAL_EXIT();
}


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void PwrCtrl_DisLpSleepGlobal (void)
{
	CPU_SR_ALLOC();
	CPU_CRITICAL_ENTER();
	
	system_sleep_global ++;
	
	CPU_CRITICAL_EXIT();
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void PwrCtrl_EnLpSleepGate (uint8 gate)
{
	if ( gate < 32 )
	{
		CPU_SR_ALLOC();
		CPU_CRITICAL_ENTER();
		
		if ( system_sleep_gate ){
			system_sleep_gate &= ~(1u<<gate);
		}
		
		CPU_CRITICAL_EXIT();	
	}
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void PwrCtrl_DisLpSleepGate (uint8 gate)
{
	if ( gate < 32 )
	{
		CPU_SR_ALLOC();
		CPU_CRITICAL_ENTER();
		
		//if ( system_sleep_gate ){
		system_sleep_gate |= (1u<<gate);
		//}
		
		CPU_CRITICAL_EXIT();	
	}
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void PwrCtrl_EnMpSleepGlobal (void)
{
	CPU_SR_ALLOC();
	CPU_CRITICAL_ENTER();
	
	if ( middle_power_global ){
		middle_power_global --;
	}
	CPU_CRITICAL_EXIT();
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void PwrCtrl_DisMpSleepGlobal (void)
{
	//void (*func)(void) = NULL;
	CPU_SR_ALLOC();
	CPU_CRITICAL_ENTER();
	
	middle_power_global ++;
	
	if ( power_mode == POWER_MODE_LOW )
	{	/* lower power mode, need change to middle mode */
		HalOS_SemaphorePost(pwrctrl_sem);		// post ,power mode change, change to middle power 
	}
		
	CPU_CRITICAL_EXIT();
}


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void PwrCtrl_EnMpSleepGate (uint8 gate)
{
	if ( gate < 32 )
	{
		CPU_SR_ALLOC();
		CPU_CRITICAL_ENTER();

		if ( middle_power_gate ){
			middle_power_gate &= ~(1u<<gate);
		}
		
		CPU_CRITICAL_EXIT();
	}
}

void PwrCtrl_DisMpSleepGate (uint8 gate)
{
	if ( gate < 32 )
	{
		CPU_SR_ALLOC();
		CPU_CRITICAL_ENTER();
	
		//if ( middle_power_gate ){
		middle_power_gate |= (1u<<gate);
		//}
		
		if ( power_mode == POWER_MODE_LOW )
		{	/* lower power mode, need change to middle mode */
			HalOS_SemaphorePost(pwrctrl_sem);		// post ,power mode change, change to middle power 
		}
		
		CPU_CRITICAL_EXIT();
	}
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void PwrCtrl_ForceAllSleep	(void)
{
	CPU_SR_ALLOC();
	CPU_CRITICAL_ENTER();
	
	middle_power_gate	= 0;
	middle_power_global = 0 ;
	system_sleep_gate 	= 0;
	system_sleep_global = 0;
	
	HalOS_SemaphorePost(pwrctrl_sem);		// post ,power mode change, change to middle power 
	
	CPU_CRITICAL_EXIT();
}



/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/



/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  **/
static void PwrCtrl_Task(void const*arg)
{	
	POWER_MODE power_mode_old = POWER_MODE_NOT_SET;
	/* create system event service	*/
	while(1)	//lint !e716
	{
		(void)HalOS_SemaphoreWait(pwrctrl_sem, HAL_OS_WaitForever);
		
		/* power state control */
		Task_PwrStateCtrl();
		
		if ( power_mode_old != power_mode )
		{
			if ( power_mode == POWER_MODE_HIGH )
			{	
				//Adc_Init();
				PwrCtrl_En1msMonitor();
				/* delay 10ms, untile adc stable */
				HalOS_Delay(10);
			}
			else
			{
				PwrCtrl_Dis1msMonitor();
				//Adc_Stop();
			}
			
			power_mode_old = power_mode;
		}
	}
}


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void PwrCtrl_StateCtrl(void)
{
	if ( api != NULL ) 
	{
		//boolean shutdown = FALSE;
		
		//if ( pwr_en1ms_monitor )
		//{	
		//	CPU_SR_ALLOC();
		//	CPU_CRITICAL_ENTER();
		//	if ( POWER_MODE_HIGH == power_mode )
		//	{
		//		CPU_CRITICAL_EXIT();
		//		if ( api->urgent_shutdown != NULL && api->urgent_shutdown())
		//		{/* need urgent shutdown, change to sleep now, hp->sleep */
		//			shutdown = TRUE;
		//			HalOS_SemaphorePost(pwrctrl_sem);		
		//		}			
		//	}
		//	else
		//	{
		//		CPU_CRITICAL_EXIT();
		//	}
		//}
		
		//if ( !shutdown )
		{
			CPU_SR_ALLOC();
			CPU_CRITICAL_ENTER();
				
			switch( power_mode )
			{
			case POWER_MODE_HIGH:
				if ( api->hp_required == NULL ){
					HalOS_SemaphorePost(pwrctrl_sem);
				}
				break;
			
			case POWER_MODE_MIDDLE:
				if (  middle_power_gate == 0x00 && middle_power_global == 0x00 ){
					HalOS_SemaphorePost(pwrctrl_sem);;
				}
				break;

			case POWER_MODE_LOW:
				/* check if need other power */
				if (  middle_power_gate || middle_power_global ){
					HalOS_SemaphorePost(pwrctrl_sem);
				}else if ( system_sleep_gate == 0 && system_sleep_global == 0  ){
					HalOS_SemaphorePost(pwrctrl_sem);
				}
				else{}
				break;
			
			default:
				power_mode = POWER_MODE_HIGH;
				break;
			}
			
			CPU_CRITICAL_EXIT();		
		}	
	}
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void Task_PwrStateCtrl ( void )
{
	if ( api != NULL )
	{
		boolean shutdown = FALSE;
		if ( pwr_en1ms_monitor)
		{
			CPU_SR_ALLOC();
			CPU_CRITICAL_ENTER();
			if ( POWER_MODE_HIGH == power_mode )
			{
				CPU_CRITICAL_EXIT();
				if ( api->urgent_shutdown != NULL && api->urgent_shutdown())
				{/* need urgent shutdown, change to sleep now, hp->sleep */
					if ( api->enter_urgent_shutdown != NULL )
					{
						api->enter_urgent_shutdown();
					}
					if ( api->request_reset_cpu )
					{
						api->request_reset_cpu();
					}
					shutdown = TRUE;					
				}			
			}
			else
			{
				CPU_CRITICAL_EXIT();
			}
		}
		
		if ( !shutdown )
		{
			boolean 	mode_chg = FALSE;
			
			CPU_SR_ALLOC();
			CPU_CRITICAL_ENTER();
			POWER_MODE  pwr_mode = power_mode;
			CPU_CRITICAL_EXIT();
			
			switch(pwr_mode)
			{
			case POWER_MODE_HIGH:
				if ( api->hp_required != NULL )
				{
					if ( !api->hp_required() )
					{
						mode_chg = TRUE;
					}
				}
				else
				{	/* api->hp_required == NULL */
					mode_chg = TRUE;
				}
				
				if ( mode_chg == TRUE )
				{	
					pwr_mode = POWER_MODE_MIDDLE;
					if ( api->exit_hp )
					{
						api->exit_hp();
					}
					if ( api->enter_mp )
					{
						api->enter_mp();
					}
				}
				break;
				
			case POWER_MODE_MIDDLE:
				if ( api->hp_required != NULL && api->hp_required() )
				{
					mode_chg = TRUE;
					pwr_mode = POWER_MODE_HIGH;
				}
				else
				{
					CPU_CRITICAL_ENTER();
					if ( middle_power_gate == 0x00 && middle_power_global == 0x00 )
					{	/* no need middle power */
						CPU_CRITICAL_EXIT();
						mode_chg = TRUE;
						pwr_mode = POWER_MODE_LOW;
					}
					else
					{
						CPU_CRITICAL_EXIT();
					}
					
				}
				if ( mode_chg )
				{
					if ( api->exit_mp )
					{
						api->exit_mp ();
					}
					if ( pwr_mode == POWER_MODE_HIGH )
					{
						if ( api->enter_hp )
						{
							api->enter_hp();
						}
					}
					else// if ( pwr_mode == POWER_MODE_LOW ) 
					{
						if ( api->enter_lp )
						{
							api->enter_lp();
						}
					}
					//else{}
				}
				break;

			case POWER_MODE_LOW:
				if ( api->hp_required != NULL && api->hp_required() )
				{
					mode_chg = TRUE;
					pwr_mode = POWER_MODE_HIGH;
				}
				else
				{
					CPU_CRITICAL_ENTER();
					if ( middle_power_gate || middle_power_global  )
					{	/* no need middle power */
						CPU_CRITICAL_EXIT();
						mode_chg = TRUE;
						pwr_mode = POWER_MODE_MIDDLE;
					}
					else
					{
						CPU_CRITICAL_EXIT();
					}
					
				}
				if ( mode_chg )
				{
					if ( api->exit_lp )
					{
						api->exit_lp ();
					}
					if ( pwr_mode == POWER_MODE_HIGH )
					{
						if ( api->enter_hp )
						{
							api->enter_hp();
						}
					}
					else //if ( pwr_mode == POWER_MODE_MIDDLE ) 
					{	 //POWER_MODE_MIDDLE
						if ( api->enter_mp )
						{
							api->enter_mp();
						}
					}
					//else{}
				}
				else
				{
					CPU_CRITICAL_ENTER();
					if ( system_sleep_gate == 0x00 && system_sleep_global == 0x00  )
					{	/* enter to sleep now */
						CPU_CRITICAL_EXIT();
						if ( api->enter_sleep )
						{
							api->enter_sleep();
						}
						if ( api->exit_sleep )
						{
							api->exit_sleep();
						}
						/* need again check or sleep */
						mode_chg = TRUE;
					}
					else
					{
						CPU_CRITICAL_EXIT();
					}	
				}
				break;
				
			default:
				mode_chg = TRUE;
				pwr_mode = POWER_MODE_HIGH;
				break;
			}

			if ( mode_chg )
			{
				//HalOS_SemaphorePost(pwrctrl_sem);
				CPU_CRITICAL_ENTER();
				power_mode = pwr_mode;
				CPU_CRITICAL_EXIT();
			}
			else
			{
				//PWR_EnterSleepMode(PWR_Regulator_ON,PWR_SLEEPEntry_WFI);
			}
		}		
	}	
}


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  **/
#endif




