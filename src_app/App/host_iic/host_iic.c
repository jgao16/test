#include "host_iic.h"
#include "bootloader.h"
#include "tp_iic.h"
#include "hal_iic_slv.h"
#include "trace_api.h"
#include "ver_info.h"
#include "backlight.h"

#define ARRAY_SIZE(x) (sizeof(x)/sizeof(x[0]))

#define HOST_I2C_ADDR_SIZE 2

#define I2C_TOUCH_ADDR_START 0
#define I2C_TOUCH_ADDR_END 0x1A

#define I2C_BACKLIGHT_ADDR_START 0x100
#define I2C_BACKLIGHT_ADDR_END 0x102

#define I2C_PHOTO_SENSOR_ADDR_START 0x200
#define I2C_PHOTO_SENSOR_ADDR_END 0x202

#define I2C_MCU_INFO_ADDR_START 0x300
#define I2C_MCU_INFO_ADDR_END 0x309

#define I2C_DEFECT_ADDR_START 0x400
#define I2C_DEFECT_ADDR_END 0x406

#define I2C_BOOTLOADER_ADDR_START 0x1000
#define I2C_BOOTLOADER_ADDR_END 0x1003

#define I2C_TEST_DATA_SIZE 16
#define I2C_TEST_ADDR_START 0x2000
#define I2C_TEST_ADDR_END (I2C_TEST_ADDR_START + I2C_TEST_DATA_SIZE - 1)

#define HOST_I2C_RX_BUFF_LEN 128

#define HOST_IC_INVALID_REG 0xFFFF

static uint8_t s_host_i2c_test_data[I2C_TEST_DATA_SIZE] = {0, 1, 2, 3 };

void* host_i2c;

static void HOST_I2C_Dispatch(IIC_SLV_EVENT event, const uint8_t * data, uint16_t data_len);

static uint8_t s_host_i2c_rx_buff[HOST_I2C_RX_BUFF_LEN];

static uint16_t s_curReg;

#if 0
void PHOTO_SENSOR_I2C_MsgHandler(uint16_t addr, uint16_t len, const uint8_t* data)
{
}
#endif

void DEFECT_I2C_MsgHandler(uint16_t addr, uint16_t len, const uint8_t* data)
{
}

void TEST_I2C_MsgHandler(uint16_t regOffset, uint16_t len, const uint8_t* data)
{
    uint8_t * addr_in_array = &s_host_i2c_test_data[regOffset];

    if (len > (sizeof(s_host_i2c_test_data) - regOffset))
    {
        len = sizeof(s_host_i2c_test_data) - regOffset;
    }

    if (len > 0)
    {
        memcpy(addr_in_array, data, len);
    }

    /* Set the data address for read. */
    HalIic_SLV_SetTxData(host_i2c, addr_in_array, sizeof(s_host_i2c_test_data) - regOffset);
}

struct i2c_rx_dispatch
{
    uint16_t startAddr;
    uint16_t endAddr;
    i2c_rx_msg_handler_t rxMsgHandler;
    i2c_tx_done_handler_t txDoneHandler;
};

static const struct i2c_rx_dispatch s_i2cDispatchTable[] =
{
    {
        I2C_TOUCH_ADDR_START,
        I2C_TOUCH_ADDR_END,
        TOUCH_I2C_RxMsgHandler,
        TOUCH_I2C_TxDoneHandler,
    },
    {
        I2C_BACKLIGHT_ADDR_START,
        I2C_BACKLIGHT_ADDR_END,
        BACKLIGHT_I2C_MsgHandler,
        NULL,
    },
#if 0
    {
        I2C_PHOTO_SENSOR_ADDR_START,
        I2C_PHOTO_SENSOR_ADDR_END,
        PHOTO_SENSOR_I2C_MsgHandler,
    },
#endif
    {
        I2C_MCU_INFO_ADDR_START,
        I2C_MCU_INFO_ADDR_END,
        MCU_INFO_I2C_MsgHandler,
        NULL,
    },
    {
        I2C_DEFECT_ADDR_START,
        I2C_DEFECT_ADDR_END,
        DEFECT_I2C_MsgHandler,
        NULL,
    },
    {
        I2C_BOOTLOADER_ADDR_START,
        I2C_BOOTLOADER_ADDR_END,
        BOOTLOADER_I2C_MsgHandler,
        NULL,
    },
    {
        I2C_TEST_ADDR_START,
        I2C_TEST_ADDR_END,
        TEST_I2C_MsgHandler,
        NULL,
    },
};

void HOST_I2C_Init(void)
{
    host_i2c = HalIic_SLV_GetDev(SOC_ID);

    if (NULL == host_i2c)
    {
        TRACE_TEXT(LOG_ERR, MOD_HOST_I2C, "Could not get host I2C device");
    }
    else
    {
        HalIic_SLV_Init(host_i2c, s_host_i2c_rx_buff, HOST_I2C_RX_BUFF_LEN, HOST_I2C_Dispatch);
        s_curReg = HOST_IC_INVALID_REG;
    }
}

static void HOST_I2C_Dispatch(IIC_SLV_EVENT event, const uint8_t * data, uint16_t data_len)
{
    uint16_t i;
    boolean data_valid = FALSE;

    if (IIC_SLV_WRITE_DONE == event)
    {
        if (data_len >= HOST_I2C_ADDR_SIZE)
        {
            s_curReg = data[1] + (data[0] << 8U);

            TRACE_VALUE(LOG_INFO, MOD_HOST_I2C, "Host access I2C register 0x%04X", s_curReg);

            data_valid = TRUE;
        }
    }
    else if (IIC_SLV_READ_DONE == event)
    {
        data_valid = TRUE;
    }

    if (TRUE == data_valid)
    {
        for (i=0; i<ARRAY_SIZE(s_i2cDispatchTable); i++)
        {
            if ((s_i2cDispatchTable[i].startAddr <= s_curReg) && (s_i2cDispatchTable[i].endAddr >= s_curReg))
            {
                if (IIC_SLV_WRITE_DONE == event)
                {
                    s_i2cDispatchTable[i].rxMsgHandler(s_curReg - s_i2cDispatchTable[i].startAddr, data_len-HOST_I2C_ADDR_SIZE, data+HOST_I2C_ADDR_SIZE);
                }
                else if (IIC_SLV_READ_DONE == event)
                {
                    if (s_i2cDispatchTable[i].txDoneHandler != NULL)
                    {
                        s_i2cDispatchTable[i].txDoneHandler(s_curReg - s_i2cDispatchTable[i].startAddr);
                    }
                }
                break;
            }
        }
    }
}
