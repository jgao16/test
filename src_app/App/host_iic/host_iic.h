#ifndef _HOST_IIC_H_
#define _HOST_IIC_H_

#include <stdint.h>

extern void* host_i2c;

typedef void (*i2c_rx_msg_handler_t)(uint16_t addr, uint16_t len, const uint8_t* data);

typedef void (*i2c_tx_done_handler_t)(uint16_t addr);

void HOST_I2C_Init(void);

#endif
