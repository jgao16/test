/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
All rights reserved
>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#include "std_type.h"
#include "lk_uboot.h"

#include "ver_info.h"
#include "serv.h"
#include "trace_api.h"
#include "crc8.h"
#include "host_iic.h"
#include "hal_iic_slv.h"

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static version_info_t s_version_info;

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void VerInfo_Init(void)
{
    uint32_t cpu_sr;

    cpu_sr = osEnterCritical();

    s_version_info.sw_ver.B.msb = SW_VER_MSB;
    s_version_info.sw_ver.B.msb_1 = SW_VER_MSB_1;
    s_version_info.sw_ver.B.lsb_1 = SW_VER_LSB_1;
    s_version_info.sw_ver.B.lsb = SW_VER_LSB;
    s_version_info.hw_ver.B.msb = HW_VER_MSB;
    s_version_info.hw_ver.B.lsb = HW_VER_LSB;
    s_version_info.tp_firmware_version = TP_FIRMWARE_VERSION;
    s_version_info.tp_firmware_build = TP_FIRMWARE_BUILD;
    s_version_info.i2c_protocal_version = I2C_PROTOCAL_VERSION;

    s_version_info.crc = crc8_Calc((uint8_t const *)(&s_version_info), 8);

    osExitCritical(cpu_sr);
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
uint32_t VerInfo_GetSWVersion(void)
{
    return s_version_info.sw_ver.val;
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
uint16_t VerInfo_GetHWVersion(void)
{
    return s_version_info.hw_ver.val;
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void VerInfo_UpdateTpFirmwareVersion(uint8_t version, uint8_t build)
{
    uint32_t cpu_sr;

    cpu_sr = osEnterCritical();

    s_version_info.tp_firmware_version = version;
    s_version_info.tp_firmware_build = build;

    s_version_info.crc = crc8_Calc((uint8_t const *)(&s_version_info), 8);

    osExitCritical(cpu_sr);
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void MCU_INFO_I2C_MsgHandler(uint16_t regOffset, uint16_t len, const uint8_t* data)
{
    uint8_t *ver = (uint8_t *)&s_version_info;

    /* Set the data address for read. */
    HalIic_SLV_SetTxData(host_i2c, &(ver[regOffset]), sizeof(s_version_info) - regOffset);
}
