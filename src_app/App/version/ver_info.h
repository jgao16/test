/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
All rights reserved
>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
//#pragma once
#ifndef VER_INFO_H_H_
#define VER_INFO_H_H_
#ifdef  __cplusplus
extern "C"
{
#endif

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

/* SW version a.b.c.d. */
#ifdef __APP_
#define SW_VER_MSB   1 /* a */
#define SW_VER_MSB_1 6 /* b */
#define SW_VER_LSB_1 0 /* c */
#define SW_VER_LSB   0 /* d */
#else
#define SW_VER_MSB   1 /* a */
#define SW_VER_MSB_1 6 /* b */
#define SW_VER_LSB_1 0 /* c */
#define SW_VER_LSB   0 /* d */
#endif

/* HW version x.y */
#define HW_VER_MSB 1 /* x */
#define HW_VER_LSB 0 /* y */

/* TP firmware version. */
#define TP_FIRMWARE_VERSION 1
#define TP_FIRMWARE_BUILD 2

/* I2C protocal version. */
#define I2C_PROTOCAL_V5 5
#define I2C_PROTOCAL_V6 6

#define I2C_PROTOCAL_VERSION I2C_PROTOCAL_V6

typedef union
{
    struct
    {
        uint8_t msb;
        uint8_t msb_1;
        uint8_t lsb_1;
        uint8_t lsb;
    } B;
    uint32_t val;
} sw_ver_t;

typedef union
{
    struct
    {
        uint8_t msb;
        uint8_t lsb;
    } B;
    uint16_t val;
} hw_ver_t;

typedef struct
{
    sw_ver_t sw_ver;
    hw_ver_t hw_ver;
    uint8_t tp_firmware_version;
    uint8_t tp_firmware_build;
    uint8_t  crc;
    uint8_t  i2c_protocal_version;
} version_info_t;

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void VerInfo_Init(void);

uint32_t VerInfo_GetSWVersion(void);

uint16_t VerInfo_GetHWVersion(void);

void VerInfo_UpdateTpFirmwareVersion(uint8_t version, uint8_t build);

void MCU_INFO_I2C_MsgHandler(uint16_t addr, uint16_t len, const uint8_t* data);

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

#ifdef  __cplusplus
}
#endif

#endif
