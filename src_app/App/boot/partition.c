/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
All rights reserved
>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#include "std_type.h"
#include "lk_uboot.h"

#define MD5_SIZE 16

#define UBOOT_PART_START 0x00
#define UBOOT_PART_END 0x1FFF

#define SWDL_PART_START 0x2000
#define SWDL_PART_END 0x0FFFF

#define APP_PART_START 0x10000
#define APP_PART_END 0x2FFFF

#ifdef	SW_BUILD_TM_AVBL
#include	"build_tm.h"
#endif

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
/*lint -e40 -e10 -e526 -e752*/
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
extern const  uint32  __vector_table[];

__root	const	char	flash_configure[]	@	0x400U	=
{
	//	0x0400-0x0407 back door comparison key
	0xFF,	0xFF,	0xFF,	0xFF,	0xFF,	0xFF,	0xFF,	0xFF,
	//	0x0408-C0x040B Reserved
	0xFF,	0xFF,	0xFF,	0xFF,
	//	FSEC, back door disable, mass erase enable, factory access granted,
	//	security is unsecure.
	0xFE,
	0x7A,	//	FOPT, disable NMI, cpu and system clock DIV=1
	0xFF,	//	FEPROT,eeprom NOT protected
	0xFF	//	Data Flash not protected
};

#ifdef __UBOOT_
__root static const	UBOOT_INFO_TypeDef	sstuBootInfo	@	UBOOT_INFO_START_ADDR	=
{
		.app_info_addr				=	APP_PART_START,
		.swdl_info_addr				=	SWDL_PART_START,
		.fbl_magic_flg_addr			=	0x2001EFFC, //0x20006FFC, 
		.uboot_partition_start_addr	=	UBOOT_PART_START,
		.uboot_partition_end_addr	=	UBOOT_PART_END,
		.swdl_partition_start_addr	=	SWDL_PART_START,
		.swdl_partition_end_addr	=	SWDL_PART_END,
		.app_partition_start_addr	=	APP_PART_START,
		.app_partition_end_addr		=	APP_PART_END, 
		.version		=	0x00000101, // v 1.1.0.0
		.share_mem_addr	=	0x2001EF00,
		.build_tm		=	BUILD_TM,
};
#elif defined(__SWDL_)
__root static const SWDL_INFO_TypeDef swdl_info_ @ SWDL_PART_START= 
{
	//	SWDL md5 sum
	//uint8	md5[16];
	//	software build time: utc time. second
#ifdef SW_BUILD_TM_AVBL
	.build_tm	=	BUILD_TM_LINUX,
#endif
	//	version: Not used
	.version	=	0,
	//	software module id, reserved, not used
	.swmi		=	0,
	//	swdl program vector table addr
	.swdl_program_start = (uint32)__vector_table,
	//	enter to boot or app flag address
	//.fbl_magic_flg_addr = MAGIC_FLAG_ADDR,	// not used now, using uboot magic address 

	/*	code start is used for md5 calculation	*/
	.swdl_code_start_addr = (SWDL_PART_START + MD5_SIZE),
	.swdl_code_end_addr   = (SWDL_PART_END - PATN_INFO_SIZE),		//	=	 - 64(PATN_INFO)

	.swdl_partition_start_addr	=	SWDL_PART_START,
	.swdl_partition_end_addr	=	SWDL_PART_END,
};

#elif defined(__APP_)
__root static const APP_INFO_TypeDef app_info_ @ APP_PART_START = 
{
#ifdef SW_BUILD_TM_AVBL
	.build_tm	=	BUILD_TM_LINUX,
#endif	
	//	version: Not used
	.version	=	0,
	//	software module id, reserved, not used
	.swmi		=	0,
	//	APP program start address, boot need jump to this address when jump to APP
	.app_program_start = (uint32)__vector_table,
	//	enter to boot or app flag address
	//.fbl_magic_flg_addr = MAGIC_FLAG_ADDR,	// not used for n331
	
	/*	APP code start is used for md5 calc	*/
	.app_code_start_addr = (APP_PART_START + MD5_SIZE),			//	=	 + 16(md5)
	.app_code_end_addr   = (APP_PART_END - PATN_INFO_SIZE),

	.app_partition_start_addr = APP_PART_START,			//	=	
	.app_partition_end_addr   = APP_PART_END //	=	
};
#else 
#error "need defined __UBOOT_, __SWDL_ or __APP_"
#endif


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

