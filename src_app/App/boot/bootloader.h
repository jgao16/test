#ifndef	__BOOTLOADER_H__
#define	__BOOTLOADER_H__

#include	"std_type.h"

#define BOOTLOADER_I2C_REG_MODE 0
#define BOOTLOADER_I2C_REG_FRAME_DATA 1
#define BOOTLOADER_I2C_REG_FRAME_STATUS 2
#define BOOTLOADER_I2C_REG_UPGRADE_STATUS 3

void BOOTLOADER_I2C_MsgHandler(uint16_t regOffset, uint16_t len, const uint8_t* data);

void BOOTLOADER_Init(void);

#endif
