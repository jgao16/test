#include "std_type.h"
#include "hal_hw.h"
#include "hal_flash.h"
#include "hal_wdt.h"
#include "cpu.h"
#include "serv.h"
#include "md5.h"
#include "lk_uboot.h"
#include "bootloader.h"
#include "trace_api.h"
#include "ver_info.h"
#include "hal_iic_slv.h"
#include "host_iic.h"
#ifdef __APP_
#include "panel.h"
#endif

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>**/
#define PAGE_TO_ADDR(page) ((page) * 64)

#define SECTOR_SIZE              (0x1000U)                   //    4K
#define PROG_UNIT_SIZE           (8U)                        //    64-bit align
#define APP_CHK_STRICT_EN        (0)

#define SWDL_READ_LEN_MAX  (4)

#define BL_MODE_APP 0
#define BL_MODE_SWDL 1

#define BOOTLOADER_FRAME_OK 1
#define BOOTLOADER_FRAME_ERROR 0

#define BOOTLOADER_UPGRADE_OK 1
#define BOOTLOADER_UPGRADE_ERROR 0

#define BOOTLOADER_MAX_PAYLOAD_LEN 64
#define BOOTLOADER_MIN_FRAME_LEN (2 + 1 + 1)
#define BOOTLOADER_MAX_FRAME_LEN (BOOTLOADER_MIN_FRAME_LEN + BOOTLOADER_MAX_PAYLOAD_LEN)
#define BOOTLOADER_GET_FRAME_PAYLOAD_LEN(data) (data[2])
#define BOOTLOADER_GET_FRAME_FLASH_PAGE(data) ((data[0] << 8U) + data[1])
#define BOOTLOADER_GET_FRAME_PAYLOAD(data) (&data[3])

typedef enum
{
    BL_PendingWork_None = 0,
    BL_PendingWork_ProgramFrame,
    BL_PendingWork_SetMode,
} bl_pending_work_t;

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>**/
static void BootLoaderTask(void const *arg);

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>**/
static uint8_t  su1TxMsgBuf[SWDL_READ_LEN_MAX];
static volatile boolean s_needErase = TRUE;
static volatile uint8_t s_frameStatus = BOOTLOADER_FRAME_ERROR;
static volatile boolean s_upgradeStatus = BOOTLOADER_UPGRADE_OK;
static volatile boolean s_programOnGoing = FALSE;
static volatile bl_pending_work_t s_pendingWork = BL_PendingWork_None;

static HAL_OS_SEM_ID s_blSema = NULL;
static osThreadDef(BootLoader, BootLoaderTask, osPriorityNormal, 0, 256);
static uint8_t  s_progFrameData[BOOTLOADER_MAX_PAYLOAD_LEN];
static uint16_t s_progPage;
static uint16_t s_progPayloadLen;

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>**/

static void        ProcCalcMd5Sum(uint32 start, uint32 end, uint8 * md5);
#ifdef __APP_
static boolean     ProcChkSwdlFwOk(void);
#else
static boolean     ProcChkAppFwOk(void);
#endif

static boolean BOOTLOADER_ModeHandler(uint16_t len, const uint8_t *data);
static boolean BOOTLOADER_FrameHandler(uint16_t len, const uint8_t *data);
static boolean BOOTLOADER_FrameStatusHandler(uint16_t len, const uint8_t *data);
static boolean BOOTLOADER_UpgradeStatusHandler(uint16_t len, const uint8_t *data);
static boolean BOOTLOADER_GetMode(void);
static boolean BOOTLOADER_SetMode(uint8_t mode);
static boolean BOOTLOADER_EraseFlash(void);
static boolean BOOTLOADER_ProgramData(uint16_t page, uint16_t size, const uint8_t *data);
static boolean BOOTLOADER_VerifyUpdate(void);
static boolean BOOTLOADER_HandleProgramFrame(void);
static boolean BOOTLOADER_HandleSetMode(void);

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>**/

static void BootLoaderTask(void const *arg)
{
    boolean err_;

    for(;;)
    {
        err_ = TRUE;

		if(HAL_OS_SEM_ERR_NONE == HalOS_SemaphoreWait(s_blSema, HAL_OS_WaitForever))
        {
            switch (s_pendingWork)
            {
                case BL_PendingWork_ProgramFrame:
                    err_ = BOOTLOADER_HandleProgramFrame();
                    break;

                case BL_PendingWork_SetMode:
                    err_ = BOOTLOADER_HandleSetMode();
                    break;

                default:
                    TRACE_TEXT(LOG_ERR, MOD_SWDL, "Not valid pending work");
            }

            s_pendingWork = BL_PendingWork_None;
        }

        if (err_ == TRUE)
        {
            TRACE_TEXT(LOG_ERR, MOD_SWDL, "Error happens while handling the pending task");
        }
    }
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>**/
static boolean BOOTLOADER_HandleProgramFrame(void)
{
    boolean err_ = TRUE;
    boolean eraseError = FALSE;

    if (s_needErase)
    {
        eraseError = BOOTLOADER_EraseFlash();
    }

    if (FALSE == eraseError)
    {
        s_needErase = FALSE;

        TRACE_TEXT(LOG_DEBUG, MOD_SWDL, "Write Data to Pflash");

        err_ = BOOTLOADER_ProgramData(s_progPage, s_progPayloadLen, s_progFrameData);

        if (TRUE == err_)
        {
            s_needErase = TRUE;
        }
    }

    if (TRUE == err_)
    {
        s_frameStatus = BOOTLOADER_FRAME_ERROR;
    }
    else
    {
        s_frameStatus = BOOTLOADER_FRAME_OK;
    }

    return err_;
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>**/
static boolean BOOTLOADER_HandleSetMode(void)
{
#ifdef __APP_
    LCD_Disable();
#endif

    HalOS_Delay(200);      // wait until log output to uart

#ifdef __APP_
    while (LCD_POWER_OFF != LCD_GetPowerState())
    {
        LCD_Disable();
        HalOS_Delay(100);
    }
#endif

    SetUbootJumpFlag();    // set indication from other

    ServWdt_ForceRstCpu();

    return FALSE;
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>**/
void BOOTLOADER_Init(void)
{
    s_blSema = HalOS_SemaphoreCreateWithMaxCnt(0, 1, "Boot Loader Sema");
    s_needErase = TRUE;

    if (NULL == s_blSema)
    {
        TRACE_TEXT(LOG_INFO, MOD_SWDL, "Boot loader initialize failed");
    }
    else
    {
        (void)osThreadCreate(&os_thread_def_BootLoader, NULL);
    }
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>**/
void BOOTLOADER_I2C_MsgHandler(uint16_t regOffset, uint16_t len, const uint8_t* data)
{
    boolean err_ = TRUE;

    switch (regOffset)
    {
        case BOOTLOADER_I2C_REG_MODE:
            err_ = BOOTLOADER_ModeHandler(len, data);
            break;

        case BOOTLOADER_I2C_REG_FRAME_DATA:
            err_ = BOOTLOADER_FrameHandler(len, data);
            break;

        case BOOTLOADER_I2C_REG_FRAME_STATUS:
            err_ = BOOTLOADER_FrameStatusHandler(len, data);
            break;

        case BOOTLOADER_I2C_REG_UPGRADE_STATUS:
            err_ = BOOTLOADER_UpgradeStatusHandler(len, data);
            break;

        default:
            break;
    }

    if (err_)
    {
        TRACE_VALUE2(LOG_ERR, MOD_SWDL, "Boot loader Error Command, cmd=0x%04x,len=%d", regOffset, len);
    }
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static boolean BOOTLOADER_ModeHandler(uint16_t len, const uint8_t *data)
{
    boolean err_;

    if (0 == len)
    {
        err_ = BOOTLOADER_GetMode();
    }
    else if (1 == len)
    {
        err_ = BOOTLOADER_SetMode(data[0]);
    }
    else
    {
        err_ = TRUE;
    }

    return err_;
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static boolean BOOTLOADER_GetMode(void)
{
#ifdef __APP_
    su1TxMsgBuf[0]    =    BL_MODE_APP;    //    APP state
#else
    su1TxMsgBuf[0]    =    BL_MODE_SWDL;    //    SWDL state
#endif
    HalIic_SLV_SetTxData(host_i2c, su1TxMsgBuf, 1);

    TRACE_VALUE(LOG_DEBUG, MOD_SWDL, "Rx read state command (0x01),state=%d",su1TxMsgBuf[0]);

    return FALSE;
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static boolean BOOTLOADER_SetMode(uint8_t mode)
{
    boolean err_ = TRUE;

    if (BL_PendingWork_None == s_pendingWork)
    {
        s_pendingWork = BL_PendingWork_SetMode;

        *(uint8*)(uboot_info->share_mem_addr) &= 0x01;

        if( mode == BL_MODE_APP)    // Reset to APP mode.
        {
            swdl_magic_flag     = 0U;
        }
        else    // reset to SWDL
        {
            swdl_magic_flag     = MAGIC_FLAG_VAL;
        }

        TRACE_VALUE(LOG_SHOUT, MOD_SWDL, "Enter to other module, MAGIC_FLAG = %x", swdl_magic_flag);

        HalOS_SemaphorePost(s_blSema);

        err_ = FALSE;
    }

    return err_;
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static boolean BOOTLOADER_EraseFlash(void)
{
    boolean err_ = TRUE;

    TRACE_TEXT(LOG_INFO, MOD_SWDL, "start erase pflash");

    uint32 start_addr;
    uint32 end_addr;

#ifdef __APP_
    start_addr        = (uboot_info->swdl_partition_start_addr / SECTOR_SIZE)*SECTOR_SIZE;
    end_addr          = ((uboot_info->swdl_partition_end_addr + 1U - PATN_INFO_SIZE)/SECTOR_SIZE) * SECTOR_SIZE;
#else    // swdl mode
    start_addr        = (uboot_info->app_partition_start_addr / SECTOR_SIZE)*SECTOR_SIZE;
    end_addr          = ((uboot_info->app_partition_end_addr + 1U - PATN_INFO_SIZE)/SECTOR_SIZE) * SECTOR_SIZE;
#endif

    ServWdt_ForcePetWdt();

    //    first, erase PATTERN sector
    if(SECTOR_SIZE == HalFlash_Erase(end_addr, SECTOR_SIZE))
    {
        //    then, erase other APP sectors
        while(start_addr < end_addr)
        {
            if(SECTOR_SIZE == HalFlash_Erase(start_addr, SECTOR_SIZE))
            {
                start_addr += SECTOR_SIZE;
            }
            else
            {
                break;
            }

            ServWdt_ForcePetWdt();
        }

        if (start_addr >= end_addr)
        {
            err_ = FALSE;
        }
    }

    ServWdt_ForcePetWdt();

    if (err_ == TRUE)
    {
        TRACE_TEXT(LOG_INFO, MOD_SWDL, "erase pflash error");
    }
    else
    {
        TRACE_TEXT(LOG_INFO, MOD_SWDL, "erase pflash success");
    }

    return err_;
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static uint8_t BOOTLOADER_FrameCheckSum(uint16_t len, const uint8_t *data)
{
    uint8_t checksum = 0;

    while (len--)
    {
        checksum += *data;
        data++;
    }

    return checksum;
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static boolean BOOTLOADER_ProgramData(uint16_t page, uint16_t size, const uint8_t *data)
{
    boolean err_ = TRUE;
    uint32_t addr;

    uint32 part_start,part_end;

#ifdef __APP_
    part_start = uboot_info->swdl_partition_start_addr;
    part_end   = uboot_info->swdl_partition_end_addr + 1U;
#else
    part_start = uboot_info->app_partition_start_addr;
    part_end   = uboot_info->app_partition_end_addr + 1U;
#endif

    addr = part_start + PAGE_TO_ADDR(page);

    if((size % PROG_UNIT_SIZE)    ==    0) /* size should be 8-byte aligned. */
    {
        while ((size > 0) && (addr + PROG_UNIT_SIZE) <= part_end)
        {
            if (addr == 0)
            {
                s_needErase = TRUE;
            }

            ServWdt_ForcePetWdt();

            if (PROG_UNIT_SIZE == HalFlash_ProgramData(addr, data, PROG_UNIT_SIZE))
            {
                size -= PROG_UNIT_SIZE;
                addr += PROG_UNIT_SIZE;
                data += PROG_UNIT_SIZE;
            }
            else
            {
                break;
            }
        }

        if (0 == size)
        {
            /* This is the last frame, verify the whole image and set available flag. */
            if (addr + PROG_UNIT_SIZE >= part_end)
            {
                err_ = BOOTLOADER_VerifyUpdate();
            }
            else
            {
                err_ = FALSE;
            }
        }
    }
    else
    {
        TRACE_TEXT(LOG_DEBUG, MOD_SWDL, "Program size should be 8-byte aligned.");
    }

    return err_;
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static boolean BOOTLOADER_FrameHandler(uint16_t len, const uint8_t * data)
{
    boolean err_ = TRUE;
    uint8_t checksum;

    s_frameStatus = BOOTLOADER_FRAME_ERROR;

    if ((len >= BOOTLOADER_MIN_FRAME_LEN) && (len <= BOOTLOADER_MAX_FRAME_LEN))
    {
        s_progPayloadLen = BOOTLOADER_GET_FRAME_PAYLOAD_LEN(data);
        s_progPage = BOOTLOADER_GET_FRAME_FLASH_PAGE(data);

        if (len == (s_progPayloadLen + 4))
        {
            checksum = BOOTLOADER_FrameCheckSum(len - 1, data);

            if (checksum != data[len-1])
            {
                TRACE_VALUE2(LOG_ERR, MOD_SWDL, "BOOTLOADER frame checksum error, should: %2x, actual: %2x", checksum, data[len-1]);
            }
            else
            {
                if (s_pendingWork == BL_PendingWork_None)
                {
                    s_pendingWork = BL_PendingWork_ProgramFrame;

                    /* Program started, set the upgrade status to not OK. */
                    s_upgradeStatus = BOOTLOADER_UPGRADE_ERROR;

                    memcpy(s_progFrameData, BOOTLOADER_GET_FRAME_PAYLOAD(data), s_progPayloadLen);

                    HalOS_SemaphorePost(s_blSema);

                    err_ = FALSE;
                }
                else
                {
                    TRACE_TEXT(LOG_ERR, MOD_SWDL, "Previous programing on-going.");
                }
            }
        }
    }

    if (err_ == TRUE)
    {
        s_frameStatus = BOOTLOADER_FRAME_ERROR;
    }

    return err_;
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static boolean BOOTLOADER_FrameStatusHandler(uint16_t len, const uint8_t *data)
{
    boolean err_ = TRUE;

    if (0 == len)
    {
        su1TxMsgBuf[0] = s_frameStatus;
        HalIic_SLV_SetTxData(host_i2c, su1TxMsgBuf, 1);
        err_ = FALSE;
    }

    return err_;
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static boolean BOOTLOADER_VerifyUpdate(void)
{
    boolean err_ = TRUE;
    boolean fw_ok;
    uint8_t aval_flag[PROG_UNIT_SIZE];
    uint32_t part_end_addr;

    HalFlash_Sync();    // sync flash first, clear flash cache

#ifdef __APP_
    fw_ok            =    ProcChkSwdlFwOk();
    part_end_addr    =    uboot_info->swdl_partition_end_addr;
#else    // in swdl mode, check app
    fw_ok            =    ProcChkAppFwOk();
    part_end_addr    =    uboot_info->app_partition_end_addr;
#endif

    ServWdt_ForcePetWdt();

    if(fw_ok == TRUE)
    {
        aval_flag[0]    =    0xFF;
        aval_flag[1]    =    0xFF;
        aval_flag[2]    =    0xFF;
        aval_flag[3]    =    0xFF;
        aval_flag[4]    =    (uint8) (AVAL_FLAG_VAL & 0xFFU);
        aval_flag[5]    =    (uint8) ((AVAL_FLAG_VAL >> 8)  & 0xFFU);
        aval_flag[6]    =    (uint8) ((AVAL_FLAG_VAL >> 16) & 0xFFU);
        aval_flag[7]    =    (uint8) ((AVAL_FLAG_VAL >> 24) & 0xFFU);

        if (PROG_UNIT_SIZE == HalFlash_ProgramData((part_end_addr - 15U), aval_flag, PROG_UNIT_SIZE))
        {
            /* Upgrade finished successfully. */
            s_upgradeStatus = BOOTLOADER_UPGRADE_OK;
            err_ = FALSE;
        }

        ServWdt_ForcePetWdt();
    }
    else
    {
        TRACE_TEXT(LOG_ERR, MOD_SWDL, "New firmware verify failed");
    }

    return err_;
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static boolean BOOTLOADER_UpgradeStatusHandler(uint16_t len, const uint8_t *data)
{
    boolean err_ = TRUE;

    if(len == 0)
    {
        su1TxMsgBuf[0] = s_upgradeStatus;
        HalIic_SLV_SetTxData(host_i2c, su1TxMsgBuf, 1);
        err_ = FALSE;
    }

    return err_;
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void ProcCalcMd5Sum(uint32 start, uint32 end, uint8 * md5)
{
    uint32 addr = start;
    uint32 len  = end - start + 1U;
    MD5_CTX sstuMD5;

    if(len > 0)
    {
        MD5Init(&sstuMD5);
        ServWdt_ForcePetWdt();

        while(len > 0)
        {
            if(len >= 0x400)
            {
                MD5Update(&sstuMD5, (const unsigned char *) addr, 0x400);
                len  -= 0x400;
                addr += 0x400;
            }
            else
            {
                MD5Update(&sstuMD5, (const unsigned char *) addr, len);
                len = 0;
            }
            ServWdt_ForcePetWdt();
        }

        MD5Final(&sstuMD5);
        ServWdt_ForcePetWdt();

        for(uint8 i = 0; i < 16; i += 1)
        {
            md5[i] = sstuMD5.digest[i];
        }
    }
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static boolean ChkMd5Ok(uint32 start, uint32 end, uint8 const* md5)
{
    boolean ok_ = FALSE;
    if ( md5 != NULL )
    {
        uint8 calc_md5[16];
        ProcCalcMd5Sum(start,end,calc_md5);

        ok_ = TRUE;
        for(uint8 i = 0; i < 16; i += 1)
        {
            if( calc_md5[i] != md5[i])
            {
                ok_ =    FALSE;
                break;
            }
        }

    }
    return ok_;
}
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#ifdef __APP_
static boolean ProcChkSwdlFwOk(void)
{
    boolean ok_= FALSE;

    if( (uint32_t)swdl_info != 0x00 && (uint32_t)swdl_info != 0xFFFFFFFF
            && swdl_info->swdl_code_start_addr >= uboot_info->swdl_partition_start_addr && swdl_info->swdl_code_end_addr <= uboot_info->swdl_partition_end_addr
            && uboot_info->swdl_partition_start_addr ==    swdl_info->swdl_partition_start_addr && uboot_info->swdl_partition_end_addr ==    swdl_info->swdl_partition_end_addr)
    {
        ok_ = ChkMd5Ok(swdl_info->swdl_code_start_addr,swdl_info->swdl_code_end_addr,swdl_info->md5);
    }
    return ok_;
}
#else
static boolean ProcChkAppFwOk(void)
{
    boolean ok_= FALSE;

    if( (uint32_t)app_info != 0x00 && (uint32_t)app_info != 0xFFFFFFFF
            && app_info->app_code_start_addr >= uboot_info->app_partition_start_addr && app_info->app_code_end_addr <= uboot_info->app_partition_end_addr
            && uboot_info->app_partition_start_addr ==    app_info->app_partition_start_addr && uboot_info->app_partition_end_addr == app_info->app_partition_end_addr)
    {
        ok_ = ChkMd5Ok(app_info->app_code_start_addr,app_info->app_code_end_addr,app_info->md5);
    }
    return ok_;
}
#endif
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

/** >>>>>>>>>>>>>>>>> end of file >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
