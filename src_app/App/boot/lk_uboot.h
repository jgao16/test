/*
 * lk_uboot.h
 *
 *  Created on: 2017-07-10
 *      Author: Li Zhuohan
 */

#ifndef __LK_UBOOT_H_
#define __LK_UBOOT_H_

#include	"std_type.h"

typedef	void	(* LK_PGM_VOID)	(void);

typedef	struct
{
	uint32	app_info_addr;			//		
	uint32	swdl_info_addr;			//		

	//	enter to swdl or app flag address
	uint32	fbl_magic_flg_addr;

	uint32	uboot_partition_start_addr;		//		
	uint32	uboot_partition_end_addr;		//		
	uint32	swdl_partition_start_addr;		//		
	uint32	swdl_partition_end_addr;		//		
	uint32	app_partition_start_addr;		//		
	uint32	app_partition_end_addr;			//		

	//	version: not used. */
	uint32	version;
	//	software module id, reserved, not used
	uint32	share_mem_addr;
	//	software build time: utc time. second
	uint32	build_tm;
	//	md5 check sum
	uint8	md5[16];
}UBOOT_INFO_TypeDef;		//	@ 0x1FC0

typedef	struct
{
	uint8	reserved[52];
	uint32	avbl_flag;		//	=	0xAA5555AA: application is available
	uint32	swdl_time;		//	the time of re-flash app, this time will be received from SPI
	uint32	swdl_cnt;		//	the counter of re-flash APP: +1 at each re-flash app
}PATTERN_INFO_TypeDef;

typedef	struct
{
	//	SWDL md5 sum
	uint8	md5[16];
	//	software build time: utc time. second
	uint32	build_tm;
	//	version: 0x04030201 = V1.2.3.4
	uint32	version;
	//	software module id, reserved, not used
	uint32	swmi;
	//	swdl program vector table addr
	uint32	swdl_program_start;
	//	enter to boot or app flag address
	uint32	fbl_magic_flg_addr;

	/*	code start is used for md5 calculation	*/
	uint32	swdl_code_start_addr;		//	=	0x2000 + 16(md5)
	uint32	swdl_code_end_addr;			//	=	0xFFFF - 64(PATN_INFO)

	uint32	swdl_partition_start_addr;	//	=	0x2000
	uint32	swdl_partition_end_addr;	//	=	0xFFFF
	uint8	reserved[12];
}SWDL_INFO_TypeDef;		//	@UBOOT_INFO.swdl_info_addr, @0x2000

typedef	struct
{
	//	APP	md5 sum
	uint8	md5[16];
	//	sw build time: utc time. second
	uint32	build_tm;
	//	version: 0x04030201 = V1.2.3.4
	uint32	version;
	//	software module id, reserved, not used
	uint32	swmi;
	//	APP program start address, boot need jump to this address when jump to APP
	uint32	app_program_start;
	//	enter to boot or app flag address
	uint32	fbl_magic_flg_addr;

	/*	APP code start is used for md5 calc	*/
	uint32	app_code_start_addr;			//	=	0x10000 + 16(md5)
	uint32	app_code_end_addr;				//	=	0x7FFFF - 64(PATN_INFO)

	uint32	app_partition_start_addr;		//	=	0x10000
	uint32	app_partition_end_addr;			//	=	0x7FFFF
	uint8	reserved[12];
}APP_INFO_TypeDef;		//	@UBOOT_INFO.app_info_addr,	@0x10000

#define	BOOT_INFO_SIZE				((uint32) sizeof(UBOOT_INFO_TypeDef))
#define	SWDL_INFO_SIZE				((uint32) sizeof(SWDL_INFO_TypeDef))
#define	APP_INFO_SIZE				((uint32) sizeof(APP_INFO_TypeDef))
#define	PATN_INFO_SIZE				((uint32) sizeof(PATTERN_INFO_TypeDef))

#define	UBOOT_INFO_START_ADDR		(0x1FC0U)
#define	UBOOT_INFO					((const UBOOT_INFO_TypeDef *)	UBOOT_INFO_START_ADDR)



//#define	MAGIC_FLAG_ADDR				(0x20006FFCU)
#define	MAGIC_FLAG_VAL				(0xAA5555AAU)
#define	AVAL_FLAG_VAL				(0xAA5555AAU)


#define	uboot_info				(UBOOT_INFO)
#define	app_info				((APP_INFO_TypeDef*)(uboot_info->app_info_addr))
#define swdl_info				((SWDL_INFO_TypeDef*)(uboot_info->swdl_info_addr))
#define	app_patn				((PATTERN_INFO_TypeDef*)(uboot_info->app_partition_end_addr+1U-PATN_INFO_SIZE))
#define	swdl_patn				((PATTERN_INFO_TypeDef*)(uboot_info->swdl_partition_end_addr+1U-PATN_INFO_SIZE))
#define swdl_magic_flag  			(*(uint32*)(uboot_info->fbl_magic_flg_addr))


#define	USING_BACKUPIMAGE	(0)	// byte 0 , using backup partation
#define	IMAGE_STATE_		(1)	// byte 1 , image is break or not
#define	NORMAL_ROOTFS_BIT	(0x01)
#define	NORMAL_RAMDISK_BIT	(0x02)
#define	BACKUP_ROOTFS_BIT	(0x04)
#define	BACKUP_RAMDISK_BIT	(0x08)

#define AppSwdlShareMem				((uint8*)(uboot_info->share_mem_addr))
#define	UbootJumpFromOther()		(((uint8*)(uboot_info->share_mem_addr))[251] == 0xAA)
#define	SetUbootJumpFlag()			do{((uint8*)(uboot_info->share_mem_addr))[251] = 0xAA;}while(0)
#define	ClrUbootJumpFlag()			do{((uint8*)(uboot_info->share_mem_addr))[251] = 0x00;}while(0)

#define ClrAllShareMem()			do{for(uint8 i =0;i<32;i++){((uint32*)(uboot_info->share_mem_addr))[i] = 0x00;}}while(0)

#endif /* __LK_UBOOT_H_ */


