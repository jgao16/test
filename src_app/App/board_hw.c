

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  **/
#include "std_type.h"

#include "cpu.h"
#include "hal_os.h"
#include "pwrctrl.h"



/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  **/
#define portNVIC_SYSTICK_CTRL_REG		( * ( ( volatile uint32 * ) 0xe000e010 ) )
#define portNVIC_SYSTICK_CLK_BIT		4u
#define portNVIC_SYSTICK_ENABLE_BIT		1u

/* disable system tick timer	*/
#define	DisableSysTickTm()	do{ portNVIC_SYSTICK_CTRL_REG  &= ~(portNVIC_SYSTICK_CLK_BIT | portNVIC_SYSTICK_ENABLE_BIT);}while(0)

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  **/
/* enable cpu to 32MHz and tick */
extern void vPortSetupTimerInterrupt( void );	//lint -e830 -e526

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  **/
static boolean BoardHw_ReqHp(void);
static void BoardHw_EnterHp(void);
static void BoardHw_ExitHp(void);
static void BoardHw_EnterMp(void);
static void BoardHw_ExitMp(void);
static void BoardHw_EnterLp(void);
static void BoardHw_ExitLp(void);
static void BoardHw_EnterSleep(void);
static void BoardHw_ExitSleep(void);
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  **/
static const POWER_API power_api = 
{
	.hp_required = BoardHw_ReqHp,
	.enter_hp	 = BoardHw_EnterHp,
	.exit_hp	 = BoardHw_ExitHp,
	.enter_mp	 = BoardHw_EnterMp,
	.exit_mp	 = BoardHw_ExitMp,
	.enter_lp	 = BoardHw_EnterLp,
	.exit_lp	 = BoardHw_ExitLp,
	.enter_sleep = BoardHw_EnterSleep,
	.exit_sleep  = BoardHw_ExitSleep,
};


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  **/
static boolean BoardHw_ReqHp(void)
{
	return TRUE;
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  **/
void 		BoardHw_Init(void)
{

}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  **/
POWER_API const * BoardHw_GetPwrCtrlApi(void)
{
	return &power_api;
	//return NULL;
}



/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  **/
static void BoardHw_EnterHp(void)
{
	vPortSetupTimerInterrupt();
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  **/
static void BoardHw_ExitHp(void)
{
	/* disabel tick 			*/
	DisableSysTickTm();
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  **/
static void BoardHw_EnterMp(void)
{
	vPortSetupTimerInterrupt();
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  **/
static void BoardHw_ExitMp(void)
{
	/* disabel tick 			*/
	DisableSysTickTm();
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  **/
static void BoardHw_EnterLp(void)
{
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  **/
static void BoardHw_ExitLp(void)
{
}



/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  **/
static void BoardHw_EnterSleep(void)
{
	//PWR_EnterSleepMode(PWR_Regulator_ON,PWR_SLEEPEntry_WFI);
	//PWR_PVDCmd ( DISABLE );											
	//PWR_UltraLowPowerCmd(ENABLE);       							
	//PWR_FastWakeUpCmd(ENABLE);			 							
	//PWR_EnterSTOPMode(PWR_Regulator_LowPower, PWR_STOPEntry_WFI);
	//PWR_UltraLowPowerCmd(DISABLE);       							
	//PWR_FastWakeUpCmd(DISABLE);
	//PWR_PVDCmd(ENABLE);  
	
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  **/
static void BoardHw_ExitSleep(void)
{
}


