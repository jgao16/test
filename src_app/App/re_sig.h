#ifndef _RE_SIG_H_
#define _RE_SIG_H_


#define DEV_SIG_WakeUpReason_BATTERY_ON (0u)
#define DEV_SIG_WakeUpReason_SACKERR    (1u)
#define DEV_SIG_WakeUpReason_MDMAP  (2u)
#define DEV_SIG_WakeUpReason_LOCKUP (3u)
#define DEV_SIG_WakeUpReason_LOL    (4u)
#define DEV_SIG_WakeUpReason_LOC    (5u)
#define DEV_SIG_WakeUpReason_VIP_WDT    (6u)
#define DEV_SIG_WakeUpReason_VIP_SW_REQ (7u)
#define DEV_SIG_WakeUpReason_UNKONWN    (0xFF)

#endif