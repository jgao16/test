#if <<LIST_LEN>> > 1
void GenEep_Get_<<LIST_NAME>>(<<LIST_TYPE>> * const <<LIST_NAME>> );
void GenEep_Get_<<LIST_NAME>>_Ex(uint8_t * const <<LIST_NAME>>,uint16_t const offset,uint16_t const len );
void GenEep_Set_<<LIST_NAME>>(<<LIST_TYPE>> const* <<LIST_NAME>> );
void GenEep_Set_<<LIST_NAME>>_Ex(uint8_t const* <<LIST_NAME>>,uint16_t const offset,uint16_t const len );
#else 
<<LIST_TYPE>> GenEep_Get_<<LIST_NAME>>(void);
void GenEep_Set_<<LIST_NAME>>(<<LIST_TYPE>> const <<LIST_NAME>>);
#endif
