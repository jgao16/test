/*******************************************************************************
 * Project         Panda pluse
 * (c) Copyright   2017
 * Company         Visteon Asia Pacific (Shanghai), inc
 *                 All rights reserved.
 * @file           dev_sig.h
 * @module         device signal 
 * @author         Chao Wang
 * @brief          device handle process
 * @changelog      1. Creat File
********************************************************************************/
#ifndef DEV_EEP_H_H_
#define DEV_EEP_H_H_

#ifdef __cplusplus
extern "C" {
#endif
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#include"gen_dev_eep.h"

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

typedef void * EEP_HANDLE;

<<EEP_INJECT_HANDLE_TYPE>>

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


EEP_HANDLE DevEep_Init(void);

<<INJECT_HANDLE_TYPE_DECLEAR>>

//<<SIG_AP_TX_API>>

#ifdef __cplusplus
}
#endif


#endif


