/*******************************************************************************
 * Project         Panada Pluse
 * (c) Copyright   2017
 * Company         Visteon Asia Pacific (Shanghai), inc
 *                 All rights reserved.
 * @file           dev_eep.c
 * @module         device signal 
 * @author         Chao Wang
 * @brief          device handle process
 * @changelog      1. Creat File
********************************************************************************/
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <pthread.h>
#include <semaphore.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <string.h>
#include <stddef.h>
#include "dev_eep.h"
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

#define	AP_WRITE_EEP_LIST_CMD		(0x21)

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
typedef struct
{
	int 			dev_fd;
	pthread_t 		read_thread;
	pthread_t 		invoke_thread;
	
	pthread_mutex_t  mutex;     	/* mutex: for set callbk_invoke and callback handle   */
	sem_t            invoke_sem;	/* for activate invoke semphore 					  */
	uint8_t 		 invoke_task;
	
	uint8_t			all_eep_get;
	
	EEPROM_ST		current_eep;
	uint8_t			calbk_req[EEP_CONTENT_LIST_NUMBER_MAX];
	void * 			callback[EEP_CONTENT_LIST_NUMBER_MAX];	// callback eeprom

	EEPROM_ST		process_eep;
	uint8_t			proc_req[EEP_CONTENT_LIST_NUMBER_MAX];
	void * 			proc_calbk[EEP_CONTENT_LIST_NUMBER_MAX];	// callback eeprom
}DEV_EEP_AP;

typedef struct
{
	uint16_t list_id;
	uint16_t list_len;	// byte len 
	uint16_t offset;	// offset 

	void (*callback)(DEV_EEP_AP *ap,void *callbk);
}EEP_LIST_INFO;
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
/*****************************************************************************************************************************/
static void *dev_eep_thread_decode(void *arg);
static void *dev_eep_thread_execute(void *arg);

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
<<DEV_EEP_LIST_FUNCTION_DEFINE>>
//<<SIG_TX_FUNCTION_DEFINE>>

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static EEP_LIST_INFO const eep_list_info[EEP_CONTENT_LIST_NUMBER_MAX]=
{
<<EEP_LIST_INFO_TBL>>};
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
EEP_HANDLE DevEep_Init(void)
{
	DEV_EEP_AP * ap_rx =(DEV_EEP_AP*)malloc(sizeof(DEV_EEP_AP));

	if ( ap_rx != NULL )
	{
		memset(ap_rx,0,sizeof(DEV_EEP_AP));
		
		(void)sem_init(&(ap_rx->invoke_sem), 0, 0);	
		pthread_mutex_init(&(ap_rx->mutex),NULL);
		
		ap_rx->dev_fd = open("/dev/ipc/eeprom", O_RDWR);
		
		(void)pthread_create(&(ap_rx->read_thread),  NULL,dev_eep_thread_decode, ap_rx);
	}
	
	return ap_rx;
}
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void *dev_eep_thread_decode(void *arg)
{
	DEV_EEP_AP *ap_rx = (DEV_EEP_AP*)arg;
	uint8_t *rx_msg 	 = 	malloc(8192);
	while(1)
	{
		int len = read(ap_rx->dev_fd,rx_msg,4096);
		
		if ( len > 2 )
		{
			if ( rx_msg[0] == 0x83 )
			{	//(vip->imx), transfer all eeprom to imx
				if ( len == sizeof(EEPROM_ST)+1 )
				{
					/* process all eeprom content */
					if ( pthread_mutex_lock(&(ap_rx->mutex)) == 0 )
					{
						for(uint16_t i = 0; i < EEP_CONTENT_LIST_NUMBER_MAX; i++)
						{
							EEP_LIST_INFO const *info = eep_list_info +i;
							uint8_t *src    = rx_msg + 1 + info->offset;
							uint8_t *desc   = (uint8_t*)&ap_rx->current_eep + info->offset;
							for(uint16_t j=0; j < info->list_len; j++)
							{
								if ( desc[j] != src[j] )
								{
									desc[j] = src[j];
									
									ap_rx->calbk_req[i] = 0x01;
									if ( ap_rx->invoke_task == 0x00 )
									{
										ap_rx->invoke_task = 0x01;
										(void)sem_post(&(ap_rx->invoke_sem));
									}									
								}
							}
						}
						memcpy(&ap_rx->current_eep,rx_msg+1,sizeof(EEPROM_ST));
						(void)pthread_mutex_unlock(&(ap_rx->mutex));
					}
				}
			}
			else if ( rx_msg[0] == 0xA0 )
			{	// transfer eeprom to imx by multi eeprom list id
				/* check list and len is ok */
				uint8_t 		cmd_ok 		= 0x01;
				uint16_t  	  	check_rx 	= len-1;
				uint8_t const * cmd 	    = rx_msg+1;
				uint16_t	 	list;
				/* check command ok first*/
				while ( check_rx > 2 && cmd_ok )
				{
					list = 		*cmd++;
					list <<= 	8;
					list += 	*cmd++;
					check_rx  -= 	2;
					if ( list < EEP_CONTENT_LIST_NUMBER_MAX )
					{
						EEP_LIST_INFO const * info = eep_list_info + list; 
						if ( info->list_len <= check_rx )
						{
							check_rx 	-= info->list_len;
							cmd 	+= info->list_len;
						}
						else
						{
							cmd_ok = 0x00;
						}
					}
					else
					{
						cmd_ok = 0x00;
					}
				}
				
				if ( check_rx == 0x00 && cmd_ok )
				{
					if ( pthread_mutex_lock(&(ap_rx->mutex)) == 0 )
					{
						check_rx = len-1;
						cmd 	 = rx_msg+1;
						while ( check_rx > 2  )
						{
							list 		= 	*cmd++;
							list 		<<= 8;
							list 		+= 	*cmd++;
							check_rx  	-= 	2;

							if ( list < EEP_CONTENT_LIST_NUMBER_MAX )
							{
								EEP_LIST_INFO const * info = eep_list_info + list; 
								if ( info->list_len <= check_rx )
								{
									check_rx -= info->list_len;

									uint8_t *desc   = (uint8_t*)&ap_rx->current_eep + info->offset;
									for(uint16_t j=0; j < info->list_len; j++)
									{
										uint8_t val = *cmd++;
										if ( desc[j] != val )
										{
											desc[j] = val;
											ap_rx->calbk_req[list] = 0x01;
											if ( ap_rx->invoke_task == 0x00 )
											{
												ap_rx->invoke_task = 0x01;
												(void)sem_post(&(ap_rx->invoke_sem));
											}									
										}
									}
								}
								else
								{
									check_rx = 0x00;
								}
							}
							else
							{
								check_rx = 0x00;
							}
						}
						(void)pthread_mutex_unlock(&(ap_rx->mutex));
					}
				}
			}
			else
			{}//other command, not support now 
			
			/* check if 0x83 command received: all eeprom content */
			if ( ap_rx->all_eep_get == 0x00 && rx_msg[0] == 0x83 && len == sizeof(EEPROM_ST)+1 )
			{
				ap_rx->all_eep_get = 0x01;
				if ( pthread_mutex_lock(&(ap_rx->mutex)) == 0 )
				{	
					/* set all callback require */
					memset(ap_rx->calbk_req,0x01,EEP_CONTENT_LIST_NUMBER_MAX);
					(void)pthread_mutex_unlock(&(ap_rx->mutex));	
				}
				(void)sem_post(&(ap_rx->invoke_sem));
				(void)pthread_create(&(ap_rx->invoke_thread),NULL,dev_eep_thread_execute,ap_rx);
			}
		}
	}

	return NULL;
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void *dev_eep_thread_execute(void *arg)
{
	DEV_EEP_AP *ap_rx 	= (DEV_EEP_AP*)arg;

	while(1)
	{
		(void)sem_wait(&(ap_rx->invoke_sem));
		

		if ( pthread_mutex_lock(&(ap_rx->mutex)) == 0 )
		{
			ap_rx->invoke_task = 0x00;
			
			for( uint16_t i = 0; i < EEP_CONTENT_LIST_NUMBER_MAX; i++ )
			{
				if ( ap_rx->calbk_req[i] )
				{	/*  copy callback information */
					ap_rx->proc_req[i]   = ap_rx->calbk_req[i];
					ap_rx->calbk_req[i]  = 0x00;
					ap_rx->proc_calbk[i] = ap_rx->callback[i];
					
					EEP_LIST_INFO const * info = eep_list_info + i; 
					uint8_t *src  = (uint8_t*)&ap_rx->current_eep + info->offset;
					uint8_t *desc = (uint8_t*)&ap_rx->process_eep + info->offset;
					memcpy(desc, src, info->list_len);
				}
				else
				{
					ap_rx->proc_req[i]   = 0x00; 
				}
			}
			(void)pthread_mutex_unlock(&(ap_rx->mutex));
			
			for ( uint16_t i = 0; i < EEP_CONTENT_LIST_NUMBER_MAX; i++ )
			{
				if( ap_rx->proc_req[i] )
				{	// invoke callback 
					//invoke_callback_handl[i](ap_rx);
					eep_list_info[i].callback(ap_rx,ap_rx->proc_calbk[i]);
				}
			}
		}
	}
	return NULL;
}
/** >>>>>>>> end of file  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


