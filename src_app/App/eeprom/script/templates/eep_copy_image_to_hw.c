	case EEP_BLOCK_<<BLOCK_NAME>>_BLOCK_ID:
#if EEP_BLOCK_<<BLOCK_NAME>>_IS_EEPROM
		if ( !HalEep_IsBusy() )
		{
			if ( wr_eep_index < EEP_BLOCK_<<BLOCK_NAME>>_LEN )
			{
				uint8 	eep_hw[EEP_PROGRAM_SIZE];
				boolean error = FALSE;
				
				uint16 hw_addr;
				if ( wr_eep_back ){
					hw_addr = (uint16)(wr_eep_index+EEP_BLOCK_<<BLOCK_NAME>>_BACKUP_ADDR);
				}else{
					hw_addr = (uint16)(wr_eep_index+EEP_BLOCK_<<BLOCK_NAME>>_NORMAL_ADDR);
				}
				
				if ( EEP_PROGRAM_SIZE == HalEep_Read(hw_addr, eep_hw, EEP_PROGRAM_SIZE) )
				{	// read ok
					uint8 * eep_image 	= (uint8_t*)&eep_hw_image.<<BLOCK_NAME>>_write_count + wr_eep_index;					
					for ( uint16 i = 0; i < EEP_PROGRAM_SIZE; i ++ )
					{	
						if ( eep_image[i] != eep_hw[i] )
						{	
							if ( EEP_PROGRAM_SIZE != HalEep_Write(hw_addr,eep_image, EEP_PROGRAM_SIZE) )
							{
								error = TRUE;
								TRACE_VALUE(EEP_ERR,MOD_EEPROM,"HalEep_Write Block:<<BLOCK_NAME>> Error, retry count=%d",wr_hw_try_cnt);
							}	
							break;
						}
					}
				}
				else
				{
					error = TRUE;
					TRACE_VALUE(EEP_ERR,MOD_EEPROM,"HalEep_Read Block:<<BLOCK_NAME>> Error, retry count=%d",wr_hw_try_cnt);
				}
				
				if ( error && wr_hw_try_cnt < 33 )
				{
					wr_eep_index ++;
				}
				else
				{
					wr_eep_index 	+=  EEP_PROGRAM_SIZE;
					wr_hw_try_cnt	= 	0x00;
				}
			}
			else if ( !wr_eep_back )
			{
				wr_eep_back  = 	TRUE;
				wr_eep_index =	0x00;
			}
			else
			{	// process success
				process_block = 0xFF;
				TRACE_VALUE(EEP_WRITE_BLOCK,MOD_EEPROM,"Write Eep Block:<<BLOCK_NAME>> to hw complete, write count=%d",eep_hw_image.<<BLOCK_NAME>>_write_count);
			}
		}	
#else	// !EEP_BLOCK_<<BLOCK_NAME>>_IS_EEPROM
		if ( !HalDFlash_IsBusy() )
		{
			if ( wr_eep_index < EEP_BLOCK_<<BLOCK_NAME>>_LEN )
			{
				boolean dflash_is_blank = TRUE;
				if ( wr_eep_index == 0x00 )
				{	/* check if need erase dflash sector */
					if ( (dflash_<<BLOCK_NAME>>_current_write_addr - EEP_BLOCK_<<BLOCK_NAME>>_DFLASH_START_ADDR) % DFLASH_SECTOR_SIZE == 0x00 )
					{	// new sector start, need erase 
						uint32 temp;
						if ( HalDFlash_ReadData(dflash_<<BLOCK_NAME>>_current_write_addr, (uint8*)&temp, 4) != 4 )
						{	// this sector is error, change to next sector
							wr_eep_index				= 	0x00;
							wr_hw_try_cnt++;
							dflash_is_blank 			= 	FALSE;
							
							dflash_<<BLOCK_NAME>>_current_write_addr 	+= 	DFLASH_SECTOR_SIZE;	
							if ( dflash_<<BLOCK_NAME>>_current_write_addr >= EEP_BLOCK_<<BLOCK_NAME>>_DFLASH_END_ADDR )
							{
								dflash_<<BLOCK_NAME>>_current_write_addr = EEP_BLOCK_<<BLOCK_NAME>>_DFLASH_START_ADDR;
							}
						}
						else if ( temp != 0xFFFFFFFF )
						{
							(void)HalDFlash_Erase(dflash_<<BLOCK_NAME>>_current_write_addr, 0);	//len is not used
							dflash_is_blank = FALSE;
							TRACE_VALUE(EEP_ERR,MOD_EEPROM,"Dflash Erase Block: <<BLOCK_NAME>> ,Address=0x%08x",dflash_<<BLOCK_NAME>>_current_write_addr+wr_eep_back);
						}
						else{}/* sector is blank*/
					}
				}

				if ( wr_hw_try_cnt < 33 )
				{
					if ( dflash_is_blank )
					{
						boolean last_prog_err = FALSE;
						if ( wr_eep_index )
						{	/* check last program */
							uint8 eep_hw[DFLASH_PROGRAM_SIZE];
							if ( HalDFlash_ReadData(dflash_<<BLOCK_NAME>>_current_write_addr+wr_eep_back, eep_hw, DFLASH_PROGRAM_SIZE) == DFLASH_PROGRAM_SIZE )
							{
								for ( uint8 i = 0; i < DFLASH_PROGRAM_SIZE; i ++ )
								{
									if ( eep_hw[i] != ((uint8_t*)&eep_hw_image.<<BLOCK_NAME>>_write_count+wr_eep_back)[i] )
									{
										last_prog_err = TRUE;
										TRACE_VALUE(EEP_ERR,MOD_EEPROM,"Dflash Program Block: <<BLOCK_NAME>> Error,Address=0x%08x",dflash_<<BLOCK_NAME>>_current_write_addr+wr_eep_back);
										break;
									}
								}
							}
							else
							{	// this block read error, use next block
								last_prog_err = TRUE;
								TRACE_VALUE(EEP_ERR,MOD_EEPROM,"Dflash Program Block: <<BLOCK_NAME>> Read error,Address=0x%08x",(dflash_<<BLOCK_NAME>>_current_write_addr+wr_eep_back));
							}
						}
						if ( !last_prog_err )
						{
							if ( DFLASH_PROGRAM_SIZE == HalDFlash_ProgramData(dflash_<<BLOCK_NAME>>_current_write_addr+wr_eep_index, (uint8_t*)&eep_hw_image.<<BLOCK_NAME>>_write_count+wr_eep_index, DFLASH_PROGRAM_SIZE) )
							{
								wr_eep_back 	= 	wr_eep_index;	// for read compare 
								wr_eep_index 	+= 	DFLASH_PROGRAM_SIZE;
							}
							else
							{
								last_prog_err = TRUE;
								TRACE_VALUE(EEP_ERR,MOD_EEPROM,"Dflash Program Block: <<BLOCK_NAME>>, HalDFlash_ProgramData error,Address=0x%08x",(dflash_<<BLOCK_NAME>>_current_write_addr+wr_eep_back));
							}
						}
						if(last_prog_err)
						{	// error, this block is error, use next sector 
							wr_hw_try_cnt ++;
							wr_eep_index = 0x00;
							wr_eep_back  = 0x00;
							
							dflash_<<BLOCK_NAME>>_current_write_addr 	+= 	EEP_BLOCK_<<BLOCK_NAME>>_LEN;	
							if ( dflash_<<BLOCK_NAME>>_current_write_addr >= EEP_BLOCK_<<BLOCK_NAME>>_DFLASH_END_ADDR )
							{
								dflash_<<BLOCK_NAME>>_current_write_addr = EEP_BLOCK_<<BLOCK_NAME>>_DFLASH_START_ADDR;
							}
						}
					}
				}
				
				if ( wr_hw_try_cnt >= 33 )
				{	// error too much, delete this write require
					process_block = 0xFF;
					TRACE_VALUE(EEP_ERR,MOD_EEPROM,"Dflash Program Block: <<BLOCK_NAME>>,addr=%08x error too much",dflash_<<BLOCK_NAME>>_current_write_addr);
				}
			}
			else if ( wr_eep_index != wr_eep_back )
			{	// check this block last write 
				boolean last_prog_err = FALSE;
				uint8 	eep_hw[DFLASH_PROGRAM_SIZE];
				if ( HalDFlash_ReadData(dflash_<<BLOCK_NAME>>_current_write_addr+wr_eep_back, eep_hw, DFLASH_PROGRAM_SIZE) == DFLASH_PROGRAM_SIZE )
				{
					for ( uint8 i = 0; i < DFLASH_PROGRAM_SIZE; i ++ )
					{
						if ( eep_hw[i] != ((uint8_t*)&eep_hw_image.<<BLOCK_NAME>>_write_count+wr_eep_back)[i] )
						{
							last_prog_err = TRUE;
							TRACE_VALUE(EEP_ERR,MOD_EEPROM,"Dflash Program Block: <<BLOCK_NAME>>, HalDFlash_ProgramData error,Address(last write)=0x%08x",(dflash_<<BLOCK_NAME>>_current_write_addr+wr_eep_back));
							break;
						}
					}
				}
				else
				{	// this block read error, use next block
					last_prog_err = TRUE;
					TRACE_VALUE(EEP_ERR,MOD_EEPROM,"Dflash Program Block: <<BLOCK_NAME>> Read(last read) error,Address=0x%08x",(dflash_<<BLOCK_NAME>>_current_write_addr+wr_eep_back));
				}
				
				wr_eep_index = 0x00;
				wr_eep_back  = 0x00;
				dflash_<<BLOCK_NAME>>_current_write_addr 	+= 	EEP_BLOCK_<<BLOCK_NAME>>_LEN;	
				if ( dflash_<<BLOCK_NAME>>_current_write_addr >= EEP_BLOCK_<<BLOCK_NAME>>_DFLASH_END_ADDR )
				{
					dflash_<<BLOCK_NAME>>_current_write_addr = EEP_BLOCK_<<BLOCK_NAME>>_DFLASH_START_ADDR;
				}
					
				if ( last_prog_err )
				{
					wr_hw_try_cnt ++;
					if ( wr_hw_try_cnt >= 33 )
					{	// error too much, delete this write require
						process_block 	= 0xFF;		
					}
				}
				else
				{	// last program ok 
					process_block 	= 0xFF;	
					TRACE_VALUE(EEP_ERR,MOD_EEPROM,"Dflash Program Block: <<BLOCK_NAME>> Complete, crc=0x%08x",eep_hw_image.<<BLOCK_NAME>>_crc);			
				}
			}
			else
			{	// process success
				process_block = 0xFF;
			}
		}
#endif		// {EEP_BLOCK_<<BLOCK_NAME>>_IS_EEPROM}
		break;
