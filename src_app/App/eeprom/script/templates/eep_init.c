static void EepBlock_<<BLOCK_NAME>>_Init(void)
{	
	/*lint -save -e831 -e662 -e661*/
#if EEP_BLOCK_<<BLOCK_NAME>>_IS_EEPROM
	uint32 crc ;
	uint8_t *src;
	uint8_t *desc;

	(void)HalEep_Read(EEP_BLOCK_<<BLOCK_NAME>>_NORMAL_ADDR,(uint8 *)(&eep_ram_image.<<BLOCK_NAME>>_write_count),EEP_BLOCK_<<BLOCK_NAME>>_LEN);
	crc = Crc32_Calc((uint8_t*)(&eep_ram_image.<<BLOCK_NAME>>_write_count),EEP_BLOCK_<<BLOCK_NAME>>_LEN-4);
	if ( crc != eep_ram_image.<<BLOCK_NAME>>_crc )
	{	// use backup data
		TRACE_TEXT(EEP_ERR,MOD_ALWAYS,"Eeprom Block:<<BLOCK_NAME>> Normal Content CRC error..");
		(void)HalEep_Read(EEP_BLOCK_<<BLOCK_NAME>>_BACKUP_ADDR,(uint8 *)(&eep_ram_image.<<BLOCK_NAME>>_write_count),EEP_BLOCK_<<BLOCK_NAME>>_LEN);
		crc = Crc32_Calc((uint8_t*)(&eep_ram_image.<<BLOCK_NAME>>_write_count),EEP_BLOCK_<<BLOCK_NAME>>_LEN-4);
		if ( crc !=  eep_ram_image.<<BLOCK_NAME>>_crc )
		{	// use default value
			TRACE_TEXT(EEP_ERR,MOD_ALWAYS,"Eeprom Block:<<BLOCK_NAME>> Backup Content CRC error, Use default value");
			desc = (uint8_t*)&eep_ram_image.<<BLOCK_NAME>>_write_count;
			src  = (uint8_t*)&dflt_eep_val.<<BLOCK_NAME>>_write_count;
			for( uint16 i = 0; i < EEP_BLOCK_<<BLOCK_NAME>>_LEN-4; i ++ )
			{
				desc[i] = src[i];
			}
			eep_ram_image.<<BLOCK_NAME>>_crc = Crc32_Calc((uint8_t*)(&eep_ram_image.<<BLOCK_NAME>>_write_count),EEP_BLOCK_<<BLOCK_NAME>>_LEN-4);
		}

		eep_write_block_req |= (1u << EEP_BLOCK_<<BLOCK_NAME>>_BLOCK_ID);
	}

	src = (uint8_t*)&eep_ram_image.<<BLOCK_NAME>>_write_count;
	desc= (uint8_t*)&eep_hw_image.<<BLOCK_NAME>>_write_count;
	for( uint16 i = 0; i < EEP_BLOCK_<<BLOCK_NAME>>_LEN; i ++ )
	{
		desc[i] = src[i];
	}
#else	//EEP_BLOCK_<<BLOCK_NAME>>_IS_EEPROM
	uint32_t 	write_count = 0;
	uint16_t	dflash_block_avbl_idx=0;
	uint32_t	crc;
	for( uint16_t i = 0; i < EEP_BLOCK_<<BLOCK_NAME>>_DFLASH_SECTOR_NUM*DFLASH_SECTOR_SIZE/EEP_BLOCK_<<BLOCK_NAME>>_LEN; i ++ )
	{
		(void)HalDFlash_ReadData(EEP_BLOCK_<<BLOCK_NAME>>_DFLASH_START_ADDR+i*EEP_BLOCK_<<BLOCK_NAME>>_LEN,(uint8 *)(&eep_ram_image.<<BLOCK_NAME>>_write_count),EEP_BLOCK_<<BLOCK_NAME>>_LEN);
		crc = Crc32_Calc((uint8_t*)(&eep_ram_image.<<BLOCK_NAME>>_write_count),EEP_BLOCK_<<BLOCK_NAME>>_LEN-4);
		if ( crc == eep_ram_image.<<BLOCK_NAME>>_crc )
		{	
			if ( write_count < eep_ram_image.<<BLOCK_NAME>>_write_count )
			{
				write_count 			= 	eep_ram_image.<<BLOCK_NAME>>_write_count;
				dflash_block_avbl_idx	=	i;
			}
		}
	}
	boolean dflash_restore_default = FALSE;
	if ( write_count )
	{
		dflash_<<BLOCK_NAME>>_current_write_addr = EEP_BLOCK_<<BLOCK_NAME>>_DFLASH_START_ADDR+dflash_block_avbl_idx*EEP_BLOCK_<<BLOCK_NAME>>_LEN;
		(void)HalDFlash_ReadData(dflash_<<BLOCK_NAME>>_current_write_addr,(uint8 *)(&eep_ram_image.<<BLOCK_NAME>>_write_count),EEP_BLOCK_<<BLOCK_NAME>>_LEN);
		/* check again */
		if(	eep_ram_image.<<BLOCK_NAME>>_crc == Crc32_Calc((uint8_t*)(&eep_ram_image.<<BLOCK_NAME>>_write_count),EEP_BLOCK_<<BLOCK_NAME>>_LEN-4) )
		{	/* re-check ok, */
			uint8_t *src;
			uint8_t *desc;
			src = (uint8_t*)&eep_ram_image.<<BLOCK_NAME>>_write_count;
			desc= (uint8_t*)&eep_hw_image.<<BLOCK_NAME>>_write_count;
			for( uint16_t i = 0; i < EEP_BLOCK_<<BLOCK_NAME>>_LEN; i ++ )
			{
				desc[i] = src[i];
			}
		}
		else
		{
			dflash_restore_default = TRUE;
		}
		
		dflash_<<BLOCK_NAME>>_current_write_addr 	+= 	EEP_BLOCK_<<BLOCK_NAME>>_LEN;	
		if ( dflash_<<BLOCK_NAME>>_current_write_addr >= EEP_BLOCK_<<BLOCK_NAME>>_DFLASH_END_ADDR )
		{
			dflash_<<BLOCK_NAME>>_current_write_addr = EEP_BLOCK_<<BLOCK_NAME>>_DFLASH_START_ADDR;
		}
	}
	else
	{
		dflash_restore_default = TRUE;
	}
	if ( dflash_restore_default )
	{
		uint8_t *src;
		uint8_t *desc;
		desc = (uint8_t*)&eep_ram_image.<<BLOCK_NAME>>_write_count;
		src  = (uint8_t*)&dflt_eep_val.<<BLOCK_NAME>>_write_count;
		for( uint16 i = 0; i < EEP_BLOCK_<<BLOCK_NAME>>_LEN-4; i ++ )
		{
			desc[i] = src[i];
		}	
		eep_ram_image.<<BLOCK_NAME>>_crc = Crc32_Calc((uint8_t*)(&eep_ram_image.<<BLOCK_NAME>>_write_count),EEP_BLOCK_<<BLOCK_NAME>>_LEN-4);
		src = (uint8_t*)&eep_ram_image.<<BLOCK_NAME>>_write_count;
		desc= (uint8_t*)&eep_hw_image.<<BLOCK_NAME>>_write_count;
		for( uint16_t i = 0; i < EEP_BLOCK_<<BLOCK_NAME>>_LEN; i ++ )
		{
			desc[i] = src[i];
		}

		TRACE_TEXT(EEP_ERR,MOD_ALWAYS,"Eeprom Block:<<BLOCK_NAME>> Dflash Error, Use default value");
		eep_write_block_req |= (1u << EEP_BLOCK_<<BLOCK_NAME>>_BLOCK_ID);
		dflash_<<BLOCK_NAME>>_current_write_addr = EEP_BLOCK_<<BLOCK_NAME>>_DFLASH_START_ADDR;
	}
#endif	//EEP_BLOCK_<<BLOCK_NAME>>_IS_EEPROM
	/*lint -restore */	
}

