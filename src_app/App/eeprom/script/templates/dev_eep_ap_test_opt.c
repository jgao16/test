static void DevEepTest_<<LIST_NAME>>(int argc, char **argv)
{
#if EEP_CONTENT_<<LIST_NAME>>_ITEM_LEN == 1
	#if EEP_CONTENT_<<LIST_NAME>>_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t <<LIST_NAME>>_val;
	#elif EEP_CONTENT_<<LIST_NAME>>_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t <<LIST_NAME>>_val;
	#else
	uint32_t <<LIST_NAME>>_val;
	#endif
	<<LIST_NAME>>_val = atoi(argv[2]);
#else // EEP_CONTENT_<<LIST_NAME>>_ITEM_LEN > 1
	#if EEP_CONTENT_<<LIST_NAME>>_ITEM_TYPE == EEP_ITEM_TYPE_8
	uint8_t <<LIST_NAME>>_val[EEP_CONTENT_<<LIST_NAME>>_ITEM_LEN];
	#elif EEP_CONTENT_<<LIST_NAME>>_ITEM_TYPE == EEP_ITEM_TYPE_16
	uint16_t <<LIST_NAME>>_val[EEP_CONTENT_<<LIST_NAME>>_ITEM_LEN];
	#else
	uint32_t <<LIST_NAME>>_val[EEP_CONTENT_<<LIST_NAME>>_ITEM_LEN];
	#endif
	for(uint16_t i = 0; i < EEP_CONTENT_<<LIST_NAME>>_ITEM_LEN; i ++ )
	{
		<<LIST_NAME>>_val[i] = atoi(argv[2+i]);
	}
#endif // {EEP_CONTENT_<<LIST_NAME>>_ITEM_LEN}	
	
	DevEep_Set_<<LIST_NAME>>(eep_handle,<<LIST_NAME>>_val);
}


#if EEP_CONTENT_<<LIST_NAME>>_ITEM_LEN == 1
#if EEP_CONTENT_<<LIST_NAME>>_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_<<LIST_NAME>>_Callback(EEP_HANDLE eep_handle,uint8_t const <<LIST_NAME>>_val)
#elif EEP_CONTENT_<<LIST_NAME>>_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_<<LIST_NAME>>_Callback(EEP_HANDLE eep_handle,uint16_t const <<LIST_NAME>>_val)
#else
void DevEepTest_<<LIST_NAME>>_Callback(EEP_HANDLE eep_handle,uint32_t const <<LIST_NAME>>_val)
#endif	
{
	
	printf("dev eep test: <<LIST_NAME>>, change to dec=%d,hex=0x%08x\r\n",<<LIST_NAME>>_val,<<LIST_NAME>>_val);
}
#else // EEP_CONTENT_<<LIST_NAME>>_ITEM_LEN > 1
#if EEP_CONTENT_<<LIST_NAME>>_ITEM_TYPE == EEP_ITEM_TYPE_8
void DevEepTest_<<LIST_NAME>>_Callback(EEP_HANDLE eep_handle,uint8_t const * <<LIST_NAME>>_val)
#elif EEP_CONTENT_<<LIST_NAME>>_ITEM_TYPE == EEP_ITEM_TYPE_16
void DevEepTest_<<LIST_NAME>>_Callback(EEP_HANDLE eep_handle,uint16_t const * <<LIST_NAME>>_val)
#else
void DevEepTest_<<LIST_NAME>>_Callback(EEP_HANDLE eep_handle,uint32_t const * <<LIST_NAME>>_val)
#endif
{
	printf("dev eep test: <<LIST_NAME>>, change to dec= ");
	for(uint16_t i = 0; i < EEP_CONTENT_<<LIST_NAME>>_ITEM_LEN; i ++)
	{
		printf("%d ", <<LIST_NAME>>_val[i]);
	}
	printf("  hex=");
	for(uint16_t i = 0; i < EEP_CONTENT_<<LIST_NAME>>_ITEM_LEN; i ++)
	{
		#if EEP_CONTENT_<<LIST_NAME>>_ITEM_TYPE == EEP_ITEM_TYPE_8
		printf("%02x ", <<LIST_NAME>>_val[i]);
		#elif EEP_CONTENT_<<LIST_NAME>>_ITEM_TYPE == EEP_ITEM_TYPE_16
		printf("%04x ", <<LIST_NAME>>_val[i]);
		#else
		printf("%08x ", <<LIST_NAME>>_val[i]);	
		#endif
	}
	printf("\r\n");
}
#endif

