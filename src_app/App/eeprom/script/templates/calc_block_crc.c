				case EEP_BLOCK_<<BLOCK_NAME>>_BLOCK_ID:
					eep_hw_image.<<BLOCK_NAME>>_write_count ++;
					TRACE_VALUE(EEP_WRITE_BLOCK,MOD_EEPROM,"Write Eep Block:<<BLOCK_NAME>> require, write count=%d",eep_hw_image.<<BLOCK_NAME>>_write_count);
					eep_hw_image.<<BLOCK_NAME>>_crc = Crc32_Calc((uint8_t const *)&eep_hw_image.<<BLOCK_NAME>>_write_count,EEP_BLOCK_<<BLOCK_NAME>>_LEN-4);
					break;
