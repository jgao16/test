#ifndef GEN_EEP_MANA_H__
#define GEN_EEP_MANA_H__
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

/*****this file is generate by perl**************************************************************************************** **/
#include"stdint.h"
// #include "dev_ipc.h"

#ifdef __cplusplus
extern "C" {
#endif

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
/*
	0x03									;			(imx->vip), read all eeprom command
 	0x83,xx,xx,xx,xx						;			(vip->imx), transfer all eeprom to imx
	
	
	(imx->vip), read eeprom by offset
	0x04,offset_h,offset_l, len_h, len_l,(offset_h,offset_l, len_h, len_l);	
	
	(vip->imx), transfer eeprom to imx by offset	
	0x84,offset_h,offset_l, len_h, len_l, xx xx xx xx,(offset_h,offset_l, len_h, len_l, xx xx xx xx);	

	
	(imx->vip), write eeprom by offset
	0x05,offset_h,offset_l, len_h, len_l, xx xx xx xx,(offset_h,offset_l, len_h, len_l, xx xx xx xx);
	
	(vip->imx), write eeprom by offset success 
	0x85,offset_h,offset_l, len_h, len_l, (offset_h,offset_l, len_h, len_l)			 ;	
	
	
	
	0x20,eep_list_id0_h eep_list_id0_l eep_list_id1_h eep_list_id1_l xx xx ;				(imx->vip), read eeprom by eeprom multi list id
	0xA0,eep_list_id0_h,eep_list_id0_l xx xx xx xx xx,eep_list_id1_h eep_list_id1_l xx xx ;	(vip->imx), transfer eeprom to imx by multi eeprom list id 
	
	
	0x21,eep_list_id0_h,eep_list_id0_l xx xx xx xx xx,eep_list_id1_h eep_list_id1_l xx xx;		(imx->vip), write eeprom by eeprom list id
	0xA1,eep_list_id0_h eep_list_id0_l eep_list_id1_h eep_list_id1_l xx xx ;					(vip->imx), write eeprom by list id success 
2.	
*/

//<<INCLUDE>>

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#define EEP_ITEM_TYPE_8			(0)
#define EEP_ITEM_TYPE_16		(1)
#define EEP_ITEM_TYPE_32		(2)

<<DEFINE>>

#define EEP_CONT_LIST_FLAG_ARRAY_NUM ((EEP_CONTENT_LIST_NUMBER_MAX+31)/32)

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/



typedef struct
{
	uint16_t id;
	uint16_t len;
	uint8_t *eep;
}EEP_ITEM_LIST_INFO;

typedef struct
{
<<EEPROM_STRUCT_LIST>>}EEPROM_ST;

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
//extern uint16_t eep_write_block_req;
//extern uint32_t wr_eep_by_list_req[EEP_CONT_LIST_FLAG_ARRAY_NUM];
//extern uint32_t tx_eep_by_list_to_imx_req[EEP_CONT_LIST_FLAG_ARRAY_NUM];
//extern uint32_t tx_eep_by_list_to_imx_cur[EEP_CONT_LIST_FLAG_ARRAY_NUM];
// eeprom ram image 
//extern EEPROM_ST eep_ram_image;
// write eep_ram_image->eep_hw_image, than eep_hw_image->real eeprom 
//extern EEPROM_ST eep_hw_image; 

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
<<FUNCTION_DECLEAR>>

void GenEep_Get_ByOffset(uint16_t const offset, uint16_t const len,uint8_t *const data);
void GenEep_Set_ByOffset(uint16_t const offset, uint16_t const len,uint8_t const* data);


void EepMana_Init(void);
boolean EepMana_InitedComp(void);
void EepMana_Sync(void);
void EepMana_IdleTask(void);
// void EepMana_RxMsg(uint16_t const len,  uint8_t const*dat);
// void EepMana_OnOff(boolean const xon_off, uint8_t const new_client,uint8_t const old_client);
// uint16_t EepMana_SegTxStart(void);
// uint16_t EepMana_SegTxGetBuf(uint8_t *const buf, uint16_t const len);
// void EepMana_SegTxResult(TX_RESULE const result);

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#ifdef __cplusplus
}
#endif

#endif
/** >>>>>>>> end of file  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/




