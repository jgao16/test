
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> <<LIST_NAME>> function start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if <<LIST_LEN>> > 1
void GenEep_Get_<<LIST_NAME>>(<<LIST_TYPE>> * const <<LIST_NAME>> )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < <<LIST_LEN>>; i ++)
    {
        <<LIST_NAME>>[i] = eep_ram_image.<<LIST_NAME>>[i];
    }
    CPU_CRITICAL_EXIT();
}
void GenEep_Get_<<LIST_NAME>>_Ex(uint8_t * const <<LIST_NAME>>,uint16_t const offset,uint16_t const len )
{
	uint8_t * src = (uint8_t*)eep_ram_image.<<LIST_NAME>>;	//EEP_CONTENT_<<LIST_NAME>>_ITEM_BYTE_LEN
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_<<LIST_NAME>>_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_<<LIST_NAME>>_ITEM_BYTE_LEN;
	}
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
	
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        <<LIST_NAME>>[i] = src[offset+i];
    }
    CPU_CRITICAL_EXIT();
}

void GenEep_Set_<<LIST_NAME>>(<<LIST_TYPE>> const* <<LIST_NAME>> )
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < <<LIST_LEN>>; i ++)
    {
        if ( eep_ram_image.<<LIST_NAME>>[i] != <<LIST_NAME>>[i] )
        {
            eep_ram_image.<<LIST_NAME>>[i] = <<LIST_NAME>>[i];
            wr_eep_by_list_req[<<LIST_NUMBER>>/32] 		|= (1u<<(<<LIST_NUMBER>>&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[<<LIST_NUMBER>>/32] 	|= (1u<<(<<LIST_NUMBER>>&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:<<LIST_NAME>> firt item[0] = %d",<<LIST_NAME>>[0]);
}

void GenEep_Set_<<LIST_NAME>>_Ex(uint8_t const* <<LIST_NAME>>,uint16_t const offset,uint16_t const len )
{
	uint16_t  eep_len;
	if ( offset + len <= EEP_CONTENT_<<LIST_NAME>>_ITEM_BYTE_LEN ){
		eep_len = len;
	}else{
		eep_len = offset + len - EEP_CONTENT_<<LIST_NAME>>_ITEM_BYTE_LEN;
	}
	uint8_t *dest = (uint8_t*)eep_ram_image.<<LIST_NAME>>;
	
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < eep_len; i ++)
    {
        if ( dest[offset+i] != <<LIST_NAME>>[i] )
        {
            dest[offset+i] = <<LIST_NAME>>[i];
            wr_eep_by_list_req[<<LIST_NUMBER>>/32] 		|= (1u<<(<<LIST_NUMBER>>&0x1F));	//lint -e778
            // tx_eep_by_list_to_imx_req[<<LIST_NUMBER>>/32] 	|= (1u<<(<<LIST_NUMBER>>&0x1F));//lint -e778
        }
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_TEXT(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:<<LIST_NAME>> by Extend Api");
}

static void EepCopy_<<LIST_NAME>>_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    for(uint16_t i = 0; i < <<LIST_LEN>>; i ++)
    {
        eep_hw_image.<<LIST_NAME>>[i] = eep_ram_image.<<LIST_NAME>>[i];
    }
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << <<LIST_BLOCK_NUM>>);
}

#else 
<<LIST_TYPE>> GenEep_Get_<<LIST_NAME>>(void)
{
    <<LIST_TYPE>> val;
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    val = eep_ram_image.<<LIST_NAME>>;
    CPU_CRITICAL_EXIT();
    return val;
}

void GenEep_Set_<<LIST_NAME>>(<<LIST_TYPE>> const <<LIST_NAME>>)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    if ( eep_ram_image.<<LIST_NAME>> != <<LIST_NAME>> )
    {
        eep_ram_image.<<LIST_NAME>> = <<LIST_NAME>>;
        //wr_eep_by_list_req |= (1u<<$num);
        wr_eep_by_list_req[<<LIST_NUMBER>>/32] 		|= (1u<<(<<LIST_NUMBER>>&0x1F));//lint -e778
        // tx_eep_by_list_to_imx_req[<<LIST_NUMBER>>/32] 	|= (1u<<(<<LIST_NUMBER>>&0x1F));//lint -e778
    }
    CPU_CRITICAL_EXIT();
	
	TRACE_VALUE(EEP_SET_CONTENT,MOD_EEPROM,"User Write Eeprom Content:<<LIST_NAME>>=%d",<<LIST_NAME>>);
}

static void EepCopy_<<LIST_NAME>>_FromUserImageToHwImage(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    eep_hw_image.<<LIST_NAME>> = eep_ram_image.<<LIST_NAME>>;
    //CPU_CRITICAL_EXIT();
    eep_write_block_req |= (1u << <<LIST_BLOCK_NUM>>);
}
#endif
//static void Proc_<<LIST_NAME>>_RestoreToDefault(void)
//{
//#if EEP_CONTENT_<<LIST_NAME>>_CONST_VARIBLE
//	GenEep_Set_<<LIST_NAME>>(dflt_eep_val.<<LIST_NAME>>);
//#endif	
//}
#if EEP_CONTENT_<<LIST_NAME>>_CONST_VARIBLE
#define Proc_<<LIST_NAME>>_RestoreToDefault()	do{GenEep_Set_<<LIST_NAME>>(dflt_eep_val.<<LIST_NAME>>);}while(0)
#else
#define Proc_<<LIST_NAME>>_RestoreToDefault() do{}while(0)
#endif	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>> <<LIST_NAME>> function end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

