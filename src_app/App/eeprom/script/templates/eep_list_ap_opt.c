// api define for <<ITEM_NEMA>> /////////////////////////////////////////////////////////
#if EEP_CONTENT_<<ITEM_NEMA>>_ITEM_LEN == 1
<<ITEM_TYPE>> DevEep_Get_<<ITEM_NEMA>>(EEP_HANDLE eep_handle)
{
	<<ITEM_TYPE>> <<ITEM_NEMA>>_val = 0x00;
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		<<ITEM_NEMA>>_val = ap_eep->process_eep.<<ITEM_NEMA>>;
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
	return <<ITEM_NEMA>>_val;
}
void DevEep_Set_<<ITEM_NEMA>>(EEP_HANDLE eep_handle,<<ITEM_TYPE>> const <<ITEM_NEMA>>_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_<<ITEM_NEMA>>_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_<<ITEM_NEMA>>_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_<<ITEM_NEMA>>_LIST_ID);
#if EEP_CONTENT_<<ITEM_NEMA>>_ITEM_TYPE == EEP_ITEM_TYPE_8
	msg[len++] = <<ITEM_NEMA>>_val;
#elif EEP_CONTENT_<<ITEM_NEMA>>_ITEM_TYPE == EEP_ITEM_TYPE_16
	msg[len++] = (uint8_t)(<<ITEM_NEMA>>_val);
	msg[len++] = (uint8_t)(<<ITEM_NEMA>>_val>>8);
#else 
	msg[len++] = (uint8_t)(<<ITEM_NEMA>>_val);
	msg[len++] = (uint8_t)(<<ITEM_NEMA>>_val>>8);
	msg[len++] = (uint8_t)(<<ITEM_NEMA>>_val>>16);
	msg[len++] = (uint8_t)(<<ITEM_NEMA>>_val>>24);
#endif // {EEP_CONTENT_<<ITEM_NEMA>>_ITEM_TYPE}	
	
	write(ap_eep->dev_fd,msg,len);
}

#else	// EEP_CONTENT_<<ITEM_NEMA>>_ITEM_LEN > 1
void DevEep_Get_<<ITEM_NEMA>>(EEP_HANDLE eep_handle,<<ITEM_TYPE>> *<<ITEM_NEMA>>_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{	
		for(uint16_t i = 0; i < EEP_CONTENT_<<ITEM_NEMA>>_ITEM_LEN; i ++)
		{
			<<ITEM_NEMA>>_val[i] = ap_eep->process_eep.<<ITEM_NEMA>>[i];
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
void DevEep_Set_<<ITEM_NEMA>>(EEP_HANDLE eep_handle,<<ITEM_TYPE>> const *<<ITEM_NEMA>>_val)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	// write data to devipc fd
	uint8_t  msg[3+EEP_CONTENT_<<ITEM_NEMA>>_ITEM_BYTE_LEN];
	uint16_t len = 0x00;
	msg[len++] = AP_WRITE_EEP_LIST_CMD;
	msg[len++] = (uint8_t)(EEP_CONTENT_<<ITEM_NEMA>>_LIST_ID>>8);
	msg[len++] = (uint8_t)(EEP_CONTENT_<<ITEM_NEMA>>_LIST_ID);
	for ( uint16_t i=0; i < EEP_CONTENT_<<ITEM_NEMA>>_ITEM_LEN; i ++ )
	{
#if EEP_CONTENT_<<ITEM_NEMA>>_ITEM_TYPE == EEP_ITEM_TYPE_8
		msg[len++] = <<ITEM_NEMA>>_val[i];
#elif EEP_CONTENT_<<ITEM_NEMA>>_ITEM_TYPE == EEP_ITEM_TYPE_16
		msg[len++] = (uint8_t)(<<ITEM_NEMA>>_val[i]);
		msg[len++] = (uint8_t)(<<ITEM_NEMA>>_val[i]>>8);
#else 
		msg[len++] = (uint8_t)(<<ITEM_NEMA>>_val[i]);
		msg[len++] = (uint8_t)(<<ITEM_NEMA>>_val[i]>>8);
		msg[len++] = (uint8_t)(<<ITEM_NEMA>>_val[i]>>16);
		msg[len++] = (uint8_t)(<<ITEM_NEMA>>_val[i]>>24);
#endif // {EEP_CONTENT_<<ITEM_NEMA>>_ITEM_TYPE}	
	}
	// write data to devipc fd 
	write(ap_eep->dev_fd,msg,len);
}
#endif // {EEP_CONTENT_<<ITEM_NEMA>>_ITEM_LEN}
void DevEep_Inject_ChgCalbk_<<ITEM_NEMA>>(EEP_HANDLE eep_handle,<<ITEM_NEMA>>_ChgCallBk calbk)
{
	DEV_EEP_AP *ap_eep = (DEV_EEP_AP*)eep_handle;
	if ( pthread_mutex_lock(&(ap_eep->mutex)) == 0 )
	{
		ap_eep->callback[EEP_CONTENT_<<ITEM_NEMA>>_LIST_ID]  = calbk;
		ap_eep->calbk_req[EEP_CONTENT_<<ITEM_NEMA>>_LIST_ID] = 0x01;
		if ( ap_eep->invoke_task == 0x00 )
		{
			ap_eep->invoke_task = 0x01;
			(void)sem_post(&(ap_eep->invoke_sem));
		}
		(void)pthread_mutex_unlock(&(ap_eep->mutex));
	}
}
static void DevEep_InvokeCalbk_<<ITEM_NEMA>>(DEV_EEP_AP *ap,void *callbk)
{
	if ( callbk != NULL )
	{
		<<ITEM_NEMA>>_ChgCallBk <<ITEM_NEMA>>_callback =  (<<ITEM_NEMA>>_ChgCallBk)callbk;
		<<ITEM_NEMA>>_callback(ap,ap->process_eep.<<ITEM_NEMA>>);
	}
}

