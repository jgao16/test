

/*****this file is generate by perl********************************************/
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#include "std_type.h"
#include "gen_eep_mana.h"
#include "cpu.h"
#include "hal_eep.h"
#include "hal_dflash.h"
#include "crc32.h"
#include "trace_api.h"

#include "bat_mana.h"
#include "hal_os.h"

/*lint -e835 -e845*/
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#define TX_EEP_OFFSET_SIZE	64ul
#define OFFSET_FIELD_NUM	((sizeof(EEPROM_ST)+TX_EEP_OFFSET_SIZE-1)/TX_EEP_OFFSET_SIZE)
#define EEP_SIZE			(sizeof(EEPROM_ST))

#define OFFSET_FIELD_TBL_NUM		((OFFSET_FIELD_NUM+31)/32)

#define TRANS_WHOLE_EEP_CMD		(0x03)
#define TRANS_EEP_OFFSET_CMD	(0x04)
#define	WRITE_EEP_OFFSET_CMD	(0x05)
#define	TRANS_EEP_LIST_CMD		(0x20)
#define	WRITE_EEP_LIST_CMD		(0x21)
#define EEP_IPC_CMD_RESP		(0x80)

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
// typedef enum
// {
// 	EEP_IPC_IDLE,

// 	EEP_IPC_TX_WHOLE,
// 	EEP_IPC_TX_WHOLE_REQ,
	
// 	EEP_IPC_TX_BY_OFFSET,
// 	EEP_IPC_TX_BY_OFFSET_REQ,

// 	EEP_IPC_TX_EEP_BY_LIST,
// 	EEP_IPC_TX_EEP_BY_LIST_REQ
	
// }EEP_IPC_STATE;

typedef void (*EepCopyUserImageToHwImageFunc)(void);

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

//static uint8 eep_sector[EEP_SECTOR_SIZE];
static boolean			eep_init_comp;
// static uint8_t			eep_last_client;
static uint8_t 			process_block;				// if process block = 0xFF, eeprom is idle 
static uint8_t			wr_hw_try_cnt;
static uint16_t			wr_eep_back;
static uint16_t			wr_eep_index;
// static EEP_IPC_STATE	eep_ipc_state;	

static boolean			eep_write_enable;
// static boolean			eep_transfer_whole;
// static boolean			ipc_tx_cmd_header;
// static boolean			ipc_tx_sub_item_header;
// static uint16_t			ipc_tx_idx;
// static uint16_t  		ipc_tx_list;	// for list and offset


// static uint32_t			trans_eep_offset_req[OFFSET_FIELD_TBL_NUM];
// static uint32_t			trans_eep_offset_cur[OFFSET_FIELD_TBL_NUM];

// static uint32_t			write_eep_offset_req[OFFSET_FIELD_TBL_NUM];	// write to real eeprom by imx command


static uint16_t eep_write_block_req;
static uint32_t wr_eep_by_list_req[EEP_CONT_LIST_FLAG_ARRAY_NUM];
// static uint32_t tx_eep_by_list_to_imx_req[EEP_CONT_LIST_FLAG_ARRAY_NUM];
// static uint32_t tx_eep_by_list_to_imx_cur[EEP_CONT_LIST_FLAG_ARRAY_NUM];
static EEPROM_ST eep_hw_image; // write eep_ram_image->eep_hw_image, than eep_hw_image->real eeprom 
// eeprom ram image 
static EEPROM_ST eep_ram_image;

<<LOCAL_VARIABLE_DEFINE>>

static EEPROM_ST const dflt_eep_val=
{	/*lint -save -e651*/
<<EEP_DEFAULT_VALUE>>	/*lint -restore */
};

#if 0
static const EEP_ITEM_LIST_INFO eep_list_info[EEP_CONTENT_LIST_NUMBER_MAX]=
{
<<EEP_CONTENT_LIST_INFO>>};
#endif

#if 0
static uint16_t const eep_block_offset_info[EEP_BLOCK_NUMBER_MAX+1] = 
{
<<EEP_BLOCK_OFFSET>>	0xFFFF
};
#endif

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void EepWrProcess(void);
// static void EepIpcTxProcess(void);

// static void ProcessImxReadEepByOffset( uint16 len, uint8 const* cmd );
// static void ProcessImxWriteEepByOffset( uint16 len, uint8 const* cmd );
// static void ProcessImxReadEepByList( uint16 len, uint8 const* cmd );
// static void ProcessImxWriteEepByList( uint16 len, uint8 const* cmd );
static void EepCopyRamImageToEepImage(void);
static void EepCopyEepImageToHw(void);

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
<<EXTERN_FUNCTION_DEFINE>>

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void GenEep_Get_ByOffset(uint16_t const offset, uint16_t len,uint8_t *const data)
{
    if ( offset < EEP_SIZE )
    {
        if ( offset + len > EEP_SIZE )
        {
            len = EEP_SIZE - offset;
        }
        uint8_t *eep = (uint8_t*)(&eep_ram_image) + offset;
        CPU_SR_ALLOC();
        CPU_CRITICAL_ENTER();
        for ( uint16_t i = 0; i < len; i ++ )
        {
            data[i] = eep[i];
        }
        CPU_CRITICAL_EXIT();
    }
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void GenEep_Set_ByOffset(uint16_t const offset, uint16_t len,uint8_t const* data)
{
    if ( offset < EEP_SIZE )
    {
        if ( offset + len > EEP_SIZE )
        {
            len = EEP_SIZE - offset;
        }
        uint8_t *eep = (uint8_t*)(&eep_ram_image) + offset;
        CPU_SR_ALLOC();
        CPU_CRITICAL_ENTER();
        for ( uint16_t i = 0; i < len; i ++ )
        {
            eep[i] = data[i];
        }
        CPU_CRITICAL_EXIT();
    }
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
<<LOCAL_FUNCTION_DEFINE>>

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static const EepCopyUserImageToHwImageFunc EepCopyUserImageToHwImageFuncTbl[EEP_CONTENT_LIST_NUMBER_MAX]=
{
<<EEP_COPY_USER_IMAGE_TO_HW_IMAGE_FUNC>>};

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void EepMana_Init(void)
{
	process_block = 0xFF;

	for(uint16_t i = 0; i < EEP_CONT_LIST_FLAG_ARRAY_NUM; i ++ )
	{
		wr_eep_by_list_req[i] 		= 0x00;
		// tx_eep_by_list_to_imx_req[i]= 0x00;
	}

<<EEP_INIT>>
	/*>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> eeprom restore to default value if it is a const varible 						*/
<<EEP_RESTORE_TO_DEFAULT>>
	/*>>>> Set initial complete flag  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>*/
	eep_init_comp = TRUE;
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
boolean EepMana_InitedComp(void)
{
	return eep_init_comp;
}
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void EepMana_Sync(void)
{
	//uint16 idx = 0;
	uint8_t cnt = 0;
	/* calc crc */
	CPU_SR_ALLOC();
	CPU_CRITICAL_ENTER();
	do
	{
		EepWrProcess();
		if ( process_block != 0xFF ){
			cnt = 0x00;
		}else{
			cnt ++;
		}
	}while( cnt < 5 );
	CPU_CRITICAL_EXIT();
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void EepMana_IdleTask(void)
{
	if ( eep_write_enable )
	{
		if ( BatMana_GetBatState() == BAT_NORMAL )
		{
			EepWrProcess();	// eeprom write process
		}
		// if ( eep_last_client )
		// {
		// 	EepIpcTxProcess();
		// }		
	}
	else
	{
		if ( HalOS_GetKernelTick() > 500 )
		{
			eep_write_enable = TRUE;
		}
	}
}



/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void EepWrProcess(void)
{
	/* write eeprom process */
	if ( process_block == 0xFF )
	{	// eeprom is idle 
		EepCopyRamImageToEepImage();
	}
	else if ( process_block < EEP_BLOCK_NUMBER_MAX ) 
	{	// write to real eeprom now 
		EepCopyEepImageToHw();
	}
	else
	{
		process_block = 0xFF;
	}
}


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void EepCopyRamImageToEepImage(void)
{
	for ( uint16 i = 0; i < EEP_CONTENT_LIST_NUMBER_MAX; i ++ )
	{
		CPU_SR_ALLOC();
		CPU_CRITICAL_ENTER();

		if ( wr_eep_by_list_req[i/32] & (1u<<(i&0x1F)) )
		{
			wr_eep_by_list_req[i/32] &= ~(1u<<(i&0x1F));

			EepCopyUserImageToHwImageFuncTbl[i]();
			
		}
		
		CPU_CRITICAL_EXIT();
	}
#if 0	
	/*check write eeprom by offset */
	for ( uint16 i = 0; i < OFFSET_FIELD_NUM; i ++ )
	{
		CPU_SR_ALLOC();
		CPU_CRITICAL_ENTER();

		if ( write_eep_offset_req[i/32] & (1u<<(i&0x1F)) )
		{
			write_eep_offset_req[i/32] &= ~(1u<<(i&0x1F));

			/* copy data now */
			uint16 offset =  (uint16)(i*TX_EEP_OFFSET_SIZE);
			
			for ( uint8 j = 1; j < EEP_BLOCK_NUMBER_MAX+1; j ++ )
			{ // need modify, offset -> write block 
				if ( offset <= eep_block_offset_info[j] )
				{
					eep_write_block_req |= (1u<<(j-1));	/*lint !e734*/
					break;
				}
			}
			
			uint16_t cp_len = TX_EEP_OFFSET_SIZE;
			for(uint8 j = 0; j < EEP_BLOCK_NUMBER_MAX; j ++)
			{
				if ( offset == eep_block_offset_info[j] )
				{
					offset += 4;
					cp_len -= 4;
					break;
				}
			}
			
			for ( uint16 j = 0; j < cp_len && offset < EEP_SIZE; j ++ )
			{	// copy to eeprom hw image 
				((uint8_t*)&eep_hw_image)[offset] = ((uint8_t*)&eep_ram_image)[offset];
				//*((uint8*)(&eeprom_image)+offset) = *((uint8*)(&eep_ram_image)+offset);
				offset ++;
			}	
		}

		CPU_CRITICAL_EXIT();
	}
#endif
	
	if ( eep_write_block_req )
	{
		for( uint8 i = 0; i < EEP_BLOCK_NUMBER_MAX; i ++ )
		{
			if ( eep_write_block_req & (1u<<i) )
			{
				eep_write_block_req &= ~(1u<<i);

				process_block = i;	// set process flag
				wr_eep_index  = 0;
				wr_eep_back   = FALSE;
				wr_hw_try_cnt = 0x00;
				
				switch(i)
				{
<<CALC_BLOCK_CRC>>	
				default:
					break;
				}				
				//EEP_BLOCK_INFO const* eep_info 	= eep_block_info	+	i;
				//uint8 *	eep 				 	= eep_info->eep_back;
				// increase write cnt;

				//uint32 *wr_cnt = (void*)eep;
				//*wr_cnt += 1;
				//*eep_info->eep_back_crc = Crc32_Calc(eep,eep_info->len-4);
	
				break;
			}
		}
	}	
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void EepCopyEepImageToHw(void)
{
	switch(process_block)
	{
<<EEP_COPY_HW_IMAGE_TO_REAL_EEP>>		
	default:
		process_block = 0xFF;
		break;
	}
}



/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/



/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/




