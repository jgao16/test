#!/usr/bin/perl


#We expect 3 arguments to this script. to generate log cfg file and trace handle.c 
#Arg[1] should be the name of the sub-directory under which the output files will be generated, ../obj
#Arg[2] should be the name of the source-file to parse for LOG* macros. CFG_FILE = ../*.xml
#Arg[3] should be the name of the sub-directory of templates, TEMPLATES_PATH = ../../

use strict;
use XML::Simple;
use Data::Dumper;



($#ARGV+1 == 4) or die "log_parse.pl: Not enough arguments...\n";

my $objpath;
my $cfg_file;
my $cfg_path;
my $template_path;

# writing the passed arguments into variables
foreach(@ARGV)
{
   if (/OBJPATH/)               { $objpath = (split(/=/))[1]; }
   if (/CFG_FILE/)              { $cfg_file = (split(/=/))[1]; }
   if (/TEMPLATES_PATH/)        { $template_path = (split(/=/))[1]; }
   if (/CFG_PATH/)           	{ $cfg_path = (split(/=/))[1]; }
#   if (/SW_COMPILER/)           { $compiler = (split(/=/))[1]; }
#   if (/ID_FILE/)               { $IDfile = (split(/=/))[1]; }	 ID_FILE = $objpath/log_id.h
}

system "rm -rf  $objpath/ap_eep_code";
system "rm -rf  $objpath/gen_eep*.*";
system "mkdir  $objpath/ap_eep_code";

my $GeneTm;
$GeneTm = sprintf("%ld", time());

#my $file 		= 'eeprom_cfg.xml';
my $xmlreader 	= XML::Simple->new(ForceArray => 1, KeyAttr => { item => });
my $eep_cfg 	= $xmlreader->XMLin("$cfg_path/$cfg_file");;
#my @eeprom 		= @{$eep_cfg->{eep_block}};

my $LIST_API_C;
my $LIST_API_H;
my $BLOCK_INIT_C;
my $BLOCK_CALC_CRC_C;
my $EEP_COPY_TO_HW_C;

my $eep_cfg_define="";
my $eep_cont_list_id_define="";
my $eep_content_restore_to_default_func="";
my $dflash_start_addr = 0;
my $dflash_sector_size= 0;

my $eep_ap_test_set_handle_info;
my $ap_eep_list_inject_callback;
my $ap_eep_list_test_opt;

$eep_cfg_define .= "#define EEP_GEN_TIME	($GeneTm\u)\n\n\n";

$eep_cfg_define .= "#define EEP_NORMAL_ADDR	(${$eep_cfg->{'eep_normal_addr'}}[0]u)	/*lint -e534 -e835*/\n";
$eep_cfg_define .= "#define EEP_BACKUK_ADDR	(${$eep_cfg->{'eep_backup_addr'}}[0]u)\n";
$eep_cfg_define .= "#define EEP_SECTOR_SIZE	(${$eep_cfg->{'eep_sector_size'}}[0]u)\n";
$eep_cfg_define .= "#define EEP_PROGRAM_SIZE	(${$eep_cfg->{'eep_program_size'}}[0]u)\n\n\n";

if ( exists ${$eep_cfg->{dflash_address_start}}[0] )
{
	$dflash_start_addr = ${$eep_cfg->{dflash_address_start}}[0];
	$dflash_sector_size= ${$eep_cfg->{dflash_sector_size}}[0];
	
	$eep_cfg_define .= "#define DFLASH_START_ADDR	(${$eep_cfg->{'dflash_address_start'}}[0]\u)\n";
	$eep_cfg_define .= "#define DFLASH_SECTOR_SIZE	(${$eep_cfg->{'dflash_sector_size'}}[0]\u)\n";
	$eep_cfg_define .= "#define DFLASH_PROGRAM_SIZE	(${$eep_cfg->{'dflash_program_size'}}[0]u)\n\n\n";
}

read_file("$template_path/eep_list_api.c",     \$LIST_API_C);
read_file("$template_path/eep_list_api.h",     \$LIST_API_H);
read_file("$template_path/eep_init.c",     	\$BLOCK_INIT_C);
read_file("$template_path/calc_block_crc.c",   \$BLOCK_CALC_CRC_C);
read_file("$template_path/eep_copy_image_to_hw.c",   \$EEP_COPY_TO_HW_C);


my $eep_list_id_number = 0;
my $eep_list_info     ="";
my $ap_side_list_info ="";
my $eep_block_number = 0;
my $eep_block_offset = 0;
my $eep_block_offset_array   ="";
my $extern_function_define="";
my $extern_function_declear="";
my $local_function_define="";
my $eep_init_func="";
my $eep_copy_user_image_to_hw_image_func="";
my $eep_content_struct ="";;
my $eep_default_val = "";
my $calc_block_crc="";
my $copy_eep_image_to_hw="";
my $dflash_write_cur_addr_define="";

my $ap_rx_inject_callbck_type;
my $ap_eep_api;

my $ap_eep_list_func_define;


Pasre_ImportFile($cfg_file);

$eep_cfg_define	.= "#define	EEP_BLOCK_NUMBER_MAX	($eep_block_number\u)\n\n\n";
$eep_cont_list_id_define	.= "#define	EEP_CONTENT_LIST_NUMBER_MAX	($eep_list_id_number\u)\n\n\n";

build_eep_cfg_h();
build_eep_cfg_c();

build_eep_ap_c();
build_eep_ap_h();
build_eep_ap_test();

sub Pasre_ImportFile
{
	my ($xml_name) = @_;
	my $xml_tree = $xmlreader->XMLin("$cfg_path/$xml_name");
	
	if ( exists ${$xml_tree->{import}}[0] )
	{
		my @import = @{$xml_tree->{import}};
		
		for (@import)
		{
			Pasre_ImportFile($_);	
		}
	}
	
	if ( exists ${$xml_tree->{eep_block}}[0] )
	{
		my @eeprom 		= @{$xml_tree->{eep_block}};

		for (@eeprom)
		{	# eepprom block
			parse_block_define($_);
			parse_block_init_func($_);
			parse_eep_block_crc($_);
			parse_eep_block_copy_hw($_);
			
			parse_eep_block($_);
			
			
			$eep_block_number++;
			$eep_block_offset += ${$_->{size}}[0];
		}
	}
}

sub parse_block_define
{
	my ($eep_block) = @_;
	
	$eep_cfg_define .= "#define EEP_BLOCK_$eep_block->{name}_BLOCK_ID			($eep_block_number\u)\n";
	
	$eep_cfg_define .= "#define	EEP_BLOCK_$eep_block->{name}_LEN  (${$eep_block->{size}}[0]u)\n";
	
	if (  ${$eep_block->{'attr'}}[0] eq 'EEPROM' )
	{
		$eep_cfg_define .= "#define EEP_BLOCK_$eep_block->{name}_IS_EEPROM (TRUE)\n";
		my $addr  		= ${$eep_cfg->{'eep_normal_addr'}}[0] + $eep_block_offset;
		my $back_addr 	= ${$eep_cfg->{'eep_backup_addr'}}[0] + $eep_block_offset;
		$eep_cfg_define .= "#define EEP_BLOCK_$eep_block->{name}_NORMAL_ADDR ($addr\u)\n";
		$eep_cfg_define .= "#define EEP_BLOCK_$eep_block->{name}_BACKUP_ADDR ($back_addr\u)\n";
	}
	else
	{
		$eep_cfg_define .= "#define EEP_BLOCK_$eep_block->{name}_IS_EEPROM (FALSE)\n";
		
		#	${$block->{'sector_start'}}[0],${$block->{'sector_num'}}[0], ";
		my $start_addr   = $dflash_start_addr+${$eep_block->{sector_start}}[0]*$dflash_sector_size;
		my $end_addr     = $dflash_start_addr+(${$eep_block->{sector_start}}[0]+${$eep_block->{sector_num}}[0])*$dflash_sector_size-1;
		my $sector_start =${$eep_block->{sector_start}}[0];
		my $sector_end   = ${$eep_block->{sector_start}}[0]+${$eep_block->{sector_num}}[0]-1;
		$eep_cfg_define .= "#define EEP_BLOCK_$eep_block->{name}_DFLASH_START_ADDR	($start_addr+DFLASH_START_ADDR)\n";
		$eep_cfg_define .= "#define EEP_BLOCK_$eep_block->{name}_DFLASH_END_ADDR	($end_addr+DFLASH_START_ADDR)\n";
		$eep_cfg_define .= "#define EEP_BLOCK_$eep_block->{name}_DFLASH_SECTOR_START ($sector_start)\n";
		$eep_cfg_define .= "#define EEP_BLOCK_$eep_block->{name}_DFLASH_SECTOR_END	($sector_end)\n";
		$eep_cfg_define .= "#define EEP_BLOCK_$eep_block->{name}_DFLASH_SECTOR_NUM 	(${$eep_block->{sector_num}}[0])\n";
		
		$dflash_write_cur_addr_define .= "static uint32_t dflash_$eep_block->{name}_current_write_addr;\n"
	}
	$eep_block_offset_array .= "	$eep_block_offset,\n";
}

sub parse_block_init_func
{
	my ($eep_block) = @_;
	
#	if (  ${$eep_block->{'attr'}}[0] eq 'EEPROM' )
	{
		my $block_init_c = $BLOCK_INIT_C;
		
		$block_init_c =~ s/<<BLOCK_NAME>>/$_->{name}/g;
		
		$local_function_define .= $block_init_c;		
		
		$eep_init_func .= "	EepBlock_$_->{name}_Init();\n";
	}
}

sub parse_eep_block_crc
{
	my ($eep_block) = @_;
	my $calc_crc = $BLOCK_CALC_CRC_C;
	
	$calc_crc =~ s/<<BLOCK_NAME>>/$eep_block->{name}/g;
	
	$calc_block_crc .= $calc_crc;
}

sub parse_eep_block_copy_hw
{
	my ($eep_block) = @_;
	
	my $copy_to_hw = $EEP_COPY_TO_HW_C;
		
	$copy_to_hw =~ s/<<BLOCK_NAME>>/$eep_block->{name}/g;
		
	$copy_eep_image_to_hw .= $copy_to_hw;


}
sub parse_eep_block
{
	my ($eep_block) = @_;
	
	#for eeprom struct content list
	my $block_size = ${$eep_block->{size}}[0];
	$eep_content_struct .= "	\/* >>>>>>>>>>>>>>>>>> $eep_block->{name} start >>>>>>>>>>>>>>>>>>>>>>>>>>> *\/\n";
	$eep_content_struct .= "	uint32_t $eep_block->{name}\_write_count;\n";
	my $eep_block_size_used	= 4;
	
	$extern_function_define .= "uint32_t GenEep_Get_$eep_block->{name}\_write_count(void)\n{\n	return eep_ram_image.$eep_block->{name}\_write_count;\n}\n";
	$extern_function_declear.= "uint32_t GenEep_Get_$eep_block->{name}\_write_count(void);\n";
	
	#$eep_cont_list_id_define .= "#define EEP_$eep_block->{name}_WriteCnt_LIST_ID			($eep_list_id_number\u)\n";
	#$eep_list_id_number ++;
	
	my @list = @{$eep_block->{eep}};
	for(@list)
	{
		$eep_cont_list_id_define .= "#define EEP_CONTENT_$_->{name}_LIST_ID				($eep_list_id_number\u)\n";
		
		parse_eep_content_struct($_,\$eep_block_size_used);
		parse_eep_default_val($_);
		parse_eep_list_api($_);
		parse_eep_list_info($_);
		
		parse_eep_ap_opt_func($_);
		
		$eep_list_id_number ++;
	}
	
	#$eep_cont_list_id_define .= "#define EEP_$eep_block->{name}_Crc_LIST_ID			($eep_list_id_number\u)\n";
	#$eep_list_id_number ++;
	
	#for eeprom struct content list
	$eep_content_struct .= "	/* eeprom $eep_block->{name} id size = $eep_block_size_used */ \n";
	my $item_size = $block_size -4;
	if ( $item_size < $eep_block_size_used )
	{
		$item_size = $item_size-4;
		$eep_content_struct .= "#error\"eeprom block $eep_block->{name} size is limited to $item_size\" \n";
	}
	elsif ( $item_size > $eep_block_size_used )
	{
		my $block_1_remainder = $item_size-$eep_block_size_used;
		$eep_content_struct .= "	uint8_t $eep_block->{name}_reserved_$eep_block_size_used\[$block_1_remainder];\n";	
		
		my $count = 0;
		while( $count < $block_1_remainder )
		{
			#$eep_default_val .= "	.$list->{name}\[$count] = (${$list->{'type'}}[0])(-1),\n";
			$eep_default_val .= "	.$eep_block->{name}_reserved_$eep_block_size_used\[$count] = 0x00,\n";
			$count++;
		}
	}
	else{}
	
	$eep_content_struct .= "	uint32_t $eep_block->{name}\_crc;	/* $eep_block->{name} crc32 checksum */\n";
	$eep_content_struct .= "	/*	eeprom $eep_block->{name} total size = $block_size */ \n";
	$eep_content_struct .= "	\/* >>>>>>>>>>>>>>>>>> $eep_block->{name} end >>>>>>>>>>>>>>>>>>>>>>>>>>> *\/\n\n"
}

sub parse_eep_content_struct
{
	my ($list, $size) = @_;
	
	if ( ${$list->{'type'}}[0] eq 'uint16_t' && $$size%2 )
	{
		$eep_content_struct .= "	uint8_t reserved_$$size\_$list->{name};\n";
		$eep_default_val    .= "	.reserved_$$size\_$list->{name} = 0x00,\n";
		
		$$size += 1;
	} 
	elsif ( ${$list->{'type'}}[0] eq 'uint32_t' && $$size%4 )
	{
		my $cnt = 4-$$size%4;
		$eep_content_struct .="	uint8_t reserved_$$size\_$list->{name}\[$cnt];\n";
		#$eep_default_val .= "	.reserved_$$size\_$list->{name} = 0x00,\n";
		my $count = 0;
		while( $count < $cnt )
		{
			#$eep_default_val .= "	.$list->{name}\[$count] = (${$list->{'type'}}[0])(-1),\n";
			$eep_default_val .= "	.reserved_$$size\_$list->{name}\[$count] = 0x00,\n";
			$count++;
		}
		$$size += $cnt;
	}
	else{}
		
	if ( ${$list->{'len'}}[0] > 1 )
	{
		$eep_content_struct .="	${$list->{'type'}}[0]   $list->{name}\[${$list->{'len'}}[0]];\n";
		
		$ap_rx_inject_callbck_type .= "typedef void (*$list->{name}_ChgCallBk)(EEP_HANDLE eep_handle,${$list->{'type'}}[0] const*$list->{name}_val);\n";
		$ap_eep_api 			   .= "void DevEep_Get_$list->{name}(EEP_HANDLE eep_handle,${$list->{'type'}}[0] *$list->{name}_val);\n";
		$ap_eep_api 			   .= "void DevEep_Set_$list->{name}(EEP_HANDLE eep_handle,${$list->{'type'}}[0] const*$list->{name}_val);\n";
	}
	else
	{
		$eep_content_struct .="	${$list->{'type'}}[0]   $list->{name};\n";
		
		$ap_rx_inject_callbck_type .= "typedef void (*$list->{name}_ChgCallBk)(EEP_HANDLE eep_handle,${$list->{'type'}}[0] const $list->{name}_val);\n";
		$ap_eep_api 			   .= "${$list->{'type'}}[0] DevEep_Get_$list->{name}(EEP_HANDLE eep_handle);\n";
		$ap_eep_api 			   .= "void DevEep_Set_$list->{name}(EEP_HANDLE eep_handle,${$list->{'type'}}[0] const $list->{name}_val);\n";
	}
	$ap_eep_api .= "void DevEep_Inject_ChgCalbk_$list->{name}(EEP_HANDLE eep_handle,$list->{name}_ChgCallBk calbk);\n";
	
	$eep_cont_list_id_define .=  "#define EEP_CONTENT_$list->{name}_ITEM_LEN		(${$list->{'len'}}[0]\u)\n";
	$eep_cont_list_id_define .=  "#define EEP_CONTENT_$list->{name}_IN_BLOCK_NUM 	($eep_block_number\u)\n";
		
	if ( ${$list->{'type'}}[0] eq 'uint8_t' )
	{
		$$size += ${$list->{'len'}}[0];
		$eep_cont_list_id_define .=  "#define EEP_CONTENT_$list->{name}_ITEM_TYPE		EEP_ITEM_TYPE_8\n";
	}
	elsif ( ${$list->{'type'}}[0] eq 'uint16_t' )
	{
		$$size += ${$list->{'len'}}[0]*2;
		$eep_cont_list_id_define .=  "#define EEP_CONTENT_$list->{name}_ITEM_TYPE		EEP_ITEM_TYPE_16\n";
	}
	elsif ( ${$list->{'type'}}[0] eq 'uint32_t' ) #uint32
	{
		$$size += ${$list->{'len'}}[0]*4;
		$eep_cont_list_id_define .=  "#define EEP_CONTENT_$list->{name}_ITEM_TYPE		EEP_ITEM_TYPE_32\n";
	}
	else
	{	
		$eep_content_struct .="not support $list->{name}\n";
	}
}

sub parse_eep_default_val
{
	my($list) = @_;
	
	if ( exists ${$list->{'dfl_val'}}[0] )
	{	#default value is avaible 
		if ( ${$list->{'len'}}[0] > 1 )
		{
			$eep_default_val .= "	.$list->{name} = { ${$list->{'dfl_val'}}[0] },\n";
		}
		else
		{
			$eep_default_val .= "	.$list->{name} = ${$list->{'dfl_val'}}[0],\n";
		}
	}
	else
	{
		#if ( ${$list->{'len'}}[0] > 1 )
		#{
		#	my $count = 0;
		#	while( $count < ${$list->{'len'}}[0] )
		#	{
		#		$eep_default_val .= "	.$list->{name}\[$count] = (${$list->{'type'}}[0])(-1),\n";
		#		$count++;
		#	}
		#}
		#else
		#{
		#	$eep_default_val .= "	.$list->{name} = (${$list->{'type'}}[0])(-1),\n";
		#}
	}
}

sub parse_eep_list_api
{
	my($list) = @_;
	
	my $list_api_c = $LIST_API_C;
	
	$list_api_c =~ s/<<LIST_LEN>>/EEP_CONTENT_$list->{name}_ITEM_LEN/g;
	$list_api_c =~ s/<<LIST_NAME>>/$list->{name}/g;
	$list_api_c =~ s/<<LIST_TYPE>>/${$list->{'type'}}[0]/g;
	$list_api_c =~ s/<<LIST_NUMBER>>/EEP_CONTENT_$_->{name}_LIST_ID/g;
	$list_api_c =~ s/<<LIST_BLOCK_NUM>>/EEP_CONTENT_$list->{name}_IN_BLOCK_NUM/g;
	
	$extern_function_define .= $list_api_c;
	
	my $list_api_h = $LIST_API_H;
	
	$list_api_h =~ s/<<LIST_LEN>>/EEP_CONTENT_$list->{name}_ITEM_LEN/g;
	$list_api_h =~ s/<<LIST_NAME>>/$list->{name}/g;
	$list_api_h =~ s/<<LIST_TYPE>>/${$list->{'type'}}[0]/g;

	$extern_function_declear .= $list_api_h;
	
	$eep_copy_user_image_to_hw_image_func .= "	EepCopy_$list->{name}_FromUserImageToHwImage,\n"
}

sub parse_eep_list_info
{
	my($list) = @_;
	
	my $item_len = ${$list->{len}}[0];
	if ( ${$list->{'type'}}[0] eq 'uint16_t' )
	{
		$item_len = $item_len * 2;
	}
	elsif ( ${$list->{'type'}}[0] eq 'uint32_t' ) #uint32
	{
		$item_len = $item_len *4;
	}
	else{}
	
	$eep_cont_list_id_define .=  "#define EEP_CONTENT_$list->{name}_ITEM_BYTE_LEN		$item_len\n";
	
	if( ${$list->{'len'}}[0] > 1 )
	{
		$eep_list_info .= "	{EEP_CONTENT_$_->{name}_LIST_ID, EEP_CONTENT_$list->{name}_ITEM_BYTE_LEN, (uint8_t*)(eep_ram_image.$list->{name})},\n";
	}
	else
	{
		$eep_list_info .= "	{EEP_CONTENT_$_->{name}_LIST_ID, EEP_CONTENT_$list->{name}_ITEM_BYTE_LEN, (uint8_t*)(&eep_ram_image.$list->{name})},\n";
	}
	$ap_side_list_info .= " {EEP_CONTENT_$list->{name}_LIST_ID,EEP_CONTENT_$list->{name}_ITEM_BYTE_LEN,offsetof(EEPROM_ST,$list->{name}),DevEep_InvokeCalbk_$list->{name}},\n"; 

	if ( exists ${$list->{const_var}}[0] && ${$list->{const_var}}[0] eq 'En' )
	{
		$eep_cont_list_id_define .=  "#define EEP_CONTENT_$list->{name}_CONST_VARIBLE	(1u)\n";
	}
	else
	{
		$eep_cont_list_id_define .=  "#define EEP_CONTENT_$list->{name}_CONST_VARIBLE	(0u)\n";
	}
	$eep_content_restore_to_default_func .= "	Proc_$list->{name}_RestoreToDefault();\n"
}

sub parse_eep_ap_opt_func
{
	my($list) = @_;
	
	my $item_func;
	read_file("$template_path/eep_list_ap_opt.c",     \$item_func);
	
	$item_func =~ s/<<ITEM_NEMA>>/$list->{name}/g;
	$item_func =~ s/<<ITEM_TYPE>>/${$list->{'type'}}[0]/g;
	
	$ap_eep_list_func_define .= $item_func;
	
	$eep_ap_test_set_handle_info .="	{\"-Set_$list->{name}\",\"-Set_$list->{name}: set $list->{name} value\\n\",DevEepTest_$list->{name}},\n";
	$ap_eep_list_inject_callback .= "	DevEep_Inject_ChgCalbk_$list->{name}(eep_handle,DevEepTest_$list->{name}_Callback);\n";
	
	
	my $eep_list_opt;
	read_file("$template_path/dev_eep_ap_test_opt.c",     \$eep_list_opt);
	$eep_list_opt =~ s/<<LIST_NAME>>/$list->{name}/g;
	#$eep_list_opt =~ s/<<LIST_NAME>>/$list->{name}/g;
	
	$ap_eep_list_test_opt .= $eep_list_opt;
}
sub build_eep_cfg_h
{
	my $eep_cfg_h;
	read_file("$template_path/eep_cfg.h",     \$eep_cfg_h);
	
	$eep_cfg_define .= $eep_cont_list_id_define;
	
	#$eep_cfg_h =~ s/<<INCLUDE>>//g;
	$eep_cfg_h =~ s/<<DEFINE>>/$eep_cfg_define/g;
	$eep_cfg_h =~ s/<<FUNCTION_DECLEAR>>/$extern_function_declear/g;
	#$eep_cfg_h =~ s/<<STRUCT>>/$eep_content_struct/g;
	
	$eep_cfg_h =~ s/<<EEPROM_STRUCT_LIST>>/$eep_content_struct/;
	
	
	
	save_file("$objpath/gen_eep_mana.h",$eep_cfg_h);
}

sub build_eep_cfg_c
{
	my $eep_cfg_c;
	read_file("$template_path/eep_cfg.c",     \$eep_cfg_c);
	
	$eep_cfg_c =~ s/<<EEP_DEFAULT_VALUE>>/$eep_default_val/;
	$eep_cfg_c =~ s/<<EEP_CONTENT_LIST_INFO>>/$eep_list_info/;
	$eep_cfg_c =~ s/<<EXTERN_FUNCTION_DEFINE>>/$extern_function_define/;
	$eep_cfg_c =~ s/<<LOCAL_FUNCTION_DEFINE>>/$local_function_define/;
	
	$eep_cfg_c =~ s/<<EEP_COPY_USER_IMAGE_TO_HW_IMAGE_FUNC>>/$eep_copy_user_image_to_hw_image_func/;
	$eep_cfg_c =~ s/<<EEP_BLOCK_OFFSET>>/$eep_block_offset_array/;
	$eep_cfg_c =~ s/<<LOCAL_VARIABLE_DEFINE>>/$dflash_write_cur_addr_define/;
	
	
	
	
	$eep_cfg_c =~ s/<<EEP_INIT>>/$eep_init_func/;
	$eep_cfg_c =~ s/<<CALC_BLOCK_CRC>>/$calc_block_crc/g;
	$eep_cfg_c =~ s/<<EEP_COPY_HW_IMAGE_TO_REAL_EEP>>/$copy_eep_image_to_hw/g;
	$eep_cfg_c =~ s/<<EEP_RESTORE_TO_DEFAULT>>/$eep_content_restore_to_default_func/g;
	
	
	save_file("$objpath/gen_eep_mana.c",$eep_cfg_c);
}

sub build_eep_ap_c
{
	my $ap_c;
	read_file("$template_path/dev_eep_ap_temp.c",     \$ap_c);
	
	$ap_c =~ s/<<DEV_EEP_LIST_FUNCTION_DEFINE>>/$ap_eep_list_func_define/;
	$ap_c =~ s/<<EEP_LIST_INFO_TBL>>/$ap_side_list_info/;
	
	save_file("$objpath/ap_eep_code/dev_eep.c",$ap_c);
}
sub build_eep_ap_h
{
	my $gen_eep_h;
	read_file("$objpath/gen_eep_mana.h",     \$gen_eep_h);
	
	$gen_eep_h =~ s/.+dev_ipc.+\n//;
	$gen_eep_h =~ s/GEN_EEP_MANA_H__/GEN_DEV_EEP_H__/g;
	$gen_eep_h =~ s/.+GenEep_.+\n//g;
	$gen_eep_h =~ s/.+EepMana_.+\n//g;
	
	#$dev_sig_h =~ s/SIG_TX_/SIG_TTTXXX_/g;
	#$dev_sig_h =~ s/SIG_RX_/SIG_TX_/g;
	#$dev_sig_h =~ s/SIG_TTTXXX_/SIG_RX_/g;
	
	save_file("$objpath/ap_eep_code/gen_dev_eep.h",$gen_eep_h);
	
	#$dev_sig_h="";
	my $eep_ap_h;
	read_file("$template_path/dev_eep_ap_temp.h",     \$eep_ap_h);
	$eep_ap_h =~ s/<<EEP_INJECT_HANDLE_TYPE>>/$ap_rx_inject_callbck_type/g;
	$eep_ap_h =~ s/<<INJECT_HANDLE_TYPE_DECLEAR>>/$ap_eep_api/g;
	#$eep_ap_h =~ s/<<SIG_AP_TX_API>>/$sig_ap_tx_sig_api/g;
	
	save_file("$objpath/ap_eep_code/dev_eep.h",$eep_ap_h);
}

sub build_eep_ap_test
{
	my $eep_test;
	read_file("$template_path/dev_eep_ap_test.c",     \$eep_test);
	
	$eep_test =~ s/<<SET_HANDLE_ARRAY>>/$eep_ap_test_set_handle_info/;
	$eep_test =~ s/<<INJECT_CALLBACK>>/$ap_eep_list_inject_callback/;
	$eep_test =~ s/<<DEV_EEP_CALLBACK_TX_DEFINE>>/$ap_eep_list_test_opt/;
	
	save_file("$objpath/ap_eep_code/dev_eep_test.c",$eep_test);
}


sub read_file
{
	my ($path, $output) = @_;

	open(templatefile, $path);
	seek(templatefile, 0, 2);
	my $bytes = tell(templatefile);
	seek(templatefile, 0, 0);
	read(templatefile, $$output, $bytes, 0);
	close(templatefile);
}

sub save_file
{
	my ($path, @output) = @_;

	open(codefile, "> $path") or die "Couldn't open Output-File : $!\r\n";
	print codefile @output;
	close codefile;
}

