/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> 
    All rights reserved
>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

#include "std_type.h"
#include "hal_os.h"

#include "trace_api.h"
//#include "map_mana.h"

#include "serv_func.h"
#include "bat_mana.h"
//#include "panel.h"
//#include "swc.h"
//#include "meter_fuel.h"
//#include "meter_tacho.h"
//#include "temperate.h"
//#include "ComRTE.h"
//#include "dvr.h"
#include "bsp_misc.h"

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#define LOG_RAW_TIME_TBL_SIZE               (sizeof(log_raw_info_tbl) / sizeof(LOG_RAW_INFO))

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
typedef struct
{	
	uint8_t  len;
	uint8_t  *cmd;

	void(*func)(void);
}CMD_INFO;

typedef struct
{
    uint8_t id;
    void(*func)(void);
}LOG_RAW_INFO;

typedef struct
{
    uint16_t cyc_cnt;   //cyc_cnt: counter
    uint16_t cyc_tm;    //cyc_tm: cycle time(ms);
    uint16_t times;     //times: 0xffff=endless.
}LOG_RAW_TIME;

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
/*lint -save -e64*/
#if 0
static const CMD_INFO ap_state_cmd_tbl[]=
{	
	{2,"\x01\x00",MapMana_EnterToUsbDownload},
	{2,"\x01\x01",MapMana_HoldPowerOn},
};
#endif
/*lint -restore */

static const LOG_RAW_INFO log_raw_info_tbl[] =
{
    {0x00, BatMana_TraceRawData},
#if 0
    {0x01, Panel_TraceRawData},
    {0x02, SwcKey_TraceRawData},
    {0x03, MeterFuel_TraceRawData},
    {0x04, MeterTacho_TraceRawData},
    {0x05, ComRTE_TraceRawData},
    {0x06, Temperate_TraceRawData},
    {0x07, Dvr_TraceRawData},
#endif
};

static boolean      log_raw_trace_en = FALSE;
static LOG_RAW_TIME log_raw_time[LOG_RAW_TIME_TBL_SIZE];

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

#if 0
static CallbackFunctionElement ap_state_cmd;
static CallbackFunctionElement system_reset_calbk_element;
static CallbackFunctionElement log_raw_ctrl;
#endif

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if 0
static	void	ApStateCmd(uint8 const len,char const *data);
static	void	SystemReset(uint8 const len,char const *data);
#endif
static  void    LogRawControl(uint8 const len,char const *data);
static  void    LogRawTraceTask(void);

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void 	LogCmd_Init(void)
{
	//TRACE_REGISTER_CALLBACK_FUNCTION(ApStateCmd,  &ap_state_cmd);
	//TRACE_REGISTER_CALLBACK_FUNCTION(SystemReset, &system_reset_calbk_element);
	//TRACE_REGISTER_CALLBACK_FUNCTION(LogRawControl, &log_raw_ctrl);
}
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if 0
static	void ApStateCmd(uint8 const len,char const *data)
{
	//MapMana_EnterToUsbDownload();
	boolean cmd_avbl = 0x00;
	
	for ( uint16 i = 0 ; i < sizeof(ap_state_cmd_tbl)/sizeof(CMD_INFO) && !cmd_avbl; i ++ )
	{
		CMD_INFO const*cmd_info = ap_state_cmd_tbl + i;
		if ( len == cmd_info->len )
		{	
			boolean diff = FALSE;
			/* check all received data */
			for(uint8 j = 0; j < len && !diff; j++)
			{
				if ( data[j] != cmd_info->cmd[j] )
				{
					diff = TRUE;
				}

			}

			if ( !diff )
			{
				cmd_avbl = TRUE;
				cmd_info->func();
			}
		}
	}

	if ( !cmd_avbl )
	{
		TRACE_VALUE4(LOG_ERR, MOD_ALWAYS, "ApStateCmd Error: 0x%02x,0x%02x,0x%02x,0x%02x,", data[0],data[1],data[2],data[3]);
	}
}
#endif

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if 0
static	void	SystemReset(uint8 const len,char const *data)
{
    TRACE_TEXT(LOG_SHOUT, MOD_ALWAYS, "[Trace] Recved reset cmd, now ready to reset...");
    BspMisc_SetSwResetReason(SW_RESET_TRACE_CMD);
	MapMana_ForceReset();
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static  void    LogRawControl(uint8 const len,char const *data)
{
    boolean is_valid_cmd = FALSE;
    for (uint8_t i = 0u; i < LOG_RAW_TIME_TBL_SIZE; i++)
    {
        const LOG_RAW_INFO *info = log_raw_info_tbl + i;
        if ((5 == len) && (data[0] == info->id))
        {
            log_raw_time[i].cyc_tm = ((uint16_t)data[1] << 8u) | data[2];
            log_raw_time[i].times  = ((uint16_t)data[3] << 8u) | data[4];

            if (!log_raw_trace_en)
            {
                //ServFunc_StartCyclic(ServFunc_Create(LogRawTraceTask, "Trace Raw data Task"), 0u, 10/10);
                log_raw_trace_en = TRUE;
            }

            is_valid_cmd = TRUE;
            break;
        }
    }

    if (!is_valid_cmd)
    {
        TRACE_VALUE2(LOG_ERR, MOD_ALWAYS, "error cmd, len=%d, id=0x%d...", len, data[0]);
    }
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static  void    LogRawTraceTask(void)
{
    for (uint8_t i = 0u; i < LOG_RAW_TIME_TBL_SIZE; i++)
    {
        if (log_raw_time[i].times > 0u)
        {
            if (log_raw_time[i].cyc_cnt > 0u)
            {
                log_raw_time[i].cyc_cnt --;
            }
            if (0u == log_raw_time[i].cyc_cnt)
            {
                const LOG_RAW_INFO *info = log_raw_info_tbl + i;
                if (NULL != info->func)
                {
                    info->func();
                }
                log_raw_time[i].cyc_cnt = log_raw_time[i].cyc_tm/10;
                if (0xffff != log_raw_time[i].times)
                {
                    log_raw_time[i].times --;
                }
            }
        }
    }
}
#endif

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


