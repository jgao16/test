/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
All rights reserved
>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
//#pragma once
#ifndef MAP_MANA__H_
#define MAP_MANA__H_

#include "stdint.h"
#include "std_type.h"

#ifdef  __cplusplus
extern "C"
{
#endif

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
// #include "gen_dev_sig.h"
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
typedef enum
{
	AP_POWER_OFF,
	AP_POWER_OFF_SEQ,		// power off sequence, wait board power off

	//AP_POWER_ON,			// Only vip, if this state is avaible
	AP_MAIN_POWER_ON,		// DO_VIP_POWER_HOLD, and wait PMIC_PWR_GOOD to high
	AP_MAIN_POWER_DELAY,	// delay 8ms then power 3v3
	AP_POWER_3V3,			// power 3v3 and delay 2ms , than power usb and camera

	AP_POWER_USB,			// power usb and camera, delay 4ms to next state
	//AP_POWER_5V_USB_H,		// delay 2ms,
	AP_USB_EN_RESET_RELEASE,	// set USB_OTG_EN,USB_HOST_EN, release  DO_VIP_SYSTEM_RESET#

	AP_ONLY_VIP,			// only CAN and dsp is activate, limphome (main power and 3v3, ap reset)

	AP_WAIT_U_BOOT,
	AP_WAIT_DEVIPC,
	AP_WAIT_TICK_MSG,		// wait mode manager tick message
	AP_STATE_RUNNING,

	AP_USB_DOWNLOAD,		// close devipc and power on until imx6 uboot ok
	AP_POWER_ON,
	AP_RAMDISK,

	AP_WAIT_SW_RESET,       //received sw reset cmd, like reboot, reconfig type...

	AP_STATE_MAX
}AP_STATE;


typedef enum
{
    DEV_SIG_SleepWakeUpSrc_PWR_ON,
    DEV_SIG_SleepWakeUpSrc_ACC_ON,
    DEV_SIG_SleepWakeUpSrc_IGN_ON,
    DEV_SIG_SleepWakeUpSrc_CAN_INH_ON,
    DEV_SIG_SleepWakeUpSrc_TCU_ACC_ON,
    DEV_SIG_SleepWakeUpSrc_UNKNOWN,
    WAKE_UP_PWR_ON      = DEV_SIG_SleepWakeUpSrc_PWR_ON,
    WAKE_UP_ACC_ON      = DEV_SIG_SleepWakeUpSrc_ACC_ON,
    WAKE_UP_IGN_ON      = DEV_SIG_SleepWakeUpSrc_IGN_ON,
    WAKE_UP_CAN_INH_ON  = DEV_SIG_SleepWakeUpSrc_CAN_INH_ON,
    WAKE_UP_TCU_ACC_ON  = DEV_SIG_SleepWakeUpSrc_TCU_ACC_ON,
    WAKE_UP_UNKNOWN     = DEV_SIG_SleepWakeUpSrc_UNKNOWN,

    WAKE_UP_MAX = 6

}WAKE_UP_SOURCE;

typedef enum
{
	SOC_POR_RESET 			= 	0x00,

	SOC_PMIC_ERR  			= 	0x40,
	SOC_UBOOT_ERR			=	0x41,
	SOC_KERNEL_ERR			=	0x42,
	SOC_DEVIPC_ACK_ERR		=   0x43,
	SOC_DEVIPC_WDT_ERR		=	0x44,
	SOC_WAIT_FIRST_TICK_ERR	=	0x45,
	SOC_WAIT_ALIVE_MSG_ERR	=	0x46,
	SOC_USB_MFG_ACC_CHG_RESET=	0x47,
	
	SOC_RESET_REASON_MAX
}SOC_RESET_REASON;

typedef struct
{
	uint8_t state1; /*** mode manager internal state ***/ 
	uint8_t state2; /*** mode manager internal state ***/ 
	uint16_t sequence; /*** increase 1 after tx mode manager alive message ***/ 

}ModeManagerAlive_T;	/*  */

typedef struct
{
	uint8_t wu_src; /***  ***/
	uint32_t wu_cnt; /***  ***/

}SleepWakeUpSrc_T;	/*  */

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void 	MapMana_SysStartup	(void);				// only enable main power (+3.3v)
void	MapMana_Init	   	(void);
//void  MapMana_10msFunc	(void);
void    MapMana_1msFunc	(void);

void	MapMana_ResetSocImagePart(void);

void 	MapMana_RcvImxCmd(void);
//boolean MapMana_Imx6RdyToSleep(void);

void 	MapMana_IpcWdtTmout(void);

void    MapMana_RequestPowerOff(void);

//MAP_STATE		MapMana_GetMapState	(void);		//return MAP_STATE
//DEV_IPC_STATE	MapMana_GetDevIpcState(void);	// return DEV_IPC_STATE
void 	MapMana_EnterToUsbDownload(void);
void	MapMana_ForceReset(void);
void 	MapMana_HoldPowerOn(void);

void    MapMana_MapPowerOffReq(void);
void    MapMana_MapRePowerOnReq(void);
void    MapMana_EnterSleepState(void);

uint8_t MapMana_GetWakeupSource(void);
uint32_t MapMana_GetWakeupSourceCnt(uint8_t source);
void    MapMana_GetSleepWakeUpSrc(SleepWakeUpSrc_T *info);

void 	MapMana_RxModeManagerAlive(ModeManagerAlive_T *msg);
void 	MapMana_IpcKernelTickLost(void);
void 	MapMana_RecordResetInfo(SOC_RESET_REASON soc_reason);

#ifdef  __cplusplus
}
#endif

#endif

