#if 0
/**********************************************************************************/
/* sys_pwr_mode.c                                                                   */

/**********************************************************************************/
#include "std_type.h"
#include "cpu.h"
#include "serv.h"
#include "hal_os.h"
#include "hal_wdt.h"

/** >>>>>>>> user include start  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#include "bsp_gpio.h"
#include "bat_mana.h"
//#include "meter_fuel.h"
#include "map_mana.h"
#include "gen_dev_sig.h"
#include "dev_ipc.h"
//#include "swc.h"
//#include "panel.h"
//#include "CanNm.h"
//#include "ComRTE.h"

#include "trace_api.h"

/** >>>>>>>> user include end    >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
/*
battery mode:   low off mode, low mode, normal mode, high mode, high off mode.

init mode: from sleep mode, battery low/high off mode, battery low/high mode
           vip/imx6 init.
           CAN init.
           peripheral device power on.

normal mode: from init mode, limit mode, battery low/high mode
             crank on/off

limit mode: from init mode, normal mode, battery low/high mode
            acc on/off

sleep mode: from limit mode, battery low/high mode
            all power off, sleep deep
            in this state, the cpu stops run.
*/
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#define POWER_MODE_DA_METER_KEY_STATE_EQ_DLY          (500 / 10 + 1)
#define POWER_MODE_DA_METER_KEY_STATE_NEQ_DLY         (5000 / 10 + 1)

#define POWER_MODE_IGN_OFF_DLY                        (15000 / 10 + 1) //ign off delay timer when eco drive is invalid
#define POWER_MODE_ECO_DRIVE_OVER_DLY                 (35000 / 10 + 1) //eco drive max wait 35s

#define POWER_MODE_METER_BK_EN_DLY                    (3000 / 10 + 1) //meter backlight enable max wait 4s.

#define SILENT_UPDATE_WAIT_RESPONSE_DLY               (5000 / 10 + 1) //5s timeout
#define SILENT_UPDATE_WAIT_START_DLY                  (10000 / 10 + 1) //10s timeout
#define SILENT_UPDATE_WAIT_DONE_DLY                   (30*60*1000 / 10 + 1) //30min timeout
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
typedef struct
{
    uint8_t da_key;
    uint8_t meter_key;
    uint8_t warning_type;
}DA_METER_KEY_TAB;

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static uint16_t ign_off_dly;            //ign off 1st delay timer.
static uint16_t eco_drive_wait_dly;     //eco drive wait delay timer.
static boolean  dev_pwr_onoff;          //disable all dev power req
static boolean  swc_onoff;              //swc onoff switch.
static boolean  req_meter_sleep;        //request CAN sleep flag

static uint16_t da_meter_eq_tm_dly;     //time delay for da eq meter key state
static uint16_t da_meter_neq_tm_dly;    //time delay for da not eq meter key state
static uint8_t  da_meter_warning;       //da and meter key state warning, 0=no warning.

static uint8_t  meter_backlight;        //meter valid backlight.
static boolean  meter_bk_en;            //trigger forcing meter backlight on.
static uint16_t meter_bk_en_dly;        //meter backlight on delay timer when da_meter_warning enable.

static uint8_t  silent_update_endis;    //enable silent update or not
static uint8_t  silent_update_state;    //recved silent update state from soc.
static uint32_t silent_update_wait_dly; //when silent update, wait time counter

#if 0
static const DA_METER_KEY_TAB da_meter_key_tab[] =
{
    {COMRTE_IGNSTATUS_RUN,          DEV_SIG_Meter_IgnStatus_CRANK,  DEV_SIG_DaMeterKeyStateWarning_DA_METER_KEY_STATE_WARNING_DA_RUN_METER_CRANK},
    {COMRTE_IGNSTATUS_RUN,          DEV_SIG_Meter_IgnStatus_ACC,    DEV_SIG_DaMeterKeyStateWarning_DA_METER_KEY_STATE_WARNING_DA_RUN_METER_ACC},
    {COMRTE_IGNSTATUS_RUN,          DEV_SIG_Meter_IgnStatus_OFF,    DEV_SIG_DaMeterKeyStateWarning_DA_METER_KEY_STATE_WARNING_DA_RUN_METER_OFF},
    {COMRTE_IGNSTATUS_START_CRANK,  DEV_SIG_Meter_IgnStatus_RUN,    DEV_SIG_DaMeterKeyStateWarning_DA_METER_KEY_STATE_WARNING_DA_CRANK_METER_RUN},
    {COMRTE_IGNSTATUS_START_CRANK,  DEV_SIG_Meter_IgnStatus_ACC,    DEV_SIG_DaMeterKeyStateWarning_DA_METER_KEY_STATE_WARNING_DA_CRANK_METER_ACC},
    {COMRTE_IGNSTATUS_START_CRANK,  DEV_SIG_Meter_IgnStatus_OFF,    DEV_SIG_DaMeterKeyStateWarning_DA_METER_KEY_STATE_WARNING_DA_CRANK_METER_OFF},
    {COMRTE_IGNSTATUS_ACC,          DEV_SIG_Meter_IgnStatus_RUN,    DEV_SIG_DaMeterKeyStateWarning_DA_METER_KEY_STATE_WARNING_DA_ACC_METER_RUN},
    {COMRTE_IGNSTATUS_ACC,          DEV_SIG_Meter_IgnStatus_CRANK,  DEV_SIG_DaMeterKeyStateWarning_DA_METER_KEY_STATE_WARNING_DA_ACC_METER_CRANK},
    {COMRTE_IGNSTATUS_ACC,          DEV_SIG_Meter_IgnStatus_OFF,    DEV_SIG_DaMeterKeyStateWarning_DA_METER_KEY_STATE_WARNING_DA_ACC_METER_OFF},
    {COMRTE_IGNSTATUS_OFF,          DEV_SIG_Meter_IgnStatus_RUN,    DEV_SIG_DaMeterKeyStateWarning_DA_METER_KEY_STATE_WARNING_DA_OFF_METER_RUN},
    {COMRTE_IGNSTATUS_OFF,          DEV_SIG_Meter_IgnStatus_CRANK,  DEV_SIG_DaMeterKeyStateWarning_DA_METER_KEY_STATE_WARNING_DA_OFF_METER_CRANK},
    {COMRTE_IGNSTATUS_OFF,          DEV_SIG_Meter_IgnStatus_ACC,    DEV_SIG_DaMeterKeyStateWarning_DA_METER_KEY_STATE_WARNING_DA_OFF_METER_ACC},
};
#endif

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void PowerMode_MainTask(void);
static void PowerMode_StateMachineHandle(void);
static void PowerMode_DaMeterKeyStateHandle(void);
static void PowerMode_SetMeterBacklightOnKeyState(void);

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void PowerMode_Init(void)
{
    ign_off_dly         = POWER_MODE_IGN_OFF_DLY;
    eco_drive_wait_dly  = 0u;  //default, eco drive invalid
    dev_pwr_onoff       = FALSE;
    swc_onoff           = FALSE;
    req_meter_sleep     = FALSE;

    da_meter_eq_tm_dly  = POWER_MODE_DA_METER_KEY_STATE_EQ_DLY;
    da_meter_neq_tm_dly = POWER_MODE_DA_METER_KEY_STATE_NEQ_DLY;
    da_meter_warning    = DEV_SIG_DaMeterKeyStateWarning_DA_METER_KEY_STATE_NO_WARNING;

    meter_backlight     = 0u;
    meter_bk_en         = FALSE;
    meter_bk_en_dly     = POWER_MODE_METER_BK_EN_DLY;

    silent_update_endis = DEV_SIG_SilentUpdateFeedback_DIS_SLIENT_UPDATE;
    silent_update_state = DEV_SIG_SilentUpdateState_INIT;
    silent_update_wait_dly = SILENT_UPDATE_WAIT_RESPONSE_DLY;

    //ServFunc_StartCyclic(ServFunc_Create(PowerMode_MainTask, "Power mode Task"), 5, 10/10);
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
boolean PowerMode_GetMeterReadyEnterSleepState(void)
{
    return (req_meter_sleep);
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
uint8_t PowerMode_GetDaMeterKeyStateWarning(void)
{
    return (da_meter_warning);
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
uint8_t PowerMode_GetMeterBacklight(void)
{
    return (meter_backlight);
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
uint8_t PowerMode_GetSilentUpdateEnDis(void)
{
    return silent_update_endis;
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void PowerMode_MainTask(void)
{
    //PowerMode_StateMachineHandle();
    //PowerMode_DaMeterKeyStateHandle();
    //PowerMode_SetMeterBacklightOnKeyState();
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if 0
static void PowerMode_StateMachineHandle(void)
{
    uint8_t batt_cur_state  = BatMana_GetBatState();
    uint8_t key_state       = ComRTE_GetIgnState();
    boolean sleep_con_meet  = FALSE;

    if ((COMRTE_IGNSTATUS_START_CRANK == key_state) || (COMRTE_IGNSTATUS_RUN == key_state))
    {
        ign_off_dly         = POWER_MODE_IGN_OFF_DLY;
        eco_drive_wait_dly  = POWER_MODE_ECO_DRIVE_OVER_DLY;
    }
    else
    {
        if (ign_off_dly > 0u)
        {
            ign_off_dly --;
        }
        if (eco_drive_wait_dly > 0u)
        {
            eco_drive_wait_dly --;
        }
    }

    if ((COMRTE_IGNSTATUS_OFF == key_state) //ign and acc off
    && ((TRUE == MeterFuel_GetIgnOff10sOver()) || (0u == ign_off_dly)) //10s fuel capture done.
    && ((0x01 == GenDevSig_GetVal_EcoDriveReportDone()) || (0u == eco_drive_wait_dly)) //eco drive report done.
    && ((CANNM_STATUS_SLEEP == CanNm_GetStatus()) || (CANNM_STATUS_STOP == CanNm_GetStatus()))) //can sleep or stop
    {
        sleep_con_meet = FALSE;
    }
    else
    {
        sleep_con_meet = FALSE;
    }

    if (sleep_con_meet)
    {
        boolean ready_sleep   = FALSE;
        uint8_t silent_update = GenDevSig_GetVal_SilentUpdateState();

        if (silent_update_wait_dly > 0u)
        {
            silent_update_wait_dly --;
        }

        silent_update_endis = DEV_SIG_SilentUpdateFeedback_EN_SLIENT_UPDATE;

        if (silent_update_state != silent_update)
        {
            if ((DEV_SIG_SilentUpdateState_NO_NEED_UPDATE == silent_update)
             || (DEV_SIG_SilentUpdateState_UPDATE_DONE == silent_update))
            {
                ready_sleep = TRUE; //no need silent update or update failed, sleep now.
                TRACE_VALUE(LOG_SHOUT, MOD_HW, "[Power mode] prev silent_update_state=%d, now ready to sleep...", silent_update_state);
            }
            else if (DEV_SIG_SilentUpdateState_INF_SILENT_UPDATE == silent_update)
            {
                if (DEV_SIG_SilentUpdateState_INIT == silent_update_state)
                {
                    silent_update_wait_dly = SILENT_UPDATE_WAIT_START_DLY; //now wait soc to update, max 10s
                    TRACE_TEXT(LOG_SHOUT, MOD_HW, "[Power mode] Now wait soc ready to update, max 10s...");
                }
                else
                {
                    ready_sleep = TRUE; //step error, sleep now.
                    TRACE_VALUE(LOG_ERR, MOD_HW, "[Power mode] rcved update information, but prev state=%d, now ready to sleep...", silent_update_state);
                }
            }
            else if (DEV_SIG_SilentUpdateState_SILENT_UPDATING == silent_update)
            {
                if (DEV_SIG_SilentUpdateState_INF_SILENT_UPDATE == silent_update_state)
                {
                    silent_update_wait_dly = SILENT_UPDATE_WAIT_DONE_DLY; //now start to wait silent update, max 30min
                    TRACE_TEXT(LOG_SHOUT, MOD_HW, "[Power mode] Now wait soc updating , max 30min...");
                }
                else
                {
                    ready_sleep = TRUE; //step error, sleep now.
                    TRACE_VALUE(LOG_ERR, MOD_HW, "[Power mode] in updating, but prev state=%d, now ready to sleep...", silent_update_state);
                }
            }
            else{}

            silent_update_state = silent_update;
        }

        if (ready_sleep || (0u == silent_update_wait_dly))
        {
            TRACE_VALUE2(LOG_SHOUT, MOD_HW, "[Power mode] Now send request to meter, 100ms waiting, eco drive from imx=%d, local timer=%d ms...", GenDevSig_GetVal_EcoDriveReportDone(), (uint16_t)(eco_drive_wait_dly * 10));
            req_meter_sleep = TRUE;
//            HalOS_Delay(100); //wait about 100ms until meter received request.
            MapMana_EnterSleepState();
        }
    }
    else
    {
        silent_update_endis    = DEV_SIG_SilentUpdateFeedback_DIS_SLIENT_UPDATE;
        silent_update_state    = DEV_SIG_SilentUpdateState_INIT;
        silent_update_wait_dly = SILENT_UPDATE_WAIT_RESPONSE_DLY;
    }

    if (!dev_pwr_onoff)
    {
        if ((BAT_VERY_LOW == batt_cur_state) || (BAT_VERY_HIGH == batt_cur_state)) //enter bat off mode
        {
            MapMana_MapPowerOffReq();
            dev_pwr_onoff = TRUE;
            TRACE_VALUE(LOG_SHOUT, MOD_HW, "[Power mode] Battery now enter mode %d (2:high off,4:low off)...", batt_cur_state);
        }
    }
    else
    {
        if ((BAT_VERY_LOW != batt_cur_state) && (BAT_VERY_HIGH != batt_cur_state))
        {
            MapMana_MapRePowerOnReq();
            dev_pwr_onoff = FALSE;
            TRACE_VALUE(LOG_SHOUT, MOD_HW, "[Power mode] Battery now back to mode %d(0:normal,1:high,3:low), restart power......", batt_cur_state);
        }
    }

    if (!swc_onoff)
    {
        if ((BAT_NORMAL == batt_cur_state) && ((COMRTE_IGNSTATUS_ACC == key_state) || (COMRTE_IGNSTATUS_RUN == key_state)))
        {
            SwcKey_SetKeyDetectOnoff(TRUE);
            Panel_SetKeyDetectOnoff(TRUE);
            swc_onoff = TRUE;
            TRACE_VALUE2(LOG_SHOUT, MOD_HW, "[Power mode] Now enable swc and panel control, batt_cur_state=%d, key_state=%d...", batt_cur_state, key_state);
        }
    }
    else
    {
        if (((BAT_LOW == batt_cur_state) || (BAT_HIGH == batt_cur_state)) || ((COMRTE_IGNSTATUS_ACC != key_state) && (COMRTE_IGNSTATUS_RUN != key_state)))
        {
            SwcKey_SetKeyDetectOnoff(FALSE);
            Panel_SetKeyDetectOnoff(FALSE);
            swc_onoff = FALSE;
            TRACE_VALUE2(LOG_SHOUT, MOD_HW, "[Power mode] Now disable swc and panel control, batt_cur_state=%d, key_state=%d...", batt_cur_state, key_state);
        }
    }
}
#endif

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if 0
static void PowerMode_DaMeterKeyStateHandle(void)
{
    uint8_t meter_key_state = GenDevSig_GetVal_Meter_IgnStatus();
    uint8_t da_key_state    = ComRTE_GetIgnState();

    if (IPC_STATE_RUNNING == DevIpc_GetState(DEV_IPC_IIC_IC))
    {
        if (meter_key_state == da_key_state)
        {
            da_meter_neq_tm_dly = POWER_MODE_DA_METER_KEY_STATE_NEQ_DLY;
        }
        else
        {
            da_meter_eq_tm_dly  = POWER_MODE_DA_METER_KEY_STATE_EQ_DLY;
        }

        if (da_meter_eq_tm_dly > 0u)
        {
            da_meter_eq_tm_dly --;
        }
        if (da_meter_neq_tm_dly > 0u)
        {
            da_meter_neq_tm_dly --;
        }

        if (0u == da_meter_eq_tm_dly)
        {
            da_meter_warning = DEV_SIG_DaMeterKeyStateWarning_DA_METER_KEY_STATE_NO_WARNING;
        }
        else if (0u == da_meter_neq_tm_dly)
        {
            for (uint8_t i = 0u; i < sizeof(da_meter_key_tab)/sizeof(DA_METER_KEY_TAB); i++)
            {
                if ((da_key_state == da_meter_key_tab[i].da_key) && (meter_key_state == da_meter_key_tab[i].meter_key))
                {
                    da_meter_warning = da_meter_key_tab[i].warning_type;
                    break;
                }
            }
            TRACE_VALUE4(LOG_INFO, MOD_HW, "[Power mode] DA and meter key state different, da_key_state=%d, meter_key_state=%d, da_meter_warning=%d...", da_key_state, meter_key_state, da_meter_warning, 0u);
        }
        else{}
    }
    else
    {
        da_meter_warning    = DEV_SIG_DaMeterKeyStateWarning_DA_METER_KEY_STATE_NO_WARNING;
        da_meter_eq_tm_dly  = POWER_MODE_DA_METER_KEY_STATE_EQ_DLY;
        da_meter_neq_tm_dly = POWER_MODE_DA_METER_KEY_STATE_NEQ_DLY;
    }
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void PowerMode_SetMeterBacklightOnKeyState(void)
{
    if (DEV_SIG_DaMeterKeyStateWarning_DA_METER_KEY_STATE_NO_WARNING == da_meter_warning)
    {
        meter_backlight = GenDevSig_GetVal_Meter_BackLight();
        meter_bk_en     = FALSE;
        meter_bk_en_dly = POWER_MODE_METER_BK_EN_DLY;
    }
    else
    {
        if (meter_bk_en_dly > 0u)
        {
            meter_bk_en_dly --;
        }
        if ((DEV_SIG_MeterHMIShowEn_SHOW_VALID == GenDevSig_GetVal_MeterHMIShowEn()) || (0u == meter_bk_en_dly))
        {
            if (!meter_bk_en)
            {
                TRACE_VALUE2(LOG_SHOUT, MOD_HW, "[Power mode] Meter backlight force to 100%, signal MeterHMIShowEn=%d(1:en), meter_bk_en_dly=%d(0:timeout)...", GenDevSig_GetVal_MeterHMIShowEn(), meter_bk_en_dly);
                meter_bk_en = TRUE;
            }
            meter_backlight = 100; //force to turn on until the key state back to normal.
        }
    }
}
#endif

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#else
void    PowerMode_Init(void)
{
}
#endif
