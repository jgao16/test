/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    Service config:
    All rights reserved
>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

#ifndef POWER_MODE_H_H_
#define POWER_MODE_H_H_


#ifdef  __cplusplus
extern "C"
{
#endif




/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void    PowerMode_Init(void);
boolean PowerMode_GetReadyEnterSleepState(void);
boolean PowerMode_GetMeterReadyEnterSleepState(void);
uint8_t PowerMode_GetDaMeterKeyStateWarning(void);
uint8_t PowerMode_GetMeterBacklight(void);
uint8_t PowerMode_GetSilentUpdateEnDis(void);

#ifdef  __cplusplus
}
#endif


#endif
