#if 0
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
All rights reserved
>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#include "std_type.h"
#include "map_mana.h"
#include "bat_mana.h"
#include "bsp_gpio.h"
#include "cpu.h"
#include "dev_ipc.h"

#include "hal_os.h"
#include "bsp_audio.h"
#include "hal_gpio.h"
#include "trace_api.h"
#include "hal_wdt.h"
#include "hal_ftm.h"
#include "hal_uart.h"
#include "hal_adc.h"
#include "hal_hw.h"
#include "hal_int.h"

#include "gen_eep_mana.h"

#if 0
#include "CanTask.h"
#include "CanNm.h"
#endif

#include "lk_uboot.h"

#include "board_hw.h"

#include "bsp_misc.h"

#if 0
#include "tdf8546.h"
#endif

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#define	HW_MAIN_POWER_STABLE_TM	100u	// Port_MainPowerEn;DO_VIP_POWER_HOLD
#define IMX6_POWER_OFF_MIN_TM	1000u	// power off min time: 1000ms
//#define	IMX6_START_UP_TM		800u
//#define	IMX6_POWERMODING_START	60000u	// imx6 power moding startup, 60 second
//#define	IMX6_POWERMODING_TICK	60000u	// imx6 power moding startup, 60 second, 1 minute

#define MAP_POWEROFF_DL_TM      10u     //power off dev delay timer, max wait 10ms for can nm

#define	IMX6_HW_FAULT_CNT_LVL	3u

#define APP_MSG_FIRST_TICK	(20000)		// 20s
#define APP_MSG_TICK_TIMER	(15000)		// 15s

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
/*imx6  state*/
static AP_STATE     	ap_state;



static boolean			rx_imx6_alive_msg;
static boolean			ipc_kernel_tick_lost;
static uint16			uboot_error_cnt;
static uint16 			map_state_last_tm;
static boolean          map_poweroff_req;
static uint16           map_poweroff_delay_timer;
static boolean          req_nm_restart;
static boolean			last_acc_state;

__no_init static uint8_t  wakeup_source;            //wakeup source.
__no_init static uint32_t wakeup_cnt[WAKE_UP_MAX];  //wakeup source occured counters.
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void MapManaStateChange(void);
static void MapPowerOffProcess(void);
static void MapPowerDisAllPeriDev(void);

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void	MapMana_Init	   	(void)
{
    //imx6_hw_fault_cnt			=	0x00;
    uboot_error_cnt     		=   0x00;

    ap_state 					= 	AP_MAIN_POWER_ON;
    map_state_last_tm			=	HW_MAIN_POWER_STABLE_TM;	/* about 100ms */
    map_poweroff_req         	=   FALSE;
    map_poweroff_delay_timer    =   0u;
    req_nm_restart           	=   FALSE;

    /*if ( Bsp_MainPowerGood() )
    {
        ap_state 			=	AP_MAIN_POWER_DELAY;
        map_state_last_tm	=	8;	
    }*/
}


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void    MapMana_1msFunc	(void)
{
    if ( map_state_last_tm > 0 )
    {
        map_state_last_tm --;
    }

    MapManaStateChange();
    MapPowerOffProcess();
}
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if 0
void	MapMana_ResetSocImagePart(void)
{
	uint8 using_backup;
	GenEep_Get_VipSwdlShareMem_Ex(&using_backup,USING_BACKUPIMAGE, 1);

    if ( using_backup == 0x01 ) 	/* use backup ramdisk*/
    {
        BspGpio_SetVal(ApEnterImageB_ID,HIGH);
    }
    else 	/* Normal ramdisk*/
    {
        BspGpio_SetVal(ApEnterImageB_ID,LOW);
    }
    TRACE_VALUE2(LOG_SHOUT, MOD_HW, "SOC Enter(using Eeprom) Req Rootfs B=%d, Real Rootfs B=%d", using_backup,BspGpio_GetVal(ApEnterImageB_ID));
}
#endif

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void MapMana_RxModeManagerAlive(ModeManagerAlive_T *msg)
{
	(void)msg;
	rx_imx6_alive_msg = TRUE;
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void MapMana_IpcKernelTickLost(void)
{
	ipc_kernel_tick_lost = TRUE;
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void MapMana_RecordResetInfo(SOC_RESET_REASON soc_reason)
{
	uint8 reason[32];
	GenEep_Get_SocResetReason(reason);
	for(uint8 i = 31; i > 0; i --)
	{
		reason[i] = reason[i-1];
	}
	reason[0] = soc_reason;
	uint8 using_backup;
	GenEep_Get_VipSwdlShareMem_Ex(&using_backup,USING_BACKUPIMAGE, 1);
	if ( using_backup )
	{
		reason[0] |= 0x80;
	}
	GenEep_Set_SocResetReason(reason);
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
//boolean MapMana_Imx6RdyToSleep(void)
//{
//    return (ap_state == AP_POWER_OFF /*|| imx6_state == MAP_PRE_SLEEP*/ || imx6_hw_fault_cnt > IMX6_HW_FAULT_CNT_LVL );
//}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void ProcSocRunningError(void)
{
	uboot_error_cnt ++;
	map_state_last_tm = 1000;
    ap_state          = AP_POWER_USB;
    //Bsp_UsbDis();
	//Bsp_MapResetAssert();
	BspMisc_SetSwResetReason(SW_RESET_IPC_KERNEL);
}
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void MapManaStateChange(void)
{
    boolean hw_fault = FALSE;
    switch(ap_state)
    {
    case AP_POWER_OFF:
        uboot_error_cnt     =   0x00;
        if ( map_state_last_tm == 0x00 &&
		   ((BatMana_GetBatState() == BAT_NORMAL) || (BatMana_GetBatState() == BAT_HIGH) ||(BatMana_GetBatState() == BAT_LOW)) )
        {
            //Bsp_MainPowerEn();
            ap_state 			= 	AP_MAIN_POWER_ON;
            map_state_last_tm	=	HW_MAIN_POWER_STABLE_TM;	/* about 100ms */
        }
        break;
	case AP_POWER_OFF_SEQ:
		if ( map_state_last_tm == 0x00 &&  ((BatMana_GetBatState() == BAT_NORMAL) || (BatMana_GetBatState() == BAT_HIGH) ||(BatMana_GetBatState() == BAT_LOW))  ){
			//ap_state = AP_POWER_OFF;
		    TRACE_TEXT(LOG_SHOUT,MOD_HW,"[Map Mana] recover from power off mode, now ready to reset...");
			HalWdt_RstCpu();
		}
		break;
    case AP_MAIN_POWER_ON:
        /*if ( Bsp_MainPowerGood() )
        {
            ap_state 			=	AP_MAIN_POWER_DELAY;
            map_state_last_tm	=	8;
        }
        else if ( map_state_last_tm == 0x00 )
        {
            hw_fault = TRUE;
        }
        else {}*/
        break;

    case AP_MAIN_POWER_DELAY:
        if ( map_state_last_tm == 0x00 )
        {
            //Bsp_Aux3v3PowerEn();
            if (!req_nm_restart)
            {
                CanTask_SetStartFlag(1);
            }
            else
            {
                CanNm_Start();
                req_nm_restart = FALSE;
            }
            ap_state 		  = AP_POWER_3V3;
            map_state_last_tm = 2; // 2ms
        }
        else {}
        break;

    case AP_POWER_3V3:
        if ( map_state_last_tm == 0x00 )
        {
            //Bsp_UsbPowerEn();
//            Bsp_TunerAntPwrEn(); //change to dev_pwr.
//            Bsp_MicPwrEn();   //CAN ID 351, remote engine run flag
            ap_state 		  = AP_POWER_USB;
            map_state_last_tm = (DEV_SIG_WakeUpReason_BATTERY_ON == BspMisc_GetWakeUpReason()) ? 40:10; // fixed, power usb->power OTG;first bat on, delay more time.

            BspAudio_PowerState(DSP_POWER_ON);	// dsp is power on ,start vip early audio
            //Bsp_LvdsPDBEn(); //wakeup 927
        }
        else {}
        break;

    case AP_POWER_USB:
		if ( map_state_last_tm == 0x00 && EepMana_InitedComp() )
        {
			//boolean soc_start = TRUE;
			if ( uboot_error_cnt > 0xFF00 ){
				uboot_error_cnt = 0xFF00;
			}
			//if ( uboot_error_cnt < 3 )
			//{
			//	soc_start = TRUE;
			//}
			//else
			uint8 using_backup;
			GenEep_Get_VipSwdlShareMem_Ex(&using_backup,USING_BACKUPIMAGE, 1);				
			if ( uboot_error_cnt >= 3 )	// max try 3 times
			{
				uint8 image_state;
				GenEep_Get_VipSwdlShareMem_Ex(&image_state,IMAGE_STATE_, 1);
				/* set a rootfs error ... */
				if ( using_backup == 0x00 ){	// normal error
					image_state |= NORMAL_ROOTFS_BIT;
				}else{	// backup error
					image_state |= BACKUP_ROOTFS_BIT;
				}
				GenEep_Set_VipSwdlShareMem_Ex(&image_state,IMAGE_STATE_, 1);
#if 0	//
				if ( (image_state&NORMAL_ROOTFS_BIT) == 0x00 || (image_state&BACKUP_ROOTFS_BIT) == 0x00 )	// there is a rootfs is ok
				{
					uboot_error_cnt = 0x00;
					using_backup   ^= 0x01;
					GenEep_Set_VipSwdlShareMem_Ex(&using_backup,USING_BACKUPIMAGE, 1);
					//soc_start = TRUE;
					
					TRACE_VALUE(LOG_SHOUT, MOD_HW, "Soc Rootfs Change to Partition=%d(0:A,1:B)",using_backup);
				}
#endif			
			}
            //if ( soc_start )
            //{
			//uint8 image_state;
			//GenEep_Get_VipSwdlShareMem_Ex(&image_state,IMAGE_STATE_, 1);
			//Bsp_UsbEn();			// usb power on
			//Bsp_MapResetRelease();	// release imx6 reset pin, imx6 now start
			MapMana_ResetSocImagePart();

            ap_state 		  = AP_USB_EN_RESET_RELEASE;
          	map_state_last_tm = 10; // 10ms
			//uint8 using_backup;
			//GenEep_Get_VipSwdlShareMem_Ex(&using_backup,USING_BACKUPIMAGE, 1);
          	TRACE_VALUE2(LOG_SHOUT, MOD_HW, "Vip Wait Uboot Start Pin to High Now, error Cnt=%d; Partition=%d(0:A, 1:B)",uboot_error_cnt,using_backup);
			//}
            //else
            //{
			//	/*check if all image is error(including rootfs and ramdisk)*/
            //    Bsp_SetApToMfg();
            //    ap_state = AP_USB_DOWNLOAD;
            //    TRACE_TEXT(LOG_ERR,MOD_HW,"Ap Enter to usb download mode as of uboot error");
            
			//	Bsp_UsbEn();
			//	Bsp_MapResetRelease();
			//	last_acc_state = Bsp_GetACCEn();	// recorde ADCC state
			//}
        }
        else {}
        break;

    case AP_USB_EN_RESET_RELEASE:
        if ( map_state_last_tm == 0x00 )
        {
            ap_state 			= AP_WAIT_U_BOOT;
            map_state_last_tm 	= 3000;		// uboot max 1000ms
        }
        break;

    case AP_WAIT_U_BOOT:
        if ( BspGpio_GetHwInsVal(UBOOT_AVBL_ID) )
        {
            DevIpc_StartReq(DEV_IPC_SPI_0);	// start devipc
            ap_state	= AP_WAIT_DEVIPC;
            if ( BspGpio_GetHwInsVal(UsbMfgAvbl_ID) )
			{
                map_state_last_tm = 30000;	// wait 10000ms to sync with ap. (USB MFG need 7s)
				TRACE_TEXT(LOG_SHOUT, MOD_HW, "Ap uboot start(Wait USB Mfg Uboot)..");
			}
			else
			{
                map_state_last_tm = 8000;
				TRACE_TEXT(LOG_SHOUT, MOD_HW, "Ap uboot start(Wait Rootfs Uboot)..");
            }
        }
        else if ( map_state_last_tm == 0x00 )
        {
            // uboot fail, reset ap 100ms
            ProcSocRunningError();
			TRACE_TEXT(LOG_ERR,MOD_HW,"UBoot Error,Reset Ap Now..");
			MapMana_RecordResetInfo(SOC_UBOOT_ERR);	// set uboot error reset info
        }
        else {}
        break;

    case AP_WAIT_DEVIPC:
        if ( DevIpc_GetState(DEV_IPC_SPI_0) == IPC_STATE_RUNNING )
        {
            ap_state 			= AP_WAIT_TICK_MSG;
            map_state_last_tm 	= APP_MSG_FIRST_TICK;	// wait first tick
        }
        else if ( map_state_last_tm == 0x00 )
        {
            // sync with ap fail, reset ap now
            DevIpc_StopReq(DEV_IPC_SPI_0);	// stop devipc
            ProcSocRunningError();
            TRACE_TEXT(LOG_ERR,MOD_HW,"Kernel Error,Reset Ap Now..");
			MapMana_RecordResetInfo(SOC_KERNEL_ERR);	// set soc kernel reset info
        }
        else {}
        break;

	case AP_WAIT_TICK_MSG:
		if ( !BspGpio_GetHwInsVal(UsbMfgAvbl_ID) )
		{
			if ( map_state_last_tm == 0x00 || ipc_kernel_tick_lost || DevIpc_GetState(DEV_IPC_SPI_0) != IPC_STATE_RUNNING )
			{
				if ( map_state_last_tm == 0x00 ){
					MapMana_RecordResetInfo(SOC_WAIT_FIRST_TICK_ERR);
				}else if ( ipc_kernel_tick_lost ){
					MapMana_RecordResetInfo(SOC_DEVIPC_WDT_ERR);
				}else{
					MapMana_RecordResetInfo(SOC_DEVIPC_ACK_ERR);
				}
				ipc_kernel_tick_lost = FALSE;	// clear lost flag
				DevIpc_StopReq(DEV_IPC_SPI_0);	// stop devipc
	            ProcSocRunningError();
	            TRACE_TEXT(LOG_ERR,MOD_HW,"Wait SOC First tick Error,Reset Ap Now..");

			}
			else if (rx_imx6_alive_msg)
			{
				rx_imx6_alive_msg 	= FALSE;

				ap_state 			= AP_STATE_RUNNING;
	            map_state_last_tm 	= APP_MSG_TICK_TIMER;	// wait user cyclic, app tick,
				/* soc now is running, clear rootfs error flag */
				uint8 image_state;
				GenEep_Get_VipSwdlShareMem_Ex(&image_state,IMAGE_STATE_, 1);
				uint8 using_backup;
				GenEep_Get_VipSwdlShareMem_Ex(&using_backup,USING_BACKUPIMAGE, 1);
				if ( using_backup == 0x00 ){	// normal error
					image_state &= ~NORMAL_ROOTFS_BIT;
				}else{	// backup error
					image_state &= ~BACKUP_ROOTFS_BIT;
				}
				GenEep_Set_VipSwdlShareMem_Ex(&image_state,IMAGE_STATE_, 1);
			}
			else{}
		}
		break;

	case AP_STATE_RUNNING:
		if ( map_state_last_tm == 0x00 || ipc_kernel_tick_lost || DevIpc_GetState(DEV_IPC_SPI_0) != IPC_STATE_RUNNING )
		{
			if ( map_state_last_tm == 0x00 ){
				MapMana_RecordResetInfo(SOC_WAIT_ALIVE_MSG_ERR);
			}else if ( ipc_kernel_tick_lost ){
				MapMana_RecordResetInfo(SOC_DEVIPC_WDT_ERR);
			}else{
				MapMana_RecordResetInfo(SOC_DEVIPC_ACK_ERR);
			}	

			ipc_kernel_tick_lost = FALSE;
			DevIpc_StopReq(DEV_IPC_SPI_0);	// stop devipc
            ProcSocRunningError();
			//uboot_error_cnt = 0xFF;// app error, assume complete error
            TRACE_TEXT(LOG_ERR,MOD_HW,"Wait SOC tick Error,Reset Ap Now..");

			/* set rootfs error flag */
			//uint8 image_state;
			//GenEep_Get_VipSwdlShareMem_Ex(&image_state,IMAGE_STATE_, 1);
			//uint8 using_backup;
			//GenEep_Get_VipSwdlShareMem_Ex(&using_backup,USING_BACKUPIMAGE, 1);
			//if ( using_backup == 0x00 ){	// normal error
			//	image_state |= NORMAL_ROOTFS_BIT;
			//}else{	// backup error
			//	image_state |= BACKUP_ROOTFS_BIT;
			//}
			//GenEep_Set_VipSwdlShareMem_Ex(&image_state,IMAGE_STATE_, 1);
		}
		else if (rx_imx6_alive_msg)
		{
			rx_imx6_alive_msg 	= FALSE;
            map_state_last_tm 	= APP_MSG_TICK_TIMER;	// wait user cyclic, app tick,
		}
		else{}
		break;
		
    case AP_USB_DOWNLOAD:
		/*if ( !last_acc_state && Bsp_GetACCEn() )
		{	// ACC LOW -> HIGH, reboot now 
			Bsp_SetApToNormal();		// reset Ap to normal
			ProcSocRunningError();
			uboot_error_cnt	= 0x00;	// clear count as of acc low -> high
			TRACE_TEXT(LOG_ERR,MOD_HW,"App USB download Mode Reset As of ACC LOW->HIGH");
			MapMana_RecordResetInfo(SOC_USB_MFG_ACC_CHG_RESET);	// set uboot error reset info
			
		}*/
		//last_acc_state = Bsp_GetACCEn();
		break;

    default:
        break;
    }
	/*if ( !Bsp_MainPowerGood()
	&& (ap_state != AP_POWER_OFF && ap_state != AP_POWER_OFF_SEQ && ap_state != AP_MAIN_POWER_ON && ap_state != AP_WAIT_SW_RESET) )
	{
		MapMana_RecordResetInfo(SOC_PMIC_ERR);	// PMIC error
		
    	hw_fault = TRUE;
    	BspMisc_SetSwResetReason(SW_RESET_PMIC_ERR);
    	TRACE_VALUE(LOG_ERR,MOD_HW,"[Map mana]PMIC power good error, in state %d...", ap_state);
	}*/
    if ( hw_fault || map_poweroff_req)
    {
        /*stop devspi communication 	*/
        DevIpc_StopReq(DEV_IPC_SPI_0);
        DevIpc_StopReq(DEV_IPC_IIC_IC);  // stop iic devipc

        CanNm_Stop();   //CAN Nm stop firstly
        req_nm_restart              = TRUE;

        map_poweroff_req 			= FALSE;
        map_poweroff_delay_timer    = MAP_POWEROFF_DL_TM;
        map_state_last_tm 			= IMX6_POWER_OFF_MIN_TM;	/* set map power off time 		*/
        
		ap_state					= AP_POWER_OFF_SEQ;
    }
}
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void	MapMana_ForceReset(void)
{
    /* disble all port */
    ap_state = AP_WAIT_SW_RESET;
    map_poweroff_delay_timer = 0u;
    Tdf8546_AmpDis();
    DevIpc_StopReq(DEV_IPC_SPI_0);   // stop spi devipc
    DevIpc_StopReq(DEV_IPC_IIC_IC);  // stop iic devipc
    MapPowerDisAllPeriDev();
    HalOS_Delay(500);               //delay about 500ms to power off device power fully.
    HalWdt_RstCpu();
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void MapMana_EnterToUsbDownload(void)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();

    ap_state = AP_USB_DOWNLOAD;
    map_poweroff_delay_timer = 0u;

    CPU_CRITICAL_EXIT();

    DevIpc_StopReq(DEV_IPC_SPI_0);	// stop devipc

    //Bsp_UsbDis();
    //Bsp_MapResetAssert();
    HalOS_Delay(100);
    //Bsp_SetApToMfg();
    TRACE_TEXT(LOG_ERR,MOD_HW,"Ap Enter to usb download mode as of vip command..");
    //Bsp_UsbEn();
    //Bsp_MapResetRelease();
}
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void MapMana_HoldPowerOn(void)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();

    ap_state = AP_POWER_ON;
    map_poweroff_delay_timer = 0u;

    CPU_CRITICAL_EXIT();

    DevIpc_StopReq(DEV_IPC_SPI_0);	// stop devipc

    //Bsp_SetApToNormal();
    //Bsp_UsbDis();
    BspGpio_SetVal(ApEnterRamdisk_ID, ACTIVATE);
    //Bsp_MapResetAssert();
    HalOS_Delay(100);
    TRACE_TEXT(LOG_ERR,MOD_HW,"Ap Power On Hold From Vip, Enter To Ramdisk From Vip Command..");
    //Bsp_UsbEn();
    //Bsp_MapResetRelease();
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void MapPowerDisAllPeriDev(void)
{
    HalFtm_ChSetPwmVal(SCREEN_BL_ID, 0u);

    BspAudio_PowerState(DSP_POWER_OFF);
    //Bsp_AmpStbAssert();
    //Bsp_HeroResetAssert();

    //Bsp_MapResetAssert();
    //Bsp_CameraPowerDis();
    //Bsp_UsbDis();
    //Bsp_UsbPowerDis();
    //Bsp_TunerAntPwrDis();
    //Bsp_MicPwrDis();
    //Bsp_LvdsPDBDis();
    //Bsp_GPSWakeupDis();
    //Bsp_Aux3v3PowerDis();
    //Bsp_MainPowerDis();
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void MapMana_MapPowerOffReq(void)
{
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();
    map_poweroff_req = TRUE;
    BspMisc_SetSwResetReason(SW_RESET_BAT_OFF);
    CPU_CRITICAL_EXIT();
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void MapMana_MapRePowerOnReq(void)
{
    //CPU_SR_ALLOC();
    //CPU_CRITICAL_ENTER();
    //ap_state = AP_POWER_OFF;
    //CPU_CRITICAL_EXIT();
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void DisAllUnusedInt(void)
{
    //disable systick
    POWER_API const* api = BoardHw_GetPwrCtrlApi();
    if ((NULL != api) && (NULL != api->exit_hp))
    {
        api->exit_hp();
    }

    //disable all unused interrupt.
    HalInt_Dis(DMA_Error_IRQn);
    HalInt_Dis(DMA1_IRQn);
    HalInt_Dis(DMA3_IRQn);
    HalInt_Dis(DMA5_IRQn);
    HalInt_Dis(LPI2C0_Master_IRQn);
    HalInt_Dis(LPI2C1_Master_IRQn);
    HalInt_Dis(LPUART0_RxTx_IRQn);
    HalInt_Dis(LPUART1_RxTx_IRQn);
    HalInt_Dis(LPUART2_RxTx_IRQn);
    HalInt_Dis(PORTA_IRQn);
    HalInt_Dis(PORTB_IRQn);
    HalInt_Dis(CAN0_ORed_IRQn);
    HalInt_Dis(CAN0_Error_IRQn);
    HalInt_Dis(CAN0_Wake_Up_IRQn);
    HalInt_Dis(CAN0_ORed_0_15_MB_IRQn);
    HalInt_Dis(CAN0_ORed_16_31_MB_IRQn);

    //disable all gpio interrupt.
    HalGpio_DisPinInt(HAL_GPIO_PT_D, 19);
    HalGpio_DisPinInt(HAL_GPIO_PT_D, 18);
    HalGpio_DisPinInt(HAL_GPIO_PT_A, 30);
    HalGpio_DisPinInt(HAL_GPIO_PT_D, 15);
    HalGpio_DisPinInt(HAL_GPIO_PT_D, 13);
    HalGpio_DisPinInt(HAL_GPIO_PT_B, 18);
    HalGpio_DisPinInt(HAL_GPIO_PT_B, 20);
    HalGpio_DisPinInt(HAL_GPIO_PT_B, 21);
    HalGpio_DisPinInt(HAL_GPIO_PT_E, 8);
    HalGpio_DisPinInt(HAL_GPIO_PT_B, 5);
    HalGpio_DisPinInt(HAL_GPIO_PT_B, 23);

    HalInt_DisAll(); //now stop response all interrupt.
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void ReconfigAllGPIO(void)
{
    HalGpio_InitPinGpio_Def(HAL_GPIO_PT_D, 19, PORT_OUTPUT,OUTPUT_OPEN_DRAIN,INPUT_NO_PULL);
    HalGpio_InitPinGpio_Def(HAL_GPIO_PT_D, 18, PORT_OUTPUT,OUTPUT_OPEN_DRAIN,INPUT_NO_PULL);
    HalGpio_InitPinGpio_Def(HAL_GPIO_PT_A, 30, PORT_OUTPUT,OUTPUT_OPEN_DRAIN,INPUT_NO_PULL);
    HalGpio_InitPinGpio_Def(HAL_GPIO_PT_D, 15, PORT_OUTPUT,OUTPUT_OPEN_DRAIN,INPUT_NO_PULL);
    HalGpio_InitPinGpio_Def(HAL_GPIO_PT_D, 13, PORT_OUTPUT,OUTPUT_OPEN_DRAIN,INPUT_NO_PULL);
    HalGpio_InitPinGpio_Def(HAL_GPIO_PT_B, 18, PORT_OUTPUT,OUTPUT_OPEN_DRAIN,INPUT_NO_PULL);
    HalGpio_InitPinGpio_Def(HAL_GPIO_PT_B, 20, PORT_OUTPUT,OUTPUT_OPEN_DRAIN,INPUT_NO_PULL);
    HalGpio_InitPinGpio_Def(HAL_GPIO_PT_B, 21, PORT_OUTPUT,OUTPUT_OPEN_DRAIN,INPUT_NO_PULL);
    HalGpio_InitPinGpio_Def(HAL_GPIO_PT_E, 8,  PORT_OUTPUT,OUTPUT_OPEN_DRAIN,INPUT_NO_PULL);
    HalGpio_InitPinGpio_Def(HAL_GPIO_PT_B, 5,  PORT_OUTPUT,OUTPUT_OPEN_DRAIN,INPUT_NO_PULL);
    HalGpio_InitPinGpio_Def(HAL_GPIO_PT_B, 23, PORT_OUTPUT,OUTPUT_OPEN_DRAIN,INPUT_NO_PULL);

    //pv feature, disable eq1 and fuel pull-up.
    BspGpio_SetVal(P_BAT_EN_ID, LOW);

    //disable com gpio.
    BspGpio_SetVal(ApEnterRamdisk_ID, LOW);
    BspGpio_SetVal(ApEnterImageB_ID, LOW);

    //spi
    HalGpio_InitPinGpio_Def(HAL_GPIO_PT_B, 14, PORT_OUTPUT,OUTPUT_OPEN_DRAIN,INPUT_NO_PULL);
    HalGpio_InitPinGpio_Def(HAL_GPIO_PT_B, 16, PORT_INPUT,OUTPUT_OPEN_DRAIN,INPUT_NO_PULL);
    HalGpio_InitPinGpio_Def(HAL_GPIO_PT_B, 15, PORT_OUTPUT,OUTPUT_OPEN_DRAIN,INPUT_NO_PULL);
    HalGpio_InitPinGpio_Def(HAL_GPIO_PT_B, 17, PORT_OUTPUT,OUTPUT_OPEN_DRAIN,INPUT_NO_PULL);

    //i2c
    HalGpio_InitPinGpio_Def(HAL_GPIO_PT_A, 2, PORT_OUTPUT, OUTPUT_OPEN_DRAIN, INPUT_NO_PULL); //
    HalGpio_SetPinVal(HAL_GPIO_PT_A, 2, LOW);
    HalGpio_InitPinGpio_Def(HAL_GPIO_PT_A, 3, PORT_OUTPUT, OUTPUT_OPEN_DRAIN, INPUT_NO_PULL); //
    HalGpio_SetPinVal(HAL_GPIO_PT_A, 3, LOW);

    HalGpio_InitPinGpio_Def(HAL_GPIO_PT_E, 1, PORT_OUTPUT, OUTPUT_OPEN_DRAIN, INPUT_NO_PULL); //
    HalGpio_SetPinVal(HAL_GPIO_PT_E, 1, LOW);
    HalGpio_InitPinGpio_Def(HAL_GPIO_PT_E, 0u,PORT_OUTPUT, OUTPUT_OPEN_DRAIN, INPUT_NO_PULL); //
    HalGpio_SetPinVal(HAL_GPIO_PT_E, 0u, LOW);

    //PWM
    HalGpio_InitPinGpio_Def(HAL_GPIO_PT_D,16,  PORT_OUTPUT,OUTPUT_OPEN_DRAIN,INPUT_NO_PULL);

    //uart
    HalGpio_InitPinGpio_Def(HAL_GPIO_PT_A,28, PORT_OUTPUT,OUTPUT_OPEN_DRAIN,INPUT_NO_PULL);
    HalGpio_InitPinGpio_Def(HAL_GPIO_PT_A,27, PORT_OUTPUT,OUTPUT_OPEN_DRAIN,INPUT_NO_PULL);

    HalGpio_InitPinGpio_Def(HAL_GPIO_PT_C,6, PORT_OUTPUT,OUTPUT_OPEN_DRAIN,INPUT_NO_PULL);
    HalGpio_InitPinGpio_Def(HAL_GPIO_PT_C,7, PORT_OUTPUT,OUTPUT_OPEN_DRAIN,INPUT_NO_PULL);

    HalGpio_InitPinGpio_Def(HAL_GPIO_PT_A,8, PORT_AF,OUTPUT_OPEN_DRAIN,INPUT_NO_PULL);
    HalGpio_InitPinGpio_Def(HAL_GPIO_PT_A,9, PORT_AF,OUTPUT_OPEN_DRAIN,INPUT_NO_PULL);
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void WakeupSrcJudge(void)
{
    if (PORTD->ISFR & 0x4) //ACC, PTD2
    {
        wakeup_source = WAKE_UP_ACC_ON;
        wakeup_cnt[WAKE_UP_ACC_ON]++;
        TRACE_VALUE(LOG_SHOUT,MOD_HW,"[Map Mana] System wakeup from sleep, wake source is ACC, counter=%d ...", wakeup_cnt[WAKE_UP_ACC_ON]);
    }
    else if (PORTD->ISFR & 0x8000000) //IGN, PTD27
    {
        wakeup_source = WAKE_UP_IGN_ON;
        wakeup_cnt[WAKE_UP_IGN_ON]++;
        TRACE_VALUE(LOG_SHOUT,MOD_HW,"[Map Mana] System wakeup from sleep, wake source is IGN, counter=%d ...", wakeup_cnt[WAKE_UP_IGN_ON]);
    }
    else if (PORTE->ISFR & 0x8) //CAN INH, PTE3
    {
        wakeup_source = WAKE_UP_CAN_INH_ON;
        wakeup_cnt[WAKE_UP_CAN_INH_ON]++;
        TRACE_VALUE(LOG_SHOUT,MOD_HW,"[Map Mana] System wakeup from sleep, wake source is CAN INH, counter=%d ...", wakeup_cnt[WAKE_UP_CAN_INH_ON]);
    }
    else if (PORTC->ISFR & 0x200) //TCU_ACC, PTC9
    {
        wakeup_source = WAKE_UP_TCU_ACC_ON;
        wakeup_cnt[WAKE_UP_TCU_ACC_ON]++;
        TRACE_VALUE(LOG_SHOUT,MOD_HW,"[Map Mana] System wakeup from sleep, wake source is TCU_ACC, counter=%d ...", wakeup_cnt[WAKE_UP_TCU_ACC_ON]);
    }
    else
    {
        wakeup_source = WAKE_UP_UNKNOWN;
        wakeup_cnt[WAKE_UP_UNKNOWN]++;
        TRACE_VALUE(LOG_SHOUT,MOD_HW,"[Map Mana] System wakeup from sleep, wake source is unknown, counter=%d ...", wakeup_cnt[WAKE_UP_UNKNOWN]);
        TRACE_VALUE(LOG_SHOUT,MOD_HW,"[Map Mana] PORTA->ISFR=0x%x ...", PORTA->ISFR);
        TRACE_VALUE(LOG_SHOUT,MOD_HW,"[Map Mana] PORTB->ISFR=0x%x ...", PORTB->ISFR);
        TRACE_VALUE(LOG_SHOUT,MOD_HW,"[Map Mana] PORTC->ISFR=0x%x ...", PORTC->ISFR);
        TRACE_VALUE(LOG_SHOUT,MOD_HW,"[Map Mana] PORTD->ISFR=0x%x ...", PORTD->ISFR);
        TRACE_VALUE(LOG_SHOUT,MOD_HW,"[Map Mana] PORTE->ISFR=0x%x ...", PORTE->ISFR);
    }
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void MapMana_EnterSleepState(void)
{
    uint8_t eq_nvm_state = GenEep_Get_Config_EQ1();

    Tdf8546_AmpDis();

    DevIpc_StopReq(DEV_IPC_SPI_0);   // stop spi devipc
    DevIpc_StopReq(DEV_IPC_IIC_IC);  // stop iic devipc
    ap_state = AP_POWER_OFF_SEQ;

    DisAllUnusedInt();

    //sleep CAN transceiver.
    //Bsp_CAN1StbDis();
    for (uint16_t i=0u; i<300;i++)
    {
        __asm("nop"); __asm("nop"); __asm("nop"); __asm("nop");
        __asm("nop"); __asm("nop"); __asm("nop"); __asm("nop");
        __asm("nop"); __asm("nop"); __asm("nop"); __asm("nop");
        __asm("nop"); __asm("nop"); __asm("nop"); __asm("nop");
    } //about 100us
    //Bsp_CAN1Dis(); //stb=L,en=H, hold time>th(min), and wake flag is cleared by UVnom

    //disable all periphral device.
    MapPowerDisAllPeriDev();
    for (uint32_t i=0u; i<280000;i++) //max wait about 100ms to shut down PMIC power fully.
    {
        __asm("nop"); __asm("nop"); __asm("nop"); __asm("nop");
        __asm("nop"); __asm("nop"); __asm("nop"); __asm("nop");
        __asm("nop"); __asm("nop"); __asm("nop"); __asm("nop");
        __asm("nop"); __asm("nop"); __asm("nop"); __asm("nop");
        __asm("nop"); __asm("nop"); __asm("nop"); __asm("nop");
        __asm("nop"); __asm("nop"); __asm("nop"); __asm("nop");
        __asm("nop"); __asm("nop"); __asm("nop"); __asm("nop");
        __asm("nop"); __asm("nop"); __asm("nop"); __asm("nop");
    }

    ReconfigAllGPIO();

    //adc
    ADC0->SC1[0] = ADC_SC1_ADCH_MASK;  //disable current channel
    ADC1->SC1[0] = ADC_SC1_ADCH_MASK;  //disable current channel

    SCG->FIRCCSR  = SCG_FIRCCSR_FIRCREGOFF_MASK;                              //disable FIRC and FIRC Regulator
    PMC->REGSC    = PMC_REGSC_CLKBIASDIS_MASK                                 //bias currents and ref voltage for SIRC FIRC and PLL disabled.
                  | PMC_REGSC_BIASEN_MASK;                                    //enable bias

    //enable wakeup source.
    BspGpio_SetGpioInt(CAN1_INH_SEN_ID, GPIO_RISING_EDGE); //Remote wakeup, rising edge.
    BspGpio_EnGpioInt(CAN1_INH_SEN_ID);

    BspGpio_SetGpioInt(DI_ACCSEN_ID, GPIO_RISING_EDGE); //ACC wakeup, rising edge.
    BspGpio_EnGpioInt(DI_ACCSEN_ID);

    BspGpio_SetGpioInt(TCU_EN_ID, GPIO_RISING_EDGE); //TCU wakeup, rising edge.
    BspGpio_EnGpioInt(TCU_EN_ID);

    if (eq_nvm_state) //    high line use this.
    {
        BspGpio_SetGpioInt(DI_IGNSEN_ID, GPIO_RISING_EDGE); //IGN wakeup, rising edge.
        BspGpio_EnGpioInt(DI_IGNSEN_ID);
    }

    /*if (Bsp_GetCAN1INHHw() || Bsp_GetACCEnHw() || (eq_nvm_state && Bsp_GetIGNEnHw()) || Bsp_GetTCUEnHw())
    {
        TRACE_VALUE4(LOG_SHOUT,MOD_HW,"[Map Mana] System reset before sleep, inh=%d, ign=%d, acc=%d, tcu_acc=%d...",Bsp_GetCAN1INHHw(), Bsp_GetIGNEnHw(), Bsp_GetACCEnHw(), Bsp_GetTCUEnHw());
        BspMisc_SetSwResetReason(SW_RESET_BEFORE_SLEEP);
        HalWdt_RstCpu();
    }*/

    SCB->SCR        |= SCB_SCR_SLEEPDEEP_Msk;
    __WFI();

    WakeupSrcJudge();

    TRACE_TEXT(LOG_SHOUT,MOD_HW,"[Map Mana] System ready to restart...");
    BspMisc_SetSwResetReason(SW_RESET_SLEEP_WAKEUP);

    HalWdt_RstCpu();
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
uint8_t MapMana_GetWakeupSource(void)
{
    return (wakeup_source);
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
uint32_t MapMana_GetWakeupSourceCnt(uint8_t source)
{
    uint32_t cnt=0u;
    if (source < WAKE_UP_MAX)
    {
        cnt = wakeup_cnt[source];
    }
    return (cnt);
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void MapMana_GetSleepWakeUpSrc(SleepWakeUpSrc_T *info)
{
    if (NULL != info)
    {
        info->wu_src = wakeup_source;
        info->wu_cnt = MapMana_GetWakeupSourceCnt(wakeup_source);
    }
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if 0
static void MapPowerOffProcess(void)
{
    if(map_poweroff_delay_timer > 0)
    {
        map_poweroff_delay_timer--;

        if (1 == map_poweroff_delay_timer)
        {
            //Bsp_CAN1StbDis();   //CAN transceiver sleep
            TRACE_TEXT(LOG_SHOUT,MOD_HW,"[Map Mana] force transceiver sleep due to CAN NM stop timeout...");
        }
        else if (0u == map_poweroff_delay_timer)
        {
            //Bsp_CAN1Dis();
            MapPowerDisAllPeriDev();
        }
        else {}

        if ((CANNM_STATUS_STOP == CanNm_GetStatus()) && (map_poweroff_delay_timer > 1))
        {
            //Bsp_CAN1StbDis();   //CAN transceiver sleep
            TRACE_VALUE(LOG_SHOUT,MOD_HW,"[Map Mana] CAN NM already stop, used time=%d ms, transceiver sleep...", MAP_POWEROFF_DL_TM-map_poweroff_delay_timer);
            map_poweroff_delay_timer = 1;
        }
    }
}
#endif

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

#else
#include "map_mana.h"
// #include "dev_ipc.h"
#include "std_type.h"
#include "hal_os.h"
#include "hal_wdt.h"

void	MapMana_Init	   	(void)
{
}

void    MapMana_1msFunc	(void)
{
}

void MapMana_RecordResetInfo(SOC_RESET_REASON soc_reason)
{
#if 0
	uint8 reason[32];
	GenEep_Get_SocResetReason(reason);
	for(uint8 i = 31; i > 0; i --)
	{
		reason[i] = reason[i-1];
	}
	reason[0] = soc_reason;
	uint8 using_backup;
	GenEep_Get_VipSwdlShareMem_Ex(&using_backup,USING_BACKUPIMAGE, 1);
	if ( using_backup )
	{
		reason[0] |= 0x80;
	}
	GenEep_Set_SocResetReason(reason);
#endif
}

void	MapMana_ForceReset(void)
{
    // DevIpc_StopReq(DEV_IPC_SPI_0);   // stop spi devipc
    HalOS_Delay(500);               //delay about 500ms to power off device power fully.
    HalWdt_RstCpu();
}

#endif
