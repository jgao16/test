/**********************************************************************************/
/* backlight.c                                      		                          */

/**********************************************************************************/
#include "std_type.h"
#include "trace_api.h"

#include "serv.h"
#include "hal_ftm.h"
#include "bat_mana.h"
#include "tp_iic.h"
#include "panel.h"
#include "bsp_gpio.h"
#include "hal_gpio.h"
#include "gpio.cfg"
#include "gen_eep_mana.h"
#include "host_iic.h"
#include "hal_iic_slv.h"
#include "crc8.h"
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

#define BACKLIGHT_I2C_REG_BACKLIGHT 0
#define BACKLIGHT_I2C_REG_BACKLIGHT_CRC 1
#define BACKLIGHT_I2C_REG_STATUS 2

typedef struct
{
    uint8 backlightValue;
    uint8 backlightValueCRC;
    uint8 backlightStatus;
} backlight_msgType;

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static backlight_msgType s_backlight_msg;
static uint8_t current_backlight_pwm;
static uint8_t allowedPwm;
static uint8_t userDefinedPwm;
static uint8_t userDefinedBlStatus;

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void Proc_BackLight_Func(void);
static void BackLight_HandlePwm(void);
static void BackLight_HandleBlStatus(void);

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void  BackLight_Init(void)
{
    allowedPwm = 100;
    userDefinedPwm = GenEep_Get_ScreenBackLightValOnDay();
    userDefinedPwm = 100;
	current_backlight_pwm = userDefinedPwm;
    userDefinedBlStatus = 1;

    s_backlight_msg.backlightValue = current_backlight_pwm;
    s_backlight_msg.backlightValueCRC = crc8_Calc(&current_backlight_pwm, 1);
    s_backlight_msg.backlightStatus = 1;

	HalFtm_ChInit(SCREEN_BL_ID);
	HalFtm_ChSetPwmVal(SCREEN_BL_ID, current_backlight_pwm);
    ServFunc_StartCyclic(ServFunc_Create(Proc_BackLight_Func, "Proc BackLight Task"), 1, 50/2);
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
/* User defined PWM duty. */
void BackLight_SetUserPwm(uint8_t pwm)
{
    if (pwm > 100)
    {
        pwm = 100;
    }

    if (pwm != userDefinedPwm)
    {
        userDefinedPwm = pwm;

        if (pwm > allowedPwm)
        {
            // TODO: Error, save to flash.
            current_backlight_pwm = allowedPwm;
        }
        else
        {
            current_backlight_pwm = pwm;
        }
    }
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void BackLight_SetAllowedPwm(uint8_t pwm)
{
    if (pwm > 100)
    {
        pwm = 100;
    }

    if (pwm != allowedPwm)
    {
        if (pwm < userDefinedPwm)
        {
            current_backlight_pwm = pwm;
        }
        else
        {
            current_backlight_pwm = userDefinedPwm;
        }

        allowedPwm = pwm;
    }
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

static void Proc_BackLight_Func(void)
{
    BackLight_HandlePwm();
    BackLight_HandleBlStatus();
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void BackLight_HandlePwm(void)
{
    uint32_t cpu_sr;

    if(current_backlight_pwm != s_backlight_msg.backlightValue)
    {
        cpu_sr = osEnterCritical();

        s_backlight_msg.backlightValue = current_backlight_pwm;
        s_backlight_msg.backlightValueCRC = crc8_Calc(&current_backlight_pwm, 1);

        osExitCritical(cpu_sr);

    	HalFtm_ChSetPwmVal(SCREEN_BL_ID, current_backlight_pwm);

        GenEep_Set_ScreenBackLightValOnDay(s_backlight_msg.backlightValue);
    }
}

static void BackLight_HandleBlStatus(void)
{
    uint32_t cpu_sr;
    boolean actualBlStatus;
    lcd_power_state_t lcd_power_state;

    cpu_sr = osEnterCritical();

    actualBlStatus = Bsp_TFTPowerBLGet();

    if (actualBlStatus != userDefinedBlStatus)
    {
        lcd_power_state = LCD_GetPowerState();

        if (userDefinedBlStatus && (LCD_POWER_ON == lcd_power_state)) // Should turn on BL
        {
            if (FALSE == actualBlStatus)
            {
                Bsp_TFTPowerBLEn();
            }
        }
        else // Should turn off BL
        {
            if (TRUE == actualBlStatus)
            {
                Bsp_TFTPowerBLDis();
            }
        }
    }

    s_backlight_msg.backlightStatus = actualBlStatus;

    osExitCritical(cpu_sr);
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
uint8_t BackLight_GetPwm(void)
{
    return s_backlight_msg.backlightValue;
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void BackLight_SetStatus(boolean enable)
{
    userDefinedBlStatus = enable;
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
boolean BackLight_GetStatus(void)
{
    return Bsp_TFTPowerBLGet();
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void BACKLIGHT_I2C_MsgHandler(uint16_t addr, uint16_t len, const uint8_t* data)
{
    if (len > 0) /* Write */
    {
        switch(addr)
        {
            case BACKLIGHT_I2C_REG_BACKLIGHT:
                if ((2 == len) || (3 == len))
                {
                    if (data[1] == crc8_Calc(&data[0], 1))
                    {
                        //BackLight_SetUserPwm(data[0]);
                    }

                    if (3 == len)
                    {
                        BackLight_SetStatus(data[2]);
                    }
                }

                break;

            case BACKLIGHT_I2C_REG_STATUS:
                if (1 == len)
                {
                    BackLight_SetStatus(data[0]);
                }
                break;

            default:
                break;
        }
    }
    else /* Read */
    {
        /* Set the data address for read. */
        HalIic_SLV_SetTxData(host_i2c, ((uint8_t*)(&s_backlight_msg)) + addr, sizeof(backlight_msgType) - addr);
    }
}

/** >>>>>>>> end of file  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
