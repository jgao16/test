/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    Service config:
    All rights reserved
>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

#ifndef BACKLIGHT_H_H
#define BACKLIGHT_H_H


#ifdef  __cplusplus
extern "C"
{
#endif

/*****************************************************************************
 *  Include Files
 *****************************************************************************/

/*****************************************************************************
 *  Global Macro Definitions
 *****************************************************************************/



/*****************************************************************************
 *  Global Type Declarations
 *****************************************************************************/
/* %<Types> */



/*****************************************************************************
 *  Global Data Declarations
 *****************************************************************************/
/* %<CalStructHeader> 	*/
/* %<CalStructContents>	*/
/* %<CalStructTrailer>	*/


/*****************************************************************************
 *  Global Function Declarations
 *****************************************************************************/

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
extern void BackLight_Init(void);

uint8_t BackLight_GetPwm(void);

/* Set allowed PWM based on temperature. */
void BackLight_SetAllowedPwm(uint8_t pwm);

/* Set user defined backlight. */
void BackLight_SetUserPwm(uint8_t pwm);

/* Set backlight on or off. */
void BackLight_SetStatus(boolean enable);

/* Get backlight actual on/off status. */
boolean BackLight_GetStatus(void);

extern void BACKLIGHT_I2C_MsgHandler(uint16_t addr, uint16_t len, const uint8_t* data);

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#ifdef  __cplusplus
}
#endif

#endif
