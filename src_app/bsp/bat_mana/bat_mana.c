/**********************************************************************************/
/* bat_mana.c                                      		                          */

/**********************************************************************************/
#include "std_type.h"
#include "cpu.h"
#include "serv.h"
#include "hal_os.h"

/** >>>>>>>> user include start  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#include "hal_adc.h"
#include "bat_mana.h"
#include "trace_api.h"

/** >>>>>>>> user include end    >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#define BAT_REAL_VOL_VAL(x)      ((uint16_t)(4.7824 * (x)))
#define BAT_STATE_CHG_DLY_TM     300 /* 3 second. */

#define BAT_LOW_HYST_HIGH    8000
#define BAT_NORMAL_HYST_LOW  7500
#define BAT_NORMAL_HYST_HIGH 19000
#define BAT_HIGH_HYST_LOW    18000

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static uint16_t  bat_vol;                           //Battery real voltage, mV
static uint8_t   bat_state;                         //Battery state, normal, low, ...
static uint16_t  bat_filter_dly[BAT_STATE_MAX];     //Battery delay time in each bat state.

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void BatMana_10ms(void);
static void BatMana_SetLeftBatStateDlyToDefault(uint8_t state);

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
//Battery management init
void   BatMana_Init(void)
{
    bat_state = BAT_NORMAL;                     //assume to be normal

    BatMana_SetLeftBatStateDlyToDefault((uint8_t)BAT_STATE_MAX);

    HalAdc_ChInit(POW_MONI_ID);

    ServFunc_StartCyclic(ServFunc_Create(BatMana_10ms, "Battery management Task"), 10, 10);
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
//set other bat state filter delay to default except current state
static void BatMana_SetLeftBatStateDlyToDefault(uint8_t state)
{
    uint16_t dly = BAT_STATE_CHG_DLY_TM;

    for (uint8_t i = 0u; i < (uint8_t)BAT_STATE_MAX; i++)
    {
        if (i != state)
        {
            bat_filter_dly[i] = dly;
        }
    }
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void BatMana_CalCurBatVol(void)
{
    uint16_t  bat_adc_val;

    HalAdc_GetChVal(POW_MONI_ID, &bat_adc_val);

    bat_vol = BAT_REAL_VOL_VAL(bat_adc_val);
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
//judge current bat state via adc raw data.
static uint8_t BatMana_GetCurBatState(void)
{
    BAT_STATE state = BAT_NORMAL;

    switch(bat_state)
    {
        case BAT_NORMAL:
            if (bat_vol < BAT_NORMAL_HYST_LOW)
            {
                state = BAT_LOW;
            }
            else if (bat_vol > BAT_NORMAL_HYST_HIGH)
            {
                state = BAT_HIGH;
            }
            else
            {
                state = BAT_NORMAL;
            }
            break;

        case BAT_LOW:
            if (bat_vol > BAT_LOW_HYST_HIGH)
            {
                state = BAT_NORMAL;
            }
            else
            {
                state = BAT_LOW;
            }
            break;

        case BAT_HIGH:
            if (bat_vol < BAT_HIGH_HYST_LOW)
            {
                state = BAT_NORMAL;
            }
            else
            {
                state = BAT_HIGH;
            }
            break;

        default:
            break;
    }

    return state;
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
//bat mana main function.
static void BatMana_10ms(void)
{
    BatMana_CalCurBatVol();

    for ( uint8_t i = 0u; i < (uint8_t)BAT_STATE_MAX; i++ )
    {
        if (bat_filter_dly[i] > 0u)
        {
            bat_filter_dly[i]--;
        }
    }

    uint8_t cur_state = BatMana_GetCurBatState();
    if (bat_state != cur_state)
    {
        if (0u == bat_filter_dly[cur_state])
        {
            TRACE_VALUE2(LOG_DEBUG, MOD_HW, "[Battery management], Battery from state%d change to state%d!", bat_state, cur_state);

            bat_state = cur_state;
        }
    }

    BatMana_SetLeftBatStateDlyToDefault(cur_state);
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
uint8_t  BatMana_GetBatState(void)
{
	return ((uint8_t)bat_state);
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
uint16_t BatMana_GetBatVol(void)
{
	return (bat_vol);
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void     BatMana_TraceRawData(void)
{
    TRACE_VALUE(LOG_RAW, MOD_HW, "[Battery management], Battery voltage=%d mV", bat_vol);
}
/** >>>>>>>> end of file  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
