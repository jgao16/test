/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    Service config:
    All rights reserved
>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

#ifndef DVR_H_H_
#define DVR_H_H_


#ifdef  __cplusplus
extern "C"
{
#endif




/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void    Dvr_Init(void);
uint8_t Dvr_GetWorkStatus(void);
uint8_t Dvr_GetUpdateStatus(void);
void    Dvr_TraceRawData(void);

#ifdef  __cplusplus
}
#endif


#endif
