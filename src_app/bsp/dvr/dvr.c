/**********************************************************************************/
/* dvr.c                                                                          */
/**********************************************************************************/
#include "std_type.h"
#include "cpu.h"
#include "serv.h"
#include "hal_os.h"

/** >>>>>>>> user include start  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#include "bsp_gpio.h"
// #include "gen_dev_sig.h"
#include "Com.h"

#include "trace_api.h"
/** >>>>>>>> user include end    >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#define DVR_TASK_BASE_TM				(50)
#define DVR_ACC_ON_DLY_TM				(20000/DVR_TASK_BASE_TM)
#define DVR_UPDATE_EN_DLY_TM			(180000/DVR_TASK_BASE_TM)

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static uint8_t  dvr_work_status;    //dvr status after ACC on
static uint16_t dvr_acc_on_tm_cnt;  //ACC ON time delay

static uint8_t  dvr_update_status;  //dvr update status, 0:normal operation;1:in update;2:update failed
static uint16_t dvr_update_tm_cnt;  //update valid time delay

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void Dvr_MainFunc(void);

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void Dvr_Init(void)
{
	dvr_work_status   = DEV_SIG_DvrWorkStatus_INITIAL;
	dvr_acc_on_tm_cnt = DVR_ACC_ON_DLY_TM;
	dvr_update_status = DEV_SIG_DvrUpdateStatus_NORMAL;
	dvr_update_tm_cnt = DVR_UPDATE_EN_DLY_TM;

    //ServFunc_StartCyclic(ServFunc_Create(Dvr_MainFunc, "DVR detection Task"),0u,DVR_TASK_BASE_TM/10);
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
uint8_t Dvr_GetWorkStatus(void)
{
	return dvr_work_status;
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
uint8_t Dvr_GetUpdateStatus(void)
{
	return dvr_update_status;
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void Dvr_MainFunc(void)
{
	/*if (Bsp_GetACCEn())
	{
		if (dvr_acc_on_tm_cnt > 0u)
		{
			dvr_acc_on_tm_cnt --;
		}

		uint8_t work_status = Com_GetSigVal_DVR_STS__DVRWorkStatus();
		if (work_status > 0u)
		{
			dvr_work_status   = work_status;
			dvr_acc_on_tm_cnt = DVR_ACC_ON_DLY_TM;
		}

		if (0u == dvr_acc_on_tm_cnt)
		{
			dvr_work_status = DEV_SIG_DvrWorkStatus_ERROR;
		}

		if (DEV_SIG_DvrWorkStatus_ERROR == dvr_work_status)
		{
			//set DTC
		}
		
		
		uint8_t update_status = Com_GetSigVal_DVR_STS__UpdateStatus();
		if (1 != update_status)
		{
			if (3 == update_status)
			{
				dvr_update_status = DEV_SIG_DvrUpdateStatus_UPDATE_FAILED;
			}
			else
			{
				dvr_update_status = DEV_SIG_DvrUpdateStatus_NORMAL;
			}
			
			dvr_update_tm_cnt = DVR_UPDATE_EN_DLY_TM;
		}
		else
		{
			if (dvr_update_tm_cnt > 0u)
			{
				dvr_update_tm_cnt --;
			}
			if (0u == dvr_update_tm_cnt)
			{
				dvr_update_status = DEV_SIG_DvrUpdateStatus_UPDATE_FAILED;
			}
			else
			{
				dvr_update_status = DEV_SIG_DvrUpdateStatus_IN_UPDATE;
			}
		}
	}
	else
	{
		dvr_work_status   = DEV_SIG_DvrWorkStatus_INITIAL;
		dvr_acc_on_tm_cnt = DVR_ACC_ON_DLY_TM;
		dvr_update_status = DEV_SIG_DvrUpdateStatus_NORMAL;
		dvr_update_tm_cnt = DVR_UPDATE_EN_DLY_TM;
	}*/
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void Dvr_TraceRawData(void)
{
    TRACE_VALUE2(LOG_RAW, MOD_HW, "[DVR] dvr_work_status=%d, dvr_update_status=%d...", dvr_work_status, dvr_update_status);
}

