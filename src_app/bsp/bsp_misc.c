/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> 
    All rights reserved
>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

#include "std_lib.h"

// #include "dev_ipc.h"
// #include "gen_dev_sig.h"
#include "re_sig.h"
#include "bsp_misc.h"
#include "lk_uboot.h"

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#pragma section = ".noinit"

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

static uint8 wakeup_reason;

__no_init static uint8_t sw_reset_reason;

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
//__no_init static boolean flash_err_reset;
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

void 	BspMisc_Init(void)
{
	uint16 rcm_srs = (uint16)(RCM->SRS);
#if 0	
	if ( rcm_srs == 0x82 || rcm_srs == 0x02 )
	{	// power on 
		wakeup_reason = DEV_SIG_WakeUpReason_BATTERY_ON;
	}
	else if ( rcm_srs & 0x20 )
	{	// vip hardware watchdog
		wakeup_reason = DEV_SIG_WakeUpReason_VIP_WDT;
	}
	else if ( rcm_srs & 0x40 )
	{	// vip reset pin 
		wakeup_reason = DEV_SIG_WakeUpReason_VIP_RST_PIN;
	}
	else if ( rcm_srs & 0x400 )
	{
		if ( UbootJumpFromOther() )
		{	// jump from other module(swdl/app), used as batter on 
			wakeup_reason = DEV_SIG_WakeUpReason_BATTERY_ON;
		}
		else
		{
			wakeup_reason = DEV_SIG_WakeUpReason_VIP_SW_REQ;
		}
	}
	else
	{
		wakeup_reason = DEV_SIG_WakeUpReason_UNKONWN;
	}
#endif

	if ( rcm_srs & (RCM_SRS_LVD_MASK+RCM_SRS_WDOG_MASK+RCM_SRS_PIN_MASK+RCM_SRS_POR_MASK+RCM_SRS_JTAG_MASK) )
	{
		/* real power on or lvd*/
		ClrAllShareMem();
	}

	if ( rcm_srs & (RCM_SRS_LVD_MASK+RCM_SRS_WDOG_MASK+RCM_SRS_PIN_MASK+RCM_SRS_POR_MASK+RCM_SRS_JTAG_MASK) || UbootJumpFromOther() )
	{
		wakeup_reason = DEV_SIG_WakeUpReason_BATTERY_ON;
	}
	else if ( rcm_srs & 0x400 )
	{
		wakeup_reason = DEV_SIG_WakeUpReason_VIP_SW_REQ;
	}
	else if ( rcm_srs & 0x2000 )
    {
        wakeup_reason = DEV_SIG_WakeUpReason_SACKERR;
    }
	else if ( rcm_srs & 0x800 )
    {
        wakeup_reason = DEV_SIG_WakeUpReason_MDMAP;
    }
	else if ( rcm_srs & 0x200 )
    {
        wakeup_reason = DEV_SIG_WakeUpReason_LOCKUP;
    }
	else if ( rcm_srs & 0x8 )
    {
        wakeup_reason = DEV_SIG_WakeUpReason_LOL;
    }
	else if ( rcm_srs & 0x4 )
    {
        wakeup_reason = DEV_SIG_WakeUpReason_LOC;
    }
	else
	{
		wakeup_reason = DEV_SIG_WakeUpReason_UNKONWN;	
	}
	
	if ( wakeup_reason == DEV_SIG_WakeUpReason_BATTERY_ON )
	{
		uint8 *_no_init_start = __section_begin(".noinit");	//lint !e64
		uint8 *_no_init_end   = __section_end(".noinit");	//lint !e64
		while( _no_init_start < _no_init_end)
		{
			*_no_init_start++ = 0;
		}
	}
	ClrUbootJumpFlag();
}

uint8	BspMisc_GetWakeUpReason(void)
{
	return wakeup_reason;
}
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void	BspMisc_GetWakeUpHwReg(WakeUpHwReg_T * reg)
{
	reg->RCM_SRS = (uint16)(RCM->SRS);
	reg->wdt_id  = 0x00;
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void BspMisc_SetSwResetReason(uint8_t rsn)
{
    sw_reset_reason = rsn;
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
uint8_t BspMisc_GetSwResetReason(void)
{
    return (sw_reset_reason);
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


