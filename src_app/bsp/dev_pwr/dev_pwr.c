#if 0
/**********************************************************************************/
/* dev_pwr.c                                                                          */
/**********************************************************************************/
#include "std_type.h"
#include "serv.h"

/** >>>>>>>> user include start  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#include "gen_eep_mana.h"
#include "bsp_gpio.h"
#include "bat_mana.h"
#include "ComRTE.h"
#include "Com.h"
#include "trace_api.h"

//#include "trace_api.h"
/** >>>>>>>> user include end    >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#define DEV_PWR_ONOFF_BASE_TM    (10u)
#define CAM_PWR_OFF_DLY          (300000u / DEV_PWR_ONOFF_BASE_TM)

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static boolean          cam_pwr_onoff_en;
static uint16_t         cam_pwr_off_dly;

static boolean          mic_pwr_onoff_en;

static boolean          tuner_pwr_onoff_en;
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void DevPower_MainFunc(void);
static void CamPwr_Handle(void);
static void MicPwr_Handle(void);
static void TunerAntPwr_Handle(void);

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void DevPower_Init(void)
{
    cam_pwr_onoff_en            = FALSE;
    cam_pwr_off_dly             = CAM_PWR_OFF_DLY;

    mic_pwr_onoff_en            = FALSE;

    tuner_pwr_onoff_en          = FALSE;

    //ServFunc_StartCyclic(ServFunc_Create(DevPower_MainFunc, "Device Power Onoff Task"),12u,DEV_PWR_ONOFF_BASE_TM/10);
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
uint8_t DevPower_GetCamPwrState(void)
{
    return cam_pwr_onoff_en;
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
uint8_t DevPower_GetMicPwrState(void)
{
    return mic_pwr_onoff_en;
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void DevPower_MainFunc(void)
{
    CamPwr_Handle();
    MicPwr_Handle();
    TunerAntPwr_Handle();
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void CamPwr_Handle(void)
{
    if (2 != GenEep_Get_Vehicle_Type()) //disabled in AVM, SRD_000_PD-2429, 20180124
    {
        uint8_t bat_state  = BatMana_GetBatState();
        uint8_t gear_state = ComRTE_GetGearReverse();

        if (!cam_pwr_onoff_en)
        {
            /*if ((COMRTE_GEAR_REVERSE == gear_state) && Bsp_MainPowerGood() && (BAT_NORMAL == bat_state))
            {
                //Bsp_CameraPowerEn();
                cam_pwr_onoff_en = TRUE;
                cam_pwr_off_dly  = CAM_PWR_OFF_DLY;
                TRACE_TEXT(LOG_SHOUT, MOD_HW, "[DevPwr] Cam power on...");
            }*/
        }
        else
        {
            boolean off_en = FALSE;
            /*if (!Bsp_MainPowerGood() || (BAT_NORMAL != bat_state))
            {
                off_en = TRUE;
            }
            else if (COMRTE_GEAR_NO_REVERSE == gear_state)
            {
                if (cam_pwr_off_dly > 0u)
                {
                    cam_pwr_off_dly --;
                }
                if (0u == cam_pwr_off_dly)
                {
                    off_en = TRUE;
                }
            }
            else
            {
                cam_pwr_off_dly  = CAM_PWR_OFF_DLY;
            }

            if (off_en)
            {
                //Bsp_CameraPowerDis();
                cam_pwr_onoff_en = FALSE;
                TRACE_TEXT(LOG_SHOUT, MOD_HW, "[DevPwr] Cam power off...");
            }*/
        }
    }
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void MicPwr_Handle(void)
{
    boolean run_flg   = Com_GetSigVal_SMART_bcmMSG_PUSH__Remote_engine_run_flag(); //0:normal, 1:remote engine start
    uint8_t bat_state = BatMana_GetBatState();
    uint8_t ign_state = ComRTE_GetIgnState();

    /*if (!run_flg && (BAT_NORMAL == bat_state) && Bsp_MainPowerGood()
    && ((COMRTE_IGNSTATUS_ACC == ign_state) || ((COMRTE_IGNSTATUS_RUN == ign_state))))
    {
        if (!mic_pwr_onoff_en)
        {
            //Bsp_MicPwrEn();
            mic_pwr_onoff_en = TRUE;
            TRACE_TEXT(LOG_SHOUT, MOD_HW, "[DevPwr] Mic power on...");
        }
    }
    else
    {
        if (mic_pwr_onoff_en)
        {
            //Bsp_MicPwrDis();
            mic_pwr_onoff_en = FALSE;
            TRACE_TEXT(LOG_SHOUT, MOD_HW, "[DevPwr] Mic power off...");
        }
    }*/
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void TunerAntPwr_Handle(void)
{
    uint8_t ign_state = ComRTE_GetIgnState();
    uint8_t bat_state = BatMana_GetBatState();

    /*if (((COMRTE_IGNSTATUS_ACC == ign_state) || (COMRTE_IGNSTATUS_RUN == ign_state))
      && (BAT_NORMAL == bat_state) && Bsp_MainPowerGood()) //acc+bat normal+PMIC ok
    {
        if (!tuner_pwr_onoff_en)
        {
            //Bsp_TunerAntPwrEn();
            tuner_pwr_onoff_en = TRUE;
            TRACE_TEXT(LOG_SHOUT, MOD_HW, "[DevPwr] Tuner Antenna power on...");
        }
    }
    else
    {
        if (tuner_pwr_onoff_en)
        {
            //Bsp_TunerAntPwrDis();
            tuner_pwr_onoff_en = FALSE;
            TRACE_TEXT(LOG_SHOUT, MOD_HW, "[DevPwr] Tuner Antenna power off...");
        }
    }*/
}

#else

void DevPower_Init(void)
{
}
#endif
