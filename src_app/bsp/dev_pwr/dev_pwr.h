/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    Service config:
    All rights reserved
>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

#ifndef DEV_PWR_H_H_
#define DEV_PWR_H_H_


#ifdef  __cplusplus
extern "C"
{
#endif




/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void    DevPower_Init(void);
uint8_t DevPower_GetCamPwrState(void);
uint8_t DevPower_GetMicPwrState(void);

#ifdef  __cplusplus
}
#endif


#endif
