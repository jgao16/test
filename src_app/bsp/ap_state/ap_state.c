/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> 
    All rights reserved
>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

#include "std_type.h"

#include "cpu.h"
#include "bsp_gpio.h"
#include "hal_os.h"

#include "ap_state.h"
#include "trace_api.h"
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#define AP_STATE_CHG_MAX_TIMES	(64u)

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
typedef enum
{
    STATE_UBOOT_AVBL = 0u,
    STATE_KERNEL_AVBL,
    STATE_3,
    STATE_4,
    STATE_5,

    STATE_NUM_MAX
}STATE_NUM;

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
//static uint16_t edge_tm[STATE_NUM_MAX][EDGE_TIMES]  = {0u}; //storage imx6 state change time.                                                                                                                            
//static uint8_t  edge_cnt[STATE_NUM_MAX]             = {0u}; //edge changed counter

static uint8_t ap_state_edge_lvl;
static uint8_t ap_state_chg_cnt[STATE_NUM_MAX];

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void ApState_Init(void)
{
	BspGpio_SetGpioInt(UBOOT_AVBL_ID,GPIO_BOTH_EDGE);
	BspGpio_EnGpioInt(UBOOT_AVBL_ID);
	BspGpio_SetGpioInt(KernelAvbl_ID,GPIO_BOTH_EDGE);
	BspGpio_EnGpioInt(KernelAvbl_ID);
	//BspGpio_SetGpioInt(ApState3_ID,GPIO_BOTH_EDGE);
	//BspGpio_EnGpioInt(ApState3_ID);
	//BspGpio_SetGpioInt(ApState4_ID,GPIO_BOTH_EDGE);
	//BspGpio_EnGpioInt(ApState4_ID);
	//BspGpio_SetGpioInt(ApState5_ID,GPIO_BOTH_EDGE);
	//BspGpio_EnGpioInt(ApState5_ID);
}


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void ApState_UbootAvblChg(void)
{
	boolean cur_lvl = BspGpio_GetHwInsVal(UBOOT_AVBL_ID);
	ap_state_chg_cnt[STATE_UBOOT_AVBL] ++;

	TRACE_VALUE4(LOG_SHOUT, MOD_HW, "(AP State): UBOOT avbl pin: AP(GPIO4_11)->Vip(PTB18), Change From %d To %d, Change Cnt=%d",	\
						(uint8)((ap_state_edge_lvl&(1<<STATE_UBOOT_AVBL))>0), cur_lvl,ap_state_chg_cnt[STATE_UBOOT_AVBL],0);
						
	if ( cur_lvl ){	
		ap_state_edge_lvl |= (1<<STATE_UBOOT_AVBL);
	}else{
		ap_state_edge_lvl &= ~(1<<STATE_UBOOT_AVBL);
	}
	if ( ap_state_chg_cnt[STATE_UBOOT_AVBL] >= AP_STATE_CHG_MAX_TIMES )
	{
		TRACE_VALUE(LOG_SHOUT, MOD_HW, "Disable UBOOT avbl pin interrupt now, cnt occured %d times",\
					ap_state_chg_cnt[STATE_UBOOT_AVBL]);
		BspGpio_DisGpioInt(UBOOT_AVBL_ID);			
	}
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void ApState_KernelAvblChg(void)
{
	boolean cur_lvl = BspGpio_GetHwInsVal(KernelAvbl_ID);
	ap_state_chg_cnt[STATE_KERNEL_AVBL] ++;

	TRACE_VALUE4(LOG_SHOUT, MOD_HW, "(AP State): Kernel avbl pin: AP(GPIO1_3)->Vip(PTB20), Change From %d To %d, Change Cnt=%d",	\
						(uint8)((ap_state_edge_lvl&(1<<STATE_KERNEL_AVBL))>0), cur_lvl,ap_state_chg_cnt[STATE_KERNEL_AVBL],0);
						
	if ( cur_lvl ){	
		ap_state_edge_lvl |= (1<<STATE_KERNEL_AVBL);
	}else{
		ap_state_edge_lvl &= ~(1<<STATE_KERNEL_AVBL);
	}
	if ( ap_state_chg_cnt[STATE_KERNEL_AVBL] >= AP_STATE_CHG_MAX_TIMES )
	{
		TRACE_VALUE(LOG_SHOUT, MOD_HW, "Disable Kernel avbl pin interrupt now, cnt occured %d times",\
					ap_state_chg_cnt[STATE_KERNEL_AVBL]);
		BspGpio_DisGpioInt(KernelAvbl_ID);			
	} 
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void ApState_State3Chg(void)
{
/* 	boolean cur_lvl = BspGpio_GetHwInsVal(ApState3_ID);
	ap_state_chg_cnt[STATE_3] ++;

	TRACE_VALUE4(LOG_SHOUT, MOD_HW, "(AP State): STATE_3 pin: AP(GPIO1_6)->Vip(PTB21), Change From %d To %d, Change Cnt=%d",	\
						(uint8)((ap_state_edge_lvl&(1<<STATE_3))>0), cur_lvl,ap_state_chg_cnt[STATE_3],0);
						
	if ( cur_lvl ){	
		ap_state_edge_lvl |= (1<<STATE_3);
	}else{
		ap_state_edge_lvl &= ~(1<<STATE_3);
	}
	if ( ap_state_chg_cnt[STATE_3] >= AP_STATE_CHG_MAX_TIMES )
	{
		TRACE_VALUE(LOG_SHOUT, MOD_HW, "Disable STATE_3 pin interrupt now, cnt occured %d times",\
					ap_state_chg_cnt[STATE_3]);
		BspGpio_DisGpioInt(ApState3_ID);			
	}*/
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void ApState_State4Chg(void)
{
/* 	boolean cur_lvl = BspGpio_GetHwInsVal(ApState4_ID);
	ap_state_chg_cnt[STATE_4] ++;

	TRACE_VALUE4(LOG_SHOUT, MOD_HW, "(AP State): STATE_4 pin: AP(GPIO7_12)->Vip(PTE8), Change From %d To %d, Change Cnt=%d",	\
						(uint8)((ap_state_edge_lvl&(1<<STATE_4))>0), cur_lvl,ap_state_chg_cnt[STATE_4],0);
						
	if ( cur_lvl ){	
		ap_state_edge_lvl |= (1<<STATE_4);
	}else{
		ap_state_edge_lvl &= ~(1<<STATE_4);
	}
	if ( ap_state_chg_cnt[STATE_4] >= AP_STATE_CHG_MAX_TIMES )
	{
		TRACE_VALUE(LOG_SHOUT, MOD_HW, "Disable STATE_4 pin interrupt now, cnt occured %d times",\
					ap_state_chg_cnt[STATE_4]);
		BspGpio_DisGpioInt(ApState4_ID);			
	}  
	*/
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void ApState_State5Chg(void)
{
//	 	boolean cur_lvl = BspGpio_GetHwInsVal(ApState5_ID);
//		ap_state_chg_cnt[STATE_5] ++;
//	
//		TRACE_VALUE4(LOG_SHOUT, MOD_HW, "(AP State): STATE_5 pin: AP(GPIO1_2)->Vip(PTB5), Change From %d To %d, Change Cnt=%d",
//							(uint8)((ap_state_edge_lvl&(1<<STATE_5))>0), cur_lvl,ap_state_chg_cnt[STATE_5],0);
//							
//		if ( cur_lvl ){	
//			ap_state_edge_lvl |= (1<<STATE_5);
//		}else{
//			ap_state_edge_lvl &= ~(1<<STATE_5);
//		}
//		if ( ap_state_chg_cnt[STATE_5] >= AP_STATE_CHG_MAX_TIMES )
//		{
//			TRACE_VALUE(LOG_SHOUT, MOD_HW, "Disable STATE_5 pin interrupt now, cnt occured %d times",
//						ap_state_chg_cnt[STATE_5]);
//			BspGpio_DisGpioInt(ApState5_ID);			
//		}    
}


