/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    Service config:
    All rights reserved
>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

//#pragma once
#ifndef BSP_MISC__H_H
#define BSP_MISC__H_H

#ifdef  __cplusplus
extern "C"
{
#endif



/*****************************************************************************
 *  Include Files
 *****************************************************************************/
//#include "skeaz1284.h"
// #include"gen_dev_sig.h"

/*****************************************************************************
 *  Global Macro Definitions
 *****************************************************************************/

//#define WAKEUP_REASON_NOT_SET	  0u	
//#define WAKEUP_REASON_POWERON     1u
//#define WAKEUP_REASON_ACC         2u
//#define WAKEUP_REASON_CAN         3u
//#define WAKEUP_REASON_IMX_CMD     4u

/*****************************************************************************
 *  Global Type Declarations
 *****************************************************************************/
/* %<Types> */
typedef enum
{
    DEV_SIG_SWResetReason_POWER_ON,
    DEV_SIG_SWResetReason_REBOOT,
    DEV_SIG_SWResetReason_ERASE_EEP,
    DEV_SIG_SWResetReason_SWDL,
    DEV_SIG_SWResetReason_SLEEP_WAKEUP,
    DEV_SIG_SWResetReason_BEFORE_SLEEP,
    DEV_SIG_SWResetReason_PMIC_ERR,
    DEV_SIG_SWResetReason_BAT_OFF,
    DEV_SIG_SWResetReason_RECONFIG_TYPE,
    DEV_SIG_SWResetReason_TRACE_CMD,
    DEV_SIG_SWResetReason_PANEL_PWR,
    DEV_SIG_SWResetReason_SERV_WDT,
    DEV_SIG_SWResetReason_IPC_KERNEL,
    SW_RESET_POWER_ON           = DEV_SIG_SWResetReason_POWER_ON,
    SW_RESET_REBOOT             = DEV_SIG_SWResetReason_REBOOT,
    SW_RESET_ERASE_EEP          = DEV_SIG_SWResetReason_ERASE_EEP,
    SW_RESET_SWDL               = DEV_SIG_SWResetReason_SWDL,
    SW_RESET_SLEEP_WAKEUP       = DEV_SIG_SWResetReason_SLEEP_WAKEUP,
    SW_RESET_BEFORE_SLEEP       = DEV_SIG_SWResetReason_BEFORE_SLEEP,
    SW_RESET_PMIC_ERR           = DEV_SIG_SWResetReason_PMIC_ERR,
    SW_RESET_BAT_OFF            = DEV_SIG_SWResetReason_BAT_OFF,
    SW_RESET_RECONFIG_TYPE      = DEV_SIG_SWResetReason_RECONFIG_TYPE,
    SW_RESET_TRACE_CMD          = DEV_SIG_SWResetReason_TRACE_CMD,
    SW_RESET_PANEL_PWR          = DEV_SIG_SWResetReason_PANEL_PWR,
    SW_RESET_SERV_WDT           = DEV_SIG_SWResetReason_SERV_WDT,
    SW_RESET_IPC_KERNEL         = DEV_SIG_SWResetReason_IPC_KERNEL,

    SW_RESET_MAX = 13
}SW_RESET_REASON;

typedef struct
{
    uint16_t RCM_SRS; /***  ***/
    uint16_t wdt_id; /***  ***/

}WakeUpHwReg_T; /*  */
    
/*****************************************************************************
 *  Global Data Declarations
 *****************************************************************************/
/* %<CalStructHeader> 	*/
/* %<CalStructContents>	*/
/* %<CalStructTrailer>	*/


/*****************************************************************************
 *  Global Function Declarations
 *****************************************************************************/

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void 	BspMisc_Init(void);
uint8	BspMisc_GetWakeUpReason(void);
void	BspMisc_GetWakeUpHwReg(WakeUpHwReg_T * reg);
void    BspMisc_SetSwResetReason(uint8_t rsn);
uint8_t BspMisc_GetSwResetReason(void);

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
//void   Port_SystemRst(uint16 cmd);

//void   Port_SetUserWakeupReason(uint16 cmd);

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/									

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/






#ifdef  __cplusplus
}
#endif

#endif



