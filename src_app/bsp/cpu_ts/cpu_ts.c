/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> 
    All rights reserved
>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

#include "std_type.h"
#include "std_lib.h"
#include "cpu.h"
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#define CPU_TS_TIME			FTM3
#define CPU_TS_TIME_IDX 	PCC_FTM3_INDEX 
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

static uint16_t cpu_dis_int_nest;
static uint16_t cpu_dis_int_max_nest;

static uint16_t cpu_dis_int_us;
static uint16_t cpu_dis_int_max_tm;

static uint16_t cpu_dis_int_long_cnt;
 
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void CpuTs_Init(void)
{
	__IO uint32_t * pcc = PCC->PCCn + CPU_TS_TIME_IDX;

	*pcc &= ~PCC_PCCn_CGC_MASK;
	*pcc &= ~PCC_PCCn_PCS_MASK;                    	//clear PCS
	*pcc |= PCC_PCCn_PCS(1);                        //select SOSC_DIV1
	*pcc |= PCC_PCCn_CGC_MASK;                      


	CPU_TS_TIME->MODE     |= FTM_MODE_WPDIS_MASK;                          /* Write protect to registers disabled (default) */
	CPU_TS_TIME->SC       |= 0x04 ;   									/* choose ps, enable channel */
	CPU_TS_TIME->COMBINE   = 0u;
	CPU_TS_TIME->POL       = 0u;
	//CPU_TS_TIME->CNTIN	   = 0x00;		// initial value
	//CPU_TS_TIME->MOD       = 100;			// load initial value    
	//CPU_TS_TIME->CNT      = 0xAA;
	CPU_TS_TIME->SC       |= FTM_SC_CLKS(3);                               /* Start FTMn counter with SOSC_DIV1 CLOCK */
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
uint16_t CpuTs_GetCnt(void)	// count, us, From Power On
{
	return (uint16_t)(CPU_TS_TIME->CNT);
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void CpuTs_IntDisMeasStart(void)
{
	if (cpu_dis_int_nest == 0u) {
		cpu_dis_int_us = CpuTs_GetCnt(); 
	}
	cpu_dis_int_nest++;

	if ( cpu_dis_int_max_nest < cpu_dis_int_nest )
	{
		cpu_dis_int_max_nest = cpu_dis_int_nest;
	}
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void CpuTs_IntDisMeasStop(void)
{
	if ( cpu_dis_int_nest > 0 )
	{
		cpu_dis_int_nest--;
		
		if ( cpu_dis_int_nest == 0x00 )
		{
			cpu_dis_int_us = CpuTs_GetCnt()-cpu_dis_int_us;
			
			if ( cpu_dis_int_us > 50 )
			{
				if ( cpu_dis_int_long_cnt < 65500 )
				{
					cpu_dis_int_long_cnt ++;
				}
			}
			if ( cpu_dis_int_max_tm < cpu_dis_int_us )
			{
				cpu_dis_int_max_tm = cpu_dis_int_us;
			}
		}
	}
	else
	{
		// Cpu code error  
	}
}
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
uint16_t CpuTs_GetDisIntNestMax(void)
{
	return cpu_dis_int_max_nest;
}
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
uint16_t CpuTs_GetDisIntMaxTm(void)
{
	return cpu_dis_int_max_tm;
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
uint16_t CpuTs_GetDisIntLongCnt(void)
{
	return cpu_dis_int_long_cnt;
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void CpuTs_ReinitCnt(void)
{
	CPU_SR  cpu_sr;
	cpu_sr = CPU_SR_Save();

	cpu_dis_int_max_nest = 0x00;
	cpu_dis_int_us 		 = 0x00;
	cpu_dis_int_max_tm 	 = 0x00;
	cpu_dis_int_long_cnt = 0x00;

	CPU_TS_TIME->CNT       = 0x00;                         

	CPU_SR_Restore(cpu_sr);
}

