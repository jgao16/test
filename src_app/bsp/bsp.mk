

SOURCE_DIRS   +=   bsp
INCLUDE_DIRS  += -Ibsp

SOURCE_DIRS   +=   bsp/gpio
INCLUDE_DIRS  += -Ibsp/gpio

#SOURCE_DIRS   +=   bsp/audio_radio
#INCLUDE_DIRS  += -Ibsp/audio_radio
#
#SOURCE_DIRS   +=   bsp/gps
#INCLUDE_DIRS  += -Ibsp/gps

SOURCE_DIRS   +=   bsp/cpu_ts
INCLUDE_DIRS  += -Ibsp/cpu_ts

#SOURCE_DIRS   +=   bsp/ap_state
#INCLUDE_DIRS  += -Ibsp/ap_state

#SOURCE_DIRS   +=   bsp/serdes
#INCLUDE_DIRS  += -Ibsp/serdes

SOURCE_DIRS   +=   bsp/bat_mana
INCLUDE_DIRS  += -Ibsp/bat_mana

#SOURCE_DIRS   +=   bsp/swc
#INCLUDE_DIRS  += -Ibsp/swc

SOURCE_DIRS   +=   bsp/panel
INCLUDE_DIRS  += -Ibsp/panel

SOURCE_DIRS   +=   bsp/temperate
INCLUDE_DIRS  += -Ibsp/temperate

SOURCE_DIRS   +=   bsp/backlight
INCLUDE_DIRS  += -Ibsp/backlight

#SOURCE_DIRS   +=   bsp/dvr
#INCLUDE_DIRS  += -Ibsp/dvr

SOURCE_DIRS   +=   bsp/dev_pwr
INCLUDE_DIRS  += -Ibsp/dev_pwr
