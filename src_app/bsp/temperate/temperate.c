/**********************************************************************************/
/* bat_mana.c                                                                        */

/**********************************************************************************/
#include "std_type.h"
#include "cpu.h"
#include "serv.h"
#include "hal_os.h"

/** >>>>>>>> user include start  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#include "hal_adc.h"
#include "temperate.h"
#include "trace_api.h"
#include "avg_filter.h"

/** >>>>>>>> user include end    >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#define TEMP_AVG_PERIOD 100          /* 1 second   */
#define TEMP_STATE_CHG_DLY_TM  30000 /* 5 minitues */

#define TEMP_OFFSET 40 /* -40 degree is saved as 0 in code. */

typedef struct
{
    uint8_t temp;
    uint16_t adc;
} temp_adc_map_t;

typedef struct
{
    uint16_t temp_low;
    uint16_t temp_high;
    uint8_t allowed_pwm;
} temp_pwm_map_t;

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static const temp_adc_map_t temp_adc_tbl[] =
{
    //lint -e778
    {-40 + TEMP_OFFSET, 3502 },
    //lint +e778
    {-35 + TEMP_OFFSET, 3347 },
    {-30 + TEMP_OFFSET, 3170 },
    {-25 + TEMP_OFFSET, 2973 },
    {-20 + TEMP_OFFSET, 2759 },
    {-15 + TEMP_OFFSET, 2534 },
    {-10 + TEMP_OFFSET, 2304 },
    {-5  + TEMP_OFFSET, 2074 },
    //lint -e835
    {0   + TEMP_OFFSET, 1850 },
    //lint +e835
    {5   + TEMP_OFFSET, 1638 },
    {10  + TEMP_OFFSET, 1441 },
    {15  + TEMP_OFFSET, 1260 },
    {20  + TEMP_OFFSET, 1097 },
    {25  + TEMP_OFFSET, 952  },
    {30  + TEMP_OFFSET, 824  },
    {35  + TEMP_OFFSET, 712  },
    {40  + TEMP_OFFSET, 615  },
    {45  + TEMP_OFFSET, 531  },
    {50  + TEMP_OFFSET, 458  },
    {55  + TEMP_OFFSET, 396  },
    {60  + TEMP_OFFSET, 343  },
    {65  + TEMP_OFFSET, 297  },
    {70  + TEMP_OFFSET, 259  },
    {71  + TEMP_OFFSET, 252  },
    {72  + TEMP_OFFSET, 245  },
    {73  + TEMP_OFFSET, 238  },
    {74  + TEMP_OFFSET, 232  },
    {75  + TEMP_OFFSET, 226  },
    {76  + TEMP_OFFSET, 219  },
    {77  + TEMP_OFFSET, 214  },
    {78  + TEMP_OFFSET, 208  },
    {79  + TEMP_OFFSET, 202  },
    {80  + TEMP_OFFSET, 197  },
    {81  + TEMP_OFFSET, 192  },
    {82  + TEMP_OFFSET, 187  },
    {83  + TEMP_OFFSET, 182  },
    {84  + TEMP_OFFSET, 177  },
    {85  + TEMP_OFFSET, 173  },
    {86  + TEMP_OFFSET, 168  },
    {87  + TEMP_OFFSET, 164  },
    {88  + TEMP_OFFSET, 159  },
    {89  + TEMP_OFFSET, 155  },
    {90  + TEMP_OFFSET, 151  },
    {91  + TEMP_OFFSET, 148  },
    {92  + TEMP_OFFSET, 144  },
    {93  + TEMP_OFFSET, 140  },
    {94  + TEMP_OFFSET, 137  },
    {95  + TEMP_OFFSET, 133  },
    {100 + TEMP_OFFSET, 117  },
    {105 + TEMP_OFFSET, 104  },
    {110 + TEMP_OFFSET, 92   },
    {115 + TEMP_OFFSET, 82   },
    {120 + TEMP_OFFSET, 73   },
    {125 + TEMP_OFFSET, 65   },
};

static const temp_pwm_map_t temp_pwm_tbl[] =
{
    //lint -e778
    { -40 + TEMP_OFFSET, 65  + TEMP_OFFSET, 100},
    //lint +e778
    {  60 + TEMP_OFFSET, 70  + TEMP_OFFSET,  90},
    {  65 + TEMP_OFFSET, 75  + TEMP_OFFSET,  80},
    {  70 + TEMP_OFFSET, 80  + TEMP_OFFSET,  60},
    {  75 + TEMP_OFFSET, 85  + TEMP_OFFSET,  40},
    {  80 + TEMP_OFFSET, 90  + TEMP_OFFSET,  20},
    {  85 + TEMP_OFFSET, 125 + TEMP_OFFSET,  0},
};

#define TEMP_STATE_COUNT (sizeof(temp_pwm_tbl) / sizeof(temp_pwm_tbl[0]))

#define TEMP_ADC_TBL_SIZE (sizeof(temp_adc_tbl) / sizeof(temp_adc_tbl[0]))

#define TEMPERATE_MAX TEMP_STATE_COUNT

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static uint8_t 	    temperate_state;                                //current temperature state, normal, low, high
static uint16_t     temperate_adc_val;                              //current temperature adc raw data
static uint8_t      temperate_val;                                  //current temperature value,offset=-55 deg.C
static uint16_t     temperate_filter_dly[(uint8_t)TEMPERATE_MAX];   //temperature filter delay

static uint16_t temp_avg_buf[TEMP_AVG_PERIOD];
avg_filter_handle_t temp_filter;

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void Temperate_MainFunc(void);
static void Temperate_SetLeftTempStateDlyToDefault(uint8_t state);

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void   Temperate_Init(void)
{
    temperate_state   = 0;
    temperate_adc_val   = 0u;
    temperate_val       = 25 + TEMP_OFFSET;                                 //default: 25 deg.C
    Temperate_SetLeftTempStateDlyToDefault((uint8_t)TEMPERATE_MAX);

    HalAdc_ChInit(THERM_SEN_ID);
    AVG_FILTER_CreateHandle(&temp_filter, temp_avg_buf, TEMP_AVG_PERIOD);

    ServFunc_StartCyclic(ServFunc_Create(Temperate_MainFunc, "Temperature management Task"), 10, 10);
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void Temperate_SetLeftTempStateDlyToDefault(uint8_t state)
{
    uint16_t dly = TEMP_STATE_CHG_DLY_TM;

    for (uint8_t i = 0u; i < (uint8_t)TEMPERATE_MAX; i++)
    {
        if (i != state)
        {
            temperate_filter_dly[i] = dly;
        }
    }
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void Temperate_CalCurTempVal(void)
{
    uint16_t i;
    uint16_t new_temperate_adc_val;

    HalAdc_GetChVal(THERM_SEN_ID, &new_temperate_adc_val);

    new_temperate_adc_val = AVG_FILTER_Feed(&temp_filter, new_temperate_adc_val);

    if (temperate_adc_val != new_temperate_adc_val)
    {
        temperate_adc_val = new_temperate_adc_val;

        if (temperate_adc_val > temp_adc_tbl[0].adc)
        {
            temperate_val = temp_adc_tbl[0].temp;

            TRACE_VALUE(LOG_ERR, MOD_HW, "[Temperature management], temperature capture error, out of sensor range!!, adc val = %d", temperate_adc_val);
        }
        else if (temperate_adc_val < temp_adc_tbl[TEMP_ADC_TBL_SIZE - 1].adc)
        {
            temperate_val = temp_adc_tbl[TEMP_ADC_TBL_SIZE - 1].temp;

            TRACE_VALUE(LOG_ERR, MOD_HW, "[Temperature management], temperature capture error, out of sensor range!!, adc val = %d", temperate_adc_val);
        }
        else
        {
            for (i=1; i < TEMP_ADC_TBL_SIZE; i++)
            {
                if ((temperate_adc_val <= temp_adc_tbl[i - 1].adc) && (temperate_adc_val >= temp_adc_tbl[i].adc))
                {
                    if (temperate_adc_val < ((temp_adc_tbl[i - 1].adc + temp_adc_tbl[i].adc) / 2))
                    {
                        temperate_val = temp_adc_tbl[i].temp;
                    }
                    else
                    {
                        temperate_val = temp_adc_tbl[i - 1].temp;
                    }
                    break;
                }
            }
        }
    }
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static uint8_t Temperate_GetCurTempState(void)
{
    uint8_t new_temp_state = temperate_state;

    for (;;)
    {
        if (temperate_val > temp_pwm_tbl[new_temp_state].temp_high)
        {
            if (new_temp_state >= (TEMP_STATE_COUNT - 1))
            {
                break;
            }
            else
            {
                new_temp_state ++;
            }
        }
        else if (temperate_val < temp_pwm_tbl[new_temp_state].temp_low)
        {
            if (new_temp_state == 0)
            {
                break;
            }
            else
            {
                new_temp_state --;
            }
        }
        else
        {
            break;
        }
    }

    return new_temp_state;
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void Temperate_MainFunc(void)
{
    Temperate_CalCurTempVal();

    for (uint8_t i = 0u; i < (uint8_t)TEMPERATE_MAX; i++)
    {
        if (temperate_filter_dly[i] > 0u)
        {
            temperate_filter_dly[i]--;
        }
    }

    uint8_t cur_state = Temperate_GetCurTempState();
    if (temperate_state != cur_state)
    {
        if (0u == temperate_filter_dly[cur_state])
        {
            TRACE_VALUE2(LOG_SHOUT, MOD_HW, "[Temperature management], temperature from state%d change to state%d!", temperate_state, cur_state);
            temperate_state = cur_state;
        }
    }

    Temperate_SetLeftTempStateDlyToDefault(cur_state);
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
uint8_t  Temperate_GetState(void)
{
	return temperate_state;
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
int16_t Temperate_GetTemp(void)
{
	return (int16_t)(temperate_val - TEMP_OFFSET);
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
uint8_t Temperate_GetAllowedPwm(void)
{
	return temp_pwm_tbl[temperate_state].allowed_pwm;
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void Temperate_TraceRawData(void)
{
    TRACE_VALUE2(LOG_RAW, MOD_HW, "[Temperate management], current temp sensor adc val=%d, temperate_val=%d...", temperate_adc_val, temperate_val);
}

/** >>>>>>>> end of file  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
