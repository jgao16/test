#include "panel.h"
#include "bsp_gpio.h"
#include "hal_gpio.h"
#include "gpio.cfg"
#include "serv.h"

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

#define LCD_ON_Power_En_Time_Out        6
#define LCD_ON_RST_Time_Out             6
#define LCD_ON_STB_Time_Out             11
#define LCD_ON_BL_Time_Out              71
#define TP_RESET_Time_Out               1

#define LCD_OFF_Power_En_Time_Out       9
#define LCD_OFF_RST_Time_Out            75
#define LCD_OFF_TFT3V3_EN_Time_Out      3
#define LCD_OFF_STB_Time_Out            100

typedef enum
{
    LCD_State_PowerOff = 0,
    LCD_State_On_Power_En,     /* Wait the time to set Power_En */
    LCD_State_On_Reset,        /* Wait the time to set RST */
    LCD_State_On_STB,          /* Wait the time to set STB */
    LCD_State_On_BL,           /* Wait the time to set BL */
    LCD_State_PowerOn,         /* Could set BL if user required. */
    LCD_State_Off_STB,         /* Wait the time to unset STB. */
    LCD_State_Off_Reset,       /* Wait the time to unset RST. */
    LCD_State_Off_Power_En,    /* Wait the time to unset Power_En. */
    LCD_State_Off_TFT3V3,      /* Wait the time to unset TFT3V3. */
    LCD_State_MAX
} lcd_state_t;

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
uint16_t lcd_timeout;
volatile tp_power_state_t tp_power_state;
volatile lcd_state_t lcd_state;
volatile lcd_power_state_t lcd_power_state;
volatile boolean lcd_disabled;

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

static void PowerTimer_Task_2ms(void);

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

static void _LCD_PowerOn(void)
{
    Bsp_TPPowerTP3V3En();
    Bsp_TFTPowerTFT3V3En();
    lcd_state = LCD_State_On_Power_En;
    lcd_power_state = LCD_POWER_ON_ONGOING;
    lcd_timeout = 0;
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void _LCD_PowerOff(void)
{
    if (Bsp_TFTPowerBLGet())
    {
        Bsp_TFTPowerBLDis();
    }
    lcd_state = LCD_State_Off_STB;
    lcd_power_state = LCD_POWER_OFF_ONGOING;
    lcd_timeout = 0;
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void PANEL_Init(void)
{
    uint32 cpu_sr;

    cpu_sr = osEnterCritical();

    lcd_state = LCD_State_PowerOff;
    tp_power_state = TP_Power_State_Off;
    lcd_power_state = LCD_POWER_OFF;
    lcd_disabled = FALSE;

    osExitCritical(cpu_sr);

    LCD_PowerOn(TRUE);

    ServFunc_StartCyclic(ServFunc_Create(PowerTimer_Task_2ms, "PowerTimer Task"),1, 2);
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void PowerTimer_Task_2ms(void)
{
    uint32 cpu_sr;

    cpu_sr = osEnterCritical();

    switch (lcd_state)
    {
        case LCD_State_On_Power_En:
            if ((lcd_timeout ++) == LCD_ON_Power_En_Time_Out)
            {
                Bsp_TFTPowerEn();
                lcd_timeout = 0;
                lcd_state = LCD_State_On_Reset;
            }
            break;

        case LCD_State_On_Reset:
            if ((lcd_timeout ++) == LCD_ON_RST_Time_Out)
            {
                Bsp_TFTPowerLCDRSTEn();
                lcd_timeout = 0;
                lcd_state = LCD_State_On_STB;
            }
            break;

        case LCD_State_On_STB:
            if ((lcd_timeout ++) == LCD_ON_STB_Time_Out)
            {
                Bsp_TFTPowerLCDSTBEn();
                Bsp_TPResetEn();
                tp_power_state = TP_Power_State_On;
                lcd_state = LCD_State_On_BL;
                lcd_timeout = 0;
            }
            break;

        case LCD_State_On_BL:
            if ((lcd_timeout ++) == LCD_ON_BL_Time_Out)
            {
                lcd_timeout = 0;
                lcd_state = LCD_State_PowerOn;
                lcd_power_state = LCD_POWER_ON;
            }
            break;

        case LCD_State_Off_STB:
            if ((lcd_timeout ++) == LCD_OFF_STB_Time_Out)
            {
                Bsp_TPResetBLDis();
                Bsp_TFTPowerLCDSTBDis();
                tp_power_state = TP_Power_State_Off;
                lcd_state = LCD_State_Off_Reset;
                lcd_timeout = 0;
            }
            break;

        case LCD_State_Off_Reset:
            if ((lcd_timeout ++) == LCD_OFF_RST_Time_Out)
            {
                Bsp_TFTPowerLCDRSTDis();
                lcd_timeout = 0;
                lcd_state = LCD_State_Off_Power_En;
            }
            break;

        case LCD_State_Off_Power_En:
            if ((lcd_timeout ++) == LCD_OFF_Power_En_Time_Out)
            {
                Bsp_TFTPowerDis();
                lcd_timeout = 0;
                lcd_state = LCD_State_Off_TFT3V3;
            }
            break;

        case LCD_State_Off_TFT3V3:
            if ((lcd_timeout ++) == LCD_OFF_TFT3V3_EN_Time_Out)
            {
                Bsp_TFTPowerTFT3V3Dis();
                Bsp_TPPowerTP3V3Dis();
                lcd_timeout = 0;
                lcd_state = LCD_State_PowerOff;
                lcd_power_state = LCD_POWER_OFF;
            }
            break;

        default:
            lcd_timeout = 0;
            break;
    }

    osExitCritical(cpu_sr);
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
lcd_power_state_t LCD_GetPowerState(void)
{
    return lcd_power_state;
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
tp_power_state_t LCD_TP_GetPowerState(void)
{
    return tp_power_state;
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void LCD_PowerOn(boolean on)
{
    uint32 cpu_sr;

    cpu_sr = osEnterCritical();

    if ((!lcd_disabled) &&  on && ((lcd_power_state == LCD_POWER_OFF) || (lcd_power_state == LCD_POWER_OFF_ONGOING)))
    {
        _LCD_PowerOn();
    }
    else if ((!on) && ((lcd_power_state == LCD_POWER_ON) || (lcd_power_state == LCD_POWER_ON_ONGOING)))
    {
        _LCD_PowerOff();
    }
    else
    {
        ;
    }

    osExitCritical(cpu_sr);
}

extern void LCD_Disable()
{
    LCD_PowerOn(FALSE);
    lcd_disabled = TRUE;
}
