#ifndef PANEL_H
#define PANEL_H

#include "std_type.h"

typedef enum
{
    LCD_POWER_OFF = 0,
    LCD_POWER_ON,
    LCD_POWER_ON_ONGOING,
    LCD_POWER_OFF_ONGOING,
} lcd_power_state_t;

typedef enum
{
    TP_Power_State_Off = 0,
    TP_Power_State_On,
} tp_power_state_t;

extern void PANEL_Init(void);

extern lcd_power_state_t LCD_GetPowerState(void);

extern tp_power_state_t LCD_TP_GetPowerState(void);

/* Only call this when  LCD power state is ON or OFF */
extern void LCD_PowerOn(boolean on);

/* Disable and power off the LCD, once disabled, could not be enabled until reset. */
extern void LCD_Disable();

#endif
