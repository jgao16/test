/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> 
    All rights reserved
>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

#include "std_type.h"
#include "bsp_gpio.h"
#include "hal_os.h"
#include "hal_gpio.h"
#include "serv.h"


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

#define OUTPUT_GPIO_MAX	(sizeof(gpio_output_attr)/sizeof(GPIO_OUT_ATTR))
#define INPUT_GPIO_MAX	(sizeof(gpio_input_attr)/sizeof(GPIO_INPUT_ATTR))

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
typedef struct
{	
	HAL_PORT_NAME	port;
	uint8 			pin;
	uint8 			flt_cnt;
	INPUT_TYPE      type;
	boolean 		actv;
}GPIO_INPUT_ATTR;

typedef struct
{//port,pin,dfl_val,actv_val
	HAL_PORT_NAME 	port;
	uint8 			pin;
	uint8 			dfl_val;
	uint8 			actv_val;
}GPIO_OUT_ATTR;
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static const GPIO_INPUT_ATTR gpio_input_attr[] = 
{
	#define GPIO_CFG_INPUT(name,port,pin, filter_cnt,input_type,actv_val)	{port,pin,filter_cnt,input_type,actv_val},
		#include "gpio.cfg"
	#undef GPIO_CFG_INPUT
};
static const GPIO_OUT_ATTR gpio_output_attr[] = 
{
	#define GPIO_CFG_OUTPUT(name,port,pin,dfl_val,actv_val)		{port,pin,dfl_val,actv_val},
		#include "gpio.cfg"
	#undef GPIO_CFG_OUTPUT
};


static uint8 io_filter_cnt[INPUT_GPIO_MAX];

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void   	BspGpio_Init(void)
{
	/* initial output gpio */
	for (uint16 i = 0; i < OUTPUT_GPIO_MAX; i ++ )
	{
		GPIO_OUT_ATTR const *attr = gpio_output_attr + i;
		HalGpio_InitPinGpio_Def(attr->port,attr->pin,	PORT_OUTPUT,OUTPUT_PUSH_PULL,INPUT_NO_PULL);
		HalGpio_SetPinVal(attr->port,attr->pin,attr->dfl_val);
	}

	for (uint16 i = 0; i < INPUT_GPIO_MAX; i ++ )
	{
		GPIO_INPUT_ATTR const *attr = gpio_input_attr + i;
		HalGpio_InitPinGpio_Def(attr->port,attr->pin,	PORT_INPUT,OUTPUT_OPEN_DRAIN,attr->type);
	}
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void   	BspGpio_EnterToSleep()
{
	
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void	BspGpio_ExitFromSleep(void)
{
	
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void 	BspGpio_SetVal(uint16 name, boolean value)
{
	if ( name < BPS_GPIO_NAME_MAX && name >= INPUT_GPIO_MAX )
	{
		name -= INPUT_GPIO_MAX;
		GPIO_OUT_ATTR const *attr = gpio_output_attr + name;
		if ( attr->actv_val )
		{
			HalGpio_SetPinVal(attr->port,attr->pin,!!value); //lint !e730
		}
		else
		{
			HalGpio_SetPinVal(attr->port,attr->pin,!value); //lint !e730
		}
	}
}
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void 	BspGpio_ToggleVal(uint16 name)
{
	//HalGpio_TogglePinval
	if ( name < BPS_GPIO_NAME_MAX && name >= INPUT_GPIO_MAX )
	{
		name -= INPUT_GPIO_MAX;
		GPIO_OUT_ATTR const *attr = gpio_output_attr + name;
		HalGpio_TogglePinval(attr->port,attr->pin);
	}	
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
boolean	BspGpio_GetVal(uint16 name)			// get value with filter
{
	boolean val = 0;
	if ( name < INPUT_GPIO_MAX )
	{
		val = (io_filter_cnt[name]&0x80)>0;
	}
	else if ( name < BPS_GPIO_NAME_MAX )
	{
		name -= INPUT_GPIO_MAX;
		val = HalGpio_GetPinVal(gpio_output_attr[name].port, gpio_output_attr[name].pin);
	}
	else{}

	return val;
}
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
boolean	BspGpio_GetHwInsVal(uint16 name)		// get value without filter, 
{
	boolean val = 0;
	if ( name < INPUT_GPIO_MAX )
	{
		val = (HalGpio_GetPinVal(gpio_input_attr[name].port, gpio_input_attr[name].pin)==gpio_input_attr[name].actv);
	}
	else if ( name < BPS_GPIO_NAME_MAX )
	{
		name -= INPUT_GPIO_MAX;
		val = HalGpio_GetPinVal(gpio_output_attr[name].port, gpio_output_attr[name].pin);
	}
	else{}

	return val;	
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void 	BspGpio_SetGpioInt(uint16 name,INT_EDGE edge)
{
	if ( name < INPUT_GPIO_MAX )
	{
		INT_EDGE edge_val;
		
		if ( edge == GPIO_BOTH_EDGE )
		{
			edge_val = GPIO_BOTH_EDGE;
		}
		else if ( edge == GPIO_RISING_EDGE ) 
		{
			if ( gpio_input_attr[name].actv == ACTIVATE_HIGH )
			{
				edge_val = GPIO_RISING_EDGE;
			}
			else
			{
				edge_val = GPIO_FALLING_EDGE;
			}
		}
		else
		{	// GPIO_FALLING_EDGE
			if ( gpio_input_attr[name].actv == ACTIVATE_HIGH )
			{
				edge_val = GPIO_FALLING_EDGE;
			}
			else
			{
				edge_val = GPIO_RISING_EDGE;
			}
		}
		HalGpio_InitPinInt_Def (gpio_input_attr[name].port, gpio_input_attr[name].pin, edge_val, NULL);
	}
	else if ( name < BPS_GPIO_NAME_MAX )
	{
		name -= INPUT_GPIO_MAX;
		INT_EDGE edge_val;
		if ( edge == GPIO_BOTH_EDGE )
		{
			edge_val = GPIO_BOTH_EDGE;
		}
		else if ( edge == GPIO_RISING_EDGE ) 
		{
			if ( gpio_output_attr[name].actv_val == ACTIVATE_HIGH )
			{
				edge_val = GPIO_RISING_EDGE;
			}
			else
			{
				edge_val = GPIO_FALLING_EDGE;
			}
		}
		else
		{	// GPIO_FALLING_EDGE
			if ( gpio_output_attr[name].actv_val == ACTIVATE_HIGH )
			{
				edge_val = GPIO_FALLING_EDGE;
			}
			else
			{
				edge_val = GPIO_RISING_EDGE;
			}
		}
		HalGpio_InitPinInt_Def (gpio_output_attr[name].port, gpio_output_attr[name].pin, edge_val, NULL);
	}
	else{}
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void 	BspGpio_EnGpioInt(uint16 name)
{
	if ( name < INPUT_GPIO_MAX )
	{
		HalGpio_EnPinInt (gpio_input_attr[name].port, gpio_input_attr[name].pin);
	}
	else if ( name < BPS_GPIO_NAME_MAX )
	{
		name -= INPUT_GPIO_MAX;
		HalGpio_EnPinInt (gpio_output_attr[name].port, gpio_output_attr[name].pin);
	}
	else{}
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void 	BspGpio_DisGpioInt(uint16 name)
{
	if ( name < INPUT_GPIO_MAX )
	{
		HalGpio_DisPinInt(gpio_input_attr[name].port, gpio_input_attr[name].pin);
	}
	else if ( name < BPS_GPIO_NAME_MAX )
	{
		name -= INPUT_GPIO_MAX;
		HalGpio_DisPinInt (gpio_output_attr[name].port, gpio_output_attr[name].pin);
	}
	else{}
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void	BspGpio_MainFunc(void)		// 10ms function
{
	for( uint16 i = 0; i < INPUT_GPIO_MAX; i ++ )
	{
		GPIO_INPUT_ATTR const*attr = gpio_input_attr + i ;
		boolean  pin_val 	  = HalGpio_GetPinVal(attr->port, attr->pin);
		if ( pin_val == attr->actv )
		{
			if ( io_filter_cnt[i]&0x80 )
			{
				io_filter_cnt[i] |= 0xFE;
			}
			else
			{
				io_filter_cnt[i] += attr->flt_cnt;
				if ( io_filter_cnt[i]&0x80 )
				{
					io_filter_cnt[i] = 0xFE;
				}
			}
		}
		else
		{	
			if ( !(io_filter_cnt[i]&0x80) )
			{
				io_filter_cnt[i] &= 0x01;
			}
			else
			{
				io_filter_cnt[i] -= attr->flt_cnt;
				if ( !(io_filter_cnt[i]&0x80) )
				{
					io_filter_cnt[i] = 0x01;
				}
			}	
		}		
	}
	
}


