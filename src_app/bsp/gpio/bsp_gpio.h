/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    Service config:
    All rights reserved
>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

#pragma once


#ifdef  __cplusplus
extern "C"
{
#endif



/*****************************************************************************
 *  Include Files
 *****************************************************************************/
#include "hal_gpio.h"
#include "gpio.cfg"
/*****************************************************************************
 *  Global Macro Definitions
 *****************************************************************************/
/* Output Definitions */

typedef enum
{
	#define GPIO_CFG_INPUT(name,port,pin, filter_cnt,pull_attr,actv_val)	name##_ID,
		#include "gpio.cfg"
	#undef GPIO_CFG_INPUT
	#define GPIO_CFG_OUTPUT(name,port,pin,dfl_val,actv_val)		name##_ID,
		#include "gpio.cfg"
	#undef GPIO_CFG_OUTPUT
	BPS_GPIO_NAME_MAX
}BPS_GPIO_NAME;

/*****************************************************************************
 *  Global Type Declarations
 *****************************************************************************/
/* %<Types> */



/*****************************************************************************
 *  Global Data Declarations
 *****************************************************************************/
/* %<CalStructHeader> 	*/
/* %<CalStructContents>	*/
/* %<CalStructTrailer>	*/


/*****************************************************************************
 *  Global Function Declarations
 *****************************************************************************/

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void   	BspGpio_Init(void);
void   	BspGpio_EnterToSleep();
void	BspGpio_ExitFromSleep(void);

void 	BspGpio_SetVal(uint16 name, boolean value);
void 	BspGpio_ToggleVal(uint16 name);
boolean	BspGpio_GetVal(uint16 name);			// get value with filter
boolean	BspGpio_GetHwInsVal(uint16 name);		// get value without filter, 

void 	BspGpio_SetGpioInt(uint16 name,INT_EDGE edge);
void 	BspGpio_EnGpioInt(uint16 name);
void 	BspGpio_DisGpioInt(uint16 name);

void	BspGpio_MainFunc(void);		// 10ms function


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
//void   Port_SystemRst(uint16 cmd);

//void   Port_SetUserWakeupReason(uint16 cmd);

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/									

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/






#ifdef  __cplusplus
}
#endif



