
trace power on:
80 01 00 00 80 F4 A3 58: TRACE_MESSAGE_HEADER: TRACE_ID_VERSION_INFO
90 00 00 00 00 00 00 00: TRACE_STATUS_HEADER:  STATUS_MSG_RESET
80 00 6D EA 00 00 00 00: TRACE_MESSAGE_HEADER, Normal trace message, "Trace_Channels_Init"



LOG_ERROR	 		/* errors, display always 						*/
LOG_SHOUT		 	/* not an error but we should always see it 	*/
LOG_WARN			/* not errors but may indicate a problem 		*/
LOG_INFO			/* key informative messages 					*/
LOG_PROG			/* progress messages 							*/
LOG_IO				/* IO to and from devices 						*/
LOG_DATA			/* log data management messages 				*/
LOG_SPIN			/* logging for catching spin bugs 				*/
LOG_RAW				/* raw low-level I/O 							*/


TRACE_TEXT(TRACE_TEST, MOD_OS, "Time 500ms occured");
-->LogText(LogLevel,ModuleName,Text)

example: LogText(LOG_ERROR,MOD_OS,"Time 500ms occured");


Trace_IDs__PROJECT
tr_, _TR_FILE , __LINE__ in Trace_IDs__PROJECT.h

#define Trace_Text__(a,b,c)                        Trace_Text  (a##b##_##c)
#define Trace_Mem__(a,b,c,d,e)                     Trace_Mem(a##b##_##c, d, e)
#define Trace_String__(a,b,c,d)                    Trace_String(a##b##_##c, d)
#define Trace_Value__(a,b,c,d)                     Trace_Value (a##b##_##c, d)
#define Trace_Value2__(a,b,c,d,e)                  Trace_Value2(a##b##_##c, d, e)
#define Trace_Value4__(a,b,c,d,e,f,g)              Trace_Value4(a##b##_##c, d, e, f, g)
#define Trace_SM_Text__(a,b,c,d)                   Trace_Text(a##b##_##c + d)
#define Trace_SM_Value__(a,b,c,d,e)                Trace_Value(a##b##_##c + d, e)
#define Trace_SM_Value2__(a, b, c, d, e, f)        Trace_Value2(a##b##_##c + d, e, f)
#define Trace_SM_Value4__(a, b, c, d, e, f, g, h)  Trace_Value4(a##b##_##c + d, e, f, g, h)

#define Trace_Text_(a,b,c)                        Trace_Text__(a,b,c)
#define Trace_Mem_(a,b,c,d,e)                     Trace_Mem__(a,b,c,d,e)
#define Trace_String_(a,b,c,d)                    Trace_String__(a,b,c,d)
#define Trace_Value_(a,b,c,d)                     Trace_Value__(a,b,c,d)
#define Trace_Value2_(a,b,c,d,e)                  Trace_Value2__(a,b,c,d,e)
#define Trace_Value4_(a,b,c,d,e,f,g)              Trace_Value4__(a,b,c,d,e,f,g)
#define Trace_SM_Text_(a,b,c,d)                   Trace_SM_Text__(a,b,c,d)
#define Trace_SM_Value_(a,b,c,d,e)                Trace_SM_Value__(a,b,c,d,e)
#define Trace_SM_Value2_(a, b, c, d, e, f)        Trace_SM_Value2__(a, b, c, d, e, f)
#define Trace_SM_Value4_(a, b, c, d, e, f, g, h)  Trace_SM_Value4__(a, b, c, d, e, f, g, h)

#define LogText(LogLevel,ModuleName,Text)	if(ModuleName##_Level >= LogLevel){ Trace_Text_ (tr_, _TR_FILE ,__LINE__) ; }
	
   #define LOG_TEXT(ModuleName, Text)                                           {if( ModuleName##_bit ) { Trace_Text_  (tr_, _TR_FILE , __LINE__) ; }}
   #define LOG_MEM(ModuleName, Text, Length, DataPointer)                        {if( ModuleName##_bit ) { Trace_Mem_(tr_, _TR_FILE , __LINE__, (Length), (DataPointer)); }}
   #define LOG_STRING(ModuleName, FormatText, String)                           {if( ModuleName##_bit ) { Trace_String_(tr_, _TR_FILE , __LINE__, String); }}
   #define LOG_VALUE(ModuleName, FormatText, Value)                             {if( ModuleName##_bit ) { Trace_Value_ (tr_, _TR_FILE , __LINE__, (Value)); }}
   #define LOG_VALUE2(ModuleName, FormatText, Value1, Value2)                   {if( ModuleName##_bit ) { Trace_Value2_(tr_, _TR_FILE , __LINE__, (Value1), (Value2)); }}
   #define LOG_VALUE4(ModuleName, FormatText, Value1, Value2, Value3, Value4)   {if( ModuleName##_bit ) { Trace_Value4_(tr_, _TR_FILE , __LINE__, (Value1), (Value2), (Value3), (Value4)); }}
   #define LOG_SM_TEXT(ModuleName, offset)                                      {if( ModuleName##_bit ) { Trace_SM_Text_(tr_, _TR_FILE, __LINE__, (offset)); }}
   #define LOG_SM_VALUE(ModuleName, offset, Value)                              {if( ModuleName##_bit ) { Trace_SM_Value_(tr_, _TR_FILE, __LINE__, (offset), (Value)); }}
   #define LOG_SM_VALUE2(ModuleName, offset, Value1, Value2)                    {if( ModuleName##_bit ) { Trace_SM_Value2_(tr_, _TR_FILE, __LINE__, (offset), (Value1), (Value2)); }}
   #define LOG_SM_VALUE4(ModuleName, offset, Value1, Value2, Value3, Value4)    {if( ModuleName##_bit ) { Trace_SM_Value4_(tr_, _TR_FILE, __LINE__, (offset), (Value1), (Value2), (Value3), (Value4)); }}


#define LogText(LogLevel,ModuleName,Text)	if(ModuleName##_Level >= LogLevel){ Trace_Text_ (tr_, _TR_FILE ,__LINE__) ; }

	
	
#define	LOG_TEXT(LogLevel,ModuleName, Text)                                           {if( ModuleName##_Level >= LogLevel) { Trace_Text_  (tr_, _TR_FILE , __LINE__) ; }}
#define LOG_MEM(LogLevel,ModuleName, Text, Length, DataPointer)                        {if( ModuleName##_Level >= LogLevel ) { Trace_Mem_(tr_, _TR_FILE , __LINE__, (Length), (DataPointer)); }}
#define LOG_STRING(LogLevel,ModuleName, FormatText, String)                           {if( ModuleName##_Level >= LogLevel ) { Trace_String_(tr_, _TR_FILE , __LINE__, String); }}
#define LOG_VALUE(LogLevel,ModuleName, FormatText, Value)                             {if( ModuleName##_Level >= LogLevel ) { Trace_Value_ (tr_, _TR_FILE , __LINE__, (Value)); }}
#define LOG_VALUE2(LogLevel,ModuleName, FormatText, Value1, Value2)                   {if( ModuleName##_Level >= LogLevel) { Trace_Value2_(tr_, _TR_FILE , __LINE__, (Value1), (Value2)); }}
#define LOG_VALUE4(LogLevel,ModuleName, FormatText, Value1, Value2, Value3, Value4)   {if( ModuleName##_Level >= LogLevel ) { Trace_Value4_(tr_, _TR_FILE , __LINE__, (Value1), (Value2), (Value3), (Value4)); }}
#define LOG_SM_TEXT(LogLevel,ModuleName, offset)                                      {if( ModuleName##_Level >= LogLevel) { Trace_SM_Text_(tr_, _TR_FILE, __LINE__, (offset)); }}
#define LOG_SM_VALUE(LogLevel,ModuleName, offset, Value)                              {if( ModuleName##_Level >= LogLevel) { Trace_SM_Value_(tr_, _TR_FILE, __LINE__, (offset), (Value)); }}
#define LOG_SM_VALUE2(LogLevel,ModuleName, offset, Value1, Value2)                    {if( ModuleName##_Level >= LogLevel ) { Trace_SM_Value2_(tr_, _TR_FILE, __LINE__, (offset), (Value1), (Value2)); }}
#define LOG_SM_VALUE4(LogLevel,ModuleName, offset, Value1, Value2, Value3, Value4)    {if( ModuleName##_Level >= LogLevel ) { Trace_SM_Value4_(tr_, _TR_FILE, __LINE__, (offset), (Value1), (Value2), (Value3), (Value4)); }}

#define LOG_REGISTER_CALLBACK_FUNCTION ...  