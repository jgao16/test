#ifndef _TRACE_DISPATCHER_H_
#define _TRACE_DISPATCHER_H_


/**
 *  @file           Trace_Dispatcher.h
 *  @ingroup        IOC_TraceTools_pool
 *  @author         Jung, Simon
 *
 *  @brief This is the header file for Trace_Dispatcher.c.
 *  The  file  contains  function prototypes for all functions exposed by Trace_Dispatcher.c.
 ******************************************************************************/


#include "Trace_Formats.h"

extern uint8_t Trace_IPC_ConnectionReadyFlag_u8;


/***********************************************************************************************/
/* Function Name    :   Trace_DispatcherInit()                                                 */
/* Description      :   This function is initialized the Dispatcher with the channel           */
/*                      preconfigured in Trace.cfg                                             */
/***********************************************************************************************/
/** @brief This function initializes the Dispatcher
 */
   void     Trace_DispatcherInit(void);



/***********************************************************************************************/
/* Function Name    :   Trace_Dispatcher_ResetConnections()                                    */
/* Description      :   This function resets the connection state of the configured interfaces */
/***********************************************************************************************/
/** @brief This function resets the connection state of the configured interfaces
 */
void Trace_Dispatcher_ResetConnections(void);





/***********************************************************************************************/
/* Function Name    :   Trace_Dispatcher_SendNextFrameFromBuffer()                                        */
/* Description      :   This function gets the next frame from buffer and transmits            */
/*                      it via the current channel (IPC, CAN, UART, etc.)                      */
/***********************************************************************************************/
/** @brief This function gets the next frame from buffer and transmits it via the current channel (IPC, CAN, UART, etc.)
 *  If the current channel is IPC first the dump state is checked: If dump is started a start-dump-message is transmitted,
 *  if dump has finished a stop-dump-message is transmitted.
 */
 void     Trace_Dispatcher_SendNextFrameFromBuffer(void);





/***********************************************************************************************/
/*                                       INTERNAL FUNCTION                                     */
/***********************************************************************************************/
/* Function Name    :  Trace_Dispatcher_TransmitComplete()                                     */
/* Description      :  This function is called by the module of the current used channel, if   */
/*                     current transmit has been completed.                                    */
/***********************************************************************************************/
/** @brief This function is called by the module of the current used channel, if current transmit has been completed.
 *  If the Transmission has been completed, the transmitted message is removed from Trace-Buffer and the Message-counter is increased.
 *  Afterwards LogPhyTransmitCompleteEvent() is called to generate an event signalling to the Trace-Server: the last transmission has finished.
 */
   void     Trace_Dispatcher_TransmitComplete(void);








/***********************************************************************************************/
/*                                       INTERNAL FUNCTION                                     */
/***********************************************************************************************/
/* Function Name    :  Trace_Dispatcher_TraceFrameReceived()                                   */
/* Description      :  This function invokes an LogPhyReceivedFrameEvent to indicate   */
/*                     to the Trace-Server that a message was received                         */
/***********************************************************************************************/
/** @brief This function invokes an LogPhyReceivedFrameEvent to signal to the Trace-Server: a message was received
 */
   void     Trace_Dispatcher_TraceFrameReceived(void);



/***********************************************************************************************/
/*                                       INTERNAL FUNCTION                                     */
/***********************************************************************************************/
/* Function Name    :  Trace_Dispatcher_AddFrameToFifoRX()                                     */
/* Description      :  This function passes the passed trace-frame to the RXBuffer in the      */
/*                     Trace-Server                                                            */
/***********************************************************************************************/
/** @brief This function forwards the passed trace-frame to the RXBuffer in the Trace-Server
 */
   void     Trace_Dispatcher_AddFrameToFifoRX(TRACE_FRAME * RXFrame, uint8_t channel);




/***********************************************************************************************/
/*                                       INTERNAL FUNCTION                                     */
/***********************************************************************************************/
/* Function Name    :  Trace_Dispatcher_TraceFrameReceived_OverIPC()                           */
/* Description      :  This function invokes an LogPhyReceivedFrameEvent to indicate   */
/*                     to the Trace-Server that a message was received over IPC                */
/***********************************************************************************************/
/** @brief This function invokes an LogPhyReceivedFrameEvent to signal to the Trace-Server: a message was received over IPC
 */
   void   Trace_Dispatcher_TraceFrameReceived_OverIPC(void);


/***********************************************************************************************/
/*                                       INTERNAL FUNCTION                                     */
/***********************************************************************************************/
/* Function Name    :  Trace_Dispatcher_GetIPCTraceCommand()                                   */
/* Description      :  This function is called to get the received IPC command                 */
/***********************************************************************************************/
/** @brief  This function is called to get the received IPC command
 */
   void   Trace_Dispatcher_GetIPCTraceCommand(void);



/**
 * @}
 */


#endif /* _TRACE_DISPATCHER_H_ */

