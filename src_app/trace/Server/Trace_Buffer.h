#ifndef _TRACE_BUFFER_H_
#define _TRACE_BUFFER_H_



#include "Trace_cfg.h"

#include "Trace_Internal.h"  /* For definiton of e_TRACE_BUFFER_STATE */
#include "Trace_Formats.h"     /* For definition of Trace-specific structures */



void 	TraceBuffer_Init(void);


void	TraceBuffer_AddFrame(TRACE_FRAME *tr);
uint16_t TraceBuffer_GetFifoAvblNum(void);
boolean	TraceBuffer_GetFrame(TRACE_FRAME *tr);

boolean	TraceBuffer_CheckBufferOk(void);


#endif /*_TRACE_BUFFER_H_*/

