

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#include "trace_cfg.h"

#include "Trace_Buffer.h"

//#include "tr_c_rs232.h"

#include "hal_hw.h"
#include "hal_uart.h"

#include "Trace_Dispatcher.h"
#include "Trace_Common.h"
#include "Trace_Common.h"
#include "log_cfg.h"
#include "trace_phy.h"

#if TRACE_UART_EN
#include "trace_phy_uart.h"
#endif

#if TRACE_CAN_EN
#include "trace_phy_can.h"
#endif
#if	TRACE_RAW_SPI_EN
#include "trace_phy_spi.h"
#endif
#if	TRACE_IPC_EN
#endif


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

#define DEF_RXWINDOWSIZE \
      11
//-------------------------------------------------------------------------------
/** Size in bytes of a Trace-message transmitted over the UART interface.
 *  The UART Trace-message consists of a start-byte, 8 bytes from the actual Trace-frame and a stop-byte
 */
#define DEF_UARTFrameLength_u8 \
      10
//-------------------------------------------------------------------------------
/** The start-byte, that every UART Trace-message has to start with
 */
#define DEF_START_BYTE \
       0xF2
//-------------------------------------------------------------------------------
/** The stop-byte, that every UART Trace-message has to be finished with
 */
#define DEF_STOP_BYTE \
       0xF8

#define MACRO_START_OF_WINDOW(X) \
	((X < (DEF_UARTFrameLength_u8 - 1)) ? ((DEF_RXWINDOWSIZE - (DEF_UARTFrameLength_u8 - 1)) + X) : (X-(DEF_UARTFrameLength_u8-1)))

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/



/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
__no_init static TRACE_PHY_COM trace_phy_com;


/** Variable, used to store fallback-channel, if UART is de-initialized  */
//uint8_t  OldTraceChannel = TRACE_SERIAL_CHANNEL;
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void TracePhy_Init(void)
{
    //HalOS_Delay(30);	// delay for rs232
    trace_phy_com = (TRACE_PHY_COM)LogPhy_GetLogChannel();
    if ( trace_phy_com == TRACE_COM_DEFAULT )
    {
        trace_phy_com = TRACE_COM_PHY_DEFAULT;
    }
    if ( trace_phy_com == TRACE_CAN0 )
    {
        // it is a CAN channel
        HalOS_Delay(30);
    }

#if TRACE_UART_EN
    TracePhyUart_Init();
#endif

#if TRACE_RAW_SPI_EN
    TracePhySpi_Init();
#endif
#if TRACE_CAN_EN
	TracePhyCAN_Init();
#endif
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void TracePhy_ResetConnection(void)
{}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
TRACE_PHY_COM TracePhy_GetCom(void)
{
    //TRACE_PHY_COM com = (TRACE_PHY_COM)LogPhy_GetLogChannel();
    //if ( com == TRACE_COM_DEFAULT )
    //{
    //	com = TRACE_COM_PHY_DEFAULT;
    //}
    //return com;
    return trace_phy_com;
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void TracePhy_SetCom(TRACE_PHY_COM com)
{
    trace_phy_com = com;
    LogPhy_SetLogChannel(com);
}
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void TracePhy_PollingFunc(void)
{
#if TRACE_UART_EN
  	TracePhyUart_PollingFunc();
#endif

#if TRACE_RAW_SPI_EN
   TracePhySpi_PollingFunc();
#endif
#if TRACE_CAN_EN
	TracePhyCAN_PollingFunc();
#endif
}
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
eTR_SEND_STATE TracePhy_SendFrame(TRACE_FRAME * trFrame)
{
    (void)trFrame;

    eTR_SEND_STATE state = SEND_PENDING;

    switch(trace_phy_com)
    {
    case TRACE_IPC:
        //(void)DevIpc_SendMsg(IPC_DEBUG, 8, trFrame->trRawData);
        state = SEND_OK;
        break;

#if TRACE_CAN_EN
    case TRACE_CAN0:
        state = TracePhyCAN_SendFrame(trFrame);
        break;
#endif

#if TRACE_UART_EN
    case TRACE_RS232:
        state = TracePhyUart_SendFrame(trFrame);
        break;
#endif

#if TRACE_RAW_SPI_EN
    case TRACE_RAW_SPI:
        state = TracePhySpi_SendFrame(trFrame);
        break;
#endif

    default:
        state = SEND_OK;
        break;
    }

    return state;
}

/** >>>>>>>>>>>>>>>>>>>>>> end of file >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


