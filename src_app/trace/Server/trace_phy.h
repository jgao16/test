/**********************************************************************************/
/* trace_phy.h                                                                    */
/* trace server  			                                                       */
/**********************************************************************************/
#ifndef TRACE_PHY_H_H__
#define TRACE_PHY_H_H__
//#pragma once

#include "stdint.h"

#include "Trace_Internal.h"
#include "Trace_Formats.h"

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

#define TracePhy_TxMsgComplete()	do{Trace_Dispatcher_TransmitComplete();}while(0)

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


#ifdef __cplusplus
extern "C" {
#endif

typedef enum
{
	TRACE_COM_DEFAULT, // default is rs232
    TRACE_RS232,
    TRACE_CAN0,
    TRACE_IPC,
    TRACE_RAW_SPI,

    TRACE_PHY_MAX
}TRACE_PHY_COM;

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void TracePhy_Init(void);
void TracePhy_ResetConnection(void);

eTR_SEND_STATE 	TracePhy_SendFrame(TRACE_FRAME * trFrame);

TRACE_PHY_COM 	TracePhy_GetCom(void);
void 			TracePhy_SetCom(TRACE_PHY_COM com);
void 			TracePhy_PollingFunc(void);


//void TracePhy_CANRxMsg(uint8 const *msg);

#ifdef __cplusplus
}
#endif

#endif // __gen_type_cf_type_h_h

