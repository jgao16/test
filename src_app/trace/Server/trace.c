
#include"trace_cfg.h"

#include "Trace_Common.h"
#include "trace_api.h"
#include "serv.h"
#include "pwrctrl.h"
#include "trace_server.h"
#include "trace_phy.h"
//-----------------------------------------------------------------------------
// Variables
//-----------------------------------------------------------------------------
//static	HAL_OS_SEM_ID trace_buffer_lock;
static 	HAL_OS_SEM_ID actv_trace_task;

// LogPhyTransmitCompleteEvent()
//
void trace_transmit_complete( void )
{
	HalOS_SemaphorePost(actv_trace_task);
}

// LogPhyReceivedFrameEvent
//
void trace_received_frame( void )
{
	HalOS_SemaphorePost(actv_trace_task);
}




// Function: Task_Trace
//
// Main function for Trace task
//
void Task_Trace( void const*p_arg )
{
	SERV_WDT		*wdt_cb;

	(void)p_arg;

	if ( actv_trace_task == NULL ){
		actv_trace_task = HalOS_SemaphoreCreateWithMaxCnt(1,1,"Actv Trace Task...");
	}
	if ( actv_trace_task == NULL ){
		(void)osThreadTerminate(osThreadGetId());
	}

	Trace_Channels_Init();
	/* create task watch dog 	*/
	wdt_cb	=	ServWdt_Create(5000,"Trace Watch Dog");								
	if ( wdt_cb == NULL ){
		(void)osThreadTerminate(osThreadGetId());
	}
	ServWdt_start(wdt_cb);

	for( ;; )
	{
		(void)HalOS_SemaphoreWait(actv_trace_task, 100);
		
		
		PwrCtrl_DisLpSleepGlobal();	// disable sleep

		Trace_Message_Transmitted();
		Trace_Message_Received();	// check if there is received message

		TracePhy_PollingFunc();		// polling function

		PwrCtrl_EnLpSleepGlobal();	// enable sleep
				

		/* reset watch dog 											*/
		ServWdt_reset(wdt_cb);    												
		
	} // task infinite for loop
}


