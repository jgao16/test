
#ifndef TRACE_VERSION_H_
#define TRACE_VERSION_H_
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

/*****this file is generate by perl**************************************************************************************** **/
//#include"stdint.h"


#ifdef __cplusplus
extern "C" {
#endif

#include "stdint.h"


uint32_t Trace_GetTraceVersion(void);

uint32_t Ver_GetSwBuildTm(void);



#ifdef __cplusplus
}
#endif

#endif