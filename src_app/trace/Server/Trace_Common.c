
#include "trace_cfg.h"
#include "Trace_Common.h"

/** Stores current Trace-buffer state  */
e_TRACE_BUFFER_STATE eTraceBufferState;

/** Stores current state of a Buffer-dump   */
e_TRACE_DUMP_STATE   eTraceDumpState;


/** Trace_Ignore_IPC_commands_u8 signals, if Trace-commands received over IPC should be ignored  */
//uint8_t                Trace_Ignore_IPC_commands_u8;


/** old module-mask storing the state of all 32 modules */
uint32_t               Trace_oldModuleMask_u32;


/** Stores absolute Systemtime, that was transmitted last */
uint32_t               lastTransmittedSystemTime;


/** Counter for counting how many times the buffer was completely filled  */
uint32_t Trace_NoOfBufferFullStates;


/** Counter for counting how many times the critical buffer-level was crossed  */
uint32_t Trace_NoOfBufferCriticalStates;


/** This boolean shows, if a client has ever connected to the Trace-Server.
 *  A connected client is interpreted from any received Trace-message over any interface.
 */
boolean TRACE_ClientConnected;


/** This boolean shows, if transmission/reception of traces is enabled or not. (TRUE = enabed, FALSE = disabled)  */
boolean Trace_EnabledState;


/** Stores number of Trace-frames successfully sent  */
uint8_t   Trace_MessageCount_u8;


/** Stores the ID for the current sequence of Data-frames  */
uint8_t   Trace_DataID_u8;


/** Indicates, that the transmission of Trace-messages must be triggered again, since it is hanging  */
uint8_t   Trace_RetriggerTransmission_Flag;


//#if(1 == SW_TR_IPC_ENABLED)
/** Flag indicating, that a Trace-message has been received over IPC-channel  */
//uint8_t Trace_IPC_MessageReceivedFlag;
//#endif



/** This variable stores the Systemtime that was transmitted last
 *  It is used to calculate the offset to the the current systemtime.
 */
uint32_t   Trace_LastTransmittedSystemtime_u32;


/** Variables, used for counting count of certain events for statistical reasons   */


uint16_t Trace_IPCSendFailedCounter;
uint16_t Trace_IPCSendPendingCounter;
uint16_t Trace_IPCChannelNotReadyCounter;
uint16_t Trace_IPCtxCallbackCounter;
uint16_t Trace_SendNextFrameCounter;
uint16_t Trace_RemovedFrameCounter;
uint16_t Trace_SendFrameFromBufferCounter;
uint16_t Trace_DoNotRemoveCounter;
uint16_t Trace_BufferRequestCounter;
uint16_t Trace_IPCConnCBCounter;
uint16_t Trace_AddedFrameToBufferCounter;
uint16_t Trace_FrameNotReadyCounter;
uint16_t Trace_TraceMacroCalledCounter;
uint16_t Trace_RetriggerTransmissionFlagSetCounter;
uint16_t Trace_BufferUsage;
uint16_t Trace_BufferUsageMax;






 /**
  * @}
  */
