#ifndef _TRACE_SERVER_H_
#define _TRACE_SERVER_H_



/**
 *  @file           Trace_Server.h
 *  @ingroup        IOC_TraceTools_pool
 *  @author         Vajapeyam, Divya - edited for new structure by Jung, Simon
 *
 *  @brief This  is the header file for Trace_Server.c.
 *  The  file  contains  function  prototypes for  all functions exposed by Trace_Server.c.
 ******************************************************************************/


/***********************************************************************************************/
/* Inclusion of requisite header files                                                         */
/***********************************************************************************************/

#include "Trace_Common.h"
//#include "TrSwitch.h"            /*For compile time switches to enable/disable traces.*/

#include "Trace_Dispatcher.h"


/***********************************************************************************************/
/*Function Name    :   Trace_API_Init()                                                        */
/*Description      :   This function initializes all Trace variables. This function MUST be    */
/*                     called before any other funnction in Trace.                             */
/***********************************************************************************************/
/**
 *  @brief Trace-API initialization function.
 *  The Trace-API initialization function Trace_API_Init() must be called before Trace-Macros are used.
 *  If a First-Start appeared the Trace_buffer is initialized.
 *  Also the Trace-Handler is initialized.
 *  The Indexes for the Receive-Fifo are initialized if necessary
 *  At last status-frames are addes to buffer with information about Trace-version, Systemtime, and Reset-Info
 *  @todo Check, whether Trace_Init might be called explicitly after the CMX-OS has been initialized to assure
 *  that used CMX-functions are available yet.
 */
void     Trace_API_Init                                   (void);  /* Call this at system start-up */



/***********************************************************************************************/
/*Function Name    :   Trace_Channels_Init()                                                   */
/*Description      :   This function initializes all Trace channels . This function MUST be    */
/*                     called before Trace-Frames are transmitted or received                  */
/***********************************************************************************************/
/**
 *  @brief Trace-Initialization function.
 *  The Trace-Channels initialization function Trace_Channels_Init() must be called before
 *  Trace-Frames are transmitted or received. If the channels are not initialized with this function
 *  No Traces will be transmitted or received.
 */

void  Trace_Channels_Init(void);

void Trace_Message_Transmitted(void);
void Trace_Message_Received(void);
/***********************************************************************************************/
/*Function Name    :   Trace_IsEnabledState()                                                */
/*Description      :   This function determines, if tracing is currently enabled               */
/***********************************************************************************************/
/**
 *  @brief This function determines, if tracing is currently enabled
 *  @ return TRUE, if tracing is currently enabled, else FALSE
 */

boolean  Trace_IsEnabledState                          (void);




/***********************************************************************************************/
/*Function Name    :   Trace_Enable()                                                          */
/*Description      :   This function enables storing of Traces into Trace-Buffer               */
/***********************************************************************************************/
/**
 *  @brief This function enables storing of Traces into Trace-Buffer.
 *         Traces are transmitted and received Traces processed.
 */
void  Trace_Enable(void);




/***********************************************************************************************/
/*Function Name    :   Trace_Disable()                                                         */
/*Description      :   This function disables storing of Traces into Trace-Buffer              */
/*                     After disabling, all Traces, left if Buffer are transmitted             */
/***********************************************************************************************/
/**
 *  @brief This function disables storing of Traces into Trace-Buffer.
 *         Remaining Traces in Buffer are transmitted, till Buffer is empty.
 *         Received Trace-Frames are discarded.
 *         Enable tracing with Trace_Enable().
 */
void  Trace_Disable(void);




/************************************************************************************************/
/* Function Name     :  Trace_AddFrameToRXBuffer(trFrameToAdd, channelReceivedFrom)             */
/* Description       :  This function adds the passed Trace-frame to the Receive-buffer.        */
/************************************************************************************************/
/**
 *  @brief This function adds the passed Trace-frame to the Receive-buffer.
 *  It is called, if the Trace-Dispatcher received a Trace-message.
 *  @param ptrFrameToAdd Pointer to the received Trace-message
 *  @param channelReceivedFrom Number identifying the channel the Trace-message was received from.
 */
void     Trace_AddFrameToRXBuffer                     (TRACE_FRAME * ptrFrameToAdd, uint8_t channelReceivedFrom);




/************************************************************************************************/
/* Function Name     :  Trace_Message_Received( )                                               */
/* Description       :  This function is called from an LogPhyReceivedFrameEvent Event,       */
/*                      if a Trace-frame was received                                           */
/************************************************************************************************/
/**
 *  @brief This function is called from an LogPhyReceivedFrameEvent Event, if a Trace-frame was received.
 *  The next message from the Receive-Buffer is processed with the rights of the channel it was received from.
 *  If the message is not an Trace-message (first bit is 0) the message is discarded.
 */
void     Trace_Message_Received                       (void);









/************************************************************************************************/
/* Function Name     :  void Trace_registerCallbackFunction(uint32_t FunctionID,
                              void (* PointerToFunction)(uint8_t, char *),
                              CallbackFunctionElement * PointerToCallbackFunctionElement,
                              char * PointerToBuffer,
                              uint8_t SizeOfBuffer),                                             */
/* Description       :  This function is called to add an element to the callback-function      */
/*                      linked list                                                             */
/************************************************************************************************/
/**
 *  @brief This function is called to add an element to the callback-function linked list
 *  The functionID, pointer to the function, pointer to a CallbackFunctionElement,
 *  pointer to the parameter-buffer are added as a new element to the linked list of callback
 *  functions.
 *  @param FunctionID The Function ID of the callback-function, that is registered (this ID is generated by the PERL scrip tr_parse.pl)
 *  @param PointerToFunction points to the function, that is should be registered as callback-function
 *  @param PointerToCallbackFunctionElement   points to a CallbackFunctionElement, which is used to register the callback-function within a linked list, containing all registered callback-functions.
 *         This element MUST NOT be manipulated by the registrator, since this could corrupt the linked-list of registered callbacks !!
 *  @param PointerToBuffer points to the parameter-buffer, which is passed by the registration
 *  @param SizeOfBuffer is the size of the parameter-buffer, which is passed by the registration
 */
void Trace_registerCallbackFunction(uint32_t FunctionID,                                            \
                                    void (* PointerToFunction)(uint8_t const, char const*),                    \
                                    CallbackFunctionElement * PointerToCallbackFunctionElement);





 /**
  * @}
  */

extern void trace_transmit_complete( void );
extern void trace_received_frame( void );
extern void trace_tx_buffer_lock( void );
extern void trace_tx_buffer_release( void );
extern void Task_Trace( void const*p_arg );
extern boolean HAL_Cpu_InterruptActive( void );
extern boolean HAL_Cpu_InterruptsDisabled( void );


#endif /* _TRACE_SERVER_H_ */



