#ifndef _TRACE_FORMATS_H_
#define _TRACE_FORMATS_H_



 /** Size of one Trace-Frame in byte  */
 #define TRACE_FRAME_SIZE         8

 /** Number of data bytes one message Trace-Frame contains  */
 #define TRACE_FRAME_VALUE_SIZE   4

 /** When clearing a Trace-Frame, the frame is filled with this letter  */
 #define TRACE_CLEAR_LETTER       0x00




 /** defines for the headers of the different trace-message types
  */

 /** A Trace-Message starts with this header  */
 #define TRACE_MESSAGE_HEADER             0x80

 /** A Trace-Status-frame starts with this header  */
 #define TRACE_STATUS_HEADER              0x90

 /** A Trace-ServerData-frame starts with this header  */
 #define TRACE_SERVER_DATA_HEADER         0xA0

 /** A Trace-ClientData-frame starts with this header  */
 #define TRACE_CLIENT_DATA_HEADER         0xB0

 /** A Trace-Data-frame starts with this header */
 #define TRACE_DATA_HEADER                0xC0

 /** A Trace-ClientData_Ack-frame starts with this header  */
 #define TRACE_CLIENT_DATA_ACK_HEADER     0xD0

 /** A Trace-ModuleMask-frame starts with this header  */
 #define TRACE_MODULE_MASK_HEADER         0xE0

  /** This Header is reserved for future use   */
 #define TRACE_FUTURE_USE_HEADER          0xF0






 /** defines for occurrence of a status message   */

 /** A Status-frame signalling "Target has had a reset" has this occurence-value  */
 #define STATUS_MSG_RESET                 0

 /** A Status-frame signalling "Trace-Buffer has crossed its critical level" has this occurence-value  */
 #define STATUS_MSG_BUFFERLEVEL_CRITICAL  1

 /** A Status-frame signalling "Trace-Buffer is full" has this occurence-value  */
 #define STATUS_MSG_BUFFER_FULL           2

 /** A Status-frame signalling "Systemtime of the target" has this occurence-value  */
 #define STATUS_MSG_SYSTEM_TIME           3

 /** A Status-frame signalling "Trace-Buffer starting a dump" has this occurence-value  */
 #define STATUS_MSG_DUMP_START            4

 /** A Status-frame signalling "Trace-Buffer dump is finished" has this occurence-value  */
 #define STATUS_MSG_DUMP_STOP             5




 /** Defines for occurrence of a Client-Data message
  */

 /** A Client-Data message signalling to get a variable has this type  */
 #define TR_CLIENTDATA_GET_ID      0x01


 /** A Client-Data message signalling to set a variable has this type  */
 #define TR_CLIENTDATA_SET_ID      0x02


 /** A Client-Data message signalling to get a single bit has this type  */
 #define TR_CLIENTDATA_BIT_GET_ID  0x03


 /** A Client-Data message signalling to set a single bit has this type  */
 #define TR_CLIENTDATA_BIT_SET_ID  0x04


 /** A Client-Data message signalling to send a event to a task has this type  */
 #define TR_CLIENTDATA_SEND_EVENT_ID  0x05


 /** A Client-Data message signalling to execute a Trace_Callback function has this type  */
 #define TR_CLIENTDATA_CALL_FUNCTION_ID  0x06





 /** defines for occurrence of a Client-Data-Ack message
  */

 /** A Client-Data-Ack message signalling to get a variable has this type  */
 #define  TR_CLIENTDATA_GET_ACK     0x01

 /** A Client-Data-Ack message signalling to set a variable has this type  */
 #define  TR_CLIENTDATA_SET_ACK     0x02

 /** A Client-Data-Ack message signalling to get a single bit has this type  */
 #define  TR_CLIENTDATA_BIT_GET_ACK 0x03

 /** A Client-Data-Ack message signalling to set a single bit has this type  */
 #define  TR_CLIENTDATA_BIT_SET_ACK 0x04

 /** A Client-Data message signalling to send a event to a task has this type  */
 #define TR_CLIENTDATA_SEND_EVENT_ID_ACK  0x05

 /** A Client-Data message signalling to execute a Trace_Callback function has this type  */
 #define TR_CLIENTDATA_CALL_FUNCTION_ID_ACK  0x06




 /** defines for TraceIDs that have a special meaning
  */

 /** Special Trace-ID used to transmit the Trace-version  */
 #define TRACE_ID_VERSION_INFO            0x0000

 /** Special Trace-ID used if transmitting Client-Data-Ack larger the 4 byte  */
 #define TRACE_ID_LARGE_DATA              0xFFFF

 /** Special Trace-ID used for transmitting old printf_() traces   */
 #define TRACE_ID_PRINTF                  0xFFFE


 /** defines for data transfer
  */

 /** Maximum number of bytes transmitted in one Data-Frame  */
 #define BYTES_PER_DATA_FRAME            6

 /** Maximum number of frames transmitted with one TRACW_MEM call  */
 #define MAX_FRAMES_PER_TRACE_MEM        42

 /** Maximum number of bytes transmitted with one TRACW_MEM call  */
 #define MAX_BYTES_PER_TRACE_MEM         (MAX_FRAMES_PER_TRACE_MEM * BYTES_PER_DATA_FRAME)




 /** defines for bit masks (because not all fields in the format need a whole byte)
  */

 /** Bit-mask to mask the 12-bit time-offset out of 16        (binary: 0000111111111111)   */
 #define TIME_OFFSET_BIT_MASK          0xFFF

 /** Bit-mask to mask the 4-bit message-header out of 8 bit   (binary: 11110000)   */
 #define HEADER_BIT_MASK               0xF0

 /** Bit-mask to mask the 6-bit sequence number out of 8 bit  (binary: 00001111)   */
 #define SEQUENCE_NUMBER_BIT_MASK      0xF

 /** Bit-mask to mask the first bit out of 8 bit  (binary: 10000000)  */
 #define NON_TRACE_FRAME_BIT_MASK      0x80

 /** Bit-mask to mask the "ignore IPC commands" bit in the module mask frame out of 8bit        (binary: 00000001)   */
 #define IGNORE_IPC_COMMANDS_BIT_MASK          0x1

 /** Bit-mask to mask the disable buffer dump bit in the module mask frame out of 8bit        (binary: 00000010)   */
 #define DISABLE_BUFFER_FUMP_BIT_MASK          0x2



/** Message-formats transmitted from target to trace-client
 */


/** Trace-Message frame structure
 */
typedef struct {
   uint8_t  Trace_Header_u8;       /**  Header of the Trace-Message frame */
   uint8_t  TraceTimeOffset_u8;    /**  Time-offset of the Trace-Message to the last transmitted Systemtime */
   uint16_t TraceID_u16;           /**  Trace-ID of the Trace-Message frame */
   uint8_t  Val[4];                /**  Data-Value(s) of the Trace-Message frame */

}TR_TRACE_MSG;


/** Trace-Version-Info frame structure
 */
typedef struct {
   uint8_t  Trace_Header_u8;       /**  Header of the Trace-Version-Info frame */
   uint8_t  ChannelInfo_u8;        /**  Channel-number of the currently used channel */
   uint16_t TraceID_u16;           /**  Trace-ID of the Trace-Version-Info frame; must be 0x00 */
   uint32_t VersionNo_u32;         /**  Trace-Version target-build */
}TR_VERSION_INFO;


/** Trace-Status frame structure
 */
typedef struct {
   uint8_t  Trace_Header_u8;       /**  Header of the Trace-Status frame */
   uint8_t  TraceTimeOffset_u8;    /**  Time-offset of the Trace-Status frame to the last transmitted Systemtime */
   uint8_t  MessageOccurrence_u8;  /**  Determines the type of the Trace-Status frame */
   uint8_t  Messagecount_u8;       /**  Number of Trace-frames successfully transmitted till this Trace-Status frame ( overflow at 255 ) */
   uint32_t SystemTime_u32;        /**  Current absolut systemtime */
}TR_STATUS_MSG;



/** Trace-Server-Data frame structure
 */
typedef struct {
   uint8_t  Trace_Header_u8;       /**  Header of the Trace-Server-Data  frame */
   uint8_t  TraceTimeOffset_u8;    /**  Time-offset of the Trace-Server-Data frame to the last transmitted Systemtime */
   uint16_t TraceID_u16;           /**  Trace-ID of the Trace-Server-Data frame */
   uint8_t  DataLength_u8;         /**  Length of the data trailing this Trace-Server-Data frame */
   uint8_t  Trace_DataID_u8;       /**  DataID of the data trailing this Trace-Server-Data frame */
}TR_SERVER_DATA;


/** Trace-Data frame structure
 */
typedef struct {
   uint8_t Trace_Header_u8;        /**  Header of the Trace-Data frame */
   uint8_t Trace_DataID_u8;        /**  DataID of the data trailing this Trace-Data frame */
   uint8_t Val[6];                 /**  Data transmitted with this Trace-Data frame */
}TR_DATA;



/** Trace-Client-Data-Ack frame structure
 */
typedef struct {
   uint8_t Trace_Header_u8;        /**  Header of the Trace-Client-Data-Ack frame */
   uint8_t TraceTimeOffset_u8;     /**  Time-offset of the Trace-Client-Data-Ack frame to the last transmitted Systemtime */
   uint8_t CommandType_u8;        /**  Type of this Trace-Client-Data-Ack frame */
   uint8_t DataLength_u8;          /**  Length of the data set/get with this Trace-Client-Data-Ack frame */
   uint8_t Val[4];                 /**  Data-Value(s) of the Trace-Client-Data-Ack frame */
}TR_CLIENT_DATA_ACK;


/** Trace-ModuleMask frame structure
 */
typedef struct {
   uint8_t Trace_Header_u8;        /**  Header of the Trace-ModuleMask frame */
   uint8_t TraceOptions_u8;        /**  Options for the current channel (precedence over SH4, etc.) */
   uint8_t Channel_u8;             /**  Channel over which to transmitt Trace-Frames */
   uint8_t emptyByte_u8;           /**  not used ; should be 0x00 */
   uint32_t moduleMask_u8;         /**  The module mask which should be set with this Trace-ModuleMask frame */
}TR_MODULE_MASK;


/** Trace-Frame Union, unioning all Trace-message types
 */
typedef union {
       TR_TRACE_MSG        trMSG;             /** A Trace-Frame may be contain a regular Trace-message */
       TR_VERSION_INFO     trInfo;            /** A Trace-Frame may be contain a Trace-Version message */
       TR_STATUS_MSG       trStatus;          /** A Trace-Frame may be contain a regular Trace-Status message */
       TR_SERVER_DATA      trServerData;      /** A Trace-Frame may be contain a regular Trace-Server-Data message */
       TR_DATA             trData;            /** A Trace-Frame may be contain a regular Trace-Data message */
       TR_CLIENT_DATA_ACK  trClientAck;       /** A Trace-Frame may be contain a regular Trace-Client-Data message */
       TR_MODULE_MASK      trModuleMask;      /** A Trace-Frame may be contain a regular Trace-ModuleMask message */
       uint8_t    	 	   trRawData[8];      /** A Trace-Frame always consists of 8 bytes */
       uint32_t			   trRawData_32[2];	
}TRACE_FRAME;




/** Callback function structure, used to store information, when a callback function is registered
 */
struct CallbackFunctionElement_struct{
        uint32_t  functionID;                               /** The ID, that is asotiated with this element */
        void (*functionPointer)(uint8_t const, char const*);           /** The pointer to the function, that is asotiated with this element */
        struct CallbackFunctionElement_struct * next;     /** The pointer to the next callback function element */
};

typedef struct CallbackFunctionElement_struct CallbackFunctionElement;



 /**
  * @}
  */


#endif /* _TRACE_FORMATS_H_ */














