

#include "Trace_cfg.h"

#include "Trace_api.h"

#include "Trace_Dispatcher.h"       /*For dispatcher-specific defines*/
#include "Trace_Buffer.h"           /* For transmitting messages */
//#include "Trace_ModuleVariables.h"        /* for modul variables */
//#include "Trace_Macros.h"

#include "trace_phy.h"

#include "Trace_Common.h"
#include "Trace_Server.h"


//#include <trswitch.h>

/** state of the current transmission  */
static eTR_SEND_STATE tr_send_state;

/***********************************************************************************************/
/* Function Name    :   Trace_DispatcherInit()                                                 */
/* Description      :   This function is initialized the Dispatcher the channel                */
/*                      preconfigured in Trace.cfg                                             */
/***********************************************************************************************/
/** @brief This function initializes the Dispatcher
*/
void Trace_DispatcherInit()
{
    TracePhy_Init();
    tr_send_state = SEND_OK;
}



/***********************************************************************************************/
/* Function Name    :   Trace_Dispatcher_ResetConnections()                                    */
/* Description      :   This function resets the connection state of the configured interfaces */
/***********************************************************************************************/
/** @brief This function resets the connection state of the configured interfaces
*/
void Trace_Dispatcher_ResetConnections()
{
    TracePhy_ResetConnection();
}


/***********************************************************************************************/
/* Function Name    :   Trace_Dispatcher_SendNextFrameFromBuffer()                                        */
/* Description      :   This function gets the next frame from buffer and transmits           */
/*                      it via the current channel (IPC, CAN, UART, etc.)                      */
/***********************************************************************************************/
/** @brief This function gets the next frame from buffer and transmits it via the current channel (IPC, CAN, UART, etc.)
*  If the current channel is IPC first the dump state is checked: If dump is started a start-dump-message is transmitted,
*  if dump has finished a stop-dump-message is transmitted.
*/
void Trace_Dispatcher_SendNextFrameFromBuffer(void)
{
    TRACE_FRAME tmpFrame;

	if  ( tr_send_state == SEND_OK )
	{
		eTR_SEND_STATE tx_state = SEND_OK;
		
		while ( tx_state == SEND_OK && TraceBuffer_GetFrame(&tmpFrame) )
	    {
			tr_send_state = SEND_PENDING;
			
	    	Trace_SendNextFrameCounter++;
	    	tx_state = TracePhy_SendFrame(&tmpFrame);
			if(tx_state==SEND_OK)
			{
				//Trace_Dispatcher_TransmitComplete();
				tr_send_state = SEND_OK;
				Trace_MessageCount_u8++;
			}
	    }
	}
}




/***********************************************************************************************/
/*                                       INTERNAL FUNCTION                                     */
/***********************************************************************************************/
/* Function Name    :  Trace_Dispatcher_TransmitComplete()                                     */
/* Description      :  This function is called by the module of the current used channel, if   */
/*                     current transmit has been completed.                                    */
/***********************************************************************************************/
/** @brief This function is called by the module of the current used channel, if current transmit has been completed.
*  A LogPhyTransmitCompleteEvent() is called to generate an event signalling to the Trace-Server: the last transmission has finished.
*/

void Trace_Dispatcher_TransmitComplete(void)
{
	tr_send_state = SEND_OK;
	

	//Trace_IncreaseMessageCountBy1();
	Trace_MessageCount_u8++;

	//Trace_Dispatcher_SendNextFrameFromBuffer();
	LogPhyTransmitCompleteEvent();
}



/***********************************************************************************************/
/*                                       INTERNAL FUNCTION                                     */
/***********************************************************************************************/
/* Function Name    :  Trace_Dispatcher_AddFrameToFifoRX()                                     */
/* Description      :  This function passes the passed trace-frame to the RXBuffer in the      */
/*                     Trace-Server                                                            */
/***********************************************************************************************/
/** @brief This function forwards the passed trace-frame to the RXBuffer in the Trace-Server
*/
void Trace_Dispatcher_AddFrameToFifoRX(TRACE_FRAME * pFrameToAdd, uint8_t channel)
{
    /* if Trace is being disabled, discard all received Trace-Frames */
    if(Trace_EnabledState)
    {
        CPU_SR_ALLOC();
        /* disable interrupt */
        CPU_CRITICAL_ENTER();

        Trace_AddFrameToRXBuffer(pFrameToAdd, channel);

        /* restore interrupt */
        CPU_CRITICAL_EXIT();
    }
}



/***********************************************************************************************/
/*                                       INTERNAL FUNCTION                                     */
/***********************************************************************************************/
/* Function Name    :  Trace_Dispatcher_TraceFrameReceived()                                   */
/* Description      :  This function invokes an LogPhyReceivedFrameEvent to indicate   */
/*                     to the Trace-Server that a message was received                         */
/***********************************************************************************************/
/** @brief This function invokes an LogPhyReceivedFrameEvent to signal to the Trace-Server: a message was received
*/
void Trace_Dispatcher_TraceFrameReceived()
{
    /* if Trace is being disabled, discard all received Trace-Frames */
    if(Trace_EnabledState)
    {
        LogPhyReceivedFrameEvent();
    }
}


