
#include "Trace_api.h"
#include "Trace_Buffer.h"    /*For buffer-specific defines*/
#include "Trace_Common.h"
#include "Trace_Server.h"
#include "Trace_Dispatcher.h"


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/**  Macro defining the threshold, when to reset the buffer level-critical state.  */
#define BUFFER_CRITICAL_RESET_THRESHOLD  20

/**  Macro defining the threshold, when to reset the buffer full state.  */
#define BUFFER_FULL_RESET_THRESHOLD   10

/** Macro defining the number of buffer entries, that match the critical threshold level .*/
#define BUFFER_NUMBER_OF_ENTRIES_FOR_LEVEL_CRITICAL   ((uint16_t)( (TRACE_FIFO_SIZE * TRACE_BUFFER_LEVEL_CRITICAL_THRESHOLD ) / 100 ))


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

/**  Flag signalling that a Buffer-state-critical message has been added to buffer yet.  */
uint8_t           BufferlevelCriticalFlag_u8;


/**  Buffer to collect Trace-frames to be transmitted  */
NO_INIT_DATA static struct
{
    uint16_t 	used;
    uint16_t	rx_idx;
    uint16_t	wr_idx;
    TRACE_FRAME trFrame[TRACE_FIFO_SIZE];       /** The Trace-frame to transmit */
} trace_fifo;

/** Trace-Buffer status message (which is stored separately from regular Trace-Buffer */
NO_INIT_DATA static TRACE_FRAME   trBufferStatusFrame;


/** Flag, indicating, that a Buffer status frame must be transmitted next. */
NO_INIT_DATA static uint8_t Trace_BufferStatusFrame_Flag_u8;


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void 	TraceBuffer_CheckLimit(void);
static void		AddSystemTmToTraceBuffer(void);

static void TraceBuffer_AddBufferStateMessage(uint8_t state);

/*
1>tx status frame
2>tx criticak frame
3>tx overrun frame
4>tx buffer full frame
*/
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void TraceBuffer_Init(void)
{
    eTraceBufferState            	= eTRACE_BUFFER_STATE_OK;
    eTraceDumpState              	= eTRACE_DUMP_STATE_NODUMP;
    BufferlevelCriticalFlag_u8   	= 0;
    Trace_BufferStatusFrame_Flag_u8 = 0;

    trace_fifo.used 	= 	0x00;
	trace_fifo.wr_idx	=	0x00;
	trace_fifo.rx_idx	=	0x00;

    Trace_NoOfBufferFullStates   = 0;
    Trace_NoOfBufferCriticalStates = 0;
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
uint16_t TraceBuffer_GetFifoAvblNum(void)
{
    return TRACE_FIFO_SIZE - trace_fifo.used;
}


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void	TraceBuffer_AddFrame(TRACE_FRAME *tr)
{
	boolean actv_trace_task = FALSE;

    /* ++++++ start of critical section ++++++ */
    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();

   	uint32_t FrozenSystemTime_u32 = HalOS_GetKernelTick();
   	
    if ( trace_fifo.used == 0x00 )
    {	// activate trace server task 
    	actv_trace_task = TRUE;
    }

    TraceBuffer_CheckLimit();

    Trace_BufferRequestCounter++;

    if ( trace_fifo.used < TRACE_FIFO_SIZE )
    {
        // at least one frame
        uint32_t TimeDifference_u32 	 =  (FrozenSystemTime_u32 - Trace_LastTransmittedSystemtime_u32) ;
        /* check, if a Trace-message updating referenced systemtime must be transmitted */
        if (TimeDifference_u32 >= (uint32_t)TRACE_SYSTEMTIME_PERIOD )
        {
            Trace_LastTransmittedSystemtime_u32 =  FrozenSystemTime_u32;
            /* tx system time */
            AddSystemTmToTraceBuffer();
        }

        /* Calculate the timeoffset to write it into the Trace-Message(s) */
        uint16_t timeOffset_u16 = (uint16_t)(FrozenSystemTime_u32 - Trace_LastTransmittedSystemtime_u32) & TIME_OFFSET_BIT_MASK ;

        //TRACE_FRAME *tr = (TRACE_FRAME*)msg;
        switch ( tr->trMSG.Trace_Header_u8 & 0xF0 )
        {
        case TRACE_MESSAGE_HEADER:
        	if ( tr->trMSG.TraceID_u16 != TRACE_ID_VERSION_INFO ) // trace version message no need time offset
        	{
	            //tr->trMSG.Trace_Header_u8		&= 	0xF0;
	            tr->trMSG.Trace_Header_u8    	|= 	(uint8_t)(timeOffset_u16 >> 8);
	            tr->trMSG.TraceTimeOffset_u8	=	(uint8_t) timeOffset_u16;
            }
            break;

        case TRACE_STATUS_HEADER:	// not include StateMessage	
        	//STATUS_MSG_SYSTEM_TIME,STATUS_MSG_DUMP_START
        	if ( tr->trStatus.MessageOccurrence_u8 == STATUS_MSG_DUMP_START || tr->trStatus.MessageOccurrence_u8 == eTRACE_DUMP_STATE_ENDDUMP )
        	{
	            //tr->trStatus.Trace_Header_u8 		&= 	0xF0;
	            tr->trStatus.Trace_Header_u8 		|= 	(uint8_t)(timeOffset_u16 >> 8);
	            tr->trStatus.TraceTimeOffset_u8		=	(uint8_t) timeOffset_u16;
        	}
            tr->trStatus.Messagecount_u8		= 	Trace_MessageCount_u8;
            ///tr->trStatus.SystemTime_u32			=	FrozenSystemTime_u32;
            break;

        case TRACE_SERVER_DATA_HEADER:
            //tr->trServerData.Trace_Header_u8	&= 	0xF0;
            tr->trServerData.Trace_Header_u8 	|= 	(uint8_t)(timeOffset_u16 >> 8);
           	tr->trStatus.TraceTimeOffset_u8		=	(uint8_t) timeOffset_u16;
            break;

		case TRACE_CLIENT_DATA_ACK_HEADER:
			//tr->trClientAck.Trace_Header_u8		&= 	0xF0;
            tr->trClientAck.Trace_Header_u8 	|= 	(uint8_t)(timeOffset_u16 >> 8);
           	tr->trClientAck.TraceTimeOffset_u8	=	(uint8_t) timeOffset_u16;
			break;

		case TRACE_MODULE_MASK_HEADER:	// from client			
		case TRACE_DATA_HEADER:			// trace server data, no time offset
        case TRACE_CLIENT_DATA_HEADER:	// trace message is data 
		default:
        	break;
        	
        }

        trace_fifo.trFrame[trace_fifo.wr_idx].trRawData_32[0] =	tr->trRawData_32[0];
        trace_fifo.trFrame[trace_fifo.wr_idx].trRawData_32[1] =	tr->trRawData_32[1];
        trace_fifo.wr_idx ++;
        trace_fifo.used ++;
        if ( trace_fifo.wr_idx >= TRACE_FIFO_SIZE )
        {
            trace_fifo.wr_idx = 0x00;
        }


        if ( eTraceBufferState == eTRACE_BUFFER_STATE_OVERSHOOT )
        {
            /* Adding another Trace-Frame would cause the Buffer to overflow. Add a Buffer-full Trace-message. */
            TraceBuffer_AddBufferStateMessage(STATUS_MSG_BUFFER_FULL);
            // set the internal buffer state.
            eTraceBufferState = eTRACE_BUFFER_STATE_FULL;
        }
        else if( eTraceBufferState == eTRACE_BUFFER_LEVEL_CRITICAL )
        {
        	if (!BufferlevelCriticalFlag_u8)
	        {
	            TraceBuffer_AddBufferStateMessage(STATUS_MSG_BUFFERLEVEL_CRITICAL);;
	            BufferlevelCriticalFlag_u8 = 1;
	        }
        }
        else{}
    }
    else
    {}

	CPU_CRITICAL_EXIT();

	if ( actv_trace_task )
	{	// activate trace task 
		//Trace_Dispatcher_SendNextFrameFromBuffer();
		LogPhyTransmitCompleteEvent();
	}
}


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
boolean	TraceBuffer_GetFrame(TRACE_FRAME *tr)
{

	boolean msg_avbl = FALSE;
	
	CPU_SR_ALLOC();
	CPU_CRITICAL_ENTER();
	
	/* if a buffer status-message must be transmitted next, copy it to the passed pointer-address  */
    if (1 == Trace_BufferStatusFrame_Flag_u8)
    {        
		tr->trRawData_32[0] = trBufferStatusFrame.trRawData_32[0];
		tr->trRawData_32[1] = trBufferStatusFrame.trRawData_32[1];

        msg_avbl = TRUE;

        Trace_BufferStatusFrame_Flag_u8 = 0;
    }
    else /* get next frame from buffer as regular */
	{
		if ( trace_fifo.used )
		{
			trace_fifo.used --;
			msg_avbl = TRUE;

			tr->trRawData_32[0] = trace_fifo.trFrame[trace_fifo.rx_idx].trRawData_32[0];
			tr->trRawData_32[1] = trace_fifo.trFrame[trace_fifo.rx_idx].trRawData_32[1];
			trace_fifo.rx_idx++;
			if ( trace_fifo.rx_idx >= TRACE_FIFO_SIZE )	
			{
				trace_fifo.rx_idx = 0x00;
			}
		}
		
		/* The window for buffer-level critical condition is BUFFER_CRITICAL_RESET_THRESHOLD Trace messages */
		/* Reset the Buffer-state, if the buffer-level is below this limit */
		if( (eTraceBufferState == eTRACE_BUFFER_LEVEL_CRITICAL) && ( trace_fifo.used < ( BUFFER_NUMBER_OF_ENTRIES_FOR_LEVEL_CRITICAL - BUFFER_CRITICAL_RESET_THRESHOLD) ))
		{
			eTraceBufferState = eTRACE_BUFFER_STATE_OK;
			BufferlevelCriticalFlag_u8 = 0;
		}
		else
		{
			/* The window for buffer-full condition is BUFFER_FULL_RESET_THRESHOLD Trace messages*/
			/* Reset the Buffer-state, if the buffer-level is below this limit */
			if( (eTraceBufferState == eTRACE_BUFFER_STATE_FULL) && ( trace_fifo.used < (TRACE_FIFO_SIZE - BUFFER_FULL_RESET_THRESHOLD)) )
			{
				eTraceBufferState = eTRACE_BUFFER_STATE_OK;
			}
		}		
	}


    
	CPU_CRITICAL_EXIT();
    
	return msg_avbl;
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
boolean	TraceBuffer_CheckBufferOk(void)
{
	boolean ok_ = TRUE;

	if ( trace_fifo.used >= TRACE_FIFO_SIZE || trace_fifo.rx_idx >= TRACE_FIFO_SIZE || trace_fifo.wr_idx >= TRACE_FIFO_SIZE )
	{
		ok_ = FALSE;
	}
	return ok_;
}
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void	AddSystemTmToTraceBuffer(void)
{
    if ( eTraceBufferState == eTRACE_BUFFER_STATE_OK )
    {
        // add system time to buffer now
        Trace_AddedFrameToBufferCounter++;

        TRACE_FRAME trFrame;
        trFrame.trStatus.Trace_Header_u8         = TRACE_STATUS_HEADER;
        trFrame.trStatus.TraceTimeOffset_u8      = 0x00;
        trFrame.trStatus.MessageOccurrence_u8    = STATUS_MSG_SYSTEM_TIME;
        trFrame.trStatus.Messagecount_u8         = Trace_MessageCount_u8;
        trFrame.trStatus.SystemTime_u32          = Trace_LastTransmittedSystemtime_u32;

        trace_fifo.trFrame[trace_fifo.wr_idx].trRawData_32[0] =	trFrame.trRawData_32[0];
        trace_fifo.trFrame[trace_fifo.wr_idx].trRawData_32[1] =	trFrame.trRawData_32[1];

        trace_fifo.wr_idx ++;
        trace_fifo.used ++;

        if ( trace_fifo.wr_idx >= TRACE_FIFO_SIZE )
        {
            trace_fifo.wr_idx = 0x00;
        }
    }
}


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
/** @brief This function determines whether it is possible to log information into the buffer.
 *  @param Length_u8 amount of data to check space for.
 *  @return The buffer-state when adding data with the size of the parameter Length_u8 to the Trace-buffer
 */
static void TraceBuffer_CheckLimit(void)
{
    /* Check, if adding another Trace-Frame would cross any warning-threshold */
    if( eTRACE_BUFFER_STATE_OK == eTraceBufferState )
    {
        /* Check, if adding another Trace-Frame would cross the buffer-level-critical threshold */
        if ( trace_fifo.used >= BUFFER_NUMBER_OF_ENTRIES_FOR_LEVEL_CRITICAL )
        {
            Trace_NoOfBufferCriticalStates++;
            eTraceBufferState = eTRACE_BUFFER_LEVEL_CRITICAL;
        }
    }

    if( eTRACE_BUFFER_LEVEL_CRITICAL == eTraceBufferState )
    {
        /* Check, if adding another Trace-Frame would lead to a buffer-overflow */
        if( trace_fifo.used >= TRACE_FIFO_SIZE-1 )
        {
            Trace_NoOfBufferFullStates++;
            eTraceBufferState = eTRACE_BUFFER_STATE_OVERSHOOT;
        }
    }
}

/***********************************************************************************************/
/* Function Name    :   TraceBuffer_AddBufferStateMessage()                                   */
/* Description      :   This function adds the Buffer-Status message passes as parameter to    */
/*                      the Trace-buffer                                                       */
/***********************************************************************************************/
/** @brief This function adds a Buffer-Status message to the Trace-buffer.
 *  @param state Type of state-message to add.
 */

static void TraceBuffer_AddBufferStateMessage(uint8_t state)
{
    uint16_t timeOffset_u16;
    uint32_t timeOffset_u32;
    /*Explicitly use a 32 bit variable to compute the difference between current time-stamp
      and previous time-stamp. This ensures that we use 32-bit subtraction here, instead of
      16-bit subtraction.*/
    timeOffset_u32 = HalOS_GetKernelTick() - Trace_LastTransmittedSystemtime_u32;
    timeOffset_u16 = (uint16_t)timeOffset_u32 & TIME_OFFSET_BIT_MASK ;

    /* create a buffer-state Trace-Frame with state passed as parameter state */
    trBufferStatusFrame.trStatus.Trace_Header_u8              = ( TRACE_STATUS_HEADER  | (timeOffset_u16 >> 8) );
    trBufferStatusFrame.trStatus.TraceTimeOffset_u8           = (uint8_t) timeOffset_u16;
    trBufferStatusFrame.trStatus.MessageOccurrence_u8  		  = state;
    trBufferStatusFrame.trStatus.Messagecount_u8              = Trace_MessageCount_u8;
    trBufferStatusFrame.trStatus.SystemTime_u32               = 0x0000;

    Trace_BufferStatusFrame_Flag_u8 = 1;
}




