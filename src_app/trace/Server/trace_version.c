
#include "std_type.h"
#include "log_cfg.h"

#if (defined (LOG_USE_EXT_VERSION)  &&  (LOG_USE_EXT_VERSION > 0))
#include LOG_GET_EXT_VERSION_H
//LOG_GET_EXT_VERSION_H
#endif

#ifdef SW_BUILD_TM_AVBL
#include "build_tm.h"
#endif

uint32_t Trace_GetTraceVersion(void)
{
#if (defined (LOG_USE_EXT_VERSION)  &&  (LOG_USE_EXT_VERSION > 0)) 
	uint32 version = 0x00;
	if ( !LOG_GET_EXT_VERSION(&version) )
	{
#ifdef SW_BUILD_TM_AVBL
		version = BUILD_TM_LINUX;
#endif		
	}
	return version;
#else	// ! LOG_USE_EXT_VERSION && LOG_USE_EXT_VERSION

#ifdef SW_BUILD_TM_AVBL
	return BUILD_TM_LINUX;
#else 
	return 0;
#endif
#endif 	//	{LOG_USE_EXT_VERSION && LOG_USE_EXT_VERSION}
}

uint32_t Ver_GetSwBuildTm(void)
{
#ifdef SW_BUILD_TM_AVBL
	return BUILD_TM_MS;
#else
	return 0;
#endif 
}




