#ifndef _TRACE_INTERNAL_H_H_
#define _TRACE_INTERNAL_H_H_



/** NOTE : This cfg file contains declarations of global variables! It was done only to make the source file look cleaner.
****DO NOT INCLUDE THIS CFG FILE IN ANY OTHER SOURCE FILE.****
*/


#include "Trace_Formats.h"

/***********************************************************************************************/
/* CHANNELS which may be used to trace messages.                                               */
/***********************************************************************************************/
/** Defining the number for the serial-interface  */
#define TRACE_SERIAL_CHANNEL     0x01

/** Defining the number for the CAN0-interface */
#define TRACE_CAN0_CHANNEL       0x02


/** Defining the number for the CAN1-interface  */
#define TRACE_CAN1_CHANNEL       0x03


/** Defining the number for the IPC-interface */
#define TRACE_IPC_CHANNEL        0x04


/** Defining the number for a project specific interface */
#define TRACE_PROJECTSPECIFIC_CHANNEL        0x05


/** Defining the number for the Dummy-interface; used if UART interface is de-initialized */
#define TRACE_DUMMY_CHANNEL        0x06



/** Defining period in ms in which the Systemtime must be transmitted. */
#define TRACE_SYSTEMTIME_PERIOD        1000



/** A structure describing an element of the RXFifo in the Trace-Server; in this Fifo the received Trace-messages are buffered
 */

typedef struct{
   TRACE_FRAME trFrame;       /** The received Trace-frame */
   //uint8_t       RXChannel;     /** The interface-number, the Trace-frame was received from */
} RXFifoEntry;


/** A structure describing an element of the TXFifo
 */
typedef struct{
   TRACE_FRAME trFrame;       /** The Trace-frame to transmit */
   uint8_t elementReadyFlag;    /** flag describing, if the element is has been written */
} TXFifoEntry;


/** A structure describing the state of the Trace-Buffer
 */
typedef enum {
   eTRACE_BUFFER_STATE_OK = 0,         /** the Trace-Buffer state is OK; Trace-frames may added */
   eTRACE_BUFFER_STATE_OVERSHOOT,      /** adding Trace-Frames to Trace-buffer would cause an overshoot */
   eTRACE_BUFFER_STATE_FULL,           /** Trace-buffer is full; no more frames may be added */
   eTRACE_BUFFER_LEVEL_CRITICAL        /** Trace-Buffer has crossed the critical level */
}e_TRACE_BUFFER_STATE;


/** A structure describing the state of a buffer-dump
 */
typedef enum {
	eTRACE_DUMP_STATE_NODUMP = 0,          /** Trace-Buffer has crossed the critical level */
	eTRACE_DUMP_STATE_DUMP,                /** dump has already begun and is still in progress */
	eTRACE_DUMP_STATE_STARTDUMP,           /** a buffer-dump must be started */
	eTRACE_DUMP_STATE_ENDDUMP              /** the buffer-dump has finished */
}e_TRACE_DUMP_STATE;


/** A structure describing the state of a Trace-Frame transmission
 */
typedef enum {
    SEND_OK = 0,     /** Transmission has successfully completed */
    SEND_FAILED,     /** Transmission failed */
    SEND_PENDING     /** Transmission is pending */
}eTR_SEND_STATE;


/** A structure describing the type of module-mask set-operation
 */
typedef enum {
    NORMAL_SET = 0,     /** Set the Module mask to match the passed mask */
    DISABLE_ALL,        /** Set the Module mask disalbe all modules - even MOD_ALWAYS - and store the previous mask */
    RESTORE_ALL         /** Restore the last saved module mask */
}eTR_MODULE_SET_TYPE;



#endif /* _TRACE_INTERNAL_CFG_ */

