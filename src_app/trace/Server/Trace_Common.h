#ifndef _TRACE_COMMON_H_
#define _TRACE_COMMON_H_



#include "Trace_Internal.h"
#include "string.h"           /* needed for memcpy, memset, etc. */




/** Stores current Trace-buffer state  */
extern e_TRACE_BUFFER_STATE eTraceBufferState;

/** Stores current state of a Buffer-dump   */
extern e_TRACE_DUMP_STATE   eTraceDumpState;


/** Trace_Ignore_IPC_commands_u8 signals, if Trace-commands received over IPC should be ignored  */
extern uint8_t  Trace_Ignore_IPC_commands_u8;


/** old module-mask storing the state of all 32 modules */
extern uint32_t Trace_oldModuleMask_u32;


/** Stores absolute Systemtime, that was transmitted last */
extern uint32_t lastTransmittedSystemTime;

/** Variable counting how many times the buffer has been completely filled */
extern uint32_t Trace_NoOfBufferFullStates;


/** Variable counting how many times the critical buffer-level has been crossed  */
extern uint32_t Trace_NoOfBufferCriticalStates;




extern uint8_t TRACE_ClientConnected;
extern uint8_t Trace_EnabledState;
extern uint8_t  Trace_MessageCount_u8;
extern uint8_t  Trace_DataID_u8;
extern uint8_t  Trace_RetriggerTransmission_Flag;
extern uint32_t  Trace_LastTransmittedSystemtime_u32;

extern uint16_t Trace_IPCSendFailedCounter;
extern uint16_t Trace_IPCSendPendingCounter;
extern uint16_t Trace_IPCChannelNotReadyCounter;
extern uint16_t Trace_IPCtxCallbackCounter;
extern uint16_t Trace_SendNextFrameCounter;
extern uint16_t Trace_RemovedFrameCounter;
extern uint16_t Trace_SendFrameFromBufferCounter;
extern uint16_t Trace_DoNotRemoveCounter;
extern uint16_t Trace_BufferRequestCounter;
extern uint16_t Trace_IPCConnCBCounter;
extern uint16_t Trace_AddedFrameToBufferCounter;
extern uint16_t Trace_FrameNotReadyCounter;
extern uint16_t Trace_TraceMacroCalledCounter;
extern uint16_t Trace_RetriggerTransmissionFlagSetCounter;
extern uint16_t Trace_BufferUsage;
extern uint16_t Trace_BufferUsageMax;


/** This macro increases the DataID (used for counting the fragments of a data-transfer) by 1.
 *  If current DataID is 254, the next is not 255, but 0 (reason: only 8 bits used for DataID)
 */
#define Trace_IncreaseDataIDBy1()  \
   ((Trace_DataID_u8 < 254) ? (Trace_DataID_u8++) : (Trace_DataID_u8 = 0))




 /**
  * @}
  */

#endif


