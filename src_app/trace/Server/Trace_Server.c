

#include "Trace_cfg.h"

#include "Trace_api.h"
#include "Trace_Server.h"

#include "Trace_Common.h"

#include "Trace_Buffer.h"
#include "log_cfg.h"
#include "log_handle.h"
#include "Trace_cfg.h"
#include "trace_phy.h"


#if( (1 == TRACE_ALLOW_VARIABLE_GET_SET) || (1 == TRACE_ALLOW_CALLBACK_FUNCTION_CALLS) )
/**  Flag indicating that a ClientData Set Bit - message was received */
uint8_t              SetDataBitFlag_u8;

/**  Flag indicating that a ClientData Set - message was received */
uint8_t              SetDataFlag_u8;

/**  Flag indicating that an error during reception of a sequence of Data-frames occured  */
uint8_t              CommandDataErrorFlag_u8;

/**  Flag indicating that a sequence of Data-frames was received correctly ans the processing of the Set-operation has been completed */
uint8_t              CommandDataCompleteFlag_u8;

/**  Flag indicating that a "invoke Callback function"- message was received */
uint8_t              InvokeCallbackFunctionFlag_u8;
#endif

#if (1 == TRACE_ALLOW_CALLBACK_FUNCTION_CALLS)

/** A linked list, which contains callback-function elements */
CallbackFunctionElement * CallbackFunctionList;

#endif

//NO_INIT_DATA static	uint8_t		Trace_PhysicalChannel_u8;
/** Variable used to check the integrity of the NO-INIT memory area */
NO_INIT_DATA static uint32_t  	Trace_MemoryIntegrityPattern;
/**  Buffer to collect the data of received Data-frames */
NO_INIT_DATA static uint8_t  	CommandDataBuffer[MAX_BYTES_PER_TRACE_MEM + 1];


/** The following Variables should be stored on non-volatile memory */

NO_INIT_DATA static struct
{
    uint8_t 		rx_idx;
    uint8_t 		wr_idx;
    uint8_t 		used;
    RXFifoEntry 	fifo[FIFOSIZERX];
} FifoRx;


/** Macro to calculate the current number of entries in the Receive-queue.
 */

// Test Code !!
//


/***********************************************************************************************/
/*                                       INTERNAL FUNCTION                                     */
/***********************************************************************************************/
/* Function Name    :  Trace_SendVersionInfo()                                                 */
/* Description      :  This function sends the version information to the TraceClient.         */
/*                     The message is passed directly to the dispatcher and not added to Buffer*/
/***********************************************************************************************/
/**
 *  @brief This function adds a message containing Trace-Version to buffer.
 */
void Trace_SendVersionInfo(void)
{
    /* Transmit Version only, if Tracing is enabled*/
    if(Trace_EnabledState)
    {
		TRACE_FRAME tr_frame;
		tr_frame.trMSG.Trace_Header_u8		= TRACE_MESSAGE_HEADER;
		tr_frame.trMSG.TraceTimeOffset_u8	= TracePhy_GetCom();
		tr_frame.trMSG.TraceID_u16			= TRACE_ID_VERSION_INFO;
		*((uint32_t*)(tr_frame.trMSG.Val))	= Trace_GetTraceVersion();
		TraceBuffer_AddFrame(&tr_frame);        
    }
}




/***********************************************************************************************/
/*                                       INTERNAL FUNCTION                                     */
/***********************************************************************************************/
/* Function Name    :  Trace_SendSystemTime()                                                  */
/* Description      :  This function sends current Systemtime to the TraceClient to calculate  */
/*                     the absolut Timestamp with this time and the offset                     */
/***********************************************************************************************/
/**
 *  @brief This function adds a message containing current systemtime to buffer.
 *  In order to calculate the absolute systemtime, this function is called periodically.
 *  It transmitts the current absolut systemtime and the difference to the last transmitted systemtime.
 */
void  Trace_SendSystemTime(void)
{
//   TRACE_FRAME trFrame;

    /* Transmit System-time only, if Tracing is enabled*/
    if(Trace_EnabledState)
    {
		TRACE_FRAME tr_frame;

		CPU_SR_ALLOC();
		CPU_CRITICAL_ENTER();
		
		Trace_LastTransmittedSystemtime_u32       = HalOS_GetKernelTick();

		tr_frame.trStatus.Trace_Header_u8         =  TRACE_STATUS_HEADER;
		tr_frame.trStatus.TraceTimeOffset_u8      =  0;
		tr_frame.trStatus.MessageOccurrence_u8    = STATUS_MSG_SYSTEM_TIME;
		tr_frame.trStatus.Messagecount_u8         = Trace_MessageCount_u8;
		tr_frame.trStatus.SystemTime_u32          = Trace_LastTransmittedSystemtime_u32;
		
		TraceBuffer_AddFrame(&tr_frame);        

		CPU_CRITICAL_EXIT();
    }
}



/***********************************************************************************************/
/*                                       INTERNAL FUNCTION                                     */
/***********************************************************************************************/
/* Function Name    :  Trace_SendResetInfo()                                                   */
/* Description      :  This function sends a notice-message to the TraceClient, to signal the  */
/*                     Client the target has had a reset                                       */
/***********************************************************************************************/
/**
 *  @brief This function adds a message to buffer reporting a reset of the target.
 */
void  Trace_SendResetInfo(void)
{
//   TRACE_FRAME trFrame;
    /* Transmit ResetInfo only, if Tracing is enabled*/
    if(Trace_EnabledState)
    {
		TRACE_FRAME tr_frame;

		CPU_SR_ALLOC();
		CPU_CRITICAL_ENTER();
		
		Trace_LastTransmittedSystemtime_u32       = HalOS_GetKernelTick();

		tr_frame.trStatus.Trace_Header_u8         =  TRACE_STATUS_HEADER;
		tr_frame.trStatus.TraceTimeOffset_u8      =  0;
		tr_frame.trStatus.MessageOccurrence_u8    = STATUS_MSG_RESET;
		tr_frame.trStatus.Messagecount_u8         = Trace_MessageCount_u8;
		tr_frame.trStatus.SystemTime_u32          = Trace_LastTransmittedSystemtime_u32;
		
		TraceBuffer_AddFrame(&tr_frame);        

		CPU_CRITICAL_EXIT();
    }
}


/***********************************************************************************************/
/*Function Name    :   Trace_IsEnabledState()                                                */
/*Description      :   This function determines, if tracing is currently enabled               */
/***********************************************************************************************/
/**
 *  @brief This function determines, if tracing is currently enabled
 *  @ return TRUE, if tracing is currently enabled, else FALSE
 */

boolean  Trace_IsEnabledState()
{
    return(Trace_EnabledState);
}


/***********************************************************************************************/
/*Function Name    :   Trace_Enable()                                                          */
/*Description      :   This function enables storing of Traces into Trace-Buffer               */
/***********************************************************************************************/
/**
 *  @brief This function enables storing of Traces into Trace-Buffer.
 *         Traces are transmitted and received Traces processed.
 */
void  Trace_Enable()
{
    LogHandle_RestoreModuleLog();
	
    Trace_EnabledState = TRUE;
    Trace_SendSystemTime();

    TRACE_TEXT(TRACE_SWITCH_ALWAYS, MOD_ALWAYS, "=============== Switching ON Trace !!!!================= ");
}




/***********************************************************************************************/
/*Function Name    :   Trace_Disable()                                                         */
/*Description      :   This function disables storing of Traces into Trace-Buffer              */
/*                     After disabling, all Traces, left if Buffer are transmitted             */
/***********************************************************************************************/
/**
 *  @brief This function disables storing of Traces into Trace-Buffer.
 *         Remaining Traces in Buffer are transmitted, till Buffer is empty.
 *         Received Trace-Frames are discarded.
 *         Enable tracing with Trace_Enable().
 */
void  Trace_Disable()
{
    TRACE_TEXT(TRACE_SWITCH_ALWAYS, MOD_ALWAYS, "++++++++++++++++ Switching OFF Trace !!!! +++++++++++++++");
    //SetModuleMask(DISABLE_ALL, 0);
	LogHandle_DisModuleLog();
	
    Trace_EnabledState = FALSE;
}


static CallbackFunctionElement trace_mod_lvl_cmd;
static void TraceSetModLvl(uint8 const len,char const *data);
/***********************************************************************************************/
/*Function Name    :   Trace_API_Init()                                                        */
/*Description      :   This function initializes all Trace variables. This function MUST be    */
/*                     called before any other funnction in Trace.                             */
/***********************************************************************************************/
/**
 *  @brief Trace-API initialization function.
 *  The Trace-API initialization function Trace_API_Init() must be called before Trace-Macros are used.
 *  If a First-Start appeared the Trace_buffer is initialized.
 *  Also the Trace-Handler is initialized.
 *  The Indexes for the Receive-Fifo are initialized if necessary
 *  At last status-frames are addes to buffer with information about Trace-version, Systemtime, and Reset-Info
 *  @todo Check, whether Trace_Init might be called explicitly after the CMX-OS has been initialized to assure
 *  that used CMX-functions are available yet.
 */
void Trace_API_Init(void)
{
    Trace_LastTransmittedSystemtime_u32 = 0;

    /* check for the reason of the init call (first-on, restart, diagnosis-start, ...).
       If one of the the cases configures for the macro MACRO_CheckForResetOfInitState() appeared,
       or if the Trace_MemoryIntegrityPattern isn't valid anymore

       -> initialize core components and NOINIT-variables.
     */
    if ( (VipLogNeedResetToInitState() ) || (Trace_GetTraceVersion() != Trace_MemoryIntegrityPattern) || !TraceBuffer_CheckBufferOk() )
    {
        Trace_MemoryIntegrityPattern = Trace_GetTraceVersion();
        //Trace_PhysicalChannel_u8     = TR_PHYSICAL_CHANNEL;

        TraceBuffer_Init();
        LogHandle_Init();

        FifoRx.rx_idx = 0;
        FifoRx.wr_idx = 0;
        FifoRx.used   = 0x00;
    }

    /* Enable tracing */
    Trace_EnabledState = TRUE;
	/* set initial mask mode */
	//SetModuleInitialMask();
    /*  Initialize Trace-Data-ID counter  */
    Trace_DataID_u8 = 0;

    /*  Initialize Message-counter  */
    Trace_MessageCount_u8 = 0;

    Trace_RetriggerTransmission_Flag = 1;

    Trace_Dispatcher_ResetConnections();

    Trace_SendVersionInfo();
    Trace_SendResetInfo();
  
    TRACE_ClientConnected  = TRUE;
	
	TRACE_REGISTER_CALLBACK_FUNCTION(TraceSetModLvl,  &trace_mod_lvl_cmd);
}


static void TraceSetModLvl(uint8 const len,char const *data)
{
	if ( len == 2 && data[0] == 0xAA && data[1] == 0x0AA )
	{
		LogHandle_OutputAllModLvl();
	}
	else
	{
		uint8_t proc_len = len;
		while ( proc_len >= 2 )
		{
			LogHandle_SetModuleLogLvl(data[0],data[1]);
			data += 2;
			proc_len -= 2;
		}
	}
}

/***********************************************************************************************/
/*Function Name    :   Trace_Channels_Init()                                                   */
/*Description      :   This function initializes all Trace channels . This function MUST be    */
/*                     called before Trace-Frames are transmitted or received                  */
/***********************************************************************************************/
/**
 *  @brief Trace-Initialization function.
 *  The Trace-Channels initialization function Trace_Channels_Init() must be called before
 *  Trace-Frames are transmitted or received. If the channels are not initialized with this function
 *  No Traces will be transmitted or received.
 */

void  Trace_Channels_Init(void)
{
    TRACE_TEXT(TRACE_SWITCH_DEBUG, MOD_ALWAYS, "############# Trace_Channels_Init() called! ");
    Trace_DispatcherInit();
}






#if ((1 == TRACE_ALLOW_VARIABLE_GET_SET)  || (1 == TRACE_ALLOW_CALLBACK_FUNCTION_CALLS))

/***********************************************************************************************/
/*                                       INTERNAL FUNCTION                                     */
/***********************************************************************************************/
/* Function Name    :  Trace_InsertCommandDataBuffer()                                             */
/* Description      :  This function collects the data for Data-Set in a buffer                */
/***********************************************************************************************/
/**
 *  @brief This function collects the data from several Data-Frames for a Data-Set command in a buffer.
 *  @param SeqNumber The sequence-number of the current Data-Frame, which contains the position of data
 *  @param pDataToInsert Pointer the the received data to insert into buffer
 *  @param Length Length of the data to insert into buffer
 */

void Trace_InsertCommandDataBuffer(uint8_t SeqNumber, uint8_t * pDataToInsert, uint8_t Length)
{
	(void)pDataToInsert;
    uint8_t *src = (uint8_t *)CommandDataBuffer + (SeqNumber * BYTES_PER_DATA_FRAME);

    for ( uint8_t i = 0; i < Length; i ++ )
    {	
    	src[i] = pDataToInsert[i];
    }
}



/***********************************************************************************************/
/*                                       INTERNAL FUNCTION                                     */
/***********************************************************************************************/
/* Function Name    :  Trace_ClearCommandDataBuffer()                                              */
/* Description      :  This function clears the Data-Set buffer                                */
/***********************************************************************************************/
/**
 *  @brief This function sets content of the the Data-Frame buffer to 0.
 */

void Trace_ClearCommandDataBuffer()
{
	for ( uint16_t i = 0; i < sizeof(CommandDataBuffer); i ++ )
	{	
		CommandDataBuffer[i] = 0x00;
	}
}

#endif


#if (1 == TRACE_ALLOW_VARIABLE_GET_SET)

/***********************************************************************************************/
/*                                       INTERNAL FUNCTION                                     */
/***********************************************************************************************/
/* Function Name    :  Trace_WatchVariableSmall()                                              */
/* Description      :  This function reads the value of a variable and sends it to the PC.     */
/***********************************************************************************************/
/**
 *  @brief This function reads the value of a variable and sends it to the PC.
 *  It is called, if the Trace-Server received a Trace-message containing a ClientData Get message with a length up to 4 bytes
 *  A ClientDataAck Trace-message is created containing the demanded bytes and added to the Trace-buffer.
 *  @param Address_u8 memory-address of the variable to read
 *  @param NumberOfBytes Length of the variable to read
 */
void Trace_WatchVariableSmall(uint8_t * Address_u8, uint8_t NumberOfBytes)
{
	(void)Address_u8;
	
	TRACE_FRAME tr_frame;
	
	// insert message header
	tr_frame.trClientAck.Trace_Header_u8     = TRACE_CLIENT_DATA_ACK_HEADER;
	tr_frame.trClientAck.CommandType_u8        = TR_CLIENTDATA_GET_ACK;
	tr_frame.trClientAck.DataLength_u8         = NumberOfBytes;

	tr_frame.trClientAck.Val[0] = Address_u8[0];
	tr_frame.trClientAck.Val[1] = Address_u8[1];
	tr_frame.trClientAck.Val[2] = Address_u8[2];
	tr_frame.trClientAck.Val[3] = Address_u8[3];
	

	TraceBuffer_AddFrame(&tr_frame);        
}



/***********************************************************************************************/
/*                                       INTERNAL FUNCTION                                     */
/***********************************************************************************************/
/* Function Name    :  Trace_WatchVariableLarge()                                              */
/* Description      :  This function reads the value of a variable and sends it to the PC.     */
/***********************************************************************************************/
/**
 *  @brief This function reads the value of a variable and sends it to the PC.
 *  It is called, if the Trace-Server received a Trace-message containing a ClientData Get message with a length greater than 4 bytes.
 *  Several Trace-message (at least 2 ) are created containing the demanded bytes and added to the Trace-buffer.
 *  The first Trace-message is an ServerData message with the special Trace-ID 0xFFFF to inform the Trace-Client that afterwards Data-Frames are transmitted.
 *  The following Trace-messages are Data-Frames containing the demanded variable(s).
 *  @param pAddress_u8 memory-address of the variable to read
 *  @param NumberOfBytes Length of the variable to read
 */

void Trace_WatchVariableLarge(uint8_t * pAddress_u8, uint8_t NumberOfBytes)
{
	(void)pAddress_u8;
	
    uint8_t       noOfFrames;// = 0;

    /* Calculate how many data frames are to use, to transmit the requested data */
    if (NumberOfBytes > MAX_BYTES_PER_TRACE_MEM)              /* totally 16 * 6 = 96 byte can be transferred in one TRACE_MEM call */
    {	/* 16 Data-Frames may be transmitted in one TRACE_MEM call */
        NumberOfBytes = MAX_BYTES_PER_TRACE_MEM;
    }

    noOfFrames = (NumberOfBytes+BYTES_PER_DATA_FRAME-1) / BYTES_PER_DATA_FRAME ;        /* One Data frame contains 6 bytes */
	
	TRACE_FRAME tr_frame;
	
	CPU_SR_ALLOC();		
	CPU_CRITICAL_ENTER();		// disable interrupt 
	
	if ( (noOfFrames + 1) <= TraceBuffer_GetFifoAvblNum() )
	{	// there is enouth buffer on trace buffer 
		/* Get the DataID, to identify the current data-transfer */
		uint8_t DataID = Trace_DataID_u8;
		/* This data transmission has been completed; generate a new dataID for the next transfer */
		Trace_IncreaseDataIDBy1();

		// insert message header and Trace-ID
        /* Write the content of the Server-Data-Frame which is transmitted first */
        tr_frame.trServerData.Trace_Header_u8	= TRACE_SERVER_DATA_HEADER;
        tr_frame.trServerData.TraceID_u16		= TRACE_ID_LARGE_DATA;
        tr_frame.trServerData.DataLength_u8		= NumberOfBytes;
        tr_frame.trServerData.Trace_DataID_u8	= DataID;
       	//tr_frame.trRawData[6]         		= TRACE_CLEAR_LETTER;
        //tr_frame.trRawData[7]         		= TRACE_CLEAR_LETTER;

        TraceBuffer_AddFrame(&tr_frame);
		
		/* In this loop, one Data-Frame after another is filled with content */
		for ( uint8_t i = 0 ; i < noOfFrames ; i++ )
		{
			tr_frame.trData.Trace_Header_u8  = (  TRACE_DATA_HEADER   | (i & SEQUENCE_NUMBER_BIT_MASK ) );
			tr_frame.trData.Trace_DataID_u8 = DataID;
			tr_frame.trData.Val[0] = *pAddress_u8++;	// 6 byte per frame
			tr_frame.trData.Val[1] = *pAddress_u8++;
			tr_frame.trData.Val[2] = *pAddress_u8++;
			tr_frame.trData.Val[3] = *pAddress_u8++;
			tr_frame.trData.Val[4] = *pAddress_u8++;
			tr_frame.trData.Val[5] = *pAddress_u8++;
			TraceBuffer_AddFrame(&tr_frame);
		}
	}

	CPU_CRITICAL_EXIT();	
}

/***********************************************************************************************/
/*                                       INTERNAL FUNCTION                                     */
/***********************************************************************************************/
/* Function Name    :  Trace_GetBitVariable()                                                  */
/* Description      :  This function sets the value of a Bit-variable and sends an ACK.        */
/***********************************************************************************************/
/**
 *  @brief This function sets the value of a Bit-variable and sends an ACK.
 *  It is called, if the Trace-Server received a Trace-message containing a ClientData GetBit message.
 *  A ClientDataAck Trace-message is created containing the demanded bit and added to the Trace-buffer.
 *  @param pAddress_u8 memory-address of the variable to read
 *  @param BitPosition_u8 position of the bit to read
 */
void Trace_GetBitVariable(uint8_t * pAddress_u8, uint8_t BitPosition_u8)
{
	(void)pAddress_u8;
	
    /* The Bit-position must be between 0 and 7 */
    if(BitPosition_u8 > 7)
    {
        TRACE_TEXT(TRACE_SWITCH_ALWAYS, MOD_ALWAYS, "Getting a Bit canceled because of invalid bit-position (must be between 0 and 7)!!");
    }
	else
	{
		TRACE_FRAME tr_frame;
		// insert message header and Trace-ID
        tr_frame.trClientAck.Trace_Header_u8         = TRACE_CLIENT_DATA_ACK_HEADER;
        tr_frame.trClientAck.CommandType_u8          = TR_CLIENTDATA_BIT_GET_ACK;
        tr_frame.trClientAck.DataLength_u8           = 0x01;
		if ( pAddress_u8[0] & (1u << BitPosition_u8) ){
			tr_frame.trClientAck.Val[0] = 0x01;
		}else{
			tr_frame.trClientAck.Val[0] = 0x00;
		}
        tr_frame.trClientAck.Val[1]                  = TRACE_CLEAR_LETTER;
        tr_frame.trClientAck.Val[2]                  = TRACE_CLEAR_LETTER;
        tr_frame.trClientAck.Val[3]                  = TRACE_CLEAR_LETTER;
		
		TraceBuffer_AddFrame(&tr_frame);
	}
}


/***********************************************************************************************/
/*                                       INTERNAL FUNCTION                                     */
/***********************************************************************************************/
/* Function Name    :  Trace_SetBitVariable()                                                  */
/* Description      :  This function sets the value of a Bit-variable and sends an ACK.        */
/***********************************************************************************************/
/**
 *  @brief This function sets the value of a Bit-variable and sends an ACK.
 *  It is called, if the Trace-Server received a Trace-message containing a ClientData SetBit message.
 *  The determined bit is set to the value contained by the Data-buffer.
 *  Afterwards a ClientDataAck Trace-message is created containing the demanded bit and added to the Trace-buffer.
 *  @param Address memory-address of the variable to read
 *  @param BitPosition_u8 position of the bit to read
 */

void Trace_SetBitVariable(uint32_t Address, uint8_t BitPosition_u8)
{
    //TRACE_FRAME  trFrame;
    /* The Bit-position must be between 0 and 7 */
    if(BitPosition_u8 > 7)
    {
        TRACE_TEXT(TRACE_SWITCH_ALWAYS, MOD_ALWAYS, "Setting a Bit canceled because of invalid bit-position (must be between 0 and 7)!!");
    }
	else
	{
		if (CommandDataBuffer[0])
		{
			*(uint8_t*)Address |= (uint8_t)(1u << BitPosition_u8);
		}
		else
		{
			*(uint8_t*)Address &= (uint8_t)(~(1u << BitPosition_u8)) ;
		}
		
		TRACE_FRAME tr_frame;
		// insert message header and Trace-ID
        tr_frame.trClientAck.Trace_Header_u8         = TRACE_CLIENT_DATA_ACK_HEADER;
        tr_frame.trClientAck.CommandType_u8          = TR_CLIENTDATA_BIT_SET_ACK;
        tr_frame.trClientAck.DataLength_u8           = 0x01;
		if ( *(uint8_t*)Address & (1u << BitPosition_u8) ){
			tr_frame.trClientAck.Val[0] = 0x01;
		}else{
			tr_frame.trClientAck.Val[0] = 0x00;
		}
        tr_frame.trClientAck.Val[1]                  = TRACE_CLEAR_LETTER;
        tr_frame.trClientAck.Val[2]                  = TRACE_CLEAR_LETTER;
        tr_frame.trClientAck.Val[3]                  = TRACE_CLEAR_LETTER;
		
		TraceBuffer_AddFrame(&tr_frame);
	}
}


/***********************************************************************************************/
/*                                       INTERNAL FUNCTION                                     */
/***********************************************************************************************/
/* Function Name    :  Trace_SetVariable()                                                     */
/* Description      :  This function sets a memory address and sends an ACK to the PC.         */
/***********************************************************************************************/
/**
 *  @brief This function sets a memory address and sends an ACK to the PC.
 *  It is called, if the Trace-Server received a Trace-message containing a ClientData Set message and all of the trailing Data-Frames containing the value to set.
 *  The determined variable (respectively the address) is set to the value contained by the Data-buffer.
 *  Afterwards a ClientDataAck Trace-message is created containing the last 4 byte of the set area and added to the Trace-buffer.
 *  @param Address memory-address of the variable to read
 *  @param NumberOfBytes Length of the variable to set
 */

void Trace_SetVariable(uint32_t Address, uint8_t NumberOfBytes)
{
//   TRACE_FRAME trFrame;
	for( uint8_t i = 0; i < NumberOfBytes; i ++ )
	{
		((uint8_t*)Address)[i] = CommandDataBuffer[i];
	}
	TRACE_FRAME tr_frame;
	/*Send an acknowledgement back to the PC*/
    tr_frame.trClientAck.Trace_Header_u8        = TRACE_CLIENT_DATA_ACK_HEADER;
    tr_frame.trClientAck.CommandType_u8          = TR_CLIENTDATA_SET_ACK;
    tr_frame.trClientAck.DataLength_u8           = NumberOfBytes;

    if ( NumberOfBytes >= 4 )
    {
        /* Send the last 4 byte of the set Data as Acknowledgement */
        /*lint -save -e534 */ /* ignoring return value the following function call, since always memcpy destionation is returnd */
        memcpy((uint8_t *)tr_frame.trClientAck.Val, ( (uint8_t *)Address + NumberOfBytes - 4) , 4);
        /*lint -restore*/
		tr_frame.trClientAck.Val[0] = ((uint8_t*)Address)[NumberOfBytes-4];
		tr_frame.trClientAck.Val[1] = ((uint8_t*)Address)[NumberOfBytes-3];
		tr_frame.trClientAck.Val[2] = ((uint8_t*)Address)[NumberOfBytes-2];
		tr_frame.trClientAck.Val[3] = ((uint8_t*)Address)[NumberOfBytes-1];
    }
    else
    {
		tr_frame.trClientAck.Val[0] = ((uint8_t*)Address)[0];
		tr_frame.trClientAck.Val[1] = ((uint8_t*)Address)[1];
		tr_frame.trClientAck.Val[2] = ((uint8_t*)Address)[2];
		tr_frame.trClientAck.Val[3] = ((uint8_t*)Address)[3];
    }
	TraceBuffer_AddFrame(&tr_frame);
}

#endif //(1 == TRACE_ALLOW_VARIABLE_GET_SET)



#if (1 == TRACE_ALLOW_CALLBACK_FUNCTION_CALLS)
/***********************************************************************************************/
/*                                       INTERNAL FUNCTION                                     */
/***********************************************************************************************/
/* Function Name    :  Trace_InvokeCallbackFunction(uint8_t parameterSize, uint32_t CallbackFunctionID) */
/* Description      :  This function invokes a callback function speficied by the parameter    */
/***********************************************************************************************/
/**
 *  @brief This function invokes a callback function specified by the address-parameter.
 *  It is called, if the Trace-Server received a Trace-message containing a "invoke Callback-function" command and
 *  all of the trailing Data-Frames containing the parameters.
 *  @param parameterSize Size of the parameters for the function to invoke in byte
 *  @param CallbackFunctionID memory-address of the function to be called
 */
void Trace_InvokeCallbackFunction(uint8_t parameterSize, uint32_t CallbackFunctionID)
{
	uint8_t elementFound = 0;
    CallbackFunctionElement * current = CallbackFunctionList;

    /* Search the list of registered callback functions for the requested function */

    /* While we're not at the end of the list, and we haven't found the data */
    while ((current != 0) && !elementFound)
    {
        /* If the name of the current item matches the functionID we're looking for... */
        if (current->functionID == CallbackFunctionID)
        {
            elementFound = 1; /* ...then we're done! */
        }
        else
        {
            current = current->next; /* else we try the next item in the list */
        }
    }

    /*If we didn't find the function ID... */
    if (!(current != 0))
    {
        TRACE_VALUE(TRACE_SWITCH_ALWAYS, MOD_ALWAYS, "!!!!! ERROR: The callback function with ID %d isn't registered !!!!!!", CallbackFunctionID);
    }
	else
	{
		TRACE_VALUE(TRACE_SWITCH_DEBUG, MOD_ALWAYS, "!! Calling callback-function with parametersize %d !!",parameterSize);
		TRACE_VALUE(TRACE_SWITCH_DEBUG, MOD_ALWAYS, "!! Calling callback-function at address %d !!",CallbackFunctionID);

		/* call the requested function */
		//current->functionPointer(parameterSize, (char *)CommandDataBuffer);
		current->functionPointer(parameterSize, (char *)CommandDataBuffer);
		
		TRACE_FRAME tr_frame;
		
		tr_frame.trClientAck.Trace_Header_u8	= TRACE_CLIENT_DATA_ACK_HEADER;
        tr_frame.trClientAck.CommandType_u8		= TR_CLIENTDATA_CALL_FUNCTION_ID_ACK;
        tr_frame.trClientAck.DataLength_u8		= parameterSize;
        tr_frame.trRawData_32[1]				= CallbackFunctionID;
		//tr_frame.trClientAck.Val[0]			= ((uint8_t*)(&CallbackFunctionID))[0];
		//tr_frame.trClientAck.Val[1]			= ((uint8_t*)(&CallbackFunctionID))[1];
		//tr_frame.trClientAck.Val[2			= ((uint8_t*)(&CallbackFunctionID))[2];
		//tr_frame.trClientAck.Val[3]			= ((uint8_t*)(&CallbackFunctionID))[3];
		
		TraceBuffer_AddFrame(&tr_frame);
	}    
}
#endif //(1 == TRACE_ALLOW_CALLBACK_FUNCTION_CALLS)






/************************************************************************************************/
/* Function Name     :  Trace_AddFrameToRXBuffer(trFrameToAdd, channelReceivedFrom)             */
/* Description       :  This function adds the passed Trace-frame to the Receive-buffer.        */
/************************************************************************************************/
/**
 *  @brief This function adds the passed Trace-frame to the Receive-buffer.
 *  It is called, if the Trace-Dispatcher received a Trace-message.
 *  @param ptrFrameToAdd Pointer to the received Trace-message
 *  @param channelReceivedFrom Number identifying the channel the Trace-message was received from.
 */

void Trace_AddFrameToRXBuffer(TRACE_FRAME * ptrFrameToAdd, uint8_t channelReceivedFrom)
{
	(void)ptrFrameToAdd;
    (void)channelReceivedFrom;

    CPU_SR_ALLOC();
    CPU_CRITICAL_ENTER();

    if ( FifoRx.used < FIFOSIZERX )
    {
        FifoRx.used ++;

        FifoRx.fifo[FifoRx.wr_idx].trFrame.trRawData_32[0] = ptrFrameToAdd->trRawData_32[0];
        FifoRx.fifo[FifoRx.wr_idx].trFrame.trRawData_32[1] = ptrFrameToAdd->trRawData_32[1];

        FifoRx.wr_idx ++;
        if ( FifoRx.wr_idx >= FIFOSIZERX )
        {
            FifoRx.wr_idx = 0x00;
        }
    }

    CPU_CRITICAL_EXIT();
}




/************************************************************************************************/
/* Function Name     :  Trace_ProcessClientCommands(uint8_t *MessageFromClient_u8)                */
/* Description       :  This function processes commands received from TraceClient.             */
/************************************************************************************************/
/**
 *  @brief This function processes commands received from TraceClient.
 *  It is called, if a new Trace-message was added to the Receive-buffer.
 *  Now this message has to be processed:
 *
 *  @param MessageFromClient_u8 Pointer to the received Trace-message
 */
void Trace_ProcessClientCommands(uint8_t *MessageFromClient_u8)
{
#if ( (1 == TRACE_ALLOW_VARIABLE_GET_SET) || (1 == TRACE_ALLOW_CALLBACK_FUNCTION_CALLS) )
    uint8_t            curSeqNo_u8;
    static uint8_t     noOfFrames;
    static uint8_t     lastFrameSize;
    static uint8_t     SetDataLength_u8;
    static uint8_t     noOfSetDataFramesReceived;
#endif

#if  (1 == TRACE_ALLOW_VARIABLE_GET_SET)
    static uint32_t    SetDataAddress;
    static uint32_t    SetBitAddress;
    static uint8_t     SetBitPosition;
#endif

#if (1 == TRACE_ALLOW_SEND_EVENTS)
    static OS_TaskType     SendEvent_TaskNumber;
    static OS_EventType    SendEvent_EventNumber;
#endif

#if (1 == TRACE_ALLOW_CALLBACK_FUNCTION_CALLS)
    static uint32_t     CallbackFunctionID;
#endif


    switch(MessageFromClient_u8[0] & HEADER_BIT_MASK)  /* Mask the first 4 bit of the Trace-frame containing the type of frame (respectively the type of command) */
    {
    case TRACE_MODULE_MASK_HEADER:
    {
        uint32_t tmpModuleMask;

        TRACE_ClientConnected = TRUE;

		if ( MessageFromClient_u8[3] == 0x00 )
		{	// old log message module mask message
			Trace_SendVersionInfo();           /* If a module-mask was received the Trace-Version must be transmitted */
			tmpModuleMask = *((uint32_t*)&(MessageFromClient_u8[4]));	//lint !e826
			LogHandle_SetModuleMask(NORMAL_SET, tmpModuleMask);   /* Calling the function to set the module-mask in the Trace-Handler */			
		}
		else
		{	// MessageFromClient_u8[3] == 0x01; new version command
			if ( MessageFromClient_u8[4] == 0x00 )
			{	// pc tool write level
				LogHandle_SetModuleLogLvl(MessageFromClient_u8[6],MessageFromClient_u8[7]);// log v2 set module mask			
			}
			else if ( MessageFromClient_u8[4] == 0x01 )
			{	// pc tool read level 
				Trace_GetModuleLvl(MessageFromClient_u8[2],MessageFromClient_u8[6]);
			}
			else
			{
                TRACE_VALUE(TRACE_SWITCH_ALWAYS, MOD_ALWAYS, "Trace: command error,byte4=0x%02x",MessageFromClient_u8[4]);
			}
		}


        // if the channel to be switched to is 0, ignore the command and switch to the default channel
        //if( !MessageFromClient_u8[2] )
        //{
            //TracePhy_SetCom(TRACE_COM_PHY_DEFAULT);
        //}
        if ( MessageFromClient_u8[2] > 0 && (uint8)TracePhy_GetCom() != MessageFromClient_u8[2] )
        {
            switch (MessageFromClient_u8[2])
            {
			case TRACE_RS232:
				TracePhy_SetCom(TRACE_RS232);
				TRACE_TEXT(TRACE_SWITCH_ALWAYS, MOD_ALWAYS, "Trace: the Trace-channel change to RS232 ");					
				break;
				
            case TRACE_IPC:
				TracePhy_SetCom(TRACE_IPC);
				TRACE_TEXT(TRACE_SWITCH_ALWAYS, MOD_ALWAYS, "Trace: the Trace-channel change to DEVIPC ");	
                break;

			case TRACE_CAN0:
                TracePhy_SetCom(TRACE_CAN0);
                TRACE_TEXT(TRACE_SWITCH_ALWAYS, MOD_ALWAYS, "Trace: the Trace-channel change to CAN bus ");
                break;				
			case TRACE_RAW_SPI:
                TracePhy_SetCom(TRACE_RAW_SPI);
                TRACE_TEXT(TRACE_SWITCH_ALWAYS, MOD_ALWAYS, "Trace: the Trace-channel change to RAW SPI ");
                break;	                
            default:
                TRACE_TEXT(TRACE_SWITCH_ALWAYS, MOD_ALWAYS, "Trace: Unknown Trace-channel requested !! ");
                break;
            }
        }
    }
		break;

    case TRACE_CLIENT_DATA_HEADER:

        TRACE_ClientConnected = TRUE;

        switch (MessageFromClient_u8[1])
        {
        case TR_CLIENTDATA_BIT_GET_ID:

#if (0 == TRACE_ALLOW_VARIABLE_GET_SET)
            TRACE_TEXT(TRACE_SWITCH_ALWAYS, MOD_ALWAYS, "Getting / Setting with current configuration not allowed !! (change Trace.cfg)");
#else
            /* A Trace-message containing ClientData Get Bit was received -> call Trace_GetBitVariable() */
            TRACE_TEXT(TRACE_SWITCH_DEBUG, MOD_ALWAYS, "Client Data Header: GET BIT received! ");
            Trace_GetBitVariable(MessageFromClient_u8+4,    /* Address of the variable  */
                                 MessageFromClient_u8[3]);  /* BitPosition              */
#endif //(0 == TRACE_ALLOW_VARIABLE_GET_SET)
            break;



        case TR_CLIENTDATA_BIT_SET_ID:

#if (0 == TRACE_ALLOW_VARIABLE_GET_SET)
            TRACE_TEXT(TRACE_SWITCH_ALWAYS, MOD_ALWAYS, "Getting / Setting with current configuration not allowed !! (change Trace.cfg)");
#else
            /* A Trace-message containing ClientData Set Bit was received -> set a flag to wait for the Data-frames containing the data to set */
            TRACE_TEXT(TRACE_SWITCH_DEBUG, MOD_ALWAYS, "Client Data Header: SET BIT received! ");

            SetBitPosition = MessageFromClient_u8[3];                                 /* BitPosition              */
            /*lint -save -e534 */ /* ignoring return value the following function call, since always memcpy destionation is returnd */
            memcpy(&SetBitAddress, MessageFromClient_u8+4, sizeof(SetBitAddress) );    /* Address of the variable  */
            /*lint -restore*/

            if (!CommandDataCompleteFlag_u8)       /* if Data-frames trailing a previous ClientData Set are still missing, clear the Data-buffer */
            {
                Trace_ClearCommandDataBuffer();
            }
            CommandDataErrorFlag_u8 = 0;           /* reset a flag signalling an error in a sequence of Data-frames */
            CommandDataCompleteFlag_u8 = 0;        /* reset a flag signalling a sequence of Data-frames if complete */
            noOfSetDataFramesReceived = 0;     /* reset counterof the currently received Data-frames */

            noOfFrames = 1;
            lastFrameSize = 1;
            SetDataBitFlag_u8  = 1;         /* set a flag signalling ClientData Set Bit was received -> wait for the trailing Data-frame containing the bit value to set */
#endif //(0 == TRACE_ALLOW_VARIABLE_GET_SET)
            break;


        case TR_CLIENTDATA_GET_ID:

#if (0 == TRACE_ALLOW_VARIABLE_GET_SET)
            TRACE_TEXT(TRACE_SWITCH_ALWAYS, MOD_ALWAYS, "Getting / Setting with current configuration not allowed !! (change Trace.cfg)");
#else
            /* A Trace-message containing ClientData Get was received -> call Trace_WatchVariableSmall() if not more than 4 bytes are demanded; call Trace_WatchVariableLarge() if more than 4 bytes are demanded */
            if (MessageFromClient_u8[2] <= 4 )    // Is length of the bytes to send less than 5
            {
                TRACE_TEXT(TRACE_SWITCH_DEBUG, MOD_ALWAYS, "Client Data Header: GET Variable small received! ");
                Trace_WatchVariableSmall( MessageFromClient_u8+4,   /*Address of the variable */
                                          MessageFromClient_u8[2]); /*Number of bytes to read */
            }
            else  // length of the bytes to send greater than 4
            {
                TRACE_TEXT(TRACE_SWITCH_DEBUG, MOD_ALWAYS, "Client Data Header: GET Variable large received! ");
                Trace_WatchVariableLarge( MessageFromClient_u8+4,   /*Address of the variable */
                                          MessageFromClient_u8[2]); /*Number of bytes to read */
            }
#endif //(0 == TRACE_ALLOW_VARIABLE_GET_SET)
            break;


        case TR_CLIENTDATA_SET_ID:
#if (0 == TRACE_ALLOW_VARIABLE_GET_SET)
            TRACE_TEXT(TRACE_SWITCH_ALWAYS, MOD_ALWAYS, "Getting / Setting with current configuration not allowed !! (change Trace.cfg)");
#else
            /* A Trace-message containing ClientData Set was received */
            TRACE_TEXT(TRACE_SWITCH_DEBUG, MOD_ALWAYS, "Client Data Header: SET received! ");

            /*lint -save -e534 */ /* ignoring return value the following function call, since always memcpy destionation is returnd */
            memcpy(&SetDataAddress, MessageFromClient_u8+4, sizeof(SetDataAddress) );    /* Address of the variable  */
            /*lint -restore*/

            SetDataLength_u8 = MessageFromClient_u8[2];

            SetDataFlag_u8 = 1;        /* set a flag to wait for the Data-frames containing the data to set */
            if (!CommandDataCompleteFlag_u8)       /* if Data-frames trailing a previous ClientData Set are still missing, clear the Data-buffer */
            {
                Trace_ClearCommandDataBuffer();
            }
            CommandDataErrorFlag_u8 = 0;           /* reset a flag signalling an error in a sequence of Data-frames */
            CommandDataCompleteFlag_u8 = 0;        /* reset a flag signalling a sequence of Data-frames if complete */
            noOfSetDataFramesReceived = 0;     /* reset counterof the currently received Data-frames */

            if (SetDataLength_u8 > MAX_BYTES_PER_TRACE_MEM)               /* totally 16 * 6 = 96 byte can be transferred in one TRACE_MEM call*/
            {
                /* 16 Data-Frames may be transmitted in one TRACE_MEM call */
                TRACE_TEXT(TRACE_SWITCH_ALWAYS, MOD_ALWAYS, " Data-set length to large !!! A maximum of 96 bytes can be set !!");
                SetDataFlag_u8 = 0;
                return;
            }

            TRACE_VALUE(TRACE_SWITCH_DEBUG, MOD_ALWAYS, "Data-set length: %d ", SetDataLength_u8);

            /* Calculate the number of relevant bytes the last Data-frame contains */
            noOfFrames = SetDataLength_u8 / BYTES_PER_DATA_FRAME ;        /* One Data frame contains 6 bytes */
            if ( (BYTES_PER_DATA_FRAME * noOfFrames) < SetDataLength_u8)
            {
                lastFrameSize = SetDataLength_u8 % BYTES_PER_DATA_FRAME;
                noOfFrames++;
            }
            else
            {
                lastFrameSize = BYTES_PER_DATA_FRAME;
            }
#endif //(0 == TRACE_ALLOW_VARIABLE_GET_SET)
            break;


        case TR_CLIENTDATA_SEND_EVENT_ID:
            TRACE_TEXT(TRACE_SWITCH_ALWAYS, MOD_ALWAYS, "Event notification of tasks not enabled !! (change Trace.cfg)");
            break;



        case TR_CLIENTDATA_CALL_FUNCTION_ID:
#if (0 == TRACE_ALLOW_CALLBACK_FUNCTION_CALLS)
            TRACE_TEXT(TRACE_SWITCH_ALWAYS, MOD_ALWAYS, "Remote call of Trace-callback functions not enabled !! (change Trace.cfg)");
#else
            /* A Trace-message containing ClientData call function was received */
            TRACE_TEXT(TRACE_SWITCH_DEBUG, MOD_ALWAYS, "Client Data Header: CALL FUNCTION received! ");

            /*lint -save -e534 */ /* ignoring return value the following function call, since always memcpy destionation is returnd */
            memcpy(&CallbackFunctionID, MessageFromClient_u8+4, sizeof(CallbackFunctionID) );    /* Address of the callback function  */
            /*lint -restore*/

            TRACE_VALUE(TRACE_SWITCH_DEBUG, MOD_ALWAYS, "!! callback-ID %d !!",CallbackFunctionID);

            SetDataLength_u8 = MessageFromClient_u8[2];

            // if the length of the parameter expected for the callback function equals 0
            if(0 == SetDataLength_u8 )
            {
                // call the callback function immediately
                Trace_InvokeCallbackFunction(SetDataLength_u8, CallbackFunctionID);
            }
            else  // set a flag, to wait for the trailing data-frames, that must be received.
            {
                InvokeCallbackFunctionFlag_u8  = 1;               /* set a flag to wait for the Data-frames containing the parameters for the callback function */
                if (!CommandDataCompleteFlag_u8)       /* if Data-frames trailing a previous ClientData Set are still missing, clear the Data-buffer */
                {
                    Trace_ClearCommandDataBuffer();
                }
                CommandDataErrorFlag_u8 = 0;           /* reset a flag signalling an error in a sequence of Data-frames */
                CommandDataCompleteFlag_u8 = 0;        /* reset a flag signalling a sequence of Data-frames if complete */
                noOfSetDataFramesReceived = 0;     /* reset counterof the currently received Data-frames */

                if (SetDataLength_u8 > MAX_BYTES_PER_TRACE_MEM)               /* totally 16 * 6 = 96 byte can be transferred in one TRACE_MEM call*/
                {
                    /* 16 Data-Frames may be transmitted in one TRACE_MEM call */
                    TRACE_TEXT(TRACE_SWITCH_ALWAYS, MOD_ALWAYS, "Size of parameters for callback function to large !!! A maximum of 96 bytes are allowed !!");
                    InvokeCallbackFunctionFlag_u8 = 0;
                    return;
                }

                TRACE_VALUE(TRACE_SWITCH_DEBUG, MOD_ALWAYS, "parameter size: %d ", SetDataLength_u8);

                /* Calculate the number of relevant bytes the last Data-frame contains */
                noOfFrames = SetDataLength_u8 / BYTES_PER_DATA_FRAME ;        /* One Data frame contains 6 bytes */
                if ( (BYTES_PER_DATA_FRAME * noOfFrames) < SetDataLength_u8)
                {
                    lastFrameSize = SetDataLength_u8 % BYTES_PER_DATA_FRAME;
                    noOfFrames++;
                }
                else
                {
                    lastFrameSize = BYTES_PER_DATA_FRAME;
                }
            }

#endif //(0 == TRACE_ALLOW_CALLBACK_FUNCTION_CALLS )
            break;


            /* default: recognized unknown command -> return */
        default:
            TRACE_TEXT(TRACE_SWITCH_ALWAYS, MOD_ALWAYS, "Trace-Server - Unknown Trace command !! ");
            return;
            // break;  /* comment "break;" out, since the compiler warns, that it's not reachable */

        }   //switch (MessageFromClient_u8[1])
        break;

    case TRACE_DATA_HEADER:

        TRACE_ClientConnected = TRUE;

#if ( (1 == TRACE_ALLOW_VARIABLE_GET_SET) || (1 == TRACE_ALLOW_CALLBACK_FUNCTION_CALLS) )

        TRACE_TEXT(TRACE_SWITCH_DEBUG, MOD_ALWAYS, "Data Header received! ");

        if (CommandDataErrorFlag_u8)         /* If a error occured: Drop all data-frames till next data-header frame */
        {
            break;
        }

        noOfSetDataFramesReceived++;           /* increase the Data-frame counter by 1 */
        curSeqNo_u8 = (MessageFromClient_u8[0] & SEQUENCE_NUMBER_BIT_MASK);
        if (curSeqNo_u8 != (noOfSetDataFramesReceived - 1) )  /* If the trailing Data-frames are not received in correct order set the error-flag */
        {
            CommandDataErrorFlag_u8 = 1;
            TRACE_TEXT(TRACE_SWITCH_ALWAYS, MOD_ALWAYS, " Lost at least one data-frame!!! ");
            break;
        }

        if (curSeqNo_u8 < noOfFrames -1)          /* If the currently received Data-Frame is NOT the last in the current sequence, add all data bytes to the Buffer */
        {
            Trace_InsertCommandDataBuffer( (noOfSetDataFramesReceived - 1 ), (uint8_t *)MessageFromClient_u8+2, BYTES_PER_DATA_FRAME);
        }
        else                                   /* If the currently received Data-Frame is the last in the current sequence, add the rest of data bytes to the Buffer */
        {
            Trace_InsertCommandDataBuffer( (noOfSetDataFramesReceived - 1) , (uint8_t *)MessageFromClient_u8+2, lastFrameSize);
        }



        if (noOfSetDataFramesReceived == noOfFrames)  /* If all Data-frames were received correctly call Trace_SetVariable() to perform the setting of the variable */
        {
#if (1 == TRACE_ALLOW_VARIABLE_GET_SET)
            if (SetDataFlag_u8)               /* if a ClientData Set was received:  */
            {
                TRACE_TEXT(TRACE_SWITCH_DEBUG, MOD_ALWAYS, "Setting variable! ");
                Trace_SetVariable( SetDataAddress,     /* Address of the variable  */
                                   SetDataLength_u8);  /* Length of variable       */
                SetDataFlag_u8 = 0;
            }

            if (SetDataBitFlag_u8)           /* if a ClientData Set Bit was received this is the trailing data-frame */
            {
                TRACE_TEXT(TRACE_SWITCH_DEBUG, MOD_ALWAYS, "Setting Bit variable! ");
                Trace_SetBitVariable(SetBitAddress, SetBitPosition);
                SetDataBitFlag_u8 = 0;        /* reset set the flag signalling ClientData Set Bit was received */
            }
#endif

#if (1 == TRACE_ALLOW_CALLBACK_FUNCTION_CALLS)
            if (InvokeCallbackFunctionFlag_u8)
            {
                TRACE_TEXT(TRACE_SWITCH_DEBUG, MOD_ALWAYS, "Invoking callback function! ");
                Trace_InvokeCallbackFunction(SetDataLength_u8, CallbackFunctionID);
                InvokeCallbackFunctionFlag_u8 = 0;
            }
#endif

            CommandDataCompleteFlag_u8 = 1;
            Trace_ClearCommandDataBuffer();
        }
#endif //( (1 == TRACE_ALLOW_VARIABLE_GET_SET) || (1 == TRACE_ALLOW_CALLBACK_FUNCTION_CALLS) )


        break;

    case TRACE_FUTURE_USE_HEADER:
    {
        /* This command is currently unused.
         * If any further command-types are needed, this command-type could be used.  */
    }
		break;

    default:
    {
        TRACE_TEXT(TRACE_SWITCH_ALWAYS, MOD_ALWAYS, "Trace-Server - Received Non-Trace-Frame !! ");
        // MACRO_NON_TRACE_FRAME_RECEIVED(&MessageFromClient_u8)
        return;
    }
    }
}


/************************************************************************************************/
/* Function Name     :  Trace_Message_Received( )                                               */
/* Description       :  This function is called from an LogPhyReceivedFrameEvent Event,       */
/*                      if a Trace-frame was received                                           */
/************************************************************************************************/
/**
 *  @brief This function is called from an LogPhyReceivedFrameEvent Event, if a Trace-frame was received.
 *  The next message from the Receive-Buffer is processed with the rights of the channel it was received from.
 *  If the message is not an Trace-message (first bit is 0) the message is discarded.
 */
void Trace_Message_Received(void)
{
	if ( FifoRx.used )
	{
		TRACE_VALUE(TRACE_SWITCH_DEBUG, MOD_ALWAYS, "???????????????????????? number of rx-commands in Q: %x ", FifoRx.used);

	    // process messages in receive queue, as long, as there are some left.
	    do
	    { 	
	        /*lint -save -e534 */ /* ignoring return value the following function call, since always memcpy destionation is returnd */
	        //memcpy(MessageFromClient_u8, (uint8_t *)FifoRx[FifoIndexRxRd].trFrame.trRawData, TRACE_FRAME_SIZE);
	        /*lint -restore*/
	       	uint8_t msg[8];
	       	
	       	msg[0]= FifoRx.fifo[FifoRx.rx_idx].trFrame.trRawData[0];
	       	msg[1]= FifoRx.fifo[FifoRx.rx_idx].trFrame.trRawData[1];
	       	msg[2]= FifoRx.fifo[FifoRx.rx_idx].trFrame.trRawData[2];
	       	msg[3]= FifoRx.fifo[FifoRx.rx_idx].trFrame.trRawData[3];
	       	msg[4]= FifoRx.fifo[FifoRx.rx_idx].trFrame.trRawData[4];
	       	msg[5]= FifoRx.fifo[FifoRx.rx_idx].trFrame.trRawData[5];
	       	msg[6]= FifoRx.fifo[FifoRx.rx_idx].trFrame.trRawData[6];
	       	msg[7]= FifoRx.fifo[FifoRx.rx_idx].trFrame.trRawData[7];
	       	
			CPU_SR_ALLOC();
			CPU_CRITICAL_ENTER();
			FifoRx.rx_idx ++;
			if ( FifoRx.rx_idx >= FIFOSIZERX )
			{
				FifoRx.rx_idx = 0x00;
			}
			FifoRx.used --;
			CPU_CRITICAL_EXIT();
			
			
	        TRACE_VALUE4(TRACE_SWITCH_DEBUG, MOD_ALWAYS, "############# Received Command1: %x %x %x %x ", msg[0], msg[1],msg[2],msg[3]);
	        TRACE_VALUE4(TRACE_SWITCH_DEBUG, MOD_ALWAYS, "############# Received Command2: %x %x %x %x ", msg[4], msg[5],msg[6],msg[7]);

	        /* Handle trace-command */
	        Trace_ProcessClientCommands(msg);

	    }while( FifoRx.used );
	}
}

/************************************************************************************************/
/* Function Name     :  Trace_Message_Transmitted( )                                            */
/* Description       :  This function is called if a MACRO_TR_MSG_TRANSMITTED_EV event occured */
/*                      in OnOff task.                                                          */
/************************************************************************************************/
/**
 *  @brief This function is called if a MACRO_TR_MSG_TRANSMITTED_EV event occured . . .
 *  The currently transmitted Frame is removed from Trace-Buffer, since it was transmitted successfully.
 *  It also triggers the transmission of the next Trace-message from the Trace-Buffer.
 */
void Trace_Message_Transmitted(void)
{
	if ( TRACE_ClientConnected )
	{
		Trace_Dispatcher_SendNextFrameFromBuffer();
	}
}

#if (1 == TRACE_ALLOW_CALLBACK_FUNCTION_CALLS)
/************************************************************************************************/
/* Function Name     :  void Trace_registerCallbackFunction(uint32_t FunctionID,
                              void (* PointerToFunction)(uint8_t, char *),
                              CallbackFunctionElement * PointerToCallbackFunctionElement,
                              char * PointerToBuffer,
                              uint8_t SizeOfBuffer),                                             */
/* Description       :  This function is called to add an element to the callback-function      */
/*                      linked list                                                             */
/************************************************************************************************/
/**
 *  @brief This function is called to add an element to the callback-function linked list
 *  The functionID, pointer to the function, pointer to a CallbackFunctionElement,
 *  pointer to the parameter-buffer are added as a new element to the linked list of callback
 *  functions.
 *  @param FunctionID The Function ID of the callback-function, that is registered (this ID is generated by the PERL scrip tr_parse.pl)
 *  @param PointerToFunction points to the function, that is should be registered as callback-function
 *  @param PointerToCallbackFunctionElement   points to a CallbackFunctionElement, which is used to register the callback-function within a linked list, containing all registered callback-functions.
 *         This element MUST NOT be manipulated by the registrator, since this could corrupt the linked-list of registered callbacks !!
 *  @param PointerToBuffer points to the parameter-buffer, which is passed by the registration
 *  @param SizeOfBuffer is the size of the parameter-buffer, which is passed by the registration
 */
void Trace_registerCallbackFunction(uint32_t FunctionID,                                            \
                                    void (* PointerToFunction)(uint8_t const, char const*),                    \
                                    CallbackFunctionElement * PointerToCallbackFunctionElement)
{
    TRACE_VALUE(TRACE_SWITCH_DEBUG, MOD_ALWAYS, "===================  Trace_registerCallbackFunction called with FunctionID %x !", FunctionID);
    TRACE_VALUE(TRACE_SWITCH_DEBUG, MOD_ALWAYS, "===================  Pointer to callback function: %x ", (uint32_t)PointerToFunction);

    /* Adding the new element to the linked list */
    PointerToCallbackFunctionElement->functionID = FunctionID;
    PointerToCallbackFunctionElement->functionPointer = PointerToFunction;

    PointerToCallbackFunctionElement->next = CallbackFunctionList;
    CallbackFunctionList = PointerToCallbackFunctionElement;

}
#endif



/**
 * @}
 */

