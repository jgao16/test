#ifndef _TRACE_IPC_H_
#define _TRACE_IPC_H_


/**
 *  @ingroup        Trace_IPC_Group
 *  @file           Trace_IPC.h
 *  @author         Jung, Simon
 *
 *  @brief This is the header file for Trace_IPC.c.
 *  The  file  contains  function prototypes for all functions exposed by Trace_IPC.c.
 ******************************************************************************/

#include "Trace_Internal.h"
#include "Trace_Formats.h"


#if (SW_TR_IPC_ENABLED == 1)


/***********************************************************************************************/
/* Function Name    :   Trace_IPCInit()                                                        */
/* Description      :   This function initializes the IPC Trace channel                        */
/***********************************************************************************************/
/** @ingroup        Trace_IPC_Group
 *  @brief This function initializes the IPC Trace channel
 */
void     Trace_IPCInit(void);


/***********************************************************************************************/
/* Function Name   :  Trace_IPC_ResetConnection()                                              */
/* Description     :  This function resets the connection state of the IPC interface           */
/*                    By calling this function within the Trace-API initialization, it's       */
/*                    assured, that the connection is used if it was already initialized       */
/***********************************************************************************************/
/** @ingroup        Trace_IPC_Group
 *  @brief This function resets the connection state of the IPC interface
 *  By calling this function within the Trace-API initialization, it's
 *  assured, that the connection is used if it was already initialized
 */
void Trace_IPC_ResetConnection(void);



/***********************************************************************************************/
/* Function Name    :   Trace_IPC_ChConCALLBACK()                                              */
/* Description      :   Called by the IPC driver when the IPC channel has been successfully    */
/*                      opened on both I/O Controller  and SH4                                 */
/***********************************************************************************************/
/** @ingroup        Trace_IPC_Group
 *  @brief Called by the IPC driver when the IPC channel has been successfully opened on both I/O Controller  and SH4
 */
   void              Trace_IPCChannelConnCB              (UInt8 channel, UInt8 oldCnt, UInt8 newCnt);



/***********************************************************************************************/
/* Function Name    :   Trace_IPC_ChRxCALLBACK()                                               */
/* Description      :   Called by the IPC driver to notify us that SH4 sent something on this  */
/*                      channel.                                                               */
/***********************************************************************************************/
/** @ingroup        Trace_IPC_Group
 *  @brief This funcfion is called by the IPC driver to notify us that SH4 sent something on this channel.
 */
   void              Trace_IPCChannelRxCB                (UInt8 channel, UInt16 msgs);





/***********************************************************************************************/
/* Function Name    :   Trace_IPC_ChTxCALLBACK(uint8_t channel)                                  */
/* Description      :   Called by the IPC driver to notify us that transmit is completed on    */
/*                      this channel.                                                          */
/***********************************************************************************************/
/** @ingroup        Trace_IPC_Group
 *  @brief This function is called by the IPC-driver, if the current transmission has been successfully completed.
 *  It informs the Trace-Dispatcher of the completed transmission
 */
   void              Trace_IPCChannelTxCB                (UInt8 channel);



/***********************************************************************************************/
/* Function Name    :   Trace_SendFrameIPC(TRACE_FRAME * tmpFrame)                             */
/* Description      :   This function transmitts the frame referenced by passed pointer via IPC*/
/***********************************************************************************************/
/** @ingroup        Trace_IPC_Group
 *  @brief This function transmitts the frame referenced by passed pointer via IPC
 *  @param tmpFrame The Trace-frame to send.
 *  @return The state of transmission
 */
   eTR_SEND_STATE    Trace_SendFrameIPC                  (TRACE_FRAME * tmpFrame);



/***********************************************************************************************/
/* Function Name    :   GetTraceCommandIPC()                                                   */
/* Description      :   This function gets the received bytes from the IPC-channel and passes  */
/*                      it the dispatcher for adding it into the RXBuffer                      */
/***********************************************************************************************/
/** @ingroup        Trace_IPC_Group
 *  @brief Retrieve the received Trace-frame from IPC-channel and adds it to the RX-Fifo in Trace-Server
 */
   void GetTraceCommandIPC(void);

#else

   /** Defining the IPC function empty, if the tracing via IPC is disabled. */
   #define  Trace_IPCInit()                                    {}

   /** Defining the IPC function empty, if the tracing via IPC is disabled. */
   #define  Trace_IPC_ResetConnection()                        {}

   /** Defining the IPC function empty, if the tracing via IPC is disabled. */
   #define  GetTraceCommandIPC()                               {}

//   /** Defining the IPC function empty, if the tracing via IPC is disabled. */
//   #define  Trace_IPCChannelConnCB(channel, oldCnt, newCnt)    {}
//
//   /** Defining the IPC function empty, if the tracing via IPC is disabled. */
//   #define  Trace_IPCChannelRxCB(channel, msgs)                {}
//
//   /** Defining the IPC function empty, if the tracing via IPC is disabled. */
//   #define  Trace_IPCChannelTxCB(channel)                      {}

   /** Defining the IPC function empty, if the tracing via IPC is disabled. */
   #define  Trace_SendFrameIPC(tmpFrame)                       SEND_FAILED

#endif


#endif /* _TRACE_IPC_H_ */

