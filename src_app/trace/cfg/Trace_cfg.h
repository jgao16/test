/*****************************************************/
/* PLEASE MAKE A CLEAN BUILD IF YOU MODIFY THIS FILE*/
/*****************************************************/

#ifndef TRACE_CFG_H_H_
#define TRACE_CFG_H_H_

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
//include exten header
#include "std_type.h"
#include "std_lib.h"
#include "cpu.h"

#include "hal_os.h"

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#define TRACE_UART_EN		(1u)
#define TRACE_CAN_EN		(0u)
#define	TRACE_RAW_SPI_EN	(0u)
#define	TRACE_IPC_EN		(0u)



/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#define LOG_ENABLE 				0	// enable trace or not, must be define 
#define LOG_EEP_MOD_LVL_AVBL	0

#define VipLogNeedResetToInitState()	(RCM->SRS&0x082)	/* check is power on */
#define LogPhyTransmitCompleteEvent() do{trace_transmit_complete();}while(0)
#define LogPhyReceivedFrameEvent()	do{trace_received_frame();}while(0)
	
#if LOG_EEP_MOD_LVL_AVBL
#include "gen_eep_mana.h"
/*module level */
#define LogHandle_GetEepModLvl(x)		do{GenEep_Get_VipLogModLvl(x);}while(0)
#define LogHandle_SetEepModLvl(x)		do{GenEep_Set_VipLogModLvl(x);}while(0)
/*module crc */
#define LogHandle_GetEepModLvlCrc(x)	GenEep_Get_TraceModuleLvlCrc()
#define LogHandle_SetEepModLvlCrc(x)	do{GenEep_Set_TraceModuleLvlCrc(x);}while(0)
/*log phy channel */
#define LogPhy_GetLogChannel()		GenEep_Get_trace_phy()
#define LogPhy_SetLogChannel(x)		do{GenEep_Set_trace_phy(x);}while(0)

#else // !LOG_EEP_MOD_LVL_AVBL
#define LogHandle_GetEepModLvl(x)		do{(void)(x);}while(0)
#define LogHandle_SetEepModLvl(x)		do{(void)(x);}while(0)
#define LogHandle_GetEepModLvlCrc()	(0)
#define LogHandle_SetEepModLvlCrc(x)	do{(void)(x);}while(0)
#define LogPhy_GetLogChannel()		TRACE_COM_DEFAULT
#define LogPhy_SetLogChannel(x)		do{(void)(x);}while(0)	
#endif // {LOG_EEP_MOD_LVL_AVBL}	
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

/*==========================================================================================*/
/*                         TRACE_ALLOW_VARIABLE_GET_SET                                     */
/*==========================================================================================*/
/* 0 - Getting / Setting of variables triggered by IOCTraceClient isn't supported           */
/*      -> saves code in ROM !                                                              */
/* 1 - Getting / Setting of variables triggered by IOCTraceClient is supported              */
/*      -> more code in ROM space                                                           */
#define TRACE_ALLOW_VARIABLE_GET_SET                1

/*==========================================================================================*/
/*                         TRACE_ALLOW_CALLBACK_FUNCTION_CALLS                              */
/*==========================================================================================*/
/* 0 - Remote-call of Trace-callback functions isn't supported!                             */
/* 1 - Remote-call of Trace-callback functions enabled!                                     */
#define TRACE_ALLOW_CALLBACK_FUNCTION_CALLS                  1



#define TRACE_ALLOW_SEND_EVENTS 					0

/*==========================================================================================*/
/*                         TRACE_WAIT_FOR_CLIENT                                            */
/*==========================================================================================*/
/* 0 - Traces are send right from the Initialization of Trace-Server                        */
/* 1 - Trace-Server waits for any Client to connect, before Traces are send                 */
#define TRACE_WAIT_FOR_CLIENT                   0


#define	NO_INIT_DATA __no_init

/* Maximal number of Trace-messages kept in buffer */
#define TRACE_FIFO_SIZE                         250
/* Number of Trace-messages to transmit in dump-mode */
#define TRACE_HISTORY_COUNT                     100
/* if the fill-state of the buffer is greater than this percentage, a warning-message should be send to TraceClient */
#define TRACE_BUFFER_LEVEL_CRITICAL_THRESHOLD   90  // unit: %


/* Size of the Buffer for received Trace-frames. ONLY powers of 2 allowed for FIFOSIZERX ! */
#define FIFOSIZERX                              8 

#endif

/** >>>>>>>>>>>>>>>>>>>> end of file >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/



