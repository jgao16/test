#!/usr/bin/perl -l



#We expect 3 arguments to this script.
#Arg[1] should be the used compiler (e.g. EWARM, GCC, KEIL, GHC, etc.) .	ex: SW_COMPILER=COMPILER_EWARM/COMPILER_GCC
#Arg[2] should be the name of the sub-directory under which the output files will be generated.
#Arg[3] should be the name of the source-file to parse for LOG* macros.
#Arg[4] should be the name of the compiler-command file.	complier

use strict;
#use warnings;
use File::Compare;


($#ARGV+1 == 3) or die "log_parse.pl: Not enough arguments...\n";

#### Setting the Markers ######
my $TraceIDMarker = chr(180);
my $TraceTextMarker = chr(180);
my $TraceModuleMarker = chr(180);
my $TraceSwitchMarker = chr(180);
my $TraceSrcFileMarker = chr(180);
my $TraceTypeMarker = chr(180);
my $TraceLineNumberMarker = chr(180);

my $FunctionIDMarker = chr(180);
my $FunctionNameMarker = chr(180);

my $objpath;
my $cfile;
my $compiler;
my $IDfile;

# writing the passed arguments into variables
foreach(@ARGV)
{
   if (/OBJPATH/)               { $objpath = (split(/=/))[1]; }
   if (/C_FILE/)                { $cfile = (split(/=/))[1]; }
#   if (/CMD_FILE/)              { $cmdfile = (split(/=/))[1]; }	CMD_FILE=src_file_complier.cmd
   if (/SW_COMPILER/)           { $compiler = (split(/=/))[1]; }
#   if (/ID_FILE/)               { $IDfile = (split(/=/))[1]; }	 ID_FILE = $objpath/log_id.h
}


my @directories;
my @split;
my $FileName;
my $currentMainDirectory;
my $currentSubDirectory;

@directories = split /\//,$cfile;

if(@directories[0] =~ /\w/)
{
    if(!(@directories[0] =~ /\./) )
    {
        $currentMainDirectory = @directories[0];
    }
}
if(@directories[1] =~ /\w/)
{
    if(!(@directories[1] =~ /\./))
    {
        $currentSubDirectory = @directories[1];
    }
}

#search for the filename in the directory variable and create a filename.CNT file to save the file-number in
foreach(@directories)
{
    if ($_ =~m/\./)
    {
       $FileName = split /.c/,$_;
       @split = split /\.c/,$_;
       $FileName =  "@split[0]";
    }
}

my $sourcefileID_tr_File = $objpath."/".$FileName."._ID.tr";
my $sourcefileCBF_tr_File = $objpath."/".$FileName."._CBF.tr";


# Create an .CNT for the source-file
$cfile =~ /([a-zA-Z0-9_]*)\.c/;




$IDfile = $objpath."/"."vip_log_id.h";

#$RebuildIndicationfile  = $objpath."/Rebuild.indication.tr";

my $trswifile = $objpath."/"."trSwitch.h";
my $trverfile = $objpath."/"."log_version.txt";


my $current_TRACE_Count;
my $current_TRACECBF_Count;
my $trace_count_file = "$objpath/log_tracecount.tr";

my $tr_id_define = "";
my $source_file_tr_file="";
my $source_file_cbf_file="";


Get_CurrentLogCount();

parse_trace_log();
#parse_trace_callback();

#parse complier file 
Gene_ComplierCmdFile();
Gene_TraceVersion();
#open the files to file-handles
save_tr_cbf_log();


#re-write tr count and cbf count
open(UNIT_CNT_FILE, "> $trace_count_file") or die "Couldn't open UnitTraceCountFile: $trace_count_file!\n";
print UNIT_CNT_FILE $current_TRACE_Count."   ".$current_TRACECBF_Count;
close(UNIT_CNT_FILE);


sub Get_CurrentLogCount
{
	#if the  countfile mainUnit_subUnit.tracecount.tr already exists.
	if (open(UNIT_CNT_FILE, "< $trace_count_file") )
	{
		my @lines = <UNIT_CNT_FILE>;		# Read it into an array
		chomp(@lines);
		if ($lines[0] =~ /([0-9]*)   ([0-9]*)/)
		{
			$current_TRACE_Count = $1;
			$current_TRACECBF_Count = $2;
		}
		else
		{
			print "Error in the file ".$trace_count_file." occured: count not find count for Trace-IDs and Trace-CallbackFunctions!!!";
			system( 'pause' );
			exit;
		}
		close(UNIT_CNT_FILE);
	}
	else
	{
		open(UNIT_CNT_FILE, "> $trace_count_file") or die "Couldn't open UnitTraceCountFile: $trace_count_file!\n";
		$current_TRACE_Count = 0;
		$current_TRACECBF_Count = 0;
		print UNIT_CNT_FILE $current_TRACE_Count."   ".$current_TRACECBF_Count;
		close(UNIT_CNT_FILE);
	}
}


sub parse_trace_log
{
	open(C_FILE,   "< $cfile"    ) or die "Couldn't open cfile: $cfile!\n";
	
	#--------------------------------------------------------------------------------------------------------------------------------
    # Parse the source-file for TRACE_* calls.
    # For each TRACE_* call a Trace-ID is generated and written into the compiler-command file
    # With this definition of a "trversion" string the specific Trace-ID is placed into each TRACE_* call
    #--------------------------------------------------------------------------------------------------------------------------------
	while (my $cfileline = <C_FILE>)
	{
		if  ( $cfileline =~ /^\s*TRACE_(TEXT|SM_TEXT|VALUE|SM_VALUE|VALUE2|SM_VALUE2|VALUE4|SM_VALUE4|MEM|STRING)\s*\(/ )
		{
			my $savedlinenumber = $.;

			################################################ Trace-syntay check #################################################
			my $regEx_TEXT = "([\x20,\x21,\x23,\x24]|[\x26-\x5b]|[\x5d-\x7f]|(%%)|\x5c\x5c\x22|\x5c\x5c\x6e)*";
			my $regEx_VALUE = "(%-?0?[1-9]*[h,l,L]?[c,d,i,u,x,X])";

			$cfileline =~ /(\"(.*)\")/;       #"
			chomp ($cfileline);

			my $TRACE_MSG;
			if ($2)
			{
				$TRACE_MSG = $2;
			}
			else
			{
				$TRACE_MSG = $1;
			}

			# remove all "´" from the trace description, since this sign is used for formatting information in the Trace.tr file
			$TRACE_MSG =~ s/´//;
			$TRACE_MSG =~ s/\s+$//;             ## Remove trailing white spaces.

            # print "Trace message :>$TRACE_MSG<";
			if  ( $cfileline =~ /^\s*TRACE_TEXT\s*\(/ )
			{
				#print "current statement \"".$cfileline.\"";
				if  (!( $TRACE_MSG =~ m/($regEx_TEXT)/))
				{
					print "The following TRACE_TEXT call (file ".$cfile.", line ".$savedlinenumber.") has an invalid format:\n    >".$TRACE_MSG."< !!!\n";
                    $TRACE_MSG = "TRACE WARNING: The TRACE_TEXT call in file ".$cfile.", line ".$savedlinenumber." has an invalid format !!!";
				}
				else
				{
					# print "The following TRACE_TEXT/_SMTEXT call has a valid format: \n first match:>$1<\n\"".$TRACE_MSG." \"!!!\n";
				}
			}
            else
            {
				if  ( $cfileline =~ /^\s*TRACE_VALUE\s*\(/ )
				{
					#print "current statement \"".$cfileline.\"";
					if  (!( $TRACE_MSG =~ m/$regEx_TEXT(($regEx_VALUE)$regEx_TEXT)?/))
                    {
						print "The following TRACE_VALUE call (file ".$cfile.", line ".$savedlinenumber.") has an invalid format:\n   >".$TRACE_MSG."< !!!\n";
						$TRACE_MSG = "TRACE WARNING: The TRACE_VALUE call in file ".$cfile.", line ".$savedlinenumber." has an invalid format !!!";
					}
				}
				else
				{
					if  ( $cfileline =~ /^\s*TRACE_VALUE2\s*\(/ )
					{
						#print "current statement \"".$cfileline.\"";
						if  (!( $TRACE_MSG =~ m/$regEx_TEXT(($regEx_VALUE)$regEx_TEXT)?(($regEx_VALUE)$regEx_TEXT)?/))
						{
							print "The following TRACE_VALUE2 call (file ".$cfile.", line ".$savedlinenumber.") has an invalid format:\n   >".$TRACE_MSG."< !!!\n";
							$TRACE_MSG = "TRACE WARNING: The TRACE_VALUE2 call in file ".$cfile.", line ".$savedlinenumber." has an invalid format !!!";
						}
					}
					else
					{
						if  ( $cfileline =~ /^\s*TRACE_VALUE4\s*\(/ )
						{
							#print "current statement \"".$cfileline.\"";
							if  (!( $TRACE_MSG =~ m/$regEx_TEXT(($regEx_VALUE)$regEx_TEXT)?(($regEx_VALUE)$regEx_TEXT)?(($regEx_VALUE)$regEx_TEXT)?(($regEx_VALUE)$regEx_TEXT)?/))
							{
								print "The following TRACE_VALUE4 call (file ".$cfile.", line ".$savedlinenumber.") has an invalid format:\n   >".$TRACE_MSG."< !!!\n";
								$TRACE_MSG = "TRACE WARNING: The TRACE_VALUE4 call in file ".$cfile.", line ".$savedlinenumber." has an invalid format !!!";
							}
						}
					}
				}
			}

			############Searching for compile time switch.
			$cfileline =~ /\(\s*([a-zA-Z0-9_]*)\s*,/;
			my $COMP_SWITCH = $1;
			my $DEFINE_YES = "#define ".$COMP_SWITCH." 1";

            ############ Search for the compile_define in trSwitch.h
			#open(TRSWI_FILE, "< $trswifile") or die "Couldn't open trswifile: $trswifile!\n";
			#while (my $swfileline = <TRSWI_FILE>)
			{
				#if( $swfileline =~ /$DEFINE_YES/ )
				{
					############# Searching for Module Name
					$cfileline =~ /,\s*([a-zA-Z0-9_ ]*)\s*,/;
					my $MODULE_NAME = $1;

					############# Extracting the exact TRACE_ phrase
					$cfileline =~ /^\s*(.*?)\(/;
					my $TRACE_TYPE = $1;
					
					$current_TRACE_Count = $current_TRACE_Count + 1;
					my $remapped_count = $current_TRACE_Count;
					
					if ( $current_TRACE_Count > 65535 )
					{
						print "The Projet need to clean and rebuild..";
						exit;
					}
					#$source_file_tr_file .= ".$TraceIDMarker.$remapped_count."    ".$TraceTextMarker.$TRACE_MSG."    ".$TraceModuleMarker.$MODULE_NAME."    ".$TraceSwitchMarker.$COMP_SWITCH."    ".$TraceSrcFileMarker.$cfile."    ".$TraceTypeMarker.$TRACE_TYPE."    ".$TraceLineNumberMarker.$savedlinenumber;
					$source_file_tr_file .=  "#id      ".$TraceIDMarker.$remapped_count."    ".$TraceTextMarker.$TRACE_MSG."    ".$TraceModuleMarker.$MODULE_NAME."    ".$TraceSwitchMarker.$COMP_SWITCH."    ".$TraceSrcFileMarker.$cfile."    ".$TraceTypeMarker.$TRACE_TYPE."    ".$TraceLineNumberMarker.$savedlinenumber."\n";

					#Writing the Trace-ID's for the different TRACE_* calls into the compiler-command file.
					$tr_id_define .= "-Dtr_$FileName\_$savedlinenumber=$remapped_count\n";
					
					last;
				}
			}
		}


		# parse for TRACE_REGISTER_CALLBACK_FUNCTION calls
		if  ( $cfileline =~ /^\s*TRACE_REGISTER_CALLBACK_FUNCTION\s*\(/ )
		{
			my $savedlinenumber = $.;

			$cfileline =~ /\(\s*([a-zA-Z0-9_& ]*)\s*,/;
			my $FunctionName = $1;
			$FunctionName =~ s/&//g ;
			
			$current_TRACECBF_Count = $current_TRACECBF_Count + 1;
			my $remapped_count = $current_TRACECBF_Count;
			if ( $current_TRACE_Count > 65535 )
			{
				print "The Projet need to clean and rebuild..";
				exit;
			}
					
			#print CBF_FILE "#cbf     ".$FunctionIDMarker.$remapped_count."     ".$FunctionNameMarker.$FunctionName ;
			#print SOURCEFILE_CBF_FILE "#cbf     ".$FunctionIDMarker.$remapped_count."     ".$FunctionNameMarker.$FunctionName ;
			
			$source_file_cbf_file .= "#cbf     ".$FunctionIDMarker.$remapped_count."     ".$FunctionNameMarker.$FunctionName."\n";
			
			#Writing the Trace-ID's for the different TRACE_* calls into the compiler-command file.
			#print UNIT_CBF_HEADER_FILE "#define cbf_".$FileName."_".$savedlinenumber."   ".$remapped_count;
			$tr_id_define .= "-Dcbf_$FileName\_$savedlinenumber=$remapped_count\n";
		}
    }
	
	close(C_FILE);
}


sub Gene_ComplierCmdFile
{
	my $cmdfile;

	$compiler = (split(/_/, $compiler))[1];
	
	my $cmd="";
	if(( $compiler eq "EWARM" )||( $compiler eq "GCC" ))
	{
		$cmd .= "-D_TR_FILE=$FileName\n";
		$cmd .= "-D_FILECNT=0\n";
	}
	$cmd .= $tr_id_define;
	log_save_file("$objpath/$FileName\_complier.cmd", $cmd);
}

sub Gene_TraceVersion
{
	if( $cfile  =~ /trace_version\.c/i )
	{  
		my $szVersion;
		$szVersion = sprintf("%ld", time());
		# write sw version to sw_verion.txt
		my($sec,$min,$hour,$day,$mon,$year,$wday,$yday,$isdst)=localtime, $szVersion;
		$year=$year+1900-2000;
		$mon =$mon+1;
		my $sw_version = $objpath."/"."sw_version.txt";
		open(SW_VER,"> $sw_version") or die "Couldn't open sw_version: $sw_version!\n";
		printf SW_VER "%02d%02d%02d%02d%02d%02d_0x%x",$year,$mon,$day,$hour,$min,$sec,$szVersion;
		close(SW_VER);
	
		#Write the trace-Version information into the compiler-command file, so the trace-version in available in the compile-process
		print CMD_FILE "-DTRVERSION=".$szVersion."\n";
		printf CMD_FILE "-DSW_BUILT_TM=%02d%02d%02d%02d",$year,$mon,$day,$hour;
		open(TRVERFILE,"> $trverfile") or die "Couldn't open trverfile: $trverfile!\n";
		print TRVERFILE "\r\n(LOG VERSION)\r\n$szVersion\r\n(/LOG VERSION)\r\n";
		close(TRVERFILE);
		
		my $build_ = $objpath."/"."build_tm.h";
		open(BUILD_TM,"> $build_") or die "Couldn't open build_tm.h: $build_!\n";
		printf BUILD_TM "\r\n";
		printf BUILD_TM "#define BUILD_TM_LINUX %d\r\n",$szVersion;
		printf BUILD_TM "#define BUILD_TM_MS %02d%02d%02d%02d\r\n",$year,$mon,$day,$hour;
		close(BUILD_TM);
   }
}

sub save_tr_cbf_log
{
	$source_file_tr_file .= $source_file_cbf_file;
	if($source_file_tr_file ne "")
	{
		#log_save_file("$objpath/$FileName\_tr.log",$source_file_tr_file);
		log_save_file("$objpath/$FileName\.IDs.tr",$source_file_tr_file);
	}
	
	#if($source_file_cbf_file ne "")
	#{
	#	#log_save_file("$objpath/$FileName\_cbf.log",$source_file_cbf_file);
	#	log_save_file("$objpath/$FileName\.CBF.tr",$source_file_cbf_file);
	#}
	
#	IDs
}

sub log_read_file
{
	my ($path, $output) = @_;

	open(templatefile, $path);
	seek(templatefile, 0, 2);
	my $bytes = tell(templatefile);
	seek(templatefile, 0, 0);
	read(templatefile, $$output, $bytes, 0);
	close(templatefile);
}

sub log_save_file
{
	my ($path, @output) = @_;

	open(codefile, "> $path") or die "Couldn't open Output-File : $!\r\n";
	print codefile @output;
	close codefile;
}
