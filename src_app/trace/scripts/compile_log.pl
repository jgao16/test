#!/usr/bin/perl -l



use strict;
# use warnings;

($#ARGV+1 == 4) or die "compile_parse.pl: Not enough arguments...\n";

my $COMPILE_FILE;
my $TRVERSION_FILE;
my $MODULE_FILE;
my $CONFIG_FILE;
my $SWITCH_FILE;
my $OUTPUT_FILE;
my $TRACEID_FILE;
my $SYS_UART_CFG_FILE;
my $TASK_FILE;
my $TRACE_SWITCH_FILE;
my $RTOS;
my $DIP_FILE;
my $geraet;
my @lines;
my @keys;
my @sortedKeys;
my $tracesPerFile;
my $filesSupported;
my $ConfigSwitch;
my $ConfigSwitchValue;
my $UARTConfig;
my $UARTConfigValue;
my $modName;
my $modDesc;
my $modState;
my $switchName;
my $switchState;
my $starttag;
my $rest;
my $rest2;
my $rest3;
my $rest4;
my $rest5;
my $address;
my $bit;
my $varName;
my $srcFileName = 0;
my $dontCare;
my $dontCare2;
my $trVersion;
my $modprefix = "MOD_";
my $moduleBit;
my $byteOrder;
my $functionName;
my $functionAddress;
my $RTOS_CMX = "RTOS_CMX";
my $buildMachineName;
my $buildUserName;
my $buildDate;
my $buildTime;

my $usingBIOSTaskFile = 1;


#### Setting the Markers ######
my $versionMarker = chr(180);
my $deviceMarker = chr(180);
my $compilerNameMarker = chr(180);
my $moduleNameMarker = chr(180);
my $moduleStateMarker = chr(180);
my $moduleDescriptionMarker = chr(180);
my $switchNameMarker = chr(180);
my $switchStateMarker = chr(180);
my $variableNameMarker = chr(180);
my $variableAddressMarker = chr(180);
my $variableBitPositonMarker = chr(180);
my $variableSizeMarker = chr(180);
my $variableSrcFileMarker = chr(180);
my $moduleBitMarker = chr(180);
my $tracesPerFileMarker = chr(180);
my $filesSupportedMarker = chr(180);
my $byteOrderMarker = chr(180);
my $ConfigSwitchMarker = chr(180);
my $ConfigSwitchValueMarker = chr(180);
my $SysUartConfigMarker = chr(180);
my $SysUartConfigValueMarker = chr(180);
my $TaskMarker = chr(180);
my $TaskNumberMarker = chr(180);
my $EventMarker = chr(180);
my $EventNumberMarker = chr(180);
my $functionMarker = chr(180);
my $functionNameMarker = chr(180);
my $functionAddressMarker = chr(180);
my $buildMachineNameMarker = chr(180);
my $buildUserNameMarker = chr(180);
my $buildDateMarker = chr(180);
my $buildTimeMarker = chr(180);






my $objpath;
my $packagepath;
my $compiler;

# Create a variable for each passed argument
foreach(@ARGV)
{
   if (/OBJPATH/)       { $objpath = (split(/=/))[1]; }
   if (/PACKAGEPATH/)   { $packagepath = (split(/=/))[1]; }
   if (/SW_COMPILER/)   { $compiler = (split(/=/))[1]; }
   if (/GERAET/)        { $geraet = (split(/=/))[1]; }
   if (/CFG_FILE/)      { $CONFIG_FILE = (split(/=/))[1]; }
   if (/MOD_FILE/)  { $MODULE_FILE = (split(/=/))[1]; }
#   if (/UART_FILE/)  { $SYS_UART_CFG_FILE = (split(/=/))[1]; }
   if (/TRACE_SWITCH_FILE/)  { $TRACE_SWITCH_FILE = (split(/=/))[1]; }
   if (/RTOS/)  { $RTOS = (split(/=/))[1]; }
   if (/DIP_FILE/)  { $DIP_FILE = (split(/=/))[1]; }
}



# Create a filename-variable for the different compiler-output-files
$compiler = (split(/_/, $compiler))[1];

if( $compiler eq "KEIL" )   # KEIL Compiler
{
    $COMPILE_FILE = $objpath."/"."$geraet.M66";
    $byteOrder = "LITTLE";
}

if( $compiler eq "GHC" )    # Green Hills Compiler
{
    $COMPILE_FILE = "$packagepath/$geraet.map";
    $byteOrder = "LITTLE";
}

if( ($compiler eq "EWARM") ||  ($compiler eq "GCC"))    # Green Hills Compiler
{
    $COMPILE_FILE = "$packagepath/$geraet.map";
    $byteOrder = "LITTLE";
}

if( $compiler eq "CCS" )  # TI CodeComposerStudio DSP Compiler
{
    $COMPILE_FILE = $objpath."/"."$geraet.map";
    $byteOrder = "LITTLE";
}


#Creating a filename-variable for the Trace-configuration file Trace.tr
$OUTPUT_FILE = $objpath."/"."Trace.tr";

#Creating a filename-variable for the switch-configuration file TrSwitch.h
$SWITCH_FILE =  $objpath."/"."TrSwitch.h";

#Creating a filename-variable for the file trVersion.txt containing the trace-version
$TRVERSION_FILE = $objpath."/"."log_version.txt";

#Creating a filename-variable for the TraceID-configuration file Trace.nfo
$TRACEID_FILE = $objpath."/"."Log_Temp.id";


#Creating a filename-variable for the Task-information file tskhandl.tr
$TASK_FILE = $objpath."/"."tskhandl.tr";


#opening the necessary files...
open(OUTPUT_FILE, "> $OUTPUT_FILE") or die "Couldn't open Output-File : $!\n";
#open(MODULE_FILE,  $MODULE_FILE) or die "Couldn't open Module-File : $!\n";
#open(CONFIG_FILE,  $CONFIG_FILE) or die "Couldn't open Configuration-File : $!\n";
open(COMPILE_FILE, $COMPILE_FILE) or die "Couldn't open Compiler-File : $!\n";
open(TRVERSION_FILE,  $TRVERSION_FILE) or die "Couldn't open TraceVersion-File : $!\n";
#open(SWITCH_FILE,  $SWITCH_FILE) or die "Couldn't open Switch-File : $!\n";
open(TRACEID_FILE,  $TRACEID_FILE) or die "Couldn't open TraceID-File : $!\n";
#open(SYS_UART_CFG_FILE,  $SYS_UART_CFG_FILE) or die "Couldn't open SYS_UART_Config-File : $!\n";
#open(TRACE_SWITCH_FILE,  $TRACE_SWITCH_FILE) or die "Couldn't open TRACE_SWITCH_FILE-File : $!\n";
#open(DIP_FILE, $DIP_FILE) or die "Couldn't open Dip-File : $!\n";


#if(-e $TASK_FILE)
#{
#    #print "........... bios task-handle file tskhandl.tr is existing!";
#    open(TASK_FILE,  $TASK_FILE) or die "Couldn't open TASK_FILE-File : $!\n";
#    $usingBIOSTaskFile = 1;
#}
#else
#{
#    #print "........... bios task-handle file tskhandl.tr is NOT existing!";
#    $usingBIOSTaskFile = 0;
#    $TASK_FILE = $objpath."/"."tskhandl.def";
#    open(TASK_FILE,  $TASK_FILE) or die "Couldn't open TASK_FILE-File : $!\n";
#}






######################### Writing Trace-Version #####################

@lines = <TRVERSION_FILE>;		# Read it into an array
chomp(@lines);

$trVersion = $lines[2];

print  OUTPUT_FILE "#ver $versionMarker$trVersion";
printf  OUTPUT_FILE "--------------------------------------------------------------------------------------\n";




######################### Writing device information #####################

print  OUTPUT_FILE "#dev $deviceMarker$geraet";
printf  OUTPUT_FILE "--------------------------------------------------------------------------------------\n";


######################### Writing build information #####################

#@lines = <DIP_FILE>;		# Read it into an array
#chomp(@lines);
#
#foreach  (@lines)
#{
#   if ($_ =~ m/(\#define SW_COMPUTERNAME)\s*"(.+)"/)
#   {
#      $buildMachineName = $2;
#   }
#
#   if ($_ =~ m/(\#define SW_USERNAME)\s*"(.+)"/)
#   {
#      $buildUserName = $2;
#   }
#}
#
#$buildDate = sprintf("%04d-%02d-%02d",(localtime)[5]+1900,(localtime)[4]+1,(localtime)[3]);
#$buildTime = sprintf("%02d:%02d:%02d",(localtime)[2,1,3]);
#
#print  OUTPUT_FILE "#build   $buildMachineNameMarker$buildMachineName   $buildUserNameMarker$buildUserName   $buildDateMarker$buildDate   $buildTimeMarker$buildTime";
#printf  OUTPUT_FILE "--------------------------------------------------------------------------------------\n";




######################### Writing abbreviation of used compiler #####################

print  OUTPUT_FILE "#comp $compilerNameMarker$compiler";
printf  OUTPUT_FILE "--------------------------------------------------------------------------------------\n";



######################### Writing information of Byte-order used #####################

print  OUTPUT_FILE "#byte_order $byteOrderMarker$byteOrder";
printf  OUTPUT_FILE "--------------------------------------------------------------------------------------\n";

######################### Parsing Trace.swi for Trace-Configuration switches #####################
#@lines = <TRACE_SWITCH_FILE>;		# Read it into an array
#chomp(@lines);
#my $config;
#foreach  (@lines)
#{
#   if ($_ =~ m/SW_TR/)
#   {
#       ($config,$dontCare) = split  /#/, $_;

#       $config =~ s/\s+/ /g;        # remove all spaces and tabs.
#       ($ConfigSwitch,$ConfigSwitchValue) = split  /=/, $config;

#        $ConfigSwitch =~ s/^\s+//g ;         # remove preliminary whitespaces
#        $ConfigSwitchValue =~ s/^\s+//g ;         # remove preliminary whitespaces
#        printf OUTPUT_FILE "#Configuration-switch      $ConfigSwitchMarker$ConfigSwitch    $ConfigSwitchValueMarker$ConfigSwitchValue\n";
#    }
#}
#printf  OUTPUT_FILE "--------------------------------------------------------------------------------------\n";


######################### Parsing Trace.cfg for Trace-Configuration switches #####################

#@lines = <CONFIG_FILE>;		# Read it into an array
#chomp(@lines);

#my $config;
#my $define = "#define";

#foreach  (@lines)
#{
#    if ($_ =~ m/$define/)
#    {
#       ($dontCare,$config) = split  /$define/, $_;

#       $config =~ s/\s+/ /g;
#       ($dontCare,$ConfigSwitch,$ConfigSwitchValue) = split  / /, $config;

#        $ConfigSwitch =~ s/^\s+//g ;         # remove preliminary whitespaces
#        $ConfigSwitchValue =~ s/^\s+//g ;         # remove preliminary whitespaces
#        printf OUTPUT_FILE "#Configuration-switch      $ConfigSwitchMarker$ConfigSwitch    $ConfigSwitchValueMarker$ConfigSwitchValue\n";
#    }
#}
#printf  OUTPUT_FILE "--------------------------------------------------------------------------------------\n";


######################### Parsing sysUART.cfg for UART-Configuration  #####################

#printf OUTPUT_FILE "#UART-Configuration      �BAUDRATE    �HAL_BTR_57K6BAUD\n";
#printf OUTPUT_FILE "#UART-Configuration      �DATABITS    �UART_DATABITS_8\n";
#printf OUTPUT_FILE "#UART-Configuration      �STOPBITS    �UART_STOPBITS_1\n";
#printf OUTPUT_FILE "#UART-Configuration      �PARITY    �UART_PARITY_NONE\n";

#@lines = <SYS_UART_CFG_FILE>;		# Read it into an array
#chomp(@lines);
#
#my $config;
#my $define = "#define";
#
#foreach  (@lines)
#{
#    if ($_ =~ m/$define/)
#    {
#       ($dontCare,$config) = split  /$define/, $_;
#
#       if ($config =~ m/SYS_UART_TRACE_/)
#       {
#           $config =~ s/SYS_UART_TRACE_//g;
#
#           $config =~ s/\s+/ /g;
#           ($dontCare,$UARTConfig,$UARTConfigValue) = split  / /, $config;
#
#            $UARTConfig =~ s/^\s+//g ;         # remove preliminary whitespaces
#            $UARTConfigValue =~ s/^\s+//g ;         # remove preliminary whitespaces
#            printf OUTPUT_FILE "#UART-Configuration      $SysUartConfigMarker$UARTConfig    $SysUartConfigValueMarker$UARTConfigValue\n";
#
#
#        }
#    }
#}
printf  OUTPUT_FILE "--------------------------------------------------------------------------------------\n";




########################## Append Task and Event information from file tskhandl.tr  #####################
#if($usingBIOSTaskFile == 1)
#{
##    print ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> using BIOS generated task-handle-file...";
#    while (<TASK_FILE>) {
#	    chomp;
#        print OUTPUT_FILE $_ ;    # simply copy all Task and event information
#    }
#    printf  OUTPUT_FILE "--------------------------------------------------------------------------------------\n";
#}
#else
#{
##    print ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> generating own task-handle-file...";
#    if ($RTOS =~ m/$RTOS_CMX/)
#    {
##        print ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> generating own task-handle-file for CMX OS";
#        @lines = <TASK_FILE>;		# Read it into an array
#        chomp(@lines);
#
#        my $EVENTDEF = "EVENT_";
#        my $TASKDEF = "TaskID_";
#
#        my $TaskName;
#        my $TaskNumber;
#        my $EventName;
#        my $EventNumber;
#
#        foreach  (@lines)
#        {
#            # parse for task-definitions
#            if ($_ =~ m/#define\s+$TASKDEF/)
#            {
#               ($dontCare,$TaskName) = split  /#define\s+$TASKDEF/, $_;   #remove "#define TaskID_"
#
#               ($TaskName,$TaskNumber) = split  /\s+/, $TaskName;
#
#                $TaskName =~ s/^\s+//g ;         # remove preliminary whitespaces
#                $TaskName =~ s/^\s+//g ;         # remove preliminary whitespaces
#
#                $TaskNumber =~ s/^\s+//g ;         # remove preliminary whitespaces
#                $TaskNumber =~ s/^\s+//g ;         # remove preliminary whitespaces
#
#                printf OUTPUT_FILE "#Task      $TaskMarker$TaskName    $TaskNumberMarker$TaskNumber\n";
#            }
#
#            else
#            {
#                #parse for Event definitions
#                if ($_ =~ m/#define\s+$EVENTDEF/)
#                {
#                    ($dontCare,$rest) = split  /#define\s+$EVENTDEF/, $_;    #remove "#define EVENT_"
#
#                    ($TaskName, $dontCare) = split  /_/, $rest;   # split at "_" to get the taskname
#
#                    $EventName = $rest;                           # remove the taskname from the input
#                    $EventName =~ s/$TaskName"_"//g ;
#
#                    ($EventName, $EventNumber) = split  /\s+/, $EventName;   # spilt at whitespace to get eventname and eventnumber
#
#                    $TaskName =~ s/^\s+//g ;         # remove preliminary whitespaces
#                    $TaskName =~ s/^\s+//g ;         # remove preliminary whitespaces
#
#                    $EventName =~ s/^\s+//g ;         # remove preliminary whitespaces
#                    $EventName =~ s/^\s+//g ;         # remove preliminary whitespaces
#
#                    $EventNumber =~ s/^\s+//g ;         # remove preliminary whitespaces
#                    $EventNumber =~ s/^\s+//g ;         # remove preliminary whitespaces
#
#                    printf OUTPUT_FILE "#Event      $TaskMarker$TaskName   $EventMarker$EventName     $EventNumberMarker$EventNumber\n";
#                }
#            }
#        }
#        printf  OUTPUT_FILE "--------------------------------------------------------------------------------------\n";
#    }
#}


######################### Parsing Tr_Modules.cfg for Trace-ratio #####################

@lines = <MODULE_FILE>;		# Read it into an array
chomp(@lines);
=pod
my $search_tag_TPF = "TRACES_PER_FILE";
my $search_tag_SF = "TR_FILES_SUPPORTED";

foreach  (@lines)
{
    if ($_ =~ m/define/)
    {
        if ($_ =~ m/$search_tag_TPF/)  # Search for symbols marking the Traces-per-file value
        {
           ($dontCare,$tracesPerFile,$rest) = split  /$search_tag_TPF/, $_;
        }

        if ($_ =~ m/$search_tag_SF/)  # Search for symbols marking the supported-files value
        {
           ($dontCare,$filesSupported,$rest) = split  /$search_tag_SF/, $_;

            $tracesPerFile =~ s/^\s+//g ;         # remove preliminary whitespaces
            $filesSupported =~ s/^\s+//g ;         # remove preliminary whitespaces
            printf OUTPUT_FILE "#ratio      $tracesPerFileMarker$tracesPerFile    $filesSupportedMarker$filesSupported\n";
        }
    }
}
printf  OUTPUT_FILE "--------------------------------------------------------------------------------------\n";
=cut



######################### Parsing Tr_Modules.cfg for Trace-Modules #####################

=pod
my $search_tag_mod = "MOD_";
my $moduleBit = 0;

foreach  (@lines)
{
    if ($_ =~ m/\($search_tag_mod/)  # Search for symbols marking module-names
    {
       $moduleBit++;

       if ($moduleBit >32)
       {
            print "!! ---------------------------------- TRACE-WARNING ---------------------------------- !!\n";
            print "!!     More than 32 Trace-Modules were defined in file /cfg/Trace/Tr_Modules.cfg       !!\n";
            print "!!           There must be  exactly 32 different Trace-Modules defined                 !!\n";
            print "!! ---------------------------------- TRACE-WARNING ---------------------------------- !!\n";
       }
       ($dontCare, $rest) = split  /MOD_/, $_;
       ($modName,$rest) = split  /,/, $rest;
       ($modState,$rest) = split  /\)/, $rest;
       ($dontCare,$modDesc,$rest) = split  /\*/, $rest;

       $modDesc =~ s/^\s+//g ;         # remove preliminary whitespaces

       $modState =~ s/^\s+//g ;         # remove preliminary whitespaces
       printf OUTPUT_FILE "#mod      $moduleNameMarker$modprefix$modName    $moduleStateMarker$modState    $moduleDescriptionMarker$modDesc    $moduleBitMarker$moduleBit\n";
    }
}

if ($moduleBit <32)
{
    print "!! ---------------------------------- TRACE-WARNING ---------------------------------- !!\n";
    print "!!     Less than 32 Trace-Modules were defined in file /cfg/Trace/Tr_Modules.cfg       !!\n";
    print "!!           There must be exactly 32 different Trace-Modules defined                  !!\n";
    print "!! ---------------------------------- TRACE-WARNING ---------------------------------- !!\n";
}

printf  OUTPUT_FILE "--------------------------------------------------------------------------------------\n";
=cut

######################### Parsing trswitch.h for Trace-Switches #####################

@lines = <SWITCH_FILE>;		# Read it into an array
chomp(@lines);

my $search_tag_switch0 = " 0";
my $search_tag_switch1 = " 1";
my $search_tag_switch2 = " 2";
my $search_tag_switch3 = " 3";

foreach  (@lines)
{
    if  ($_ =~ m/$search_tag_switch0/)    # Search for switch-definitions with switch "off" ( == 0 )
    {
       ($dontCare, $switchName,$switchState, $rest) = split  / /, $_;
       print OUTPUT_FILE "#swi      ".$switchNameMarker.$switchName."    ".$switchStateMarker."0";
    }
    if  ($_ =~ m/$search_tag_switch1/)    # Search for switch-definitions with switch "on" ( == 1 )
    {
       ($dontCare, $switchName,$switchState, $rest) = split  / /, $_;
       print OUTPUT_FILE "#swi      ".$switchNameMarker.$switchName."    ".$switchStateMarker."1";
    }
    if  ($_ =~ m/$search_tag_switch2/)    # Search for switch-definitions with switch "on" ( == 2 )
    {
       ($dontCare, $switchName,$switchState, $rest) = split  / /, $_;
       print OUTPUT_FILE "#swi      ".$switchNameMarker.$switchName."    ".$switchStateMarker."2";
    }
    if  ($_ =~ m/$search_tag_switch3/)    # Search for switch-definitions with switch "on" ( == 3 )
    {
       ($dontCare, $switchName,$switchState, $rest) = split  / /, $_;
       print OUTPUT_FILE "#swi      ".$switchNameMarker.$switchName."    ".$switchStateMarker."3";
    }
}
printf  OUTPUT_FILE "--------------------------------------------------------------------------------------\n";




######################### Appending TraceID information #####################

while (<TRACEID_FILE>) {
	chomp;
    print OUTPUT_FILE $_ ;    # simply copy all Trace-ID information from Trace.nfo
}
printf  OUTPUT_FILE "--------------------------------------------------------------------------------------\n";



######################### Parsing Compiler-Output file for Variables  and Trace Callback functions ("TR_CB_xxxx") #####################

my %varHash;
my $varInfo;

# Each compiler has a differently structured output-map file
# So for every output-map file the parsing has to be done differently


# KEIL Compiler
if( $compiler eq "KEIL" )
{
    #defining expressions to search for, which indicate variables
    my $search_tag_var = "  VAR   -";
    my $search_tag_var2 = "   VAR     ";
    my $search_tag_bit = "  BIT   -";

    @lines = <COMPILE_FILE>;		# Read it into an array
    chomp(@lines);


    foreach  (@lines)
    {
        if(!($_ =~ m/\?/))       # skip variables containing questionmarks
        {
            if ($_=~m/$search_tag_var2/)     # Search for Symbols marking Variables
            {
                ($starttag,$rest)= split /      /,$_;
                ($address, $rest2) = split /H     /,$rest;
                ($dontCare,$dontCare2,$varName,$rest3) = split /---/,$_;
                $varName =~ s/^\s+//g ;         # remove preliminary whitespaces
                $address =~ s/^\s+//g ;         # remove preliminary whitespaces
                $varName =~ s/^\s+//;           # remove trailing whitespaces
                $varName =~ s/\s+$//;
                $address =~ s/^\s+//;           # remove trailing whitespaces
                $address =~ s/\s+$//;
                $varInfo = sprintf "$variableAddressMarker$address";
                $varHash{$varName} = $varInfo;  # Create a hash with the variable names, to be able to sort it
            }
        }
    }


    foreach  (@lines)
    {
        if(!($_ =~ m/\?/))    # skip variables containing questionmarks
        {
            if ($_=~m/$search_tag_var/)     # Search for Symbols marking Variables
            {
                ($starttag,$rest)= split /      /,$_;
                ($address, $rest2) = split /H     /,$rest;
                ($varName,$rest3) = split /  VAR/,$rest2;
                ($dontCare,$dontCare2,$srcFileName) = split /\?/,$_;
                $srcFileName =~ s/^\s+//g ;         # remove preliminary whitespaces
                $srcFileName =~ s/^\s+//;           # remove trailing whitespaces
                $srcFileName =~ s/\s+$//;
                $varName =~ s/^\s+//g ;         # remove preliminary whitespaces
                $address =~ s/^\s+//g ;         # remove preliminary whitespaces
                $varName =~ s/^\s+//;           # remove trailing whitespaces
                $varName =~ s/\s+$//;
                $address =~ s/^\s+//;           # remove trailing whitespaces
                $address =~ s/\s+$//;
                $varInfo = sprintf "$variableAddressMarker$address    $variableSrcFileMarker$srcFileName";
                $varHash{$varName} = $varInfo;  # Create a hash with the variable names, to be able to sort it
            }
        }
    }

    # sort the Variable-list in alphabetical order
    @keys = keys %varHash;
    @sortedKeys = sort {lc $a cmp lc $b} @keys;

    foreach $varName (@sortedKeys){
        printf OUTPUT_FILE "#var      $variableNameMarker$varName     $varHash{$varName}\n";
    }


    printf  OUTPUT_FILE "--------------------------------------------------------------------------------------\n";


    foreach  (@lines)
    {
        if ($_=~m/$search_tag_bit/)     # Search for Symbols marking BIT-Variables
        {
            #print $_;
            ($starttag,$rest)= split /      /,$_;
            ($address, $rest) = split /H\./,$rest;
            ($bit, $rest) = split /   /,$rest;
            ($varName,$rest) = split /  BIT/,$rest;
            printf OUTPUT_FILE "#bit      $variableNameMarker$varName    $variableAddressMarker$address    $variableBitPositonMarker$bit\n";
        }
    }
}


# Green Hills Compiler
if( $compiler eq "GHC" )
{
   my $lastlinevariable;
   my $lastlinesection = '';
   my %sectionends = ();

   while ( <COMPILE_FILE> ) {
      next unless (/^\s+\./);          # remove unnecessary  compiler information which matches the filter

      #  .rodata              0002361c  000014bc         5308   00038fc
      if(/\.(\w+)\s+([0-9a-f]+)\s+([0-9a-f]+)\s+\d+\s+[0-9a-f]+/) {
          $sectionends{$1} = (hex $2) + (hex $3);
      }

      # .small           ffff5076 _waitForReferenceVoltage..D.3A.5CP4Work.5CSYMC_W200.5C7619.5Csysadc.
      if (/\.([\w_]+)\s+([a-fA-F0-9]+)\s+_([\w_]+)(.*5C([\w_]+)\.)?/) {

         my $address = hex $2;
         my $varname = $3;
         my $srcFileName = $5 ? $5 : '';

         next if ($address < 0x200000);
         next if ($varname =~ /^_/);

         if ($lastlinesection eq $1) {
            $varHash{$lastlinevariable}{SIZE} = $address - hex $varHash{$lastlinevariable}{ADDRESS};
         } else {
            $varHash{$lastlinevariable}{SIZE} = $sectionends{$lastlinesection} - hex $varHash{$lastlinevariable}{ADDRESS} if defined $lastlinevariable;
         }
         $varHash{$varname} = {ADDRESS => sprintf("%8x",$address), SRCFILENAME => $srcFileName};

         $lastlinesection = $1 if ($lastlinesection ne $1);
         $lastlinevariable = $varname;
      }
   }

   delete $varHash{$lastlinevariable};

   # sort the Variable-list in alphabetical order
   my @variablelist = keys %varHash;
   @variablelist = sort {lc $a cmp lc $b} @variablelist;

  # printf  OUTPUT_FILE "--------------------------------------------------------------------------------------\n";

   foreach my $varname (@variablelist) {
      printf OUTPUT_FILE "#var      $variableNameMarker$varname      ";
      printf OUTPUT_FILE "$variableAddressMarker%x$varHash{$varname}{ADDRESS}      ";
      printf OUTPUT_FILE "$variableSrcFileMarker$varHash{$varname}{SRCFILENAME}      ";
      printf OUTPUT_FILE "$variableSizeMarker$varHash{$varname}{SIZE}\n";
   }
}

# TI CodeComposer Studio DSP Compiler
if( $compiler eq "CCS" )
{
    my $i;
    my $match_idx;
    my $match_idx2;
    my $search_tag1 = "SORTED ALPHABETICALLY BY Name";
    my $search_tag2 = "SORTED BY Symbol Address";

    @lines = <COMPILE_FILE>;		# Read it into an array
    chomp(@lines);

    for($i = 0; $i < @lines; $i++)
    {
        if ($lines[$i] =~ /$search_tag1/ )
        {
            $match_idx = $i;
            last;
        }
    }

    print "$match_idx";
    $match_idx = $match_idx + 4;

    for($i = $match_idx; $i < @lines; $i++)
    {
        if ($lines[$i] =~ /$search_tag2/ )
        {
            $match_idx2 = $i;
            last;
        }
    }

# TI DSP Compiler
    print "$match_idx2";
    for ($i = $match_idx; $i < $match_idx2; $i++)
    {
        ($address, $varName ,$rest) = split( /   /,$lines[$i] );

        chomp($varName);
        printf OUTPUT_FILE "#var      $variableNameMarker$varName     $variableAddressMarker$address\n";
    }
}


printf  OUTPUT_FILE "\n";
printf  OUTPUT_FILE "--------------------------------------------------------------------------------------\n";
printf  OUTPUT_FILE "--------------------------------------------------------------------------------------\n";
close(COMPILE_FILE);			# Close the file


