#!/usr/bin/perl -l



#We expect 3 arguments to this script. to generate log cfg file and trace handle.c 
#Arg[1] should be the name of the sub-directory under which the output files will be generated.
#Arg[2] should be the name of the source-file to parse for LOG* macros. CFG_FILE = ../*.xml
#Arg[3] should be the name of the sub-directory of templates, TEMPLATES_PATH = ../../

#output: log_cfg.h,trace_handle.c,log_module.id

use strict;
#use warnings;
use File::Compare;
use XML::Simple;
use Data::Dumper;

($#ARGV+1 == 3) or die "log_parse.pl: Not enough arguments...\n";

#### Setting the Markers ######
my $LogMarker = chr(180);

my $objpath;
my $cfg_file;
my $template_path;
#my $cfile;
#my $compiler;
#my $IDfile;
my $log_mod_id = "--------------------------------------------------------------------------------------\n";
my $new_mod_id = "--------------------------------------------------------------------------------------\n";
my $log_swi    = "--------------------------------------------------------------------------------------\n";
my $log_lvl_define = "";
my $log_lvl_declear= "";
my $log_handle_init="";
my $set_mod_log_lvl="";
my $get_mod_log_lvl="";
my $get_mod_lvl_sig="";
my $log_mod_lvl_set="";
my $output_mod_log_lvl="";

# writing the passed arguments into variables
foreach(@ARGV)
{
   if (/OBJPATH/)               { $objpath = (split(/=/))[1]; }
   if (/CFG_FILE/)              { $cfg_file = (split(/=/))[1]; }
   if (/TEMPLATES_PATH/)        { $template_path = (split(/=/))[1]; }
#   if (/SW_COMPILER/)           { $compiler = (split(/=/))[1]; }
#   if (/ID_FILE/)               { $IDfile = (split(/=/))[1]; }	 ID_FILE = $objpath/log_id.h
}

my $xmlreader 	= XML::Simple->new(ForceArray => 1, KeyAttr => { item => });
my $log_cfg 	= $xmlreader->XMLin($cfg_file);

my @log_mod 		= @{$log_cfg->{log_mode}};
my @log_swi 		= @{$log_cfg->{log_swi}};
my @define 		    = @{$log_cfg->{define}};
my $log_id			= ${$log_cfg->{log_id}}[0];

my $log_count = "$log_id->{tr_id_start}   $log_id->{cbf_id_start}";
log_save_file("$objpath/log_tracecount.tr",$log_count);

my $mod_name = "";
my $cfg_define ="";
my $log_mod_num = 1;
my $log_mod_lvl_idx=0;
my $en = "1";
for(@log_mod)
{
	$mod_name 		.= 	"	$_->{name}, /*** $_->{interpret} ***/\n";
	$cfg_define		.= 	"#define $_->{name}_DefaultLevel\t\t\t$_->{dflt_lvl} /*** $_->{interpret} ***/\n";
	
	#$log_mod_id     .= "#mod      $LogMarker$_->{name}    $LogMarker$_->{dflt_lvl}    $LogMarker$log_mod_lvl_idx:$_->{interpret}    $LogMarker$log_mod_num\n";
	$log_mod_id     .= "#mod      $LogMarker$_->{name}    $LogMarker$en    $LogMarker$log_mod_lvl_idx:$_->{interpret}    $LogMarker$log_mod_num\n";
	$new_mod_id     .= "#log_mod  $LogMarker$_->{name}    $LogMarker$_->{dflt_lvl}    $LogMarker$log_mod_lvl_idx:$_->{interpret}    $LogMarker$log_mod_lvl_idx\n";	
	$log_mod_num ++;
	$log_mod_lvl_idx++;
	
	$log_lvl_define .= "__no_init uint8_t $_->{name}\_Lvl;\n";
	$log_lvl_declear.= "extern uint8_t $_->{name}\_Lvl;\n";
	$log_handle_init.= "	$_->{name}\_Lvl=$_->{name}_DefaultLevel;\n";
	$set_mod_log_lvl.= "\tif(module_id==$_->{name}/*&&$_->{name}\_Lvl!=log_lvl*/){\n\t\t$_->{name}\_Lvl=log_lvl; mod_lvl[$_->{name}]=log_lvl;\n\t\tTRACE_VALUE2(TRACE_SWITCH_ALWAYS, MOD_ALWAYS, \" ----- Log-Info:SetModuleLogLvl($_->{name}) module_id=%d,log_level=%d ...\", module_id,log_lvl);\n\t}\n\n";
	$get_mod_log_lvl.= "\tif(module_id==$_->{name}){\n\t\tlog_lvl=$_->{name}\_Lvl;\n\t}\n\n";
	
	$output_mod_log_lvl .= "\tTRACE_VALUE2(LOG_SHOUT, MOD_ALWAYS, \" ----- Log-Info:OutputModuleLogLvl($_->{name}) module_id=%d,log_level=%d ...\", $_->{name},$_->{name}\_Lvl);\n";
	
	$get_mod_lvl_sig .= "\t\t$_->{name}\_Lvl=$_->{name}_DefaultLevel;\n\t\tmod_lvl[$_->{name}]=$_->{name}\_Lvl;\n";
	
	$log_mod_lvl_set .= "#if LOG_ENABLE && LOG_ENABLE \n";
	$log_mod_lvl_set .= "#define LogHandle_SetLogLvl_$_->{name}(lvl)	do{LogHandle_SetModuleLogLvl($_->{name},lvl);}while(0)\n";
	$log_mod_lvl_set .= "#else //!{LOG_ENABLE && LOG_ENABLE}\n";
	$log_mod_lvl_set .= "#define LogHandle_SetLogLvl_$_->{name}(lvl)	do{(void)(lvl);}while(0)\n";
	$log_mod_lvl_set .= "#endif //{LOG_ENABLE && LOG_ENABLE}\n\n";
	#$log_mod_lvl_set .= "void LogHandle_SetLogLvl_$_->{name}(uint8_t lvl);\n";
}

while($log_mod_lvl_idx < 32)
{
	my $dis = "0";
	$log_mod_id     .= "#mod      $LogMarker\MOD_UNUSED$log_mod_lvl_idx    $LogMarker$dis    $LogMarker$log_mod_lvl_idx:--Unused--    $LogMarker$log_mod_num\n";
	$log_mod_num ++;
	$log_mod_lvl_idx++;
}
if ($log_mod_num > 33)
{
	$cfg_define .="#error \"too many Log Module, only support 32 Log Modules\"\n";
}

for(@log_swi)
{
	$cfg_define		.= 	"#define $_->{name}\t\t\t$_->{level} /*** $_->{interpret} ***/\n";
	$log_swi        .=  "#swi      $LogMarker$_->{name}    $LogMarker$_->{level}\n";
}

for(@define)
{
	$cfg_define		.= 	"#define $_->{name}\t\t\t$_->{val} /*** $_->{interpret} ***/\n";
}

build_log_cfg_h();
build_log_mod_id_swi();
build_log_handle_c();
build_log_handle_h();


sub build_log_cfg_h
{
	my $log_cfg_h;
	log_read_file("$template_path/log_cfg_temp.h",     \$log_cfg_h);
	
	$log_cfg_h =~ s/<<DEFINE>>/$cfg_define/;
	$log_cfg_h =~ s/<<LOG_MODULE_NAME>>/$mod_name/;
	
	
	log_save_file("$objpath/log_cfg.h",$log_cfg_h);
}

sub build_log_mod_id_swi
{
	$log_mod_id .= "\n";
	$log_mod_id .= $log_swi;
	$log_mod_id .= $new_mod_id;
	$log_mod_id .= "--------------------------------------------------------------------------------------\n";
	log_save_file("$objpath/log_mod.id_swi",$log_mod_id);
}

sub build_log_handle_c
{
	my $log_handle_c;
	log_read_file("$template_path/log_handle_temp.c",     \$log_handle_c);
	
	$log_handle_c =~ s/<<MODULE_LOG_LEVEL_DEFINE>>/$log_lvl_define/;
	$log_handle_c =~ s/<<LOG_HANDLE_INIT>>/$log_handle_init/;
	$log_handle_c =~ s/<<SET_MODULE_LOG_LEVEL>>/$set_mod_log_lvl/;
	$log_handle_c =~ s/<<PROC_GET_MODULE_LVL_BY_ID>>/$get_mod_log_lvl/;	
	
	
	$log_handle_c =~ s/<<GET_MODULE_LOG_LEVEL_FUNC>>/$get_mod_lvl_sig/;
	
	$log_handle_c =~ s/<<LOG_OUTPUT_ALL_MOD_LVL>>/$output_mod_log_lvl/;
	
	log_save_file("$objpath/log_handle.c",$log_handle_c);
}

sub build_log_handle_h
{
	my $log_handle_h;
	log_read_file("$template_path/log_handle_temp.h",     \$log_handle_h);
	
	$log_handle_h =~ s/<<EXTERN_MODULE_LOG_LVL_DECLEAR>>/$log_lvl_declear/;
	#$log_handle_h =~ s/<<LOG_MODULE_NAME>>/$mod_name/;
	$log_handle_h =~ s/<<LOGHANDLE_SET_MOD_LOG_LVL>>/$log_mod_lvl_set/;
	
	log_save_file("$objpath/log_handle.h",$log_handle_h);	
}

sub log_read_file
{
	my ($path, $output) = @_;

	open(templatefile, $path);
	seek(templatefile, 0, 2);
	my $bytes = tell(templatefile);
	seek(templatefile, 0, 0);
	read(templatefile, $$output, $bytes, 0);
	close(templatefile);
}

sub log_save_file
{
	my ($path, @output) = @_;

	open(codefile, "> $path") or die "Couldn't open Output-File : $!\r\n";
	print codefile @output;
	close codefile;
}
