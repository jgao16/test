#!/usr/bin/perl


#We expect 3 arguments to this script. to generate log cfg file and trace handle.c 
#Arg[1] should be the name of the sub-directory under which the output files will be generated, ../obj
#Arg[2] should be the name of the source-file to parse for LOG* macros. CFG_FILE = ../*.xml
#Arg[3] should be the name of the sub-directory of templates, TEMPLATES_PATH = ../../

use strict;
use XML::Simple;
use Data::Dumper;

my $map;
my $code_data;


system "sed '1,/^Entry/d' n331_vip.map > temp1.map";
#system "sed ':label;N;s/\\r\\n  /  /;t label' temp1.map > code_data.map";
#system "sed -n '/ Data /'p code_data.map > data.map";

read_file("temp1.map",\$map);
$map =~ s/[\r\n]  //gm;
$map =~ s/.+ Code .+//g;
$map =~ s/[\r\n]{1,}/\n/gm;

#$map =~ s/.+ Code .+//g;

save_file("code.map",$map);

sub read_file
{
	my ($path, $output) = @_;

	open(templatefile, $path);
	seek(templatefile, 0, 2);
	my $bytes = tell(templatefile);
	seek(templatefile, 0, 0);
	read(templatefile, $$output, $bytes, 0);
	close(templatefile);
}

sub save_file
{
	my ($path, @output) = @_;

	open(codefile, "> $path") or die "Couldn't open Output-File : $!\r\n";
	print codefile @output;
	close codefile;
}

