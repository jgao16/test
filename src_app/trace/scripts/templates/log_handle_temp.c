
#include "Trace_cfg.h"
#include "Trace_api.h"

//#include <stdio.h>           /* For sprintf() */
//#include <string.h>          /* for memset() */
#include "Trace_Common.h"
#include "Trace_Buffer.h"  /*For buffer management routines. */
#include "Trace_Server.h"
#include "Trace_Common.h"

#include "log_cfg.h"
#include "crc16.h"
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
//define CHECKSUM_INIT_VALUE (0xAA55)
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
/** current module-mask storing the state of all 16 modules */
//NO_INIT_DATA static uint32_t	Trace_ModuleMask_u32;
<<MODULE_LOG_LEVEL_DEFINE>>
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void LogHandle_Init(void)
{
<<LOG_HANDLE_INIT>>}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void LogHandle_SetModuleLogLvl(uint8_t module_id, uint8_t log_lvl)
{
	// GET EEPROM
	uint8_t mod_lvl[32];
	LogHandle_GetEepModLvl(mod_lvl);
	
<<SET_MODULE_LOG_LEVEL>>	
	// SET EEPROM
	LogHandle_SetEepModLvl(mod_lvl);
	//uint16_t crc = CHECKSUM_INIT_VALUE;
	//for ( uint8_t i = 0; i < 32; i ++ )
	//{
	//	crc += mod_lvl[i];
	//}
	uint16_t crc = Crc16_Calc(mod_lvl,32);
	LogHandle_SetEepModLvlCrc(crc);
}


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void LogHandle_OutputAllModLvl(void)
{
<<LOG_OUTPUT_ALL_MOD_LVL>>	
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if LOG_EEP_MOD_LVL_AVBL
void LogHandle_RestoreModuleLogLvlFromEep(void)
{
	uint8_t mod_lvl[32];
	
	LogHandle_GetEepModLvl(mod_lvl);
	
	//uint16_t crc = CHECKSUM_INIT_VALUE;//GenEep_Get_TraceModuleLvlCrc();	
	//for ( uint8_t i = 0; i < 32; i ++ )
	//{
	//	crc += mod_lvl[i];
	//}
	uint16_t crc = Crc16_Calc(mod_lvl,32);
	if ( crc ==  LogHandle_GetEepModLvlCrc() )
	{
		for ( uint8_t i = 0; i < LOG_MODULE_NAME_MAX; i ++ )
		{
			LogHandle_SetModuleLogLvl(i,mod_lvl[i]);
		}		
	}
	else
	{	// calc crc for module log level
<<GET_MODULE_LOG_LEVEL_FUNC>>
		//crc = CHECKSUM_INIT_VALUE;
		//for ( uint8_t i = 0; i < 32; i ++ )
		//{
		//	crc += mod_lvl[i];
		//}
		crc = Crc16_Calc(mod_lvl,32);
		/* write modlule log to eeprom */
		LogHandle_SetEepModLvl(mod_lvl);
		LogHandle_SetEepModLvlCrc(crc);
	}
}
#endif

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
/***********************************************************************************************/
/* Function Name   :    SetModuleMask(eTR_MODULE_SET_TYPE setType, uint32_t ModuleMask)          */
/* Description     :    This function sets the Module-mask.                                    */
/***********************************************************************************************/
/**  @brief  This function sets the Module-mask.
 *   @param  setType The type of module-mask set-operation
 *   @param  ModuleMask The module-mask to set
 */
void LogHandle_SetModuleMask(eTR_MODULE_SET_TYPE setType, uint32_t ModuleMask)
{
    // check, what type of set should be performed...
    switch(setType)
    {
    case NORMAL_SET:	// regular module mask set.
        /* Ensure that MOD_ALWAYS is always enabled.*/
        ModuleMask         |= (uint32_t)0x00000001;
		for ( uint8_t i = 0; i < LOG_MODULE_NAME_MAX; i ++ )
		{
			if ( ModuleMask & (1u<<i) ){
				LogHandle_SetModuleLogLvl(i,1);
			}else{
				LogHandle_SetModuleLogLvl(i,0);
			}
		}
		break;
   
    case DISABLE_ALL: // all modules should be disabled.
		for ( uint8_t i = 0; i < LOG_MODULE_NAME_MAX; i ++ )
		{
			LogHandle_SetModuleLogLvl(i,0);
		}		
		break;
		
    default:
		break;
    }
}

#if 0
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void 	SetModuleInitialMask(void)
{
	SetModuleMask(NORMAL_SET, Trace_ModuleMask_u32);
}

/***********************************************************************************************/
/* Function Name   :    Trace_GetConfiguredModuleMask(void)                                    */
/* Description     :    This function returns the default Module-mask as configured in         */
/*                      Tr_Modules.cfg.                                                        */
/***********************************************************************************************/
/**  @brief  This function returns the default Module-mask as configured in Tr_Modules.cfg.
 */
uint32_t Trace_GetConfiguredModuleMask(void)
{
    uint32_t returnValue = 0;
    uint32_t bitPos = 0x01;

#define TRACE_MODULE(ModuleNumber, DefaultValue)         \
      if( DefaultValue )     {returnValue |= bitPos;}       \
      TRACE_MODULE_##ModuleNumber##_bit = DefaultValue;     \
      bitPos = bitPos << 1;
#include <Trace_Defines.h>
#undef  TRACE_MODULE

    return(returnValue);
}

#endif

static uint8 ProcGetModuleLvl(uint8 module_id)
{
	uint8 log_lvl = 0x00;
	
<<PROC_GET_MODULE_LVL_BY_ID>>	
	
	return log_lvl;
}

void Trace_GetModuleLvl(uint8 channel,uint8 module_id)
{
	TRACE_FRAME tr_frame;

    tr_frame.trRawData[0] = TRACE_MODULE_MASK_HEADER;
	tr_frame.trRawData[1] = 0x00;
	tr_frame.trRawData[2] = channel;
	tr_frame.trRawData[3] = 0x01;
	tr_frame.trRawData[4] = 0x02;
	tr_frame.trRawData[5] = 0x00;
	tr_frame.trRawData[6] = module_id;
	tr_frame.trRawData[7] = ProcGetModuleLvl(module_id);
	
    TraceBuffer_AddFrame(&tr_frame);	
}

/***********************************************************************************************/
/* Function Name   :    Trace_Text(uint16_t TraceID_u16)                                         */
/* Description     :    Call this function to log a simple text message.                       */
/***********************************************************************************************/
/**  @brief  Call this function to log a simple text message.
 *   @param  TraceID_u16 The Trace-ID for the text to trace. This ID is generated automatocally with the compilation process.
 */
void Trace_Text(uint16_t TraceID_u16)
{
	Trace_TraceMacroCalledCounter++;
	
    //TRACE_FRAME trFrame;
	TRACE_FRAME tr_frame;

    tr_frame.trMSG.Trace_Header_u8		= TRACE_MESSAGE_HEADER;
    tr_frame.trMSG.TraceID_u16			= TraceID_u16;
	*((uint32_t*)(tr_frame.trMSG.Val))	= 0x00;
    TraceBuffer_AddFrame(&tr_frame);
}



/***********************************************************************************************/
/* Function Name   :  Trace_Value(uint16_t TraceID_u16, uint32_t Val_u32)                          */
/* Description     :  This function may be called when a text message ALONG WITH A 32 bit-VALUE*/
/*                    needs to be logged.                                                      */
/***********************************************************************************************/
/**  @brief  This function may be called when a text message ALONG WITH A 32 bit-VALUE needs to be logged.
 *   @param  TraceID_u16 The Trace-ID for the text to trace. This ID is generated automatocally with the compilation process.
 *   @param  Val_u32 The 32-bit value to trace.
 */
void Trace_Value(uint16_t TraceID_u16,
                 uint32_t Val_u32)
{
    //TRACE_FRAME trFrame;
    Trace_TraceMacroCalledCounter++;

    /* Check, if we have to wait until a client is connected */
    TRACE_FRAME tr_frame;

    tr_frame.trMSG.Trace_Header_u8       = TRACE_MESSAGE_HEADER;
    tr_frame.trMSG.TraceID_u16            = TraceID_u16;
    *((uint32_t*)(tr_frame.trMSG.Val))    = Val_u32;

    TraceBuffer_AddFrame(&tr_frame);
}

/************************************************************************************************/
/* Function Name    :  Trace_Value2(uint16_t TraceID_u16,   uint16_t Val1_u16,   uint16_t Val2_u16)   */
/* Description      :  Call this function to log a text message ALONG WITH 2 16 bit-VALUES      */
/************************************************************************************************/
/**  @brief  Call this function to log a text message ALONG WITH 2 16 bit-VALUES
 *   @param  TraceID_u16 The Trace-ID for the text to trace. This ID is generated automatocally with the compilation process.
 *   @param  Val1_u16 The first 16-bit value to trace.
 *   @param  Val2_u16 The second 16-bit value to trace.
 */
void Trace_Value2(uint16_t TraceID_u16,
                  uint16_t Val1_u16,
                  uint16_t Val2_u16)
{
    Trace_TraceMacroCalledCounter++;

    TRACE_FRAME tr_frame;
    tr_frame.trMSG.Trace_Header_u8       	= TRACE_MESSAGE_HEADER;
    tr_frame.trMSG.TraceID_u16            	= TraceID_u16;
    *((uint16_t*)(tr_frame.trMSG.Val)  )  	= Val1_u16;
    *((uint16_t*)((tr_frame.trMSG.Val)+2))	= Val2_u16;

    TraceBuffer_AddFrame(&tr_frame);
}




/***********************************************************************************************/
/* Function Name     :  Trace_Value4( uint16_t TraceID_u16, uint8_t Val1_u8,                       */
/*                                   uint8_t Val2_u8,       uint8_t Val3_u8,      uint8_t Val4_u8)   */
/* Description       :  Call this function to log a text message ALONG WITH 4 8 bit-VALUES     */
/***********************************************************************************************/
/**  @brief  Call this function to log a text message ALONG WITH 4 8 bit-VALUES
 *   @param  TraceID_u16 The Trace-ID for the text to trace. This ID is generated automatocally with the compilation process.
 *   @param  Val1_u8 The first 8-bit value to trace.
 *   @param  Val2_u8 The second 8-bit value to trace.
 *   @param  Val3_u8 The third 8-bit value to trace.
 *   @param  Val4_u8 The fourth 8-bit value to trace.
 */
void Trace_Value4(uint16_t TraceID_u16,
                  uint8_t  Val1_u8,
                  uint8_t  Val2_u8,
                  uint8_t  Val3_u8,
                  uint8_t  Val4_u8)
{
    //TRACE_FRAME trFrame;
    Trace_TraceMacroCalledCounter++;

    TRACE_FRAME tr_frame;
    tr_frame.trMSG.Trace_Header_u8	= TRACE_MESSAGE_HEADER;
    tr_frame.trMSG.TraceID_u16		= TraceID_u16;
    tr_frame.trMSG.Val[0]			= Val1_u8;
    tr_frame.trMSG.Val[1] 			= Val2_u8;
    tr_frame.trMSG.Val[2]			= Val3_u8;
    tr_frame.trMSG.Val[3]			= Val4_u8;

    TraceBuffer_AddFrame(&tr_frame);

}



/************************************************************************************************/
/* Function Name     :  Trace_Mem( uint16_t TraceID_u16,   uint8_t Length,    char * dataPointer     )      */
/* Description       :  Call this function to log complete block of memory                      */
/************************************************************************************************/
/**  @brief   Call this function to log complete block of memory
 *   @param  TraceID_u16 The Trace-ID for the text to trace. This ID is generated automatocally with the compilation process.
 *   @param  Length Number bytes to log
 *   @param  dataPointer Pointer to the start-address of of the memory-block to trace.
 */
void Trace_Mem(uint16_t  TraceID_u16,
               uint8_t   Length,
               const uint8_t *  dataPointer)
{
    uint8_t  noOfFrames;// = 0;
    uint8_t  DataID;
    
    Trace_TraceMacroCalledCounter++;
	
	if ( dataPointer != NULL && Length > 0 )
	{
		/* If more than 96 bytes should be traced with this TRACE_MEM call transfer only 96 */
		if ( Length > MAX_BYTES_PER_TRACE_MEM )             /* totally 16 * 6 = 96 byte can be transferred in one TRACE_MEM call*/
		{
			/* 16 Data-Frames may be transmitted in one TRACE_MEM call */
			TRACE_VALUE(TRACE_SWITCH_ALWAYS, MOD_ALWAYS, "################# TRACE_MEM or TRACE_STRING called with %d bytes - maximum 96 Bytes !! ", Length);
		}
		else
		{	
			
			/* Calculate how many Data-Frames have to be used for this TRACE_MEM call and how many bytes the last frame will contain */
			noOfFrames = (Length+BYTES_PER_DATA_FRAME-1) / BYTES_PER_DATA_FRAME ;       /* Calculate number of Trace-Data frames - One Data frame contains 6 bytes */

			TRACE_FRAME tr_frame;
			
			CPU_SR_ALLOC();		
			CPU_CRITICAL_ENTER();		// disable interrupt 
			
			if ( (noOfFrames + 1) <= TraceBuffer_GetFifoAvblNum() )
			{	// there is enouth buffer on trace buffer 
				/* Get the DataID, to identify the current data-transfer */
		        DataID = Trace_DataID_u8;
		        /* Increase the Data-ID, so that the next data-transfer used a new ID */
		        Trace_IncreaseDataIDBy1();

		        /* Write the content of the Server-Data-Frame which is transmitted first */
		        tr_frame.trServerData.Trace_Header_u8	= TRACE_SERVER_DATA_HEADER;
		        tr_frame.trServerData.TraceID_u16		= TraceID_u16;
		        tr_frame.trServerData.DataLength_u8		= Length;
		        tr_frame.trServerData.Trace_DataID_u8	= DataID;
		       	tr_frame.trRawData[6]         			= TRACE_CLEAR_LETTER;
		        tr_frame.trRawData[7]         			= TRACE_CLEAR_LETTER;

		        TraceBuffer_AddFrame(&tr_frame);

		        
				/* In this loop, one Data-Frame after another is filled with content */
				for ( uint8_t frameCount = 0 ; frameCount < noOfFrames ; frameCount++ )
				{
					tr_frame.trData.Trace_Header_u8  = (  TRACE_DATA_HEADER   | (frameCount & SEQUENCE_NUMBER_BIT_MASK ) );
					tr_frame.trData.Trace_DataID_u8 = DataID;
					
					tr_frame.trData.Val[0] = *dataPointer++;	// 6 byte per frame
					tr_frame.trData.Val[1] = *dataPointer++;
					tr_frame.trData.Val[2] = *dataPointer++;
					tr_frame.trData.Val[3] = *dataPointer++;
					tr_frame.trData.Val[4] = *dataPointer++;
					tr_frame.trData.Val[5] = *dataPointer++;

					TraceBuffer_AddFrame(&tr_frame);
				}
			
			}

			CPU_CRITICAL_EXIT();
		}
	}
}




/************************************************************************************************/
/* Function Name   :  Trace_String(uint16_t  TraceID_u16, const char* str)                        */
/* Description     :  Call this function to log information that consists of a format string    */
/*                      and a null terminated string of "printable" characters.                 */
/************************************************************************************************/
/**  @brief  Call this function to log information that consists of a format string and a null terminated string of "printable" characters.
 *   @param  TraceID_u16 The Trace-ID for the text to trace. This ID is generated automatocally with the compilation process.
 *   @param  str Pointer to the string to trace.
 */
void Trace_String(uint16_t  TraceID_u16,
                  const char* str)
{
    /*lint -save -e64*/ /* lint has a problem with signed/unsigned pointers*/
    Trace_Mem(TraceID_u16, strlen(str), (uint8_t*)str);	//lint !e734
    /*lint -restore */
    return;
}



/***********************************************************************************************/
/* Function Name   :  printf_(const char* str)                                                 */
/* Description     :  Transmit a string of characters.                                         */
/***********************************************************************************************/
/**  @brief  Transmit a string of characters.
 *   @param  str Pointer to the string to trace.
 */
void printf_(const char* str)
{
    /*lint -save -e64*/ /* lint has a problem with signed/unsigned pointers*/
    Trace_Mem(TRACE_ID_PRINTF, strlen((char*)str),(const uint8_t*) str);	//lint !e734
    /*lint -restore */
    return;
}




/***********************************************************************************************/
/* Function Name   :  printf_1(const char* str, unsigned long p1)                              */
/* Description     :  Transmit a formatted string with a value.                                */
/***********************************************************************************************/
/**  @brief  Transmit a formatted string with a value.
 *   @param  str Pointer to the string to trace.
 *   @param  p1 Pointer to the value to trace.
 */
void printf_1(const char* str, unsigned long p1)
{
    char  text[MAX_BYTES_PER_TRACE_MEM];

    sprintf(text, str, p1);
    Trace_Mem(TRACE_ID_PRINTF, strlen((char*)text),(const uint8_t*) text); //lint !e734
    /*lint -restore */
    return;
}




/**
 * @}
 */
