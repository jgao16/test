#ifndef _LOG_HANDLER_H_
#define _LOG_HANDLER_H_

#include"trace_cfg.h"
#include"Trace_Internal.h"

#include "log_cfg.h"
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
// extern module log level declear
<<EXTERN_MODULE_LOG_LVL_DECLEAR>>

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void LogHandle_Init(void);

void LogHandle_RestoreModuleLog(void);
void LogHandle_DisModuleLog(void);
void LogHandle_SetModuleMask(eTR_MODULE_SET_TYPE setType, uint32_t ModuleMask);
#if LOG_EEP_MOD_LVL_AVBL
void LogHandle_RestoreModuleLogLvlFromEep(void);
#else
#define LogHandle_RestoreModuleLogLvlFromEep()	do{}while(0)	
#endif

void LogHandle_SetModuleLogLvl(uint8_t module_id, uint8_t log_lvl);
void LogHandle_OutputAllModLvl(void);

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
<<LOGHANDLE_SET_MOD_LOG_LVL>>
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

/***********************************************************************************************/
/* Function Name   :    SetModuleMask(eTR_MODULE_SET_TYPE setType, uint32_t ModuleMask)          */
/* Description     :    This function sets the Module-mask.                                    */
/***********************************************************************************************/
/**  @brief  This function sets the Module-mask.
 *   @param  setType The type of module-mask set-operation
 *   @param  ModuleMask The module-mask to set
 */
//void	SetModuleMask    (eTR_MODULE_SET_TYPE setType, uint32_t ModuleMask);

//void	SetModuleInitialMask(void);


/***********************************************************************************************/
/* Function Name   :    Trace_GetConfiguredModuleMask(void)                                    */
/* Description     :    This function returns the default Module-mask as configured in         */
/*                      Tr_Modules.cfg.                                                        */
/***********************************************************************************************/
/**  @brief  This function returns the default Module-mask as configured in Tr_Modules.cfg.
 */
//uint32_t Trace_GetConfiguredModuleMask(void);


void Trace_GetModuleLvl(uint8 channel,uint8 module_id);
/***********************************************************************************************/
/* Function Name   :    Trace_Text(uint16_t TraceID_u16)                                         */
/* Description     :    Call this function to log a simple text message.                       */
/***********************************************************************************************/
/**  @brief  Call this function to log a simple text message.
 *   @param  TraceID_u16 The Trace-ID for the text to trace. This ID is generated automatically by the compilation process.
 */
   void     Trace_Text        (uint16_t TraceID_u16);



/***********************************************************************************************/
/* Function Name   :  Trace_Value(uint16_t TraceID_u16, uint32_t Val_u32)                          */
/* Description     :  This function may be called when a text message ALONG WITH A 32 bit-VALUE*/
/*                    needs to be logged.                                                      */
/***********************************************************************************************/
/**  @brief  This function may be called when a text message ALONG WITH A 32 bit-VALUE needs to be logged.
 *   @param  TraceID_u16 The Trace-ID for the text to trace. This ID is generated automatically by the compilation process.
 *   @param  Val_u32 The 32-bit value to trace.
 */
   void     Trace_Value       (uint16_t TraceID_u16, uint32_t Val_u32);




/************************************************************************************************/
/* Function Name    :  Trace_Value2(uint16_t TraceID_u16,   uint16_t Val1_u16,   uint16_t Val2_u16)   */
/* Description      :  Call this function to log a text message ALONG WITH 2 16 bit-VALUES      */
/************************************************************************************************/
/**  @brief  Call this function to log a text message ALONG WITH 2 16 bit-VALUES
 *   @param  TraceID_u16 The Trace-ID for the text to trace. This ID is generated automatically by the compilation process.
 *   @param  Val1_u16 The first 16-bit value to trace.
 *   @param  Val2_u16 The second 16-bit value to trace.
 */
   void     Trace_Value2      (uint16_t TraceID_u16, uint16_t Val1_u16, uint16_t Val2_u16);





/***********************************************************************************************/
/* Function Name     :  Trace_Value4( uint16_t TraceID_u16, uint8_t Val1_u8,                       */
/*                                   uint8_t Val2_u8,       uint8_t Val3_u8,      uint8_t Val4_u8)   */
/* Description       :  Call this function to log a text message ALONG WITH 4 8 bit-VALUES     */
/***********************************************************************************************/
/**  @brief  Call this function to log a text message ALONG WITH 4 8 bit-VALUES
 *   @param  TraceID_u16 The Trace-ID for the text to trace. This ID is generated automatically by the compilation process.
 *   @param  Val1_u8 The first 8-bit value to trace.
 *   @param  Val2_u8 The second 8-bit value to trace.
 *   @param  Val3_u8 The third 8-bit value to trace.
 *   @param  Val4_u8 The fourth 8-bit value to trace.
 */
   void     Trace_Value4      (uint16_t TraceID_u16, uint8_t Val1_u8, uint8_t Val2_u8, uint8_t Val3_u8, uint8_t Val4_u8);

/************************************************************************************************/
/* Function Name     :  Trace_Mem( uint16_t TraceID_u16,   uint8_t Length,    char * ptr     )      */
/* Description       :  Call this function to log complete block of memory                      */
/************************************************************************************************/
/**  @brief   Call this function to log complete block of memory
 *   @param  TraceID_u16 The Trace-ID for the text to trace. This ID is generated automatically by the compilation process.
 *   @param  Length Number bytes to log
 *   @param  ptr Pointer to the start-address of of the memory-block to trace.
 */
   void      Trace_Mem         (uint16_t TraceID_u16, uint8_t Length, const uint8_t * dataPointer);



/************************************************************************************************/
/* Function Name   :  Trace_String(uint16_t  TraceID_u16, const char* str)                        */
/* Description     :  Call this function to log information that consists of a format string    */
/*                      and a null terminated string of "printable" characters.                 */
/************************************************************************************************/
/**  @brief  Call this function to log information that consists of a format string and a null terminated string of "printable" characters.
 *   @param  TraceID_u16 The Trace-ID for the text to trace. This ID is generated automatically by the compilation process.
 *   @param  str Pointer to the string to trace.
 */
   void      Trace_String      (uint16_t TraceID_u16, const char * str);



/***********************************************************************************************/
/* Function Name   :  printf_(const char* str)                                                 */
/* Description     :  Transmit a string of characters.                                         */
/***********************************************************************************************/
/**  @brief  Transmit a string of characters.
 *   @param  str Pointer to the string to trace.
 */

   void      printf_(const char* str);




/***********************************************************************************************/
/* Function Name   :  printf_1(const char* str, unsigned long p1)                              */
/* Description     :  Transmit a formatted string with a value.                                */
/***********************************************************************************************/
/**  @brief  Transmit a formatted string with a value.
 *   @param  str Pointer to the string to trace.
 *   @param  p1 Pointer to the value to trace.
 */
   void      printf_1(const char* str, unsigned long p1);


   void     Trace_SendUARTExtern(uint8_t * pDataToSend, uint8_t NoOfBytes);




/**
 * @}
 */


#endif /* _TRACE_HANDLER_H_ */

