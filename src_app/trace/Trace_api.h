#ifndef _TRACE_API_H_
#define _TRACE_API_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "Trace_cfg.h"
#include "Trace_Formats.h"
#include "log_cfg.h"
#include "log_handle.h"
#include "trace_version.h"
#include "Trace_Server.h"


#ifndef LOG_ENABLE
//#define LOG_ENABLE 0
#error "Need define LOG_ENABLE 0/1" 
#endif

#ifndef LOG_EEP_MOD_LVL_AVBL
#error "need define LOG_EEP_MOD_LVL_AVBL 0/1"
#endif


#if LOG_ENABLE
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void Trace_registerCallbackFunction(uint32_t FunctionID,                                            \
                                    void (* PointerToFunction)(uint8_t const, char const*),                    \
                                    CallbackFunctionElement * PointerToCallbackFunctionElement);

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

/*lint -esym(40,tr*) */
//The following block of complicated macros is just a way to cheat the compiler into
//accepting 3 token-pasting operators simultaneously for all the TRACE macros.
#define Trace_Text__(a,b,c)                        Trace_Text  (a##b##_##c)
#define Trace_Mem__(a,b,c,d,e)                     Trace_Mem(a##b##_##c, d, e)
#define Trace_String__(a,b,c,d)                    Trace_String(a##b##_##c, d)
#define Trace_Value__(a,b,c,d)                     Trace_Value (a##b##_##c, d)
#define Trace_Value2__(a,b,c,d,e)                  Trace_Value2(a##b##_##c, d, e)
#define Trace_Value4__(a,b,c,d,e,f,g)              Trace_Value4(a##b##_##c, d, e, f, g)
#define Trace_SM_Text__(a,b,c,d)                   Trace_Text(a##b##_##c + d)
#define Trace_SM_Value__(a,b,c,d,e)                Trace_Value(a##b##_##c + d, e)
#define Trace_SM_Value2__(a, b, c, d, e, f)        Trace_Value2(a##b##_##c + d, e, f)
#define Trace_SM_Value4__(a, b, c, d, e, f, g, h)  Trace_Value4(a##b##_##c + d, e, f, g, h)

#define Trace_Text_(a,b,c)                        Trace_Text__(a,b,c)
#define Trace_Mem_(a,b,c,d,e)                     Trace_Mem__(a,b,c,d,e)
#define Trace_String_(a,b,c,d)                    Trace_String__(a,b,c,d)
#define Trace_Value_(a,b,c,d)                     Trace_Value__(a,b,c,d)
#define Trace_Value2_(a,b,c,d,e)                  Trace_Value2__(a,b,c,d,e)
#define Trace_Value4_(a,b,c,d,e,f,g)              Trace_Value4__(a,b,c,d,e,f,g)
#define Trace_SM_Text_(a,b,c,d)                   Trace_SM_Text__(a,b,c,d)
#define Trace_SM_Value_(a,b,c,d,e)                Trace_SM_Value__(a,b,c,d,e)
#define Trace_SM_Value2_(a, b, c, d, e, f)        Trace_SM_Value2__(a, b, c, d, e, f)
#define Trace_SM_Value4_(a, b, c, d, e, f, g, h)  Trace_SM_Value4__(a, b, c, d, e, f, g, h)


#define TRACE_API_INIT()					do{Trace_API_Init();}while(0)
#define TRACE_TEXT(LogLvl, ModuleName, Text)     					\
	{/*lint -save -e568 -e685*/if( (LogLvl) == 0x00 || ModuleName##_Lvl >= (LogLvl) ) { Trace_Text_  (tr_, _TR_FILE , __LINE__) ; }/*lint -restore*/}
#define TRACE_MEM(LogLvl, ModuleName, Text, Length, DataPointer)	\
	{/*lint -save -e568 -e685*/if(  (LogLvl) == 0x00 || ModuleName##_Lvl >= (LogLvl) ) { Trace_Mem_(tr_, _TR_FILE , __LINE__, (Length), (DataPointer)); }/*lint -restore*/}
#define TRACE_STRING(LogLvl, ModuleName, FormatText, String)    	\
	{/*lint -save -e568 -e685*/if(  (LogLvl) == 0x00 || ModuleName##_Lvl >= (LogLvl) ) { Trace_String_(tr_, _TR_FILE , __LINE__, String); }/*lint -restore*/}
#define TRACE_VALUE(LogLvl, ModuleName, FormatText, Value)			\
		{/*lint -save -e568 -e685*/if(  (LogLvl) == 0x00 || ModuleName##_Lvl >= (LogLvl) ) { Trace_Value_ (tr_, _TR_FILE , __LINE__, (Value)); }/*lint -restore*/}
#define TRACE_VALUE2(LogLvl, ModuleName, FormatText, Value1, Value2)	\
	{/*lint -save -e568 -e685*/if(  (LogLvl) == 0x00 || ModuleName##_Lvl >= (LogLvl) ) { Trace_Value2_(tr_, _TR_FILE , __LINE__, (Value1), (Value2)); }/*lint -restore*/}
#define TRACE_VALUE4(LogLvl, ModuleName, FormatText, Value1, Value2, Value3, Value4)	\
	{/*lint -save -e568 -e685*/if(  (LogLvl) == 0x00 || ModuleName##_Lvl >= (LogLvl) ) { Trace_Value4_(tr_, _TR_FILE , __LINE__, (Value1), (Value2), (Value3), (Value4)); }/*lint -restore*/}

#define TRACE_SM_TEXT(LogLvl, ModuleName, offset)                                      	{/*lint -save -e568 -e685*/if( ModuleName##_Lvl >= (LogLvl) ) { Trace_SM_Text_(tr_, _TR_FILE, __LINE__, (offset)); }/*lint -restore*/}
#define TRACE_SM_VALUE(LogLvl, ModuleName, offset, Value)                              	{/*lint -save -e568 -e685*/if( ModuleName##_Lvl >= (LogLvl) ) { Trace_SM_Value_(tr_, _TR_FILE, __LINE__, (offset), (Value)); }/*lint -restore*/}
#define TRACE_SM_VALUE2(LogLvl, ModuleName, offset, Value1, Value2)                    	{/*lint -save -e568 -e685*/if( ModuleName##_Lvl >= (LogLvl) ) { Trace_SM_Value2_(tr_, _TR_FILE, __LINE__, (offset), (Value1), (Value2)); }/*lint -restore*/}
#define TRACE_SM_VALUE4(LogLvl, ModuleName, offset, Value1, Value2, Value3, Value4)    	{/*lint -save -e568 -e685*/if( ModuleName##_Lvl >= (LogLvl) ) { Trace_SM_Value4_(tr_, _TR_FILE, __LINE__, (offset), (Value1), (Value2), (Value3), (Value4)); }/*lint -restore*/}


#if (TRACE_ALLOW_CALLBACK_FUNCTION_CALLS != 0)
	#define Trace_registerCallbackFunction__(a,b,c,d,e)    Trace_registerCallbackFunction(a##b##_##c,d,e)
	#define Trace_registerCallbackFunction_(a,b,c,d,e)     Trace_registerCallbackFunction__(a,b,c,d,e)
	#define TRACE_REGISTER_CALLBACK_FUNCTION(ptrToCallbackFunction, ptrToCallbackFunctionElement);    {Trace_registerCallbackFunction_(cbf_, _TR_FILE, __LINE__, ptrToCallbackFunction, ptrToCallbackFunctionElement);}
#else
	#define TRACE_REGISTER_CALLBACK_FUNCTION(ptrToCallbackFunction, ptrToCallbackFunctionElement)   {}
#endif

#define TRACE_RESTORE_MODULE_LVL_FROM_EEP()		do{LogHandle_RestoreModuleLogLvlFromEep();/*TracePhy_SetCom((TRACE_PHY_COM)LogPhy_GetLogChannel());*/}while(0)


#else // !LOG_ENABLE
#define TRACE_API_INIT()													do{}while(0)
#define TRACE_TEXT(LogLvl, ModuleName, Text)     							do{}while(0)
#define TRACE_MEM(LogLvl, ModuleName, Text, Length, DataPointer)			do{}while(0)
#define TRACE_STRING(LogLvl, ModuleName, FormatText, String)    			do{}while(0)
#define TRACE_VALUE(LogLvl, ModuleName, FormatText, Value)					do{(void)(Value);}while(0)
#define TRACE_VALUE2(LogLvl, ModuleName, FormatText, Value1, Value2)		do{(void)(Value1);(void)(Value2);}while(0)
#define TRACE_VALUE4(LogLvl, ModuleName, FormatText, Value1, Value2, Value3, Value4)	do{(void)(Value1);(void)(Value2);(void)(Value3);(void)(Value4);}while(0)
#define TRACE_REGISTER_CALLBACK_FUNCTION(ptrToCallbackFunction, ptrToCallbackFunctionElement)   do{(void)ptrToCallbackFunction;(void)(ptrToCallbackFunctionElement);}while(0)
#define TRACE_RESTORE_MODULE_LVL_FROM_EEP()		do{}while(0)

#endif	//{LOG_ENABLE}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/



#ifdef __cplusplus
   }
#endif

#endif /* TRACE_API.h */

