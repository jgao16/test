/**********************************************************************************/
/* trace_phy.h                                                                    */
/* trace server  			                                                       */
/**********************************************************************************/
#ifndef TRACE_PHY_CAN_H_H__
#define TRACE_PHY_CAN_H_H__
//#pragma once

#include "trace_cfg.h"
#include "Trace_Internal.h"

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#if TRACE_CAN_EN > 0

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#include "canstack_type.h"
#include "canif.h"
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

#ifdef __cplusplus
extern "C" {
#endif


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void TracePhyCAN_Init(void);

eTR_SEND_STATE 	TracePhyCAN_SendFrame(TRACE_FRAME * trFrame);
void 			TracePhyCAN_RxCallback(const uint16_t canif_rxpdu_id, const Can_PduInfoType * const canif_rxpdu_info);
void 			TracePhyCAN_TxCallback(const uint16_t canif_txpdu_id);

void 			TracePhyCAN_PollingFunc(void);

#ifdef __cplusplus
}
#endif


#endif //TRACE_CAN_EN > 0

#endif // __gen_type_cf_type_h_h


