

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#include "trace_cfg.h"

#include "Trace_Buffer.h"


#include "hal_hw.h"
#include "hal_uart.h"
#include "Trace_Dispatcher.h"
#include "Trace_Common.h"
#include "log_cfg.h"
#include "trace_phy.h"
#include "trace_phy_uart.h"
#include "hal_spi.h"
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

#define DEF_RXWINDOWSIZE \
      11
//-------------------------------------------------------------------------------
/** Size in bytes of a Trace-message transmitted over the UART interface.
 *  The UART Trace-message consists of a start-byte, 8 bytes from the actual Trace-frame and a stop-byte
 */
#define DEF_UARTFrameLength_u8 \
      10
//-------------------------------------------------------------------------------
/** The start-byte, that every UART Trace-message has to start with
 */
#define DEF_START_BYTE \
       0xF2
//-------------------------------------------------------------------------------
/** The stop-byte, that every UART Trace-message has to be finished with
 */
#define DEF_STOP_BYTE \
       0xF8

#define MACRO_START_OF_WINDOW(X) \
	((X < (DEF_UARTFrameLength_u8 - 1)) ? ((DEF_RXWINDOWSIZE - (DEF_UARTFrameLength_u8 - 1)) + X) : (X-(DEF_UARTFrameLength_u8-1)))

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
/* rs232 trace interface 										*/
static void *  rs232_hdl;
static uint8_t	RXWindowWr;
static uint8_t 	RXWindowRd;
static uint8_t 	t_szFrame[DEF_UARTFrameLength_u8];
/** Receive-buffer for bytes received from the RS232 interface  */
static uint8_t RXWindow[DEF_RXWINDOWSIZE];

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void TracePhy_UartRxCallback (void*arg,unsigned char *rx_dat, unsigned short len);
static void TracePhy_UartTxCallback(void*arg);
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void TracePhyUart_Init(void)
{
    RXWindowWr = 0;
    RXWindowRd = 0;
    rs232_hdl = HalUart_GetDev(TRACE_UART_ID);
    if ( rs232_hdl != NULL )
    {
        HalUart_SetParameter(rs232_hdl,HAL_UART_BAUD_115200,HAL_UART_PARITY_NONE,HAL_UART_WORD_8_BITS, HAL_UART_STOP_1_BIT);
        HalUart_SetCalBk(rs232_hdl,NULL, TracePhy_UartRxCallback, TracePhy_UartTxCallback, NULL, NULL, NULL);
        HalUart_InitDev(rs232_hdl);
    }
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
eTR_SEND_STATE TracePhyUart_SendFrame(TRACE_FRAME * trFrame)
{
    (void)trFrame;
	
    t_szFrame[0] 	= 	DEF_START_BYTE;
    t_szFrame[1]	=	trFrame->trRawData[0];
    t_szFrame[2]	=	trFrame->trRawData[1];
    t_szFrame[3]	=	trFrame->trRawData[2];
    t_szFrame[4]	=	trFrame->trRawData[3];
    t_szFrame[5]	=	trFrame->trRawData[4];
    t_szFrame[6]	=	trFrame->trRawData[5];
    t_szFrame[7]	=	trFrame->trRawData[6];
    t_szFrame[8]	=	trFrame->trRawData[7];
    t_szFrame[9] 	= 	DEF_STOP_BYTE;
    HalUart_Send(rs232_hdl, t_szFrame, sizeof(t_szFrame));

    return SEND_PENDING;
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void TracePhyUart_PollingFunc(void)
{
	
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void TracePhy_UartTxCallback(void*arg)
{
    (void)arg;
    Trace_Dispatcher_TransmitComplete();
}
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void TracePhy_UartRxCallback (void*arg,unsigned char *rx_dat, unsigned short len)
{
    (void)arg;
    (void)rx_dat;

    TRACE_FRAME receivedFrame;

    if ( rx_dat != NULL /* && len > 0*/ )
    {
        for ( uint8 ii = 0; ii < len; ii ++ )
        {
            RXWindow[RXWindowWr] = rx_dat[ii];             // Add received byte to RX-buffer
            if (DEF_STOP_BYTE  == RXWindow[RXWindowWr])     // if the currently received byte is a DEF_STOP_BYTE -> End of Frame received
            {
                if ( DEF_START_BYTE ==  RXWindow[MACRO_START_OF_WINDOW(RXWindowWr)])   // check, if the byte with distance of a complete frame is a START-BYTE -> Complete TRACE_FRAME received
                {
                    RXWindowRd = MACRO_START_OF_WINDOW(RXWindowWr) + 1;
                    ( DEF_RXWINDOWSIZE <= RXWindowRd ) ? (RXWindowRd = 0) : (RXWindowRd = RXWindowRd);        // Set Read-Index to next index in RXWindows-ringbuffer

                    for (uint8 i = 0; i < TRACE_FRAME_SIZE ; i++ )              // Copy recieved Frame into RxFifo.
                    {
                        receivedFrame.trRawData[i] = RXWindow[RXWindowRd];
                        RXWindowRd++;
                        ( DEF_RXWINDOWSIZE <= RXWindowRd ) ? (RXWindowRd = 0) : (RXWindowRd = RXWindowRd);     // Set Read-Index to next index in RXWindows-ringbuffer
                    }

                    Trace_Dispatcher_AddFrameToFifoRX(&receivedFrame, TRACE_SERIAL_CHANNEL);              // Add the received frame to the RX-buffer
                    Trace_Dispatcher_TraceFrameReceived();
                }
            }
            RXWindowWr++;
            ( DEF_RXWINDOWSIZE <= RXWindowWr ) ? (RXWindowWr = 0) : (RXWindowWr = RXWindowWr);     // Set Write-Index to next index in RXWindows-ringbuffer
        }
    }
}

/** >>>>>>>>>>>>>>>>>>>>>> end of file >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


