

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#include "trace_cfg.h"
#include "Trace_Buffer.h"


//#include "tr_c_rs232.h"
#if TRACE_CAN_EN > 0

#include "hal_hw.h"
#include "hal_uart.h"

#include "dev_ipc.h"

#include "Trace_Dispatcher.h"
#include "Trace_Common.h"
#include "Trace_Common.h"
#include "log_cfg.h"
#include "trace_phy.h"

#include "trace_phy_can.h"
#include "canif.h"

#include "gen_eep_mana.h"
//#include "bsp_misc.h"

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


#define TRACE_CAN_TX_MSG(x)	{Can_PduInfoType pdu;pdu.pdu_length=8;pdu.pdu_data=(x);CanIf_Log_Transmit(0,&pdu);}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
/* rs232 trace interface 										*/

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void TracePhyCAN_Init(void)
{

}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
eTR_SEND_STATE TracePhyCAN_SendFrame(TRACE_FRAME * trFrame)
{
	eTR_SEND_STATE state = SEND_PENDING;

	if ( GenEep_Get_trace_phy_en() & TRACE_CAN0 )
	{
    	TRACE_CAN_TX_MSG(trFrame->trRawData);
	}
	else
	{
		state = SEND_OK;
	}
	return state;
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void TracePhyCAN_RxCallback(const uint16_t canif_rxpdu_id, const Can_PduInfoType * const canif_rxpdu_info)
//void TracePhy_CANRxCallback()
{
	if ( GenEep_Get_trace_phy_en() & TRACE_CAN0 )
	{
	    TRACE_FRAME receivedFrame;
	    receivedFrame.trRawData[0] = canif_rxpdu_info->pdu_data[0];
	    receivedFrame.trRawData[1] = canif_rxpdu_info->pdu_data[1];
	    receivedFrame.trRawData[2] = canif_rxpdu_info->pdu_data[2];
	    receivedFrame.trRawData[3] = canif_rxpdu_info->pdu_data[3];
	    receivedFrame.trRawData[4] = canif_rxpdu_info->pdu_data[4];
	    receivedFrame.trRawData[5] = canif_rxpdu_info->pdu_data[5];
	    receivedFrame.trRawData[6] = canif_rxpdu_info->pdu_data[6];
	    receivedFrame.trRawData[7] = canif_rxpdu_info->pdu_data[7];

	    Trace_Dispatcher_AddFrameToFifoRX(&receivedFrame, TRACE_CAN0_CHANNEL);              // Add the received frame to the RX-buffer
	    Trace_Dispatcher_TraceFrameReceived();

	}
}
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void TracePhyCAN_TxCallback(const uint16_t canif_txpdu_id)
{
    Trace_Dispatcher_TransmitComplete();
}
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void TracePhyCAN_PollingFunc(void)
{

}

#endif // TRACE_CAN_EN > 0
/** >>>>>>>>>>>>>>>>>>>>>> end of file >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


