

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
#include "trace_cfg.h"
#include "Trace_Buffer.h"

#if TRACE_RAW_SPI_EN > 0


//#include "tr_c_rs232.h"

#include "hal_hw.h"
#include "hal_uart.h"

// #include "dev_ipc.h"

#include "Trace_Dispatcher.h"
#include "Trace_Common.h"
#include "Trace_Common.h"
#include "log_cfg.h"
#include "trace_phy.h"


#include "trace_phy_spi.h"

#include "hal_spi.h"
#include "hal_gpio.h"
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

#define DEF_RXWINDOWSIZE \
      11
//-------------------------------------------------------------------------------
/** Size in bytes of a Trace-message transmitted over the UART interface.
 *  The UART Trace-message consists of a start-byte, 8 bytes from the actual Trace-frame and a stop-byte
 */
#define DEF_UARTFrameLength_u8 \
      10
//-------------------------------------------------------------------------------
/** The start-byte, that every UART Trace-message has to start with
 */
#define DEF_START_BYTE \
       0xF2
//-------------------------------------------------------------------------------
/** The stop-byte, that every UART Trace-message has to be finished with
 */
#define DEF_STOP_BYTE \
       0xF8

#define MACRO_START_OF_WINDOW(X) \
	((X < (DEF_UARTFrameLength_u8 - 1)) ? ((DEF_RXWINDOWSIZE - (DEF_UARTFrameLength_u8 - 1)) + X) : (X-(DEF_UARTFrameLength_u8-1)))

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

/** Receive-buffer for bytes received from the SPI interface  */
static uint8_t RXWindow[DEF_RXWINDOWSIZE];
static uint8_t	RXWindowWr;
static uint8_t 	RXWindowRd;

static uint32	last_com_tick;
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void TracePhy_SpiRxCallback (unsigned char *rx_dat, unsigned short len);
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
//static void TracePhySpi_TxCallback(void);
#if (TRACE_RAW_SPI_ATTR&SPI_NSS_HARD) != SPI_NSS_HARD
static void SpiSlaveChipEn(void)
{
	TRACE_RAW_SPI_SLAVE_En();
}
static void SpiSlaveChipDis(void)
{
	TRACE_RAW_SPI_SLAVE_Dis();
}

#endif 

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void TracePhySpi_Init(void)
{
	SPI_DEVICE *trace_spi_dev = HalSpi_GetDev(TRACE_RAW_SPI_ID);
    trace_spi_dev->spi_attr = TRACE_RAW_SPI_ATTR;
    trace_spi_dev->spi_baud = TRACE_RAW_SPI_BAUD;
    //trace_spi_dev->trans_calbk = TracePhySpi_TxCallback;
#if (TRACE_RAW_SPI_ATTR&SPI_NSS_HARD) != SPI_NSS_HARD    
    trace_spi_dev->chip_en  = SpiSlaveChipEn;
    trace_spi_dev->chip_dis  = SpiSlaveChipDis;

	TRACE_RAW_SPI_SLAVE_CS_INIT();

#endif
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
eTR_SEND_STATE TracePhySpi_SendFrame(TRACE_FRAME * trFrame)
{
	(void)trFrame;
	
	uint8 temp_fram[10];
	uint8 rx_msg[10];
	
    temp_fram[0] 	= 	DEF_START_BYTE;
    temp_fram[1]	=	trFrame->trRawData[0];
    temp_fram[2]	=	trFrame->trRawData[1];
    temp_fram[3]	=	trFrame->trRawData[2];
    temp_fram[4]	=	trFrame->trRawData[3];
    temp_fram[5]	=	trFrame->trRawData[4];
    temp_fram[6]	=	trFrame->trRawData[5];
    temp_fram[7]	=	trFrame->trRawData[6];
    temp_fram[8]	=	trFrame->trRawData[7];
    temp_fram[9] 	= 	DEF_STOP_BYTE;
    HalSpi_Com(TRACE_RAW_SPI_ID, 10, temp_fram, rx_msg);
	
	TracePhy_SpiRxCallback(rx_msg,10);

	last_com_tick = HalOS_GetKernelTick();
	
	return SEND_OK;
}

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
//static void TracePhySpi_TxCallback(void)
//{
//    //Trace_Dispatcher_TransmitComplete();
//}
/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void TracePhySpi_PollingFunc(void)
{
	//while ( TRACE_RAW_SPI_SLAVE_REQ() )
	uint32 cur_tick = HalOS_GetKernelTick();
	if ( cur_tick - last_com_tick > 50)	// at least 50ms,
	{
		last_com_tick = cur_tick;
		
		uint8 rx_msg[10];
		HalSpi_Com(TRACE_RAW_SPI_ID, 10, NULL, rx_msg);
		TracePhy_SpiRxCallback(rx_msg,10);
		//HalOS_Delay(3);
	}
}


/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
static void TracePhy_SpiRxCallback (unsigned char *rx_dat, unsigned short len)
{
    (void)rx_dat;

    TRACE_FRAME receivedFrame;

    //if ( rx_dat != NULL /* && len > 0*/ )
    {
        for ( uint8 ii = 0; ii < len; ii ++ )
        {
            RXWindow[RXWindowWr] = rx_dat[ii];             // Add received byte to RX-buffer
            if (DEF_STOP_BYTE  == RXWindow[RXWindowWr])     // if the currently received byte is a DEF_STOP_BYTE -> End of Frame received
            {
                if ( DEF_START_BYTE ==  RXWindow[MACRO_START_OF_WINDOW(RXWindowWr)])   // check, if the byte with distance of a complete frame is a START-BYTE -> Complete TRACE_FRAME received
                {
                    RXWindowRd = MACRO_START_OF_WINDOW(RXWindowWr) + 1;
                    ( DEF_RXWINDOWSIZE <= RXWindowRd ) ? (RXWindowRd = 0) : (RXWindowRd = RXWindowRd);        // Set Read-Index to next index in RXWindows-ringbuffer

                    for (uint8 i = 0; i < TRACE_FRAME_SIZE ; i++ )              // Copy recieved Frame into RxFifo.
                    {
                        receivedFrame.trRawData[i] = RXWindow[RXWindowRd];
                        RXWindowRd++;
                        ( DEF_RXWINDOWSIZE <= RXWindowRd ) ? (RXWindowRd = 0) : (RXWindowRd = RXWindowRd);     // Set Read-Index to next index in RXWindows-ringbuffer
                    }

                    Trace_Dispatcher_AddFrameToFifoRX(&receivedFrame, TRACE_SERIAL_CHANNEL);              // Add the received frame to the RX-buffer
                    Trace_Dispatcher_TraceFrameReceived();
                }
            }
            RXWindowWr++;
            ( DEF_RXWINDOWSIZE <= RXWindowWr ) ? (RXWindowWr = 0) : (RXWindowWr = RXWindowWr);     // Set Write-Index to next index in RXWindows-ringbuffer
        }
    }
}

#endif // TRACE_RAW_SPI_EN > 0

/** >>>>>>>>>>>>>>>>>>>>>> end of file >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


