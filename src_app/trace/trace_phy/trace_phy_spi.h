/**********************************************************************************/
/* trace_phy.h                                                                    */
/* trace server  			                                                       */
/**********************************************************************************/
#ifndef TRACE_PHY_SPI_H_H__
#define TRACE_PHY_SPI_H_H__
//#pragma once

#include "trace_cfg.h"

#if TRACE_RAW_SPI_EN > 0

#include "Trace_Internal.h"

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
/* for trace spi */
#define TRACE_RAW_SPI_ID	(SPI_TRACE_ID)	// for trace log via SPI
	
#define	TRACE_RAW_SPI_ATTR	(/*SPI_NSS_HARD+SPI_HW_NSS_HIGH+SPI_HW_NSS(1)+*/SPI_CPHA_FIRST_EDGE+SPI_MSB_FIRST+SPI_DATA_LENGTH_8)
#define TRACE_RAW_SPI_BAUD	(HAL_SPI_8MBIT)

#define	TRACE_RAW_SPI_SLAVE_CS_INIT()	do{HalGpio_InitPinGpio_Def(HAL_GPIO_PT_E,10,PORT_OUTPUT,OUTPUT_PUSH_PULL,INPUT_NO_PULL);}while(0)
#define TRACE_RAW_SPI_SLAVE_En()		do{HalGpio_SetPinVal(HAL_GPIO_PT_E, 10, 0);}while(0)
#define TRACE_RAW_SPI_SLAVE_Dis()		do{HalGpio_SetPinVal(HAL_GPIO_PT_E, 10, 1);}while(0)

#define TRACE_RAW_SPI_SLAVE_REQ_INIT()	do{HalGpio_InitPinGpio_Def(HAL_GPIO_PT_D,1,PORT_INPUT,OUTPUT_OPEN_DRAIN,INPUT_PULL_DOWN);}while(0)
#define TRACE_RAW_SPI_SLAVE_REQ()		(HalGpio_GetPinVal(HAL_GPIO_PT_D, 1)==TRUE)
#define	TRACE_IPC_CH		(IPC_DEBUG)

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/


#ifdef __cplusplus
extern "C" {
#endif



/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/

/** >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> **/
void TracePhySpi_Init(void);

eTR_SEND_STATE TracePhySpi_SendFrame(TRACE_FRAME * trFrame);

void TracePhySpi_PollingFunc(void);

#ifdef __cplusplus
}
#endif


#endif // TRACE_RAW_SPI_EN > 0


#endif // 


